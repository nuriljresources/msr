﻿using JResources.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.AppConsole
{
    public class MSRStartup
    {
        public void Init()
        {
            Company company = Company.GetByCode("HO");

            if (company == null)
            {
                company = new Company();
                company.ID = Guid.NewGuid();
                company.CompanyCode = "JRESOURCES";
                company.CompanyName = "PT JResources HO";
                company.ADCode = "JRESOURCES";
                company.DocumentCode = "JRN";
                company.Currency = "USD";
                company.IsEnabled = true;
                company.CreatedBy = "SYSTEM";
                company.UpdatedBy = "SYSTEM";
                company.CreatedDate = DateTime.Now;
                company.UpdatedDate = DateTime.Now;
                company.Save<Company>();
            }

            Department department = Department.GetById(Guid.NewGuid());
            if (department == null)
            {
                department = new Department();
                department.ID = Guid.NewGuid();
                department.CompanyId = company.ID;
                department.DepartmentName = "Information Technology";
                department.CategoryCode = "61";
                department.CreatedBy = "SYSTEM";
                department.UpdatedBy = "SYSTEM";
                department.CreatedDate = DateTime.Now;
                department.UpdatedDate = DateTime.Now;
                department.Save<Department>();
            }

            UserLogin userlogin = UserLogin.GetByUsername("nuril.umam");
            if (userlogin == null)
            {
                userlogin = new UserLogin();
                userlogin.ID = Guid.NewGuid();
                userlogin.Username = "nuril.umam";
                userlogin.FullName = "Nuril Umam";
                userlogin.Email = "nuril.umam@jresources.com";
                userlogin.Password = "";
                userlogin.DepartmentId = department.ID;
                userlogin.CreatedBy = "SYSTEM";
                userlogin.UpdatedBy = "SYSTEM";
                userlogin.CreatedDate = DateTime.Now;
                userlogin.UpdatedDate = DateTime.Now;
                userlogin.Save<UserLogin>();
            }


        }
    }
}
