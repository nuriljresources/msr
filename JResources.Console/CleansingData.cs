﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using JResources.Common;
using JResources.Common.Enum;
using JResources.Data;
using JResources.Data.Model;

namespace JResources.AppConsole
{
    public class CleansingData
    {
        public class DocumentCompare
        {
            public string DocumentNo { get; set; }
            public string ItemNo { get; set; }
            public int LineNo { get; set; }
            public decimal Qty { get; set; }
        }

        public void Start()
        {
            List<MSRIssued> documents = MSRIssued.GetAll()
                        .Where(x => x.IsTransferAccpac && (
                            x.IssuedType == RequestStatus.ISSUED ||
                            x.IssuedType == RequestStatus.TRANSFER ||
                            x.IssuedType == RequestStatus.REVERSE_TRANSFER ||
                            x.IssuedType == RequestStatus.PURCHASING) )
                        .ToList();

            var docGroupByCompany = documents.GroupBy(x => x.CompanyId)
                .Select(x => x.ToList())
                .ToList();

            MemoryStream stream = new MemoryStream();
            using (CsvFileWriter csvWriter = new CsvFileWriter(stream))
            {
                CsvRow row = new CsvRow();
                string CompanyName = string.Empty;
                string DocumentType = string.Empty;
                string DocumentNo = string.Empty;
                ResponseModel response = new ResponseModel();

                #region LOOP COMPANY
                foreach (var docCompanies in docGroupByCompany)
                {
                    Company company = Company.GetById(docCompanies.FirstOrDefault().CompanyId);
                    DataRepositoryStoreAccpac.CurrentDataStore[DataRepositoryStoreAccpac.KEY_DATACONTEXT] = new AccpacEntities(JResources.Common.ConnectionManager.GetConnectionStringByCompanyCode(Common.DatabaseGroupName.ACCPAC_DATABASE, company.CompanyCode));

                    foreach (var docCompany in docCompanies)
                    {
                        response = new ResponseModel();
                        CompanyName = company.CompanyName;
                        DocumentType = docCompany.IssuedType;
                        DocumentNo = docCompany.MSRIssuedNo;

                        switch (docCompany.IssuedType)
                        {
                            case RequestStatus.ISSUED:
                                Console.WriteLine("Issued Process");
                                MSR msr = MSR.GetById(docCompany.MSRId);

                                if (string.IsNullOrEmpty(msr.WorkOrderNo))
                                {
                                    var SHIPMENT = AccpacDataContext.CurrentContext.ICSHEHs
                                        .FirstOrDefault(x => x.DOCNUM.Contains(docCompany.MSRIssuedNo));

                                    if (SHIPMENT != null)
                                    {
                                        var DETAIL = AccpacDataContext.CurrentContext.ICSHEDs
                                        .Where(x => x.SEQUENCENO == SHIPMENT.SEQUENCENO);

                                        var DETAIL_ITEM = AccpacDataContext.CurrentContext.ICITEMs;

                                        var SHIPMENT_DETAIL = (from a in DETAIL
                                                               join b in DETAIL_ITEM on
                                                                   a.ITEMNO equals b.ITEMNO
                                                               select new DocumentCompare()
                                                               {
                                                                   ItemNo = b.FMTITEMNO,
                                                                   Qty = a.QUANTITY
                                                               }).ToList();

                                        List<MSRIssuedDetail> details = MSRIssuedDetail.GetByMSRIssuedId(docCompany.ID);
                                        if (SHIPMENT_DETAIL.Count == details.Count)
                                        {


                                        }
                                        else
                                        {
                                            response.SetError("Total Item Not Match");
                                        }
                                    }
                                    else
                                    {
                                        response.SetError("Shipment Not Found");
                                    }

                                }
                                else
                                {

                                }
                                break;

                            case RequestStatus.TRANSFER:
                            case RequestStatus.REVERSE_TRANSFER:

                                break;

                            case RequestStatus.PURCHASING:

                                break;
                        }





                    }

                }
                #endregion
            }




        }



    }
}
