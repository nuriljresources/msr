﻿using JResources.Common;
using JResources.Data;
using JResources.Data.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using JResources.Data.Model.Accpac;
using JResources.Common.Enum;
using JResources.BusinessLogic.Accpac;
using JResources.BusinessLogic.AccpacConnect;
using JResources.BusinessLogic;

namespace JResources.AppConsole
{
    class Program
    {
        #region DATABASE SETUP
        public class DataContextDataStoreAccpac : IDataRepositoryStoreAccpac
        {
            public object this[string key]
            {
                get { return HttpContext.Current.Items[key]; }
                set { HttpContext.Current.Items[key] = value; }
            }
        }

        public class DataContextDataStore : IDataRepositoryStore
        {
            public object this[string key]
            {
                get { return HttpContext.Current.Items[key]; }
                set { HttpContext.Current.Items[key] = value; }
            }
        }

        public static void SetDatabase()
        {
            HttpContext context = HttpContext.Current;
            if (context == null)
            {
                HttpRequest request = new HttpRequest(string.Empty, SiteSettings.BASE_URL, string.Empty);
                HttpResponse response = new HttpResponse(new StreamWriter(new MemoryStream()));
                context = new HttpContext(request, response);
                HttpContext.Current = context;
            }

            DataRepositoryStore.CurrentDataStore = new DataContextDataStore();
            DataRepositoryStoreAccpac.CurrentDataStore = new DataContextDataStoreAccpac();
        }
        #endregion

        #region Set MENU CONSOLE
        public static ConsoleParameterModel GetMenuCode(string[] strConsole)
        {
            ConsoleParameterModel menuCode = new ConsoleParameterModel();
            string lineString = string.Join("", strConsole);
            var splitStrings = lineString.Split('/');

            foreach (var str in splitStrings)
            {
                var strParam = str.Split('=');
                if (strParam.Length > 1)
                {
                    if (!string.IsNullOrEmpty(strParam[0]) && !string.IsNullOrEmpty(strParam[1]))
                    {
                        if (strParam[0] == "MENU")
                        {
                            menuCode.MENU = strParam[1];
                        }

                        if (strParam[0] == "DATE")
                        {
                            DateTime dtTime = DateTime.MinValue;
                            DateTime.TryParse(strParam[1], out dtTime);
                            if (dtTime != DateTime.MinValue)
                            {
                                menuCode.DATE = dtTime;
                            }
                        }

                    }
                }
            }


            return menuCode;
        }

        #endregion

        static void Main(string[] args)
        {
            SetDatabase();
            Console.WriteLine("Start Scheduler MSR");

            var menuCode = GetMenuCode(args);

            var schedulerItems = SchedulerTask.GetAllToProcess()
                .Where(x => x.TryCount <= SiteSettings.SIZE_SCHEDULER_TRY)
                .ToList();

            //schedulerItems = SchedulerTask.GetAll().Where(x => x.ID == 873864).ToList();

            if (menuCode.MENU == "REPORT")
            {
                Console.WriteLine("Start Scheduler REPORT");
                schedulerItems = schedulerItems.Where(x => x.SchedulerType == "REPORT").ToList();
            }
            else
            {
                Console.WriteLine("Start Scheduler POSTING ACCPAC");
                schedulerItems = schedulerItems.Where(x => x.SchedulerType != "REPORT").ToList();

                //if (menuCode.MENU == "FUEL")
                //{
                //    schedulerItems = schedulerItems.Where(x => x.SchedulerType == "FUEL").ToList();
                //}
                //else
                //{
                //    schedulerItems = schedulerItems.Where(x => x.SchedulerType != "FUEL").ToList();
                //}

                if (menuCode.MENU == "CEA-REMINDER")
                {
                    CEALogic.ReminderOutstandingCEA();
                }

                if (menuCode.MENU == "CEA-CLOSED")
                {
                    CEALogic.CloseOutstandingCEA();
                }
            }

            Console.WriteLine("Total Item Process " + schedulerItems.Count);

            foreach (var item in schedulerItems)
            {
                item.SchedulerStatus = SCHEDULER_STATUS.PROCESS;
                item.Save<SchedulerTask>();
            }

            ResponseModel response = new ResponseModel();
            foreach (var item in schedulerItems)
            {
                Console.WriteLine("Start Item " + item.DocumentNo);

                SchedulerLogic taskLogic = new SchedulerLogic(item.ID);
                taskLogic.RunningScheduler();

                Console.WriteLine("");
                Console.WriteLine("FINISH");
            }

            ReportLogic.DeleteReport();

        }


        static void SetErrorMessageManual()
        {
            DateTime DateFrom = DateTime.Now.AddMonths(-3);
            DateTime DateTo = DateTime.Now;

            var schedulerItems = CurrentDataContext.CurrentContext.SchedulerTasks
                .Where(x => x.SchedulerType != "FUEL" && x.SchedulerType != "REPORT" && x.SchedulerStatus == 2 && x.CreatedDate >= DateFrom && x.CreatedDate <= DateTo)
                .ToList();

            for (int i = 0; i < schedulerItems.Count; i++)
            {
                var schedulerItem = schedulerItems[i];


                var logsLast = CurrentDataContext.CurrentContext.SchedulerLogs.Where(x => x.SchedulerTaskId == schedulerItem.ID).OrderByDescending(x => x.CreatedDate).FirstOrDefault();
                if(logsLast != null)
                {
                    var enumError = CurrentDataContext.CurrentContext.EnumTables.FirstOrDefault(x => x.EnumCode == "ERROR_TYPE" && logsLast.LogMessage.Contains(x.EnumValue));
                    if (enumError != null)
                    {
                        schedulerItem.LastLog = enumError.EnumLabel.Replace("[DOCUMENT_NO]", schedulerItem.DocumentNo);
                    }
                    else
                    {
                        schedulerItem.LastLog = "Not identified";
                    }

                    schedulerItem.UpdateSave<SchedulerTask>();
                }

            }
        }
    }


}

