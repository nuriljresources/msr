﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACCPAC.Advantage;
using JResources.Data.Model;
using JResources.Data.Model.Accpac;

namespace JResources.AppConsole.AccpacConnect
{
    public class Transfer
    {
        public static ResponseModel Save(IC0740 ic0740)
        {
            ResponseModel responseModel = new ResponseModel(false);
            Session session = new Session();
            string lineItemNo = string.Empty;

            try
            {
                session = new Session();
                session.Init("", "XY", "XY1000", "61A");
                session.Open(ic0740.DBUsername, ic0740.DBPassword, ic0740.DBName, DateTime.Today, 0);
                DBLink link = session.OpenDBLink(DBLinkType.Company, DBLinkFlags.ReadWrite);
                DBLink link2 = session.OpenDBLink(DBLinkType.System, DBLinkFlags.ReadWrite);
                View IC0740View = link.OpenView("IC0740");
                View IC0730View = link.OpenView("IC0730");
                View IC0741View = link.OpenView("IC0741");
                View IC0735View = link.OpenView("IC0735");
                View IC0733View = link.OpenView("IC0733");
                View IC0738View = link.OpenView("IC0738");
                IC0740View.Compose(new View[] { IC0730View, IC0741View });
                View[] views = new View[9];
                views[0] = IC0740View;
                views[6] = IC0735View;
                views[7] = IC0733View;
                views[8] = IC0738View;
                IC0730View.Compose(views);
                IC0741View.Compose(new View[] { IC0740View });
                IC0735View.Compose(new View[] { IC0730View });
                IC0733View.Compose(new View[] { IC0730View });
                IC0738View.Compose(new View[] { IC0730View });
                IC0740View.Order = 3;
                IC0740View.FilterSelect("(DELETED = 0 )", true, 3, (ViewFilterOrigin)0);
                IC0740View.Order = 3;
                IC0740View.Order = 0;
                IC0740View.Fields.FieldByName("TRANFENSEQ").SetValue("0", false);
                IC0740View.Init();
                bool exists = IC0730View.Exists;
                IC0730View.RecordClear();
                IC0740View.Order = 3;
                exists = IC0730View.Exists;
                exists = IC0730View.Exists;
                if (!string.IsNullOrEmpty(ic0740.DOCNUM))
                {
                    IC0740View.Fields.FieldByName("DOCNUM").SetValue(ic0740.DOCNUM, true);
                }
                exists = IC0740View.Exists;
                IC0740View.Fields.FieldByName("PROCESSCMD").SetValue("1", false);
                IC0740View.Process();
                IC0740View.Fields.FieldByName("TRANSDATE").SetValue(ic0740.TRANSDATE, true);
                IC0740View.Fields.FieldByName("DATEBUS").SetValue(ic0740.DATEBUS, true);
                IC0740View.Fields.FieldByName("EXPARDATE").SetValue(ic0740.EXPARDATE, true);
                IC0740View.Fields.FieldByName("HDRDESC").SetValue(ic0740.HDRDESC, true);
                IC0740View.Fields.FieldByName("REFERENCE").SetValue(ic0740.REFERENCE, true);
                IC0740View.Fields.FieldByName("ADDCOST").SetValue(ic0740.ADDCOST, true);
                IC0740View.Fields.FieldByName("PRORMETHOD").SetValue(ic0740.PRORMETHOD, true);
                exists = IC0730View.Exists;
                IC0730View.RecordClear();

                foreach (var ic0730 in ic0740.IC0730List)
                {
                    lineItemNo = ic0730.ITEMNO;
                    exists = IC0730View.Exists;
                    IC0730View.RecordCreate(ViewRecordCreate.NoInsert);
                    IC0730View.Fields.FieldByName("ITEMNO").SetValue(ic0730.ITEMNO, true);
                    IC0730View.Fields.FieldByName("PROCESSCMD").SetValue("1", false);
                    IC0730View.Process();
                    IC0730View.Fields.FieldByName("FROMLOC").SetValue(ic0730.FROMLOC, true);
                    IC0730View.Fields.FieldByName("TOLOC").SetValue(ic0730.TOLOC, true);
                    IC0730View.Fields.FieldByName("QTYREQ").SetValue(ic0730.QTYREQ, true);
                    IC0730View.Fields.FieldByName("QUANTITY").SetValue(ic0730.QUANTITY, true);
                    IC0730View.Fields.FieldByName("UNITREQ").SetValue(ic0730.UNITREQ, true);
                    IC0730View.Fields.FieldByName("UNIT").SetValue(ic0730.UNIT, true);
                    IC0730View.Fields.FieldByName("COMMENTS").SetValue(ic0730.COMMENTS, true);
                    IC0730View.Fields.FieldByName("MANITEMNO").SetValue(ic0730.MANITEMNO, true);
                    IC0730View.Fields.FieldByName("EXTWEIGHT").SetValue(ic0730.EXTWEIGHT, true);
                    exists = IC0730View.Exists;
                    IC0730View.Fields.FieldByName("FUNCTION").SetValue(ic0730.FUNCTION, true);
                    IC0730View.Process();
                    IC0730View.Insert();
                    IC0730View.Fields.FieldByName("LINENO").SetValue("-" + ic0730.LINENO, false);
                    IC0730View.Read(false);
                }

                IC0740View.Fields.FieldByName("STATUS").SetValue(ic0740.STATUS, false);
                IC0740View.Insert();
                IC0740View.Order = 0;
                IC0740View.Fields.FieldByName("TRANFENSEQ").SetValue("0", false);
                IC0740View.Init();
                exists = IC0730View.Exists;
                IC0730View.RecordClear();
                IC0740View.Order = 3;
                session.Dispose();
                responseModel.SetSuccess();
            }
            catch (Exception exception)
            {
                StringBuilder sbError = new StringBuilder();
                if (session != null && session.Errors != null)
                {

                    int TotalError = session.Errors.Count;
                    for (int i = 0; i < TotalError; i++)
                    {
                        if (session.Errors[i] != null && !string.IsNullOrEmpty(session.Errors[i].Message))
                        {
                            if (!string.IsNullOrEmpty(lineItemNo))
                            {
                                lineItemNo = lineItemNo + "-";
                            }

                            sbError.AppendLine(lineItemNo + session.Errors[i].Message);
                        }
                    }
                }

                sbError.AppendLine(exception.Message);
                responseModel.Message = sbError.ToString();
            }

            if (!responseModel.IsSuccess && session != null)
            {
                session.Dispose();
            }

            if (responseModel.IsSuccess)
            {
                AccpacPostBaseModel AccpacPost = new AccpacPostBaseModel();
                AccpacPost.DBName = ic0740.DBName;
                AccpacPost.DBUsername = ic0740.DBUsername;
                AccpacPost.DBPassword = ic0740.DBPassword;
                AccpacPost.DOCNUM = ic0740.DOCNUM;

                responseModel = PostTransfers(AccpacPost);
            }

            return responseModel;
        }

        public static ResponseModel PostTransfers(AccpacPostBaseModel model)
        {
            ResponseModel responseModel = new ResponseModel(false);
            Session session = new Session();

            try
            {
                session.Init("", "XY", "XY1000", "61A");
                session.Open(model.DBUsername, model.DBPassword, model.DBName, DateTime.Today, 0);
                DBLink link = session.OpenDBLink(DBLinkType.Company, DBLinkFlags.ReadWrite);
                DBLink link2 = session.OpenDBLink(DBLinkType.System, DBLinkFlags.ReadWrite);
                View view = link.OpenView("IC0740");
                ViewFields fields = view.Fields;
                View view2 = link.OpenView("IC0730");
                ViewFields fields2 = view2.Fields;
                View view3 = link.OpenView("IC0741");
                ViewFields fields3 = view3.Fields;
                View view4 = link.OpenView("IC0735");
                ViewFields fields4 = view4.Fields;
                View view5 = link.OpenView("IC0733");
                ViewFields fields5 = view5.Fields;
                View view6 = link.OpenView("IC0738");
                ViewFields fields6 = view6.Fields;
                view.Compose(new View[] { view2, view3 });
                View[] views = new View[9];
                views[0] = view;
                views[6] = view4;
                views[7] = view5;
                views[8] = view6;
                view2.Compose(views);
                view3.Compose(new View[] { view });
                view4.Compose(new View[] { view2 });
                view5.Compose(new View[] { view2 });
                view6.Compose(new View[] { view2 });
                view.Order = 3;
                view.FilterSelect("(DELETED = 0)", true, 3, (ViewFilterOrigin)0);
                view.Order = 3;
                view.Order = 0;
                view.Fields.FieldByName("TRANFENSEQ").SetValue("0", false);
                view.Init();
                bool exists = view2.Exists;
                view2.RecordClear();
                view.Order = 3;
                view.Fields.FieldByName("DOCNUM").SetValue(model.DOCNUM, false);
                exists = view.Exists;
                view.Read(false);
                exists = view.Exists;
                view.Fields.FieldByName("STATUS").SetValue("2", false);
                view.Update();
                view.Order = 0;
                view.Fields.FieldByName("TRANFENSEQ").SetValue("0", false);
                view.Init();
                exists = view2.Exists;
                view2.RecordClear();
                view.Order = 3;
                session.Dispose();
                responseModel.SetSuccess();
            }
            catch (Exception exception)
            {
                StringBuilder sbError = new StringBuilder();
                if (session != null && session.Errors != null)
                {

                    int TotalError = session.Errors.Count;
                    for (int i = 0; i < TotalError; i++)
                    {
                        if (session.Errors[i] != null && !string.IsNullOrEmpty(session.Errors[i].Message))
                        {
                            sbError.Append(session.Errors[i].Message);
                        }
                    }
                }

                sbError.Append(exception.Message);
                responseModel.Message = sbError.ToString();
            }

            if (!responseModel.IsSuccess && session != null)
            {
                session.Dispose();
            }

            return responseModel;
        }


    }
}
