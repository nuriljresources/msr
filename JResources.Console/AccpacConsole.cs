﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACCPAC.Advantage;

namespace JResources.AppConsole
{
    public class AccpacConsole
    {
        public class WorkFlowDocument
        {
            public string DOCNUMBER { get; set; }
            public string SEQUENCE { get; set; }
        }

        public void GetDocumentWorkFlow()
        {
            List<WorkFlowDocument> WorkFlowDocuments = new List<WorkFlowDocument>();
            Session session = new Session();

            session.Init("", "XY", "XY1000", "61A");
            session.Open("FRF", "FRF", "SVMBAK", DateTime.Today, 0);
            DBLink link = session.OpenDBLink(DBLinkType.Company, DBLinkFlags.ReadWrite);
            DBLink link2 = session.OpenDBLink(DBLinkType.System, DBLinkFlags.ReadWrite);
            View view = link.OpenView("PT0030");
            ViewFields fields = view.Fields;
            bool flag2 = view.GoTop();

            view.Browse("DOCNUMBER=RQN0011961", false);
            view.GoTop();

            while (flag2)
            {
                WorkFlowDocuments.Add(new WorkFlowDocument() {
                    DOCNUMBER = view.Fields.FieldByName("DOCNUMBER").Value.ToString(),
                    SEQUENCE = view.Fields.FieldByName("SEQUENCE").Value.ToString()
                });

                flag2 = view.GoNext();
            }

            View view2 = link.OpenView("PT0032");
            ViewFields fields2 = view2.Fields;
            view.Compose(new View[] { view2 });
            view2.Compose(new View[] { view });
            View view3 = link.OpenView("PT0902");
            ViewFields fields3 = view3.Fields;

            //view.Fields.FieldByName("WORKFLOW").SetValue(workFlow, true);
            //view.Fields.FieldByName("SEQUENCE").SetValue(newValue, true);

            view.Read(false);
            //view.Fields.FieldByName("COMMENT").SetValue(comment, false);
            view.Update();
            view.Read(false);
            //view.Fields.FieldByName("APPRSTATUS").SetValue(apprStatus, false);
            view.Update();
            view3.Process();
            session.Dispose();


        }
    }
}
