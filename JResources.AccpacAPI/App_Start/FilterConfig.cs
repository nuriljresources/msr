﻿using JResources.AccpacAPI.Controllers;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace JResources.AccpacAPI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            GlobalConfiguration.Configuration.Filters.Add(new JResourcesApiErrorLog());
        }
    }
}