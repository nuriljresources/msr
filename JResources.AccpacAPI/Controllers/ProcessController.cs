﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using System.Data;
using ACCPAC.Advantage;
using JResources.Data.Model;
using JResources.Data.Model.Accpac;
using System.Text;
using JResources.BusinessLogic.AccpacConnect;
using JResources.BusinessLogic;

namespace JResources.AccpacAPI.Controllers
{
    public class ProcessController : ApiController
    {
        /// <summary>
        /// Transfer Save
        /// </summary>
        /// <param name="ic0740"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Save(AccpacPostBaseModel model)
        {
            SchedulerLogic logicPost = new SchedulerLogic(model.SchedulerId);
            logicPost.IsManualPost = true;
            ResponseModel responseModel = logicPost.RunningScheduler();

            var response = Request.CreateResponse(HttpStatusCode.OK, responseModel);
            return response;
        }


    }
}
