﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using System.Data;
using ACCPAC.Advantage;
using JResources.Data.Model;
using JResources.Data.Model.Accpac;
using System.Text;
using System.IO;
using System.Collections;
using System.Threading.Tasks;
using JResources.BusinessLogic.AccpacConnect;
using JResources.Data;
using JResources.Common.Enum;
using JResources.BusinessLogic;

namespace JResources.AccpacAPI.Controllers
{
    /// <summary>
    /// PURCHASING
    /// </summary>
    public class PurchasingController : ApiController
    {

        // Fields
        private static string lastItemNo = "";
        private static Session session;

        /// <summary>
        /// SAVE REQUISITION
        /// </summary>
        /// <param name="pt0040"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage SaveRequisition(PT0040 pt0040)
        {
            ResponseModel responseModel = Purchasing.SaveRequisition(pt0040);
            var response = Request.CreateResponse(HttpStatusCode.OK, responseModel);
            return response;
        }

        /// <summary>
        /// SAVE WORKFLOW DOCUMENT
        /// </summary>
        /// <param name="wd0100"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage SaveWorkflowDocument(WD0100 wd0100)
        {
            ResponseModel responseModel = new ResponseModel(false);
            Session session = new Session();

            try
            {
                int num3;
                session.Init("", "XY", "XY1000", "61A");
                session.Open(wd0100.DBUsername, wd0100.DBPassword, wd0100.DBName, DateTime.Today, 0);
                DBLink link = session.OpenDBLink(DBLinkType.Company, DBLinkFlags.ReadWrite);
                DBLink link2 = session.OpenDBLink(DBLinkType.System, DBLinkFlags.ReadWrite);
                View view = link.OpenView("PT0040");
                ViewFields fields = view.Fields;
                string filter = "RQNNUMBER LIKE " + wd0100.RQNNUMBER + " ";
                view.Browse(filter, true);
                string newValue = "";


                for (bool flag2 = view.GoTop(); flag2; flag2 = view.GoNext())
                {
                    newValue = Convert.ToString(view.Fields.FieldByName("RQNHSEQ").Value);
                }
                View view2 = link.OpenView("WD0100");
                ViewFields fields2 = view2.Fields;
                View view3 = link.OpenView("WD0101");
                ViewFields fields3 = view3.Fields;
                view2.Compose(new View[] { view3 });
                view3.Compose(new View[] { view2 });
                int num = 0;
                View view4 = link.OpenView("WD0001");
                ViewFields fields4 = view4.Fields;
                int num2 = 0;
                for (bool flag3 = view4.GoTop(); flag3; flag3 = view4.GoNext())
                {
                    num2 = Convert.ToInt32(view4.Fields.FieldByName("LASTDOCSEQ").Value) + 1;
                }
                bool exists = view4.Exists;
                view4.Read(false);
                view4.Fields.FieldByName("LASTDOCSEQ").SetValue(num2.ToString(), false);
                view4.Update();
                view4.Order = 0;
                view2.Fields.FieldByName("RQNHSEQ").SetValue(newValue, true);
                view2.Fields.FieldByName("DOCSEQ").SetValue(num2.ToString(), true);
                view2.Fields.FieldByName("DOCREV").SetValue("1", true);
                view2.Fields.FieldByName("DOCPATH").SetValue(wd0100.DOCPATH, true);
                view2.Fields.FieldByName("DOCDESC").SetValue(wd0100.DOCDESC, true);


                ArrayList list = new ArrayList(SplitByLenght(EncodeData(wd0100.DOCPATH), 0xf8));
                if (list.Count < 15)
                {
                    exists = view2.Exists;
                    view3.RecordCreate(ViewRecordCreate.NoInsert);
                    view3.Fields.FieldByName("RQNHSEQ").SetValue(newValue, true);
                    view3.Fields.FieldByName("DOCSEQ").SetValue(num2.ToString(), true);
                    view3.Fields.FieldByName("DOCREV").SetValue("1", true);
                    for (num = 0; num < list.Count; num++)
                    {
                        num3 = num + 1;
                        view3.Fields.FieldByName("DOCDATA" + num3).SetValue(list[num].ToString(), true);
                    }
                    view3.Process();
                    view3.Insert();
                    view3.Read(false);
                }
                else
                {
                    int num4 = 0;
                    if ((list.Count % 15) == 0)
                    {
                        num4 = list.Count / 15;
                    }
                    else
                    {
                        num4 = (list.Count / 15) + 1;
                    }
                    int num5 = 0;
                    for (num = 0; num < num4; num++)
                    {
                        exists = view2.Exists;
                        view3.RecordCreate(ViewRecordCreate.NoInsert);
                        view3.Fields.FieldByName("RQNHSEQ").SetValue(newValue, true);
                        view3.Fields.FieldByName("DOCSEQ").SetValue(num2.ToString(), true);
                        view3.Fields.FieldByName("DOCREV").SetValue("1", true);
                        for (int i = 0; i < 15; i++)
                        {
                            string str4 = list[num5].ToString();
                            num3 = i + 1;
                            view3.Fields.FieldByName("DOCDATA" + num3).SetValue(str4, true);
                            if (num5 == (list.Count - 1))
                            {
                                break;
                            }
                            num5++;
                        }
                        view3.Process();
                        view3.Insert();
                        view3.Read(false);
                    }
                }
                view2.Insert();
                session.Dispose();
                responseModel.SetSuccess();
            }
            catch (Exception exception)
            {
                StringBuilder sbError = new StringBuilder();
                if (session != null && session.Errors != null)
                {

                    int TotalError = session.Errors.Count;
                    for (int i = 0; i < TotalError; i++)
                    {
                        if (session.Errors[i] != null && !string.IsNullOrEmpty(session.Errors[i].Message))
                        {
                            sbError.AppendLine(session.Errors[i].Message);
                        }
                    }
                }

                sbError.Append(exception.Message);
                responseModel.Message = sbError.ToString();
            }


            if (session != null)
            {
                session.Dispose();
            }

            var response = Request.CreateResponse(HttpStatusCode.OK, responseModel);
            return response;
        }

        /// <summary>
        /// Trigger WorkFlow Requisition
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public HttpResponseMessage TriggerWorkFlowRequisition(AccpacPostBaseModel model)
        {
            ResponseModel responseModel = new ResponseModel(false);
            Session session = new Session();

            session.Init("", "XY", "XY1000", "61A");
            session.Open(model.DBUsername, model.DBPassword, model.DBName, DateTime.Today, 0);
            DBLink link = session.OpenDBLink(DBLinkType.Company, DBLinkFlags.ReadWrite);
            DBLink link2 = session.OpenDBLink(DBLinkType.System, DBLinkFlags.ReadWrite);
            View view = link.OpenView("PT0030");
            ViewFields fields = view.Fields;
            bool flag2 = view.GoTop();

            view.Browse(string.Format("DOCNUMBER={0}", model.DOCNUM), false);
            view.GoTop();


            view.Fields.FieldByName("APPRSTATUS").SetValue(1, false);
            view.Update();
            session.Dispose();
            responseModel.SetSuccess();

            var response = Request.CreateResponse(HttpStatusCode.OK, responseModel);
            return response;
        }

        [HttpPost]
        public HttpResponseMessage UpdateRequisition(AccpacPostPurchaseModel model)
        {
            ResponseModel responseModel = new ResponseModel();

            try
            {
                Session session = new Session();
                session.Init("", "XY", "XY1000", "61A");
                session.Open(model.DBUsername, model.DBPassword, model.DBName, DateTime.Today, 0);
                DBLink link = session.OpenDBLink(DBLinkType.Company, DBLinkFlags.ReadWrite);
                DBLink link2 = session.OpenDBLink(DBLinkType.System, DBLinkFlags.ReadWrite);
                View view = link.OpenView("PT0030");
                ViewFields fields = view.Fields;
                bool flag2 = view.GoTop();
                string str = "";
                string newValue = "";
                while (flag2)
                {
                    str = (string)view.Fields.FieldByName("DOCNUMBER").Value;
                    if (str == model.DOCNUM)
                    {
                        newValue = Convert.ToString(view.Fields.FieldByName("SEQUENCE").Value);
                        break;
                    }
                    flag2 = view.GoNext();
                }
                View view2 = link.OpenView("PT0032");
                ViewFields fields2 = view2.Fields;
                view.Compose(new View[] { view2 });
                view2.Compose(new View[] { view });
                View view3 = link.OpenView("PT0902");
                ViewFields fields3 = view3.Fields;
                view.Fields.FieldByName("WORKFLOW").SetValue(model.WorkFlow, true);
                view.Fields.FieldByName("SEQUENCE").SetValue(newValue, true);
                view.Read(false);
                view.Fields.FieldByName("COMMENT").SetValue(model.Comment, false);
                view.Update();
                view.Read(false);
                view.Fields.FieldByName("APPRSTATUS").SetValue(model.ApproveStatus, false);
                view.Update();
                view3.Process();
                session.Dispose();
                responseModel.SetSuccess();
            }
            catch (Exception exception)
            {
                StringBuilder sbError = new StringBuilder();
                if (session != null && session.Errors != null)
                {

                    int TotalError = session.Errors.Count;
                    for (int i = 0; i < TotalError; i++)
                    {
                        if (session.Errors[i] != null && !string.IsNullOrEmpty(session.Errors[i].Message))
                        {
                            sbError.AppendLine(session.Errors[i].Message);
                        }
                    }
                }

                sbError.Append(exception.Message);
                responseModel.Message = sbError.ToString();
            }

            if (session != null)
            {
                session.Dispose();
            }


            var response = Request.CreateResponse(HttpStatusCode.OK, responseModel);
            return response;
        }

        public static byte[] GetDetailDoc(string RQNHSEQ, string DOCSEQ, string DBUserName, string DBPassword, string DBName)
        {
            session = new Session();
            session.Init("", "XY", "XY1000", "61A");
            session.Open(DBUserName, DBPassword, DBName, DateTime.Today, 0);
            DBLink link = session.OpenDBLink(DBLinkType.Company, DBLinkFlags.ReadWrite);
            DBLink link2 = session.OpenDBLink(DBLinkType.System, DBLinkFlags.ReadWrite);
            View view = link.OpenView("WD0101");
            ViewFields fields = view.Fields;
            DataTable table = new DataTable();
            int fieldID = 1;
            while (fieldID <= view.Fields.Count)
            {
                table.Columns.Add(new DataColumn(view.Fields.FieldByID(fieldID).Name.ToString()));
                fieldID++;
            }
            DataSet set = new DataSet();
            set.Tables.Add(table);
            string fieldName = "";
            string s = "";
            string filter = "RQNHSEQ =" + RQNHSEQ + " AND DOCSEQ= " + DOCSEQ + " ";
            view.Browse(filter, true);
            for (bool flag2 = view.GoTop(); flag2; flag2 = view.GoNext())
            {
                DataRow row = set.Tables[0].NewRow();
                for (fieldID = 1; fieldID <= view.Fields.Count; fieldID++)
                {
                    fieldName = view.Fields.FieldByID(fieldID).Name.ToString();
                    row[fieldName] = Convert.ToString(view.Fields.FieldByName(fieldName).Value);
                    if ((row[fieldName].ToString().Trim().Replace("\n", "") != "") && (fieldName.Substring(0, 6) == "DOCDAT"))
                    {
                        s = s + row[fieldName].ToString().Trim().Replace("\n", "");
                    }
                }
                set.Tables[0].Rows.Add(row);
            }
            byte[] bytes = Convert.FromBase64String(s);
            string str4 = Encoding.UTF8.GetString(bytes);
            session.Dispose();
            return bytes;
        }

        public static DataSet GetHeaderDoc(string RQNNUMBER, string DBUserName, string DBPassword, string DBName)
        {
            session = new Session();
            session.Init("", "XY", "XY1000", "61A");
            session.Open(DBUserName, DBPassword, DBName, DateTime.Today, 0);
            DBLink link = session.OpenDBLink(DBLinkType.Company, DBLinkFlags.ReadWrite);
            DBLink link2 = session.OpenDBLink(DBLinkType.System, DBLinkFlags.ReadWrite);
            View view = link.OpenView("PT0040");
            ViewFields fields = view.Fields;
            string str = "";
            string filter = "RQNNUMBER LIKE " + RQNNUMBER + " ";
            view.Browse(filter, true);
            for (bool flag2 = view.GoTop(); flag2; flag2 = view.GoNext())
            {
                str = Convert.ToString(view.Fields.FieldByName("RQNHSEQ").Value);
            }
            View view2 = link.OpenView("WD0100");
            ViewFields fields2 = view2.Fields;
            string str3 = "RQNHSEQ =" + str + " ";
            view2.Browse(str3, true);
            bool flag3 = view2.GoTop();
            DataTable table = new DataTable();
            int fieldID = 0;
            fieldID = 1;
            while (fieldID <= view2.Fields.Count)
            {
                table.Columns.Add(new DataColumn(view2.Fields.FieldByID(fieldID).Name.ToString()));
                fieldID++;
            }
            table.Columns.Add("fileName");
            DataSet set = new DataSet();
            set.Tables.Add(table);
            string fieldName = "";
            while (flag3)
            {
                if (Convert.ToString(view2.Fields.FieldByName("RQNHSEQ").Value) == str)
                {
                    DataRow row = set.Tables[0].NewRow();
                    for (fieldID = 1; fieldID <= view2.Fields.Count; fieldID++)
                    {
                        fieldName = view2.Fields.FieldByID(fieldID).Name.ToString();
                        row[fieldName] = Convert.ToString(view2.Fields.FieldByName(fieldName).Value);
                    }
                    set.Tables[0].Rows.Add(row);
                }
                flag3 = view2.GoNext();
            }
            session.Dispose();
            return set;
        }

        public static string GetWorkflowDocument()
        {
            Session session = new Session();
            session.Init("", "XY", "XY1000", "61A");
            session.Open("EIENONE", "EIENONE", "JRNDT", DateTime.Today, 0);
            DBLink link = session.OpenDBLink(DBLinkType.Company, DBLinkFlags.ReadWrite);
            DBLink link2 = session.OpenDBLink(DBLinkType.System, DBLinkFlags.ReadWrite);
            ViewFields fields = link.OpenView("WD0100").Fields;
            View view2 = link.OpenView("WD0101");
            ViewFields fields2 = view2.Fields;
            bool flag = view2.GoTop();
            string str = "";
            DataTable table = new DataTable();
            for (int i = 1; i < view2.Fields.Count; i++)
            {
                table.Columns.Add(new DataColumn(view2.Fields.FieldByID(i).Name.ToString()));
            }
            DataSet set = new DataSet();
            set.Tables.Add(table);
            while (flag)
            {
                str = (string)view2.Fields.FieldByName("DOCDATA1").Value;
                flag = view2.GoNext();
            }
            return str;
        }

        public static string PostApproval(string DBUserName, string DBPassword, string DBName, string workFlow, string sequence, string comment, string apprStatus)
        {
            Session session;
            Exception exception;
            try
            {
                session = new Session();
            }
            catch (Exception exception1)
            {
                exception = exception1;
                return exception.Message;
            }
            try
            {
                session.Init("", "XY", "XY1000", "61A");
                session.Open(DBUserName, DBPassword, DBName, DateTime.Today, 0);
                DBLink link = session.OpenDBLink(DBLinkType.Company, DBLinkFlags.ReadWrite);
                DBLink link2 = session.OpenDBLink(DBLinkType.System, DBLinkFlags.ReadWrite);
                View view = link.OpenView("PT0030");
                ViewFields fields = view.Fields;
                bool flag2 = view.GoTop();
                string str = "";
                string newValue = "";
                while (flag2)
                {
                    str = (string)view.Fields.FieldByName("DOCNUMBER").Value;
                    if (str == sequence)
                    {
                        newValue = Convert.ToString(view.Fields.FieldByName("SEQUENCE").Value);
                        break;
                    }
                    flag2 = view.GoNext();
                }
                View view2 = link.OpenView("PT0032");
                ViewFields fields2 = view2.Fields;
                view.Compose(new View[] { view2 });
                view2.Compose(new View[] { view });
                View view3 = link.OpenView("PT0902");
                ViewFields fields3 = view3.Fields;
                view.Fields.FieldByName("WORKFLOW").SetValue(workFlow, true);
                view.Fields.FieldByName("SEQUENCE").SetValue(newValue, true);
                view.Read(false);
                view.Fields.FieldByName("COMMENT").SetValue(comment, false);
                view.Update();
                view.Read(false);
                view.Fields.FieldByName("APPRSTATUS").SetValue(apprStatus, false);
                view.Update();
                view3.Process();
                session.Dispose();
                if (session.Errors != null)
                {
                    return session.Errors[0].Message;
                }
                return "";
            }
            catch (Exception exception2)
            {
                exception = exception2;
                if (session != null)
                {
                    if (session.Errors != null)
                    {
                        string message = session.Errors[0].Message;
                        session.Dispose();
                        return message;
                    }
                    session.Dispose();
                    if (exception.InnerException != null)
                    {
                        return exception.InnerException.Message;
                    }
                }
                return exception.Message;
            }
        }


        private static string[] SplitByLenght(string s, int split)
        {
            List<string> list = new List<string>();
            int num = s.Length / split;
            for (int i = 0; i < num; i++)
            {
                list.Add(s.Substring(i * split, split) + "\n");
            }
            if ((num * split) != s.Length)
            {
                list.Add(s.Substring(num * split) + "\n");
            }
            return list.ToArray();
        }

        private static string EncodeData(string path)
        {
            string str = "";
            if (!string.IsNullOrEmpty(path))
            {
                FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                str = Convert.ToBase64String(File.ReadAllBytes(path));
            }
            return str;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage CEAToRequisition(PT0040 pt0040)
        {
            var response = Purchasing.CEARequisition(pt0040);

            var http_response = Request.CreateResponse(HttpStatusCode.OK, response);
            return http_response;
        }
    }
}
