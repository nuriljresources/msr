﻿using System;
using System.Web;
using System.Web.Http.Filters;

namespace JResources.AccpacAPI.Controllers
{
    /// <summary>
    /// Accpac Api Error Log Attribute.
    /// </summary>
    public class JResourcesApiErrorLog : ExceptionFilterAttribute
    {
        /// <summary>
        /// Called when [exception].
        /// </summary>
        /// <param name="context">The context.</param>
        public override void OnException(HttpActionExecutedContext context)
        {
            if (context.Exception != null)
            {
                var error = context.Exception;
                var currentUrl = HttpContext.Current.Request.Url.AbsoluteUri;

                //CreateErrorLog(error, username, currentUrl);
            }
        }

        #region Private Methods

        /// <summary>
        /// Creates the error log.
        /// </summary>
        /// <param name="error">The error.</param>
        /// <param name="username">The username.</param>
        /// <param name="currentUrl">The current URL.</param>
        private static void CreateErrorLog(Exception error, string username, string currentUrl)
        {
            //var log = new DataAPI.Model.LogModel
            //{
            //    CreatedDate = DateTime.Now,
            //    Exception = error.InnerException != null ? error.InnerException.Message : error.Message,
            //    LogCategory = Log.Category.API_SERVER_ERROR,
            //    LogMessage = error.Message,
            //    MessageType = Log.Type.ERROR,
            //    Reference = username,
            //    Url = currentUrl,
            //    Trace = error.StackTrace
            //};

            //DataAPI.Log.CreateLog(log, (int)Constant.LogType.WebAPIErrorLog);
        }

        #endregion
    }
}
