﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using System.Data;
using ACCPAC.Advantage;
using JResources.Data.Model;
using JResources.Data.Model.Accpac;
using System.Text;
using JResources.BusinessLogic.AccpacConnect;

namespace JResources.AccpacAPI.Controllers
{
    /// <summary>
    /// VM Document
    /// </summary>
    public class VMDocumentController : ApiController
    {
        /// <summary>
        /// VMDocument POST DOCUMENT
        /// </summary>
        /// <param name="AccpacModel"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Post(AccpacPostBaseModel AccpacModel)
        {
            StringBuilder sbMessage = new StringBuilder();
            ResponseModel responseModel = new ResponseModel(false);

            Session session = new Session();
            string newValue = "0";
            try
            {
                sbMessage.AppendLine("Start post");
                session = new Session();
                session.Init("", "XY", "XY1000", "61A");
                session.Open(AccpacModel.DBUsername, AccpacModel.DBPassword, AccpacModel.DBName, DateTime.Today, 0);
                DBLink link = session.OpenDBLink(DBLinkType.Company, DBLinkFlags.ReadWrite);
                DBLink link2 = session.OpenDBLink(DBLinkType.System, DBLinkFlags.ReadWrite);
                View view = link.OpenView("VM0050");
                ViewFields fields = view.Fields;
                View view2 = link.OpenView("VM0060");
                ViewFields fields2 = view2.Fields;
                View view3 = link.OpenView("VM0061");
                ViewFields fields3 = view3.Fields;
                View view4 = link.OpenView("VM0062");
                ViewFields fields4 = view4.Fields;
                View view5 = link.OpenView("VM0067");
                ViewFields fields5 = view5.Fields;
                View view6 = link.OpenView("VM0068");
                ViewFields fields6 = view6.Fields;
                View view7 = link.OpenView("VM0069");
                ViewFields fields7 = view7.Fields;
                View view8 = link.OpenView("VM0220");
                ViewFields fields8 = view8.Fields;
                View[] views = new View[0x15];
                views[0] = view2;
                view.Compose(views);
                views = new View[9];
                views[0] = view;
                views[1] = view3;
                views[2] = view4;
                views[3] = view5;
                views[4] = view6;
                views[5] = view7;
                view2.Compose(views);
                views = new View[9];
                views[0] = view;
                view3.Compose(views);
                views = new View[14];
                views[0] = view;
                views[1] = view2;
                views[7] = view5;
                view4.Compose(views);
                views = new View[9];
                views[0] = view;
                views[1] = view2;
                views[2] = view3;
                views[3] = view6;
                views[4] = view7;
                view5.Compose(views);
                views = new View[0x11];
                views[0] = view;
                views[1] = view2;
                views[2] = view5;
                views[13] = view3;
                view6.Compose(views);
                views = new View[0x12];
                views[0] = view;
                views[1] = view2;
                views[2] = view5;
                view7.Compose(views);
                view.Fields.FieldByName("WDDOCTYPE").SetValue(newValue, true);
                view.Fields.FieldByName("TXDOCID").SetValue(AccpacModel.DOCNUM, false);
                view.Order = 1;
                string filter = "TXDOCID = " + AccpacModel.DOCNUM + " AND SWREADY = 1 AND WDDOCTYPE = 0";
                sbMessage.AppendLine(filter);
                view.Browse(filter, true);

                int iLoop = 1;
                for (bool flag2 = view.GoTop(); flag2; flag2 = view.GoNext())
                {
                    sbMessage.AppendLine(string.Format("{0} - Start", iLoop));
                    sbMessage.AppendLine(string.Format("{0} - {1}", iLoop, view.Fields.FieldByName("NMDOCID").Value));
                    view.Fields.FieldByName("WDCALCCOMM").SetValue(false, true);
                    view.Fields.FieldByName("SWCALCTAX").SetValue(true, true);
                    view.Process();
                    view8.Init();
                    view8.Fields.FieldByName("NMDOCID").SetValue(view.Fields.FieldByName("NMDOCID").Value, true);
                    view8.Fields.FieldByName("SWCALCTAX").SetValue(false, true);
                    view8.Fields.FieldByName("SWPOSTINV").SetValue(false, true);
                    view8.Fields.FieldByName("SWPOSTCRN").SetValue(false, true);
                    view8.Fields.FieldByName("SWPOSTCST").SetValue(true, true);
                    view8.Fields.FieldByName("TXBILLTO").SetValue(view.Fields.FieldByName("TXBILLTO").Value, true);
                    view8.Fields.FieldByName("TXREF").SetValue(view.Fields.FieldByName("TXREF").Value, true);
                    view8.Process();
                    sbMessage.AppendLine(string.Format("{0} - End", iLoop));
                    iLoop++;
                }

                session.Dispose();
                responseModel.SetSuccess(sbMessage.ToString());
            }
            catch (Exception exception)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.Append(exception.Message);

                if (session != null && session.Errors != null)
                {
                    int TotalError = session.Errors.Count;
                    for (int i = 0; i < TotalError; i++)
                    {
                        if (session.Errors[i] != null && !string.IsNullOrEmpty(session.Errors[i].Message))
                        {
                            sbError.Append(session.Errors[i].Message);
                        }
                    }
                }

                responseModel.Message = sbError.ToString();
            }

            if (session != null)
            {
                session.Dispose();
            }

            var response = Request.CreateResponse(HttpStatusCode.OK, responseModel);
            return response;
        }

        /// <summary>
        /// VMDocument SaveDocument
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Save(VM0050 model)
        {
            ResponseModel responseModel = CMMS.Save(model);

            var response = Request.CreateResponse(HttpStatusCode.OK, responseModel);
            return response;
        }

        /// <summary>
        /// GET VIEW DOCUMENT
        /// </summary>
        /// <param name="AccpacModel"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage View(AccpacPostBaseModel AccpacModel)
        {
            ResponseModel responseModel = new ResponseModel(false);
            Session session = new Session();
            VM0050 model = new VM0050();

            try
            {
                session.Init("", "XY", "XY1000", "61A");
                session.Open(AccpacModel.DBUsername, AccpacModel.DBPassword, AccpacModel.DBName, DateTime.Today, 0);
                DBLink link = session.OpenDBLink(DBLinkType.Company, DBLinkFlags.ReadWrite);
                DBLink link2 = session.OpenDBLink(DBLinkType.System, DBLinkFlags.ReadWrite);
                View view = link.OpenView("VM0050");
                ViewFields fields = view.Fields;
                View view2 = link.OpenView("VM0060");
                ViewFields fields2 = view2.Fields;
                View view3 = link.OpenView("VM0061");
                ViewFields fields3 = view3.Fields;
                View view4 = link.OpenView("VM0062");
                ViewFields fields4 = view4.Fields;
                View view5 = link.OpenView("VM0067");
                ViewFields fields5 = view5.Fields;
                View view6 = link.OpenView("VM0068");
                ViewFields fields6 = view6.Fields;
                View view7 = link.OpenView("VM0069");
                ViewFields fields7 = view7.Fields;
                ViewFields fields8 = link.OpenView("VM0220").Fields;
                View[] views = new View[0x15];
                views[0] = view2;
                view.Compose(views);
                views = new View[9];
                views[0] = view;
                views[1] = view3;
                views[2] = view4;
                views[3] = view5;
                views[4] = view6;
                views[5] = view7;
                view2.Compose(views);
                views = new View[9];
                views[0] = view;
                view3.Compose(views);
                views = new View[14];
                views[0] = view;
                views[1] = view2;
                views[7] = view5;
                view4.Compose(views);
                views = new View[9];
                views[0] = view;
                views[1] = view2;
                views[2] = view3;
                views[3] = view6;
                views[4] = view7;
                view5.Compose(views);
                views = new View[0x11];
                views[0] = view;
                views[1] = view2;
                views[2] = view5;
                views[13] = view3;
                view6.Compose(views);
                views = new View[0x12];
                views[0] = view;
                views[1] = view2;
                views[2] = view5;
                view7.Compose(views);
                view.Fields.FieldByName("WDDOCTYPE").SetValue(0, true);
                view.Fields.FieldByName("TXDOCID").SetValue(AccpacModel.DOCNUM, false);
                view.Order = 1;
                string filter = "TXDOCID = " + AccpacModel.DOCNUM + " ";
                view.Browse(filter, true);
                for (bool flag2 = view.GoTop(); flag2; flag2 = view.GoNext())
                {
                    if ((Convert.ToInt32(view.Fields.FieldByName("WDSTATUS").Value) != 4) && (Convert.ToInt32(view.Fields.FieldByName("WDPSTLINES").Value) > 0))
                    {
                        view2.Browse("WDLINETYPE = 4", true);
                        for (bool flag3 = view2.GoTop(); flag3; flag3 = view2.GoNext())
                        {
                            VM0069 modelItem = new VM0069();
                            modelItem.WDTRANNUM = Convert.ToInt32(view7.Fields.FieldByName("WDTRANNUM").Value);
                            modelItem.TXITEM = view7.Fields.FieldByName("TXITEM").Value.ToString();
                            modelItem.ITEMNO = modelItem.TXITEM.Replace("-", "");                            
                            modelItem.TXDESC = view7.Fields.FieldByName("TXDESC").Value.ToString();
                            modelItem.QTBACKORD = Convert.ToDecimal(view7.Fields.FieldByName("QTBACKORD").Value);
                            modelItem.QTESTIMATE = Convert.ToDecimal(view7.Fields.FieldByName("QTESTIMATE").Value);
                            modelItem.QTSUPPLIED = Convert.ToDecimal(view7.Fields.FieldByName("QTSUPPLIED").Value);
                            model.VM0069List.Add(modelItem);
                        }
                    }
                }

                if (model.VM0069List != null && model.VM0069List.Count > 0)
                {
                    model.DBName = AccpacModel.DBName;
                    model.DBUsername = AccpacModel.DBUsername;
                    model.DBPassword = AccpacModel.DBPassword;
                    model.TXDOCID = AccpacModel.DOCNUM;
                    model.DOCNUM = AccpacModel.DOCNUM;

                    session.Dispose();
                    responseModel.SetSuccess();
                    responseModel.ResponseObject = model;
                }
                else
                {
                    responseModel.SetError("WO {0} dont have detail", AccpacModel.DOCNUM);
                }
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.Append(ex.Message);

                if (session != null && session.Errors != null)
                {
                    int TotalError = session.Errors.Count;
                    for (int i = 0; i < TotalError; i++)
                    {
                        if (session.Errors[i] != null && !string.IsNullOrEmpty(session.Errors[i].Message))
                        {
                            sbError.Append(session.Errors[i].Message);
                        }
                    }
                }

                responseModel.Message = sbError.ToString();
            }


            if (session != null)
            {
                session.Dispose();
            }

            var response = Request.CreateResponse(HttpStatusCode.OK, responseModel);
            return response;

        }

    }
}
