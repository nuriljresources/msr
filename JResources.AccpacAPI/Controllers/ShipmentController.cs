﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using System.Data;
using ACCPAC.Advantage;
using JResources.Data.Model;
using JResources.Data.Model.Accpac;
using System.Text;
using JResources.BusinessLogic.AccpacConnect;

namespace JResources.AccpacAPI.Controllers
{
    /// <summary>
    /// Shipment Method
    /// </summary>
    public class ShipmentController : ApiController
    {
        /// <summary>
        /// Shipment Save
        /// </summary>
        /// <param name="IC0640"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Save(IC0640 ic0640)
        {
            StringBuilder sbMessage = new StringBuilder();
            ResponseModel responseModel = Shipment.Save(ic0640);

            var response = Request.CreateResponse(HttpStatusCode.OK, responseModel);
            return response;
        }

        /// <summary>
        /// Post Shipment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage PostShipment(AccpacPostBaseModel model)
        {
            ResponseModel responseModel = new ResponseModel(false);
            Session session = new Session();

            try
            {                
                session.Init("", "XY", "XY1000", "61A");
                session.Open(model.DBUsername, model.DBPassword, model.DBName, DateTime.Today, 0);
                DBLink link = session.OpenDBLink(DBLinkType.Company, DBLinkFlags.ReadWrite);
                DBLink link2 = session.OpenDBLink(DBLinkType.System, DBLinkFlags.ReadWrite);
                View view = link.OpenView("IC0640");
                ViewFields fields = view.Fields;
                View view2 = link.OpenView("IC0630");
                ViewFields fields2 = view2.Fields;
                View view3 = link.OpenView("IC0645");
                ViewFields fields3 = view3.Fields;
                View view4 = link.OpenView("IC0635");
                ViewFields fields4 = view4.Fields;
                View view5 = link.OpenView("IC0632");
                ViewFields fields5 = view5.Fields;
                View view6 = link.OpenView("IC0636");
                ViewFields fields6 = view6.Fields;
                View[] views = new View[3];
                views[0] = view2;
                views[2] = view3;
                view.Compose(views);
                views = new View[10];
                views[0] = view;
                views[7] = view4;
                views[8] = view5;
                views[9] = view6;
                view2.Compose(views);
                view3.Compose(new View[] { view });
                view4.Compose(new View[] { view2 });
                view5.Compose(new View[] { view2 });
                view.Order = 3;
                view.FilterSelect("(DELETED = 0)", true, 3, (ViewFilterOrigin)0);
                view.Order = 3;
                view.Order = 0;
                view.Fields.FieldByName("SEQUENCENO").SetValue("0", false);
                view.Init();
                bool exists = view2.Exists;
                view2.RecordClear();
                view.Order = 3;
                view.Fields.FieldByName("DOCNUM").SetValue(model.DOCNUM, false);
                exists = view.Exists;
                view.Read(false);
                view.Fields.FieldByName("STATUS").SetValue("2", false);
                view.Update();
                view.Order = 0;
                view.Fields.FieldByName("SEQUENCENO").SetValue("0", false);
                view.Init();
                exists = view2.Exists;
                view2.RecordClear();
                view.Order = 3;
                session.Dispose();
                responseModel.SetSuccess();
            }
            catch (Exception exception)
            {
                StringBuilder sbError = new StringBuilder();
                if (session != null && session.Errors != null)
                {

                    int TotalError = session.Errors.Count;
                    for (int i = 0; i < TotalError; i++)
                    {
                        if (session.Errors[i] != null && !string.IsNullOrEmpty(session.Errors[i].Message))
                        {
                            sbError.AppendLine(session.Errors[i].Message);
                        }
                    }
                }

                sbError.Append(exception.Message);
                responseModel.Message = sbError.ToString();
            }

            if (session != null)
            {
                session.Dispose();
            }

            var response = Request.CreateResponse(HttpStatusCode.OK, responseModel);
            return response;
        }
    }
}
