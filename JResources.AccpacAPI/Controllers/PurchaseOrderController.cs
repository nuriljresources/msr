﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using System.Data;
using ACCPAC.Advantage;
using JResources.Data.Model;
using JResources.Data.Model.Accpac;
using System.Text;

namespace JResources.AccpacAPI.Controllers
{
    /// <summary>
    /// PURCHASE ORDER
    /// </summary>
    public class PurchaseOrderController : ApiController
    {
        /// <summary>
        /// Post Approval
        /// </summary>
        /// <param name="pt0030"></param>
        /// <returns></returns>
        public HttpResponseMessage PostApproval(PT0030 pt0030)
        {
            ResponseModel responseModel = new ResponseModel(false);
            Session session = new Session();

            try
            {                
                session.Init("", "XY", "XY1000", "61A");
                session.Open(pt0030.DBUsername, pt0030.DBPassword, pt0030.DBName, DateTime.Today, 0);
                DBLink link = session.OpenDBLink(DBLinkType.Company, DBLinkFlags.ReadWrite);
                DBLink link2 = session.OpenDBLink(DBLinkType.System, DBLinkFlags.ReadWrite);
                View PT0030View = link.OpenView("PT0030");
                View PO0620View = link.OpenView("PO0620");
                string filter = "PONUMBER =" + pt0030.SEQUENCE + " ";
                PO0620View.Browse(filter, true);
                for (bool flag2 = PO0620View.GoTop(); flag2; flag2 = PO0620View.GoNext())
                {
                    pt0030.SEQUENCE = Convert.ToString(PO0620View.Fields.FieldByName("PORHSEQ").Value);
                }

                View view3 = link.OpenView("PT0032");
                ViewFields fields3 = view3.Fields;
                PT0030View.Compose(new View[] { view3 });
                view3.Compose(new View[] { PT0030View });
                View view4 = link.OpenView("PT0902");
                ViewFields fields4 = view4.Fields;
                PT0030View.Fields.FieldByName("WORKFLOW").SetValue(pt0030.WORKFLOW, true);
                PT0030View.Fields.FieldByName("SEQUENCE").SetValue(pt0030.SEQUENCE, true);
                PT0030View.Read(false);
                PT0030View.Fields.FieldByName("COMMENT").SetValue(pt0030.COMMENT, false);
                PT0030View.Update();
                PT0030View.Read(false);
                PT0030View.Fields.FieldByName("APPRSTATUS").SetValue(pt0030.APPRSTATUS, false);
                PT0030View.Update();
                view4.Process();
                session.Dispose();
                responseModel.SetSuccess();
            }
            catch (Exception exception)
            {
                StringBuilder sbError = new StringBuilder();
                if (session != null && session.Errors != null)
                {

                    int TotalError = session.Errors.Count;
                    for (int i = 0; i < TotalError; i++)
                    {
                        if (session.Errors[i] != null && !string.IsNullOrEmpty(session.Errors[i].Message))
                        {
                            sbError.AppendLine(session.Errors[i].Message);
                        }
                    }
                }

                sbError.Append(exception.Message);
                responseModel.Message = sbError.ToString();
            }


            if (session != null)
            {
                session.Dispose();
            }
             
            var response = Request.CreateResponse(HttpStatusCode.OK, responseModel);
            return response;

        }

        /// <summary>
        /// Save PurchaseOrder
        /// </summary>
        /// <param name="po0620"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage SavePurchaseOrder(PO0620 po0620)
        {
            ResponseModel responseModel = new ResponseModel(false);
            Session session = new Session();
            string lineItemNo = string.Empty;

            try
            {
                session.Init("", "XY", "XY1000", "61A");
                session.Open(po0620.DBUsername, po0620.DBPassword, po0620.DBName, DateTime.Today, 0);
                DBLink link = session.OpenDBLink(DBLinkType.Company, DBLinkFlags.ReadWrite);
                DBLink link2 = session.OpenDBLink(DBLinkType.System, DBLinkFlags.ReadWrite);
                View view = link.OpenView("PT0900");
                ViewFields fields = view.Fields;
                ViewFields fields2 = link.OpenView("PT0035").Fields;
                ViewFields fields3 = link.OpenView("PT0050").Fields;
                View view4 = link.OpenView("PO0620");
                ViewFields fields4 = view4.Fields;
                View view5 = link.OpenView("PO0630");
                ViewFields fields5 = view5.Fields;
                View view6 = link.OpenView("PO0610");
                ViewFields fields6 = view6.Fields;
                View view7 = link.OpenView("PO0632");
                ViewFields fields7 = view7.Fields;
                View view8 = link.OpenView("PO0619");
                ViewFields fields8 = view8.Fields;
                View view9 = link.OpenView("PO0623");
                ViewFields fields9 = view9.Fields;
                View view10 = link.OpenView("PO0633");
                ViewFields fields10 = view10.Fields;
                view4.Compose(new View[] { view6, view5, view7, view8, view9 });
                View[] views = new View[6];
                views[0] = view4;
                views[1] = view6;
                views[2] = view8;
                views[5] = view10;
                view5.Compose(views);
                view6.Compose(new View[] { view4, view5 });
                view7.Compose(new View[] { view4, view8 });
                view8.Compose(new View[] { view4, view6, view5, view7 });
                view9.Compose(new View[] { view4 });
                view10.Compose(new View[] { view5 });
                view4.Order = 1;
                view4.Order = 0;
                view4.Fields.FieldByName("PORHSEQ").SetValue("0", false);
                view4.Init();
                view4.Order = 1;
                bool exists = view5.Exists;
                view5.RecordClear();
                view7.Init();
                view6.Init();
                view4.Fields.FieldByName("PROCESSCMD").SetValue("1", true);
                if (!string.IsNullOrEmpty(po0620.PONumber))
                {
                    view4.Fields.FieldByName("PONUMBER").SetValue(po0620.PONumber, true);
                    view4.Order = 1;
                    exists = view4.Exists;
                }
                view4.Fields.FieldByName("VDCODE").SetValue(po0620.VDCODE, false);
                view4.Fields.FieldByName("PROCESSCMD").SetValue("1", true);
                view4.Process();
                exists = view4.Exists;
                exists = view4.Exists;
                exists = view4.Exists;
                view4.Fields.FieldByName("VDADDRESS1").SetValue(po0620.VDADDRESS1, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("VDADDRESS2").SetValue(po0620.VDADDRESS2, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("VDADDRESS3").SetValue(po0620.VDADDRESS3, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("VDADDRESS4").SetValue(po0620.VDADDRESS4, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("VDCITY").SetValue(po0620.VDCITY, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("VDSTATE").SetValue(po0620.VDSTATE, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("VDZIP").SetValue(po0620.VDZIP, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("VDCOUNTRY").SetValue(po0620.VDCOUNTRY, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("VDPHONE").SetValue(po0620.VDPHONE, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("VDFAX").SetValue(po0620.VDFAX, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("VDEMAIL").SetValue(po0620.VDEMAIL, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("VDCONTACT").SetValue(po0620.VDCONTACT, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("VDPHONEC").SetValue(po0620.VDPHONEC, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("VDFAXC").SetValue(po0620.VDFAXC, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("VDEMAILC").SetValue(po0620.VDEMAILC, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("STCODE").SetValue(po0620.STCODE, false);
                view4.Fields.FieldByName("PROCESSCMD").SetValue("1", true);
                view4.Process();
                exists = view4.Exists;
                exists = view4.Exists;
                exists = view4.Exists;
                view4.Fields.FieldByName("STDESC").SetValue(po0620.STDESC, true);
                exists = view4.Exists;
                view4.Fields.FieldByName("STADDRESS1").SetValue(po0620.STADDRESS1, true);
                exists = view4.Exists;
                view4.Fields.FieldByName("STADDRESS2").SetValue(po0620.STADDRESS2, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("STADDRESS3").SetValue(po0620.STADDRESS3, false);
                view4.Fields.FieldByName("STADDRESS4").SetValue(po0620.STADDRESS4, false);
                view4.Fields.FieldByName("STCITY").SetValue(po0620.STCITY, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("STSTATE").SetValue(po0620.STSTATE, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("STZIP").SetValue(po0620.STZIP, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("STCOUNTRY").SetValue(po0620.STCOUNTRY, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("STPHONE").SetValue(po0620.STPHONE, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("STFAX").SetValue(po0620.STFAX, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("STEMAIL").SetValue(po0620.STEMAIL, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("STCONTACT").SetValue(po0620.STCONTACT, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("STPHONEC").SetValue(po0620.STPHONEC, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("STFAXC").SetValue(po0620.STFAXC, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("STEMAILC").SetValue(po0620.STEMAILC, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("BTCODE").SetValue(po0620.BTCODE, true);
                view4.Fields.FieldByName("PROCESSCMD").SetValue("1", true);
                view4.Process();
                exists = view4.Exists;
                exists = view4.Exists;
                exists = view4.Exists;
                view4.Fields.FieldByName("BTDESC").SetValue(po0620.BTDESC, true);
                exists = view4.Exists;
                view4.Fields.FieldByName("BTADDRESS1").SetValue(po0620.BTADDRESS1, true);
                exists = view4.Exists;
                view4.Fields.FieldByName("BTADDRESS2").SetValue(po0620.BTADDRESS2, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("BTADDRESS3").SetValue(po0620.BTADDRESS3, false);
                view4.Fields.FieldByName("BTADDRESS4").SetValue(po0620.BTADDRESS4, false);
                view4.Fields.FieldByName("BTCITY").SetValue(po0620.BTCITY, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("BTSTATE").SetValue(po0620.BTSTATE, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("BTZIP").SetValue(po0620.BTZIP, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("BTCOUNTRY").SetValue(po0620.BTCOUNTRY, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("BTPHONE").SetValue(po0620.BTPHONE, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("BTFAX").SetValue(po0620.BTFAX, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("BTEMAIL").SetValue(po0620.BTEMAIL, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("BTCONTACT").SetValue(po0620.BTCONTACT, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("BTPHONEC").SetValue(po0620.BTPHONEC, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("BTFAXC").SetValue(po0620.BTFAXC, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("BTEMAILC").SetValue(po0620.BTEMAILC, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("VDACCTSET").SetValue(po0620.VDACCTSET, true);
                exists = view4.Exists;
                view4.Fields.FieldByName("PORTYPE").SetValue(po0620.PORTYPE, true);
                exists = view4.Exists;
                view4.Fields.FieldByName("DATE").SetValue(po0620.DATE, false);
                exists = view4.Exists;
                view4.Fields.FieldByName("WORKFLOW").SetValue(po0620.WORKFLOW, true);
                exists = view4.Exists;
                view4.Fields.FieldByName("HASJOB").SetValue(po0620.HASJOB, true);
                exists = view4.Exists;
                view4.Fields.FieldByName("COSTCTR").SetValue(po0620.COSTCTR, true);
                exists = view4.Exists;
                view4.Fields.FieldByName("EXPARRIVAL").SetValue(po0620.EXPARRIVAL, true);
                exists = view4.Exists;
                view4.Fields.FieldByName("FOBPOINT").SetValue(po0620.FOBPOINT, true);
                exists = view4.Exists;
                view4.Fields.FieldByName("VIANAME").SetValue(po0620.VIANAME, true);
                exists = view4.Exists;
                view4.Fields.FieldByName("TERMSCODE").SetValue(po0620.TERMSCODE, true);
                exists = view4.Exists;
                view4.Fields.FieldByName("DESCRIPTIO").SetValue(po0620.DESCRIPTIO, true);
                exists = view4.Exists;
                view4.Fields.FieldByName("REFERENCE").SetValue(po0620.REFERENCE, true);
                exists = view4.Exists;
                view4.Fields.FieldByName("COMMENT").SetValue(po0620.COMMENT, true);
                exists = view4.Exists;
                exists = view4.Exists;
                exists = view4.Exists;
                view5.RecordClear();

                int i = 0;
                foreach (var po0630 in po0620.PO0630List)
                {
                    lineItemNo = po0630.ITEMNO;
                    exists = view5.Exists;
                    view5.RecordCreate(ViewRecordCreate.NoInsert);
                    if (po0620.HASJOB == 1)
                    {
                        view5.Fields.FieldByName("CONTRACT").SetValue(po0630.CONTRACT, true);
                        view5.Fields.FieldByName("PROJECT").SetValue(po0630.PROJECT, true);
                        view5.Fields.FieldByName("CCATEGORY").SetValue(po0630.CCATEGORY, true);
                    }

                    view5.Fields.FieldByName("ITEMNO").SetValue(po0630.ITEMNO, true);
                    view5.Fields.FieldByName("PROCESSCMD").SetValue("1", true);
                    view5.Process();
                    view5.Fields.FieldByName("LOCATION").SetValue(po0630.LOCATION, true);
                    view5.Fields.FieldByName("HASDROPSHI").SetValue(po0630.HASDROPSHI, true);
                    view5.Fields.FieldByName("DROPTYPE").SetValue(po0630.DROPTYPE, true);

                    switch (po0630.DROPTYPE)
                    {
                        case "3" :
                            view5.Fields.FieldByName("DLOCATION").SetValue(po0630.DLOCATION, true);
                            break;

                        case "4":
                        case "5":
                            view5.Fields.FieldByName("IDCUST").SetValue(po0630.IDCUST, true);
                            if (po0630.DROPTYPE == "5")
                            {
                                view5.Fields.FieldByName("IDCUSTSHPT").SetValue(po0630.IDCUSTSHPT, true);
                            }
                            break;
                    }


                    view5.Fields.FieldByName("DESC").SetValue(po0630.DESC, true);
                    view5.Fields.FieldByName("ADDRESS1").SetValue(po0630.ADDRESS1, true);
                    view5.Fields.FieldByName("ADDRESS2").SetValue(po0630.ADDRESS2, true);
                    view5.Fields.FieldByName("ADDRESS3").SetValue(po0630.ADDRESS3, true);
                    view5.Fields.FieldByName("ADDRESS4").SetValue(po0630.ADDRESS4, true);
                    view5.Fields.FieldByName("CITY").SetValue(po0630.CITY, true);
                    view5.Fields.FieldByName("STATE").SetValue(po0630.STATE, true);
                    view5.Fields.FieldByName("ZIP").SetValue(po0630.ZIP, true);
                    view5.Fields.FieldByName("COUNTRY").SetValue(po0630.COUNTRY, true);
                    view5.Fields.FieldByName("PHONE").SetValue(po0630.PHONE, true);
                    view5.Fields.FieldByName("FAX").SetValue(po0630.FAX, true);
                    view5.Fields.FieldByName("EMAIL").SetValue(po0630.EMAIL, true);
                    view5.Fields.FieldByName("CONTACT").SetValue(po0630.CONTACT, true);
                    view5.Fields.FieldByName("PHONEC").SetValue(po0630.PHONEC, true);
                    view5.Fields.FieldByName("FAXC").SetValue(po0630.FAXC, true);
                    view5.Fields.FieldByName("EMAILC").SetValue(po0630.EMAILC, true);
                    view5.Fields.FieldByName("ISCOMPLETE").SetValue(po0630.ISCOMPLETE, true);
                    view5.Fields.FieldByName("OQORDERED").SetValue(po0630.OQORDERED, true);
                    view5.Fields.FieldByName("UNITCOST").SetValue(po0630.UNITCOST, true);
                    view5.Fields.FieldByName("EXTENDED").SetValue(po0630.EXTENDED, true);
                    view5.Fields.FieldByName("DISCPCT").SetValue(po0630.DISCPCT, true);
                    view5.Fields.FieldByName("DISCOUNT").SetValue(po0630.DISCOUNT, true);
                    view5.Fields.FieldByName("WEIGHTUNIT").SetValue(po0630.WEIGHTUNIT, true);
                    view5.Fields.FieldByName("UNITWEIGHT").SetValue(po0630.UNITWEIGHT, true);
                    view5.Fields.FieldByName("EXTWEIGHT").SetValue(po0630.EXTWEIGHT, true);
                    view5.Fields.FieldByName("EXPARRIVAL").SetValue(po0630.EXPARRIVAL, true);
                    view5.Fields.FieldByName("OEONUMBER").SetValue(po0630.OEONUMBER, true);
                    view5.Fields.FieldByName("HASCOMMENT").SetValue(po0630.HASCOMMENT, true);
                    view6.Fields.FieldByName("COMMENT").SetValue(po0630.COMMENT, true);
                    view6.Insert();
                    view6.Read(false);
                    view5.Fields.FieldByName("MANITEMNO").SetValue(po0630.MANITEMNO, true);
                    view5.Fields.FieldByName("OQOUTSTAND").SetValue(po0630.OQOUTSTAND, true);

                    if (po0620.HASJOB == 1)
                    {
                        view5.Fields.FieldByName("BILLRATE").SetValue(po0630.BILLRATE, true);
                        view5.Fields.FieldByName("ARITEMNO").SetValue(po0630.ARITEMNO, true);
                        view5.Fields.FieldByName("ARUNIT").SetValue(po0630.ARUNIT, true);
                    }

                    foreach (var ic0635 in po0630.IC0635List)
                    {
                        view10.Fields.FieldByName("OPTFIELD").SetValue(ic0635.OPTFIELD, false);
                        view10.Read(false);
                        view10.Fields.FieldByName("SWSET").SetValue(ic0635.SWSET, true);
                        exists = view10.Exists;
                        view10.Fields.FieldByName("VALIFTEXT").SetValue(ic0635.VALIFTEXT, true);
                        view10.Update();
                    }

                    view10.Read(false);
                    exists = view5.Exists;
                    view5.Insert();
                    view5.Process();
                    view5.Fields.FieldByName("PORLREV").SetValue("-" + i, false);
                    view5.Read(false);
                    i++;
                }

                exists = view4.Exists;
                exists = view4.Exists;
                exists = view4.Exists;
                view9.Fields.FieldByName("OPTFIELD").SetValue(po0620.OPTFIELD, false);
                view9.Read(false);
                view9.Fields.FieldByName("SWSET").SetValue(po0620.SWSET, true);
                exists = view9.Exists;
                view9.Fields.FieldByName("VALIFTEXT").SetValue(po0620.VALIFTEXT, true);
                view9.Update();
                view9.Read(false);
                view4.Fields.FieldByName("LABELCOUNT").SetValue(po0620.LABELCOUNT, true);
                exists = view4.Exists;
                view8.Fields.FieldByName("FUNCTION").SetValue("8", false);
                view8.Process();
                exists = view4.Exists;
                view4.Insert();
                string newValue = Convert.ToString(view4.Fields.FieldByName("PORHSEQ").Value);
                view.Fields.FieldByName("FUNCTION").SetValue("20", false);
                view.Fields.FieldByName("WORKFLOW").SetValue(po0620.WORKFLOW, false);
                view.Fields.FieldByName("TYPE").SetValue("20", false);
                view.Fields.FieldByName("KEY").SetValue(newValue, false);
                view.Process();
                exists = view4.Exists;
                view4.Init();
                exists = view4.Exists;
                view4.Order = 0;
                view4.Fields.FieldByName("PORHSEQ").SetValue("0", false);
                view4.Init();
                view4.Order = 1;
                exists = view4.Exists;
                exists = view5.Exists;
                view5.RecordClear();
                view7.Init();
                view6.Init();
                view4.Read(false);
                session.Dispose();
                responseModel.SetSuccess();
            }
            catch (Exception exception)
            {
                StringBuilder sbError = new StringBuilder();
                if (session != null && session.Errors != null)
                {

                    int TotalError = session.Errors.Count;
                    for (int i = 0; i < TotalError; i++)
                    {
                        if (session.Errors[i] != null && !string.IsNullOrEmpty(session.Errors[i].Message))
                        {
                            if (!string.IsNullOrEmpty(lineItemNo))
                            {
                                lineItemNo = lineItemNo + "-";
                            }

                            sbError.AppendLine(lineItemNo + session.Errors[i].Message);
                        }
                    }
                }

                sbError.Append(exception.Message);
                responseModel.Message = sbError.ToString();
            }


            if (session != null)
            {
                session.Dispose();
            }

            var response = Request.CreateResponse(HttpStatusCode.OK, responseModel);
            return response;
        }
    }
}
