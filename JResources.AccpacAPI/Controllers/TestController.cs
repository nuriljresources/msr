﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Transactions;
using System.Data;
using System.Text;
using JResources.Data.Model.Accpac;
using JResources.Data.Model;
using JResources.BusinessLogic;
using JResources.Data;

namespace JResources.AccpacAPI.Controllers
{
    public class InfoController : ApiController
    {
        //
        // GET: /Test/
        [HttpPost]
        public HttpResponseMessage InfoCompany(AccpacPostBaseModel param)
        {
            AccpacInfoModel model = new AccpacInfoModel();
            ACCPAC.Advantage.Session session = new ACCPAC.Advantage.Session();
            session.Init("", "XY", "XY1000", "61A");
            session.Open(param.DBUsername, param.DBPassword, param.DBName, DateTime.Today, 0);
            ACCPAC.Advantage.DBLink mDBLinkCmpRW = session.OpenDBLink(ACCPAC.Advantage.DBLinkType.Company, ACCPAC.Advantage.DBLinkFlags.ReadWrite);


            ACCPAC.Advantage.Company compInfo = mDBLinkCmpRW.Company;
            model.CompanyName = compInfo.Name;
            model.HomeCurrency = compInfo.HomeCurrency;
            model.LegalName = compInfo.LegalName;
            model.ACCPACVersion = "Sage 300 Version: " + session.ACCPACVersion + " build version: " + session.ACCPACVersionBuild + " app: " + session.AppID + "app ver: " + session.AppVersion;
            model.UserID = session.UserID;
            model.UserLanguage = session.UserLanguage;

            var response = Request.CreateResponse(HttpStatusCode.OK, model);
            return response;
        }

        [HttpGet]
        public HttpResponseMessage ItemList()
        {
            Company company = Company.GetByCode("BAKAN");
            DataRepositoryStoreAccpac.CurrentDataStore[DataRepositoryStoreAccpac.KEY_DATACONTEXT] = new AccpacEntities(JResources.Common.ConnectionManager.GetConnectionStringByCompanyCode(Common.DatabaseGroupName.ACCPAC_DATABASE, company.CompanyCode));

            PartItemSearchModel model = new PartItemSearchModel();
            var result = ItemLogic.GetItemList(model);

            var response = Request.CreateResponse(HttpStatusCode.OK, result);
            return response;
        }
    }
}
