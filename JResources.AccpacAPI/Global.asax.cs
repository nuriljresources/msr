﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using JResources.Data;

namespace JResources.AccpacAPI
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class WebApiApplication : System.Web.HttpApplication
    {
        public class HttpDataContextDataStore : IDataRepositoryStore
        {
            public object this[string key]
            {
                get { return HttpContext.Current.Items[key]; }
                set { HttpContext.Current.Items[key] = value; }
            }
        }

        public class HttpDataContextDataStoreAccpac : IDataRepositoryStoreAccpac
        {
            public object this[string key]
            {
                get { return HttpContext.Current.Items[key]; }
                set { HttpContext.Current.Items[key] = value; }
            }
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            DataRepositoryStore.CurrentDataStore = new HttpDataContextDataStore();
            DataRepositoryStoreAccpac.CurrentDataStore = new HttpDataContextDataStoreAccpac();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //MvcHandler.DisableMvcResponseHeader = true;
        }

        public void Application_BeginRequest()
        {
            string url = Request.Url.AbsoluteUri;
        }

    }
}