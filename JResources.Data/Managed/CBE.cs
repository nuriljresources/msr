﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class CBE
    {
        public static IQueryable<CBE> GetAll()
        {
            return CurrentDataContext.CurrentContext.CBEs.OrderByDescending(x => x.CreatedDate);
        }

        public static CBE GetById(Guid Id)
        {
            return CurrentDataContext.CurrentContext.CBEs.FirstOrDefault(x => x.ID == Id);
        }

        public static CBE GetByNo(string CBENo)
        {
            return CurrentDataContext.CurrentContext.CBEs.FirstOrDefault(x => x.CBENo == CBENo);
        }

        /*
        public static IQueryable<CBE> GetByWorkOrderNo(string WorkOrderNo)
        {
            return CurrentDataContext.CurrentContext.CBEs.Where(x => x.wo == WorkOrderNo);
        }
        */
    }
    public partial class CBEBid
    {
        public static IQueryable<CBEBid> GetAll()
        {
            return CurrentDataContext.CurrentContext.CBEBids.OrderByDescending(x => x.CreatedDate);
        }

        public static CBEBid GetById(Guid Id)
        {
            return CurrentDataContext.CurrentContext.CBEBids.FirstOrDefault(x => x.ID == Id && Convert.ToBoolean(!x.IsDeleted));
        }

        public static CBEBid GetByCBEId(Guid Id)
        {
            return CurrentDataContext.CurrentContext.CBEBids.FirstOrDefault(x => x.CBEid == Id && Convert.ToBoolean(!x.IsDeleted));
        }
    }
    public partial class CBEBidSupplierWinner
    {
        public static IQueryable<CBEBidSupplierWinner> GetAll()
        {
            return CurrentDataContext.CurrentContext.CBEBidSupplierWinners.OrderByDescending(x => x.CBEId);
        }

        public static CBEBidSupplierWinner GetById(Guid Id)
        {
            return CurrentDataContext.CurrentContext.CBEBidSupplierWinners.FirstOrDefault(x => x.ID == Id);
        }

        public static CBEBidSupplierWinner GetByCBEId(Guid Id)
        {
            return CurrentDataContext.CurrentContext.CBEBidSupplierWinners.FirstOrDefault(x => x.CBEId == Id);
        }
        public static IQueryable<CBEBidSupplierWinner> GetByIsWinner(Guid Id)
        {
            return CurrentDataContext.CurrentContext.CBEBidSupplierWinners.Where(x => x.CBEId == Id && x.IsWinner == true);
        }
    }

}

