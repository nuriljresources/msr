﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class PTPRD
    {
        public static IQueryable<PTPRD> GetAll()
        {
            return AccpacDataContext.CurrentContext.PTPRDs;
        }

        public static IQueryable<PTPRD> GetByRQNHSEQ(Decimal RQNHSEQ)
        {
            return AccpacDataContext.CurrentContext.PTPRDs.Where(x => x.RQNHSEQ == RQNHSEQ);
        }
    }
}
