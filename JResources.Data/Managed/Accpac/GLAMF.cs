﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class GLAMF
    {
        public static IQueryable<GLAMF> GetAll()
        {
            return AccpacDataContext.CurrentContext.GLAMFs.Where(x => x.ACTIVESW == 1).OrderByDescending(x => x.ACCTDESC);
        }

        public static GLAMF GetByCode(string code)
        {
            return AccpacDataContext.CurrentContext.GLAMFs.Where(x => x.ACTIVESW == 1).FirstOrDefault(x => x.ACCTDESC == code);
        }
    }
}
