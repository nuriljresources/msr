﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class POPORL
    {
        public static IQueryable<POPORL> GetAll()
        {
            return AccpacDataContext.CurrentContext.POPORLs.OrderByDescending(x => x.AUDTDATE);
        }

        public static IQueryable<POPORL> GetByPONumber(decimal PORHSEQ)
        {
            return AccpacDataContext.CurrentContext.POPORLs.Where(x => x.PORHSEQ == PORHSEQ);
        }
    }
}
