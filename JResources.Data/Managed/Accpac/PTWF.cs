﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class PTWF
    {
        public static IQueryable<PTWF> GetAll()
        {
            return AccpacDataContext.CurrentContext.PTWFs.Where(x => x.INACTIVE == 0);
        }
    }
}
