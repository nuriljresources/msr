﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class ICCATG
    {
        public static IQueryable<ICCATG> GetAll()
        {
            return AccpacDataContext.CurrentContext.ICCATGs.OrderByDescending(x => x.AUDTDATE);
        }

        public static ICCATG GetByCategoryNo(string CategoryNo)
        {
            return AccpacDataContext.CurrentContext.ICCATGs.FirstOrDefault(x => x.CATEGORY == CategoryNo);
        }
    }
}
