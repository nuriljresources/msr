﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class ICTRANH
    {
        public static IQueryable<ICTRANH> GetAll()
        {
            return AccpacDataContext.CurrentContext.ICTRANHs.OrderByDescending(x => x.AUDTDATE);
        }

        public static ICTRANH GetByTransNo(string DOCNUM)
        {
            return AccpacDataContext.CurrentContext.ICTRANHs.FirstOrDefault(x => x.DOCNUM == DOCNUM);
        }


    }
}
