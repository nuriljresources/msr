﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class PTCOST
    {
        public static IQueryable<PTCOST> GetAll()
        {
            return AccpacDataContext.CurrentContext.PTCOSTs.Where(x => x.INACTIVE == 0);
        }
    }
}
