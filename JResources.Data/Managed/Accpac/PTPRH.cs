﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class PTPRH
    {
        public static IQueryable<PTPRH> GetAll()
        {
            return AccpacDataContext.CurrentContext.PTPRHs;
        }

        public static PTPRH GetByRQNNUMBER(string RQNNUMBER)
        {
            return AccpacDataContext.CurrentContext.PTPRHs.FirstOrDefault(x => x.RQNNUMBER == RQNNUMBER);
        }
    }
}
