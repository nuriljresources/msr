﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class ICITEM
    {
        public static IQueryable<ICITEM> GetAll()
        {
            return AccpacDataContext.CurrentContext.ICITEMs.OrderByDescending(x => x.AUDTDATE);
        }

        public static ICITEM GetByItemNo(string ITEMNO)
        {
            return AccpacDataContext.CurrentContext.ICITEMs.FirstOrDefault(x => x.ITEMNO == ITEMNO);
        }

        public static List<ItemOutstandingPost_Result> GetItemOutStandingMSR(List<string> ItemListNo)
        {
            string listItemNo = string.Join(",", ItemListNo.ToArray());
            List<ItemOutstandingPost_Result> list = CurrentDataContext.CurrentContext.ItemOutstandingPost(listItemNo).ToList();
            return list;
        }
    }
}
