﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class ICILOC
    {
        public static IQueryable<ICILOC> GetAll()
        {
            return AccpacDataContext.CurrentContext.ICILOCs.OrderByDescending(x => x.AUDTDATE);
        }

        public static ICILOC GetByItemNo(string ItemNo)
        {
            return AccpacDataContext.CurrentContext.ICILOCs.FirstOrDefault(x => x.ITEMNO == ItemNo);
        }

        public static ICILOC GetByItemNoAndLocation(string ItemNo, string Location)
        {
            return AccpacDataContext.CurrentContext.ICILOCs.FirstOrDefault(x => x.ITEMNO == ItemNo && x.LOCATION.Contains(Location));
        }
    }
}
