﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class CSOPTFD
    {
        public static IQueryable<CSOPTFD> GetAll()
        {
            return AccpacDataContext.CurrentContext.CSOPTFDs;
        }


        #region COST CENTER
        public static IQueryable<CSOPTFD> GetCostCenter()
        {
            return AccpacDataContext.CurrentContext.CSOPTFDs.Where(x => x.OPTFIELD == "CCCODE"
                && !x.VDESC.ToLower().Contains("do not use")
                && !x.VDESC.ToLower().Contains("dont use")
                && !x.VDESC.ToLower().Contains("don't use")
                );
        }

        public static CSOPTFD GetCostCenterByValue(string value)
        {
            return AccpacDataContext.CurrentContext.CSOPTFDs.FirstOrDefault(x => x.OPTFIELD == "CCCODE" && x.VALUE == value);
        }

        public static IQueryable<CSOPTFD> GetCostCenterByValueList(List<string> valueList)
        {
            return AccpacDataContext.CurrentContext.CSOPTFDs.Where(x => x.OPTFIELD == "CCCODE" && valueList.Contains(x.VALUE));
        }
        #endregion

        #region EQUIPMENT
        public static IQueryable<CSOPTFD> GetEquipment()
        {
            return AccpacDataContext.CurrentContext.CSOPTFDs.Where(x => x.OPTFIELD == "ECCODE"
                && !x.VDESC.ToLower().Contains("do not use")
                && !x.VDESC.ToLower().Contains("dont use")
                && !x.VDESC.ToLower().Contains("don't use")
                );
        }

        public static CSOPTFD GetEquipmentByValue(string value)
        {
            return AccpacDataContext.CurrentContext.CSOPTFDs.FirstOrDefault(x => x.OPTFIELD == "ECCODE" && x.VALUE == value
                && !x.VDESC.ToLower().Contains("do not use")
                && !x.VDESC.ToLower().Contains("dont use")
                && !x.VDESC.ToLower().Contains("don't use")
                );
        }

        public static IQueryable<CSOPTFD> GetEquipmentByValueList(List<string> valueList)
        {
            return AccpacDataContext.CurrentContext.CSOPTFDs.Where(x => x.OPTFIELD == "ECCODE" && valueList.Contains(x.VALUE)
                && !x.VDESC.ToLower().Contains("do not use")
                && !x.VDESC.ToLower().Contains("dont use")
                && !x.VDESC.ToLower().Contains("don't use")
                );
        }
        #endregion

        #region MAINTENANCE CODE
        public static IQueryable<CSOPTFD> GetMaintenanceCode()
        {
            return AccpacDataContext.CurrentContext.CSOPTFDs.Where(x => x.OPTFIELD == "MTCODE");
        }

        public static CSOPTFD GetMaintenanceCodeByValue(string value)
        {
            return AccpacDataContext.CurrentContext.CSOPTFDs.FirstOrDefault(x => x.OPTFIELD == "MTCODE" && x.VALUE == value);
        }

        public static IQueryable<CSOPTFD> GetMaintenanceCodeByValueList(List<string> valueList)
        {
            return AccpacDataContext.CurrentContext.CSOPTFDs.Where(x => x.OPTFIELD == "MTCODE" && valueList.Contains(x.VALUE));
        }
        #endregion


    }

}
