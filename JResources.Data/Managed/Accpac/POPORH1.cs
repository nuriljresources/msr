﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class POPORH1
    {
        public static IQueryable<POPORH1> GetAll()
        {
            return AccpacDataContext.CurrentContext.POPORH1.OrderByDescending(x => x.AUDTDATE);
        }

        public static POPORH1 GetByPONumber(string PONumber)
        {
            return AccpacDataContext.CurrentContext.POPORH1.FirstOrDefault(x => x.PONUMBER == PONumber);
        }
    }
}
