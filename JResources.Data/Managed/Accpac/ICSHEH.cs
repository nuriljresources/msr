﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class ICSHEH
    {
        public static IQueryable<ICSHEH> GetAll()
        {
            return AccpacDataContext.CurrentContext.ICSHEHs.OrderByDescending(x => x.AUDTDATE);
        }

        public static ICSHEH GetByTransNo(string DOCNUM)
        {
            return AccpacDataContext.CurrentContext.ICSHEHs.FirstOrDefault(x => x.DOCNUM == DOCNUM);
        }
    }
}
