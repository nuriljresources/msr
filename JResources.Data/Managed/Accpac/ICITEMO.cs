﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class ICITEMO
    {
        public static ICITEMO GetByItemNo(string ITEMNO, string OPTFIELD)
        {
            return AccpacDataContext.CurrentContext.ICITEMOes.FirstOrDefault(x => x.ITEMNO == ITEMNO && x.OPTFIELD == OPTFIELD);
        }
        public static IQueryable<ICITEMO> GetITEMCIFD()
        {
            return AccpacDataContext.CurrentContext.ICITEMOes.Where(x => x.OPTFIELD == "ITMCLASS" && x.VALUE != "00");
        }
    }
}
