﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class UserLogin
    {
        public static IQueryable<UserLogin> GetAll()
        {
            return CurrentDataContext.CurrentContext.UserLogins.OrderBy(x => x.CreatedDate);
        }

        public static UserLogin GetById(Guid Id)
        {
            return CurrentDataContext.CurrentContext.UserLogins.FirstOrDefault(x => x.ID == Id);
        }

        public static UserLogin GetByNIKSite(String NIKSite)
        {
            return CurrentDataContext.CurrentContext.UserLogins.FirstOrDefault(x => x.NIKSite == NIKSite);
        }

        public static UserLogin GetByUsername(string username)
        {
            return CurrentDataContext.CurrentContext.UserLogins.FirstOrDefault(x => x.Username == username);
        }

        public static UserLogin GetByLogin(string username, string password)
        {
            return CurrentDataContext.CurrentContext.UserLogins.FirstOrDefault(x => x.Username == username && x.Password == password);
        }
    }
}
