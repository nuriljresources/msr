﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class CEA
    {

        public static IQueryable<CEA> GetAll()
        {
            return CurrentDataContext.CurrentContext.CEAs.OrderBy(x => x.CreatedDate);
        }

        public static CEA GetById(Guid Id)
        {
            return CurrentDataContext.CurrentContext.CEAs.FirstOrDefault(x => x.ID == Id);
        }

        public static List<CEA> GetByListId(List<Guid> Ids)
        {
            return CurrentDataContext.CurrentContext.CEAs.Where(x => Ids.Contains(x.ID)).ToList();
        }

        public static IQueryable<CEA> GetOutstanding(int month)
        {
            DateTime DateStart = DateTime.Now.Date.AddMonths(-month).AddDays(-1).AddMinutes(1);
            DateTime DateEnd = DateTime.Now.Date.AddMonths(-month).AddDays(1).AddMinutes(-1);

            return CurrentDataContext.CurrentContext.CEAs.OrderByDescending(x => x.CEADate >= DateStart && x.CEADate <= DateEnd);
        }

    }
}
