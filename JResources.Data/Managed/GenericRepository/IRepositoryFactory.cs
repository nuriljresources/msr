﻿
using JResources.Data.GenericRepository;
namespace JResources.Data
{
    public interface IRepositoryFactory
    {
        IRepository Repository();
    }
}
