﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class V_CEA_ITEMACCPAC
    {
        public static IQueryable<V_CEA_ITEMACCPAC> GetAll()
        {
            return CurrentDataContext.CurrentContext.V_CEA_ITEMACCPAC.OrderByDescending(x => x.cer_number);
        }

        public static IQueryable<V_CEA_ITEMACCPAC> GetByCEAId(string id)
        {
            return CurrentDataContext.CurrentContext.V_CEA_ITEMACCPAC.Where(x => x.ce_header_id == id);
        }

        public static V_CEA_ITEMACCPAC GetById(string Id)
        {
            return CurrentDataContext.CurrentContext.V_CEA_ITEMACCPAC.FirstOrDefault(x => x.sys_id == Id);
        }
    }
}
