﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class DocumentNumber
    {
        public static IQueryable<DocumentNumber> GetAll()
        {
            return CurrentDataContext.CurrentContext.DocumentNumbers.OrderByDescending(x => x.CreatedDate);
        }

        public static DocumentNumber GetDocumentNoByTypeYear(string DocumentType, Guid CompanyId)
        {
            int Year = DateTime.Now.Year;
            return CurrentDataContext.CurrentContext.DocumentNumbers
                .Where(x => x.DocumentType == DocumentType && x.CompanyId == CompanyId && x.Year == Year)
                .OrderByDescending(x => x.RunningNumber)
                .FirstOrDefault();
        }
    }
}
