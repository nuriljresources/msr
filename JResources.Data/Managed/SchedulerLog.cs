﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class SchedulerLog
    {
        public static IQueryable<SchedulerLog> GetAll()
        {
            return CurrentDataContext.CurrentContext.SchedulerLogs.OrderByDescending(x => x.CreatedDate);
        }

        public static List<SchedulerLog> GetBySchedulerId(int _schedulerId)
        {
            return CurrentDataContext.CurrentContext.SchedulerLogs
                .Where(x => x.SchedulerTaskId == _schedulerId)
                .OrderByDescending(x => x.CreatedDate)
                .Take(10)
                .ToList();
        }


    }
}
