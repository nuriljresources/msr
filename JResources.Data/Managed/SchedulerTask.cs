﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JResources.Common.Enum;

namespace JResources.Data
{
    public partial class SchedulerTask
    {
        public static IQueryable<SchedulerTask> GetAll()
        {
            return CurrentDataContext.CurrentContext.SchedulerTasks.OrderByDescending(x => x.ID);
        }

        public static IQueryable<SchedulerTask> GetAllToProcess()
        {
            return CurrentDataContext.CurrentContext.SchedulerTasks
                .Where(x => x.SchedulerStatus == SCHEDULER_STATUS.NEW ||
                    x.SchedulerStatus == SCHEDULER_STATUS.FAILED
                    //x.SchedulerStatus == SCHEDULER_STATUS.PROCESS
                    )
                .OrderByDescending(x => x.ID);
        }

        public static SchedulerTask GetById(int _id)
        {
            return CurrentDataContext.CurrentContext.SchedulerTasks.FirstOrDefault(x => x.ID == _id);
        }

    }
}
