﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class Department
    {
        public static IQueryable<Department> GetAll()
        {
            return CurrentDataContext.CurrentContext.Departments.OrderByDescending(x => x.CreatedDate);
        }

        public static IQueryable<Department> GetAllActive()
        {
            return CurrentDataContext.CurrentContext.Departments.Where(x => !x.IsDeleted).OrderByDescending(x => x.CreatedDate);
        }

        public static Department GetById(Guid Id)
        {
            return CurrentDataContext.CurrentContext.Departments.FirstOrDefault(x => x.ID == Id);
        }

        public static Department GetByMaintenanceCode(string MaintenanceCode)
        {
            return CurrentDataContext.CurrentContext.Departments.FirstOrDefault(x => x.MaintenanceTypeCode == MaintenanceCode);
        }

        public static List<Department> GetByCurrentCompany()
        {
            var list = new List<Department>();
            var CompanyId = Model.CurrentUser.COMPANY;
            var company = Company.GetByCode(CompanyId);
            if (company != null && company.ID != Guid.Empty)
            {
                list = CurrentDataContext.CurrentContext.Departments.Where(x => !x.IsDeleted && x.CompanyId == company.ID).ToList();
            }

            return list;
        }

        public static IQueryable<Department> GetByCompanyId(Guid CompanyId)
        {
            return CurrentDataContext.CurrentContext.Departments.Where(x => !x.IsDeleted && x.CompanyId == CompanyId);
        }

        public static void DeleteById(Guid Id)
        {
            var department = CurrentDataContext.CurrentContext.Departments.FirstOrDefault(x => x.ID == Id);
            department.IsDeleted = true;
            department.Save<Department>();
        }
    }
}
