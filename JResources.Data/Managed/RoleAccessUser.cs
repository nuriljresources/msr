﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class RoleAccessUser
    {
        public static IQueryable<RoleAccessUser> GetAll()
        {
            return CurrentDataContext.CurrentContext.RoleAccessUsers.OrderBy(x => x.CreatedDate);
        }

        public static RoleAccessUser GetById(Guid _id)
        {
            return CurrentDataContext.CurrentContext.RoleAccessUsers.FirstOrDefault(x => x.ID == _id);
        }

        public static IQueryable<RoleAccessUser> GetByUserId(Guid UserId)
        {
            DateTime dtNow = DateTime.Now;
            return CurrentDataContext.CurrentContext.RoleAccessUsers.Where(x => x.UserId == UserId && (x.DateFrom <= dtNow && x.DateTo >= dtNow));
        }

        public static IQueryable<RoleAccessUser> GetByRoleAccessId(Guid RoleAccessId)
        {
            return CurrentDataContext.CurrentContext.RoleAccessUsers.Where(x => x.RoleAccessId == RoleAccessId && (x.DateFrom <= DateTime.Now && x.DateTo >= DateTime.Now));
        }

        public static List<roleaccess_department_Result> GetByDepartmentList(string Username, string[] RoleAccessCode)
        {
            string _roleCodesString = string.Empty;
            if (RoleAccessCode != null && RoleAccessCode.Length > 0)
            {
                _roleCodesString = string.Join(",", RoleAccessCode);
            }

            return CurrentDataContext.CurrentContext.roleaccess_department(Username, _roleCodesString).ToList();
        }
    }
}
