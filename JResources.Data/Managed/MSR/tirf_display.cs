﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class tirf_display
    {
        public static IQueryable<tirf_display> GetAll()
        {
            return AccpacDataContext.CurrentContext.tirf_display.OrderByDescending(x => x.irf_no);
        }

        public static IQueryable<tirf_display> GetByIRFNo(string IRFNo)
        {
            return AccpacDataContext.CurrentContext.tirf_display.Where(x => x.irf_no == IRFNo);
        }

        public static tuser_roles GetUserByIRFNo(string IRFNo)
        {
            var irf_display = GetByIRFNo(IRFNo).FirstOrDefault();

            if (irf_display != null)
            {
                tuser_roles user = tuser_roles.GetByUserId(irf_display.emp_id);
                return user;
            }

            return null;
        }

        
    }
}
