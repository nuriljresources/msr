﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class tfield_value
    {
        public static IQueryable<tfield_value> GetAll()
        {
            return MSRDataContext.CurrentContext.tfield_value.OrderByDescending(x => x.fld_valu);
        }

        public static List<tfield_value> GetByFieldName(string FieldName)
        {
            return MSRDataContext.CurrentContext.tfield_value.Where(x => x.fld_nm == FieldName).ToList();
        }
    }
}
