﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class tirf_master
    {
        public static IQueryable<tirf_master> GetAll()
        {
            return AccpacDataContext.CurrentContext.tirf_master.OrderByDescending(x => x.irf_dt.Value);
        }

        public static tirf_master GetByNo(string IRFNo)
        {
            return AccpacDataContext.CurrentContext.tirf_master.FirstOrDefault(x => x.irf_no == IRFNo);
        }
    }
}
