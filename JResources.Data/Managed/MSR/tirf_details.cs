﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class tirf_details
    {
        public static IQueryable<tirf_details> GetAll()
        {
            return AccpacDataContext.CurrentContext.tirf_details.OrderByDescending(x => x.irf_no);
        }

        public static List<tirf_details> GetByNo(string IRFNo)
        {
            return AccpacDataContext.CurrentContext.tirf_details.Where(x => x.irf_no == IRFNo).ToList();
        }
    }
}
