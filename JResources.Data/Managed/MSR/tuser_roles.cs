﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class tuser_roles
    {
        public static IQueryable<tuser_roles> GetAll()
        {
            return AccpacDataContext.CurrentContext.tuser_roles.OrderByDescending(x => x.usrid);
        }

        public static tuser_roles GetByUserId(string UserId)
        {
            return AccpacDataContext.CurrentContext.tuser_roles.FirstOrDefault(x => x.usrid == UserId);
        }
    }
}
