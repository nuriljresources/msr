﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class ttransfer_maintenance
    {
        public static IQueryable<ttransfer_maintenance> GetAll()
        {
            return AccpacDataContext.CurrentContext.ttransfer_maintenance.OrderByDescending(x => x.irf_no);
        }

        public static ttransfer_maintenance GetByIRFNo(string IRFNo)
        {
            return AccpacDataContext.CurrentContext.ttransfer_maintenance.FirstOrDefault(x => x.irf_no == IRFNo);
        }
    }
}
