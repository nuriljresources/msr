﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class CSOPTFD
    {
        public static IQueryable<CSOPTFD> GetAll()
        {
            return MSRDataContext.CurrentContext.CSOPTFDs.OrderByDescending(x => x.OPTFIELD);
        }

        public static List<CSOPTFD> GetByFieldName(string FieldName)
        {
            return MSRDataContext.CurrentContext.CSOPTFDs.Where(x => x.OPTFIELD == FieldName).ToList();
        }
    }
}
