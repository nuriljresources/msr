﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class tirf_history
    {
        public static IQueryable<tirf_history> GetAll()
        {
            return AccpacDataContext.CurrentContext.tirf_history.OrderByDescending(x => x.action_dt.Value);
        }

        public static IQueryable<tirf_history> GetByIRFNo(string IRFNo)
        {
            return AccpacDataContext.CurrentContext.tirf_history.Where(x => x.irf_no == IRFNo);
        }
    }
}
