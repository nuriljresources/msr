﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class tirf_approval
    {
        public static IQueryable<tirf_approval> GetAll()
        {
            return MSRDataContext.CurrentContext.tirf_approval.OrderByDescending(x => x.irf_no);
        }

        public static IQueryable<tirf_approval> GetByIRFNo(string IRFNo)
        {
            return MSRDataContext.CurrentContext.tirf_approval.Where(x => x.irf_no == IRFNo);
        }
    }
}
