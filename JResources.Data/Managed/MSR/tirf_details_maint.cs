﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class tirf_details_maint
    {
        public static IQueryable<tirf_details_maint> GetAll()
        {
            return AccpacDataContext.CurrentContext.tirf_details_maint.OrderByDescending(x => x.irf_no);
        }

        public static List<tirf_details_maint> GetByNo(string IRFNo)
        {
            return AccpacDataContext.CurrentContext.tirf_details_maint.Where(x => x.irf_no == IRFNo).ToList();
        }
    }
}
