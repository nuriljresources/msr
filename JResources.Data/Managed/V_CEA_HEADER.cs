﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;

namespace JResources.Data
{
    public partial class V_CEA_HEADER
    {
        public static IQueryable<V_CEA_HEADER> GetAll()
        {
            return CurrentDataContext.CurrentContext.V_CEA_HEADER.OrderByDescending(x => x.cea_date);
        }

        public static V_CEA_HEADER GetById(string Id)
        {
            return CurrentDataContext.CurrentContext.V_CEA_HEADER.FirstOrDefault(x => x.sys_id == Id);
        }

        public static V_CEA_HEADER GetByCode(string CEANo)
        {
            return CurrentDataContext.CurrentContext.V_CEA_HEADER.FirstOrDefault(x => x.cea_number == CEANo);
        }
        public static IQueryable<V_CEA_HEADER> GetOutstanding(int month)
        {
            DateTime DateStart = DateTime.Now.Date.AddMonths(-month).AddDays(-1).AddMinutes(1);
            DateTime DateEnd = DateTime.Now.Date.AddMonths(-month).AddDays(1).AddMinutes(-1);

            return CurrentDataContext.CurrentContext.V_CEA_HEADER.OrderByDescending(x => x.cea_date.HasValue && x.cea_date.Value >= DateStart && x.cea_date.Value <= DateEnd);
        }
    }
}
