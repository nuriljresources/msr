﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class tfrf_master
    {
        public static IQueryable<tfrf_master> GetAll()
        {
            return CurrentDataContext.CurrentContext.tfrf_master.OrderByDescending(x => x.frf_dt);
        }

        public static tfrf_master GetById(Guid Id)
        {
            return CurrentDataContext.CurrentContext.tfrf_master.FirstOrDefault(x => x.ID == Id);
        }

        public static tfrf_master GetByFRFNo(string FRFNo)
        {
            return CurrentDataContext.CurrentContext.tfrf_master.FirstOrDefault(x => x.frf_no == FRFNo);
        }
    }

    public partial class tfrf_detail
    {
        public static IQueryable<tfrf_detail> GetAll()
        {
            return CurrentDataContext.CurrentContext.tfrf_detail.OrderByDescending(x => x.log_sheet_dt);
        }

        public static IQueryable<tfrf_detail> GetByFRFNo(string FRFNo)
        {
            return CurrentDataContext.CurrentContext.tfrf_detail.Where(x => x.frf_no == FRFNo);
        }
    }
}
