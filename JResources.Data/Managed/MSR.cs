﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class MSR
    {
        public static IQueryable<MSR> GetAll()
        {
            return CurrentDataContext.CurrentContext.MSRs.OrderByDescending(x => x.CreatedDate);
        }

        public static MSR GetById(Guid Id)
        {
            return CurrentDataContext.CurrentContext.MSRs.FirstOrDefault(x => x.ID == Id);
        }

        public static MSR GetByNo(string MSRNo)
        {
            return CurrentDataContext.CurrentContext.MSRs.FirstOrDefault(x => x.MSRNo == MSRNo);
        }

        public static IQueryable<MSR> GetByWorkOrderNo(string WorkOrderNo)
        {
            return CurrentDataContext.CurrentContext.MSRs.Where(x => x.WorkOrderNo == WorkOrderNo);
        }
    }

    public partial class MSRDetail
    {
        public static IQueryable<MSRDetail> GetAll()
        {
            return CurrentDataContext.CurrentContext.MSRDetails.OrderByDescending(x => x.CreatedDate);
        }

        public static MSRDetail GetById(Guid Id)
        {
            return CurrentDataContext.CurrentContext.MSRDetails.FirstOrDefault(x => x.ID == Id && !x.IsDeleted);
        }

        public static IQueryable<MSRDetail> GetAllByMSRId(Guid MSRId)
        {
            return CurrentDataContext.CurrentContext.MSRDetails.Where(x => x.MSRId == MSRId && !x.IsDeleted);
        }

        public static MSRDetail GetByNo(string MSRNo)
        {
            return CurrentDataContext.CurrentContext.MSRDetails.FirstOrDefault(x => x.MSR.MSRNo == MSRNo && !x.IsDeleted);
        }
    }


    
}

