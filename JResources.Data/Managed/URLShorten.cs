﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class URLShorten
    {
        public static IQueryable<URLShorten> GetAll()
        {
            return CurrentDataContext.CurrentContext.URLShortens.OrderByDescending(x => x.CreatedDate);
        }

        public static URLShorten GetByCode(string Code)
        {
            return CurrentDataContext.CurrentContext.URLShortens.FirstOrDefault(x => x.UrlCode == Code);
        }
    }
}
