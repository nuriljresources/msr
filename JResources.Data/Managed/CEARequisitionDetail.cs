﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class CEARequisitionDetail
    {

        public static IQueryable<CEARequisitionDetail> GetAll()
        {
            return CurrentDataContext.CurrentContext.CEARequisitionDetails.OrderBy(x => x.CreatedDate);
        }

        public static CEARequisitionDetail GetById(Guid Id)
        {
            return CurrentDataContext.CurrentContext.CEARequisitionDetails.FirstOrDefault(x => x.ID == Id);
        }

        public static List<CEARequisitionDetail> GetByCEARequisitionId(Guid ID)
        {
            return CurrentDataContext.CurrentContext.CEARequisitionDetails.Where(x => x.CEARequisitionId == ID).ToList();

        }
        public static List<CEARequisitionDetail> GetByCEAId(Guid CEAId)
        {
            var CEARequistionIds = CEARequisition.GetByCEAId(CEAId).Select(x => x.ID).ToList();
            if(CEARequistionIds != null)
                return CurrentDataContext.CurrentContext.CEARequisitionDetails.Where(x => CEARequistionIds.Contains(x.CEARequisitionId)).ToList();

            return new List<CEARequisitionDetail>();
        }

    }
}
