﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class EnumTable
    {
        public enum EnumType
        {
            STATUS_MSR,
            JOB_PRIORITY,
            CANCEL_REASON,
            STORAGE_LOCATION,
            GENERAL_PARTS,
            STATUS_CEA,
            CBESummary_PPN//20200826FM D.Nugroho
        }

        public static IQueryable<EnumTable> GetAll()
        {
            return CurrentDataContext.CurrentContext.EnumTables.OrderBy(x => x.EnumCode);
        }

        public static EnumTable GetById(Guid Id)
        {
            return CurrentDataContext.CurrentContext.EnumTables.FirstOrDefault(x => x.ID == Id);
        }

        public static IQueryable<EnumTable> GetByCode(EnumType enumType)
        {
            string enumTypeStr = enumType.ToString();
            return CurrentDataContext.CurrentContext.EnumTables.Where(x => x.EnumCode == enumTypeStr).OrderByDescending(x => x.Sequence);
        }

        public static EnumTable GetDescriptionByCodeValue(EnumType enumType, string EnumValue)
        {
            string enumTypeStr = enumType.ToString();
            return CurrentDataContext.CurrentContext.EnumTables.FirstOrDefault(x => x.EnumCode == enumTypeStr && x.EnumValue == EnumValue);
        }

        public static EnumTable GetDescriptionByCodeValue(string CompanyCode, EnumType enumType, string EnumValue)
        {
            string enumTypeStr = enumType.ToString();
            return CurrentDataContext.CurrentContext.EnumTables.FirstOrDefault(x => x.CompanyCode == CompanyCode && x.EnumCode == enumTypeStr && x.EnumValue == EnumValue);
        }

        public static IQueryable<EnumTable> GetStorageLocation()
        {
            string enumTypeStr = EnumType.STORAGE_LOCATION.ToString();
            return CurrentDataContext.CurrentContext.EnumTables.Where(x => x.EnumCode == enumTypeStr).OrderByDescending(x => x.Sequence);
        }

    }
}
