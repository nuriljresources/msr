﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class RoleAccess
    {
        public static IQueryable<RoleAccess> GetAll()
        {
            return CurrentDataContext.CurrentContext.RoleAccesses.OrderBy(x => x.RoleAccessCode);
        }

        public static RoleAccess GetById(Guid Id)
        {
            return CurrentDataContext.CurrentContext.RoleAccesses.FirstOrDefault(x => x.ID == Id);
        }

        public static RoleAccess GetByCode(string roleCode)
        {
            return CurrentDataContext.CurrentContext.RoleAccesses.FirstOrDefault(x => x.RoleAccessCode == roleCode);
        }
    }
}
