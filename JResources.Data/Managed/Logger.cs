﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public enum LoggerCategory
    {
        [Description("MSR_CREATE")]
        MSR_CREATE,

        [Description("MSR_APPROVE")]
        MSR_APPROVE,

        [Description("MSR_ISSUED")]
        MSR_ISSUED,

        [Description("FRF_CREATE")]
        FRF_CREATE,

        [Description("FRF_ISSUED")]
        FRF_ISSUED,

        [Description("REPORT_TEST")]
        REPORT_TEST,

        [Description("JOB_VALIDATE")]
        JOB_VALIDATE,
    }

    public partial class Logger
    {
        public static IQueryable<Logger> GetAll()
        {
            return CurrentDataContext.CurrentContext.Loggers.OrderBy(x => x.CreatedDate);
        }

        public static void Set(LoggerCategory category, object objData)
        {
            string message = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
            Set(category, string.Empty, message, null);
        }

        public static void Set(LoggerCategory category, string pageURL, object objData)
        {
            string message = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
            Set(category, pageURL, message, null);
        }

        public static void Set(LoggerCategory category, string pageURL, object objData, Exception ex)
        {
            string message = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
            Set(category, pageURL, message, ex);
        }

        public static void Set(LoggerCategory category, string pageURL, string message)
        {
            Set(category, pageURL, message, null);
        }

        public static void Set(LoggerCategory category, string pageURL, string message, Exception ex)
        {
            Logger log = new Logger();
            log.LogCategory = category.ToString();
            log.LogMessage = message;
            log.PageURL = pageURL;

            if (ex != null)
            {
                var exStr = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                log.Exception = exStr;
            }

            log.Save<Logger>();
        }

    }
}
