﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class DocumentApproval
    {
        public static IQueryable<DocumentApproval> GetAll()
        {
            return CurrentDataContext.CurrentContext.DocumentApprovals.OrderByDescending(x => x.CreatedDate);
        }

        public static IQueryable<DocumentApproval> GetByDocumentNo(string DocumentNo)
        {
            return CurrentDataContext.CurrentContext.DocumentApprovals.Where(x => x.DocumentNo == DocumentNo).OrderBy(x => x.Sequence);
        }
    }
}
