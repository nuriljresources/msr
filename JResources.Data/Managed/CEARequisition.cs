﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class CEARequisition
    {

        public static IQueryable<CEARequisition> GetAll()
        {
            return CurrentDataContext.CurrentContext.CEARequisitions.Where(x => !x.IsDeleted).OrderBy(x => x.CreatedDate);
        }

        public static IQueryable<CEARequisition> GetByCEAId(Guid Id)
        {
            return CurrentDataContext.CurrentContext.CEARequisitions.Where(x => x.CEAId == Id && !x.IsDeleted);
        }


        public static CEARequisition GetByRQNNo(string documentNo)
        {
            return CurrentDataContext.CurrentContext.CEARequisitions.FirstOrDefault(x => x.RequisitionNo == documentNo && !x.IsDeleted);
        }

        public static CEARequisition GetById(Guid Id)
        {
            return CurrentDataContext.CurrentContext.CEARequisitions.FirstOrDefault(x => x.ID == Id);
        }



    }
}
