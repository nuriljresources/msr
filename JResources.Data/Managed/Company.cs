﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class Company
    {
        public static IQueryable<Company> GetAll()
        {
            return CurrentDataContext.CurrentContext.Companies.OrderByDescending(x => x.CreatedDate);
        }

        public static IQueryable<Company> GetAllActive()
        {
            return CurrentDataContext.CurrentContext.Companies.Where(x => !x.IsDeleted).OrderByDescending(x => x.CreatedDate);
        }

        public static Company GetById(Guid Id)
        {
            return CurrentDataContext.CurrentContext.Companies.FirstOrDefault(x => x.ID == Id);
        }

        public static Company GetByDepartmentId(Guid DepartmentId)
        {
            var department = CurrentDataContext.CurrentContext.Departments.FirstOrDefault(x => x.ID == DepartmentId);
            return CurrentDataContext.CurrentContext.Companies.FirstOrDefault(x => x.ID == department.CompanyId);
        }

        public static Company GetByCode(string CompanyCode)
        {
            return CurrentDataContext.CurrentContext.Companies.FirstOrDefault(x => x.CompanyCode == CompanyCode);
        }

        public static void DeleteById(Guid Id)
        {
            var company = CurrentDataContext.CurrentContext.Companies.FirstOrDefault(x => x.ID == Id);
            if (company != null)
            {
                company.IsDeleted = true;
            }

            company.Save<Company>();
        }
    }
}
