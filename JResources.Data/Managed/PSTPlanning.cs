﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class PSTPlanning
    {
        public static IQueryable<PSTPlanning> GetAll()
        {
            return CurrentDataContext.CurrentContext.PSTPlannings.Where(x => !x.IsDeleted).OrderByDescending(x => x.CreatedDate);
        }

        public static PSTPlanning GetByDate(DateTime pstDate)
        {
            return CurrentDataContext.CurrentContext.PSTPlannings.FirstOrDefault(x =>
                !x.IsDeleted && (x.DateFrom <= pstDate && x.DateTo >= pstDate)
            );
        }        

        public static PSTPlanning GetByRangeDate(DateTime dateFrom, DateTime dateTo)
        {
            return CurrentDataContext.CurrentContext.PSTPlannings.FirstOrDefault(x =>
                !x.IsDeleted &&  (
                    (
                        (x.DateFrom >= dateFrom && x.DateFrom <= dateTo) 
                            || 
                        (x.DateTo >= dateFrom && x.DateTo <= dateTo)
                    )
                    ||
                    (
                        (dateFrom >= x.DateTo && dateFrom <= x.DateTo)
                            || 
                        (dateTo >= x.DateFrom && dateTo <= x.DateTo)
                    )
                )
            );
        }

        public static PSTPlanning GetById(Guid Id)
        {
            return CurrentDataContext.CurrentContext.PSTPlannings.FirstOrDefault(x => x.ID == Id);
        }

        public static PSTPlanning GetByPlanningNo(string _planningNo)
        {
            return CurrentDataContext.CurrentContext.PSTPlannings.FirstOrDefault(x => x.PlanningNo == _planningNo);
        }
    }
}
