﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class EquipmentMapping
    {
        public static IQueryable<EquipmentMapping> GetAll()
        {
            return CurrentDataContext.CurrentContext.EquipmentMappings.OrderByDescending(x => x.CreatedDate);
        }

        public static EquipmentMapping GetByEquipmentNo(string EquipmentNo)
        {
            return CurrentDataContext.CurrentContext.EquipmentMappings.FirstOrDefault(x => x.EquipmentCode == EquipmentNo);
        }

        public static EquipmentMapping GetByEquipmentMappingId(Guid EquipmentMappingId)
        {
            return CurrentDataContext.CurrentContext.EquipmentMappings.FirstOrDefault(x => x.ID == EquipmentMappingId);
        }
    }
}
