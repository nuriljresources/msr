﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class EmailTemplate
    {
        public static IQueryable<EmailTemplate> GetAll()
        {
            return CurrentDataContext.CurrentContext.EmailTemplates.OrderByDescending(x => x.CreatedDate);
        }

        public static EmailTemplate GetByEmailCode(string EmailCode)
        {
            return CurrentDataContext.CurrentContext.EmailTemplates.FirstOrDefault(x => x.EmailCode == EmailCode);
        }

        public static EmailTemplate GetById(Guid Id)
        {
            return CurrentDataContext.CurrentContext.EmailTemplates.FirstOrDefault(x => x.ID == Id);
        }
    }
}
