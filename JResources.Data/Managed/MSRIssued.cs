﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public partial class MSRIssued
    {
        public static IQueryable<MSRIssued> GetAll()
        {
            return CurrentDataContext.CurrentContext.MSRIssueds.Where(x => !x.IsDeleted).OrderByDescending(x => x.CreatedDate);
        }

        public static MSRIssued GetById(Guid Id)
        {
            return CurrentDataContext.CurrentContext.MSRIssueds.FirstOrDefault(x => x.ID == Id);
        }

        public static MSRIssued GetByIssuedNo(string IssuedNo)
        {
            return CurrentDataContext.CurrentContext.MSRIssueds.FirstOrDefault(x => x.MSRIssuedNo == IssuedNo);
        }

        public static IQueryable<MSRIssued> GetByMSRId(Guid MSRId)
        {
            return CurrentDataContext.CurrentContext.MSRIssueds.Where(x => x.MSRId == MSRId && !x.IsDeleted);
        }

        public static IQueryable<MSRIssued> GetByMSRNo(string MSRNo)
        {
            return CurrentDataContext.CurrentContext.MSRIssueds.Where(x => !x.IsDeleted && x.MSR.MSRNo == MSRNo);
        }

        
    }

    public partial class MSRIssuedDetail
    {
        public static List<MSRIssuedDetail> GetByMSRIssuedId(Guid Id)
        {
            return CurrentDataContext.CurrentContext.MSRIssuedDetails.Where(x => x.MSRIssuedId == Id).ToList();
        }

        public static IQueryable<MSRIssuedDetail> GetAll()
        {
            return CurrentDataContext.CurrentContext.MSRIssuedDetails.OrderByDescending(x => x.CreatedDate);
        }
    }
}
