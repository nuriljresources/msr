﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Objects;
using System.Linq;
using JResources.Data;
using JResources.Data.GenericRepository;
using System.Data.EntityClient;
using JResources.Common;
using System.Data.SqlClient;

namespace JResources.Data
{
    public static class CurrentDataContext
    {
        public static JResourcesEntities CurrentContext
        {
            get
            {
                var repository = DataRepositoryStore.CurrentDataStore[DataRepositoryStore.KEY_DATACONTEXT] as JResourcesEntities;//IRepository;
                if (repository == null)
                {
                    repository = new JResourcesEntities(JResources.Common.ConnectionManager.MainConnection);
                    //repository.CommandTimeout = 1800;
                    DataRepositoryStore.CurrentDataStore[DataRepositoryStore.KEY_DATACONTEXT] = repository;
                }

                return repository;

            }
        }

        public static void CloseCurrentRepository()
        {
            var repository = DataRepositoryStore.CurrentDataStore[DataRepositoryStore.KEY_DATACONTEXT] as JResourcesEntities;//IRepository;
            if (repository != null)
            {
                repository.Dispose();
                DataRepositoryStore.CurrentDataStore[DataRepositoryStore.KEY_DATACONTEXT] = null;
            }
        }
    }

    public static class AccpacDataContext
    {
        public static AccpacEntities CurrentContext
        {
            get
            {
                var repository = DataRepositoryStoreAccpac.CurrentDataStore[DataRepositoryStoreAccpac.KEY_DATACONTEXT] as AccpacEntities;//IRepository;
                if (repository == null)
                {
                    repository = new AccpacEntities(JResources.Common.ConnectionManager.GetConnection(Common.DatabaseGroupName.ACCPAC_DATABASE));
                    DataRepositoryStoreAccpac.CurrentDataStore[DataRepositoryStoreAccpac.KEY_DATACONTEXT] = repository;
                }

                return repository;
            }
        }

        public static void CloseCurrentRepository()
        {
            var repository = DataRepositoryStoreAccpac.CurrentDataStore[DataRepositoryStoreAccpac.KEY_DATACONTEXT] as AccpacEntities;//IRepository;
            if (repository != null)
            {
                repository.Dispose();
                DataRepositoryStoreAccpac.CurrentDataStore[DataRepositoryStoreAccpac.KEY_DATACONTEXT] = null;
            }
        }

        public static string GetDatabaseName(string CompanyCode)
        {
            var connection = ConnectionManager.GetConnectionStringByCompanyCode(Common.DatabaseGroupName.ACCPAC_DATABASE, CompanyCode);

            EntityConnectionStringBuilder conn = new EntityConnectionStringBuilder(connection);
            SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder(conn.ProviderConnectionString);

            return connectionStringBuilder.InitialCatalog;
        }
    }
}