﻿using System;


namespace JResources.Data
{
    public class DataRepositoryStore
    {
        public static readonly string KEY_DATACONTEXT = "DataRepository";
        public static IDataRepositoryStore CurrentDataStore;
    }

    public class DataRepositoryStoreAccpac
    {
        public static readonly string KEY_DATACONTEXT = "DataRepositoryMSR";
        public static IDataRepositoryStoreAccpac CurrentDataStore;
    }
}
