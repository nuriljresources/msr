﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data
{
    public interface IDataRepositoryStore
    {
        object this[string key] { get; set; }
    }

    public interface IDataRepositoryStoreAccpac
    {
        object this[string key] { get; set; }
    }
}
