﻿using System;
using System.Data;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Reflection;
using JResources.Data;

public static class EntityObjectExtension
{
    public static IEntityWithKey Save(this IEntityWithKey entity)
    {
        object originalItem;

        if (CurrentDataContext.CurrentContext.TryGetObjectByKey(entity.EntityKey, out originalItem))
        {
            CurrentDataContext.CurrentContext.ApplyCurrentValues(entity.EntityKey.EntitySetName, entity);
        }
        else
        {
            CurrentDataContext.CurrentContext.AddObject(entity.EntityKey.EntitySetName, entity);
        }
        CurrentDataContext.CurrentContext.SaveChanges();
        return entity;
    }

    /// <summary>
    ///Saves the specified object to the current Context
    /// </summary>
    /// <param name="obj">The obj.</param>
    public static void Save<T>(this EntityObject obj) where T : class, new()
    {
        var isNewRecord = IsNewRecord(obj.EntityKey);
        if (isNewRecord)
        {

            var objectSet = CurrentDataContext.CurrentContext.CreateObjectSet<T>();
            CurrentDataContext.CurrentContext.AddObject(objectSet.EntitySet.Name, obj);
        }

        SetPropertyUpdated(obj, isNewRecord);
        var objectState = isNewRecord ? EntityState.Added :  EntityState.Modified;
        CurrentDataContext.CurrentContext.ObjectStateManager.ChangeObjectState(obj, objectState);
        CurrentDataContext.CurrentContext.SaveChanges();
    }

    public static void SetPropertyUpdated(EntityObject obj, bool IsNewRecord)
    {
        string CurrentUser = JResources.Data.Model.CurrentUser.NIKSITE;

        if (IsNewRecord)
        {
            PropertyInfo prop = obj.GetType().GetProperty("CreatedBy", BindingFlags.Public | BindingFlags.Instance);
            if (null != prop && prop.CanWrite)
            {
                var createdBy = prop.GetValue(obj, null);
                if (string.IsNullOrEmpty(CurrentUser) && createdBy != null && !string.IsNullOrEmpty(createdBy.ToString()))
                {
                    CurrentUser = createdBy.ToString();
                }

                prop.SetValue(obj, CurrentUser, null);
            }

            prop = obj.GetType().GetProperty("CreatedDate", BindingFlags.Public | BindingFlags.Instance);
            if (null != prop && prop.CanWrite)
            {
                prop.SetValue(obj, DateTime.Now, null);
            }
        }

        PropertyInfo propUpdate = obj.GetType().GetProperty("UpdatedBy", BindingFlags.Public | BindingFlags.Instance);
        if (null != propUpdate && propUpdate.CanWrite)
        {
            propUpdate.SetValue(obj, CurrentUser, null);
        }

        propUpdate = obj.GetType().GetProperty("UpdatedDate", BindingFlags.Public | BindingFlags.Instance);
        if (null != propUpdate && propUpdate.CanWrite)
        {
            propUpdate.SetValue(obj, DateTime.Now, null);
        }
    }

    /// <summary>
    /// Inserts the specified obj.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj">The obj.</param>
    public static void InsertSave<T>(this EntityObject obj) where T : class, new()
    {
        var isNewRecord = IsNewRecord(obj.EntityKey);
        if (isNewRecord)
        {
            var objectSet = CurrentDataContext.CurrentContext.CreateObjectSet<T>();
            CurrentDataContext.CurrentContext.AddObject(objectSet.EntitySet.Name, obj);
        }

        SetPropertyUpdated(obj, isNewRecord);
        CurrentDataContext.CurrentContext.ObjectStateManager.ChangeObjectState(obj, EntityState.Added);
        CurrentDataContext.CurrentContext.SaveChanges();
    }

    /// <summary>
    /// Updates the save.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj">The obj.</param>
    public static void UpdateSave<T>(this EntityObject obj) where T : class, new()
    {
        var isNewRecord = IsNewRecord(obj.EntityKey);
        if (isNewRecord)
        {
            var objectSet = CurrentDataContext.CurrentContext.CreateObjectSet<T>();
            CurrentDataContext.CurrentContext.AddObject(objectSet.EntitySet.Name, obj);
        }

        SetPropertyUpdated(obj, isNewRecord);
        CurrentDataContext.CurrentContext.ObjectStateManager.ChangeObjectState(obj, EntityState.Modified);
        CurrentDataContext.CurrentContext.SaveChanges();
    }

    /// <summary>
    /// Deletes the specified EntityObject.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj">The obj.</param>
    public static void Delete<T>(this EntityObject obj) where T : class, new()
    {
        var isNewRecord = IsNewRecord(obj.EntityKey);
        if (isNewRecord)
        {
            var objectSet = CurrentDataContext.CurrentContext.CreateObjectSet<T>();
            CurrentDataContext.CurrentContext.AddObject(objectSet.EntitySet.Name, obj);
        }

        SetPropertyUpdated(obj, isNewRecord);
        CurrentDataContext.CurrentContext.ObjectStateManager.ChangeObjectState(obj, EntityState.Deleted);
        CurrentDataContext.CurrentContext.SaveChanges();
    }

    /// <summary>
    /// Determines whether current object is new. 
    /// </summary>
    /// <param name="entityKey">The current EntityObject.</param>
    private static bool IsNewRecord(EntityKey entityKey)
    {
        if (entityKey == null)
        {
            return true;
        }
        int temp = 0;
        var keyMembers = entityKey.EntityKeyValues;
        return keyMembers.Where(x => string.IsNullOrWhiteSpace(x.Value.ToString()) || (Int32.TryParse(x.Value.ToString(), out temp) && Convert.ToInt32(x.Value.ToString()) == 0)).Any();
    }

    /// <summary>
    /// Determines whether current object is new. 
    /// </summary>
    /// <param name="obj">The current EntityObject.</param>
    public static bool IsNewRecord(this EntityObject obj)
    {
        if (obj == null)
        {
            return true;
        }
        return IsNewRecord(obj.EntityKey);
    }
}

