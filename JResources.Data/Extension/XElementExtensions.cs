﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;

namespace JResources.Data.Extension
{
    public static class XElementExtensions
    {
        public static int? SafeNullableInt32Attribute(this XElement node, string attributeName)
        {
            int? result = null;
            if (node != null && node.Attribute(attributeName) != null)
            {
                int parseOut;
                if (int.TryParse(node.Attribute(attributeName).Value, out parseOut))
                {
                    result = parseOut;
                }
            }
            return result;
        }

        public static int SafeInt32Attribute(this XElement node, string attributeName)
        {
            int result = 0;
            if (node != null && node.Attribute(attributeName) != null)
            {
                int.TryParse(node.Attribute(attributeName).Value, out result);
            }
            return result;
        }

        public static long? SafeNullableInt64Attribute(this XElement node, string attributeName)
        {
            long? result = null;
            if (node != null && node.Attribute(attributeName) != null)
            {
                long parseOut;
                if (long.TryParse(node.Attribute(attributeName).Value, out parseOut))
                {
                    result = parseOut;
                }
            }
            return result;
        }

        public static long SafeInt64Attribute(this XElement node, string attributeName)
        {
            long result = 0L;
            if (node != null && node.Attribute(attributeName) != null)
            {
                long.TryParse(node.Attribute(attributeName).Value, out result);
            }
            return result;
        }

        public static string SafeStringAttribute(this XElement node, string attributeName)
        {
            if (node != null && node.Attribute(attributeName) != null)
            {
                return node.Attribute(attributeName).Value;
            }
            return null;
        }

        public static bool SafeBoolAttribute(this XElement node, string attributeName)
        {
            bool result = false;
            if (node != null && node.Attribute(attributeName) != null)
            {
                string attr = node.Attribute(attributeName).Value.ToLower();
                if ((attr == "true") || (attr == "1"))
                {
                    result = true;
                }
            }
            return result;
        }

        public static DateTime? SafeNullableDateTimeAttribute(this XElement node, string attributeName)
        {
            DateTime? result = null;
            if (node != null && node.Attribute(attributeName) != null)
            {
                try
                {
                    result = DateTime.ParseExact(node.Attribute(attributeName).Value, "yyyy-MM-dd HH:mm:ss", null);
                }
                catch (FormatException)
                {
                }
            }
            return result;
        }

        public static decimal SafeDecimalAttribute(this XElement node, string attributeName)
        {
            decimal result = 0M;
            if (node != null && node.Attribute(attributeName) != null)
            {
                decimal.TryParse(node.Attribute(attributeName).Value, out result);
            }
            return result;
        }

        public static int? SafeNullableInt32Element(this XElement node, string elementName)
        {
            int? result = null;
            if (node != null && node.XPathSelectElement(elementName) != null)
            {
                int parseOut;
                if (int.TryParse(node.XPathSelectElement(elementName).Value, out parseOut))
                {
                    result = parseOut;
                }
            }
            return result;
        }

        public static int SafeInt32Element(this XElement node, string elementName)
        {
            int result = 0;
            if (node != null && node.XPathSelectElement(elementName) != null)
            {
                int.TryParse(node.XPathSelectElement(elementName).Value, out result);
            }
            return result;
        }

        public static long? SafeNullableInt64Element(this XElement node, string elementName)
        {
            long? result = null;
            if (node != null && node.XPathSelectElement(elementName) != null)
            {
                long parseOut;
                if (long.TryParse(node.XPathSelectElement(elementName).Value, out parseOut))
                {
                    result = parseOut;
                }
            }
            return result;
        }

        public static long SafeInt64Element(this XElement node, string elementName)
        {
            long result = 0L;
            if (node != null && node.XPathSelectElement(elementName) != null)
            {
                long.TryParse(node.XPathSelectElement(elementName).Value, out result);
            }
            return result;
        }

        public static string SafeStringElement(this XElement node, string elementName)
        {
            if (node != null && node.XPathSelectElement(elementName) != null)
            {
                return node.XPathSelectElement(elementName).Value;
            }
            return null;
        }

        public static bool SafeBoolElement(this XElement node, string elementName)
        {
            bool result = false;
            if (node != null && node.XPathSelectElement(elementName) != null)
            {
                string attr = node.XPathSelectElement(elementName).Value.ToLower();
                if ((attr == "true") || (attr == "1"))
                {
                    result = true;
                }
            }
            return result;
        }

        /// <summary>
        /// Safely gets a DateTime from a "datetime" element
        /// </summary>
        /// <remarks>
        /// Luxbet datetime element is an XML node with a "tz_gmt_offset" attribute and yyyy-MM-dd HH:mm:ss format contents
        /// </remarks>
        /// <param name="node">The node.</param>
        /// <param name="elementName">Name of the element.</param>
        /// <returns></returns>
        public static DateTime? SafeLuxbetDateTimeElement(this XElement node, string elementName)
        {
            DateTime? result = null;
            if (node != null && node.XPathSelectElement(elementName) != null)
            {
                try
                {
                    result = DateTime.ParseExact(node.XPathSelectElement(elementName).Value, "yyyy-MM-dd HH:mm:ss", null);
                    long tzGmtOffset = node.XPathSelectElement(elementName).SafeInt64Attribute("tz_gmt_offset");
                    // convert time from XML to GMT by subtracting tz_gmt_offset value
                    DateTime gmtTime = DateTime.SpecifyKind(result.Value.AddSeconds(-1.0D * (double)tzGmtOffset), DateTimeKind.Utc);
                    // and then convert back to webserver local time
                    result = gmtTime.ToLocalTime();
                }
                catch (FormatException)
                {
                }
            }
            return result;
        }

        public static DateTime? SafeNullableDateTimeElement(this XElement node, string elementName)
        {
            DateTime? result = null;
            if (node != null && node.XPathSelectElement(elementName) != null)
            {
                try
                {
                    result = DateTime.ParseExact(node.XPathSelectElement(elementName).Value, "yyyy-MM-dd HH:mm:ss", null);
                }
                catch (FormatException)
                {
                }
            }
            return result;
        }

        public static decimal SafeDecimalElement(this XElement node, string elementName)
        {
            decimal result = 0M;
            if (node != null && node.XPathSelectElement(elementName) != null)
            {
                decimal.TryParse(node.XPathSelectElement(elementName).Value, out result);
            }

            return result;
        }

        public static decimal SafeDecimalValue(this XElement node)
        {
            decimal result = 0M;
            decimal.TryParse(node.Value, out result);
            return result;
        }
    }
}
