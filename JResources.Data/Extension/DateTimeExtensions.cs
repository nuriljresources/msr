﻿using System;
using System.Collections.Generic;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Text;

namespace JResources.Data.Extension
{
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Previouses the month first day.
        /// </summary>
        /// <param name="currentDate">The current date.</param>
        /// <returns></returns>
        public static DateTime PreviousMonthFirstDay(this DateTime currentDate)
        {
            DateTime date = currentDate.PreviousMonthLastDay();
            return new DateTime(date.Year, date.Month, 1);
        }

        /// <summary>
        /// Previouses the month last day.
        /// </summary>
        /// <param name="currentDate">The current date.</param>
        /// <returns></returns>
        public static DateTime PreviousMonthLastDay(this DateTime currentDate)
        {
            return new DateTime(currentDate.Year, currentDate.Month, 1).AddDays(-1);
        }

        /// <summary>
        /// Gets the first day of month.
        /// </summary>
        /// <param name="currentDate">The current date.</param>
        /// <returns></returns>
        public static DateTime GetFirstDayOfMonth(this DateTime currentDate)
        {
            DateTime dtFrom = currentDate;
            dtFrom = dtFrom.AddDays(-(dtFrom.Day - 1));
            return dtFrom;
        }

        /// <summary>
        /// Gets the last day of month.
        /// </summary>
        /// <param name="currentDate">The current date.</param>
        /// <returns></returns>
        public static DateTime GetLastDayOfMonth(this DateTime currentDate)
        {
            DateTime dtTo = currentDate;
            // overshoot the date by a month 
            dtTo = dtTo.AddMonths(1);

            // remove all of the days in the next month 
            // to get bumped down to the last day of the 
            // previous month 
            dtTo = dtTo.AddDays(-(dtTo.Day));
            // return the last day of the month 
            return dtTo;
        }

        /// <summary>
        /// Gets the start of last week.
        /// </summary>
        /// <param name="currentDate">The current Date.</param>
        /// <returns></returns>
        public static DateTime GetStartOfLastWeek(this DateTime currentDate)
        {
            int daysToSubtract = (int)currentDate.DayOfWeek + 7;
            DateTime dt = currentDate.Subtract(TimeSpan.FromDays(daysToSubtract));
            return new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0, 0);
        }

        /// <summary>
        /// Gets the end of last week.
        /// </summary>
        /// <param name="currentDate">The current Date.</param>
        /// <returns></returns>
        public static DateTime GetEndOfLastWeek(this DateTime currentDate)
        {
            DateTime dt = GetStartOfLastWeek(currentDate).AddDays(6);
            return new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59, 999);
        }

        /// <summary>
        /// Gets the start of current week.
        /// </summary>
        /// <param name="currentDate">The current Date.</param>
        /// <returns></returns>
        public static DateTime GetStartOfCurrentWeek(this DateTime currentDate)
        {
            int daysToSubtract = (int)DateTime.Now.DayOfWeek;
            DateTime dt = DateTime.Now.Subtract(TimeSpan.FromDays(daysToSubtract));
            return new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0, 0);
        }

        /// <summary>
        /// Gets the end of current week.
        /// </summary>
        /// <param name="currentDate">The current Date.</param>
        /// <returns></returns>
        public static DateTime GetEndOfCurrentWeek(this DateTime currentDate)
        {
            DateTime dt = GetStartOfCurrentWeek(currentDate).AddDays(6);
            return new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59, 999);
        }

        public static System.DateTime AddBusinessDaysNoSql(this System.DateTime source, int businessDays)
        {
            var dayOfWeek = businessDays < 0
                                ? ((int)source.DayOfWeek - 12) % 7
                                : ((int)source.DayOfWeek + 6) % 7;

            switch (dayOfWeek)
            {
                case 6:
                    businessDays--;
                    break;
                case -6:
                    businessDays++;
                    break;
            }

            return source.AddDays(businessDays + ((businessDays + dayOfWeek) / 5) * 2);
        }

        [EdmFunction("JResourcesModel.Store","AddBusinessDays")]
        public static DateTime AddBusinessDays(DateTime date, int businessDays)
        {
            throw new NotSupportedException("This function can only be used in a LINQ to Entities query");
        }

        [EdmFunction("JResourcesModel.Store", "ufn_StatusSalesOrderDetailCMS")]
        public static string ufn_StatusSalesOrderDetailCMS(string internalStatus, int paymentStatus, string CODstatus, string PaymentGatewayCode)
        {
            throw new NotSupportedException("This function can only be used in a LINQ to Entities query");
        }


        [EdmFunction("JResourcesModel.Store", "ufn_StatusSalesOrderDetailWeb")]
        public static string ufn_StatusSalesOrderDetailWeb(string internalStatus, int paymentStatus, string soStatus)
        {
            throw new NotSupportedException("This function can only be used in a LINQ to Entities query");
        }
    }
}
