﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using JResources.Common;

namespace JResources.Data.Model
{
    public class MSRModel
    {
        public MSRModel()
        {
            DateCreated = DateTime.Now;
            DateCreatedStr = DateTime.Now.ToString(Constant.FORMAT_DATE_JRESOURCES);
            MSRDetail = new List<MSRDetailModel>();
            Requestor = new UserModel();

            Manager = new UserModel();
            FunctionalGeneralManager = new UserModel();
            GeneralManagerSite = new UserModel();
            FunctionalDirector = new UserModel();
            FinanceManager = new UserModel();

            DateCreated = DateTime.Now;
        }

        public void SetDateFrom()
        {
            if (!string.IsNullOrEmpty(DateCreatedStr))
            {
                DateCreated = CommonFunction.DateTimeJResources(DateCreatedStr);
            }

            if (!string.IsNullOrEmpty(ExpectedDateStr))
            {
                ExpectedDate = CommonFunction.DateTimeJResources(ExpectedDateStr);
            }
        }

        public Guid ID { get; set; }
        public string DocumentNo { get; set; }
        public string PPP { get; set; }
        public string MSRType { get; set; }
        public string MSRTypeLabel { get; set; }
        public Guid CompanyId { get; set; }
        public string CompanyName { get; set; }
        public Guid DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string CostCenter { get; set; }
        public string CostCenterLabel { get; set; }
        public string MaintenanceCode { get; set; }
        public string MaintenanceCodeLabel { get; set; }
        public bool IsUnloading { get; set; }
        public bool IsReOrderPoint { get; set; }
        public bool IsRequestRequisition { get; set; }
        public bool IsDataMigration { get; set; }

        public string DateCreatedStr { get; set; }
        public DateTime DateCreated { get; set; }
        public string DateUpdateStr { get; set; }
        public DateTime DateUpdate { get; set; }
        
        public string ExpectedDateStr { get; set; }
        public DateTime ExpectedDate { get; set; }

        public string ApprovedDateStr { get; set; }
        public DateTime ApprovedDate { get; set; }
        public UserModel ApprovedBy { get; set; }

        public string WorkOrderNo { get; set; }
        public string EquipmentNo { get; set; }
        public string Reference { get; set; }
        public string Reason { get; set; }
        public string StatusRequest { get; set; }
        public string StatusRequestLabel { get; set; }

        public LeadTimeModel LeadTime { get; set; }
        public int LeadTimeNumber { get; set; }

        public UserModel Requestor { get; set; }
        public UserModel Manager { get; set; }
        public UserModel FunctionalGeneralManager { get; set; }
        public UserModel GeneralManagerSite { get; set; }
        public UserModel FunctionalDirector { get; set; }
        public UserModel FinanceManager { get; set; }
        public decimal TotalWithApprovalGM { get; set; }

        public bool IsNeedActionRequest { get; set; }
        public bool IsWaitingApprovelBefore { get; set; }
        public decimal TotalAmount { get; set; }

        /// <summary>
        /// Additional Property
        /// </summary>
        public int WorkOrderStatus { get; set; }
        public string WorkOrderStatusLabel { get; set; }

        public List<MSRDetailModel> MSRDetail { get; set; }


        public decimal TotalInStock { get; set; }
        public decimal TotalBudgeted { get; set; }
        public decimal TotalUnBudgeted { get; set; }
        public void SetNewRequest()
        {
            StatusRequest = Common.Enum.RequestStatus.NEW;
        }
    }

    public class MSRDetailModel
    {
        public MSRDetailModel()
        {
            ItemLocations = new List<ItemLocationModel>();
        }

        public Guid ID { get; set; }
        public int LineNo { get; set; }
        public string EquipmentNo { get; set; }
        public string ItemNo { get; set; }
        public string UOM { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public string LocationFrom { get; set; }
        public string PickingSeq { get; set; }
        public decimal Qty { get; set; }
        public decimal QtyUnloading { get; set; }
        public decimal Cost { get; set; }
        public decimal StockOnHand { get; set; }
        public string Images { get; set; }
        public string CostCode { get; set; }
        public string CostCodeLabel { get; set; }
        public string Category { get; set; }
        public string CategoryLabel { get; set; }
        public bool IsDeleted { get; set; }

        public List<ItemLocationModel> ItemLocations { get; set; }

        public string IdLookup { get; set; }

        /// <summary>
        /// For Budget Calculation
        /// </summary>
        
        public string BudgetLabel { get; set; }

        // For Budget Validation
        public decimal QtyBudget { get; set; }
        public decimal SubTotalBudget { get; set; }


        // History Purchase
        public decimal QtyBudgetHistory { get; set; }
        public decimal SubTotalHistory { get; set; }
    }

    public class MSRListModel
    {
        public Guid ID { get; set; }
        public string DocumentNo { get; set; }
        public string WorkOrderNo { get; set; }
        public string DepartmentName { get; set; }
        public DateTime DateCreated { get; set; }
        public string CostCenter { get; set; }
        public string MaintenanceCode { get; set; }
        public DateTime ExpectedDate { get; set; }
        public string Reason { get; set; }
        public string EquipmentNo { get; set; }
        public string StatusRequestCode { get; set; }
        public string StatusRequestLabel { get; set; }
        public string Requestor { get; set; }
        public string NextApproval { get; set; }

        public string DateCreatedStr { get; set; }
        public string ExpectedDateStr { get; set; }

        public bool IsCanEdit { get; set; }
        public string RowCssColor { get; set; }
        public string LabelCssColor { get; set; }

        public bool IsDoneRequisition { get; set; }
        public bool IsCanRequisition { get; set; }

        public bool IsDoneUnloading { get; set; }
        public bool IsUnloading { get; set; }
    }
}

