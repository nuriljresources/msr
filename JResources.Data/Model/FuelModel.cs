﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class FuelModel
    {
        public Guid IDS { get; set; }
        public string filling_typ { get; set; }
        public DateTime filling_dttm { get; set; }
        public string fuelman_id { get; set; }
        public string comp_id { get; set; }
        public string fuelstation_id { get; set; }
        public string item_no { get; set; }
        public string equ_tank_id { get; set; }
        public string operator_id { get; set; }
        public decimal prev_hmkm { get; set; }
        public decimal last_hmkm { get; set; }
        public decimal start_totalisator { get; set; }
        public decimal end_totalisator { get; set; }
        public decimal quantity { get; set; }
        public decimal prev_stock { get; set; }
        public decimal end_stock { get; set; }
        public string remark { get; set; }
        public string frf_no { get; set; }
        public bool? is_posted { get; set; }
        public string shift { get; set; }
        public string posting_status { get; set; }
        public string posting_log { get; set; }
        public string fuelstation_id_to { get; set; }

        public string cat_code { get; set; }
        public string cccode { get; set; }
        public string mt_code { get; set; }
        public string po_no { get; set; }
        public string do_no { get; set; }
    }
}
