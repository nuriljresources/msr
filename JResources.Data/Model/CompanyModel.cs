﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class CompanyModel
    {
        public CompanyModel()
        {
            ConnectionString = new ConnectionStringModel();
        }

        public Guid ID { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string ADCode { get; set; }
        public bool IsEnabled { get; set; }
        public string DocumentCode { get; set; }
        public string Currency { get; set; }

        public ConnectionStringModel ConnectionString { get; set; }
    }

    public class CompanyDepartmentModel : CompanyModel
    {
        public CompanyDepartmentModel()
        {
            Departments = new List<DepartmentModel>();
        }

        public List<DepartmentModel> Departments { get; set; }
    }


}
