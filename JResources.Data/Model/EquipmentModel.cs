﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class EquipmentModel
    {
        public string EquipmentCode { get; set; }
        public string EquipmentDescription { get; set; }
        public string CostCenter { get; set; }


        public Guid EquipmentMappingID { get; set; }
        public string COA { get; set; }
    }
}

