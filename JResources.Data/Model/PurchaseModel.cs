﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class PurchaseModel : MSRIssuedModel
    {
        public string RQNNo { get; set; }
        public string WorkflowCode { get; set; }
        public string WorkflowLabel { get; set; }
    }
}
