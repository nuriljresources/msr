﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class ItemSearchModel
    {
        public ItemSearchModel()
        {
            suggestions = new List<ItemDetail>();
        }

        public string query { get; set; }
        public List<ItemDetail> suggestions { get; set; }
    }

    public class ItemDetail
    {
        public string value { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string UOM { get; set; }
        public string Category { get; set; }
        public decimal LastPrice { get; set; }
        public string COA { get; set; }
    }
}
