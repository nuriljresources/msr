﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JResources.Common;

namespace JResources.Data.Model
{
    public class MenuModel
    {
        public string NameMenu { get; set; }
        public string Url { get; set; }
        public string[] RoleAccess { get; set; }

        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string Icon { get; set; }

        public bool IsHeading { get; set; }
        public bool IsCurrent { get; set; }

        public bool IsHide { get; set; }

        public List<MenuModel> ChildsMenus { get; set; }
    }
}
