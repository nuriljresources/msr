﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class EmailModel
    {
        public EmailModel()
        {
            EmailAddress = new List<EmailAddressModel>();
        }

        public string From { get; set; }
        public string FromName { get; set; }
        public List<string> FromList { get; set; }
        public string To { get; set; }
        public string ToName { get; set; }
        public List<string> ToList { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

        public List<EmailAddressModel> EmailAddress { get; set; }
    }

    public class EmailAddressModel
    {
        public string EmailTo { get; set; }
        public string EmailToName { get; set; }
    }
}
