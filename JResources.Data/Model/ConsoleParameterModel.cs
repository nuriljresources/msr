﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class ConsoleParameterModel
    {
        public string MENU { get; set; }
        public DateTime DATE { get; set; }

        public DateTime? DATE_START { get; set; }
        public DateTime? DATE_END { get; set; }
    }
}
