﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    //public class MSROldModel
    //{
    //    public tMSR_master tMSR { get; set; }
    //    public List<tMSR_details> details { get; set; }
    //    public List<tMSR_details_maint> details_maint { get; set; }
    //    public List<tMSR_approval> approval { get; set; }
    //    public List<tMSR_display> display { get; set; }
    //    public List<tMSR_history> history { get; set; }
    //    public List<ttransfer_maintenance> transfer { get; set; }
    //}

    

    

    public class MSROldGridModel
    {
        public string MSRNo { get; set; }
        public DateTime? MSRDate { get; set; }
        public string CostCenter { get; set; }
        public string MaintenanceCode { get; set; }
        public string WorkOrder { get; set; }
        public DateTime? ExpectedDate { get; set; }
        public string Reason { get; set; }
        public string EquipmentNo { get; set; }
        public string RequestStatus { get; set; }
        public string RequestorId { get; set; }
        public string RequestorName { get; set; }
        public string NextApproval { get; set; }
    }





    


}
