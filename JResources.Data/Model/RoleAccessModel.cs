﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class RoleAccessModel
    {
        public Guid ID { get; set; }
        public string RoleAccessCode { get; set; }
        public string RoleAccessName { get; set; }
        public string AdditionalValue { get; set; }
        public bool IsHaveAdditional { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsCanDelegate { get; set; }


        /// <summary>
        /// Additional For Set Role Input
        /// </summary>
        public bool IsSelected { get; set; }
    }

    public class RoleAccessUserModel
    {
        public Guid ID { get; set; }
        public Guid RoleAccessID { get; set; }
        public string RoleAccessCode { get; set; }
        public string RoleAccessName { get; set; }
        public bool IsDefault { get; set; }
        public Guid CompanyId { get; set; }
        public string CompanyName { get; set; }
        public Guid DepartmentId { get; set; }
        public string DepartmentName { get; set; }

        /// <summary>
        /// For Additional Input/Edit User
        /// </summary>
        public bool IsHaveAccess { get; set; }
    }


    public class RoleCompanyModel
    {
        public RoleCompanyModel()
        {
            RoleCompanyDetails = new List<RoleCompanyDetailModel>();
        }

        public Guid UserId { get; set; }
        public Guid RoleAccessId { get; set; }
        public Guid CompanyId { get; set; }
        public bool IsHaveAdditional { get; set; }
        public List<RoleCompanyDetailModel> RoleCompanyDetails { get; set; }
    }

    public class RoleCompanyDetailModel
    {
        public Guid DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string AdditionalValue { get; set; }
        public bool IsSelected { get; set; }
    }
}
