﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JResources.Common;

namespace JResources.Data.Model
{
    public class SearchModel
    {
        public SearchModel()
        {
            CurrentPage = 1;
            initSet(false);

            ListDataGeneral = new List<object>();
        }

        public SearchModel(bool IsSetDate)
        {
            initSet(IsSetDate);
        }

        private void initSet(bool IsSetDate)
        {
            if (IsSetDate)
            {
                DateTime dtTo = DateTime.Now;
                DateTime dtFrom = DateHelper.DateFromMinimun(new DateTime(dtTo.Year, dtTo.Month, 1));
                DateTo = dtTo;
                DateToStr = dtTo.ToString(Constant.FORMAT_DATE_JRESOURCES);
                DateFrom = dtFrom;
                DateFromStr = dtFrom.ToString(Constant.FORMAT_DATE_JRESOURCES);
            }
        }

        public string[] ROLE_ACCESS { get; set; }
        public string[] MSR_STATUS { get; set; }
        public string[] CEA_STATUS { get; set; } //20200826FM D.Nugroho

        public string SearchCriteria { get; set; }

        public string DateFromStr { get; set; }
        public DateTime? DateFrom { get; set; }
        public string DateToStr { get; set; }
        public DateTime? DateTo { get; set; }

        public string IsDeletedString { get; set; }
        public bool? IsDeleted
        {
            get
            {
                if (string.IsNullOrEmpty(this.IsDeletedString))
                {
                    if (this.IsDeletedString == "1")
                        return true;
                }

                return false;
            }
        }

        public string MoreCriteria { get; set; }

        // Untuk Order By
        // Additional
        public string OrderBy { get; set; }
        public bool IsOrderByDesc { get; set; }

        public int TotalItems { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public int TotalPages { get; set; }
        public int StartPage { get; set; }
        public int EndPage { get; set; }

        // Additonal Untuk Approva Cancel
        public bool IsPendingCancel { get; set; }

        // Additional Untuk Exclude data Hasil migration MSR 1.0
        public bool IsExcludeDataMigration { get; set; }

        /// <summary>
        /// Untuk Search Requisition
        /// </summary>
        public bool IsRequisition { get; set; }

        public void SetPager(int totalItems)
        {
            // calculate total, start and end pages
            var totalPages = (int)Math.Ceiling((decimal)totalItems / (decimal)Common.SiteSettings.PAGE_SIZE_PAGING);
            var startPage = CurrentPage - 5;
            var endPage = CurrentPage + 4;
            if (startPage <= 0)
            {
                endPage -= (startPage - 1);
                startPage = 1;
            }
            if (endPage > totalPages)
            {
                endPage = totalPages;
                if (endPage > 10)
                {
                    startPage = endPage - 9;
                }
            }

            TotalItems = totalItems;
            PageSize = Common.SiteSettings.PAGE_SIZE_PAGING;
            TotalPages = totalPages;
            StartPage = startPage;
            EndPage = endPage;
        }

        public void SetDateRange()
        {
            if (!string.IsNullOrEmpty(DateFromStr))
            {
                var dateConvert = Common.CommonFunction.DateTimeJResources(DateFromStr);
                if (dateConvert != DateTime.MinValue)
                {
                    DateFrom = Common.DateHelper.DateFromMinimun(dateConvert);
                }
            }

            if (!string.IsNullOrEmpty(DateToStr))
            {
                var dateConvert = Common.CommonFunction.DateTimeJResources(DateToStr);
                if (dateConvert != DateTime.MinValue)
                {
                    DateTo = Common.DateHelper.DateToMax(dateConvert);
                }
            }
        }
        public bool IsSearchFilter { get; set; }

        public List<Object> ListDataGeneral { get; set; }
    }

    public class PSTUploadSearchModel : SearchModel
    {
        public string DocumentNo { get; set; }
        public string CreatedBy { get; set; }
    }

    public class UserSearchModel : SearchModel
    {
        public UserSearchModel()
        {
            ListData = new List<UserModel>();
        }

        public string NIKSITE { get; set; }
        public string Name { get; set; }
        public string CodeSite { get; set; }
        public Guid RoleAccessId { get; set; }

        public Guid CompanyId { get; set; }
        public Guid DepartmentId { get; set; }

        public List<UserModel> ListData { get; set; }
    }

    public class PartItemSearchModel : SearchModel
    {
        public PartItemSearchModel()
        {
            ListData = new List<ItemPartModel>();
            IsInventory = true;
        }

        public string ItemNo { get; set; }
        public string ItemDesc { get; set; }
        public string Location { get; set; }
        public bool IsZeroQty { get; set; }
        public bool IsInventory { get; set; }
        public List<string> ListItemNo { get; set; }
        public bool IsGetAllItem { get; set; }
        public bool IsReOrderPoint { get; set; }

        public List<ItemPartModel> ListData { get; set; }

        /// <summary>
        /// For Validation JOBS
        /// </summary>

        public string CompanyCode { get; set; }
        public Guid DepartmentId { get; set; }
        public string CostCenter { get; set; }
    }

    public class MSRSearchModel : SearchModel
    {
        public MSRSearchModel()
        {
            initSet(false);
        }

        public MSRSearchModel(bool IsSetDate)
        {
            initSet(IsSetDate);
        }

        private void initSet(bool IsSetDate)
        {
            ListData = new List<MSRListModel>();
            if (IsSetDate)
            {
                DateTime dtTo = DateTime.Now;
                DateTime dtFrom = DateHelper.DateFromMinimun(new DateTime(dtTo.Year, dtTo.Month, 1));
                DateTo = dtTo;
                DateToStr = dtTo.ToString(Constant.FORMAT_DATE_JRESOURCES);
                DateFrom = dtFrom;
                DateFromStr = dtFrom.ToString(Constant.FORMAT_DATE_JRESOURCES);
            }
        }

        public string MSRNo { get; set; }
        public string RequestStatus { get; set; }
        public string CodeCostCenter { get; set; }
        public string Reason { get; set; }
        public Guid CompanyId { get; set; }
        public Guid DepartmentId { get; set; }
        public string IssuedType { get; set; }
        public string EquipmentNo { get; set; }
        public string MaintenanceCode { get; set; }
        public string ItemNo { get; set; }
        public string IssuedNo { get; set; }

        public bool IsLookup { get; set; }
        public bool IsReOrderPoint { get; set; }
        public bool IsRequisition { get; set; }
        public bool IsRecentActivity { get; set; }

        public string WorkOrderNo { get; set; }
        public List<MSRListModel> ListData { get; set; }

        /// <summary>
        /// Additional MSR To Modal
        /// </summary>
        public Guid MSRIdModal { get; set; }
    }

    public class FRFSearchModel : SearchModel
    {
        public FRFSearchModel()
        {
            initSet(false);
            ListData = new List<FRFModel>();
        }

        public FRFSearchModel(bool IsSetDate)
        {
            initSet(IsSetDate);
        }

        private void initSet(bool IsSetDate)
        {
            DateTime dtTo = DateTime.Now;
            DateTime dtFrom = DateHelper.DateFromMinimun(new DateTime(dtTo.Year, dtTo.Month, 1)).AddMonths(-6);
            DateTo = dtTo;
            DateToStr = dtTo.ToString(Constant.FORMAT_DATE_JRESOURCES);
            DateFrom = dtFrom;
            DateFromStr = dtFrom.ToString(Constant.FORMAT_DATE_JRESOURCES);
        }

        public string FRFNo { get; set; }
        public string FRFStatus { get; set; }
        public string Requestor { get; set; }
        public Guid CompanyId { get; set; }

        public List<FRFModel> ListData { get; set; }
    }

    public class WorkOrderSearchModel : SearchModel
    {
        public string WorkOrderNo { get; set; }

        public List<WorkOrderModel> ListData { get; set; }
    }

    public class MSRIssuedSearchModel : SearchModel
    {
        public string MSRIssuedNo { get; set; }
        public string MSRNo { get; set; }
        public string RequestStatus { get; set; }        
        public Guid CompanyId { get; set; }
        public Guid DepartmentId { get; set; }

        public string[] IssuedType { get; set; }

        public MSRIssuedSearchModel()
        {
            ListData = new List<MSRIssuedListmodel>();
        }

        public List<MSRIssuedListmodel> ListData { get; set; }
    }

    public class RoleDelegateSearchModel : SearchModel
    {
        public RoleDelegateSearchModel()
        {
            ListData = new List<RoleDelegationModel>();
        }

        public Guid RoleAccessId { get; set; }

        public List<RoleDelegationModel> ListData { get; set; }        
    }

    public class SchedulerSearchModel : SearchModel
    {
        public string MSRIssuedNo { get; set; }
        public string MSRNo { get; set; }
        public string RequestStatus { get; set; }
        public Guid CompanyId { get; set; }

        public string[] IssuedType { get; set; }

        public SchedulerSearchModel()
        {
            ListData = new List<SchedulerTaskModel>();
        }

        public List<SchedulerTaskModel> ListData { get; set; }
    }

    //20200826FM D.Nugroho
    public class CEASearchModel : SearchModel
    {
        public CEASearchModel()
        {
            initSet(false);
        }

        public CEASearchModel(bool IsSetDate)
        {
            initSet(IsSetDate);
        }

        private void initSet(bool IsSetDate)
        {
            ListData = new List<CEAListModel>();
            if (IsSetDate)
            {
                DateTime dtTo = DateTime.Now;
                DateTime dtFrom = DateHelper.DateFromMinimun(new DateTime(dtTo.Year, dtTo.Month, 1));
                DateTo = dtTo;
                DateToStr = dtTo.ToString(Constant.FORMAT_DATE_JRESOURCES);
                DateFrom = dtFrom;
                DateFromStr = dtFrom.ToString(Constant.FORMAT_DATE_JRESOURCES);
            }
        }

        public string CEANo { get; set; }
        public string RequestStatus { get; set; }
        public string CodeCostCenter { get; set; }
        public string Reason { get; set; }
        public Guid CompanyId { get; set; }
        public Guid DepartmentId { get; set; }
        public string IssuedType { get; set; }
        public string EquipmentNo { get; set; }
        public string MaintenanceCode { get; set; }
        public string ItemNo { get; set; }
        public string IssuedNo { get; set; }

        public bool IsLookup { get; set; }
        public bool IsReOrderPoint { get; set; }
        public bool IsRequisition { get; set; }
        public bool IsRecentActivity { get; set; }

        public string WorkOrderNo { get; set; }
        public List<CEAListModel> ListData { get; set; }

        /// <summary>
        /// Additional MSR To Modal
        /// </summary>
        public Guid CEAIdModal { get; set; }
    }
    public class CBESearchModel : SearchModel
    {
        public CBESearchModel()
        {
            initSet(false);
        }

        public CBESearchModel(bool IsSetDate)
        {
            initSet(IsSetDate);
        }
        private void initSet(bool IsSetDate)
        {
            ListData = new List<CBEListmodel>();
            if (IsSetDate)
            {
                DateTime dtTo = DateTime.Now;
                DateTime dtFrom = DateHelper.DateFromMinimun(new DateTime(dtTo.Year, dtTo.Month, 1));
                DateTo = dtTo;
                DateToStr = dtTo.ToString(Constant.FORMAT_DATE_JRESOURCES);
                DateFrom = dtFrom;
                DateFromStr = dtFrom.ToString(Constant.FORMAT_DATE_JRESOURCES);
            }
        }
        public string CBENo { get; set; }
        public string MSRNo { get; set; }
        public string RequestStatus { get; set; }
        public Guid CompanyId { get; set; }
        public Guid DepartmentId { get; set; }
        public List<CBEListmodel> ListData { get; set; }
    }
    public class SupplierSearchModel : SearchModel
    {
        public SupplierSearchModel()
        {
            ListData = new List<MSRSupplier>();
        }

        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public List<MSRSupplier> ListData { get; set; }
        public bool IsGetAllItem { get; set; }
        public List<SupplierDetail> suggestions { get; set; }

    }
    public class SupplierDetail
    {
        public string value { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
    }
    public class CurrencySearchModel : SearchModel
    {
        public CurrencySearchModel()
        {
            ListData = new List<CurrencyModel>();
        }

        public string CurrencyCode { get; set; }
        public string CurrencyName { get; set; }
        public List<CurrencyModel> ListData { get; set; }
        public bool IsGetAllItem { get; set; }
        public List<CurrencyDetail> suggestions { get; set; }

    }
    public class CurrencyDetail
    {
        public string value { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyName { get; set; }
    }
}
