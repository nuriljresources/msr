﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using JResources.Common;


namespace JResources.Data.Model
{
    //public class CEAModel
    //{
    //    public CEAModel()
    //    {
    //        DateCreated = DateTime.Now;
    //        DateCreatedStr = DateTime.Now.ToString(Constant.FORMAT_DATE_JRESOURCES);
    //        CEADetail = new List<CEADetailModel>();
    //        Requestor = new UserModel();

    //        Manager = new UserModel();
    //        FunctionalGeneralManager = new UserModel();
    //        GeneralManagerSite = new UserModel();
    //        FunctionalDirector = new UserModel();
    //        FinanceManager = new UserModel();

    //        DateCreated = DateTime.Now;
    //    }

    //    public void SetDateFrom()
    //    {
    //        if (!string.IsNullOrEmpty(DateCreatedStr))
    //        {
    //            DateCreated = CommonFunction.DateTimeJResources(DateCreatedStr);
    //        }

    //        if (!string.IsNullOrEmpty(ExpectedDateStr))
    //        {
    //            ExpectedDate = CommonFunction.DateTimeJResources(ExpectedDateStr);
    //        }
    //    }

    //    public Guid ID { get; set; }
    //    public string DocumentNo { get; set; }
    //    public string PPP { get; set; }
    //    public string CEAType { get; set; }
    //    public string CEATypeLabel { get; set; }
    //    public Guid CompanyId { get; set; }
    //    public string CompanyName { get; set; }
    //    public Guid DepartmentId { get; set; }
    //    public string DepartmentName { get; set; }
    //    public string CostCenter { get; set; }
    //    public string CostCenterLabel { get; set; }
    //    public string MaintenanceCode { get; set; }
    //    public string MaintenanceCodeLabel { get; set; }


    //    public string DateCreatedStr { get; set; }
    //    public DateTime DateCreated { get; set; }
    //    public string DateUpdateStr { get; set; }
    //    public DateTime DateUpdate { get; set; }

    //    public string ExpectedDateStr { get; set; }
    //    public DateTime ExpectedDate { get; set; }

    //    public string ApprovedDateStr { get; set; }
    //    public DateTime ApprovedDate { get; set; }
    //    public UserModel ApprovedBy { get; set; }

    //    public string WorkOrderNo { get; set; }
    //    public string EquipmentNo { get; set; }
    //    public string Reference { get; set; }
    //    public string Reason { get; set; }
    //    public string StatusRequest { get; set; }
    //    public string StatusRequestLabel { get; set; }

    //    public LeadTimeModel LeadTime { get; set; }
    //    public int LeadTimeNumber { get; set; }

    //    public UserModel Requestor { get; set; }
    //    public UserModel Manager { get; set; }
    //    public UserModel FunctionalGeneralManager { get; set; }
    //    public UserModel GeneralManagerSite { get; set; }
    //    public UserModel FunctionalDirector { get; set; }
    //    public UserModel FinanceManager { get; set; }
    //    public decimal TotalWithApprovalGM { get; set; }

    //    public bool IsNeedActionRequest { get; set; }
    //    public bool IsWaitingApprovelBefore { get; set; }
    //    public decimal TotalAmount { get; set; }

    //    /// <summary>
    //    /// Additional Property
    //    /// </summary>
    //    public int WorkOrderStatus { get; set; }
    //    public string WorkOrderStatusLabel { get; set; }

    //    public List<CEADetailModel> CEADetail { get; set; }


    //    public decimal TotalInStock { get; set; }
    //    public decimal TotalBudgeted { get; set; }
    //    public decimal TotalUnBudgeted { get; set; }
    //    public void SetNewRequest()
    //    {
    //        StatusRequest = Common.Enum.RequestStatus.NEW;
    //    }
    //}

    public class CEADetailModel
    {
        public CEADetailModel()
        {
            ItemLocations = new List<ItemLocationModel>();
        }

        public Guid ID { get; set; }
        public int LineNo { get; set; }
        public string EquipmentNo { get; set; }
        public string ItemNo { get; set; }
        public string UOM { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public string LocationFrom { get; set; }
        public string PickingSeq { get; set; }
        public decimal Qty { get; set; }
        public decimal QtyUnloading { get; set; }
        public decimal Cost { get; set; }
        public decimal StockOnHand { get; set; }
        public string Images { get; set; }
        public string CostCode { get; set; }
        public string CostCodeLabel { get; set; }
        public string Category { get; set; }
        public string CategoryLabel { get; set; }
        public bool IsDeleted { get; set; }

        public List<ItemLocationModel> ItemLocations { get; set; }

        public string IdLookup { get; set; }

        /// <summary>
        /// For Budget Calculation
        /// </summary>

        public string BudgetLabel { get; set; }

        // For Budget Validation
        public decimal QtyBudget { get; set; }
        public decimal SubTotalBudget { get; set; }


        // History Purchase
        public decimal QtyBudgetHistory { get; set; }
        public decimal SubTotalHistory { get; set; }
    }

    public class CEAListModel
    {
        public String sys_id { get; set; }
        public string cer_draft_number { get; set; }
        public string cer_number { get; set; }
        public string cea_number { get; set; }

        public Guid ID { get; set; }
        public string DocumentNo { get; set; }
        public string WorkOrderNo { get; set; }
        public string DepartmentName { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? CERDate { get; set; }
        public DateTime? CEADate { get; set; }
        public string CostCenter { get; set; }
        public string MaintenanceCode { get; set; }
        public DateTime ExpectedDate { get; set; }
        public string Reason { get; set; }
        public string EquipmentNo { get; set; }
        public string StatusRequestCode { get; set; }
        public string StatusRequestLabel { get; set; }
        public string Requestor { get; set; }
        public string NextApproval { get; set; }

        public string DateCreatedStr { get; set; }
        public string ExpectedDateStr { get; set; }

        public bool IsCanEdit { get; set; }
        public string RowCssColor { get; set; }
        public string LabelCssColor { get; set; }

        public bool IsDoneRequisition { get; set; }
        public bool IsCanRequisition { get; set; }

        public bool IsDoneUnloading { get; set; }
        public bool IsUnloading { get; set; }
        public bool IsSync { get; set; }
    }

    public class CEAModel
    {
        public CEAModel()
        {
            CEAItems = new List<CEAItemModel>();
            CEAAdditionalItems = new List<CEAItemModel>();
            History = new List<CEARequistionModel>();
        }

        public Guid CEAId { get; set; }
        public string CEANo { get; set; }
        public decimal TotalAmount { get; set; }
        public string TotalAmountStr
        {
            get
            {
                return this.TotalAmount.ToString("N2");
            }
        }
        public decimal TotalAmountRQN { get; set; }
        public string TotalAmountRQNStr
        {
            get
            {
                return this.TotalAmountRQN.ToString("N2");
            }
        }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string DepartmentName { get; set; }
        public DateTime CEADate { get; set; }
        public string CEADateStr { get; set; }
        public DateTime RQNDate { get; set; }
        public string WorkFlow { get; set; }
        public bool IsInventory { get; set; }
        public string Status { get; set; }
        public string CostCenter { get; set; }
        public string Remarks { get; set; }

        public string CreatedByCEA { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDateStr { get; set; }

        public List<CEAItemModel> CEAItems { get; set; }
        public List<CEAItemModel> CEAAdditionalItems { get; set; }

        public List<CEARequistionModel> History { get; set; }

        
    }

    public class CEAItemModel
    {
        public string IdLookup { get; set; }
        public Guid ID { get; set; }
        public Guid ReferenceId { get; set; }
        public int LineNo { get; set; }
        public string Type { get; set; }
        public string ItemNo { get; set; }
        public string ItemDescription { get; set; }

        public string GLAccount { get; set; }
        public string CCCODE { get; set; }
        public string UOM { get; set; }
        public decimal UnitCost { get; set; }
        public decimal UnitCostLimit { get; set; }
        public string Location { get; set; }
        public decimal QtyRequest { get; set; }
        public decimal QtyOutstanding { get; set; }
        public decimal Qty { get; set; }

        public bool IsAdditional { get; set; }
        public bool IsExclude { get; set; }
        public bool IsContract { get; set; }
        public bool IsReference { get; set; }
        public bool IsCompleted { get; set; }
    }

    public class CEARequistionModel
    {
        public CEARequistionModel()
        {
            Items = new List<CEARequistionItemModel>();
        }

        public string RequisitionNo { get; set; }
        public string RequisitionDate { get; set; }
        public string WorkFlowName { get; set; }
        public string RequisitionType { get; set; }

        public List<CEARequistionItemModel> Items { get; set; }
    }

    public class CEARequistionItemModel
    {
        public int LineNo { get; set; }
        public string ItemNo { get; set; }
        public string ItemDescription { get; set; }
        public string ItemType { get; set; }
        public decimal Qty { get; set; }
        public decimal UnitCost { get; set; }
    }
}

