﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class WorkOrderModel
    {
        public decimal WorkOrderId { get; set; }
        public string WorkOrderNo { get; set; }
        public string MaintainType { get; set; }
        public decimal AuditDateDecimal { get; set; }

        public bool IsUseComplete { get; set; }
    }
}
