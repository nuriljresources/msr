﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model.Report
{
    public class MSRStatusModel
    {
        public string MSRNo { get; set; }
        public string HeaderStatusLabel { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ApproveDate { get; set; }
        public DateTime? FullfillmentDate { get; set; }
        public DateTime ExpectedDate { get; set; }
        public DateTime? DismantleDate { get; set; }
        public DateTime? RequisitionDate { get; set; }

        public string MaintenanceCode { get; set; }
        public int LeadTime { get; set; }
        public string WorkOrderNo { get; set; }
        public string DepartmentName { get; set; }
        public string CostCode { get; set; }
        public string EquipmentNo { get; set; }
        public int LineNo { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string UOM { get; set; }
        public decimal QtyRequest { get; set; }
        public decimal QtyTransfer { get; set; }
        public decimal QtyReverseTransfer { get; set; }
        public decimal QtyCancel { get; set; }
        public decimal QtyPurchasing { get; set; }
        public decimal QtyIssued { get; set; }
        public DateTime DateTransfer { get; set; }
        public DateTime DateCancel { get; set; }
        public DateTime DatePurchasing { get; set; }
        public DateTime DateIssued { get; set; }
        public string DetailStatusLabel { get; set; }
        public string Requestor { get; set; }
        public string Remark { get; set; }
    }

    public class MSRStatusDetailModel
    {
        public MSRStatusDetailModel()
        {
            DateTransfer = DateTime.MinValue;
            DateReverseTransfer = DateTime.MinValue;
            DateCancel = DateTime.MinValue;
            DatePurchasing = DateTime.MinValue;
            DateIssued = DateTime.MinValue;
        }

        public int LineNo { get; set; }
        public string ItemNo { get; set; }

        public decimal QtyRequest { get; set; }
        public decimal QtyTransfer { get; set; }
        public decimal QtyReverseTransfer { get; set; }
        public decimal QtyCancel { get; set; }
        public decimal QtyPurchasing { get; set; }
        public decimal QtyIssued { get; set; }

        public DateTime DateTransfer { get; set; }
        public DateTime DateReverseTransfer { get; set; }
        public DateTime DateCancel { get; set; }
        public DateTime DatePurchasing { get; set; }
        public DateTime DateIssued { get; set; }
    }

}
