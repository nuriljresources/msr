﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model.Report
{
    public class ReportComparisonModel
    {
        public ReportComparisonModel()
        {
            Details = new List<ReportComparisonDetailModel>();
        }

        public string DateFromStr { get; set; }
        public DateTime DateFrom { get; set; }
        public string DateToStr { get; set; }
        public DateTime DateTo { get; set; }
        public string WorkOrderNo { get; set; }
        public string MSRNo { get; set; }
        public List<ReportComparisonDetailModel> Details { get; set; }
    }

    public class ReportComparisonDetailModel
    {
        public string WorkOrderNo { get; set; }
        public string MSRNo { get; set; }
        public DateTime MSRDate { get; set; }
        public string ItemNo { get; set; }
        public string ItemDescription { get; set; }

        public decimal QtyPost { get; set; }
        public string DocumentType { get; set; }
        public string DocumentTransactionNo { get; set; }
        public string DocumentTransactionDate { get; set; }

        public decimal QtyPosted { get; set; }
        public decimal QtyDiff { get; set; }

    }
}
