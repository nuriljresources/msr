﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class MSRIssuedModel
    {
        public MSRIssuedModel()
        {
            MSRIssuedDetails = new List<MSRIssuedDetailModel>();
            MSR = new MSRModel();
            MSRSupplierDetails = new List<MSRSupplier>();
            MSRCurrencyDetails = new List<MSRCurrency>();
            CBEDocumentDetails = new List<CBEDocumentModel>();
        }

        public Guid ID { get; set; }
        public string MSRIssuedNo { get; set; }
        public Guid CompanyId { get; set; }
        public string CompanyName { get; set; }
        public Guid DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string CostCenter { get; set; }

        public DateTime MSRIssuedDate { get; set; }
        public string MSRIssuedDateStr { get; set; }
        
        public string IssuedType { get; set; }
        public string IssuedTypeLabel { get; set; }
        public string Remarks { get; set; }
        public bool IsTransferAccpac { get; set; }
        public bool IsCanReverse { get; set; }
        public bool IsNonInventory { get; set; }

        public Guid MSRId { get; set; }
        public string MSRNo { get; set; }
        public string RQNNumber { get; set; }
        public string WorkOrderNo { get; set; }
        public decimal TotalCost { get; set; }
        public bool isAcceptable { get; set; }
        public string AcceptableStr { get; set; }

        public List<MSRIssuedDetailModel> MSRIssuedDetails { get; set; }
        public List<MSRSupplier> MSRSupplierDetails { get; set; }
        public List<MSRCurrency> MSRCurrencyDetails { get; set; }
        public List<CBEDocumentModel> CBEDocumentDetails { get; set; }
        public MSRModel MSR { get; set; }
        public UserModel Creator { get; set; }
        public string WorkFlowCode { get; set; }
        public string GLAccount { get; set; }

        #region Cancel Approval Additional
        public bool IsApproveCancelForm { get; set; }
        public string CancelRequest { get; set; }
        #endregion
    }

    public class MSRIssuedDetailModel : MSRDetailModel
    {
        public MSRIssuedDetailModel()
        {
            CBEBidSuppliers = new List<CBEBidSupplierModel>();
        }
        public Guid MSRIssuedId { get; set; }
        public decimal QtyDelivered { get; set; }
        public decimal QtyProcess { get; set; }
        public decimal QtyRequest { get; set; }
        public decimal QtyIssued { get; set; }
        public decimal QtyCancel { get; set; }
        public decimal QtyPurchase { get; set; }
        public decimal QtyTransfered { get; set; }

        public decimal QtyIssuedOutstanding
        {
            get
            {
                return QtyRequest - (QtyDelivered + QtyCancel);
            }
        }

        public decimal QtyTransferOutstanding
        {
            get
            {
                return QtyRequest - (QtyTransfered + QtyCancel);
            }
        }

        public decimal QtyPurchaseOutstanding
        {
            get
            {
                return QtyRequest - (QtyDelivered + QtyCancel + QtyPurchase);
            }
        }

        public decimal QtyCancelOutstanding
        {
            get
            {
                decimal Qty = QtyDelivered;
                if (QtyDelivered != QtyTransfered)
                {
                    if (QtyDelivered > QtyTransfered)
                    {
                        Qty = QtyDelivered;
                    }
                    else
                    {
                        Qty = QtyTransfered;
                    }
                }

                return QtyRequest - (Qty + QtyCancel);
            }
        }

        public bool Isfulfilled { get; set; }
        public bool IsRemove { get; set; }
        public bool IsView { get; set; }
        public string GLAccount { get; set; }
        public List<CBEBidSupplierModel> CBEBidSuppliers { get; set; }
        
    }
    public class MSRSupplier
    {
        public string Supplier { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string Currency { get; set; }
        public string AddSupplierName { get; set; }
        public string AddSupplierContactPerson { get; set; }
        public string AddSupplierAddress { get; set; }
        public string AddSupplierPayment { get; set; }
    }
    public class MSRCurrency
    {
        public string Currency { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyName { get; set; }
    }
    public class MSRIssuedListmodel
    {
        public Guid ID { get; set; }
        public string MSRIssuedNo { get; set; }
        public Guid MSRId { get; set; }
        public string MSRNo { get; set; }
        public string RQNNo { get; set; }

        public DateTime MSRIssuedDate { get; set; }
        public string MSRIssuedDateStr { get; set; }
        public string MSRIssuedTimeStr { get; set; }

        public Guid CompanyId { get; set; }
        public string CompanyName { get; set; }
        public Guid DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string Requestor { get; set; }
        public string IssuedType { get; set; }
        public string Remarks { get; set; }


        public bool IsPurchased { get; set; }
        public string LabelCssColor { get; set; }
        public bool IsFailedPost { get; set; }

        public string LogMessage { get; set; }
    }

}
