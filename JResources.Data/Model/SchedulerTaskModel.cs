﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class SchedulerTaskModel
    {
        public int ID { get; set; }
        public Guid CompanyId { get; set; }
        public string DocumentNo { get; set; }
        public string DocumentRefNo { get; set; }
        public string DocumentType { get; set; }

        public string SchedulerType { get; set; }
        public string TypeCssColor { get; set; }

        public int SchedulerStatus { get; set; }
        public string StatusLabel { get; set; }
        public string StatusCssColor { get; set; }

        public int TryCount { get; set; }

        public DateTime CreatedDate { get; set; }
        public string CreatedDateStr { get; set; }
        public string CreatedTimeStr { get; set; }

        public DateTime? ManualPostDate { get; set; }
        public string ManualPostDateStr { get; set; }
        public string ManualPostTimeStr { get; set; }

        public string LogMessage { get; set; }
        public string LogDataJson { get; set; }
        public bool IsEnablePost { get; set; }
        
        
    }
}

