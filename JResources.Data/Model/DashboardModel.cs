﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class DashboardModel
    {
        public DashboardModel()
        {
            Activities = new List<ActivityModel>();
        }

        public int TOTAL_NEW_REQUEST { get; set; }
        public int TOTAL_APPROVED_REQUEST { get; set; }
        public int TOTAL_TRANSFER_WAREHOUSE { get; set; }
        public int TOTAL_PENDING_ACCPAC { get; set; }

        public List<ActivityModel> Activities { get; set; }
    }

    public class ActivityModel
    {
        public string ActivityType { get; set; }
        public string ActivityIcon { get; set; }

        public Guid UserId { get; set; }
        public string Fullname { get; set; }
        public string DocumentNo { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
