﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class LeadTimeModel
    {
        public string MaintenanceType { get; set; }
        public bool IsFreeNumber { get; set; }
        public int MinNumber { get; set; }
        public int MaxNumber { get; set; }
    }
}
