﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class RoleDelegationModel
    {
        public RoleDelegationModel()
        {
            DelegationRoleAccessList = new List<DelegationRoleAccess>();
        }

        public Guid ID { get; set; }
        public Guid UserId { get; set; }
        public string FullName { get; set; }
        public RoleAccessModel roleAccess { get; set; }
        public CompanyModel company { get; set; }
        public DepartmentModel department { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }

        public string DateFromStr { get; set; }
        public string DateToStr { get; set; }

        public List<DelegationRoleAccess> DelegationRoleAccessList { get; set; }
    }

    public class DelegationRoleAccess : RoleAccessModel
    {
        public DelegationRoleAccess()
        {
            Companies = new List<CompanyDepartmentModel>();
        }

        public List<CompanyDepartmentModel> Companies { get; set; }
    }
}
