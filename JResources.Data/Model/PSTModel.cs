﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class PSTModel : ResponseModel
    {
        public PSTModel()
        {
            PSTDetails = new List<PSTDetailModel>();
            PIC1 = new UserModel();
            PIC2 = new UserModel();
        }

        public Guid ID { get; set; }
        public string PSTNo { get; set; }
        public DateTime PSTDate { get; set; }
        public string PSTDateStr { get; set; }
        public string PSTType { get; set; }
        public string PSTStatus { get; set; }
        public string ReferenceRandom { get; set; }
        public string StorageLocation { get; set; }
        public bool IsZeroLoc { get; set; }

        public string PIC1Entry { get; set; }
        public string PIC2Entry { get; set; }
        public UserModel PIC1 { get; set; }
        public UserModel PIC2 { get; set; }

        public bool IsUse { get; set; }
        public bool IsDeleted { get; set; }

        public List<PSTDetailModel> PSTDetails { get; set; }

        public void SetDateString()
        {
            if (!string.IsNullOrEmpty(PSTDateStr))
            {
                var dateConvert = Common.CommonFunction.DateTimeJResources(PSTDateStr);
                if (dateConvert != DateTime.MinValue)
                {
                    PSTDate = Common.DateHelper.DateFromMinimun(dateConvert);
                }
            }
        }
    }

    public class PSTDetailModel
    {
        public Guid ID { get; set; }
        public Guid TransId { get; set; }
        public bool IsUse { get; set; }
        public string ItemNo { get; set; }
        public string ItemDescription { get; set; }
        public string UOM { get; set; }
        public string Location { get; set; }
        public string BinLocation { get; set; }
        public decimal QtyActual { get; set; }
        public decimal QtyStockCard { get; set; }
        public string FinalRemark { get; set; }
        public bool IsDeleted { get; set; }
    }
}
