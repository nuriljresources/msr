﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class NotificationModel
    {
        public UserModel UserCreated { get; set; }
        public string Icon { get; set; }
        public string Area { get; set; }
        public string TargetUrl { get; set; }
    }
}
