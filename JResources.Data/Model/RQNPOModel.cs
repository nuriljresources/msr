﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class RQNPOModel
    {
        public string PONUMBER { get; set; }
        public short ONHOLD { get; set; }
        public string CURRENCY { get; set; }
        public decimal RATE { get; set; }
        public string rqnnumber { get; set; }
        public string CEAno { get; set; }
        public short COMPLETION { get; set; }
        public decimal DTCOMPLETE { get; set; }
        public string ITEMNO { get; set; }
        public string LOCATION { get; set; }
        public string ITEMDESC { get; set; }
        public string ORDERUNIT { get; set; }
        public decimal OQORDERED { get; set; }
        public decimal OQRECEIVED { get; set; }
        public decimal OQCANCELED { get; set; }
        public decimal UNITCOST { get; set; }
    }
}
