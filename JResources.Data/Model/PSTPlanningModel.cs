﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class PSTPlanningModel
    {
        public PSTPlanningModel()
        {
            PSTPlanningScheduler = new List<PSTPlanningSchedulerModel>();
            PSTPlanningDetails = new List<PSTPlanningDetailModel>();
            DateFrom = DateTime.MinValue;
            DateTo = DateTime.MinValue;
            CreatedDate = DateTime.MinValue;
            UpdatedDate = DateTime.MinValue;
        }

        public Guid ID { get; set; }
        public string PlanningNo { get; set; }
        public Guid CompanyId { get; set; }
        public DateTime DateFrom { get; set; }
        public string DateFromStr { get; set; }

        public DateTime DateTo { get; set; }
        public string DateToStr { get; set; }

        public bool IsEdit { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedDateStr { get; set; }

        public string UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedDateStr { get; set; }
        public List<PSTPlanningDetailModel> PSTPlanningDetails { get; set; }
        public List<PSTPlanningSchedulerModel> PSTPlanningScheduler { get; set; }

        public bool IsUse { get; set; }
        public int TotalItem { get; set; }
        #region Date set
        public void SetDateString()
        {
            if (DateFrom != DateTime.MinValue)
            {
                DateFromStr = DateFrom.ToString(JResources.Common.Constant.FORMAT_DATE_JRESOURCES);
            }

            if (DateTo != DateTime.MinValue)
            {
                DateToStr = DateTo.ToString(JResources.Common.Constant.FORMAT_DATE_JRESOURCES);
            }

            if (CreatedDate != DateTime.MinValue)
            {
                CreatedDateStr = CreatedDate.ToString(JResources.Common.Constant.FORMAT_DATE_JRESOURCES);
            }

            if (UpdatedDate != DateTime.MinValue)
            {
                UpdatedDateStr = UpdatedDate.ToString(JResources.Common.Constant.FORMAT_DATE_JRESOURCES);
            }
        }
        #endregion
    }

    public class PSTPlanningDetailModel
    {
        public Guid ID { get; set; }
        public Guid PSTPlanningId { get; set; }
        public string ItemNo { get; set; }
        public string ItemDescription { get; set; }
        public string Location { get; set; }
        public string Bin { get; set; }
        public string UOM { get; set; }
        public DateTime PlanningDate { get; set; }
        public bool IsUse { get; set; }
        public decimal BookedQty { get; set; }

        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }

    public class PSTPlanningSchedulerModel
    {
        public PSTPlanningSchedulerModel()
        {
            ListData = new List<PSTPlanningDetailModel>();
        }

        public DateTime PlanningDate { get; set; }
        public int TotalRow { get; set; }

        public List<PSTPlanningDetailModel> ListData { get; set; }
    }
}
