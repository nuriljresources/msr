﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class EnumTableModel
    {
        public Guid EnumTableID { get; set; }
        public string CompanyCode { get; set; }
        public string EnumCode { get; set; }
        public int Sequence { get; set; }
        public string EnumValue { get; set; }
        public bool EnumLabel { get; set; }
        public bool IsDeleted { get; set; }

    }

}

