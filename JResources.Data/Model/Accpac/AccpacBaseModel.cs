﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model.Accpac
{
    public class AccpacBaseModel
    {
        public string DBUserName { get; set; }
        public string DBPassword { get; set; }
        public string DBName { get; set; }
        public string docNum { get; set; }
    }
}
