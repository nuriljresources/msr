﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model.Accpac
{
    public class PO0620 : AccpacPostBaseModel
    {
        public PO0620()
        {
            PO0630List = new List<PO0630>();
        }

        public string PONumber { get; set; }
        public string VDCODE { get; set; }
        public string VDADDRESS1 { get; set; }
        public string VDADDRESS2 { get; set; }
        public string VDADDRESS3 { get; set; }
        public string VDADDRESS4 { get; set; }
        public string VDCITY { get; set; }
        public string VDSTATE { get; set; }
        public string VDZIP { get; set; }
        public string VDCOUNTRY { get; set; }
        public string VDPHONE { get; set; }
        public string VDFAX { get; set; }
        public string VDEMAIL { get; set; }
        public string VDCONTACT { get; set; }
        public string VDPHONEC { get; set; }
        public string VDFAXC { get; set; }
        public string VDEMAILC { get; set; }
        public string STCODE { get; set; }
        public string STDESC { get; set; }
        public string STADDRESS1 { get; set; }
        public string STADDRESS2 { get; set; }
        public string STADDRESS3 { get; set; }
        public string STADDRESS4 { get; set; }
        public string STCITY { get; set; }
        public string STSTATE { get; set; }
        public string STZIP { get; set; }
        public string STCOUNTRY { get; set; }
        public string STPHONE { get; set; }
        public string STFAX { get; set; }
        public string STEMAIL { get; set; }
        public string STCONTACT { get; set; }
        public string STPHONEC { get; set; }
        public string STFAXC { get; set; }
        public string STEMAILC { get; set; }
        public string BTCODE { get; set; }
        public string BTDESC { get; set; }
        public string BTADDRESS1 { get; set; }
        public string BTADDRESS2 { get; set; }
        public string BTADDRESS3 { get; set; }
        public string BTADDRESS4 { get; set; }
        public string BTCITY { get; set; }
        public string BTSTATE { get; set; }
        public string BTZIP { get; set; }
        public string BTCOUNTRY { get; set; }
        public string BTPHONE { get; set; }
        public string BTFAX { get; set; }
        public string BTEMAIL { get; set; }
        public string BTCONTACT { get; set; }
        public string BTPHONEC { get; set; }
        public string BTFAXC { get; set; }
        public string BTEMAILC { get; set; }
        public string DATE { get; set; }
        public string VDACCTSET { get; set; }
        public string PORTYPE { get; set; }
        public string WORKFLOW { get; set; }
        public string COSTCTR { get; set; }
        public string EXPARRIVAL { get; set; }
        public string FOBPOINT { get; set; }
        public string VIANAME { get; set; }
        public string TERMSCODE { get; set; }
        public string DESCRIPTIO { get; set; }
        public string REFERENCE { get; set; }
        public string LABELCOUNT { get; set; }
        public string COMMENT { get; set; }
        public int HASJOB { get; set; }
        public string OPTFIELD { get; set; }
        public string SWSET { get; set; }
        public string VALIFTEXT { get; set; }


        public string dsPurchaseOrder { get; set; }

        public List<PO0630> PO0630List { get; set; }
    }
}
