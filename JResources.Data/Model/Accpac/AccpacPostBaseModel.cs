﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model.Accpac
{
    public class AccpacPostBaseModel
    {
        public string DBUsername { get; set; }
        public string DBPassword { get; set; }
        public string DBName { get; set; }
        public string DOCNUM { get; set; }

        public int SchedulerId { get; set; }

        /// <summary>
        /// IC0645
        /// </summary>
        public string OPTFIELD { get; set; }
        public string SWSET { get; set; }
        public string VALIFTEXT { get; set; }
    }
}
