﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model.Accpac
{
    public class VM0069
    {
        public int WDTRANNUM { get; set; }
        public string ITEMNO { get; set; }
        public string TXITEM { get; set; }
        public string TXDESC { get; set; }
        public decimal QTESTIMATE { get; set; }
        public decimal QTBACKORD { get; set; }
        public decimal QTSUPPLIED { get; set; }
        public decimal QTINVOICED { get; set; }
    }
}
