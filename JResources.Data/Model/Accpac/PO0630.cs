﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model.Accpac
{
    public class PO0630
    {
        public PO0630()
        {
            IC0635List = new List<IC0635>();
        }

        public string ITEMNO { get; set; }
        public string CONTRACT { get; set; }
        public string PROJECT { get; set; }
        public string CCATEGORY { get; set; }
        public string LOCATION { get; set; }
        public string HASDROPSHI { get; set; }
        public string DROPTYPE { get; set; }
        public string DLOCATION { get; set; }
        public string IDCUST { get; set; }
        public string IDCUSTSHPT { get; set; }
        public string DESC { get; set; }
        public string ADDRESS1 { get; set; }
        public string ADDRESS2 { get; set; }
        public string ADDRESS3 { get; set; }
        public string ADDRESS4 { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string ZIP { get; set; }
        public string COUNTRY { get; set; }
        public string PHONE { get; set; }
        public string FAX { get; set; }
        public string EMAIL { get; set; }
        public string CONTACT { get; set; }
        public string PHONEC { get; set; }
        public string FAXC { get; set; }
        public string EMAILC { get; set; }
        public string ISCOMPLETE { get; set; }
        public string OQORDERED { get; set; }
        public string UNITCOST { get; set; }
        public string EXTENDED { get; set; }
        public string DISCPCT { get; set; }
        public string DISCOUNT { get; set; }
        public string WEIGHTUNIT { get; set; }
        public string UNITWEIGHT { get; set; }
        public string EXTWEIGHT { get; set; }
        public string EXPARRIVAL { get; set; }
        public string OEONUMBER { get; set; }
        public string HASCOMMENT { get; set; }
        public string COMMENT { get; set; }
        public string MANITEMNO { get; set; }
        public string OQOUTSTAND { get; set; }
        public string BILLRATE { get; set; }
        public string ARITEMNO { get; set; }
        public string ARUNIT { get; set; }

        public List<IC0635> IC0635List { get; set; }
    }
}
