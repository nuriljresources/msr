﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model.Accpac
{
    public class PT0040 : AccpacPostBaseModel
    {
        public PT0040()
        {
            PT0041List = new List<PT0041>();
        }

        public string WORKFLOW { get; set; }
        public string COSTCTR { get; set; }
        public string STCODE { get; set; }
        public string BTCODE { get; set; }
        public DateTime RQRDDATE { get; set; }
        public string DESCRIPTIO { get; set; }
        public string REFERENCE { get; set; }
        public string COMMENT { get; set; }
        public int HASJOB { get; set; }

        public List<PT0041> PT0041List { get; set; }
    }


    public class PT0041
    {
        public PT0041()
        {
            IC0635List = new List<IC0635>();
        }

        public int LineNo { get; set; }
        public string FMTITEMNO { get; set; }
        public string CONTRACT { get; set; }
        public string PROJECT { get; set; }
        public string CCATEGORY { get; set; }
        public string VDCODE { get; set; }
        public string LOCATION { get; set; }
        public string COMMENT { get; set; }
        public decimal REQQTY { get; set; }
        public string GLACCTFULL { get; set; }
        public DateTime RQRDDATE { get; set; }
        public decimal UNITCOST { get; set; }
        public string ORDERUNIT { get; set; }
        public string VENDITEMNO { get; set; }
        public string BILLRATE { get; set; }
        public string ARITEMNO { get; set; }
        public string ARUNIT { get; set; }
        public bool ISSTOCK { get; set; }
        public bool ISVENDOR { get; set; }

        public List<IC0635> IC0635List { get; set; }
    }
}
