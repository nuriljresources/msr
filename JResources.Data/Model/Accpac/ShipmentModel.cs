﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model.Accpac
{
    public class ShipmentModel : AccpacBaseModel
    {
        public ShipmentModel()
        {
            ShipmentDetails = new List<ShipmentDetailModel>();
        }

        public string description { get; set; }
        public string reference { get; set; }
        public string transDate { get; set; }
        public string dateBus { get; set; }
        public string custNo { get; set; }
        public string contact { get; set; }
        public string status { get; set; }
        public string currency { get; set; }
        public string optField { get; set; }
        public string swset { get; set; }
        public string valifText { get; set; }
        public List<ShipmentDetailModel> ShipmentDetails { get; set; }
    }

    public class ShipmentDetailModel
    {
        public string ITEMNO { get; set; }

        ////Added by Erda on 20150106
        public string CATEGORY { get; set; }

        public string LOCATION { get; set; }
        public string QUANTITY { get; set; }
        ////Added by Erda on 20150217
        public string UNIT { get; set; }
        //public string UNITPRICE { get; set; }
        public string OPTFIELD1 { get; set; }
        public string OPTFIELD2 { get; set; }
        public string OPTFIELD3 { get; set; }

        ////Added by Erda on 20150218
        public string OPTFIELD4 { get; set; }

        public string SWSET1 { get; set; }
        public string SWSET2 { get; set; }
        public string SWSET3 { get; set; }

        ////Added by Erda on 20150218
        public string SWSET4 { get; set; }

        public string VALIFTEXT1 { get; set; }
        public string VALIFTEXT2 { get; set; }
        public string VALIFTEXT3 { get; set; }

        ////Added by Erda on 20150218
        public string VALIFTEXT4 { get; set; }

        public string COMMENTS { get; set; }
        public string MANITEMNO { get; set; }
        public string FUNCTION { get; set; }
        public string LINENO { get; set; }

    }
}
