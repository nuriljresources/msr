﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model.Accpac
{
    public class PTPRDModel
    {
        public PTPRDModel()
        {
            POPORH1s = new List<POPORH1Model>();
        }

        public decimal RQNHSEQ { get; set; }        
        public decimal AUDTDATE { get; set; }
        public decimal AUDTTIME { get; set; }
        public decimal RQNLSEQ { get; set; }
        public string RQNNUMBER { get; set; }
        public string ITEMNO { get; set; }
        public string FMTITEMNO { get; set; }
        public string ITEMDESC { get; set; }
        public string LOCATION { get; set; }
        public decimal RQRDDATE { get; set; }
        public decimal REQQTY { get; set; }
        public short ISCOMPLETE { get; set; }
        public short STATUS { get; set; }
        public string PONUMBER { get; set; }
        public decimal PORLSEQ { get; set; }
        public decimal ISSUESEQ { get; set; }

        public List<POPORH1Model> POPORH1s { get; set; }
    }
}
