﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model.Accpac
{
    public class POPORH1Model
    {
        public POPORH1Model()
        {
            POPORLs = new List<POPORLModel>();
        }

        public decimal PORHSEQ { get; set; }
        public decimal AUDTDATE { get; set; }
        public decimal AUDTTIME { get; set; }
        public string AUDTUSER { get; set; }
        public string AUDTORG { get; set; }
        public decimal NEXTLSEQ { get; set; }
        public int LINES { get; set; }
        public int LINESCMPL { get; set; }
        public int TAXLINES { get; set; }
        public int RQNS { get; set; }
        public int RQNSCMPL { get; set; }
        public short ISPRINTED { get; set; }
        public short TAXAUTOCAL { get; set; }
        public short LABELPRINT { get; set; }
        public short LABELCOUNT { get; set; }
        public short ISCOMPLETE { get; set; }
        public decimal DTCOMPLETE { get; set; }
        public decimal POSTDATE { get; set; }
        public decimal DATE { get; set; }
        public string PONUMBER { get; set; }
        public string TEMPLATE { get; set; }
        public string FOBPOINT { get; set; }
        public string VDCODE { get; set; }
        public short VDEXISTS { get; set; }
        public string VDNAME { get; set; }
        public string VDADDRESS1 { get; set; }
        public string VDADDRESS2 { get; set; }
        public string VDADDRESS3 { get; set; }
        public string VDADDRESS4 { get; set; }
        public string VDCITY { get; set; }
        public string VDSTATE { get; set; }
        public string VDZIP { get; set; }
        public string VDCOUNTRY { get; set; }
        public string VDPHONE { get; set; }
        public string VDFAX { get; set; }
        public string VDCONTACT { get; set; }
        public string TERMSCODE { get; set; }
        public short HASRQNDATA { get; set; }
        public short PORTYPE { get; set; }
        public short ONHOLD { get; set; }
        public decimal ORDEREDON { get; set; }
        public decimal EXPARRIVAL { get; set; }
        public decimal VCORIGINAL { get; set; }
        public decimal VCAVAILABL { get; set; }
        public string DESCRIPTIO { get; set; }
        public string REFERENCE { get; set; }
        public string COMMENT { get; set; }
        public string VIACODE { get; set; }
        public string VIANAME { get; set; }
        public string LASTRECEIP { get; set; }
        public decimal RCPDATE { get; set; }
        public short RCPCOUNT { get; set; }
        public string CURRENCY { get; set; }
        public decimal RATE { get; set; }
        public decimal TAXAMOUNT { get; set; }
        public short DOCSOURCE { get; set; }
        public string VDEMAIL { get; set; }
        public string VDPHONEC { get; set; }
        public string VDFAXC { get; set; }
        public string VDEMAILC { get; set; }
        public decimal DISCPCT { get; set; }
        public decimal DISCOUNT { get; set; }
        public int VALUES { get; set; }
        public string RQNNUMBER { get; set; }
        public decimal RQNHSEQ { get; set; }

        public List<POPORLModel> POPORLs { get; set; }
    }
}
