﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model.Accpac
{
    public class TransferModel : AccpacBaseModel
    {
        public TransferModel()
        {
            TransferDetails = new List<TransferDetailModel>();
        }

        public string description { get; set; }
        public string reference { get; set; }
        public string transDate { get; set; }
        public string dateBus { get; set; }
        public string exparDate { get; set; }
        public string addCost { get; set; }
        public string prorMethod { get; set; }
        public string status { get; set; }
        public List<TransferDetailModel> TransferDetails { get; set; }
    }

    public class TransferDetailModel
    {
        public string ITEMNO { get; set; }
        public string FROMLOC { get; set; }
        public string TOLOC { get; set; }
        public string QTYREQ { get; set; }
        public string QUANTITY { get; set; }
        public string UNITREQ { get; set; }
        public string UNIT { get; set; }
        public string COMMENTS { get; set; }
        public string MANITEMNO { get; set; }
        public string EXTWEIGHT { get; set; }
        public string FUNCTION { get; set; }
    }
}
