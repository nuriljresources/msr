﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model.Accpac
{
    public class AccpacInfoModel
    {
        public string CompanyName { get; set; }
        public string HomeCurrency { get; set; }
        public string LegalName { get; set; }
        public string ACCPACVersion { get; set; }
        public string UserID { get; set; }
        public string UserLanguage { get; set; }
        
    }
}
