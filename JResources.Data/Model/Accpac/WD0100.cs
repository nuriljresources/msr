﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model.Accpac
{
    public class WD0100 : AccpacPostBaseModel
    {
        public string RQNNUMBER { get; set; }
        public string DOCPATH { get; set; }
        public string DOCDESC { get; set; }
    }
}
