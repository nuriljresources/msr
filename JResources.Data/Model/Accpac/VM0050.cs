﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model.Accpac
{
    public class VM0050 : AccpacPostBaseModel
    {
        public VM0050()
        {
            VM0069List = new List<VM0069>();
        }

        public string TXDOCID { get; set; }
        public short WDDOCTYPE { get; set; }

        public List<VM0069> VM0069List { get; set; }
    }
}
