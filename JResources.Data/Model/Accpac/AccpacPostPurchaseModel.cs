﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model.Accpac
{
    public class AccpacPostPurchaseModel : AccpacPostBaseModel
    {
        public string WorkFlow { get; set; }
        public string Sequence { get; set; }
        public string Comment { get; set; }
        public string ApproveStatus { get; set; }
    }
}
