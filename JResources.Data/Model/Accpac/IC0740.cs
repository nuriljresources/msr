﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model.Accpac
{
    public class IC0740 : AccpacPostBaseModel
    {
        public IC0740()
        {
            IC0730List = new List<IC0730>();
        }

        public string DOCNUM { get; set; }
        public string PROCESSCMD { get; set; }
        public DateTime TRANSDATE { get; set; }
        public DateTime DATEBUS { get; set; }
        public DateTime EXPARDATE { get; set; }
        public string HDRDESC { get; set; }
        public string REFERENCE { get; set; }
        public string ADDCOST { get; set; }
        public string PRORMETHOD { get; set; }
        public string STATUS { get; set; }

        public List<IC0730> IC0730List { get; set; }
    }
}
