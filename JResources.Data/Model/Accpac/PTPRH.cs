﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model.Accpac
{
    public class PTPRHModel
    {
        public PTPRHModel()
        {
            PTPRDs = new List<PTPRDModel>();
        }

        public decimal REQDATE { get; set; }
        public string RQNNUMBER { get; set; }
        public decimal RQNHSEQ { get; set; }
        public decimal AUDTDATE { get; set; }
        public decimal AUDTTIME { get; set; }

        public List<PTPRDModel> PTPRDs { get; set; }
    }
}
