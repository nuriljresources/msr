﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model.Accpac
{
    public class AccpacVMDocument
    {
        public string WDTRANNUM { get; set; }
        public string TXITEM { get; set; }
        public string TXDESC { get; set; }
        public decimal QTESTIMATE { get; set; }
        public decimal QTBACKORD { get; set; }
        public decimal QTSUPPLIED { get; set; }
    }
}
