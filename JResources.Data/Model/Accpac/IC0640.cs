﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model.Accpac
{
    public class IC0640 : AccpacPostBaseModel
    {
        public IC0640()
        {
            IC0630List = new List<IC0630>();
        }

        public string HDRDESC { get; set; }
        public string REFERENCE { get; set; }
        public DateTime TRANSDATE { get; set; }
        public DateTime DATEBUS { get; set; }
        public string CUSTNO { get; set; }
        public string CONTACT { get; set; }
        public string STATUS { get; set; }
        public string CURRENCY { get; set; }

        public List<IC0630> IC0630List { get; set; }
    }

    public class IC0630
    {
        public IC0630()
        {
            IC0635List = new List<IC0635>();
        }

        public int LINENO { get; set; }
        public string ITEMNO { get; set; }
        public string PROCESSCMD { get; set; }
        public string CATEGORY { get; set; }
        public string LOCATION { get; set; }
        public decimal QUANTITY { get; set; }
        public string UNIT { get; set; }

        public string COMMENTS { get; set; }
        public string MANITEMNO { get; set; }
        public string FUNCTION { get; set; }

        public List<IC0635> IC0635List { get; set; }
    }

    public class IC0635
    {
        public string OPTFIELD { get; set; }
        public string SWSET { get; set; }
        public string VALIFTEXT { get; set; }
    }

    public class IC0RCV
    {
        public IC0RCV()
        {
            ICRCVList = new List<ICRCVItem>();
        }

        public string DBUserName { get; set; } 
        public string DBPassword { get; set; } 
        public string DBName { get; set; } 
        public string receiptNo { get; set; } 
        public string receiptType { get; set; } 
        public string desc { get; set; } 
        public string reference { get; set; } 
        public DateTime receiptDate { get; set; } 
        public DateTime postingDate { get; set; } 
        public string PONo { get; set; } 
        public string vendorNo { get; set; } 
        public string receiptCurr { get; set; } 
        public string receiptRate { get; set; } 
        public string addCostCurr { get; set; } 
        public string addCost { get; set; }

        public string do_no { get; set; }

        public List<ICRCVItem> ICRCVList { get; set; }
    }

    public class ICRCVItem
    {
        public string itemNo { get; set; }
        public string FMItemNo { get; set; }

        public string itemLocation { get; set; } 
        public string qtyReceipt { get; set; } 
        public string UOM { get; set; } 
        public string unitCost { get; set; } 
        public string labels { get; set; } 
        public string comments { get; set; } 
        public string manItemNo { get; set; } 
        public string optField1 { get; set; } 
        public string optField1ValSet { get; set; } 
        public string optField1Val { get; set; } 
        public string optField2 { get; set; } 
        public string optField2ValSet { get; set; } 
        public string optField2Val { get; set; } 
        public string optField3 { get; set; } 
        public string optField3ValSet { get; set; } 
        public string optField3Val { get; set; }
    }
}
