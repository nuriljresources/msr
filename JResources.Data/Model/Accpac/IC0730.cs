﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model.Accpac
{
    public class IC0730
    {
        public int LINENO { get; set; }
        public string ITEMNO { get; set; }
        public string FROMLOC { get; set; }
        public string TOLOC { get; set; }
        public decimal QTYREQ { get; set; }
        public decimal QUANTITY { get; set; }
        public string UNITREQ { get; set; }
        public string UNIT { get; set; }
        public string COMMENTS { get; set; }
        public string MANITEMNO { get; set; }
        public string EXTWEIGHT { get; set; }
        public string FUNCTION { get; set; }
    }
}
