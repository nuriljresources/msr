﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model.Accpac
{
    public class PT0030 : AccpacPostBaseModel
    {
        public string WORKFLOW { get; set; }
        public string SEQUENCE { get; set; }
        public string COMMENT { get; set; }
        public string APPRSTATUS { get; set; }
    }
}
