﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class DepartmentModel
    {
        public DepartmentModel()
        {
            Company = new CompanyModel();
        }

        public Guid ID { get; set; }
        public Guid CompanyId { get; set; }
        public string DepartmentName { get; set; }
        public string MaintenanceTypeCode { get; set; }
        public string CategoryCode { get; set; }
        public string MappingCostCenter { get; set; }

        public bool IsHaveNonInventory { get; set; }
        public bool IsDeleted { get; set; }

        public CompanyModel Company { get; set; }

        public List<string> CostCenterCode
        {
            get
            {
                List<string> list = new List<string>();
                if (!string.IsNullOrEmpty(MappingCostCenter))
                {
                    var splitCode = MappingCostCenter.Split('|');
                    list = splitCode.Where(x => !x.Contains("~")).ToList();
                }

                return list;
            }
        }


        public List<string> CostCenterCodeExclude
        {
            get
            {
                List<string> list = new List<string>();
                if (!string.IsNullOrEmpty(MappingCostCenter))
                {
                    var splitCode = MappingCostCenter.Split('|');
                    list = splitCode.Where(x => x.Contains("~")).ToList();
                }

                return list;
            }
        }

    }

}

