﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class SchedulerLogModel
    {
        public Guid ID { get; set; }
        public string DocumentNo { get; set; }
        public bool IsSuccess { get; set; }
        public string LogMessage { get; set; }
        public string Data { get; set; }
        public string Exception { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
