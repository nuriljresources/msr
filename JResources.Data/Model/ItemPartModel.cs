﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class ItemPartModel
    {
        public ItemPartModel()
        {
            ItemLocations = new List<ItemLocationModel>();
        }

        public string ItemNo { get; set; }
        public string UOM { get; set; }
        public string Category { get; set; }
        public string CategoryLabel { get; set; }
        public string Description { get; set; }

        public decimal _totalQtyOnHand;
        public decimal TotalQtyOnHand
        {
            get
            {
                decimal totalQtyOnHand = 0;
                if (_totalQtyOnHand == 0 && ItemLocations != null && ItemLocations.Count > 0)
                {
                    totalQtyOnHand = ItemLocations.Sum(x => x.QtyOnHand);
                    _totalQtyOnHand = totalQtyOnHand;
                }

                return _totalQtyOnHand;
            }
            set
            {
                _totalQtyOnHand = value;
            }
        }

        public decimal _totalQtyOnOrder;
        public decimal TotalQtyOnOrder
        {
            get
            {
                if (_totalQtyOnOrder == 0 && ItemLocations != null && ItemLocations.Count > 0)
                {
                    _totalQtyOnOrder = ItemLocations.Sum(x => x.QtyOnOrder);
                }

                return _totalQtyOnOrder;
            }
            set
            {
                _totalQtyOnOrder = value;
            }
        }

        public decimal _totalCost;
        public decimal TotalCost
        {
            get
            {
                decimal totalCost = 0;
                if (_totalCost == 0 && ItemLocations != null && ItemLocations.Count > 0)
                {
                    _totalCost = ItemLocations.OrderByDescending(x => x.LastCost).FirstOrDefault().LastCost;
                }

                return _totalCost;
            }
            set
            {
                _totalCost = value;
            }
        }

        public List<ItemLocationModel> ItemLocations { get; set; }
        public bool IsCreated { get; set; }



        // For Budget Validation
        public decimal Price { get; set; }
        public decimal QtyBudget { get; set; }
        public decimal SubTotalBudget { get; set; }


        // History Purchase
        public decimal QtyBudgetHistory { get; set; }
        public decimal SubTotalHistory { get; set; }
    }


    public class ItemLocationModel
    {
        public string Location { get; set; }
        public decimal QtyOnHand { get; set; }
        public decimal QtyOnOrder { get; set; }
        public decimal LastCost { get; set; }
        public decimal AuditDateDecimal { get; set; }
    }
}

