﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class CBEModel
    {
        public CBEModel()
        {
            MSRIssuedDetails = new List<MSRIssuedDetailModel>();
            ItemLocations = new List<ItemLocationModel>();
            MSRSupplierDetails = new List<MSRSupplier>();
            MSRCurrencyDetails = new List<MSRCurrency>();
            EnumTableDetails = new List<EnumTable>();
            CBEDocumentDetails = new List<CBEDocumentModel>();
            CBERecomendationDetails = new List<CBERecomendationModel>();
            CBESummaryDetails = new List<CBESummaryModel>();
            CBESetWinners = new List<CBESetWinnerModel>();
            MSR = new MSRModel();
            PPNs = new List<PPNModel>();
        }

        public Guid ID { get; set; }
        public string CBENo { get; set; }
        public string IssuedType { get; set; }
        public string DocumentNo { get; set; }
        public string RQNNumber { get; set; }
        public Guid MSRId { get; set; }
        public string MSRNo { get; set; }
        public Guid CompanyId { get; set; }
        public string CompanyName { get; set; }
        public Guid DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string WorkOrderNo { get; set; }
        public string RequestorCostCenter { get; set; }
        public string MaintenanceCode { get; set; }
        public string Equipment { get; set; }
        public string EquipmentNo { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime ExpectedDate { get; set; }
        public string ExpectedDateStr { get; set; }
        public string CostCenter { get; set; }
        public string Remarks { get; set; }
        public string WorkFlowCode { get; set; }
        public string WorkOrderStatus { get; set; }
        public string TechnicalSummary { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public string homecur { get; set; }
        public string sourcecur { get; set; }
        public string pycostcode { get; set; }
        public MSRModel MSR { get; set; }
        public UserModel Creator { get; set; }
        public CBESearchModel CBESearch { get; set; }
        public void SetDateRange()
        {
            string DateFromStr = null;
            string DateToStr = null;
            DateTime DateFrom = DateTime.Now;
            DateTime DateTo = DateTime.Now;

            if (!string.IsNullOrEmpty(DateFromStr))
            {
                var dateConvert = Common.CommonFunction.DateTimeJResources(DateFromStr);
                if (dateConvert != DateTime.MinValue)
                {
                    DateFrom = Common.DateHelper.DateFromMinimun(dateConvert);
                }
            }

            if (!string.IsNullOrEmpty(DateToStr))
            {
                var dateConvert = Common.CommonFunction.DateTimeJResources(DateToStr);
                if (dateConvert != DateTime.MinValue)
                {
                    DateTo = Common.DateHelper.DateToMax(dateConvert);
                }
            }
        }
        public List<MSRIssuedDetailModel> MSRIssuedDetails { get; set; }
        public List<MSRSupplier> MSRSupplierDetails { get; set; }
        public List<MSRCurrency> MSRCurrencyDetails { get; set; }
        public List<EnumTable> EnumTableDetails { get; set; }
        public List<CBERecomendationModel> CBERecomendationDetails { get; set; }
        public List<CBESummaryModel> CBESummaryDetails { get; set; }
        public List<CBEDocumentModel> CBEDocumentDetails { get; set; }
        public List<ItemLocationModel> ItemLocations { get; set; }
        public List<CBESetWinnerModel> CBESetWinners { get; set; }
        public List<PPNModel> PPNs { get; set; }
    }

    public class CBEBidSupplierModel
    {
        public CBEBidSupplierModel()
        {
            CBESetWinners = new List<CBESetWinnerModel>();
        }
        public Guid Id { get; set; }
        public Guid CBEBidId { get; set; }
        public string ItemNo { get; set; }
        public string Supplier { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public int Qty { get; set; }
        public string Currency { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal? TotalPrice { get; set; }
        public decimal? Rate { get; set; }
        public decimal? TotalPrice_USD { get; set; }
        public bool? SetAsWinner { get; set; }
        public string Remark { get; set; }
        public List<CBESetWinnerModel> CBESetWinners { get; set; }
    }

    public class CBERecomendationModel
    {
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string ItemNo { get; set; }
        public string ItemPart { get; set; }
        public string Description { get; set; }
        public decimal? QtyProcess { get; set; }
        public string UoM { get; set; }
        public string Currency { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal? TotalPrice { get; set; }
        public decimal? TotalPrice_USD { get; set; }
        public string PersenTotalPrice_USD { get; set; }
        public int Qty { get; set; }
        public int QtyOffering { get; set; }
        public decimal UnitCost { get; set; }
        public bool? IsWinner { get; set; }
        public string IsWinnerText { get; set; }
        public string LabelCssColor { get; set; }
    }
    public class CBESummaryModel
    {
        public Guid ID { get; set; }
        public Guid CBEBid { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public decimal? TotalBeforeDiscount { get; set; }
        public decimal? Discount { get; set; }
        public decimal? TotalAfterDiscount { get; set; }
        public decimal? PPN { get; set; }
        public decimal? FreightCost { get; set; }
        public string NotForPPH { get; set; }
        public decimal GT_Offering { get; set; }
        public decimal Comparison1 { get; set; }
        public decimal? GT_Requirement { get; set; }
        public decimal Comparison2 { get; set; }
        public DateTime? QuotationReceivedDate { get; set; }
        public string QuotationReceivedDateStr { get; set; }
        public DateTime? PriceValidity { get; set; }
        public string PriceValidityStr { get; set; }
        public string ContactPerson { get; set; }
        public string TermsOfPayment { get; set; }
        public string DeliveryTime { get; set; }
        public string Incoterm { get; set; }
        public bool? IsWinner { get; set; }
    }
    public class PPNModel
    {
        public decimal Value { get; set; }
        public string Text { get; set; }
    }
    public class CBESetWinnerModel
    {
        public Guid ID { get; set; }
        public Guid? CBEId { get; set; }
        public string SupplierName { get; set; }
        public string SupplierCode { get; set; }
        public bool? IsProprietary { get; set; }
        public bool? IsLowestCost { get; set; }
        public bool? IsPriceStatedinContract { get; set; }
        public bool? IsBetterDelivery { get; set; }
        public bool? IsMeetSchedule { get; set; }
        public bool? IsPaymentTerm { get; set; }
        public bool? IsRepeatOrder { get; set; }
        public string OrderNo { get; set; }
        public bool? IsTechnicalAcceptable { get; set; }
        public bool? IsBetterAfterSales { get; set; }
        public bool? IsCommunityDevelopment { get; set; }
        public bool? IsOthers { get; set; }
        public string OthersRemark { get; set; }
        public string Remark { get; set; }
        public bool? IsWinner { get; set; }
    }
    public class CBEListmodel : EnumTable
    {
        public Guid ID { get; set; }
        public string CBENo { get; set; }
        public Guid MSRId { get; set; }
        public string MSRNo { get; set; }
        public string Status { get; set; }
        public string RQNNo { get; set; }
        public DateTime MSRIssuedDate { get; set; }
        public string MSRIssuedDateStr { get; set; }
        public string MSRIssuedTimeStr { get; set; }

        public Guid? CompanyId { get; set; }
        public string CompanyName { get; set; }
        public Guid DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string Requestor { get; set; }
        public string IssuedType { get; set; }
        public string Remarks { get; set; }


        public bool IsPurchased { get; set; }
        public string LabelCssColor { get; set; }
        public bool IsFailedPost { get; set; }

        public string LogMessage { get; set; }
    }
    public class CBEDocumentModel
    {
        public Guid id { get; set; }
        public string uriFile { get; set; }
        public string fileName { get; set; }
        public decimal? fileSize { get; set; }
        public string Type { get; set; }
    }

    public class CBESummarySupplier
    {
        public string Supplier { get; set; }
        public decimal? TotalBeforeDiscount { get; set; }
        public decimal? GT_Requirement { get; set; }
        public int IsWinner { get; set; }
    }
}
