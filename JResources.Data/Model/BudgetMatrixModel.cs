﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class BudgetMatrixModel
    {
        public BudgetMatrixModel()
        {
            Details = new List<BudgetMatrixDetailModel>();
        }
        public string CompanyCode { get; set; }
        public int Year { get; set; }
        public string CostCenter { get; set; }

        public List<BudgetMatrixDetailModel> Details { get; set; }
    }

    public class BudgetMatrixDetailModel
    {
        public string ItemCategory { get; set; }
        public string ItemCode { get; set; }
        public decimal Qty { get; set; }
        public decimal Price { get; set; }
        public decimal Total { get; set; }
    }
}
