﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace JResources.Data.Model
{
    public class UploadDocumentModel
    {
        public UploadDocumentModel()
        {
            DocumentDetails = new List<UploadDetailModel>();
        }

        public Guid CompanyId { get; set; }
        public string DocumentNo { get; set; }
        public string NIK { get; set; }
        public string Site { get; set; }

        public string DateFromStr { get; set; }
        public DateTime DateFrom { get; set; }
        public string DateToStr { get; set; }
        public DateTime DateTo { get; set; }

        public UserModel User { get; set; }
        public HttpPostedFileBase FileUpload { get; set; }

        public List<UploadDetailModel> DocumentDetails { get; set; }
    }


    public class UploadDetailModel
    {
        public Guid ID { get; set; }
        public Guid UploadId { get; set; }
        public string ItemNo { get; set; }
        public string ItemDescription{ get; set; }
        public string UOM { get; set; }
        public string StorageLocation { get; set; }
        public string StorageBin { get; set; }
        public decimal BookedQty { get; set; }
        public DateTime PSTDate { get; set; }

        public bool IsSucces { get; set; }
        public string ErrorMessage { get; set; }
    }
}
