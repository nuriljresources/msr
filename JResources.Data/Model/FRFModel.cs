﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class FRFModel
    {
        public FRFModel()
        {
            FRFDetails = new List<FRFDetailModel>();
            UserCreated = new UserModel();
            UserUpdated = new UserModel();
            UserApproval = new UserModel();

            FRFDate = DateTime.Now;
        }

        public Guid ID { get; set; }
        public Guid CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string FRFNo { get; set; }
        public DateTime? FRFDate { get; set; }
        public string FRFDateStr { get; set; }
        public string FRFStatus { get; set; }
        public string FRFStatusLabel { get; set; }     

        public UserModel UserCreated { get; set; }
        public DateTime CreatedDate { get; set; }
        public UserModel UserUpdated { get; set; }
        public DateTime UpdatedDate { get; set; }

        public UserModel UserApproval { get; set; }
        public decimal TotalQty
        {
            get
            {
                decimal _totalQty = 0;
                if (FRFDetails != null && FRFDetails.Count > 0)
                {
                    foreach (var item in FRFDetails)
                    {
                        if (item.Qty.HasValue)
                        {
                            _totalQty += item.Qty.Value;
                        }
                    }
                }

                return _totalQty;
            }
        }

        public string RowCssColor { get; set; }
        public string LabelCssColor { get; set; }

        public List<FRFDetailModel> FRFDetails { get; set; }
    }

    public class FRFDetailModel
    {
        public FRFDetailModel()
        {
            LogSheetDate = DateTime.Now;
        }

        public Guid ID { get; set; }
        public Guid FRFId { get; set; }
        public DateTime? LogSheetDate { get; set; }
        public string LogSheetDateStr { get; set; }
        public string ItemNo { get; set; }
        public string ItemDescription { get; set; }
        public decimal? Qty { get; set; }
        public string LocationId { get; set; }
        public decimal? EndOfKM { get; set; }
        public string OperatorName { get; set; }
        public string Remarks { get; set; }
        public FRFItemEquipment Equipment { get; set; }
        public string EquipmentCode { get; set; }
        public string MaterialCode { get; set; }
        public string CostCode { get; set; }
        public string Shift { get; set; }
        public string Category { get; set; }
        public string Unit { get; set; }
        public ItemPartModel ItemPart { get; set; }
        public FRFItemEquipment ItemEquipment { get; set; }
    }

    public class FRFItemEquipment
    {
        public string EquipmentCode { get; set; }
        public string EquipmentLabel { get; set; }
        public string CostCenter { get; set; }
        public string COA { get; set; }
    }

}
