﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Data.Model
{
    public class MSRHistoryModel : MSRModel
    {
        public MSRHistoryModel()
        {
            MSRIssueds = new List<MSRIssuedModel>();
        }

        public DateTime? DateApproveManager { get; set; }
        public DateTime? DateApproveSiteManager { get; set; }
        public List<MSRIssuedModel> MSRIssueds { get; set; }

        public MSRIssuedModel MSRLast { get; set; }
    }
}
