﻿using JResources.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JResources.Web
{
    public static class NotificationExtensions
    {
        private static IDictionary<String, String> NotificationKey = new Dictionary<String, String>
        {
            { "Error",      "App.Notifications.Error" },
            { "Warning",    "App.Notifications.Warning" },
            { "Success",    "App.Notifications.Success" },
            { "Info",       "App.Notifications.Info" }
        };

        public static void AddNotification(this ControllerBase controller, String message, String notificationType)
        {
            string NotificationKey = getNotificationKeyByType(notificationType);
            ICollection<String> messages = controller.TempData[NotificationKey] as ICollection<String>;

            if (messages == null)
            {
                controller.TempData[NotificationKey] = (messages = new HashSet<String>());
            }
            
            messages.Add(message);

            controller.TempData[NotificationKey] = messages;
        }

        public static void AddNotification(this ControllerBase controller, ResponseModel response)
        {
            string NotificationKey = "";
            if (!string.IsNullOrEmpty(response.Message))
            {
                if (response.IsSuccess)
                {
                    NotificationKey = getNotificationKeyByType(NotificationType.SUCCESS);
                }
                else
                {
                    NotificationKey = getNotificationKeyByType(NotificationType.ERROR);
                }

                ICollection<String> messages = controller.TempData[NotificationKey] as ICollection<String>;

                if (messages == null)
                {
                    controller.TempData[NotificationKey] = (messages = new HashSet<String>());
                }

                messages.Add(response.Message);
            }
        }

        public static IEnumerable<String> GetNotifications(this HtmlHelper htmlHelper, String notificationType)
        {
            string NotificationKey = getNotificationKeyByType(notificationType);
            return htmlHelper.ViewContext.Controller.TempData[NotificationKey] as ICollection<String> ?? null;
        }

        private static string getNotificationKeyByType(string notificationType)
        {
            try
            {
                return NotificationKey[notificationType];
            }
            catch (IndexOutOfRangeException e)
            {
                ArgumentException exception = new ArgumentException("Key is invalid", "notificationType", e);
                throw exception;
            }
        }

        public static void AddNotificationModelState(this ControllerBase controller, ModelStateDictionary ModelState)
        {
            foreach (ModelState modelState in ModelState.Values)
            {
                foreach (ModelError error in modelState.Errors)
                {
                    AddNotification(controller, error.ErrorMessage, NotificationType.ERROR);
                }
            }
        }
    }

    public static class NotificationType
    {
        public const string ERROR = "Error";
        public const string WARNING = "Warning";
        public const string SUCCESS = "Success";
        public const string INFO = "Info";

    }
}