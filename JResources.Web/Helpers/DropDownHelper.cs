﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JResources.Data;

namespace JResources.Web
{
    public class DropDownHelper
    {
        public static SelectListItem DefaultSelect()
        {
            return new SelectListItem()
            {
                Value = Guid.Empty.ToString(),
                Text = "Select",
                Selected = true
            };
        }

        public static List<SelectListItem> BindCompanyList(Guid CompanyId)
        {
            var companyList = Company.GetAll();
            var listSelect = new List<SelectListItem>();
            listSelect.Add(DefaultSelect());
            foreach (var item in companyList)
            {
                bool IsSelect = false;
                if (item.ID == CompanyId)
                {
                    IsSelect = true;
                }

                listSelect.Add(new SelectListItem()
                {
                    Value = item.ID.ToString(),
                    Text = string.Format("{0} [{1}]", item.CompanyName, item.CompanyCode),
                    Selected = IsSelect
                });
            }

            return listSelect;
        }

        public static List<SelectListItem> BindDepartmentList(Guid CompanyId, Guid DepartmentId)
        {
            var listSelect = new List<SelectListItem>();
            if (CompanyId != Guid.Empty)
            {
                var companyList = Department.GetAll().Where(x => x.CompanyId == CompanyId);
                listSelect.Add(DefaultSelect());
                foreach (var item in companyList)
                {
                    bool IsSelect = false;
                    if (item.ID == DepartmentId)
                    {
                        IsSelect = true;
                    }

                    listSelect.Add(new SelectListItem()
                    {
                        Value = item.ID.ToString(),
                        Text = item.DepartmentName,
                        Selected = IsSelect
                    });
                }
            }

            return listSelect;
        }

        public static List<SelectListItem> BindRoleList(Guid roleId)
        {
            var roleAccessList = RoleAccess.GetAll();
            var listSelect = new List<SelectListItem>();
            listSelect.Add(DefaultSelect());

            foreach (var item in roleAccessList)
            {
                bool IsSelect = false;
                if (item.ID == roleId)
                {
                    IsSelect = true;
                }

                listSelect.Add(new SelectListItem()
                {
                    Value = item.ID.ToString(),
                    Text = item.RoleAccessName,
                    Selected = IsSelect
                });
            }

            return listSelect;
        }

        public static List<SelectListItem> RequestStatus(string value = "")
        {
            var statusMSRs = EnumTable.GetByCode(EnumTable.EnumType.STATUS_MSR).ToList();
            var list = new List<SelectListItem>();
            foreach (var item in statusMSRs)
            {
                list.Add(new SelectListItem() {
                    Value = item.EnumValue,
                    Text = item.EnumLabel,
                    Selected = (item.EnumValue == value ? true : false)
                });
            }

            return list;
        }

        public static List<SelectListItem> RequestApprove(string value = "")
        {
            var statusMSRs = EnumTable.GetByCode(EnumTable.EnumType.STATUS_MSR).ToList();
            statusMSRs = statusMSRs.Where(x =>
                x.EnumValue == Common.Enum.RequestStatus.APPROVED ||
                x.EnumValue == Common.Enum.RequestStatus.REJECTED ||
                x.EnumValue == Common.Enum.RequestStatus.REVISION).ToList();

            var list = new List<SelectListItem>();
            foreach (var item in statusMSRs)
            {
                list.Add(new SelectListItem()
                {
                    Value = item.EnumValue,
                    Text = item.EnumLabel,
                    Selected = (item.EnumValue == value ? true : false)
                });
            }

            return list;
        }

        public static List<SelectListItem> StorageLocation(string value = "")
        {
            var sLocs = EnumTable.GetByCode(EnumTable.EnumType.STORAGE_LOCATION).ToList();
            var list = new List<SelectListItem>();
            list.Add(new SelectListItem() { Text = "Storage Location", Value = "" });
            foreach (var item in sLocs)
            {
                list.Add(new SelectListItem()
                {
                    Value = item.EnumValue,
                    Text = item.EnumLabel,
                    Selected = (item.EnumValue == value ? true : false)
                });
            }

            return list;
        }
    }
}