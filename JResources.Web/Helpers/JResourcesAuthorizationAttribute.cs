﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using JResources.Common;
using System.Web.Routing;
using JResources.Data.Model;
using JResources.BusinessLogic;
using System.Text;

namespace JResources.Web
{
    public class JResourcesAuthorizationAttribute : AuthorizeAttribute
    {
        private AuthorizationContext _currentAuthContext;
        public string ROLE_CODE { get; set; }
        public string[] ROLE_CODES { get; set; }
        public bool IS_AJAX { get; set; }

        ///// <summary>
        ///// Called when a process requests authorization.
        ///// </summary>
        ///// <param name="filterContext">The filter context, which encapsulates information for using <see cref="T:System.Web.Mvc.AuthorizeAttribute" />.</param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            _currentAuthContext = filterContext;

            HttpCookie cookie = filterContext.RequestContext.HttpContext.Request.Cookies[CookieManager.COOKIE_PREFIX + COOKIES_NAME.USER_MODEL];
            if (cookie != null && cookie.Value != null && !string.IsNullOrEmpty(cookie.ToString()))
            {
                var userStr = DataEncription.Decrypt(cookie.Value.ToString());
                UserModel model = new UserModel();
                if (!string.IsNullOrEmpty(userStr) && !string.IsNullOrEmpty(CurrentUser.NIKSITE))
                {
                    model.SetFromCookies(userStr);
                    if (string.IsNullOrEmpty(model.Username))
                    {
                        if (IS_AJAX)
                        {
                            RedirectUnauthorisePageAjax();
                        }
                        else
                        {
                            RedirectToLoginPage();
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ROLE_CODE) || (ROLE_CODES != null && ROLE_CODES.Length > 0))
                        {
                            var roleCookies = CookieManager.Get(COOKIES_NAME.ROLE);
                            model.SetRolesFromCookies(roleCookies);
                            if (!model.IsAdministrator)
                            {
                                if (!model.RoleAccessCodes.Contains(ROLE_CODE))
                                {
                                    if (ROLE_CODES != null && ROLE_CODES.Length > 0)
                                    {
                                        var checkROLECODE = (from a in model.RoleAccessCodes
                                                             join b in ROLE_CODES on a equals b
                                                             select a).FirstOrDefault();

                                        if (string.IsNullOrEmpty(checkROLECODE))
                                        {
                                            if (IS_AJAX)
                                            {
                                                RedirectUnauthorisePageAjax();
                                            }
                                            else
                                            {
                                                RedirectToUnauthorisePage();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (IS_AJAX)
                                        {
                                            RedirectUnauthorisePageAjax();
                                        }
                                        else
                                        {
                                            RedirectToUnauthorisePage();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (IS_AJAX)
                    {
                        RedirectUnauthorisePageAjax();
                    }
                    else
                    {
                        RedirectToLoginPage();
                    }
                }
            }
            else
            {
                if (IS_AJAX)
                {
                    RedirectUnauthorisePageAjax();
                }
                else
                {
                    RedirectToLoginPage();
                }
            }
        }

        #region Private Methods
        private class Http401Result : ActionResult
        {
            public override void ExecuteResult(ControllerContext context)
            {
                context.HttpContext.Response.ContentType = "application/json";
                ResponseModel response = new ResponseModel();
                response.SetError("401 Unauthorized");

                byte[] data = Encoding.UTF8.GetBytes(Newtonsoft.Json.JsonConvert.SerializeObject(response));
                context.HttpContext.Response.StatusCode = 401;
                //context.HttpContext.Response.Write("User login timeout, login again");
                context.HttpContext.Response.BinaryWrite(data);
                context.HttpContext.Response.End();
            }
        }

        /// <summary>
        /// Redirects to unauthorise page.
        /// </summary>
        private void RedirectToUnauthorisePage()
        {
            var redirectResult = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Error", action = "AccessDenied" }));
            _currentAuthContext.Result = redirectResult;
        }

        /// <summary>
        /// Redirects to login page.
        /// </summary>
        private void RedirectToLoginPage()
        {
            //var returnUrl = _currentAuthContext.HttpContext.Request.Url.AbsolutePath;
            //var redirectResult = new RedirectToRouteResult(new RouteValueDictionary(new { Areas = "", controller = "User", action = "Login", returnUrl = returnUrl }));

            var returnUrl = _currentAuthContext.HttpContext.Request.Url.AbsolutePath;
            var redirectResult = new RedirectToRouteResult(new RouteValueDictionary
                       {
                           { "action", "Login" },
                           { "controller", "User" },
                           { "Area", String.Empty }
                       });

            _currentAuthContext.Result = redirectResult;

            _currentAuthContext.Result = redirectResult;
        }

        private void RedirectUnauthorisePageAjax()
        {
            _currentAuthContext.Result = new Http401Result();
        }

        

        #endregion
    }
}