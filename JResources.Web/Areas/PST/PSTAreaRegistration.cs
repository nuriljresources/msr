﻿using System.Web.Mvc;

namespace JResources.Web.Areas.PST
{
    public class PSTAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PST";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PST_default",
                "PST/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
