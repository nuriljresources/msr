﻿using System;
using System.Linq;
using System.Web.Mvc;
using JResources.Data.Model;
using JResources.Common;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using JResources.Data;
using Omu.ValueInjecter;
using JResources.BusinessLogic;
using System.Collections.Generic;

namespace JResources.Web.Areas.PST.Controllers
{
    [JResourcesAuthorization]
    public partial class PlanningController : Controller
    {
        public ResponseModel _response { get; set; }

        #region PAGE LEVEL
        [HttpGet]
        public virtual ActionResult Index()
        {
            SearchModel model = new SearchModel();
            ViewData.Model = model;
            return View();
        }

        [HttpGet]
        public virtual ActionResult Detail()
        {
            UploadDocumentModel model = new UploadDocumentModel();
            ViewData.Model = model;
            return View();
        }

        [HttpPost]
        public virtual ActionResult Detail(UploadDocumentModel model)
        {
            var response = new ResponseModel();
            HSSFWorkbook wb = null;
            ISheet sheet = null;

            #region VALIDATION
            model.CompanyId = CurrentUser.GetCurrentCompany().ID;
            model.User = UserLogic.GetByNIK(CurrentUser.NIKSITE);
            model.DateFrom = CommonFunction.DateTimeJResources(model.DateFromStr);
            model.DateTo = CommonFunction.DateTimeJResources(model.DateToStr);
            model.FileUpload = Request.Files["FileUpload"];

            if (model.FileUpload != null && model.FileUpload.ContentLength > 0)
            {
                try
                {
                    // extract only the filename
                    var fileName = Path.GetFileName(model.FileUpload.FileName);
                    // store the file inside ~/App_Data/uploads folder
                    var UploadExcelPath = Path.Combine(SiteSettings.PATH_UPLOAD_EXCEL, fileName);
                    model.FileUpload.SaveAs(UploadExcelPath);

                    FileStream fs = new FileStream(UploadExcelPath, FileMode.Open, FileAccess.Read);
                    wb = new HSSFWorkbook(fs);
                    sheet = wb.GetSheetAt(0);

                    if (sheet.LastRowNum > 0)
                    {
                        response.SetSuccess();
                    }
                    else
                    {
                        response.SetError("Empty Excel file");
                    }
                }
                catch (Exception ex)
                {
                    //Logger.Error(LogCategory.PSTUpload, "Fails When Document Upload", ex);
                    response.SetError("Fails When Document Upload");
                }
            }
            else
            {
                response.SetError("Must Excel File Attached");
            }
            #endregion

            #region PROCESS TO MODEL
            if (response.IsSuccess)
            {
                for (int row = 1; row <= sheet.LastRowNum; row++)
                {
                    if (sheet.GetRow(row) != null)
                    {
                        var docDetail = new UploadDetailModel();
                        try
                        {
                            if (sheet.GetRow(row).GetCell(0) != null)
                            {
                                docDetail.ItemNo = sheet.GetRow(row).GetCell(0).StringCellValue;
                            }

                            docDetail.ItemDescription = sheet.GetRow(row).GetCell(1).StringCellValue;
                            docDetail.UOM = sheet.GetRow(row).GetCell(2).StringCellValue;
                            docDetail.StorageLocation = sheet.GetRow(row).GetCell(3).StringCellValue;
                            docDetail.StorageBin = sheet.GetRow(row).GetCell(4).StringCellValue;
                            docDetail.PSTDate = sheet.GetRow(row).GetCell(5).DateCellValue;
                            docDetail.IsSucces = true;
                        }
                        catch (Exception ex)
                        {
                            response.SetError();
                            docDetail.ErrorMessage = (string.IsNullOrEmpty(ex.Message) ? "Exception Error" : ex.Message);
                        }

                        if (!string.IsNullOrEmpty(docDetail.ItemNo))
                            model.DocumentDetails.Add(docDetail);
                    }
                }

                if (!(model.DocumentDetails != null && model.DocumentDetails.Count > 0))
                {
                    response.SetError("Blank data in Excel Document");
                }
                else
                {
                    var detailError = model.DocumentDetails.Where(x => !x.IsSucces).ToList();
                    if (detailError != null && detailError.Count > 0)
                    {
                        response.SetError(string.Format("Excel documents contained {0} row Error", detailError.Count));
                    }
                    else
                    {
                        model.DocumentDetails = model.DocumentDetails.OrderBy(x => x.StorageBin).OrderBy(x => x.StorageLocation).ToList();
                    }
                }


            }

            #endregion

            if (response.IsSuccess)
            {
                PSTPlanningLogic logic = new PSTPlanningLogic();
                logic.model = model;
                response = logic.SavePlanning();
            }

            if (response.IsSuccess)
            {
                this.AddNotification("Documents Successfully Submitted", NotificationType.SUCCESS);
                return RedirectToAction(MVC.PST.Planning.ScheduleView((Guid)response.ResponseObject));
            }
            else
            {
                this.AddNotification(response.Message, NotificationType.ERROR);
            }

            ViewData.Model = model;
            return View();
        }

        public virtual ActionResult ScheduleView(Guid Id)
        {
            PSTPlanningModel model = new PSTPlanningModel();
            PSTPlanning _planning = PSTPlanning.GetById(Id);
            if (_planning != null)
            {
                model.InjectFrom(_planning);
                model.SetDateString();
                var user = UserLogic.GetByNIK(_planning.CreatedBy);
                if (user != null)
                {
                    model.CreatedBy = user.FullName;
                }

                var groupByDate = _planning.PSTPlanningDetails.GroupBy(x => x.PlanningDate);

                foreach (var itemByDate in groupByDate.OrderBy(x => x.Key))
                {
                    PSTPlanningSchedulerModel _scheduler = new PSTPlanningSchedulerModel();
                    _scheduler.PlanningDate = itemByDate.FirstOrDefault().PlanningDate;
                    _scheduler.TotalRow = itemByDate.Count();
                    model.TotalItem += _scheduler.TotalRow;

                    foreach (var item in itemByDate)
                    {
                        PSTPlanningDetailModel _detail = new PSTPlanningDetailModel();
                        _detail.InjectFrom(item);
                        _scheduler.ListData.Add(_detail);
                    }

                    model.PSTPlanningScheduler.Add(_scheduler);
                }
            }
            else
            {
                this.AddNotification("PST Planning not found", NotificationType.ERROR);
                return RedirectToAction(MVC.PST.Planning.Index());
            }

            ViewData.Model = model;
            return View();
        }
        #endregion

        [HttpPost]
        public virtual JsonResult GetList(SearchModel model)
        {
            model.SetDateRange();

            var Plannings = PSTPlanning.GetAll();

            if (!string.IsNullOrEmpty(model.SearchCriteria))
            {
                Plannings = Plannings.Where(x => x.PlanningNo.Contains(model.SearchCriteria));
            }

            if (!string.IsNullOrEmpty(model.MoreCriteria))
            {
                var users = UserLogin.GetAll()
                    .Where(x => x.NIKSite.Contains(model.MoreCriteria) ||
                        x.FullName.Contains(model.MoreCriteria));

                if (users != null && users.Count() > 0)
                {
                    Plannings = (from p in Plannings
                                 join u in users on p.CreatedBy equals u.NIKSite
                                 select p);
                }
            }

            model.SetPager(Plannings.Count());
            var listData = Plannings.OrderByDescending(x => x.CreatedDate)
                .Skip((model.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
                .Take(Common.SiteSettings.PAGE_SIZE_PAGING);

            foreach (var item in listData)
            {
                var listItem = new PSTPlanningModel();
                listItem.InjectFrom(item);
                listItem.DateFromStr = item.DateFrom.ToString(Constant.FORMAT_DATE_JRESOURCES);
                listItem.DateToStr = item.DateTo.ToString(Constant.FORMAT_DATE_JRESOURCES);
                listItem.CreatedDateStr = item.CreatedDate.ToString(Constant.FORMAT_DATE_JRESOURCES);
                var userCreated = UserLogic.GetByNIK(item.CreatedBy);
                if (userCreated != null && userCreated.ID != Guid.Empty)
                {
                    listItem.CreatedBy = userCreated.FullName;
                }

                var checkUse = item.PSTPlanningDetails.FirstOrDefault(x => x.IsUse);
                if (checkUse != null)
                {
                    listItem.IsUse = true;
                }

                model.ListDataGeneral.Add(listItem);
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonResult Delete(Guid Id)
        {
            _response = new ResponseModel(true);

            PSTPlanning pstPlanning = PSTPlanning.GetById(Id);
            if (pstPlanning != null)
            {
                if (pstPlanning.PSTPlanningDetails != null &&
                    pstPlanning.PSTPlanningDetails.Count > 0)
                {
                    var checkIsUser = pstPlanning.PSTPlanningDetails.FirstOrDefault(x => x.IsUse);
                    if (checkIsUser != null)
                    {
                        _response.SetError(string.Format("Some detail has been use - date {0}", checkIsUser.PlanningDate.ToString(Constant.FORMAT_DATE_JRESOURCES)));
                    }
                }
            }
            else
            {
                _response.SetError("PST No Found");
            }

            if (_response.IsSuccess)
            {
                pstPlanning.IsDeleted = true;
                pstPlanning.Save<PSTPlanning>();
                string PSTPlanningNo = pstPlanning.PlanningNo;
                _response.SetSuccess(string.Format("PST {0} has been deleted", PSTPlanningNo));
            }

            return Json(_response, JsonRequestBehavior.AllowGet);
        }


        #region PST Document
        [HttpGet]
        public virtual ActionResult Index2()
        {
            //SearchModel paramDefault = FunctionWebHelpers.GetQueryString(Request.QueryString);
            //var SearchParam = new PSTUploadSearchModel();
            //SearchParam.InjectFrom(paramDefault);

            //if (!string.IsNullOrEmpty(Request["DocumentNo"]))
            //{
            //    SearchParam.DocumentNo = Request["DocumentNo"].ToString();
            //}

            //if (!string.IsNullOrEmpty(Request["CreatedBy"]))
            //{
            //    SearchParam.CreatedBy = Request["CreatedBy"].ToString();
            //}

            //var models = ZZPSTNON_TRANS.GetAll();
            //if (!string.IsNullOrEmpty(SearchParam.DocumentNo))
            //{
            //    models = models.Where(x => x.NoReg == SearchParam.DocumentNo);
            //}

            //if (!string.IsNullOrEmpty(SearchParam.CreatedBy))
            //{
            //    models = models.Where(x => x.CreatedBy == SearchParam.CreatedBy);
            //}

            //if (SearchParam.DateFrom.HasValue)
            //{
            //    models = models.Where(x => x.CreatedDate >= SearchParam.DateFrom.Value);
            //}

            //if (SearchParam.DateTo.HasValue)
            //{
            //    models = models.Where(x => x.CreatedDate <= SearchParam.DateTo.Value);
            //}


            //ViewData.Model = models;
            //ViewBag.SearchParam = SearchParam;
            return View();
        }

        public virtual ActionResult CreatePST(Guid ID)
        {
            //var User = CurrentUser.Model;
            //var model = new PSTModel();

            //if (ID != Guid.Empty)
            //{
            //    model = PSTTransLogic.GetByID(ID);
            //}
            //else
            //{
            //    model = new PSTModel();
            //}

            //ViewData.Model = model;
            return View();
        }

        [HttpPost]
        public virtual JsonResult CreatePST(PSTModel model)
        {
            var response = new ResponseModel(false);

            //try
            //{
            //    response = ValidatePSTTrans(model);
            //    if (response.IsSuccess)
            //    {
            //        if (model.PSTType == PSTType.PST_SCHEDULE)
            //        {
            //            response = PSTTransLogic.CreateByPSTScheduler(model);
            //        }
            //        else
            //        {
            //            response = PSTTransLogic.CreatePST(model, model.PSTType);
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    response.SetError("An error occurred while saving data");
            //    Logger.Error(LogCategory.PSTTrans, "PST Trans Exception", model, ex);
            //}

            //if (response.IsSuccess)
            //{
            //    this.AddNotification(string.Format("PST Trans No {0} Has Been Saved", response.ResponseObject.ToString()), NotificationType.SUCCESS);
            //}

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region PST First Input
        public virtual ActionResult FirstInput()
        {
            ViewData.Model = new PSTModel();
            return View();
        }

        [HttpPost]
        public virtual JsonResult FirstInput(PSTModel model)
        {
            var response = new ResponseModel();
            //try
            //{
            //    response = ValidatePSTTrans(model, true);
            //    if (response.IsSuccess)
            //    {
            //        response = PSTTransLogic.FirstInputUpdate(model);
            //    }

            //    if (response.IsSuccess)
            //    {
            //        this.AddNotification(string.Format("PST Trans No {0} Has Been Saved", response.ResponseObject.ToString()), NotificationType.SUCCESS);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logger.Error(LogCategory.PSTTrans, "Fails When Update First Input", ex);
            //    response.SetError("Fails When Update First Input");
            //}

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region PST Final Input
        [HttpGet]
        public virtual ActionResult PSTFinalList()
        {
            //var models = ZZPSTNON_TRANS.GetAll();
            //var SearchParam = new PSTUploadSearchModel();

            //ViewData.Model = models;
            //ViewBag.SearchParam = SearchParam;
            return View();
        }

        public virtual ActionResult FinalInput(Guid ID)
        {
            //var User = CurrentUser.Model;
            //var model = new PSTModel();

            //if (ID != Guid.Empty)
            //{
            //    model = PSTTransLogic.GetByID(ID);
            //}
            //else
            //{
            //    model = new PSTModel();
            //}

            //ViewData.Model = model;
            return View();
        }

        [HttpPost]
        public virtual JsonResult FinalInput(PSTModel model)
        {
            var response = new ResponseModel();
            //try
            //{
            //    response = ValidatePSTTrans(model, true);
            //    if (response.IsSuccess)
            //    {
            //        response = PSTTransLogic.FinalInputUpdate(model);
            //    }

            //    if (response.IsSuccess)
            //    {
            //        this.AddNotification(string.Format("PST Trans No {0} Has Been Saved", response.ResponseObject.ToString()), NotificationType.SUCCESS);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logger.Error(LogCategory.PSTTrans, "Fails When Update First Input", ex);
            //    response.SetError("Fails When Update First Input");
            //}

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Function JSON
        [HttpPost]
        public virtual ActionResult GetListItem(PSTModel model)
        {
            var response = new ResponseModel();
            //PSTModel responseObject = null;
            //model.PSTDate = CommonFunction.DateTimeJResources(model.PSTDateStr);

            //try
            //{
            //    if (model.PSTType == PSTType.PST_SCHEDULE)
            //    {
            //        var UPLOAD_GEN = ZZPSTNON_UPLOAD_GEN.GetByDate(model.PSTDate);
            //        response = PSTTransLogic.ValidatePSTScheduler(UPLOAD_GEN);

            //        if (response.IsSuccess)
            //        {
            //            responseObject = PSTTransLogic.GetPSTGenerateByUploadGen(UPLOAD_GEN.ID);
            //        }
            //    }
            //    else if (model.PSTType == PSTType.PST_TRANSACTED_ITEM)
            //    {
            //        responseObject = PSTTransLogic.GetItemFromDate(model.PSTDate);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    response.SetError("Error Exception When Get Data");
            //    Logger.Error(LogCategory.PSTGenerate, "PST GetListItem", model, ex);
            //}

            //if (response.IsSuccess)
            //{
            //    if (responseObject != null)
            //    {
            //        response.SetSuccess();
            //        response.ResponseObject = responseObject;
            //    }
            //    else
            //    {
            //        response.SetError("Document on date {0} not found", model.PSTDate.ToString(Constant.FORMAT_DATE_JRESOURCES_TEXT));
            //    }
            //}


            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ResponseModel ValidatePSTTrans(PSTModel model, bool IsCheckDetail = false)
        {
            var response = new ResponseModel();
            //if (!ModelState.IsValid)
            //{
            //    var errState = FunctionWebHelpers.GetErrorModelState(ModelState);
            //    response.SetError(errState);
            //}
            //else
            //{
            //    model.PSTDate = CommonFunction.DateTimeJResources(model.PSTDateStr);
            //    if (model.PSTDate == DateTime.MinValue)
            //    {
            //        response.SetError("PST Date Not Valid");
            //    }
            //    else
            //    {
            //        if (IsCheckDetail)
            //        {
            //            if (model.PSTGenerateDetails != null && model.PSTGenerateDetails.Count > 0)
            //            {
            //                foreach (var PSTDetail in model.PSTGenerateDetails)
            //                {
            //                    if (!model.IncludeZeroQty)
            //                    {
            //                        if (!(PSTDetail.QtyActual > 0))
            //                        {
            //                            response.SetError("Item No {0} Qty Actual Cannot be zero", PSTDetail.ItemNo.Trim());
            //                            break;
            //                        }

            //                        if (!(PSTDetail.QtyStockCard > 0))
            //                        {
            //                            response.SetError("Item No {0} Qty Stock Cannot be zero", PSTDetail.ItemNo.Trim());
            //                            break;
            //                        }
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                response.SetError("PST Dont have detail");
            //            }
            //        }
            //    }
            //}

            return response;
        }

        [HttpGet]
        public virtual JsonResult GetTransById(Guid ID)
        {
            //var model = PSTTransLogic.GetByID(ID);
            var model = "";
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }

}