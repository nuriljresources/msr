﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using JResources.BusinessLogic;
using JResources.Common.Enum;
using JResources.Data;
using JResources.Data.Model;

namespace JResources.Web.Areas.PST.Controllers
{
    [JResourcesAuthorization]
    public partial class PSTDocumentController : Controller
    {
        public virtual ActionResult Index()
        {
            return View();
        }

        public virtual ActionResult Create()
        {
            PSTModel model = new PSTModel();

            ViewBag.StorageLocation = DropDownHelper.StorageLocation();
            ViewData.Model = model;
            return View();
        }

        [HttpPost]
        public virtual JsonResult GetPlanningSchedule(PSTModel model)
        {
            if (model.PSTType == Common.PSTType.SCHEDULE)
            {
                model.SetDateString();
                if (model.PSTDate != DateTime.MinValue)
                {
                    var pstPlanning = PSTPlanning.GetByDate(model.PSTDate);
                    if (pstPlanning != null)
                    {
                        var details = pstPlanning.PSTPlanningDetails.Where(x => x.PlanningDate.Date == model.PSTDate.Date);
                        if (details != null)
                        {
                            var detailFirst = details.FirstOrDefault();
                            if (!detailFirst.IsUse)
                            {
                                foreach (var item in details)
                                {
                                    ICILOC icLoc = ICILOC.GetByItemNoAndLocation(item.ItemNo, item.Location);
                                    ICITEM icItem = ICITEM.GetByItemNo(item.ItemNo);

                                    model.PSTDetails.Add(new PSTDetailModel() {
                                        ItemNo = icItem.ITEMNO.Trim(),
                                        ItemDescription = (!string.IsNullOrEmpty(icItem.DESC) ? icItem.DESC.Trim() : string.Empty),
                                        IsUse = false,
                                        UOM = item.ItemNo,
                                        Location = item.ItemNo,
                                        BinLocation = item.ItemNo,
                                        QtyActual = icLoc.QTYONHAND,
                                        QtyStockCard = 0,
                                    });
                                }

                                model.SetSuccess();
                            }
                            else
                            {
                                model.SetError(string.Format("Planning {0} on date {1} already use", model.PSTNo, model.PSTDateStr));
                            }
                        }
                        else
                        {
                            model.SetError(string.Format("Planning {0} on date {1} dont have detail", model.PSTNo, model.PSTDateStr));
                        }
                    }
                    else
                    {
                        model.SetError("Dont have planning date on this date");
                    }
                }
                else
                {
                    model.SetError("Format date error");
                }
            }
            else
            {
                model.SetError("PST Type not schedule");
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public virtual JsonResult Save(PSTModel model)
        {
            ResponseModel response = ValidationPST(model);

            try
            {
                if (response.IsSuccess)
                {
                    Data.PST pst = new Data.PST()
                    {
                        ID = Guid.NewGuid(),
                        PSTNo = DocumentNumberLogic.GetPSTNo(CurrentUser.GetCurrentCompany().ID, model.PSTType),
                        PSTType = model.PSTType,
                        PSTStatus = PSTStatus.NEW,
                        ReferenceRandom = (string.IsNullOrEmpty(model.ReferenceRandom) ? string.Empty : model.ReferenceRandom),
                        StorageLocation = model.StorageLocation,
                        IsZeroLoc = model.IsZeroLoc,
                        PIC1Entry = model.PIC1.NIKSite,
                        PIC2Entry = (model.PIC2 != null ? model.PIC2.NIKSite : string.Empty),
                        CreatedDate = DateTime.Now,
                        CreatedBy = CurrentUser.NIKSITE,
                        UpdatedDate = DateTime.Now,
                        UpdatedBy = CurrentUser.NIKSITE
                    };

                    pst.PSTDetails = new System.Data.Objects.DataClasses.EntityCollection<PSTDetail>();
                    foreach (var item in model.PSTDetails)
                    {
                        PSTDetail detail = new PSTDetail()
                        {
                            ID = Guid.NewGuid(),
                            PSTId = pst.ID,
                            ItemNo = item.ItemNo,
                            ItemDescription = item.ItemDescription,
                            Location = item.Location,
                            BinLocation = item.BinLocation,
                            QtyActual = item.QtyActual
                        };

                        pst.PSTDetails.Add(detail);
                    }

                    pst.Save<Data.PST>();
                    response.SetSuccess();
                }
            }
            catch (Exception ex)
            {
                response.SetError(ex.Message);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ResponseModel ValidationPST(PSTModel model)
        {
            ResponseModel response = new ResponseModel();
            StringBuilder sbMessage = new StringBuilder();
            if (string.IsNullOrEmpty(model.PSTType))
            {
                sbMessage.AppendLine("PST Type must be select");
            }

            model.SetDateString();
            if (model.PSTDate == DateTime.MinValue)
            {
                sbMessage.AppendLine("PST Date must be select");
            }

            if (model.PIC1 != null && string.IsNullOrEmpty(model.PIC1.NIKSite))
            {
                sbMessage.AppendLine("PIC 1 must be enter");
            }

            return response;
        }
    }
}
