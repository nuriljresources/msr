﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using JResources.Data;
using JResources.Data.Model;
using JResources.BusinessLogic;
using JResources.Common;
using JResources.Common.Enum;
using System.Text;
using JResources.Data.Model.Accpac;
using NPOI.SS.Formula.Functions;

namespace JResources.Web.Controllers
{
    [JResourcesAuthorization(ROLE_CODE = ROLE_CODE.CEA_ENTRY)]
    public partial class CEAController : Controller
    {
        #region LEVEL PAGE
        /// <summary>
        /// List Request Material
        /// </summary>
        /// <returns></returns>
        //[JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.MSR_ENTRY, ROLE_CODE.CEA_ENTRY })]
        public virtual ActionResult Index()
        {
            var model = new CEASearchModel();
            List<EnumTable> CEAStatus = new List<EnumTable>() { new EnumTable() { EnumValue = "", EnumLabel = "SELECT ALL STATUS" } };
            CEAStatus.AddRange(EnumTable.GetByCode(EnumTable.EnumType.STATUS_CEA).ToList());

            CEAStatus = CEAStatus.Where(x =>
                x.EnumValue == ""
                || x.EnumValue == Common.Enum.RequestStatus.NEW
                || x.EnumValue == Common.Enum.RequestStatus.CANCEL
                || x.EnumValue == Common.Enum.RequestStatus.REJECTED
                || x.EnumValue == Common.Enum.RequestStatus.APPROVED
                || x.EnumValue == Common.Enum.RequestStatus.PARTIAL_APPROVED
                || x.EnumValue == Common.Enum.RequestStatus.PARTIAL_COMPLETED
                || x.EnumValue == Common.Enum.RequestStatus.PARTIAL_TRANSFER
                || x.EnumValue == Common.Enum.RequestStatus.FULL_TRANSFER
                || x.EnumValue == Common.Enum.RequestStatus.PARTIAL_ISSUED
                || x.EnumValue == Common.Enum.RequestStatus.PARTIAL_PURCHASING
                || x.EnumValue == Common.Enum.RequestStatus.FULL_PURCHASING
                || x.EnumValue == Common.Enum.RequestStatus.COMPLETED).ToList();

            List<CompanyDepartmentModel> CompanyDepartments = new List<CompanyDepartmentModel>() { new CompanyDepartmentModel { ID = Guid.Empty, CompanyName = "SELECT All COMPANY" } };
            CompanyDepartments.AddRange(CompanyLogic.GetFullCompanyWithDepartmentByCurrentRole(new string[] { ROLE_CODE.MSR_ENTRY, ROLE_CODE.MSR_ISSUED, ROLE_CODE.MSR_APPROVAL, ROLE_CODE.MSR_APPROVAL_GM }));
            BindData();

            string ceaStatus = Request.QueryString["status"];
            if (ceaStatus != null)
            {
                if (ceaStatus == RequestStatus.NEW)
                {
                    model.CEA_STATUS = new string[] { RequestStatus.NEW };
                    model.IsSearchFilter = true;
                }

                if (ceaStatus == RequestStatus.APPROVED)
                {
                    model.CEA_STATUS = new string[] { RequestStatus.APPROVED };
                    model.IsSearchFilter = true;
                }
            }

            ViewData.Model = model;
            ViewBag.CEAStatus = CEAStatus;
            ViewBag.CompanyDepartments = CompanyDepartments;
            return View();
        }

        [JResourcesAuthorization(IS_AJAX = true, ROLE_CODE = ROLE_CODE.CEA_ENTRY)]
        [HttpGet]
        public virtual JsonResult GetByCEAId(string Id)
        {
            ResponseModel response = new ResponseModel();
            CEAModel model = new CEAModel();
            CEA _CEA = new CEA();

            Guid IdGuid = Guid.Empty;
            Guid.TryParse(Id, out IdGuid);

            if (IdGuid != Guid.Empty)
            {
                // TEKNIK BODOH KARENA KONSEPNYA JUGA NGGAK JELAS INI MODUL PROJECTNYA
                #region INSERT INTO MSR
                _CEA = CurrentDataContext.CurrentContext.CEAs.FirstOrDefault(x => x.ID == IdGuid);
                if (_CEA == null)
                {
                    V_CEA_HEADER cea = V_CEA_HEADER.GetById(Id);
                    List<V_CEA_ITEMACCPAC> cea_details = V_CEA_ITEMACCPAC.GetByCEAId(Id).ToList();
                    var locationItem = CurrentDataContext.CurrentContext.v_nintex_location_cearqn.FirstOrDefault(x => x.compid == cea.company);

                    _CEA = new CEA();
                    _CEA.ID = IdGuid;
                    _CEA.CompanyCode = cea.company.Trim();
                    _CEA.CEANo = cea.cea_number.Trim();
                    _CEA.TotalAmount = cea.total_amount.HasValue ? cea.total_amount.Value : 0;
                    _CEA.CEADate = cea.cer_date.HasValue ? cea.cer_date.Value : DateTime.Now;
                    _CEA.StatusCEA = CEAStatus.NEW;
                    _CEA.IsDeleted = false;
                    _CEA.Remarks = cea.subject.Trim();
                    _CEA.CreatedBy = CurrentUser.NIKSITE;
                    _CEA.CreatedDate = DateTime.Now;
                    _CEA.UpdatedBy = CurrentUser.NIKSITE;
                    _CEA.UpdatedDate = DateTime.Now;

                    decimal TotalAmount = 0;

                    if (cea_details != null && cea_details.Count > 0)
                    {
                        _CEA.CEADetails = new System.Data.Objects.DataClasses.EntityCollection<CEADetail>();
                        int LineNumber = 1;
                        foreach (var itemCEA in cea_details)
                        {
                            CEADetail _CEADetail = new CEADetail();
                            _CEADetail.ID = Guid.NewGuid();
                            _CEADetail.CEAId = IdGuid;
                            _CEADetail.LineNumber = LineNumber++;
                            _CEADetail.ItemNo = itemCEA.item_no.Trim();
                            _CEADetail.ItemDescription = itemCEA.descx.Trim();
                            _CEADetail.ItemType = string.Empty;
                            _CEADetail.Qty = itemCEA.qty.HasValue ? itemCEA.qty.Value : 0;
                            _CEADetail.UnitCost = itemCEA.unit_cost.HasValue ? itemCEA.unit_cost.Value : 0;
                            _CEADetail.GLAccount = itemCEA.cocoding_coa;
                            _CEADetail.Location = locationItem != null ? locationItem.location.Trim() : "";
                            _CEADetail.UOM = itemCEA.uom;
                            _CEADetail.UnitCost = itemCEA.unit_cost.HasValue ? itemCEA.unit_cost.Value : 0;
                            _CEADetail.GLAccount = itemCEA.cocoding_coa;
                            _CEADetail.CCCODE = itemCEA.coding_cost_center;

                            _CEADetail.CreatedBy = CurrentUser.NIKSITE;
                            _CEADetail.CreatedDate = DateTime.Now;
                            _CEADetail.UpdatedBy = CurrentUser.NIKSITE;
                            _CEADetail.UpdatedDate = DateTime.Now;
                            _CEA.CEADetails.Add(_CEADetail);


                            TotalAmount += _CEADetail.Qty * _CEADetail.UnitCost;
                        }
                    }

                    _CEA.TotalAmount = TotalAmount;
                    _CEA.Save<CEA>();
                }
                #endregion

                model.CEAId = _CEA.ID;
                model.CEANo = _CEA.CEANo;
                model.TotalAmount = _CEA.TotalAmount;
                model.CEADate = _CEA.CEADate;
                model.CEADateStr = _CEA.CEADate.ToString(Constant.FORMAT_DATE_JRESOURCES);
                model.Status = _CEA.StatusCEA;
                model.CreatedDateStr = _CEA.CreatedDate.ToString(Constant.FORMAT_DATE_JRESOURCES);
                model.RQNDate = DateTime.Now;
                model.CompanyCode = _CEA.CompanyCode;
                model.Remarks = _CEA.Remarks;
                model.History = CEALogic.GetRequistion(IdGuid);
                model.CreatedBy = CurrentUser.NIKSITE;


                var CEADetails = _CEA.CEADetails.Where(x => !x.ParentId.HasValue).ToList();
                if (CEADetails != null && CEADetails.Count > 0)
                {                    
                    foreach (var CEADetail in CEADetails.OrderBy(x => x.LineNumber))
                    {
                        var itemModel = new CEAItemModel();
                        itemModel.ID = CEADetail.ID;
                        itemModel.LineNo = CEADetail.LineNumber;
                        itemModel.ItemNo = CEADetail.ItemNo;
                        itemModel.ItemDescription = CEADetail.ItemDescription;
                        itemModel.QtyRequest = CEADetail.Qty;
                        itemModel.Qty = 0;
                        itemModel.Location = CEADetail.Location;
                        itemModel.UOM = CEADetail.UOM;
                        itemModel.UnitCost = CEADetail.UnitCost;
                        itemModel.UnitCostLimit = CEADetail.UnitCost;
                        itemModel.GLAccount = CEADetail.GLAccount;
                        itemModel.CCCODE = CEADetail.CCCODE;

                        model.CEAItems.Add(itemModel);
                    }
                }

                var CEAAdditionals = _CEA.CEADetails.Where(x => x.ParentId.HasValue).ToList();
                if (CEAAdditionals != null && CEAAdditionals.Count > 0)
                {
                    foreach (var CEADetail in CEAAdditionals.OrderBy(x => x.LineNumber))
                    {
                        var itemModel = new CEAItemModel();
                        itemModel.ID = CEADetail.ID;
                        itemModel.IsCompleted = true;
                        itemModel.LineNo = CEADetail.LineNumber;
                        itemModel.ReferenceId = CEADetail.ParentId.Value;
                        itemModel.ItemNo = CEADetail.ItemNo;
                        itemModel.ItemDescription = CEADetail.ItemDescription;
                        itemModel.QtyRequest = CEADetail.Qty;
                        itemModel.Qty = CEADetail.Qty;
                        itemModel.Location = CEADetail.Location;
                        itemModel.UOM = CEADetail.UOM;
                        itemModel.UnitCost = CEADetail.UnitCost;
                        itemModel.UnitCostLimit = CEADetail.UnitCost;
                        itemModel.GLAccount = CEADetail.GLAccount;
                        itemModel.CCCODE = CEADetail.CCCODE;

                        model.CEAAdditionalItems.Add(itemModel);

                        foreach (var item in model.CEAItems)
                        {
                            if (item.ID == CEADetail.ParentId.Value)
                                item.IsReference = true;
                        }
                    }
                }

                CEALogic.SetStatusCEA(model);
                response.ResponseObject = model;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [JResourcesAuthorization(IS_AJAX = true, ROLE_CODE = ROLE_CODE.CEA_ENTRY)]
        [HttpGet]
        public virtual JsonResult CompleteCEA(Guid Id)
        {
            ResponseModel response = new ResponseModel();

            CEA _CEA = CurrentDataContext.CurrentContext.CEAs.FirstOrDefault(x => x.ID == Id);
            _CEA.StatusCEA = CEAStatus.COMPLETED;
            _CEA.UpdateSave<CEA>();

            response.SetSuccess(string.Format("CEA {0} success to completed", _CEA.CEANo));

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region LEVEL POST JSON
        /// <summary>
        /// Get JSON MSR
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [JResourcesAuthorization(IS_AJAX = true, ROLE_CODE = ROLE_CODE.CEA_ENTRY)]
        [HttpPost]
        public virtual JsonResult GetListCEA(CEASearchModel model)
        {
            model.ROLE_ACCESS = new string[] { ROLE_CODE.CEA_ENTRY };
            model = CEALogic.GetListCEA(model);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Post RQN
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [JResourcesAuthorization(IS_AJAX = true, ROLE_CODE = ROLE_CODE.CEA_ENTRY)]
        [HttpPost]
        public virtual JsonResult PostRequisition(CEAModel model)
        {
            var response = new ResponseModel();
            #region VALIDATION
            StringBuilder sbError = new StringBuilder();
            CEA cea = CEA.GetById(model.CEAId);
            var CEAItemAll = model.CEAItems;
            List<CEARequistionModel> CEARequistions = CEALogic.GetRequistion(cea.ID);
            decimal RequisitionTotalAmount = CEARequistions.Sum(x => x.Items.Sum(xx => xx.Qty * xx.UnitCost));

            if (string.IsNullOrEmpty(model.CostCenter)) sbError.AppendLine("Cost center must be select");
            if (string.IsNullOrEmpty(model.WorkFlow)) sbError.AppendLine("Workflow must be select");
            model.CEAItems = model.CEAItems.Where(x => !x.IsExclude && !x.IsCompleted).ToList();
            CEALogic.SetStatusCEA(model);

            List<Guid> ListIdReferences = new List<Guid>();

            if (model.CEAAdditionalItems != null && model.CEAAdditionalItems.Count > 0)
            {
                foreach (var item in model.CEAAdditionalItems)
                {
                    // Dibuat kondisi tidak ada mode outstanding untuk additional item
                    //if ((item.QtyOutstanding - item.Qty) < 0)
                    //    sbError.AppendLine(string.Format("Item {0} cannot over qty request", item.ItemNo));

                    if (item.ReferenceId == Guid.Empty)
                        sbError.AppendLine(string.Format("Line {0}-{1} must be selectreference item", item.LineNo, item.ItemDescription));

                    if (string.IsNullOrEmpty(item.ItemNo))
                        sbError.AppendLine(string.Format("Line {0}-{1} must be select item", item.LineNo, item.ItemDescription));

                    if (!item.IsCompleted)
                    {
                        if (!(item.Qty > 0))
                            sbError.AppendLine(string.Format("Item {0}-{1} don't have quantity", item.ItemNo, item.ItemDescription));

                        if (!(item.UnitCost > 0) && !item.IsCompleted)
                            sbError.AppendLine(string.Format("Item {0}-{1} must be input unit cost", item.ItemNo, item.ItemDescription));

                        //if (item.UnitCost > item.UnitCostLimit)
                        //    sbError.AppendLine(string.Format("Item {0} cannot over price from {1}", item.ItemNo, item.UnitCostLimit));

                    }

                    ListIdReferences.Add(item.ReferenceId);
                }

                var listNewAdditional = model.CEAAdditionalItems.Where(x => x.ID == Guid.Empty && x.IsCompleted == false).ToList();
                if(listNewAdditional != null && listNewAdditional.Count > 0)
                {
                    var listIdReference = listNewAdditional.GroupBy(x => x.ReferenceId).Select(x => x.FirstOrDefault().ReferenceId);
                    foreach (var IdReference in listIdReference)
                    {
                        var mainItem = model.CEAItems.FirstOrDefault(x => x.ID == IdReference);

                        decimal limitTotal = 0;
                        if(mainItem.QtyRequest > 0)
                        {
                            limitTotal = mainItem.QtyRequest * mainItem.UnitCost;
                        } else
                        {
                            limitTotal = (mainItem.Qty > 0 ? mainItem.Qty : 1) * mainItem.UnitCost;
                        }

                        var additionalTotal = model.CEAAdditionalItems.Where(x => x.ReferenceId == IdReference).Sum(x => x.Qty * x.UnitCost);

                        if (additionalTotal > limitTotal)
                        {
                            sbError.AppendLine(string.Format("Amount total additional item with reference item {0}-{1} cannot over from {2}", mainItem.ItemNo, mainItem.ItemDescription, limitTotal.ToString("n2")));
                        }
                    }


                }
            }

            if (model.CEAItems != null && model.CEAItems.Count > 0)
            {                
                foreach (var item in model.CEAItems)
                {
                    if (!item.IsContract)
                    {
                        if(!ListIdReferences.Contains(item.ID))
                        {
                            if (!(item.Qty > 0))
                                sbError.AppendLine(string.Format("Item {0}-{1} don't have quantity", item.ItemNo, item.ItemDescription));

                            if ((item.QtyOutstanding - item.Qty) < 0)
                                sbError.AppendLine(string.Format("Item {0}-{1} cannot over qty request", item.ItemNo, item.ItemDescription));

                            if (item.UnitCost > item.UnitCostLimit)
                                sbError.AppendLine(string.Format("Item {0}-{1} cannot over price from {2}", item.ItemNo, item.ItemDescription, item.UnitCostLimit));
                        } else
                        {
                            item.Qty = 0;
                        }
                    }
                    else
                    {
                        item.Qty = item.QtyRequest;
                    }
                }
            }
            else
            {
                sbError.AppendLine("dont have detail");
            }

            if(sbError.Length < 1)
            {
                RequisitionTotalAmount += model.CEAItems.Sum(x => x.Qty * x.UnitCost);
                var CEAAdditionalItems = model.CEAAdditionalItems.Where(x => x.ID == Guid.Empty && x.IsCompleted == false).ToList();

                if(CEAAdditionalItems != null && CEAAdditionalItems.Count > 0)
                {
                    RequisitionTotalAmount += CEAAdditionalItems.Sum(x => x.Qty * x.UnitCost);
                }
                

                if (RequisitionTotalAmount > cea.TotalAmount)
                {
                    sbError.AppendLine(string.Format("Total Amount Request Requisition : {0} cannot over from {1}", RequisitionTotalAmount.ToString("##"), cea.TotalAmount));
                }
            }


            if (sbError.Length > 0)
                response.SetError(sbError.ToString());
            #endregion

            if (response.IsSuccess)
            {
                try
                {
                    var ContractItems = model.CEAItems.Where(x => x.IsContract).ToList();

                    var RQNItems = model.CEAItems.Where(x => !x.IsContract && x.Qty > 0).ToList();
                    var RQNAdditionals = model.CEAAdditionalItems.Where(x => x.ID == Guid.Empty && x.IsCompleted == false).ToList();
                    if (ContractItems != null && ContractItems.Count > 0)
                    {
                        var modelContract = model;
                        modelContract.CEAItems = ContractItems;
                        Company company = Company.GetByCode(model.CompanyCode);
                        string contractNo = DocumentNumberLogic.GetContractCEANo(company.ID);
                        response = SaveRequisition(contractNo, modelContract);
                    }

                    if ((RQNItems != null && RQNItems.Count > 0) || (RQNAdditionals != null && RQNAdditionals.Count > 0))
                    {
                        model.CEAItems = RQNItems;
                        if (RQNAdditionals != null && RQNAdditionals.Count > 0)
                        {
                            var listIdRef = RQNAdditionals.Select(x => x.ReferenceId).ToList();
                            model.CEAItems = model.CEAItems.Where(x => !listIdRef.Contains(x.ID)).ToList();
                            foreach (var RQNAdditional in RQNAdditionals)
                            {
                                var itemRef = CEAItemAll.FirstOrDefault(x => x.ID == RQNAdditional.ReferenceId);
                                RQNAdditional.Location = itemRef.Location;
                                RQNAdditional.CCCODE = itemRef.CCCODE;
                                RQNAdditional.GLAccount = itemRef.GLAccount;
                                model.CEAItems.Add(RQNAdditional);
                            }
                        }

                        PT0040 pt0040 = BusinessLogic.Accpac.PurchasingLogic.MappingCEARequisition(model);

                        var json = JsonConvert.SerializeObject(pt0040);
                        var data = new StringContent(json, Encoding.UTF8, "application/json");

                        var url = SiteSettings.ACCPAC_URL + "/Purchasing/CEAToRequisition";
                        var client = new HttpClient();
                        var responsePost = client.PostAsync(url, data);

                        string result = responsePost.Result.Content.ReadAsStringAsync().Result;
                        response = JsonConvert.DeserializeObject<ResponseModel>(result);
                        if (response.IsSuccess)
                        {
                            response = SaveRequisition(response.ResponseObject.ToString(), model);
                            if (RQNAdditionals != null && RQNAdditionals.Count > 0)
                                SaveAdditional(model.CEAId, RQNAdditionals);
                        }
                        else
                        {
                            response.ResponseObject = json;
                        }
                    }
                }
                catch (Exception ex)
                {
                    response.SetError("Error from web : " + ex.Message);
                }
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public List<RQNPOModel> GetRQNPODetail(string RQNNumber)
        {
            var datas = AccpacDataContext.CurrentContext.ExecuteStoreQuery<RQNPOModel>(string.Format(@"
            SELECT 
	            POPORH1.PONUMBER, 
	            POPORH1.ONHOLD, 
	            POPORH1.CURRENCY, 
	            POPORH1.RATE, 
	            ISNULL((SELECT top 1 RQNNUMBER FROM PTPRD WHERE PONUMBER = POPORH1.PONUMBER),'') AS RQNNUMBER, 
	            ISNULL((SELECT VALUE FROM POPORHO WHERE PORHSEQ=POPORH1.PORHSEQ and OPTFIELD='CEA'),'') as CEAno, 
	            POPORL.COMPLETION,
	            POPORL.DTCOMPLETE,
	            POPORL.ITEMNO, 
	            POPORL.LOCATION, 
	            POPORL.ITEMDESC, 
	            POPORL.ORDERUNIT,
	            POPORL.OQORDERED, 
	            POPORL.OQRECEIVED,
	            POPORL.OQCANCELED,
	            POPORL.UNITCOST   
            FROM POPORH1
            INNER JOIN POPORL ON POPORH1.PORHSEQ=POPORL.PORHSEQ
            WHERE POPORH1.PONUMBER IN (
	            SELECT PONUMBER FROM PTPRD WHERE RQNNUMBER = '{0}'
            )
            ", RQNNumber), RQNNumber).ToList();

            return datas;
        }


        [HttpGet]
        public virtual JsonResult GetRQNPO(string RQNNumber)
        {
            var response = new ResponseModel(false);

            var datas = GetRQNPODetail(RQNNumber);
            var HeaderCancel = datas.FirstOrDefault(x => x.ONHOLD == 1);
            if (HeaderCancel != null)
            {
                response.SetSuccess("RQN {0} Is On Hold", HeaderCancel.rqnnumber.Trim());
            }
            else
            {
                var DetailCancel = datas.Where(x => x.OQCANCELED > 0).ToList();
                if (DetailCancel != null && DetailCancel.Count > 0)
                {
                    response.SetSuccess("Detail item {0} will be cancelled", string.Join(",", DetailCancel.Select(x => x.ITEMNO.Trim()).ToArray()));
                }
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public virtual JsonResult CancelRQN(string RQNNumber)
        {
            var response = new ResponseModel(false);
            var RQNPO = GetRQNPODetail(RQNNumber);
            StringBuilder sbMessage = new StringBuilder();

            var MSRRQN = CEARequisition.GetByRQNNo(RQNNumber);
            if (MSRRQN != null)
            {
                var IsOnHold = RQNPO.FirstOrDefault(x => x.ONHOLD == 1);
                if (IsOnHold != null)
                {
                    MSRRQN.IsDeleted = true;
                    MSRRQN.UpdateSave<CEARequisition>();
                    response.SetSuccess("RQN Has been deleted");
                }
                else
                {
                    var RQNDetailCancelleds = RQNPO.Where(x => x.OQCANCELED > 0).ToList();
                    if (RQNDetailCancelleds != null && RQNDetailCancelleds.Count > 0)
                    {
                        var MSRRQNDetail = CEARequisitionDetail.GetByCEARequisitionId(MSRRQN.ID).ToList();
                        List<CEADetail> CEADetailUpdates = new List<CEADetail>();

                        if (MSRRQNDetail != null && MSRRQNDetail.Count > 0)
                        {
                            foreach (var ItemCancel in RQNDetailCancelleds)
                            {
                                string ITEMNO = ItemCancel.ITEMNO.Replace("-", "").Trim();
                                var MSRRQNItems = MSRRQNDetail.Where(x => x.ItemNo.Trim() == ITEMNO).ToList();
                                if (MSRRQNItems != null && MSRRQNItems.Count > 0)
                                {
                                    foreach (var MSRRQNItem in MSRRQNItems)
                                    {
                                        var CEAItem = CurrentDataContext.CurrentContext.CEADetails.FirstOrDefault(x => x.CEAId == MSRRQN.CEAId && x.LineNumber == MSRRQNItem.LineNumber);
                                        if (MSRRQNItem.Qty >= ItemCancel.OQCANCELED)
                                        {
                                            MSRRQNItem.Qty = MSRRQNItem.Qty - ItemCancel.OQCANCELED;
                                            MSRRQNItem.QtyCancel = ItemCancel.OQCANCELED;
                                            ItemCancel.OQCANCELED = MSRRQNItem.Qty;
                                            
                                            CEAItem.Qty = CEAItem.Qty - ItemCancel.OQCANCELED;
                                            CEADetailUpdates.Add(CEAItem);
                                            if (MSRRQNItem.Qty == 0)
                                            {
                                                MSRRQNItem.IsDeleted = true;
                                            }

                                            if (CEAItem.Qty == 0)
                                            {
                                                CEAItem.IsDeleted = true;
                                            }
                                        }
                                        else
                                        {
                                            MSRRQNItem.QtyCancel = MSRRQNItem.Qty;
                                            MSRRQNItem.Qty = 0;
                                            ItemCancel.OQCANCELED = ItemCancel.OQCANCELED - MSRRQNItem.Qty;
                                            CEAItem.Qty = 0;
                                            CEADetailUpdates.Add(CEAItem);
                                        }
                                    }
                                }

                            }


                            for (int i = 0; i < MSRRQNDetail.Count; i++)
                            {
                                var MSRRQNItem = MSRRQNDetail[i];
                                if (MSRRQNItem.IsDeleted)
                                {
                                    sbMessage.AppendLine(string.Format("Item {0} has been deleted", MSRRQNItem.ItemNo));
                                }

                                if (MSRRQNItem.QtyCancel.HasValue && MSRRQNItem.QtyCancel.Value > 0)
                                {
                                    sbMessage.AppendLine(string.Format("Item {0} has been save qty cancel {1}", MSRRQNItem.ItemNo.Trim(), MSRRQNItem.QtyCancel.Value));
                                }

                                MSRRQNItem.UpdateSave<CEARequisitionDetail>();
                            }

                            for (int i = 0; i < CEADetailUpdates.Count; i++)
                            {
                                var CEADetailUpdate = CEADetailUpdates[i];
                                CEADetailUpdate.UpdateSave<CEADetail>();
                            }
                        }

                    }


                }
            }

            if (sbMessage.Length > 0)
            {
                response.SetSuccess(sbMessage.ToString());
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region LEVEL FUNCTION
        public void BindData()
        {
            List<SelectListItem> listWorkFlowList = new List<SelectListItem>();
            var PTWFs = PTWF.GetAll().Where(x => !x.DESC.Contains("NOT USED")).ToList();
            foreach (var item in PTWFs)
            {
                listWorkFlowList.Add(new SelectListItem() { Text = string.Format("{0}-{1}", item.WORKFLOW.Trim(), item.DESC.Trim()), Value = item.WORKFLOW });
            }

            List<SelectListItem> listCostCenter = new List<SelectListItem>();
            var PTCOSTs = PTCOST.GetAll().ToList();
            foreach (var item in PTCOSTs)
            {
                listCostCenter.Add(new SelectListItem() { Text = item.DESC, Value = item.COSTCTR });
            }

            ViewBag.listCostCenter = listCostCenter;
            ViewBag.listWorkFlowList = listWorkFlowList;
        }
        public ResponseModel SaveRequisition(string RQNNumber, CEAModel model)
        {
            var response = new ResponseModel();

            try
            {
                CEARequisition cEARequisition = new CEARequisition();
                cEARequisition.ID = Guid.NewGuid();
                cEARequisition.CEAId = model.CEAId;
                cEARequisition.RequisitionNo = RQNNumber;
                cEARequisition.RequisitionDate = DateTime.Now;
                cEARequisition.WorkFlow = model.WorkFlow;
                cEARequisition.CreatedBy = model.CreatedBy;
                cEARequisition.CreatedDate = DateTime.Now;
                cEARequisition.UpdatedBy = model.CreatedBy;
                cEARequisition.UpdatedDate = DateTime.Now;
                cEARequisition.CEARequisitionDetails = new System.Data.Objects.DataClasses.EntityCollection<CEARequisitionDetail>();

                foreach (var item in model.CEAItems)
                {
                    CEARequisitionDetail cEARequisitionDetail = new CEARequisitionDetail();
                    cEARequisitionDetail.ID = Guid.NewGuid();
                    cEARequisitionDetail.CEARequisitionId = cEARequisition.ID;
                    cEARequisitionDetail.LineNumber = item.LineNo;
                    cEARequisitionDetail.ItemNo = item.ItemNo;
                    cEARequisitionDetail.ItemDescription = item.ItemDescription;
                    cEARequisitionDetail.ItemType = item.IsContract ? "CONTRACT" : "PURCHASE";
                    cEARequisitionDetail.Qty = item.Qty;
                    cEARequisitionDetail.UnitCost = item.UnitCost;
                    cEARequisitionDetail.CreatedBy = model.CreatedBy;
                    cEARequisitionDetail.CreatedDate = DateTime.Now;
                    cEARequisitionDetail.UpdatedBy = model.CreatedBy;
                    cEARequisitionDetail.UpdatedDate = DateTime.Now;
                    cEARequisition.CEARequisitionDetails.Add(cEARequisitionDetail);
                }

                cEARequisition.Save<CEARequisition>();
                response.SetSuccess("RQN {0} success to post accpac", RQNNumber.Trim());
            }
            catch (Exception ex)
            {
                response.SetError(ex.Message);
            }

            return response;
        }

        public ResponseModel SaveAdditional(Guid CEAId, List<CEAItemModel> list)
        {
            var response = new ResponseModel();

            foreach (var item in list)
            {
                CEADetail _CEADetail = new CEADetail();
                _CEADetail.ID = Guid.NewGuid();
                _CEADetail.CEAId = CEAId;
                _CEADetail.ParentId = item.ReferenceId;
                _CEADetail.LineNumber = item.LineNo;
                _CEADetail.ItemNo = item.ItemNo;
                _CEADetail.ItemDescription = item.ItemDescription;
                _CEADetail.ItemType = REQUISITION_TYPE.PURCHASE;
                _CEADetail.Qty = item.Qty;
                _CEADetail.UnitCost = item.UnitCost;
                _CEADetail.GLAccount = item.GLAccount;
                _CEADetail.Location = item.Location;
                _CEADetail.UOM = item.UOM;
                _CEADetail.CCCODE = item.CCCODE;

                _CEADetail.CreatedBy = CurrentUser.NIKSITE;
                _CEADetail.CreatedDate = DateTime.Now;
                _CEADetail.UpdatedBy = CurrentUser.NIKSITE;
                _CEADetail.UpdatedDate = DateTime.Now;
                _CEADetail.Save<CEADetail>();
            }

            return response;
        }
        #endregion

    }
}
