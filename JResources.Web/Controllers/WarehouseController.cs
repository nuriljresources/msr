﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using JResources.BusinessLogic;
using JResources.Common;
using JResources.Common.Enum;
using JResources.Data;
using JResources.Data.Model;
using Omu.ValueInjecter;
using JResources.BusinessLogic.Accpac;
using JResources.Data.Model.Accpac;

namespace JResources.Web.Controllers
{
    public partial class WarehouseController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.MSR_ISSUED })]
        public virtual ActionResult IssuedRequest()
        {
            var model = new MSRIssuedModel();
            model.IssuedType = RequestStatus.ISSUED;


            MSRSearchModel MSRSearch = new MSRSearchModel();
            MSRSearch.IssuedType = RequestStatus.ISSUED;
            MSRSearch.IsLookup = true;
            
            ViewBag.MSRSearchModel = MSRSearch;
            ViewData.Model = model;
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.MSR_ISSUED })]
        public virtual ActionResult TransferRequest()
        {
            var model = new MSRIssuedModel();
            model.IssuedType = RequestStatus.TRANSFER;

            MSRSearchModel MSRSearch = new MSRSearchModel();
            MSRSearch.IssuedType = RequestStatus.TRANSFER;
            MSRSearch.IsLookup = true;

            ViewBag.MSRSearchModel = MSRSearch;
            ViewData.Model = model;
            return View("IssuedRequest");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.MSR_CANCEL })]
        public virtual ActionResult CancelRequest()
        {
            var model = new MSRIssuedModel();
            model.IssuedType = RequestStatus.CANCEL;

            MSRSearchModel MSRSearch = new MSRSearchModel();
            MSRSearch.IssuedType = RequestStatus.CANCEL;
            MSRSearch.IsLookup = true;

            var EnumCancelReason = EnumTable.GetByCode(EnumTable.EnumType.CANCEL_REASON);
            List<SelectListItem> listCancelReason = new List<SelectListItem>();
            foreach (var ItemCancelReason in EnumCancelReason)
            {
                listCancelReason.Add(new SelectListItem() {
                    Value = ItemCancelReason.EnumValue,
                    Text = ItemCancelReason.EnumLabel
                });
            }

            ViewBag.CancelReason = listCancelReason;
            ViewBag.MSRSearchModel = MSRSearch;
            ViewData.Model = model;
            return View("IssuedRequest");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.MSR_ISSUED, ROLE_CODE.MSR_CANCEL, ROLE_CODE.PROCUREMENT })]
        public virtual JsonResult GetDataIssuedByMSRId(Guid Id, string IssuedType)
        {
            MSRIssuedModel model = WarehouseLogic.GetDataByMSRId(Id, IssuedType);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Save Document Issued
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [JResourcesAuthorization(IS_AJAX = true, ROLE_CODES = new string[] { ROLE_CODE.MSR_ISSUED, ROLE_CODE.MSR_CANCEL, ROLE_CODE.PROCUREMENT })]
        public virtual JsonResult CreateIssued(MSRIssuedModel model)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                response = WarehouseLogic.CreateIssued(model);
            }
            catch (Exception ex)
            {
                Logger.Set(LoggerCategory.MSR_ISSUED, "Warehouse/CreateIssued", model, ex);
                response.SetError("Error Exception handling {0}", ex.Message);
            }

            if (response.IsSuccess)
            {
                foreach (var item in response.MessageList)
                {
                    this.AddNotification(item, NotificationType.SUCCESS);
                }                              
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [JResourcesAuthorization(IS_AJAX = true, ROLE_CODES = new string[] { ROLE_CODE.MSR_ISSUED, ROLE_CODE.PROCUREMENT })]
        public virtual JsonResult AccpacProcess(Guid IssuedId)
        {
            var response = WarehouseLogic.ProcessToAccpac(IssuedId);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Save Document Issued
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [JResourcesAuthorization(IS_AJAX = true, ROLE_CODES = new string[] { ROLE_CODE.MSR_ISSUED })]
        public virtual JsonResult ReverseTransfer(MSRIssuedModel model)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                response = WarehouseLogic.ReverseTransfer(model.ID);
            }
            catch (Exception ex)
            {
                Logger.Set(LoggerCategory.MSR_ISSUED, "Warehouse/ReverseTransfer", model, ex);
                response.SetError("Error Exception handling {0}", ex.Message);
            }

            if (response.IsSuccess)
            {
                foreach (var item in response.MessageList)
                {
                    this.AddNotification(item, NotificationType.SUCCESS);
                }
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.MSR_ISSUED, ROLE_CODE.MSR_CANCEL, ROLE_CODE.MSR_REVERSE_TRANSFER })]
        public virtual ActionResult ListRequestIssued()
        {
            var Model = new MSRIssuedSearchModel();
            List<CompanyDepartmentModel> CompanyDepartments = new List<CompanyDepartmentModel>() { new CompanyDepartmentModel { ID = Guid.Empty, CompanyName = "SELECT All COMPANY" } };
            CompanyDepartments.AddRange(CompanyLogic.GetFullCompanyWithDepartmentByCurrentRole(new string[] { ROLE_CODE.MSR_ENTRY, ROLE_CODE.MSR_ISSUED, ROLE_CODE.MSR_CANCEL, ROLE_CODE.MSR_APPROVAL }));

            ViewData.Model = Model;
            ViewBag.CompanyDepartments = CompanyDepartments;
            return View();
        }

        /// <summary>
        /// Modal View MSR Issued
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.MSR_ISSUED, ROLE_CODE.MSR_APPROVAL, ROLE_CODE.PROCUREMENT })]
        public virtual ActionResult MSRIssuedModal()
        {
            ViewData.Model = new MSRIssuedModel();

            List<EnumTable> MSRStatus = EnumTable.GetByCode(EnumTable.EnumType.STATUS_MSR).ToList();
            List<SelectListItem> StatusMSR = new List<SelectListItem>();
            MSRStatus = MSRStatus.Where(x =>
                x.EnumValue == Common.Enum.RequestStatus.REJECTED
                || x.EnumValue == Common.Enum.RequestStatus.APPROVED).ToList();

            foreach (var item in MSRStatus)
            {
                StatusMSR.Add(new SelectListItem()
                {
                    Value = item.EnumValue,
                    Text = item.EnumLabel
                });
            }

            ViewBag.StatusMSR = StatusMSR;
            return PartialView();
        }

        /// <summary>
        /// GetMSRIssuedModal
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual JsonResult GetMSRIssuedModal(Guid Id)
        {
            MSRIssuedModel model = WarehouseLogic.GetMSRIssuedId(Id);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get JSON MSRIssued
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult GetListMSRIssued(MSRIssuedSearchModel model)
        {
            model.ROLE_ACCESS = new string[] { ROLE_CODE.MSR_ISSUED };
            model.IssuedType = new string[] { RequestStatus.ISSUED, RequestStatus.TRANSFER, RequestStatus.CANCEL, RequestStatus.REVERSE_TRANSFER };
            model = WarehouseLogic.GetListIssued(model);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        
        #region FUEL REGION
        /// <summary>
        /// List Request To Purchase
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.FRF_ISSUED })]
        public virtual ActionResult ListFuelIssued()
        {
            var model = new FRFSearchModel();
            List<EnumTable> FRFStatus = new List<EnumTable>() { new EnumTable() { EnumValue = "", EnumLabel = "SELECT ALL STATUS" } };
            FRFStatus.AddRange(EnumTable.GetByCode(EnumTable.EnumType.STATUS_MSR).ToList());

            FRFStatus = FRFStatus.Where(x =>
                x.EnumValue == ""
                || x.EnumValue == Common.Enum.RequestStatus.APPROVED).ToList();

            List<CompanyDepartmentModel> CompanyDepartments = new List<CompanyDepartmentModel>() { new CompanyDepartmentModel { ID = Guid.Empty, CompanyName = "SELECT All COMPANY" } };
            CompanyDepartments.AddRange(CompanyLogic.GetFullCompanyWithDepartmentByCurrentRole(new string[] { ROLE_CODE.FRF_APPROVAL }));

            ViewData.Model = model;
            ViewBag.FRFStatus = FRFStatus;
            ViewBag.CompanyDepartments = CompanyDepartments;
            return View();
        }

        [HttpPost]
        public virtual JsonResult GetListFRFIssued(FRFSearchModel model)
        {
            model.ROLE_ACCESS = new string[] { ROLE_CODE.FRF_ISSUED };
            model.MSR_STATUS = new string[] { Common.Enum.RequestStatus.APPROVED };
            model = FRFLogic.GetListFRF(model);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public virtual JsonResult FuelIssued(FRFModel model)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                response = WarehouseLogic.IssuedFRF(model);
            }
            catch (Exception ex)
            {
                Logger.Set(LoggerCategory.FRF_ISSUED, "Warehouse/FuelIssued", model, ex);
                response.SetError("Error Exception handling {0}", ex.Message);
            }

            if (response.IsSuccess)
            {
                foreach (var item in response.MessageList)
                {
                    this.AddNotification(item, NotificationType.SUCCESS);
                }
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion

        [HttpGet]
        public virtual JsonResult TestPost(string no)
        {
            MSRIssued msrIssued = MSRIssued.GetByIssuedNo(no);
            MSRIssuedModel issuedModel = WarehouseLogic.GetMSRIssuedId(msrIssued.ID);

            VM0050 vm0050 = new VM0050();
            if (!string.IsNullOrEmpty(msrIssued.MSR.WorkOrderNo))
            {
                vm0050 = WorkOrderLogic.GetWorkOrderAccpac(msrIssued.MSR.WorkOrderNo);
            }

            var response = VMDocumentLogic.Save(issuedModel, vm0050);

            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
