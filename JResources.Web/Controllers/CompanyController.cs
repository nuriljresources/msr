﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JResources.Data;
using JResources.Data.Model;

using Omu.ValueInjecter;

namespace JResources.Web.Controllers
{
    public partial class CompanyController : Controller
    {
        public virtual ActionResult Index()
        {
            var model = Company.GetAll();
            ViewData.Model = model;
            return View();
        }

        public virtual ActionResult Detail(Guid Id)
        {
            var model = new CompanyModel();
            if (Id != Guid.Empty)
            {
                var company = Company.GetById(Id);
                if (company != null)
                {
                    model.InjectFrom(company);
                }
            }

            ViewData.Model = model;
            return View();
        }

        [HttpPost]
        public virtual ActionResult Detail(CompanyModel model)
        {
            var response = new ResponseModel(false);
            Company company = new Company();

            if (ModelState.IsValid)
            {
                if (model.ID != Guid.Empty)
                {
                    company = Company.GetById(model.ID);
                    if (company != null)
                    {
                        company.CompanyName = model.CompanyName;
                        company.CompanyCode = model.CompanyCode;
                        company.DocumentCode = model.DocumentCode;
                        company.ADCode = model.ADCode;
                        company.IsEnabled = model.IsEnabled;
                        company.Save<Company>();
                        response.SetSuccess("Company {0} has been update", model.CompanyName);
                        //return RedirectToAction(MVC.Company.Index());
                    }
                    else
                    {
                        response.SetError("Company {0} not found", company.CompanyName);
                    }
                }
                else
                {
                    company.InjectFrom(model);
                    company.ID = Guid.NewGuid();
                    company.DocumentCode = model.DocumentCode;
                    var connectionStr = Newtonsoft.Json.JsonConvert.SerializeObject(model.ConnectionString);
                    company.ConnectionString = connectionStr;
                    company.Save<Company>();
                    response.SetSuccess("Company {0} has been insert", company.CompanyName);
                    //return RedirectToAction(MVC.Company.Index());
                }
            }
            else
            {
                response.SetError(FunctionWebHelpers.GetErrorModelState(ModelState));
            }

            this.AddNotification(response);
            if(response.IsSuccess)
                return RedirectToAction(MVC.Company.Index());

            return View(company);
        }


        [HttpGet]
        public virtual ActionResult Delete(Guid Id)
        {
            Company.DeleteById(Id);
            return RedirectToAction("Index");
        }

    }
}