﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JResources.Data;
using JResources.Data.Model;
using JResources.BusinessLogic;
using JResources.Common;
using System.Text.RegularExpressions;
using Omu.ValueInjecter;
using JResources.Common.Enum;
using System.Web.Hosting;

namespace JResources.Web.Controllers
{
    public partial class MSRController : Controller
    {
        #region LEVEL PAGE
        /// <summary>
        /// List Request Material
        /// </summary>
        /// <returns></returns>
        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.MSR_ENTRY, ROLE_CODE.MSR_APPROVAL, ROLE_CODE.MSR_APPROVAL_GM })]
        public virtual ActionResult Index()
        {
            var model = new MSRSearchModel();
            List<EnumTable> MSRStatus = new List<EnumTable>() { new EnumTable() { EnumValue = "", EnumLabel = "SELECT ALL STATUS" } };
            MSRStatus.AddRange(EnumTable.GetByCode(EnumTable.EnumType.STATUS_MSR).ToList());

            MSRStatus = MSRStatus.Where(x =>
                x.EnumValue == ""
                || x.EnumValue == Common.Enum.RequestStatus.NEW
                || x.EnumValue == Common.Enum.RequestStatus.CANCEL
                || x.EnumValue == Common.Enum.RequestStatus.REJECTED
                || x.EnumValue == Common.Enum.RequestStatus.APPROVED
                || x.EnumValue == Common.Enum.RequestStatus.PARTIAL_APPROVED
                || x.EnumValue == Common.Enum.RequestStatus.PARTIAL_COMPLETED
                || x.EnumValue == Common.Enum.RequestStatus.PARTIAL_TRANSFER
                || x.EnumValue == Common.Enum.RequestStatus.FULL_TRANSFER
                || x.EnumValue == Common.Enum.RequestStatus.PARTIAL_ISSUED
                || x.EnumValue == Common.Enum.RequestStatus.PARTIAL_PURCHASING
                || x.EnumValue == Common.Enum.RequestStatus.FULL_PURCHASING
                || x.EnumValue == Common.Enum.RequestStatus.COMPLETED).ToList();

            List<CompanyDepartmentModel> CompanyDepartments = new List<CompanyDepartmentModel>() { new CompanyDepartmentModel { ID = Guid.Empty, CompanyName = "SELECT All COMPANY" } };
            CompanyDepartments.AddRange(CompanyLogic.GetFullCompanyWithDepartmentByCurrentRole(new string[] { ROLE_CODE.MSR_ENTRY, ROLE_CODE.MSR_ISSUED, ROLE_CODE.MSR_APPROVAL, ROLE_CODE.MSR_APPROVAL_GM }));
            BindData(true);

            string msrStatus = Request.QueryString["status"];
            if (msrStatus != null)
            {
                if (msrStatus == RequestStatus.NEW)
                {
                    model.MSR_STATUS = new string[] { RequestStatus.NEW };
                    model.IsSearchFilter = true;
                }

                if (msrStatus == RequestStatus.APPROVED)
                {
                    model.MSR_STATUS = new string[] { RequestStatus.APPROVED };
                    model.IsSearchFilter = true;
                }
            }

            ViewData.Model = model;
            ViewBag.MSRStatus = MSRStatus;
            ViewBag.CompanyDepartments = CompanyDepartments;
            return View();
        }


        /// <summary>
        /// Page List Approve
        /// </summary>
        /// <returns></returns>
        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.MSR_APPROVAL, ROLE_CODE.MSR_APPROVAL_GM })]
        public virtual ActionResult ListApprove()
        {
            var model = new MSRSearchModel();
            List<CompanyDepartmentModel> CompanyDepartments = new List<CompanyDepartmentModel>() { new CompanyDepartmentModel { ID = Guid.Empty, CompanyName = "SELECT All COMPANY" } };

            CompanyDepartments.AddRange(CompanyLogic.GetFullCompanyWithDepartmentByCurrentRole(new string[] { ROLE_CODE.MSR_ENTRY, ROLE_CODE.MSR_ISSUED, ROLE_CODE.MSR_APPROVAL, ROLE_CODE.MSR_APPROVAL_GM }));

            var MSRNo = CookieManager.Get(COOKIES_NAME.URL_DOCUMENTNO);
            if (!string.IsNullOrEmpty(MSRNo))
            {
                MSR MSR = MSR.GetByNo(MSRNo);
                model.MSRIdModal = MSR.ID;
                CookieManager.Delete(COOKIES_NAME.URL_CODE);
                CookieManager.Delete(COOKIES_NAME.URL_DOCUMENTNO);
            }

            ViewData.Model = model;
            ViewBag.CompanyDepartments = CompanyDepartments;
            return View();
        }

        /// <summary>
        /// Detail Request Material
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [JResourcesAuthorization(ROLE_CODE = ROLE_CODE.MSR_ENTRY)]
        public virtual ActionResult Detail(Guid Id)
        {
            var model = new MSRModel();

            if (Id != Guid.Empty)
            {
                model = MSRLogic.GetMSRModel(Id);
            }

            model.Requestor = CurrentUser.Model;

            var RequestNew = EnumTable.GetDescriptionByCodeValue(EnumTable.EnumType.STATUS_MSR, Common.Enum.RequestStatus.NEW);
            model.StatusRequest = RequestNew.EnumValue;
            model.StatusRequestLabel = RequestNew.EnumLabel;

            List<SelectListItem> ListMSRType = JResources.Common.EnumUtils<JResources.Common.MSRType>.GetDropDownList();
            List<SelectListItem> ListDepartment = RoleLogic.GetListDepartmentUserByRole(CurrentUser.USERNAME, CurrentUser.COMPANY, new string[] { ROLE_CODE.MSR_ENTRY });
            List<Guid> DeptIdMaintenances = Department.GetByCurrentCompany().Where(x => x.MaintenanceTypeCode != "").Select(x => x.ID).ToList();
            BindData();
            ViewBag.ListMSRType = ListMSRType;
            ViewBag.ListDepartment = ListDepartment;
            ViewBag.DeptIdMaintenances = DeptIdMaintenances;

            ViewData.Model = model;
            return View();
        }

        /// <summary>
        /// Detail Request Material
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [JResourcesAuthorization(ROLE_CODE = ROLE_CODE.MSR_ROP_ENTRY)]
        public virtual ActionResult DetailReOrderPoint(Guid Id)
        {
            var model = new MSRModel();
            model.IsReOrderPoint = true;
            model.MSRType = ((int)MSRType.Inventory).ToString();

            if (Id != Guid.Empty)
            {
                model = MSRLogic.GetMSRModel(Id);
            }

            model.Requestor = CurrentUser.Model;

            var RequestNew = EnumTable.GetDescriptionByCodeValue(EnumTable.EnumType.STATUS_MSR, Common.Enum.RequestStatus.NEW);
            model.StatusRequest = RequestNew.EnumValue;
            model.StatusRequestLabel = RequestNew.EnumLabel;

            List<SelectListItem> ListMSRType = JResources.Common.EnumUtils<JResources.Common.MSRType>.GetDropDownList();
            List<SelectListItem> ListDepartment = RoleLogic.GetListDepartmentUserByRole(CurrentUser.USERNAME, CurrentUser.COMPANY, new string[] { ROLE_CODE.MSR_ENTRY });
            ListDepartment = ListDepartment.Where(x => x.Text == "SCM").ToList();

            List<Guid> DeptIdMaintenances = Department.GetByCurrentCompany().Where(x => x.MaintenanceTypeCode != "").Select(x => x.ID).ToList();
            BindData(false, true);

            ViewBag.ListMSRType = ListMSRType;
            ViewBag.ListDepartment = ListDepartment;
            ViewBag.DeptIdMaintenances = DeptIdMaintenances;

            ViewData.Model = model;

            return View("Detail");
        }

        /// <summary>
        /// For Detail Modal MSR
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult MSRModal()
        {
            List<SelectListItem> StatusMSR = new List<SelectListItem>();
            List<EnumTable> MSRStatus = EnumTable.GetByCode(EnumTable.EnumType.STATUS_MSR).ToList();

            MSRStatus = MSRStatus.Where(x =>
                x.EnumValue == Common.Enum.RequestStatus.REJECTED
                || x.EnumValue == Common.Enum.RequestStatus.APPROVED
                || x.EnumValue == Common.Enum.RequestStatus.REVISION
                ).ToList();

            foreach (var item in MSRStatus)
            {
                StatusMSR.Add(new SelectListItem()
                {
                    Value = item.EnumValue,
                    Text = item.EnumLabel
                });
            }

            ViewBag.StatusMSR = StatusMSR;
            return PartialView();
        }

        [HttpGet]
        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.MSR_ENTRY, ROLE_CODE.MSR_APPROVAL, ROLE_CODE.MSR_ISSUED })]
        public virtual ActionResult MSRTimeline(Guid Id)
        {
            MSRHistoryModel model = MSRLogic.GetMSRHistoryModel(Id);
            ViewData.Model = model;
            return View();
        }

        /// <summary>
        /// Page List Approve
        /// </summary>
        /// <returns></returns>
        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.MSR_APPROVAL, ROLE_CODE.MSR_ISSUED })]
        public virtual ActionResult ListCancel()
        {
            var Model = new MSRIssuedSearchModel();
            Model.IssuedType = new string[] { RequestStatus.CANCEL };
            List<CompanyDepartmentModel> CompanyDepartments = new List<CompanyDepartmentModel>() { new CompanyDepartmentModel { ID = Guid.Empty, CompanyName = "SELECT All COMPANY" } };
            CompanyDepartments.AddRange(CompanyLogic.GetFullCompanyWithDepartmentByCurrentRole(new string[] { ROLE_CODE.MSR_ENTRY, ROLE_CODE.MSR_ISSUED, ROLE_CODE.MSR_APPROVAL }));

            ViewData.Model = Model;
            ViewBag.CompanyDepartments = CompanyDepartments;
            return View();
        }
        #endregion

        #region LEVEL POST JSON
        /// <summary>
        /// Get JSON MSR
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult GetListMSR(MSRSearchModel model)
        {
            model.ROLE_ACCESS = new string[] { ROLE_CODE.MSR_ENTRY, ROLE_CODE.MSR_APPROVAL, ROLE_CODE.MSR_APPROVAL_GM, ROLE_CODE.MSR_ISSUED };
            model = MSRLogic.GetListMSR(model);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get JSON MSR Ready For Approve
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult GetListMSRReadyApprove(MSRSearchModel model)
        {
            model.ROLE_ACCESS = new string[] { ROLE_CODE.MSR_APPROVAL, ROLE_CODE.MSR_APPROVAL_GM };
            model.MSR_STATUS = new string[] { 
                Common.Enum.RequestStatus.NEW, Common.Enum.RequestStatus.PARTIAL_APPROVED
            };

            model.IsExcludeDataMigration = true;
            model = MSRLogic.GetListMSR(model);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get JSON MSR Ready For Approve
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult GetListMSRApprove(MSRSearchModel model)
        {
            model.ROLE_ACCESS = new string[] { ROLE_CODE.MSR_ISSUED, ROLE_CODE.MSR_CANCEL };
            model.MSR_STATUS = new string[] { 
                Common.Enum.RequestStatus.APPROVED,
                Common.Enum.RequestStatus.PARTIAL_COMPLETED, 
                Common.Enum.RequestStatus.PARTIAL_TRANSFER, 
                Common.Enum.RequestStatus.FULL_TRANSFER, 
                Common.Enum.RequestStatus.PARTIAL_ISSUED,
                Common.Enum.RequestStatus.PARTIAL_PURCHASING,
                Common.Enum.RequestStatus.FULL_PURCHASING
            };

            model.IsExcludeDataMigration = true;
            model = MSRLogic.GetListMSR(model);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get JSON MSR Ready For Approve
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult GetListMSRRequisition(MSRSearchModel model)
        {
            model.ROLE_ACCESS = new string[] { ROLE_CODE.PROCUREMENT };
            model.IsRequisition = true;
            model.MSR_STATUS = new string[] { 
                Common.Enum.RequestStatus.APPROVED, 
                Common.Enum.RequestStatus.PARTIAL_COMPLETED,
                Common.Enum.RequestStatus.PARTIAL_TRANSFER,
                Common.Enum.RequestStatus.FULL_TRANSFER,
                Common.Enum.RequestStatus.PARTIAL_ISSUED,
                Common.Enum.RequestStatus.PARTIAL_PURCHASING,
                Common.Enum.RequestStatus.FULL_PURCHASING
            };

            model.IsExcludeDataMigration = true;
            model = MSRLogic.GetListMSR(model);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get JSON MSRIssued
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult GetListIssuedCancel(MSRIssuedSearchModel model)
        {
            model.ROLE_ACCESS = new string[] { ROLE_CODE.MSR_ISSUED, ROLE_CODE.MSR_APPROVAL };
            model.IssuedType = new string[] { RequestStatus.CANCEL };
            model.IsPendingCancel = true;

            model = WarehouseLogic.GetListIssued(model);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Post JSON For Create MSR
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult Save(MSRModel model)
        {
            var response = new ResponseModel();
            try
            {
                if (model.ID == Guid.Empty)
                {
                    response = MSRLogic.CreateMSR(model);
                }
                else
                {
                    response = MSRLogic.EditMSR(model);
                }
            }
            catch (Exception ex)
            {
                Logger.Set(LoggerCategory.MSR_CREATE, "MSR/Save", model, ex);
                response.SetError(ex.Message);
            }

            if (response.IsSuccess)
            {
                this.AddNotification(response.Message, NotificationType.SUCCESS);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// For Detail Modal MSR
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual JsonResult GetMSRModal(Guid Id)
        {
            MSRModel model = null;
            try
            {
                model = MSRLogic.GetMSRModel(Id);
            }
            catch (Exception ex)
            {
                Logger.Set(LoggerCategory.MSR_APPROVE, "MSR/GetMSRModal", model, ex);
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public virtual JsonResult ApproveMSR(MSRModel model)
        {
            var response = new ResponseModel();
            try
            {
                response = MSRLogic.SetActionMSRStatusApproval(model);
            }
            catch (Exception ex)
            {
                Logger.Set(LoggerCategory.MSR_APPROVE, "MSR/ApproveMSR", model, ex);
                response.SetError(ex.Message);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public virtual JsonResult ApproveCancel(MSRIssuedModel model)
        {
            var response = new ResponseModel();
            try
            {
                response = WarehouseLogic.IssuedCancelApprove(model.ID, model.CancelRequest);
            }
            catch (Exception ex)
            {
                Logger.Set(LoggerCategory.MSR_APPROVE, "MSR/ApproveCancel", model, ex);
                response.SetError(ex.Message);
            }

            if (response.IsSuccess)
            {
                foreach (var item in response.MessageList)
                {
                    this.AddNotification(item, NotificationType.SUCCESS);
                }
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [JResourcesAuthorization(IS_AJAX = true, ROLE_CODE = ROLE_CODE.MSR_ENTRY)]
        [HttpGet]
        public virtual JsonResult Dismantle(Guid Id)
        {
            var response = new ResponseModel();
            try
            {
                response = MSRLogic.DismantleProcess(Id);
            }
            catch (Exception ex)
            {
                Logger.Set(LoggerCategory.MSR_CREATE, "MSR/Dismantle", "DismantleProcess " + Id.ToString(), ex);
                response.SetError(ex.Message);
            }

            //if (response.IsSuccess)
            //{
            //    this.AddNotification(response.Message, NotificationType.SUCCESS);
            //}

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonResult DismantleTest()
        {
            var response = new ResponseModel();
            var Id = MSR.GetByNo("MSR-SPP/2018/6981").ID;

            try
            {
                response = MSRLogic.DismantleProcess(Id);
            }
            catch (Exception ex)
            {
                Logger.Set(LoggerCategory.MSR_CREATE, "MSR/Dismantle", "DismantleProcess " + Id.ToString(), ex);
                response.SetError(ex.Message);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        //[JResourcesAuthorization(IS_AJAX = true, ROLE_CODE = ROLE_CODE.MSR_ISSUED)]
        [HttpGet]
        public virtual JsonResult RequestRequisition(Guid Id)
        {
            var response = new ResponseModel();
            try
            {
                MSR msrUpdate = MSR.GetById(Id);
                msrUpdate.RequisitionDate = DateTime.Now;
                msrUpdate.Save<MSR>();
                response.SetSuccess("MSR can be post to purchase");
            }
            catch (Exception ex)
            {
                Logger.Set(LoggerCategory.MSR_CREATE, "MSR/RequestRequisition", "RequestRequisition " + Id.ToString(), ex);
                response.SetError(ex.Message);
            }

            //if (response.IsSuccess)
            //{
            //    this.AddNotification(response.Message, NotificationType.SUCCESS);
            //}

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region LEVEL FUNCTION
        public void BindData(bool IsIncludeAllJobType = false, bool IsSRO = false)
        {
            List<SelectListItem> ListJobTypeNonCMMS = new List<SelectListItem>();
            List<LeadTimeModel> ListLeadTime = new List<LeadTimeModel>();

            string[] listJobTypeCodes = new string[] { "P01", "P02", "P03", "P04", "SR" };
            if (IsIncludeAllJobType)
            {
                listJobTypeCodes = new string[] { "P01", "P02", "P03", "P04", "P05", "P06", "P07", "P08", "P09", "P10", "SR" };
            }

            if(IsSRO)
            {
                listJobTypeCodes = new string[] { "SR" };
            }

            var CompanyCode = Common.CookieManager.Get(COOKIES_NAME.USER_COMPANY_CODE);
            if (CompanyCode == COMPANY_CODE.ASA || CompanyCode == COMPANY_CODE.GSM || CompanyCode == COMPANY_CODE.LANUT)
            {
                #region JobType Manual
                ListJobTypeNonCMMS.Add(new SelectListItem()
                {
                    Value = "P01",
                    Text = "P01-UNSCHEDULE MACHINE DOWN / TOP PRIORITY*"
                });

                var itemLeadTime = MSRLogic.GetLeadTimeByCode("P01");
                ListLeadTime.Add(itemLeadTime);

                ListJobTypeNonCMMS.Add(new SelectListItem()
                {
                    Value = "P02",
                    Text = "P02-SCHEDULED IMMINENT FAILURE / URGENT*"
                });

                itemLeadTime = MSRLogic.GetLeadTimeByCode("P02");
                ListLeadTime.Add(itemLeadTime);

                ListJobTypeNonCMMS.Add(new SelectListItem()
                {
                    Value = "P03",
                    Text = "P03-SCHEDULED BACKLOG / NORMAL*"
                });

                itemLeadTime = MSRLogic.GetLeadTimeByCode("P03");
                ListLeadTime.Add(itemLeadTime);

                ListJobTypeNonCMMS.Add(new SelectListItem()
                {
                    Value = "P04",
                    Text = "P04-SCHEDULED PCR / SCHEDULED*"
                });

                itemLeadTime = MSRLogic.GetLeadTimeByCode("P04");
                ListLeadTime.Add(itemLeadTime);

                ListJobTypeNonCMMS.Add(new SelectListItem()
                {
                    Value = "SR",
                    Text = "STOCK REPLENISHMENT"
                });

                itemLeadTime = MSRLogic.GetLeadTimeByCode("SR");
                ListLeadTime.Add(itemLeadTime);
                #endregion
            }
            else
            {
                #region JobType From Accpac
                var vmmtype = AccpacDataContext.CurrentContext.VMTYPEs.Where(x => listJobTypeCodes.Contains(x.TXTYPE));
                foreach (var item in vmmtype)
                {
                    ListJobTypeNonCMMS.Add(new SelectListItem()
                    {
                        Value = item.TXTYPE.Trim(),
                        Text = item.TXDESC.Trim()
                    });

                    var itemLeadTime = MSRLogic.GetLeadTimeByCode(item.TXTYPE.Trim());
                    ListLeadTime.Add(itemLeadTime);
                }
                #endregion
            }

            ViewBag.ListJobTypeNonCMMS = ListJobTypeNonCMMS;
            ViewBag.ListLeadTime = ListLeadTime;
        }
        #endregion

    }
}
