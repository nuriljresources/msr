﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using JResources.Data;
using JResources.Data.Model;
using JResources.Common;
using Newtonsoft.Json;

namespace JResources.Web.Controllers
{
    public partial class ShortenController : Controller
    {
        public virtual ActionResult ByCode(string urlCode)
        {
            var response = new ResponseModel();

            var URL_REDIRECT = MVC.Error.NotFound();
            StringBuilder sbMessage = new StringBuilder();
            if (!string.IsNullOrEmpty(urlCode))
            {
                URLShorten urlShorten = URLShorten.GetByCode(urlCode);
                if (urlShorten != null)
                {
                    #region MSR NEW REDIRECT
                    if (urlShorten.UrlType == JResources.Common.Enum.URLShortenType.MSR_NEW)
                    {
                        URL_REDIRECT = MVC.MSR.ListApprove();
                        MSR MSR = MSR.GetByNo(urlShorten.DataModel);
                        if (MSR != null)
                        {
                            if (MSR.MSRStatus == Common.Enum.RequestStatus.NEW)
                            {
                                CookieManager.Set(COOKIES_NAME.URL_CODE, urlCode, false);
                                CookieManager.Set(COOKIES_NAME.URL_DOCUMENTNO, MSR.MSRNo, false);
                                return RedirectToAction(URL_REDIRECT);
                            }
                            else
                            {
                                response.SetError("MSR {0} already in approved", MSR.MSRNo);
                            }
                        }
                        else
                        {
                            response.SetError("MSR not found");
                        }
                    }
                    #endregion

                    #region REPORT TEST
                    if (urlShorten.UrlType == JResources.Common.Enum.URLShortenType.REPORT)
                    {
                        return Redirect(string.Format("{0}/Download/{1}", SiteSettings.BASE_URL, urlShorten.DataModel));
                    }
                    #endregion
                }
                else
                {
                    response.SetError("URL NOT FOUND");
                }
            }
            else
            {
                response.SetError("URL NOT DEFINE");
            }



            return RedirectToAction(URL_REDIRECT);
        }

    }
}
