﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JResources.BusinessLogic;
using JResources.Data;
using JResources.Data.Model;

namespace JResources.Web.Controllers
{
    public partial class LookupController : Controller
    {
        #region ITEM LOOKUP
        /// <summary>
        /// Lookup Item
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult ItemPartLookup()
        {
            ViewBag.ItemCEALookup = false;
            return PartialView();
        }

        public virtual ActionResult ItemPartCEALookup()
        {
            ViewBag.ItemCEALookup = true;
            return PartialView("ItemPartLookup");
        }

        /// <summary>
        /// Json Data Lookup Item
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult ItemPartLookup(PartItemSearchModel model)
        {
            var result = ItemLogic.GetItemList(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult ItemPart()
        {
            String query = string.Empty;

            if (Request.QueryString != null && !String.IsNullOrEmpty(Request.QueryString["query"]))
            {
                query = Request.QueryString["query"];
            }

            ItemSearchModel result = new ItemSearchModel();
            List<ItemPartModel> list = new List<ItemPartModel>();
            var queryData = AccpacDataContext.CurrentContext.ICITEMs.Where(x => x.INACTIVE == 0);

            if (!string.IsNullOrEmpty(query))
            {
                queryData = queryData.Where(x => x.ITEMNO.Contains(query) || x.FMTITEMNO.Contains(query) || x.DESC.Contains(query));
            }

            var resultData = queryData.Take(10).ToList();
            if (resultData != null && resultData.Count() > 0)
            {
                foreach (var item in resultData)
                {
                    result.suggestions.Add(new ItemDetail() { value = String.Format("{0}-{1}", item.ITEMNO.Trim(), item.DESC.Trim()), ItemNo = item.ITEMNO.Trim(), Description = item.DESC.Trim() });
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region LOCATION LOOKUP
        /// <summary>
        /// Item Location Page View
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult ItemLocationLookup()
        {
            return PartialView();
        }        

        public virtual ActionResult LocationStorageLookup()
        {
            return PartialView();
        }

        [HttpPost]
        public virtual JsonResult LocationStorageLookup(SearchModel model)
        {
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region WORK ORDER LOOKUP
        /// <summary>
        /// Work Order Lookup Page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult WorkOrderLookup()
        {
            return PartialView();
        }

        /// <summary>
        /// JSON Get Work Order Data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult WorkOrderLookup(WorkOrderSearchModel model)
        {
            var result = WorkOrderLogic.SearchWorkOrder(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region MSR APPROVAL LOOKUP
        /// <summary>
        /// MSR Approval Lookup
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult MSRApproveLookup()
        {
            return PartialView();
        }

        /// <summary>
        /// JSON Get List MSR Approve Ready To Issued
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual JsonResult GetMSRApproveLookup(MSRSearchModel model)
        {
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region COST CODE LOOKUP
        /// <summary>
        /// Cost Code Lookup
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult CostCodeLookup()
        {
            return PartialView();
        }

        /// <summary>
        /// Cost Code Lookup
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult CostCodeLookup(SearchModel model)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            IQueryable<GLAMF> glamfs = GLAMF.GetAll();

            if (model.IsSearchFilter &&!string.IsNullOrEmpty(model.MoreCriteria))
            {
                Guid departmentId = Guid.Parse(model.MoreCriteria);

                // REQUEST PAK SUPRIADI UNTUK DI REMOVE FILTERNYA
                // Minta dipasang lagi filternya sesuai CR : ITCR-JRN/2020/0009
                Department department = Department.GetById(departmentId);
                if (department != null && !string.IsNullOrEmpty(department.CategoryCode))
                {
                    glamfs = glamfs.Where(x => x.ACCTID.Substring(0, 2) == department.CategoryCode);
                }
            }

            if (!string.IsNullOrEmpty(model.SearchCriteria))
            {
                glamfs = glamfs.Where(x => x.ACCTID.Contains(model.SearchCriteria) 
                    || x.ACCTDESC.Contains(model.SearchCriteria)
                    || x.ACCTFMTTD.Contains(model.SearchCriteria)
                    );
            }

            model.SetPager(glamfs.Count());
            List<GLAMF> listItem = new List<GLAMF>();
            listItem = glamfs.OrderBy(x => x.ACCTID).Skip((model.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
            .Take(Common.SiteSettings.PAGE_SIZE_PAGING).ToList();

            foreach (var item in listItem)
            {
                SelectListItem itemSelect = new SelectListItem();
                itemSelect.Text = item.ACCTDESC.Trim();
                itemSelect.Value = item.ACCTFMTTD.Trim();
                
                model.ListDataGeneral.Add(itemSelect);
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region USER LOOKUP
        [HttpGet]
        public virtual ActionResult UserLookup()
        {

            return PartialView();
        }

        [HttpPost]
        public virtual JsonResult UserLookup(UserSearchModel model)
        {
            var users = UserLogin.GetAll();
            var company = CurrentUser.GetCurrentCompany();

            users = users.Where(x => x.Department.CompanyId == company.ID);
            if (!string.IsNullOrEmpty(model.Name))
            {
                users = users.Where(x => x.FullName.Contains(model.Name));
            }

            if (!string.IsNullOrEmpty(model.NIKSITE))
            {
                users = users.Where(x => x.NIKSite.Contains(model.NIKSITE));
            }

            model.SetPager(users.Count());
            List<UserLogin> listData = users.OrderByDescending(x => x.CreatedDate)
                .Skip((model.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
                .Take(Common.SiteSettings.PAGE_SIZE_PAGING).ToList();

            foreach (var item in listData)
            {
                var _detail = UserLogic.GetByUserId(item.ID);
                model.ListData.Add(_detail);
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region VENDOR OFFER LOOKUP
        public virtual ActionResult VendorOfferLookup()
        {
            return PartialView();
        }
        #endregion

        #region ENUMTABLE
        public virtual ActionResult CBEEnumTableLookup()
        {
            return PartialView();
        }

        #endregion

        #region SUPPLIER WINNER
        public virtual ActionResult CBESupplierWinnerLookup()
        {
            return PartialView();
        }

        #endregion

        #region SUPPLIER LOOKUP
        /// <summary>
        /// Lookup Item
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult SupplierLookup()
        {
            //ViewBag.ItemCEALookup = false;
            return PartialView();
        }
        public virtual ActionResult CurrencyLookup()
        {
            //ViewBag.ItemCEALookup = false;
            return PartialView();
        }

        [HttpPost]
        public virtual JsonResult SupplierLookup(SupplierSearchModel model)
        {
            var result = SupplierLogic.GetSupplierList(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public virtual JsonResult CurrencyLookup(CurrencySearchModel model)
        {
            var result = CurrencyLogic.GetCurrencyList(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        #endregion
    }
}
