﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Omu.ValueInjecter;
using JResources.Common;
using JResources.Data;
using JResources.Data.Model;
using JResources.Data.Model.Report;
using System.IO;
using JResources.Common.Enum;
using System.Text;
using System.Text.RegularExpressions;
using JResources.BusinessLogic;

namespace JResources.Web.Controllers
{
    [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.REPORT })]
    public partial class ReportController : Controller
    {
        public virtual ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// MSR Status Page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult MSRStatusOld()
        {
            MSRSearchModel model = new MSRSearchModel();

            List<SelectListItem> listDepartment = new List<SelectListItem>();
            var departmenst = Department.GetByCurrentCompany();
            listDepartment.Add(new SelectListItem()
            {
                Value = Guid.Empty.ToString(),
                Text = "Select All",
                Selected = true
            });

            foreach (var item in departmenst)
            {
                listDepartment.Add(new SelectListItem()
                {
                    Value = item.ID.ToString(),
                    Text = item.DepartmentName
                });
            }

            ViewBag.ListDepartment = listDepartment;
            ViewData.Model = model;
            return View();
        }

        /// <summary>
        /// MSR Status Page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult MSRStatus()
        {
            MSRSearchModel model = new MSRSearchModel();
            List<SelectListItem> MSRStatus = new List<SelectListItem>() { new SelectListItem() { Value = "", Text = "SELECT ALL STATUS", Selected = true } };
            var STATUS_MSRs = EnumTable.GetByCode(EnumTable.EnumType.STATUS_MSR).ToList();
            STATUS_MSRs = STATUS_MSRs.Where(x =>
                x.EnumValue == ""
                || x.EnumValue == Common.Enum.RequestStatus.NEW
                || x.EnumValue == Common.Enum.RequestStatus.CANCEL
                || x.EnumValue == Common.Enum.RequestStatus.REJECTED
                || x.EnumValue == Common.Enum.RequestStatus.APPROVED
                || x.EnumValue == Common.Enum.RequestStatus.PARTIAL_APPROVED
                || x.EnumValue == Common.Enum.RequestStatus.PARTIAL_COMPLETED
                || x.EnumValue == Common.Enum.RequestStatus.PARTIAL_TRANSFER
                || x.EnumValue == Common.Enum.RequestStatus.FULL_TRANSFER
                || x.EnumValue == Common.Enum.RequestStatus.PARTIAL_ISSUED
                || x.EnumValue == Common.Enum.RequestStatus.PARTIAL_PURCHASING
                || x.EnumValue == Common.Enum.RequestStatus.FULL_PURCHASING
                || x.EnumValue == Common.Enum.RequestStatus.COMPLETED).ToList();

            foreach (var item in STATUS_MSRs)
            {
                MSRStatus.Add(new SelectListItem() { Value = item.EnumValue, Text = item.EnumLabel });
            }            

            List<SelectListItem> listDepartment = new List<SelectListItem>();
            var departmenst = Department.GetByCurrentCompany();
            listDepartment.Add(new SelectListItem()
            {
                Value = Guid.Empty.ToString(),
                Text = "Select All",
                Selected = true
            });

            foreach (var item in departmenst)
            {
                listDepartment.Add(new SelectListItem()
                {
                    Value = item.ID.ToString(),
                    Text = item.DepartmentName
                });
            }

            ViewBag.MSRStatus = MSRStatus.OrderBy(x => x.Text).ToList();
            ViewBag.ListDepartment = listDepartment;
            ViewData.Model = model;
            return View();
        }

        /// <summary>
        /// MSR Status Post Execution
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult GetMSRStatus(MSRSearchModel param)
        {
            ResponseModel response = new ResponseModel();

            try
            {
                var CurrentCompanyCode = CookieManager.Get(COOKIES_NAME.USER_COMPANY_CODE);
                Company company = Company.GetByCode(CurrentCompanyCode);
                var user = UserLogic.GetByNIK(CurrentUser.NIKSITE);
                if (user != null && !string.IsNullOrEmpty(user.Email))
                {
                    param.MoreCriteria = user.NIKSite;
                    param.CompanyId = company.ID;
                    param.SetDateRange();

                    if ((param.DateFrom != null && param.DateFrom != DateTime.MinValue) && (param.DateTo != null && param.DateTo != DateTime.MinValue))
                    {
                        var totalDay = param.DateTo.Value.Subtract(param.DateFrom.Value).TotalDays;

                        var IsNotOfficeHour = false;
                        if (DateTime.Now.Hour >= 17 || DateTime.Now.Hour <= 6)
                        {
                            IsNotOfficeHour = true;
                        }


                        if (totalDay <= 31 || IsNotOfficeHour)
                        {
                            var checkSchedulerTask = SchedulerTask.GetAllToProcess()
                                .Where(x => 
                                        x.TryCount <= SiteSettings.SIZE_SCHEDULER_TRY &&
                                        x.SchedulerType == "REPORT" &&
                                        x.CreatedBy == CurrentUser.NIKSITE
                                    ).ToList();

                            if (!(checkSchedulerTask != null && checkSchedulerTask.Count > 0))
                            {
                                string fileName = string.Format("{0}_{1}_{2}_{3}.csv",
                                    REPORT_TYPE.MSR_STATUS,
                                    Guid.NewGuid().ToString().Split('-')[0].ToUpper(),
                                    param.DateFrom.Value.ToString(Constant.FORMAT_DATE_TRANSDATE_ACCPAC),
                                    param.DateTo.Value.ToString(Constant.FORMAT_DATE_TRANSDATE_ACCPAC));

                                string DataParameter = Newtonsoft.Json.JsonConvert.SerializeObject(param);
                                SchedulerLogic.Create(company.ID, DocumentType.REPORT, fileName, DataParameter);
                                response.SetSuccess();
                            }
                            else
                            {
                                response.SetError("Do not download again if the previous download request has not been completed");
                            }
                        }
                        else
                        {
                            response.SetError("Can't download more than 1 month");
                        }
                    }
                    else
                    {
                        response.SetError("Date range cannot be empty");
                    }

                }
                else
                {
                    response.SetError("User Dont have valid Email");
                }
            }
            catch (Exception ex)
            {
                Logger.Set(LoggerCategory.REPORT_TEST, "REPORT/REPORT_TEST", param);
                response.SetError("Exception");
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Historical Transaction
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult HistoricalTransaction()
        {
            MSRSearchModel model = new MSRSearchModel();
            List<SelectListItem> listType = new List<SelectListItem>();
            listType.Add(new SelectListItem() { Text = "ALL", Value = "", Selected = true });
            listType.Add(new SelectListItem() { Text = "ISSUED", Value = RequestStatus.ISSUED });
            listType.Add(new SelectListItem() { Text = "TRANSFER", Value = RequestStatus.TRANSFER });
            listType.Add(new SelectListItem() { Text = "CANCEL", Value = RequestStatus.CANCEL });
            listType.Add(new SelectListItem() { Text = "REVERSE_TRANSFER", Value = RequestStatus.REVERSE_TRANSFER });

            ViewBag.ListType = listType;
            ViewData.Model = model;
            return View();
        }

        /// <summary>
        /// MSR Status Post Execution
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual FileContentResult GetHistoricalTransaction()
        {
            MSRSearchModel param = new MSRSearchModel();
            #region PARAM QUERY STRING
            var strQuery = Request.QueryString;
            if (strQuery["DateFromStr"] != null && !string.IsNullOrEmpty(strQuery["DateFromStr"].ToString()))
            {
                param.DateFromStr = strQuery["DateFromStr"].ToString();
            }

            if (strQuery["DateToStr"] != null && !string.IsNullOrEmpty(strQuery["DateToStr"].ToString()))
            {
                param.DateToStr = strQuery["DateToStr"].ToString();
            }

            if (strQuery["MSRNo"] != null && !string.IsNullOrEmpty(strQuery["MSRNo"].ToString()))
            {
                param.MSRNo = strQuery["MSRNo"].ToString();
            }

            if (strQuery["IssuedType"] != null && !string.IsNullOrEmpty(strQuery["IssuedType"].ToString())
                && strQuery["IssuedType"].ToString() != "null")
            {
                param.IssuedType = strQuery["IssuedType"].ToString();
            }
            else
            {
                param.IssuedType = string.Empty;
            }

            if (strQuery["ItemNo"] != null && !string.IsNullOrEmpty(strQuery["ItemNo"].ToString()))
            {
                param.ItemNo = strQuery["ItemNo"].ToString();
            }

            param.SetDateRange();
            #endregion

            MemoryStream stream = new MemoryStream();

            int rowIndex = 0;
            string fileName = Guid.NewGuid().ToString().Split('-')[0].ToUpper();
            using (CsvFileWriter csvWriter = new CsvFileWriter(stream))
            {
                CsvRow csvRow = new CsvRow();
                if (param.DateFrom != DateTime.MinValue
                    && param.DateTo != DateTime.MinValue
                    && param.DateTo > param.DateFrom)
                {
                    fileName = string.Format("_{0}_[{1}-{2}]",
                        fileName,
                        param.DateFrom.Value.ToString(Constant.FORMAT_DATE_TRANSDATE_ACCPAC),
                        param.DateTo.Value.ToString(Constant.FORMAT_DATE_TRANSDATE_ACCPAC));

                    var CurrentCompanyCode = CookieManager.Get(COOKIES_NAME.USER_COMPANY_CODE);
                    Company company = Company.GetByCode(CurrentCompanyCode);

                    var reportResultFlats = CurrentDataContext.CurrentContext.report_historical_transaction(
                        company.ID,
                        param.DateFrom,
                        param.DateTo,
                        !string.IsNullOrEmpty(param.MSRNo) ? param.MSRNo : "",
                        !string.IsNullOrEmpty(param.IssuedType) ? param.IssuedType : "",
                        !string.IsNullOrEmpty(param.ItemNo) ? param.ItemNo : "").ToList();

                    if (reportResultFlats != null && reportResultFlats.Count > 0)
                    {
                        string[] headerColoums = new string[] { 
                            "No",
                            "MSR No",
                            "Document No",
                            "Transaction Type",
                            "Transaction Date",
                            "User",
                            "Item No",
                            "Description",
                            "UOM",
                            "Quantity",
                            "From Location",
                            "To Location",
                            "Equipment No",
                            "WO No"
                        };


                        foreach (var item in headerColoums)
                        {
                            csvRow.Add(item);
                        }
                        csvWriter.WriteRow(csvRow);

                        var reportResultGroupMSRs = reportResultFlats
                                .GroupBy(g => g.MSRNo)
                                .Select(x => x.ToList())
                                .ToList();

                        foreach (var reportResultGroupMSR in reportResultGroupMSRs)
                        {
                            var reportResultGroupIssueds = reportResultGroupMSR
                                .GroupBy(g => g.DocumentNo)
                                .Select(x => x.ToList())
                                .ToList();

                            foreach (var reportResults in reportResultGroupIssueds)
                            {
                                foreach (var itemData in reportResults.OrderByDescending(x => x.TransactionDate))
                                {
                                    string[] strValues = new string[] {
                                        (rowIndex + 1).ToString(),
                                        itemData.MSRNo,
                                        itemData.DocumentNo,
                                        itemData.TransactionTypeLabel,
                                        itemData.TransactionDate.ToString(Constant.FORMAT_DATE_JRESOURCES),
                                        itemData.User,
                                        itemData.ItemNo,
                                        itemData.Description,
                                        itemData.UOM,
                                        itemData.Quantity.ToString("#.##"),
                                        itemData.LocationIdFrom,
                                        itemData.LocationIdTo,
                                        itemData.EquipmentNo,
                                        itemData.WorkOrderNo
                                    };

                                    csvRow = new CsvRow();
                                    foreach (var strItem in strValues)
                                    {
                                        csvRow.Add(strItem);
                                    }
                                    csvWriter.WriteRow(csvRow);

                                    rowIndex++;
                                }
                            }

                        }
                    }

                }
            }

            fileName = "MSR_HIST_TRAN" + fileName + ".csv";
            return File(stream.ToArray(), "application/vnd.ms-excel", fileName);
        }

        [HttpGet]
        public virtual ActionResult Requisition()
        {
            MSRSearchModel model = new MSRSearchModel();
            ViewData.Model = model;
            return View();
        }

        [HttpGet]
        public virtual FileContentResult GetRequisition()
        {
            MSRSearchModel param = new MSRSearchModel();
            #region PARAM QUERY STRING
            var strQuery = Request.QueryString;
            if (strQuery["DateFromStr"] != null && !string.IsNullOrEmpty(strQuery["DateFromStr"].ToString()))
            {
                param.DateFromStr = strQuery["DateFromStr"].ToString();
            }

            if (strQuery["DateToStr"] != null && !string.IsNullOrEmpty(strQuery["DateToStr"].ToString()))
            {
                param.DateToStr = strQuery["DateToStr"].ToString();
            }

            if (strQuery["MSRNo"] != null && !string.IsNullOrEmpty(strQuery["MSRNo"].ToString()))
            {
                param.MSRNo = strQuery["MSRNo"].ToString();
            }

            if (strQuery["ItemNo"] != null && !string.IsNullOrEmpty(strQuery["ItemNo"].ToString()))
            {
                param.ItemNo = strQuery["ItemNo"].ToString();
            }

            if (strQuery["IssuedNo"] != null && !string.IsNullOrEmpty(strQuery["IssuedNo"].ToString()))
            {
                param.IssuedNo = strQuery["IssuedNo"].ToString();
            }

            param.SetDateRange();
            #endregion

            MemoryStream stream = new MemoryStream();
            string fileName = Guid.NewGuid().ToString().Split('-')[0].ToUpper();

            using (CsvFileWriter csvWriter = new CsvFileWriter(stream))
            {
                CsvRow csvRow = new CsvRow();
                int rowIndex = 0;

                if (param.DateFrom != DateTime.MinValue
                    && param.DateTo != DateTime.MinValue
                    && param.DateTo > param.DateFrom)
                {
                    fileName = string.Format("_{0}_[{1}-{2}]",
                        fileName,
                        param.DateFrom.Value.ToString(Constant.FORMAT_DATE_TRANSDATE_ACCPAC),
                        param.DateTo.Value.ToString(Constant.FORMAT_DATE_TRANSDATE_ACCPAC));

                    var CurrentCompanyCode = CookieManager.Get(COOKIES_NAME.USER_COMPANY_CODE);
                    Company company = Company.GetByCode(CurrentCompanyCode);

                    var reportResultFlats = CurrentDataContext.CurrentContext.report_requisition(
                        company.ID,
                        param.DateFrom,
                        param.DateTo,
                        !string.IsNullOrEmpty(param.MSRNo) ? param.MSRNo : "",
                        !string.IsNullOrEmpty(param.IssuedNo) ? param.IssuedNo : "",
                        !string.IsNullOrEmpty(param.ItemNo) ? param.ItemNo : "").ToList();

                    if (reportResultFlats != null && reportResultFlats.Count > 0)
                    {
                        string[] headerColoums = new string[] {
                        "No",
                        "MSR No",
                        "MSR Create Date",
                        "MSR Approval Date",
                        "Document No",
                        "RQN No",
                        "RQN Date",
                        "User",
                        "Priority Order",
                        "Item No",
                        "Description",
                        "UOM",
                        "Quantity Purchase",
                        "To Location"
                        };


                        foreach (var item in headerColoums)
                        {
                            csvRow.Add(item);
                        }
                        csvWriter.WriteRow(csvRow);

                        var reportResultGroupMSRs = reportResultFlats
                            .OrderByDescending(x => x.MSRCreatedDate)
                                .GroupBy(g => g.MSRNo)
                                .Select(x => x.ToList())
                                .ToList();

                        foreach (var reportResultGroupMSR in reportResultGroupMSRs)
                        {
                            var reportResultGroupIssueds = reportResultGroupMSR
                                .GroupBy(g => g.RQNNo)
                                .Select(x => x.ToList())
                                .ToList();

                            foreach (var reportResults in reportResultGroupIssueds)
                            {
                                foreach (var itemData in reportResults.OrderByDescending(x => x.RQNDate))
                                {
                                    string[] strValues = new string[] {
                                    (rowIndex + 1).ToString(),
                                    itemData.MSRNo,
                                    itemData.MSRCreatedDate.ToString(Constant.FORMAT_DATE_JRESOURCES),
                                    (itemData.MSRApprovalDate.HasValue ? itemData.MSRApprovalDate.Value.ToString(Constant.FORMAT_DATE_JRESOURCES) : ""),
                                    itemData.DocumentNo,
                                    itemData.RQNNo,
                                    itemData.RQNDate.ToString(Constant.FORMAT_DATE_JRESOURCES),
                                    itemData.User,
                                    itemData.PriorityOrder,
                                    itemData.ItemNo,
                                    itemData.Description,
                                    itemData.UOM,
                                    itemData.QuantityPurchase.ToString("#.##"),
                                    itemData.ToLocation
                                };

                                    csvRow = new CsvRow();
                                    foreach (var strItem in strValues)
                                    {
                                        csvRow.Add(strItem);
                                    }
                                    csvWriter.WriteRow(csvRow);
                                    rowIndex++;
                                }
                            }

                        }
                    }



                }
            }

            fileName = "MSR_REQUISITION" + fileName + ".csv";
            return File(stream.ToArray(), "application/vnd.ms-excel", fileName);
        }

        [HttpGet]
        public virtual ActionResult FRF()
        {
            FRFSearchModel model = new FRFSearchModel();
            ViewData.Model = model;
            return View();
        }

        [HttpGet]
        public virtual FileContentResult GetFRF()
        {
            MSRSearchModel param = new MSRSearchModel();
            #region PARAM QUERY STRING
            var strQuery = Request.QueryString;
            if (strQuery["DateFromStr"] != null && !string.IsNullOrEmpty(strQuery["DateFromStr"].ToString()))
            {
                param.DateFromStr = strQuery["DateFromStr"].ToString();
            }

            if (strQuery["DateToStr"] != null && !string.IsNullOrEmpty(strQuery["DateToStr"].ToString()))
            {
                param.DateToStr = strQuery["DateToStr"].ToString();
            }

            param.SetDateRange();
            #endregion

            MemoryStream stream = new MemoryStream();

            int rowIndex = 0;
            string fileName = Guid.NewGuid().ToString().Split('-')[0].ToUpper();

            using (CsvFileWriter csvWriter = new CsvFileWriter(stream))
            {
                CsvRow row = new CsvRow();
                if (param.DateFrom != DateTime.MinValue
                    && param.DateTo != DateTime.MinValue
                    && param.DateTo > param.DateFrom)
                {
                    fileName = string.Format("_{0}_[{1}-{2}]",
                        fileName,
                        param.DateFrom.Value.ToString(Constant.FORMAT_DATE_TRANSDATE_ACCPAC),
                        param.DateTo.Value.ToString(Constant.FORMAT_DATE_TRANSDATE_ACCPAC));

                    var CurrentCompanyCode = CookieManager.Get(COOKIES_NAME.USER_COMPANY_CODE);
                    Company company = Company.GetByCode(CurrentCompanyCode);

                    var reportResultFlats = CurrentDataContext.CurrentContext.report_frf(
                        company.CompanyCode,
                        param.DateFrom,
                        param.DateTo).ToList();

                    if (reportResultFlats != null && reportResultFlats.Count > 0)
                    {
                        string[] headerColoums = new string[] {
                        "No",
                        "FRF Log Date",                        
                        "Fuel Desc",
                        "FRF No",
                        "FRF Status",
                        "Remark",
                        "Equipment Name",
                        "Quantity",
                        "End Of HM",
                        "Driver Name",
                        "Cost Center",
                        "Location",
                        "Shift",
                        "Supervisor",
                    };

                        foreach (var item in headerColoums)
                        {
                            row.Add(item);
                        }

                        csvWriter.WriteRow(row);

                        var reportResultGroupFRFs = reportResultFlats
                            .OrderByDescending(x => x.LogSheetDate)
                                .GroupBy(g => g.FRFNo)
                                .Select(x => x.ToList())
                                .ToList();

                        var rowGroupIssued = 0;
                        foreach (var reportResultGroupFRF in reportResultGroupFRFs)
                        {

                            rowGroupIssued = rowIndex + 1;
                            var resultGroupFRF = reportResultGroupFRF.OrderBy(x => x.EndOfHM);
                            foreach (var itemData in resultGroupFRF)
                            {
                                string[] strValues = new string[] {
                                    (rowIndex + 1).ToString(),
                                    (itemData.LogSheetDate.HasValue ? itemData.LogSheetDate.Value.ToString(Constant.FORMAT_DATE_JRESOURCES) : ""),
                                    itemData.ItemDescription,
                                    itemData.FRFNo,
                                    itemData.StatusLabel,
                                    itemData.remarks,
                                    itemData.EquipmentName,
                                    (itemData.Quantity.HasValue ? itemData.Quantity.Value.ToString("#.##") : ""),
                                    (itemData.EndOfHM.HasValue ? itemData.EndOfHM.Value.ToString("#.##") : ""),
                                    itemData.DriverName,
                                    itemData.CostCenter,
                                    itemData.Location,
                                    itemData.Shift,
                                    itemData.Supervisor
                                };


                                row = new CsvRow();
                                foreach (var strItem in strValues)
                                {
                                    row.Add(strItem);
                                }
                                csvWriter.WriteRow(row);
                                rowIndex++;
                            }
                        }
                    }
                }
            }

            fileName = "FRF_" + fileName + ".csv";
            return File(stream.ToArray(), "application/vnd.ms-excel", fileName);
        }

        public virtual ActionResult PickingList()
        {
            return View();
        }


        [HttpGet]
        public virtual ActionResult ReportOTIF()
        {

            return View();
        }

        [HttpPost]
        public virtual JsonResult ReportOTIF(MSRSearchModel param)
        {
            ResponseModel response = new ResponseModel();

            try
            {
                var CurrentCompanyCode = CookieManager.Get(COOKIES_NAME.USER_COMPANY_CODE);
                Company company = Company.GetByCode(CurrentCompanyCode);
                var user = UserLogic.GetByNIK(CurrentUser.NIKSITE);
                if (user != null && !string.IsNullOrEmpty(user.Email))
                {
                    param.MoreCriteria = user.NIKSite;
                    param.CompanyId = company.ID;
                    param.SetDateRange();

                    if ((param.DateFrom != null && param.DateFrom != DateTime.MinValue) && (param.DateTo != null && param.DateTo != DateTime.MinValue))
                    {
                        //DateTime dtMax = param.DateTo.Value.AddMonths(-1);
                        //if(dtMax < param.DateFrom.Value)
                        //{
                        string fileName = string.Format("{0}_{1}_{2}_{3}.csv",
                            REPORT_TYPE.OTIF,
                            Guid.NewGuid().ToString().Split('-')[0].ToUpper(),
                            param.DateFrom.Value.ToString(Constant.FORMAT_DATE_TRANSDATE_ACCPAC),
                            param.DateTo.Value.ToString(Constant.FORMAT_DATE_TRANSDATE_ACCPAC));

                        string DataParameter = Newtonsoft.Json.JsonConvert.SerializeObject(param);
                        SchedulerLogic.Create(company.ID, DocumentType.REPORT, fileName, DataParameter);
                        response.SetSuccess();
                        //} else
                        //{
                        //    response.SetError("Date range more from 1 month");
                        //}
                    }
                    else
                    {
                        response.SetError("Date range cannot be empty");
                    }

                }
                else
                {
                    response.SetError("User Dont have valid Email");
                }
            }
            catch (Exception ex)
            {
                Logger.Set(LoggerCategory.REPORT_TEST, "REPORT/REPORT_TEST", param);
                response.SetError("Exception");
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult ReportComparison()
        {
            ReportComparisonModel model = new ReportComparisonModel();

            return View();
        }


        [HttpPost]
        public virtual JsonResult ReportComparison(ReportComparisonModel model)
        {
            var response = new ResponseModel();
            DateTime DateFrom = DateTime.Now.AddDays(-8).Date.AddMinutes(1);
            DateTime DateTo = DateTime.Now.AddDays(1).Date.AddMinutes(-1);

            if (string.IsNullOrEmpty(model.DateFromStr) || string.IsNullOrEmpty(model.DateToStr))
            {
                if (!string.IsNullOrEmpty(model.DateFromStr))
                {
                    response.SetError("Date from cannot be null");
                }

                if (!string.IsNullOrEmpty(model.DateToStr))
                {
                    response.SetError("Date to cannot be null");
                }
            } 
            else
            {
                if(!(DateTime.TryParse(model.DateFromStr, out DateFrom) && DateTime.TryParse(model.DateToStr, out DateTo)))
                {
                    response.SetError("Date convert failed");
                } else
                {
                    if(DateFrom > DateTo)
                    {
                        response.SetError("Date From cannot over from Date to");
                    }
                }
            }


            if(response.IsSuccess)
            {
                var MSRs = CurrentDataContext.CurrentContext.MSRs.Where(x => x.WorkOrderNo != "");

                if(!string.IsNullOrEmpty(model.WorkOrderNo))
                {
                    MSRs = MSRs.Where(x => x.WorkOrderNo.Contains(model.WorkOrderNo));
                } else
                {
                    MSRs = MSRs.Where(x => x.CreatedDate >= DateFrom && x.CreatedDate <= DateTo);
                }


                var MSRIds = MSRs.Select(x => x.ID);
                var MSRDetails = CurrentDataContext.CurrentContext.MSRDetails.Where(x => MSRIds.Contains(x.MSRId));

                var MSRIssueds = CurrentDataContext.CurrentContext.MSRIssueds.Where(x => MSRIds.Contains(x.MSRId) && x.IssuedType == RequestStatus.ISSUED);
                var MSRIssuedIds = MSRIssueds.Select(x => x.ID);
                var MSRIssuedDetails = CurrentDataContext.CurrentContext.MSRIssuedDetails.Where(x => MSRIssuedIds.Contains(x.MSRIssuedId));


    //            var models = (from msr in MSRs
    //                          join msr_detail in MSRDetails on msr.ID equals msr_detail.MSRId
    //                          join issued in MSRIssueds on msr.ID equals issued.MSRId
    //                          join issued_detail in MSRIssuedDetails on issued.ID equals issued_detail.MSRIssuedId
    //                          select new ReportComparisonDetailModel()
    //                          {
    //                              WorkOrderNo = msr.WorkOrderNo,
    //                              MSRNo = msr.MSRNo,
    //                              MSRDate = msr.CreatedDate,
    //                              ItemNo = msr_detail.ItemNo,
    //                              ItemDescription = msr_detail.Description

    //    //public string  { get; set; }
    //    //public string MSRNo { get; set; }
    //    //public DateTime MSRDate { get; set; }
    //    //public string ItemNo { get; set; }
    //    //public string ItemDescription { get; set; }

    //    //public decimal QtyPost { get; set; }
    //    //public string DocumentType { get; set; }
    //    //public string DocumentTransactionNo { get; set; }
    //    //public string DocumentTransactionDate { get; set; }

    //    //public decimal QtyPosted { get; set; }
    //    //public decimal QtyDiff { get; set; }

    //}).ToList();
                

                response.ResponseObject = model;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
