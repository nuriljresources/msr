﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using JResources.BusinessLogic;
using JResources.Common;
using JResources.Common.Enum;
using JResources.Data;
using JResources.Data.Model;
using JResources.Data.Model.Accpac;

namespace JResources.Web.Controllers
{
    public partial class LogController : Controller
    {
        [HttpGet]
        public virtual ActionResult Synchronize()
        {
            SchedulerSearchModel model = new SchedulerSearchModel();
            List<CompanyDepartmentModel> CompanyDepartments = new List<CompanyDepartmentModel>() { new CompanyDepartmentModel { ID = Guid.Empty, CompanyName = "SELECT All COMPANY" } };
            CompanyDepartments.AddRange(CompanyLogic.GetFullCompanyWithDepartmentByCurrentRole(new string[] { ROLE_CODE.MSR_ENTRY, ROLE_CODE.MSR_ISSUED, ROLE_CODE.MSR_APPROVAL }));

            ViewBag.CompanyDepartments = CompanyDepartments;
            ViewData.Model = model;
            return View();
        }

        [HttpPost]
        public virtual JsonResult GetSynchronize(SchedulerSearchModel model)
        {
            model.SetDateRange();
            
            Company company = Company.GetByCode(CurrentUser.COMPANY);
            var schedulerTasks = SchedulerTask.GetAllToProcess();
            schedulerTasks = schedulerTasks.Where(x => x.SchedulerType != DocumentType.REPORT);

            if (!RoleLogic.IsAdministrator(CurrentUser.NIKSITE, company.ID))
            {
                schedulerTasks = schedulerTasks.Where(x => x.CompanyId == company.ID);
            }

            if (!string.IsNullOrEmpty(model.MSRIssuedNo))
            {
                schedulerTasks = schedulerTasks.Where(x => x.DocumentNo == model.MSRIssuedNo);
            }

            if (!string.IsNullOrEmpty(model.MSRNo))
            {
                var MSRIssueds = MSRIssued.GetByMSRNo(model.MSRNo);
                if (MSRIssueds != null && MSRIssueds.Count() > 0)
                {
                    List<string> ListDocumentNo = MSRIssueds.Select(x => x.MSRIssuedNo).ToList();
                    schedulerTasks = schedulerTasks.Where(x => ListDocumentNo.Contains(x.DocumentNo));
                }
            }

            if (model.DateFrom.HasValue)
            {
                schedulerTasks = schedulerTasks.Where(x => x.CreatedDate >= model.DateFrom.Value);
            }

            if (model.DateTo.HasValue)
            {
                schedulerTasks = schedulerTasks.Where(x => x.CreatedDate <= model.DateTo.Value);
            }

            if (model.CompanyId != null && model.CompanyId != Guid.Empty)
            {
                schedulerTasks = schedulerTasks.Where(x => x.CompanyId == model.CompanyId);
            }

            int SIZE_SCHEDULER_TRY = SiteSettings.SIZE_SCHEDULER_TRY;
            model.SetPager(schedulerTasks.Count());
            List<SchedulerTaskModel> listData = schedulerTasks.OrderByDescending(x => x.CreatedDate)
                .Skip((model.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
                .Take(Common.SiteSettings.PAGE_SIZE_PAGING)
                .Select(x => new SchedulerTaskModel()
                {
                    ID = x.ID,
                    CompanyId = x.CompanyId,
                    DocumentNo = x.DocumentNo,
                    DocumentRefNo = "",
                    SchedulerStatus = x.SchedulerStatus,
                    SchedulerType = x.SchedulerType,
                    TryCount = x.TryCount,
                    CreatedDate = x.CreatedDate,
                    ManualPostDate = x.ManualPostDate
                }).ToList();

            
            StringBuilder sbLog = new StringBuilder();
            StringBuilder sbLogData = new StringBuilder();
            var statusMSRs = EnumTable.GetByCode(EnumTable.EnumType.STATUS_MSR).ToList();
            foreach (var item in listData)
            {
                sbLog = new StringBuilder();
                if (item.SchedulerType == DocumentType.MSR)
                {
                    MSRIssued issued = MSRIssued.GetByIssuedNo(item.DocumentNo);
                    if (issued != null)
                    {
                        item.DocumentRefNo = issued.MSR.MSRNo;
                        item.DocumentType = issued.IssuedType;

                        item.TypeCssColor = CommonFunction.LabelStatusCssHelper(item.DocumentType);
                        var statusMSR = statusMSRs.FirstOrDefault(x => x.EnumValue == item.DocumentType);
                        if (statusMSR != null)
                        {
                            item.DocumentType = statusMSR.EnumLabel;
                        }
                    }
                }
                else if (item.SchedulerType == DocumentType.FRF)
                {
                    item.TypeCssColor = CommonFunction.LabelStatusCssHelper(RequestStatus.APPROVED);
                    var statusMSR = statusMSRs.FirstOrDefault(x => x.EnumValue == RequestStatus.APPROVED);
                    if (statusMSR != null)
                    {
                        item.DocumentType = "FUEL-ISSUED";
                    }
                } else
                {
                    item.DocumentType = item.SchedulerType;
                    item.TypeCssColor = CommonFunction.LabelStatusCssHelper(item.DocumentType);
                }

                var logs = SchedulerLog.GetBySchedulerId(item.ID);
                foreach (var itemLog in logs)
                {
                    sbLog.AppendLine(itemLog.CreatedDate.JResourcesDateTime());
                    sbLog.AppendLine(itemLog.LogMessage);
                    sbLog.AppendLine("");

                    if (sbLogData.Length < 1)
                    {
                        sbLogData.AppendLine(Newtonsoft.Json.JsonConvert.SerializeObject(itemLog));
                    }
                }

                if (RoleLogic.CheckRoleAccess(CurrentUser.NIKSITE, ROLE_CODE.MSR_ISSUED, item.CompanyId, Guid.Empty))
                {
                    item.IsEnablePost = true;
                }



                item.CreatedDateStr = item.CreatedDate.JResourcesDate();
                item.CreatedTimeStr = item.CreatedDate.JResourcesTime();

                if (item.ManualPostDate.HasValue)
                {
                    item.ManualPostDateStr = item.ManualPostDate.Value.JResourcesDate();
                    item.ManualPostTimeStr = item.ManualPostDate.Value.JResourcesTime();
                }

                item.LogMessage = sbLog.ToString();
                item.LogDataJson = sbLogData.ToString();

                item.StatusLabel = CommonFunction.SchedulerTextHelper(item.SchedulerStatus);
                item.StatusCssColor = CommonFunction.SchedulerCssHelper(item.SchedulerStatus);
            }

            model.ListData = listData;
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonResult SendAccpac(int schedulerId)
        {
            AccpacPostBaseModel AccpacPost = new AccpacPostBaseModel();
            AccpacPost.SchedulerId = schedulerId;

            API api = new API();
            api.BASE_URL = SiteSettings.ACCPAC_URL;
            var response = api.Post(URI_ACCPAC.PROCESS_SAVE, AccpacPost);

            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
