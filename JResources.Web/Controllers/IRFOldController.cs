﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using JResources.Data;
using JResources.Data.Model;
using JResources.BusinessLogic;

namespace JResources.Web.Controllers
{
    public partial class IRFOldController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult IrfList()
        {
            //ViewData.Model = MSRLogic.GetListIRF();
            var models = new List<IRFOldGridModel>();

            var tirfs = tirf_master.GetAll().Take(10);
            var costCenters = tirfs.Select(x => x.cccode).Distinct().Where(x => !string.IsNullOrEmpty(x)).ToList();
            var equipments = tirfs.Select(x => x.equipment_no.Trim()).Distinct().Where(x => !string.IsNullOrEmpty(x)).ToList();
            var maintenanceCodes = tirfs.Select(x => x.mtcode).Distinct().Where(x => !string.IsNullOrEmpty(x)).ToList();            
            var users = tirfs.Select(x => x.requestor).Distinct().Where(x => !string.IsNullOrEmpty(x)).ToList();

            var costCenterList = CSOPTFD.GetAll().Where(x => x.OPTFIELD == "CCCODE" && costCenters.Contains(x.VALUE)).ToList();
            var equipmentList = CSOPTFD.GetAll().Where(x => x.OPTFIELD == "ECCODE" && equipments.Contains(x.VALUE)).ToList();
            var maintenanceList = CSOPTFD.GetAll().Where(x => x.OPTFIELD == "MTCODE" && maintenanceCodes.Contains(x.VALUE)).ToList();
            var statusList = tfield_value.GetByFieldName("status_id");
            var userList = tuser_roles.GetAll().Where(x => users.Contains(x.usrid)).ToList();

            foreach (var tirf in tirfs)
            {
                var model = new IRFOldGridModel();
                model.IRFNo = tirf.irf_no;
                model.IRFDate = tirf.irf_dt;
                model.MaintenanceCode = tirf.mtcode;
                model.WorkOrder = tirf.workorder_no;
                model.ExpectedDate = tirf.expected_dt;
                model.Reason = tirf.remark;
                model.RequestStatus = tirf.status_id;
                model.RequestorName = tirf.requestor;
                model.NextApproval = string.Empty;

                model.CostCenter = tirf.cccode;
                var ccenter = costCenterList.FirstOrDefault(x => x.VALUE == tirf.cccode);
                if (ccenter != null)
                {
                    model.CostCenter = string.Format("{0} - {1}", tirf.cccode, ccenter.VDESC);
                }

                model.EquipmentNo = tirf.equipment_no;
                var equipment = equipmentList.FirstOrDefault(x => x.VALUE == tirf.equipment_no);
                if (equipment != null)
                {
                    model.EquipmentNo = string.Format("{0} - {1}", tirf.equipment_no, equipment.VDESC);
                }

                model.MaintenanceCode = tirf.mtcode;
                var maintenance = maintenanceList.FirstOrDefault(x => x.VALUE == tirf.mtcode);
                if (maintenance != null)
                {
                    model.MaintenanceCode = string.Format("{0} - {1}", tirf.mtcode, maintenance.VDESC);
                }

                var statusField = statusList.FirstOrDefault(x => x.fld_valu == tirf.status_id.Trim());
                if (statusField != null)
                {
                    model.RequestStatus = statusField.fld_desc;
                }

                var user = userList.FirstOrDefault(x => x.usrid == tirf.requestor);
                if (user != null)
                {
                    model.RequestorId = user.usrid;
                    model.RequestorName = user.fullname;
                }

                var nextApproval = tirf_display.GetUserByIRFNo(tirf.irf_no);
                if (nextApproval != null)
                {
                    model.NextApproval = nextApproval.fullname;
                }

                models.Add(model);
            }


            ViewData.Model = models;
            return View();
        }

        [HttpGet]
        public virtual ActionResult IrfDetail(string IRFNo)
        {

            return View();
        }

        [HttpPost]
        public virtual JsonResult IrfDetail(IRFModel model)
        {

            return Json(true, JsonRequestBehavior.AllowGet);
        }

    }
}
