﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using JResources.Common;
using JResources.Data;
using JResources.Data.Model;

namespace JResources.Web.Controllers
{
    [JResourcesAuthorization(ROLE_CODE = ROLE_CODE.ADMINISTRATOR)]
    public partial class SettingController : Controller
    {
        public virtual ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public virtual ActionResult JobPriority()
        {
            Company company = Company.GetByCode(CurrentUser.COMPANY);
            List<EnumTable> EnumTables = EnumTable.GetByCode(EnumTable.EnumType.JOB_PRIORITY)
                .Where(x => x.CompanyCode == company.CompanyCode)
                .ToList();

            for (int i = 1; i < 11; i++)
            {
                string JobPriorityCode = "P" + i.ToString("D2");
                var EnumJobPriority = EnumTables.FirstOrDefault(x => x.EnumValue == JobPriorityCode);
                if (EnumJobPriority == null)
                {
                    EnumJobPriority = new EnumTable();
                    EnumJobPriority.EnumValue = JobPriorityCode;
                    EnumJobPriority.Sequence = i;
                    EnumTables.Add(EnumJobPriority);
                }
            }

            EnumTables = EnumTables.OrderBy(x => x.EnumValue).ToList();
            ViewData.Model = EnumTables;
            return View();
        }

        [HttpPost]
        public virtual ActionResult JobPriority(List<EnumTable> models)
        {
            var response = new ResponseModel();
            #region Validation Job Priority
            StringBuilder sbLog = new StringBuilder();
            foreach (var item in models)
            {
                if (!string.IsNullOrEmpty(item.EnumLabel))
                {
                    if (!(item.EnumLabel == "*" || item.EnumLabel == "-"))
                    {
                        if (item.EnumLabel.Contains("-"))
                        {
                            string[] jobValues = item.EnumLabel.Split('-');
                            int jobValue1 = 0;
                            int jobValue2 = 0;

                            if (!(jobValues.Length > 1 &&
                                    !string.IsNullOrEmpty(jobValues[0]) &&
                                    !string.IsNullOrEmpty(jobValues[1]) &&
                                    int.TryParse(jobValues[0], out jobValue1) &&
                                    int.TryParse(jobValues[1], out jobValue2)))
                            {
                                response.MessageList.Add(string.Format("Job Code {0} Value range not valid", item.EnumValue));
                            }
                            else
                            {
                                if (jobValue1 > jobValue2)
                                {
                                    response.MessageList.Add(string.Format("Job Code {0} Value range not valid", item.EnumValue));
                                }
                            }
                        }
                        else
                        {
                            int jobValue = 0;
                            if (!int.TryParse(item.EnumLabel, out jobValue))
                            {
                                response.MessageList.Add(string.Format("Job Code {0} Value must be number", item.EnumValue));
                            }
                        }
                    }
                }
                else
                {
                    response.MessageList.Add(string.Format("Job Code {0} Value must be input", item.EnumValue));
                }
            }
            #endregion

            if (response.MessageList.Count > 0)
            {
                response.SetError();
            }
            else
            {
                for (int i = 0; i < models.Count; i++)
                {
                    EnumTable item = models[i];
                    if (item.ID != Guid.Empty)
                    {
                        EnumTable enumTable = EnumTable.GetById(item.ID);
                        enumTable.EnumLabel = item.EnumLabel;
                        enumTable.UpdateSave<EnumTable>();
                    }
                    else
                    {
                        item.ID = Guid.NewGuid();
                        item.Save<EnumTable>();
                    }
                }

                response.SetSuccess("Job Priority Hasb Been Saved");
            }

            if (!response.IsSuccess)
            {
                foreach (var item in response.MessageList)
                {
                    this.AddNotification(item, NotificationType.ERROR);
                }
            }
            else
            {
                this.AddNotification(response.Message, NotificationType.SUCCESS);
                return RedirectToAction(MVC.Setting.JobPriority());
            }

            ViewData.Model = models;
            return View();
        }

        public class DataTableAjaxPostModel
        {
            // properties are not capital due to json mapping
            public int draw { get; set; }
            public int start { get; set; }
            public int length { get; set; }
            public List<Column> columns { get; set; }
            public Search search { get; set; }
            public List<Order> order { get; set; }
        }

        public class Column
        {
            public string data { get; set; }
            public string name { get; set; }
            public bool searchable { get; set; }
            public bool orderable { get; set; }
            public Search search { get; set; }
        }

        public class Search
        {
            public string value { get; set; }
            public string regex { get; set; }
        }

        public class Order
        {
            public int column { get; set; }
            public string dir { get; set; }
        }

        public class YourCustomSearchClass
        {
            public Guid ID { get; set; }
            public string EquipmentCode { get; set; }
            public string CostCenter { get; set; }
            public string COA { get; set; }
        }

        [HttpPost]
        public virtual JsonResult CustomServerSideSearchAction(DataTableAjaxPostModel model)
        {
            // action inside a standard controller
            int filteredResultsCount;
            int totalResultsCount;
            model.search.value = Request.Form[35];
            var res = YourCustomSearchFunc(model, out filteredResultsCount, out totalResultsCount);

            var result = new List<YourCustomSearchClass>(res.Count);
            foreach (var s in res)
            {
                // simple remapping adding extra info to found dataset
                result.Add(new YourCustomSearchClass
                {
                    ID = s.ID,
                    EquipmentCode = s.EquipmentCode,
                    CostCenter = s.CostCenter,
                    COA = s.COA
                });
            };

            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = filteredResultsCount,
                data = result
            });
        }


        public IList<YourCustomSearchClass> YourCustomSearchFunc(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount)
        {
            var searchBy = (model.search != null) ? model.search.value : null;
            var take = model.length;
            var skip = model.start;

            string sortBy = "";
            bool sortDir = true;

            if (model.order != null)
            {
                if (string.IsNullOrEmpty(model.order[0].dir))
                {
                    model.order[0].dir = "asc";
                }

                sortBy = model.columns[model.order[0].column].data;
                sortDir = model.order[0].dir.ToLower() == "asc";
            }

            // search the dbase taking into consideration table sorting and paging
            var result = GetDataFromDbase(searchBy, take, skip, sortBy, sortDir, out filteredResultsCount, out totalResultsCount);
            if (result == null)
            {
                // empty collection...
                return new List<YourCustomSearchClass>();
            }
            return result;
        }

        public List<YourCustomSearchClass> GetDataFromDbase(string searchBy, int take, int skip, string sortBy, bool sortDir, out int filteredResultsCount, out int totalResultsCount)
        {
            // the example datatable used is not supporting multi column ordering
            // so we only need get the column order from the first column passed to us.      
            var equipmentMappings = CurrentDataContext.CurrentContext.EquipmentMappings.AsQueryable();

            if (String.IsNullOrEmpty(searchBy))
            {
                // if we have an empty search then just order the results by Id ascending
                sortBy = "Id";
                sortDir = true;
            } else
            {
                equipmentMappings = equipmentMappings.Where(x => x.EquipmentCode.Contains(searchBy) || x.COA.Contains(searchBy) || x.CostCode.Contains(searchBy));
            }

            var result = equipmentMappings
                .Select(m => new YourCustomSearchClass
                {
                    ID = m.ID,
                    EquipmentCode = m.EquipmentCode,
                    CostCenter = m.CostCode,
                    COA = m.COA
                })
                .OrderBy(x => x.ID)
                .Skip(skip)
                .Take(take)
                .ToList();

            // now just get the count of items (without the skip and take) - eg how many could be returned with filtering
            filteredResultsCount = equipmentMappings.Count();
            totalResultsCount = result.Count;

            return result;
        }

        [HttpGet]
        public virtual ActionResult EquipmentMapping()
        {
            EquipmentModel model = new EquipmentModel();
            ViewData.Model = model;
            return View();
        }

        [HttpPost]
        public virtual ActionResult EquipmentMapping(EquipmentModel model)
        {
            ResponseModel response = new ResponseModel();
            EquipmentMapping _equipmentMap = new EquipmentMapping();

            if (string.IsNullOrEmpty(model.EquipmentCode))
            {
                response.MessageList.Add("Equipment Code cannot be null");
            }
            else
            {
                if (model.EquipmentMappingID == Guid.Empty)
                {
                    _equipmentMap = Data.EquipmentMapping.GetByEquipmentNo(model.EquipmentCode);
                    if (_equipmentMap != null && !string.IsNullOrEmpty(_equipmentMap.EquipmentCode))
                    {
                        response.MessageList.Add("Equipment No Mapping already exists");
                    }
                    else
                    {
                        _equipmentMap = new EquipmentMapping();
                    }
                }
                else
                {
                    _equipmentMap = Data.EquipmentMapping.GetByEquipmentMappingId(model.EquipmentMappingID);
                }
            }

            if (string.IsNullOrEmpty(model.CostCenter))
            {
                response.MessageList.Add("Cost Center cannot be null");
            }

            if (string.IsNullOrEmpty(model.COA))
            {
                response.MessageList.Add("COA Group cannot be null");
            } else
            {
                if(model.COA.Length > 2)
                {
                    response.MessageList.Add("COA Group must 2 character");
                }
            }

            if (response.MessageList.Count > 0)
            {
                response.SetError(string.Join("<br />", response.MessageList.ToArray()));
            }
            else
            {
                _equipmentMap.EquipmentCode = model.EquipmentCode;
                _equipmentMap.COA = model.COA;
                _equipmentMap.CostCode = model.CostCenter;

                if (_equipmentMap.ID == Guid.Empty)
                {
                    _equipmentMap.ID = Guid.NewGuid();
                    _equipmentMap.Save<EquipmentMapping>();
                    response.SetSuccess("Equipment has been save");
                }
                else
                {
                    _equipmentMap.UpdateSave<EquipmentMapping>();
                    response.SetSuccess("Equipment has been update");
                }
            }

            if (!response.IsSuccess)
            {
                foreach (var item in response.MessageList)
                {
                    this.AddNotification(item, NotificationType.ERROR);
                }
            }
            else
            {
                this.AddNotification(response.Message, NotificationType.SUCCESS);
                return RedirectToAction(MVC.Setting.EquipmentMapping());
            }

            ViewData.Model = model;
            return View();
        }

        [HttpGet]
        public virtual ActionResult EquipmentMappingDelete(Guid Id)
        {
            var _equipmentMap = Data.EquipmentMapping.GetByEquipmentMappingId(Id);
            _equipmentMap.Delete<EquipmentMapping>();
            this.AddNotification("Done to delete", NotificationType.SUCCESS);
            return RedirectToAction(MVC.Setting.EquipmentMapping());
        }

        [HttpGet]
        public virtual ActionResult EquipmentFRFMapping()
        {
            return View();
        }

        [HttpPost]
        public virtual ActionResult EquipmentFRFMapping(EquipmentModel model)
        {
            ResponseModel response = new ResponseModel();
            EquipmentMapping _equipmentMap = new EquipmentMapping();

            if (string.IsNullOrEmpty(model.EquipmentCode))
            {
                response.MessageList.Add("Equipment Code cannot be null");
            }
            else
            {
                if (model.EquipmentMappingID == Guid.Empty)
                {
                    _equipmentMap = Data.EquipmentMapping.GetByEquipmentNo(model.EquipmentCode);
                    if (_equipmentMap != null && !string.IsNullOrEmpty(_equipmentMap.EquipmentCode))
                    {
                        response.MessageList.Add("Equipment No Mapping already exists");
                    }
                    else
                    {
                        _equipmentMap = new EquipmentMapping();
                    }
                }
                else
                {
                    _equipmentMap = Data.EquipmentMapping.GetByEquipmentMappingId(model.EquipmentMappingID);
                }
            }

            if (string.IsNullOrEmpty(model.CostCenter))
            {
                response.MessageList.Add("Cost Center cannot be null");
            }

            if (string.IsNullOrEmpty(model.COA))
            {
                response.MessageList.Add("COA cannot be null");
            }

            if (response.MessageList.Count > 0)
            {
                response.SetError(string.Join("<br />", response.MessageList.ToArray()));
            }
            else
            {
                _equipmentMap.EquipmentCode = model.EquipmentCode;
                _equipmentMap.COA = model.COA;
                _equipmentMap.CostCode = model.CostCenter;

                if (_equipmentMap.ID == Guid.Empty)
                {
                    _equipmentMap.ID = Guid.NewGuid();
                    _equipmentMap.Save<EquipmentMapping>();
                    response.SetSuccess("Equipment has been save");
                }
                else
                {
                    _equipmentMap.UpdateSave<EquipmentMapping>();
                    response.SetSuccess("Equipment has been update");
                }
            }

            if (!response.IsSuccess)
            {
                foreach (var item in response.MessageList)
                {
                    this.AddNotification(item, NotificationType.ERROR);
                }
            }
            else
            {
                this.AddNotification(response.Message, NotificationType.SUCCESS);
                return RedirectToAction(MVC.Setting.EquipmentFRFMapping());
            }

            ViewData.Model = model;
            return View();
        }

    }
}
