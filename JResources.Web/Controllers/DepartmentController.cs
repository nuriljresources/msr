﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JResources.Data;
using JResources.Data.Model;

using Omu.ValueInjecter;

namespace JResources.Web.Controllers
{
    public partial class DepartmentController : Controller
    {
        public virtual ActionResult Index()
        {
            var model = Department.GetAllActive().OrderBy(x => x.CompanyId);
            ViewData.Model = model;
            return View();
        }

        public virtual ActionResult Detail(Guid Id)
        {
            var model = new DepartmentModel();
            if (Id != Guid.Empty)
            {
                var department = Department.GetById(Id);
                if (department != null)
                {
                    model.InjectFrom(department);
                }
            }

            ViewBag.CompanyList = DropDownHelper.BindCompanyList(model.CompanyId);
            ViewData.Model = model;
            return View();
        }

        [HttpPost]
        public virtual ActionResult Detail(DepartmentModel model)
        {
            var response = new ResponseModel(false);
            Department department = new Department();

            if (ModelState.IsValid)
            {
                if (model.ID != Guid.Empty)
                {
                    department = Department.GetById(model.ID);
                    if (department != null)
                    {
                        department.IsDeleted = model.IsDeleted;
                        department.IsHaveNonInventory = model.IsHaveNonInventory;
                        department.MaintenanceTypeCode = (!string.IsNullOrEmpty(model.MaintenanceTypeCode) ? model.MaintenanceTypeCode : string.Empty);
                        department.MappingCostCenter = (!string.IsNullOrEmpty(model.MappingCostCenter) ? model.MappingCostCenter : string.Empty);
                        department.CategoryCode = (!string.IsNullOrEmpty(model.CategoryCode) ? model.CategoryCode : string.Empty);
                        department.DepartmentName = department.DepartmentName;
                        department.Save<Department>();
                        response.SetSuccess("Department {0} has been update", model.DepartmentName);
                    }
                    else
                    {
                        response.SetError("Department {0} not found", model.DepartmentName);
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(model.MaintenanceTypeCode))
                    {
                        model.MaintenanceTypeCode = string.Empty;
                    }

                    model.MappingCostCenter = (string.IsNullOrEmpty(model.MappingCostCenter) ? "" : model.MappingCostCenter);

                    department.InjectFrom(model);
                    department.ID = Guid.NewGuid();
                    department.Save<Department>();
                    response.SetSuccess("Department {0} has been insert", department.DepartmentName);
                }
            }
            else
            {
                response.SetError(FunctionWebHelpers.GetErrorModelState(ModelState));
            }

            this.AddNotification(response);
            if (response.IsSuccess)
            {
                return RedirectToAction("Index");
            }

            ViewBag.CompanyList = DropDownHelper.BindCompanyList(department.CompanyId);
            return View(model);
        }

        [HttpGet]
        public virtual ActionResult Delete(Guid Id)
        {
            Department.DeleteById(Id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual JsonResult GetByCompanyId(Guid CompanyId)
        {
            var departments = Department.GetByCompanyId(CompanyId).ToList();
            var result = departments.Select(x => new Department() {
                ID = x.ID,
                CompanyId = (x.Company != null ? x.Company.ID : Guid.Empty),
                DepartmentName = x.DepartmentName
            });

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}