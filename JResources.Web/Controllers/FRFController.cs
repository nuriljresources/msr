﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JResources.Data;
using JResources.Data.Model;
using JResources.BusinessLogic;
using JResources.Common;
using System.Text.RegularExpressions;
using Omu.ValueInjecter;
using System.Text;
using System.Transactions;

namespace JResources.Web.Controllers
{
    public partial class FRFController : Controller
    {
        #region PAGE LIST FUEL
        /// <summary>
        /// List Request Fuel
        /// </summary>
        /// <returns></returns>
        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.FRF_ENTRY, ROLE_CODE.FRF_APPROVAL, ROLE_CODE.FRF_ISSUED })]
        public virtual ActionResult Index()
        {
            var model = new FRFSearchModel();
            List<EnumTable> MSRStatus = new List<EnumTable>() { new EnumTable() { EnumValue = "", EnumLabel = "SELECT ALL STATUS" } };
            MSRStatus.AddRange(EnumTable.GetByCode(EnumTable.EnumType.STATUS_MSR).ToList());

            MSRStatus = MSRStatus.Where(x =>
                x.EnumValue == ""
                || x.EnumValue == Common.Enum.RequestStatus.NEW
                || x.EnumValue == Common.Enum.RequestStatus.CANCEL
                || x.EnumValue == Common.Enum.RequestStatus.REJECTED
                || x.EnumValue == Common.Enum.RequestStatus.APPROVED
                || x.EnumValue == Common.Enum.RequestStatus.ENTRY_FUEL
                || x.EnumValue == Common.Enum.RequestStatus.ISSUED).ToList();

            List<CompanyDepartmentModel> CompanyDepartments = new List<CompanyDepartmentModel>() { new CompanyDepartmentModel { ID = Guid.Empty, CompanyName = "SELECT All COMPANY" } };
            CompanyDepartments.AddRange(CompanyLogic.GetFullCompanyWithDepartmentByCurrentRole(new string[] { ROLE_CODE.FRF_ENTRY, ROLE_CODE.FRF_ISSUED, ROLE_CODE.FRF_APPROVAL }));

            ViewData.Model = model;
            ViewBag.MSRStatus = MSRStatus;
            ViewBag.CompanyDepartments = CompanyDepartments;

            return View();
        }

        /// <summary>
        /// Get JSON FRF
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult GetListFRF(FRFSearchModel model)
        {
            model.ROLE_ACCESS = new string[] { ROLE_CODE.FRF_ENTRY, ROLE_CODE.FRF_APPROVAL, ROLE_CODE.FRF_ISSUED };
            model = FRFLogic.GetListFRF(model);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region PAGE LIST NEED APPROVED
        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.FRF_APPROVAL })]
        public virtual ActionResult ListApprove()
        {
            var model = new FRFSearchModel();
            List<EnumTable> MSRStatus = new List<EnumTable>() { new EnumTable() { EnumValue = "", EnumLabel = "SELECT ALL STATUS" } };
            MSRStatus.AddRange(EnumTable.GetByCode(EnumTable.EnumType.STATUS_MSR).ToList());

            MSRStatus = MSRStatus.Where(x =>
                x.EnumValue == ""
                || x.EnumValue == Common.Enum.RequestStatus.NEW                
                || x.EnumValue == Common.Enum.RequestStatus.ENTRY_FUEL).ToList();

            List<CompanyDepartmentModel> CompanyDepartments = new List<CompanyDepartmentModel>() { new CompanyDepartmentModel { ID = Guid.Empty, CompanyName = "SELECT All COMPANY" } };
            CompanyDepartments.AddRange(CompanyLogic.GetFullCompanyWithDepartmentByCurrentRole(new string[] { ROLE_CODE.FRF_APPROVAL }));

            ViewData.Model = model;
            ViewBag.MSRStatus = MSRStatus;
            ViewBag.CompanyDepartments = CompanyDepartments;

            return View();
        }

        /// <summary>
        /// Get JSON FRF
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult GetListFRFApprovel(FRFSearchModel model)
        {
            model.ROLE_ACCESS = new string[] { ROLE_CODE.FRF_APPROVAL };
            model.MSR_STATUS = new string[] { Common.Enum.RequestStatus.NEW, Common.Enum.RequestStatus.ENTRY_FUEL };
            model = FRFLogic.GetListFRF(model);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion


        /// <summary>
        /// Detail Fuel Request
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [JResourcesAuthorization(ROLE_CODE = ROLE_CODE.FRF_ENTRY)]
        public virtual ActionResult Detail(Guid Id)
        {
            var model = new FRFModel();
            if (Id != Guid.Empty)
            {
                model = FRFLogic.GetDataById(Id);
            }
            else
            {
                model.UserCreated = UserLogic.GetByNIK(CurrentUser.NIKSITE);
            }

            var paramItem = new PartItemSearchModel();
            paramItem.ItemNo = "B0";
            paramItem.IsGetAllItem = true;
            var ItemFuels = ItemLogic.GetItemList(paramItem);

            List<SelectListItem> ListMaintenanceCodes = new List<SelectListItem>();
            List<FRFItemEquipment> ListEquipments = new List<FRFItemEquipment>();

            List<CSOPTFD> MaintenanceCodes = CSOPTFD.GetMaintenanceCode().ToList();
            foreach (var maintenanceCode in MaintenanceCodes)
            {
                ListMaintenanceCodes.Add(new SelectListItem() {
                    Value = maintenanceCode.VALUE.Trim(),
                    Text = string.Format("{0}-{1}", maintenanceCode.VALUE.Trim(), maintenanceCode.VDESC.Trim())
                });
            }

            List<CSOPTFD> Equipments = CSOPTFD.GetEquipment().ToList();
            List<string> EquipmentCodes = Equipments.Select(x => x.VALUE.Trim()).ToList();
            List<EquipmentMapping> equipmentMappings = EquipmentMapping.GetAll()
                .Where(x => EquipmentCodes.Contains(x.EquipmentCode)).ToList();

            foreach (var equipment in Equipments)
            {
                var CostMapping = equipmentMappings.FirstOrDefault(x => x.EquipmentCode == equipment.VALUE.Trim());

                ListEquipments.Add(new FRFItemEquipment() {
                    EquipmentCode = equipment.VALUE.Trim(),
                    EquipmentLabel = string.Format("{0}-{1}", equipment.VALUE.Trim(), equipment.VDESC.Trim()),
                    CostCenter = CostMapping != null ? CostMapping.CostCode : string.Empty,
                    COA = CostMapping != null ? CostMapping.COA : string.Empty,
                });
            }

            Company company = Company.GetByCode(CurrentUser.COMPANY);
            var FRFApproval = RoleAccess.GetByCode(ROLE_CODE.FRF_APPROVAL);
            var FRFApprovalUser = RoleAccessUser.GetByRoleAccessId(FRFApproval.ID)
                .Where(x => x.CompanyId == company.ID)
                .Where(x => (x.IsDefault || x.DateFrom >= DateTime.Now && x.DateTo <= DateTime.Now))
                .OrderBy(x => x.UpdatedDate).FirstOrDefault();

            model.UserApproval = UserLogic.GetByUserId(FRFApprovalUser.UserId);

            ViewBag.ItemFuels = ItemFuels;
            ViewBag.ListMaintenanceCodes = ListMaintenanceCodes;
            ViewBag.ListEquipments = ListEquipments;
            ViewData.Model = model;
            return View();
        }

        [HttpPost]
        public virtual JsonResult FuelSave(FRFModel model)
        {
            ResponseModel response = new ResponseModel();
            StringBuilder sbError = new StringBuilder();

            try
            {
                if (string.IsNullOrEmpty(model.FRFDateStr))
                {
                    sbError.AppendLine("FRF Date cannot be null");
                }
                else
                {
                    DateTime dtFRFDate = CommonFunction.DateTimeJResources(model.FRFDateStr);

                    if (dtFRFDate == DateTime.MinValue)
                    {
                        sbError.AppendLine("Format date not correct");
                    }
                    else
                    {
                        model.FRFDate = dtFRFDate;
                    }
                }

                if (model.FRFDetails != null && model.FRFDetails.Count > 0)
                {
                    int iLine = 1;
                    foreach (var itemFRF in model.FRFDetails)
                    {
                        if (!(itemFRF.ItemPart != null && !string.IsNullOrEmpty(itemFRF.ItemPart.ItemNo)))
                        {
                            sbError.AppendLine(string.Format("Line {0} must select item", iLine));
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(itemFRF.LocationId))
                            {
                                sbError.AppendLine(string.Format("Line {0} must select location", iLine));
                            }

                            if (!(itemFRF.Qty.HasValue && itemFRF.Qty.Value > 0))
                            {
                                sbError.AppendLine(string.Format("Line {0} must input Qty", iLine));
                            }

                            if (string.IsNullOrEmpty(itemFRF.MaterialCode))
                            {
                                sbError.AppendLine(string.Format("Line {0} must select material code", iLine));
                            }

                            if (string.IsNullOrEmpty(itemFRF.Category))
                            {
                                sbError.AppendLine(string.Format("Line {0} must have category", iLine));
                            }

                            if (itemFRF.Equipment != null && !string.IsNullOrEmpty(itemFRF.Equipment.EquipmentCode))
                            {
                                if (string.IsNullOrEmpty(itemFRF.Equipment.CostCenter))
                                {
                                    sbError.AppendLine(string.Format("Line {0} with equipment {1} dont have mapping cost center", iLine, itemFRF.Equipment.EquipmentCode));
                                }

                                if (string.IsNullOrEmpty(itemFRF.Equipment.COA))
                                {
                                    sbError.AppendLine(string.Format("Line {0} with equipment {1} dont have mapping COA", iLine, itemFRF.Equipment.EquipmentCode));
                                }
                            }
                            else
                            {
                                sbError.AppendLine(string.Format("Line {0} must select equipment code", iLine));
                            }

                            if (!(itemFRF.EndOfKM.HasValue && itemFRF.EndOfKM.Value > 0))
                            {
                                sbError.AppendLine(string.Format("Line {0} must input HM/KM Unit", iLine));
                            }
                        }

                        iLine++;
                    }
                }
                else
                {
                    sbError.AppendLine("FRF dont have detail");
                }

                if (sbError.Length > 0)
                {
                    response.SetError(sbError.ToString());
                }


                if (response.IsSuccess)
                {
                    Company company = Company.GetByCode(CurrentUser.COMPANY);
                    var FRFApproval = RoleAccess.GetByCode(ROLE_CODE.FRF_APPROVAL);
                    var FRFApprovalUser = RoleAccessUser.GetByRoleAccessId(FRFApproval.ID)
                        .Where(x => x.CompanyId == company.ID)
                        .Where(x => (x.IsDefault || x.DateFrom >= DateTime.Now && x.DateTo <= DateTime.Now))
                        .OrderBy(x => x.UpdatedDate).FirstOrDefault();

                    var UserApproval = UserLogic.GetByUserId(FRFApprovalUser.UserId);


                    using (var transaction = new TransactionScope())
                    {
                        tfrf_master frf = new tfrf_master();
                        frf.ID = Guid.NewGuid();
                        frf.frf_no = DocumentNumberLogic.GetFRFDocumentNo(company.ID);
                        frf.frf_dt = model.FRFDate;
                        frf.requestor = CurrentUser.NIKSITE;
                        frf.approver = UserApproval.NIKSite;
                        frf.compid = CurrentUser.COMPANY;
                        frf.status_id = Common.Enum.RequestStatus.NEW;
                        frf.is_project = false;
                        frf.Save<tfrf_master>();

                        foreach (var item in model.FRFDetails)
                        {
                            tfrf_detail detail = new tfrf_detail();
                            detail.ID = Guid.NewGuid();
                            detail.frf_no = frf.frf_no;
                            detail.stock_no = item.ItemPart.ItemNo;
                            detail.item_desc = item.ItemPart.Description;
                            detail.log_sheet_dt = model.FRFDate;
                            detail.qty = item.Qty;
                            detail.loc_id = item.LocationId;
                            detail.end_of_km = item.EndOfKM;
                            detail.oper_nm = string.IsNullOrEmpty(item.OperatorName) ? "" : item.OperatorName;
                            detail.remarks = "";
                            detail.mtcode = item.MaterialCode;
                            detail.eccode = item.Equipment.EquipmentCode;
                            detail.cccode = item.Equipment.CostCenter;
                            detail.shift = item.Shift;
                            detail.cat_code = item.Category;
                            detail.is_project = false;
                            detail.contract = string.Empty;
                            detail.project = string.Empty;
                            detail.category = item.Category;
                            detail.InsertSave<tfrf_detail>();
                        }

                        transaction.Complete();
                        response.SetSuccess("FRF {0} has been saved", frf.frf_no);
                        this.AddNotification(response.Message, NotificationType.SUCCESS);
                    }
                    
                }
            }
            catch (Exception ex)
            {
                response.SetError("Error Exception");
                Logger.Set(LoggerCategory.FRF_CREATE, "Fuel/FuelSave", model, ex);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Modal Fuel Detail
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public virtual ActionResult FRFModal()
        {
            List<SelectListItem> StatusMSR = new List<SelectListItem>();
            List<EnumTable> MSRStatus = EnumTable.GetByCode(EnumTable.EnumType.STATUS_MSR).ToList();

            MSRStatus = MSRStatus.Where(x =>
                x.EnumValue == Common.Enum.RequestStatus.REJECTED
                || x.EnumValue == Common.Enum.RequestStatus.APPROVED).ToList();

            foreach (var item in MSRStatus)
            {
                StatusMSR.Add(new SelectListItem()
                {
                    Value = item.EnumValue,
                    Text = item.EnumLabel
                });
            }

            ViewBag.StatusMSR = StatusMSR;
            ViewData.Model = new FRFModel();
            return PartialView();
        }

        /// <summary>
        /// For Detail Modal MSR
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual JsonResult GetFRFModal(Guid Id)
        {
            FRFModel model = FRFLogic.GetDataById(Id);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Fule Approved
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult ApproveFRF(FRFModel model)
        {
            var response = new ResponseModel();
            try
            {
                response = FRFLogic.SetActionFRFStatusApproval(model.ID, model.FRFStatus);
            }
            catch (Exception ex)
            {
                response.SetError(ex.Message);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
