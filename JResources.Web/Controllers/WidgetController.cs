﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JResources.BusinessLogic;
using JResources.Common;
using JResources.Data.Model;

namespace JResources.Web.Controllers
{
    public partial class WidgetController : Controller
    {
        public virtual ActionResult Header()
        {
            return PartialView();
        }

        //[OutputCache(Duration = 3600, VaryByParam = "none")]
        public virtual ActionResult Sidebar()
        {
            var models = new List<MenuModel>();
            models.Add(new MenuModel()
            {
                NameMenu = "Dashboard",
                Url = Url.Action(MVC.Home.Index()),
                RoleAccess = new string[] { 
                    ROLE_CODE.USERS 
                },
                ControllerName = "Home",
                ActionName = "Index",
                Icon = "icon-home"
            });

            #region Material & Fueld Menu
            models.AddRange(
                new List<MenuModel>() {
                    new MenuModel()
                    {
                        NameMenu = "Material & Fuel Menu",
                        Url = "",
                        RoleAccess = new string[] { 
                            ROLE_CODE.FRF_ENTRY, 
                            ROLE_CODE.FRF_APPROVAL, 
                            ROLE_CODE.FRF_ISSUED
                        },
                        IsHeading = true
                    },
                    
                    #region DETAIL FUEL REQUEST
                    new MenuModel()
                    {
                        NameMenu = "Fuel Request",
                        Url = "",
                        RoleAccess = new string[] { 
                            ROLE_CODE.FRF_ENTRY, 
                            ROLE_CODE.FRF_APPROVAL 
                        },
                        Icon = "glyphicon glyphicon-tint",

                        ChildsMenus = new List<MenuModel>(){

                            new MenuModel(){
                                NameMenu = "Create Request",
                                Url = Url.Action(MVC.FRF.Detail(Guid.Empty)),
                                RoleAccess = new string[] { 
                                    ROLE_CODE.FRF_ENTRY 
                                },
                                ControllerName = "FRF",
                                ActionName = "Detail",
                                Icon = "fa fa-pencil-square-o"
                            },

                            new MenuModel(){
                                NameMenu = "Approve Request",
                                Url = Url.Action(MVC.FRF.ListApprove()),
                                RoleAccess = new string[] { 
                                    ROLE_CODE.FRF_APPROVAL 
                                },
                                ControllerName = "FRF",
                                ActionName = "ListApprove",
                                Icon = "fa fa-check-square-o"
                            },

                            new MenuModel(){
                                NameMenu = "List Request",
                                Url = Url.Action(MVC.FRF.Index()),
                                RoleAccess = new string[] { 
                                    ROLE_CODE.FRF_ENTRY, 
                                    ROLE_CODE.FRF_ISSUED 
                                },
                                ControllerName = "FRF",
                                ActionName = "Index",
                                Icon = "fa fa-list-ul"
                            },

                        }
                    },
                    #endregion

                    #region DETAIL MATERIAL REQUEST
                    new MenuModel()
                    {
                        NameMenu = "Material Request",
                        Url = "",
                        RoleAccess = new string[] { 
                            ROLE_CODE.MSR_ENTRY,  
                            ROLE_CODE.MSR_APPROVAL, 
                            ROLE_CODE.MSR_APPROVAL_GM,
                            //20200826FM D.Nugroho
                            ROLE_CODE.CEA_ENTRY
                        },
                        Icon = "fa fa-cube",

                        ChildsMenus = new List<MenuModel>(){
                            new MenuModel(){
                                NameMenu = "Create Request",
                                Url = Url.Action(MVC.MSR.Detail(Guid.Empty)),
                                RoleAccess = new string[] { 
                                    ROLE_CODE.MSR_ENTRY 
                                },
                                ControllerName = "MSR",
                                ActionName = "Detail",
                                Icon = "fa fa-pencil-square-o"
                            },

                            new MenuModel(){
                                NameMenu = "Create Stock Order",
                                Url = Url.Action(MVC.MSR.DetailReOrderPoint(Guid.Empty)),
                                RoleAccess = new string[] { 
                                    ROLE_CODE.MSR_ROP_ENTRY 
                                },
                                ControllerName = "MSR",
                                ActionName = "DetailReOrderPoint",
                                Icon = "fa fa-pencil-square-o"
                            },

                            new MenuModel(){
                                NameMenu = "Approve Request",
                                Url = Url.Action(MVC.MSR.ListApprove()),
                                RoleAccess = new string[] { 
                                    ROLE_CODE.MSR_APPROVAL, 
                                    ROLE_CODE.MSR_APPROVAL_GM 
                                },
                                ControllerName = "MSR",
                                ActionName = "ListApprove",
                                Icon = "fa fa-check-square-o"
                            },

                            new MenuModel(){
                                NameMenu = "List Request",
                                Url = Url.Action(MVC.MSR.Index()),
                                RoleAccess = new string[] { 
                                    ROLE_CODE.MSR_ENTRY,  
                                    ROLE_CODE.MSR_APPROVAL, 
                                    ROLE_CODE.MSR_APPROVAL_GM
                                },
                                ControllerName = "MSR",
                                ActionName = "Index|MSRTimeline",
                                Icon = "fa fa-list-ul"
                            },                               
                        
                            new MenuModel(){
                                NameMenu = "Cancel Request",
                                Url = Url.Action(MVC.Warehouse.CancelRequest()),
                                RoleAccess = new string[] { 
                                    ROLE_CODE.MSR_CANCEL 
                                },
                                ControllerName = "Warehouse",
                                ActionName = "CancelRequest",
                                Icon = "fa fa-times-circle"
                            }, 
                        }


                    },
                    #endregion

                    #region DETAIL WAREHOUSE
                    new MenuModel()
                    {
                        NameMenu = "Warehouse",
                        Url = "",
                        RoleAccess = new string[] { ROLE_CODE.MSR_ISSUED,  ROLE_CODE.FRF_ISSUED },
                        Icon = "fa fa-cubes",

                        ChildsMenus = new List<MenuModel>(){

                            new MenuModel(){
                                NameMenu = "Transfer Material",
                                Url = Url.Action(MVC.Warehouse.TransferRequest()),
                                RoleAccess = new string[] { ROLE_CODE.MSR_ISSUED },
                                ControllerName = "Warehouse",
                                ActionName = "TransferRequest",
                                Icon = "fa fa-exchange"
                            }, 

                            new MenuModel(){
                                NameMenu = "Issued Material",
                                Url = Url.Action(MVC.Warehouse.IssuedRequest()),
                                RoleAccess = new string[] { ROLE_CODE.MSR_ISSUED },
                                ControllerName = "Warehouse",
                                ActionName = "IssuedRequest",
                                Icon = "fa fa-check-square-o"
                            }, 

                            new MenuModel(){
                                NameMenu = "List Request Material",
                                Url = Url.Action(MVC.Warehouse.ListRequestIssued()),
                                RoleAccess = new string[] { ROLE_CODE.MSR_ISSUED },
                                ControllerName = "Warehouse",
                                ActionName = "ListRequestIssued",
                                Icon = "fa fa-list-ul"
                            },

                            new MenuModel(){
                                NameMenu = "Issued Fuel Request",
                                Url = Url.Action(MVC.Warehouse.ListFuelIssued()),
                                RoleAccess = new string[] { ROLE_CODE.FRF_ISSUED },
                                ControllerName = "Warehouse",
                                ActionName = "ListFuelIssued",
                                Icon = "fa fa-pencil-square-o"
                            },


                        }
                    },
                    #endregion

                    #region DETAIL REQUISITION
                    new MenuModel()
                    {
                        NameMenu = "Requisition",
                        Url = "",
                        RoleAccess = new string[] { 
                            ROLE_CODE.PROCUREMENT
                        },
                        Icon = "icon-basket",

                        ChildsMenus = new List<MenuModel>(){
                            new MenuModel(){
                                NameMenu = "Create Requisition",
                                Url = Url.Action(MVC.Purchase.DetailPurchase(Guid.Empty)),
                                RoleAccess = new string[] { 
                                    ROLE_CODE.PROCUREMENT 
                                },
                                ControllerName = "Purchase",
                                ActionName = "DetailPurchase",
                                Icon = "fa fa-pencil-square-o"
                            },

                            new MenuModel(){
                                NameMenu = "List Requisition",
                                Url = Url.Action(MVC.Purchase.Index()),
                                RoleAccess = new string[] { 
                                    ROLE_CODE.PROCUREMENT,
                                },
                                ControllerName = "Purchase",
                                ActionName = "Index",
                                Icon = "fa fa-list-ul"
                            },

                            //D. Nugroho 20210719
                            new MenuModel(){
                                NameMenu = "Create CBE",
                                Url = Url.Action(MVC.CBE.DetailCBE(Guid.Empty)),
                                RoleAccess = new string[] {
                                    ROLE_CODE.PROCUREMENT
                                },
                                ControllerName = "CBE",
                                ActionName = "DetailCBE",
                                Icon = "fa fa-pencil-square-o"
                            },

                            new MenuModel(){
                                NameMenu = "List CBE",
                                Url = Url.Action(MVC.CBE.Index()),
                                RoleAccess = new string[] {
                                    ROLE_CODE.PROCUREMENT,
                                },
                                ControllerName = "CBE",
                                ActionName = "Index",
                                Icon = "fa fa-list-ul"
                            },
                        }
                    },

                    // Menu CEA To RQN dipindah satu menu saja
                    new MenuModel()
                    {
                        NameMenu = "CEA Requistion",
                        Url = Url.Action(MVC.CEA.Index()),
                        RoleAccess = new string[] {
                            ROLE_CODE.USERS,
                            ROLE_CODE.CEA_ENTRY
                        },
                        ControllerName = "CEA",
                        ActionName = "Index",
                        Icon = "fa fa-list-alt"
                    },
                    #endregion
                });
            #endregion

            if (SiteSettings.IS_PST_LIVE)
            {
                #region PST MENU
                models.AddRange(
                    new List<MenuModel>(){

                    new MenuModel()
                    {
                        NameMenu = "PST Menu",
                        Url = "",
                        RoleAccess = new string[] { 
                            ROLE_CODE.USERS, 
                            ROLE_CODE.PST_CREATOR
                        },
                        IsHeading = true
                    },

                    #region PST Planning
                    new MenuModel()
                    {
                        NameMenu = "PST Planning",
                        Url = "",
                        RoleAccess = new string[] { 
                            ROLE_CODE.PST_CREATOR
                        },
                        Icon = "icon-calendar",

                        ChildsMenus = new List<MenuModel>(){
                            new MenuModel(){
                                NameMenu = "Upload Document",
                                Url = Url.Action(MVC.PST.Planning.Detail()),
                                RoleAccess = new string[] { 
                                    ROLE_CODE.PST_CREATOR 
                                },
                                ControllerName = "Planning",
                                ActionName = "Detail|ScheduleView",
                                Icon = "icon-cloud-upload"
                            },

                            new MenuModel(){
                                NameMenu = "List Document",
                                Url = Url.Action(MVC.PST.Planning.Index()),
                                RoleAccess = new string[] { 
                                    ROLE_CODE.PST_CREATOR
                                },
                                ControllerName = "Planning",
                                ActionName = "Index",
                                Icon = "fa fa-list-ul"
                            },
                        }

                    },
                    #endregion

                    #region PST Document
                    new MenuModel()
                    {
                        NameMenu = "PST Document",
                        Url = "",
                        RoleAccess = new string[] { 
                            ROLE_CODE.PST_CREATOR
                        },
                        Icon = "fa fa-file-text",

                        ChildsMenus = new List<MenuModel>(){
                            new MenuModel(){
                                NameMenu = "Create Document",
                                Url = Url.Action(MVC.PST.PSTDocument.Create()),
                                RoleAccess = new string[] { 
                                    ROLE_CODE.PST_CREATOR 
                                },
                                ControllerName = "PSTDocument",
                                ActionName = "Create",
                                Icon = "fa fa-pencil-square-o"
                            },

                            new MenuModel(){
                                NameMenu = "List Document",
                                Url = Url.Action(MVC.PST.PSTDocument.Index()),
                                RoleAccess = new string[] { 
                                    ROLE_CODE.PST_CREATOR 
                                },
                                ControllerName = "PSTDocument",
                                ActionName = "Index",
                                Icon = "fa fa-list-ul"
                            },
                        }

                    },
                    #endregion
                }
                );
                #endregion
            }

            #region Other & Setting
            models.AddRange(
                new List<MenuModel>(){

                    #region Other Menu
                    new MenuModel()
                    {
                        NameMenu = "Other Menu",
                        Url = "",
                        RoleAccess = new string[] { 
                            ROLE_CODE.USERS, 
                            ROLE_CODE.REPORT
                        },
                        IsHeading = true
                    },

                    new MenuModel()
                    {
                        NameMenu = "Report",
                        Url = Url.Action(MVC.Report.Index()),
                        RoleAccess = new string[] { ROLE_CODE.REPORT },
                        ControllerName = "Report",
                        ActionName = "Index",
                        Icon = "fa fa-bar-chart-o"
                    },

                    new MenuModel()
                    {
                        NameMenu = "Data Synchronize",
                        Url = Url.Action(MVC.Log.Synchronize()),
                        RoleAccess = new string[] { ROLE_CODE.USERS },
                        ControllerName = "Log",
                        ActionName = "Synchronize",
                        Icon = "fa fa-retweet"
                    },
                    #endregion

                    #region Setting Menu
                    new MenuModel()
                    {
                        NameMenu = "Settings",
                        Url = "",
                        RoleAccess = new string[] { ROLE_CODE.ADMINISTRATOR, ROLE_CODE.USERS },
                        IsHeading = true
                    },

                    new MenuModel()
                    {
                        NameMenu = "Application Settings",
                        Url = "",
                        RoleAccess = new string[] { ROLE_CODE.ADMINISTRATOR },
                        Icon = "icon-settings",

                        ChildsMenus = new List<MenuModel>(){
                            new MenuModel(){
                                NameMenu = "Job Priority",
                                Url = Url.Action(MVC.Setting.JobPriority()),
                                RoleAccess = new string[] { ROLE_CODE.ADMINISTRATOR },
                                ControllerName = "Setting",
                                ActionName = "JobPriority",
                                Icon = "fa fa-list-ol"
                            },

                            new MenuModel(){
                                NameMenu = "Equipment Mapping",
                                Url = Url.Action(MVC.Setting.EquipmentMapping()),
                                RoleAccess = new string[] { ROLE_CODE.ADMINISTRATOR },
                                ControllerName = "Setting",
                                ActionName = "EquipmentMapping",
                                Icon = "fa fa-random"
                            },
                        }
                    },

                    new MenuModel()
                    {
                        NameMenu = "Company",
                        Url = "",
                        RoleAccess = new string[] { ROLE_CODE.ADMINISTRATOR },
                        Icon = "fa fa-building",

                        ChildsMenus = new List<MenuModel>(){
                            new MenuModel(){
                                NameMenu = "Entry Company",
                                Url = Url.Action(MVC.Company.Detail(Guid.Empty)),
                                RoleAccess = new string[] { ROLE_CODE.ADMINISTRATOR },
                                ControllerName = "Company",
                                ActionName = "Detail"
                            },

                            new MenuModel(){
                                NameMenu = "List Company",
                                Url = Url.Action(MVC.Company.Index()),
                                RoleAccess = new string[] { ROLE_CODE.ADMINISTRATOR },
                                ControllerName = "Company",
                                ActionName = "Index"
                            },

                            new MenuModel(){
                                NameMenu = "Entry Department",
                                Url = Url.Action(MVC.Department.Detail(Guid.Empty)),
                                RoleAccess = new string[] { ROLE_CODE.ADMINISTRATOR },
                                ControllerName = "Department",
                                ActionName = "Detail"
                            },

                            new MenuModel(){
                                NameMenu = "List Department",
                                Url = Url.Action(MVC.Department.Index()),
                                RoleAccess = new string[] { ROLE_CODE.ADMINISTRATOR },
                                ControllerName = "Department",
                                ActionName = "Index"
                            },

                        }
                    },


                    new MenuModel()
                    {
                        NameMenu = "User Management",
                        Url = "",
                        RoleAccess = new string[] { ROLE_CODE.ADMINISTRATOR, ROLE_CODE.USERS },
                        Icon = "icon-users",

                        ChildsMenus = new List<MenuModel>(){
                            new MenuModel(){
                                NameMenu = "Entry User",
                                Url = Url.Action(MVC.User.UserDetail(Guid.Empty)),
                                RoleAccess = new string[] { ROLE_CODE.ADMINISTRATOR },
                                ControllerName = "User",
                                ActionName = "UserDetail",
                                Icon = "fa fa-user"
                            },

                            new MenuModel(){
                                NameMenu = "List User",
                                Url = Url.Action(MVC.User.Index()),
                                RoleAccess = new string[] { ROLE_CODE.ADMINISTRATOR },
                                ControllerName = "User",
                                ActionName = "Index",
                                Icon = "fa fa-users"
                            },

                            new MenuModel(){
                                NameMenu = "Code Access",
                                Url = Url.Action(MVC.User.RoleAccessList()),
                                RoleAccess = new string[] { ROLE_CODE.ADMINISTRATOR },
                                ControllerName = "User",
                                ActionName = "RoleAccessList",
                                Icon = "fa fa-qrcode"
                            },

                            new MenuModel(){
                                NameMenu = "List Delegation",
                                Url = Url.Action(MVC.User.DelegationList()),
                                RoleAccess = new string[] { ROLE_CODE.USERS },
                                ControllerName = "User",
                                ActionName = "DelegationList",
                                Icon = "fa fa-list-ul"
                            },

                            //new MenuModel(){
                            //    NameMenu = "Entry Delegation Role",
                            //    Url = Url.Action(MVC.User.DelegationEntry(Guid.Empty)),
                            //    RoleAccess = new string[] { ROLE_CODE.ADMINISTRATOR, ROLE_CODE.USERS },
                            //    ControllerName = "User",
                            //    ActionName = "DelegationEntry",
                            //    Icon = "fa fa-unlock-alt"
                            //},

                        }
                    }

                    #endregion
                }
            );
            #endregion

            ViewData.Model = models;
            return PartialView();
        }

        public virtual ActionResult Notification()
        {
            List<NotificationModel> list = new List<NotificationModel>();
            UserModel user = UserLogic.GetByNIK("JRN0291");


            list.AddRange(new List<NotificationModel>(){

                new NotificationModel() {
                    UserCreated = user,
                    Area = "MSR",
                    Icon = "fa fa-cube",
                    TargetUrl = Url.Action(MVC.MSR.Detail(Guid.Empty))
                },
                new NotificationModel() { },
                new NotificationModel() { },
                new NotificationModel() { }

            });

            ViewData.Model = list;
            return PartialView();
        }

        public virtual ActionResult UserDropDown()
        {
            return PartialView();
        }

        public virtual ActionResult Sidebar2()
        {
            return PartialView();
        }

        public virtual ActionResult Footer()
        {
            return PartialView();
        }

    }
}

