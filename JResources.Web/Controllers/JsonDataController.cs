﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JResources.BusinessLogic;
using JResources.Data;
using JResources.Data.Model;

namespace JResources.Web.Controllers
{
    public partial class JsonDataController : Controller
    {
        /// <summary>
        /// JSON request data from active directory
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public virtual JsonResult SearchUserByActiveDirectory(string username)
        {
            UserModel model = new UserModel();

            UserLoginModel userLogin = new UserLoginModel();
            if(!username.Contains("\\"))
            {
                username = "JRESOURCES\\" + username; 
            }

            userLogin.Username = username;

            var response = UserLogic.LoginActiveDirectory(userLogin, true);
            if (response.IsSuccess && response.ResponseObject != null)
            {
                model = (UserModel)response.ResponseObject;
                var checkUser = UserLogic.GetFullDataByNik(model.NIKSite);
                if (checkUser != null && checkUser.ID != Guid.Empty)
                {
                    model = checkUser;
                }
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get List Department by CompanyId
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <returns></returns>
        public virtual JsonResult GetDepartmentByCompanyId(Guid CompanyId)
        {
            List<SelectListItem> model = new List<SelectListItem>();
            var depatments = Department.GetByCompanyId(CompanyId);
            if (depatments != null)
            {
                foreach (var item in depatments)
                {
                    SelectListItem listItem = new SelectListItem();
                    listItem.Value = item.ID.ToString();
                    listItem.Text = item.DepartmentName;
                    model.Add(listItem);
                }
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get JSON Equipment List By Deparment
        /// </summary>
        /// <param name="DepartmentId"></param>
        /// <returns></returns>
        public virtual JsonResult EquipmentByDepartmentId(Guid DepartmentId)
        {
            var equipment = EquipmentLogic.GetListEquipment(DepartmentId);
            return Json(equipment, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get JSON Cost Center List By Deparment
        /// </summary>
        /// <param name="DepartmentId"></param>
        /// <returns></returns>
        [HandleError]
        [OutputCache(Duration = 3600, VaryByParam = "DepartmentId")]
        public virtual JsonResult CostCenterByDepartmentId(Guid DepartmentId)
        {
            var costcenter = CostCenterLogic.GetCostCenterByDepartmentId(DepartmentId);
            return Json(costcenter, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get JSON Work Order By Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual JsonResult GetWorkOrderById(string WorkOrderNo)
        {
            MSRModel model = WorkOrderLogic.GetWorkOrderById(WorkOrderNo, Guid.Empty);
            MSRModel MSRApproval = MSRLogic.GetApproval(model.DepartmentId);

            model.Manager = MSRApproval.Manager;
            model.GeneralManagerSite = MSRApproval.GeneralManagerSite;
            model.TotalWithApprovalGM = MSRApproval.TotalWithApprovalGM;
            model.Manager = MSRApproval.Manager;

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Approval MSR By Role and Department
        /// </summary>
        /// <param name="DepartmentId"></param>
        /// <returns></returns>
        [HttpGet]
        //[OutputCache(Duration = 3600, VaryByParam = "DepartmentId")]
        public virtual JsonResult GetApprovalByDepartment(Guid DepartmentId)
        {
            MSRModel model = MSRLogic.GetApproval(DepartmentId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
