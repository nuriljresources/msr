﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JResources.BusinessLogic;
using JResources.Common;
using JResources.Data.Model;
using JResources.Data.Model.Chart;

namespace JResources.Web.Controllers
{
    [JResourcesAuthorization(ROLE_CODE = ROLE_CODE.USERS)]
    public partial class HomeController : Controller
    {
        public virtual ActionResult Index()
        {
            var model = new DashboardModel();
            //CompanyModel company = CurrentUser.GetCurrentCompany();
            //model.TOTAL_NEW_REQUEST = DashboardLogic.GetTotalNewRequest(company.ID);
            //model.TOTAL_APPROVED_REQUEST = DashboardLogic.GetTotalApproveRequest(company.ID);
            //model.TOTAL_TRANSFER_WAREHOUSE = DashboardLogic.GetTotalTransferWarehouse(company.ID);
            //model.TOTAL_PENDING_ACCPAC = DashboardLogic.GetTotalPendingToAccpac();

            var MSRSearch = new MSRSearchModel();
            MSRSearch.IsExcludeDataMigration = true;
            MSRSearch.IsRecentActivity = true;
            MSRSearch.ROLE_ACCESS = new string[] { 
                ROLE_CODE.MSR_ENTRY, 
                ROLE_CODE.MSR_APPROVAL, 
                ROLE_CODE.MSR_APPROVAL_GM, 
                ROLE_CODE.MSR_ISSUED 
            };

            var MSR = MSRLogic.GetListMSR(MSRSearch);
            if (MSR != null)
            {
                foreach (var item in MSR.ListData)
                {
                    var act = new ActivityModel();
                    act.ActivityIcon = "fa fa-user";
                    act.ActivityType = "MSR_ENTRY";
                    act.DocumentNo = item.DocumentNo;
                    act.Fullname = item.Requestor;
                    act.CreatedDate = item.DateCreated;
                    model.Activities.Add(act);
                }
            }

            //20200827FM D.Nugroho
            var CEASearch = new CEASearchModel();
            CEASearch.IsExcludeDataMigration = true;
            CEASearch.IsRecentActivity = true;
            CEASearch.ROLE_ACCESS = new string[] {
                ROLE_CODE.CEA_ENTRY
            };

            var CEA = CEALogic.GetListCEA(CEASearch);
            if (CEA != null)
            {
                foreach (var item in CEA.ListData)
                {
                    var act = new ActivityModel();
                    act.ActivityIcon = "fa fa-user";
                    act.ActivityType = "CEA_ENTRY";
                    act.DocumentNo = item.DocumentNo;
                    act.Fullname = item.Requestor;
                    act.CreatedDate = item.DateCreated;
                    model.Activities.Add(act);
                }
            }

            //ViewBag.Chart = ChartLogic.GetChartHome(company.ID);
            ViewBag.Chart = new List<ChartBaseModel>();
            ViewData.Model = model;
            return View();
        }


    }
}
