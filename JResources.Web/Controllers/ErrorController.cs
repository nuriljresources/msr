﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JResources.Data;
using JResources.Data.Model;

namespace JResources.Web.Controllers
{
    public partial class ErrorController : Controller
    {
        public virtual ActionResult AccessDenied()
        {
            return View();
        }

        public virtual ActionResult NotFound()
        {
            return View();
        }

        public virtual ActionResult ErrorProcess()
        {
            //ViewData.Model = Logger.GetLastError();
            return View();
        }

        public virtual JsonResult UserTimeOut()
        {
            ResponseModel response = new ResponseModel();
            response.SetError("User login timeout, login again");

            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}