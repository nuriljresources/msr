﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JResources.BusinessLogic;
using JResources.BusinessLogic.Accpac;
using JResources.Common;
using JResources.Common.Enum;
using JResources.Data;
using JResources.Data.Model;
using JResources.Data.Model.Accpac;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.IO;

namespace JResources.Web.Controllers
{
    [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.PROCUREMENT })]
    public partial class CBEController : Controller
    {
        //
        // GET: /CBE/

        public virtual ActionResult Index()
        {
            var Model = new CBESearchModel();
            List<CompanyDepartmentModel> CompanyDepartments = new List<CompanyDepartmentModel>() { new CompanyDepartmentModel { ID = Guid.Empty, CompanyName = "SELECT All COMPANY" } };
            CompanyDepartments.AddRange(CompanyLogic.GetFullCompanyWithDepartmentByCurrentRole(new string[] { ROLE_CODE.MSR_ENTRY, ROLE_CODE.MSR_ISSUED, ROLE_CODE.MSR_APPROVAL }));

            ViewData.Model = Model;
            ViewBag.CompanyDepartments = CompanyDepartments;
            return View();
        }

        [HttpPost]
        public virtual JsonResult GetListMSRCBE(CBESearchModel model)
        {
            model.ROLE_ACCESS = new string[] { ROLE_CODE.MSR_ISSUED, ROLE_CODE.PROCUREMENT };
            model = CBELogic.GetListCBE(model);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.PROCUREMENT })]

        public virtual ActionResult Detail(Guid Id)
        {
            var model = new MSRIssuedModel();
            PTPRHModel RQNModel = new PTPRHModel();

            if (Id != Guid.Empty)
            {
                model = WarehouseLogic.GetDataByPurchaseId(Id);

                if (!string.IsNullOrEmpty(model.RQNNumber))
                {
                    RQNModel = RQNLogic.GetRQN(model.RQNNumber);
                }
            }
            else
            {
            }

            model.IssuedType = RequestStatus.CBE;

            List<SelectListItem> listWorkFlowList = new List<SelectListItem>();
            var PTWFs = PTWF.GetAll().ToList();
            foreach (var item in PTWFs)
            {
                listWorkFlowList.Add(new SelectListItem() { Text = string.Format("{0}-{1}", item.WORKFLOW.Trim(), item.DESC.Trim()), Value = item.WORKFLOW });
            }

            List<SelectListItem> listCostCenter = new List<SelectListItem>();
            var PTCOSTs = PTCOST.GetAll().ToList();
            foreach (var item in PTCOSTs)
            {
                listCostCenter.Add(new SelectListItem() { Text = item.DESC, Value = item.COSTCTR });
            }


            MSRSearchModel MSRSearch = new MSRSearchModel();
            MSRSearch.IssuedType = RequestStatus.CBE;
            ViewBag.MSRSearchModel = MSRSearch;

            ViewBag.WorkFlowCodeList = listWorkFlowList;
            ViewBag.CostCenterList = listCostCenter;
            ViewBag.RQNModel = RQNModel;
            ViewData.Model = model;
            return View();
        }

        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.PROCUREMENT })]
        public virtual ActionResult DetailCBE(Guid Id)
        {
            //var model = new MSRIssuedModel();
            var model = new CBEModel();
            PTPRHModel RQNModel = new PTPRHModel();


            if (Id != Guid.Empty)
            {
                model = CBELogic.GetDataByCBEId(Id);
                
                if (!string.IsNullOrEmpty(model.RQNNumber))
                {
                    RQNModel = RQNLogic.GetRQN(model.RQNNumber);
                }
            }

            //model.IssuedType = RequestStatus.CBE;

            List<SelectListItem> listWorkFlowList = new List<SelectListItem>();
            var PTWFs = PTWF.GetAll().ToList();
            foreach (var item in PTWFs)
            {
                listWorkFlowList.Add(new SelectListItem() { Text = string.Format("{0}-{1}", item.WORKFLOW.Trim(), item.DESC.Trim()), Value = item.WORKFLOW });
            }

            List<SelectListItem> listCostCenter = new List<SelectListItem>();
            var PTCOSTs = PTCOST.GetAll().ToList();
            foreach (var item in PTCOSTs)
            {
                listCostCenter.Add(new SelectListItem() { Text = item.DESC, Value = item.COSTCTR });
            }


            MSRSearchModel MSRSearch = new MSRSearchModel();
            MSRSearch.IssuedType = RequestStatus.CBE;
            ViewBag.MSRSearchModel = MSRSearch;

            ViewBag.WorkFlowCodeList = listWorkFlowList;
            ViewBag.CostCenterList = listCostCenter;
            ViewBag.RQNModel = RQNModel;
            ViewData.Model = model;
            return View();
        }

        [HttpGet]
        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.MSR_ISSUED, ROLE_CODE.MSR_CANCEL, ROLE_CODE.PROCUREMENT })]
        public virtual JsonResult GetDataRate(string homecurr)
        {
            decimal Rate = CBELogic.GetDataRate(homecurr, "USD", "BAK");

            ResponseModel response = new ResponseModel();
            response.Message = Rate.ToString();
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.MSR_ISSUED, ROLE_CODE.MSR_CANCEL, ROLE_CODE.PROCUREMENT })]
        public virtual JsonResult GetDataWinner(Guid Id)
        {
            CBEModel model = new CBEModel();
            model = CBELogic.GetDataWinner(Id);

            return Json(model.CBESetWinners, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [JResourcesAuthorization(IS_AJAX = true, ROLE_CODES = new string[] { ROLE_CODE.MSR_ISSUED, ROLE_CODE.MSR_CANCEL, ROLE_CODE.PROCUREMENT })]
        public virtual JsonResult CreateCBE(CBEModel model)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                if (model.ID == Guid.Empty)
                {
                    response = CBELogic.CreateCBE(model);
                }
                else
                {
                    response = CBELogic.UpdateCBE(model);
                }
            }
            catch (Exception ex)
            {
                Logger.Set(LoggerCategory.MSR_ISSUED, "CBE/CreateCBE", model, ex);
                response.SetError("Error Exception handling {0}", ex.Message);
            }

            if (response.IsSuccess)
            {
                foreach (var item in response.MessageList)
                {
                    this.AddNotification(item, NotificationType.SUCCESS);
                }
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [JResourcesAuthorization(IS_AJAX = true, ROLE_CODES = new string[] { ROLE_CODE.MSR_ISSUED, ROLE_CODE.MSR_CANCEL, ROLE_CODE.PROCUREMENT })]
        public virtual JsonResult UpdateWinnerCBE(CBEModel model)
        {
            ResponseModel response = new ResponseModel();

            try
            {
                if (model.CBESetWinners[0].ID != Guid.Empty)
                {
                    response = CBELogic.UpdateWinnerCBE(model);
                }
            }
            catch (Exception ex)
            {
                Logger.Set(LoggerCategory.MSR_ISSUED, "CBE/SetWinnerCBE", model, ex);
                response.SetError("Error Exception handling {0}", ex.Message);
            }

            if (response.IsSuccess)
            {
                foreach (var item in response.MessageList)
                {
                    this.AddNotification(item, NotificationType.SUCCESS);
                }
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [JResourcesAuthorization(IS_AJAX = true, ROLE_CODES = new string[] { ROLE_CODE.MSR_ISSUED, ROLE_CODE.MSR_CANCEL, ROLE_CODE.PROCUREMENT })]
        public virtual JsonResult SubmitCBE(CBEModel model)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                //plus event save as draft
                response = CBELogic.SubmitCBE(model);
            }
            catch (Exception ex)
            {
                Logger.Set(LoggerCategory.MSR_ISSUED, "CBE/SubmitCBE", model, ex);
                response.SetError("Error Exception handling {0}", ex.Message);
            }

            if (response.IsSuccess)
            {
                GenereateRFAReportExcel(model.ID, "Submitted");

                foreach (var item in response.MessageList)
                {
                    this.AddNotification(item, NotificationType.SUCCESS);
                }
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [JResourcesAuthorization(IS_AJAX = true, ROLE_CODES = new string[] { ROLE_CODE.MSR_ISSUED, ROLE_CODE.MSR_CANCEL, ROLE_CODE.PROCUREMENT })]
        public virtual JsonResult DiscardCBE(CBEModel model)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                response = CBELogic.DiscardCBE(model);
            }
            catch (Exception ex)
            {
                Logger.Set(LoggerCategory.MSR_ISSUED, "CBE/DiscardCBE", model, ex);
                response.SetError("Error Exception handling {0}", ex.Message);
            }

            if (response.IsSuccess)
            {
                foreach (var item in response.MessageList)
                {
                    this.AddNotification(item, NotificationType.SUCCESS);
                }
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [JResourcesAuthorization(IS_AJAX = true, ROLE_CODES = new string[] { ROLE_CODE.MSR_ISSUED, ROLE_CODE.MSR_CANCEL, ROLE_CODE.PROCUREMENT })]
        public virtual JsonResult PostRequisitionCBE(CBEModel model)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                response = CBELogic.PostRQNCBE(model);
            }
            catch (Exception ex)
            {
                Logger.Set(LoggerCategory.MSR_ISSUED, "CBE/PostRequisitionCBE", model, ex);
                response.SetError("Error Exception handling {0}", ex.Message);
            }

            if (response.IsSuccess)
            {
                foreach (var item in response.MessageList)
                {
                    this.AddNotification(item, NotificationType.SUCCESS);
                }
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public virtual JsonResult AddSupplier(MSRSupplier model)
        {
            var result = CBELogic.AddSupplierCBE(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult GenereateRFAReport(Guid CBEId)
        {
            var report = GenereateRFAReportExcel(CBEId, "Generate");

            return File(report.GetAsByteArray(),   //The binary data of the XLS file
                 "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",//MIME type of Excel files
                 "CBE_RFA");
        }
        public ExcelPackage GenereateRFAReportExcel(Guid CBEId, string GenerateType)
        {
            try
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                ExcelPackage Ep = new ExcelPackage();
                ExcelWorksheet Sheet;

                ExcelPackage Ep_Error = new ExcelPackage();
                ExcelWorksheet Sheet_Error;

                string filename = string.Empty;

                Sheet = Ep.Workbook.Worksheets.Add("RFA");

                int column = 1;
                int row = 4;

                var cbeH = CBE.GetById(CBEId);
                var cbebidsupplierwinnerH = CBEBidSupplierWinner.GetByIsWinner(CBEId);
                var msrH = MSR.GetById(cbeH.MSRId);
                var departmentH = Department.GetById(cbeH.DepartmentId);
                var companyH = Company.GetById(msrH.CompanyID);

                #region Report Header

                Color HeaderColorBG = System.Drawing.ColorTranslator.FromHtml("#f5b041");
                Color SubHeaderColorBG = System.Drawing.ColorTranslator.FromHtml("#f5b041");
                Color SupplierHeaderColorBG = System.Drawing.ColorTranslator.FromHtml("#f5b041");
                Color SupplierSubHeaderColorBG = System.Drawing.ColorTranslator.FromHtml("#f5b041");
                Color SupplierSubSubHeaderColorBG = System.Drawing.ColorTranslator.FromHtml("#f5b041");
                Color SpecialColumnColorBG = System.Drawing.ColorTranslator.FromHtml("#f9e79f");
                Color HeaderColorFont = System.Drawing.ColorTranslator.FromHtml("#17202a");

                row++;
                var columnHeader = column;

                string PathJRNLogo = System.Configuration.ConfigurationManager.AppSettings["PathJRNLogo"].ToString();
                Image logo = Image.FromFile(PathJRNLogo);

                var picture = Sheet.Drawings.AddPicture("0", logo);
                picture.From.Column = 0;
                picture.From.Row = 0;

                Sheet.Cells[row - 2, column].Value = "COMPANY NAME : " + CurrentUser.COMPANY_NAME;
                Sheet.Cells[row - 2, column, row - 2, column + 4].Merge = true;
                Sheet.Cells[row - 2, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                Sheet.Cells[row - 2, column].Style.Font.Bold = true;

                Sheet.Cells[row - 1, column].Value = "Department : " + departmentH.DepartmentName;
                Sheet.Cells[row - 1, column, row - 1, column + 4].Merge = true;
                Sheet.Cells[row - 1, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                Sheet.Cells[row - 1, column].Style.Font.Bold = true;

                Sheet.Cells[row, column].Value = "No.";
                Sheet.Cells[row, column, row + 2, column].Merge = true;
                Sheet.Cells[row, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(HeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;
                column++;

                Sheet.Cells[row, column].Value = "Item Description";
                Sheet.Cells[row, column, row, column + 2].Merge = true;
                Sheet.Cells[row, column, row + 2, column + 2].Merge = true;
                Sheet.Cells[row, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(HeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;
                column = column + 2;
                column++;

                Sheet.Cells[row, column].Value = "Qty. Request";
                Sheet.Cells[row, column].Style.WrapText = true;
                Sheet.Cells[row, column, row + 2, column].Merge = true;
                Sheet.Cells[row, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(HeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;
                column++;

                #endregion

                #region Detail Item

                var cbe = from a in CurrentDataContext.CurrentContext.CBEBids
                          join b in CurrentDataContext.CurrentContext.CBEBidSuppliers on a.ID equals b.CBEBidId
                          join c in CurrentDataContext.CurrentContext.vw_CBE_Vendor on b.SupplierCode equals c.VENDORID into LeftJoin1
                          from c in LeftJoin1.DefaultIfEmpty()
                          join d in CurrentDataContext.CurrentContext.CBE_MsSupplier on b.SupplierCode equals d.SupplierCode into LeftJoin2
                          from d in LeftJoin2.DefaultIfEmpty()
                          where a.CBEid == CBEId && c.compid == CurrentUser.COMPANY //&& d.IsAccpacSync == false
                          select new
                          {
                              a.Itemno,
                              a.Description,
                              a.QtyRequest,
                              b.SupplierCode,
                              SupplierName = c.VENDORID != null ? c.VENDNAME : d.SupplierName,
                              b.Qty,
                              a.UoM,
                              b.Currency,
                              b.UnitPrice,
                              b.TotalPrice_USD
                          };

                var cbeGroupbyItem = cbe.GroupBy(d => d.Itemno)
                    .Select(
                    g => new
                    {
                        key = g.Key,
                    });

                var cbeGroupbySupplier = cbe.GroupBy(d => new {
                    d.Itemno,
                    d.SupplierCode,
                    d.SupplierName
                })
                    .Select(
                    g => new
                    {
                        key1 = g.Key.Itemno,
                        key2 = g.Key.SupplierCode,
                        key3 = g.Key.SupplierName,
                        TotalPrice = g.Sum(s => s.TotalPrice_USD)
                    });

                var columnDetailSupllier = column;

                foreach (var supplier in cbeGroupbySupplier.OrderBy(x => x.TotalPrice))
                {
                    Sheet.Cells[row, column].Value = supplier.key3.Trim();
                    Sheet.Cells[row, column, row, column + 5].Merge = true;
                    Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    Sheet.Cells[row, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierHeaderColorBG);
                    Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                    Sheet.Cells[row, column].Style.Font.Bold = true;

                    Sheet.Cells[row + 1, column].Value = "Quantity";
                    Sheet.Cells[row + 1, column, row + 1, column + 1].Merge = true;
                    Sheet.Cells[row + 1, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    Sheet.Cells[row + 1, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    Sheet.Cells[row + 1, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    Sheet.Cells[row + 1, column].Style.Fill.BackgroundColor.SetColor(SupplierSubHeaderColorBG);
                    Sheet.Cells[row + 1, column].Style.Font.Color.SetColor(HeaderColorFont);
                    Sheet.Cells[row + 1, column].Style.Font.Bold = true;

                    Sheet.Cells[row + 2, column].Value = "Offering";
                    Sheet.Cells[row + 2, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    Sheet.Cells[row + 2, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    Sheet.Cells[row + 2, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    Sheet.Cells[row + 2, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                    Sheet.Cells[row + 2, column].Style.Font.Color.SetColor(HeaderColorFont);
                    Sheet.Cells[row + 2, column].Style.Font.Bold = true;
                    column++;

                    Sheet.Cells[row + 2, column].Value = "UoM";
                    Sheet.Cells[row + 2, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    Sheet.Cells[row + 2, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    Sheet.Cells[row + 2, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    Sheet.Cells[row + 2, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                    Sheet.Cells[row + 2, column].Style.Font.Color.SetColor(HeaderColorFont);
                    Sheet.Cells[row + 2, column].Style.Font.Bold = true;
                    column++;

                    Sheet.Cells[row + 1, column].Value = "Currency";
                    Sheet.Cells[row + 1, column, row + 2, column].Merge = true;
                    Sheet.Cells[row + 1, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    Sheet.Cells[row + 1, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    Sheet.Cells[row + 1, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    Sheet.Cells[row + 1, column].Style.Fill.BackgroundColor.SetColor(SupplierSubHeaderColorBG);
                    Sheet.Cells[row + 1, column].Style.Font.Color.SetColor(HeaderColorFont);
                    Sheet.Cells[row + 1, column].Style.Font.Bold = true;
                    column++;

                    Sheet.Cells[row + 1, column].Value = "Unit Price";
                    Sheet.Cells[row + 1, column, row + 2, column].Merge = true;
                    Sheet.Cells[row + 1, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    Sheet.Cells[row + 1, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    Sheet.Cells[row + 1, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    Sheet.Cells[row + 1, column].Style.Fill.BackgroundColor.SetColor(SupplierSubHeaderColorBG);
                    Sheet.Cells[row + 1, column].Style.Font.Color.SetColor(HeaderColorFont);
                    Sheet.Cells[row + 1, column].Style.Font.Bold = true;
                    column++;

                    Sheet.Cells[row + 1, column].Value = "Total Price";
                    Sheet.Cells[row + 1, column, row + 1, column + 1].Merge = true;
                    Sheet.Cells[row + 1, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    Sheet.Cells[row + 1, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    Sheet.Cells[row + 1, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    Sheet.Cells[row + 1, column].Style.Fill.BackgroundColor.SetColor(SupplierSubHeaderColorBG);
                    Sheet.Cells[row + 1, column].Style.Font.Color.SetColor(HeaderColorFont);
                    Sheet.Cells[row + 1, column].Style.Font.Bold = true;

                    Sheet.Cells[row + 2, column].Value = "Value (USD)";
                    Sheet.Cells[row + 2, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    Sheet.Cells[row + 2, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    Sheet.Cells[row + 2, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    Sheet.Cells[row + 2, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                    Sheet.Cells[row + 2, column].Style.Font.Color.SetColor(HeaderColorFont);
                    Sheet.Cells[row + 2, column].Style.Font.Bold = true;
                    column++;

                    Sheet.Cells[row + 2, column].Value = "%";
                    Sheet.Cells[row + 2, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    Sheet.Cells[row + 2, column].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    Sheet.Cells[row + 2, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    Sheet.Cells[row + 2, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                    Sheet.Cells[row + 2, column].Style.Font.Color.SetColor(HeaderColorFont);
                    Sheet.Cells[row + 2, column].Style.Font.Bold = true;
                    column++;
                }

                var endofcolumn = column - 1;
                Sheet.Cells[row - 3, 1].Value = "COMMERCIAL BID EVALUATION";
                Sheet.Cells[row - 3, 1, row - 3, endofcolumn].Merge = true;
                Sheet.Cells[row - 3, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                Sheet.Cells[row - 3, 1].Style.Font.Bold = true;

                Sheet.Cells[row - 2, column - 3].Value = "MSR Number : " + msrH.MSRNo;
                Sheet.Cells[row - 2, column - 3, row - 2, endofcolumn].Merge = true;
                Sheet.Cells[row - 2, column - 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                Sheet.Cells[row - 2, column - 3].Style.Font.Bold = true;

                Sheet.Cells[row - 1, column - 3].Value = "Doc. No. : " + cbeH.CBENo;
                Sheet.Cells[row - 1, column - 3, row - 1, endofcolumn].Merge = true;
                Sheet.Cells[row - 1, column - 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                Sheet.Cells[row - 1, column - 3].Style.Font.Bold = true;

                row = row + 3;

                var rowItem = 1;
                foreach (var item in cbeGroupbyItem)
                {
                    column = columnHeader;

                    var itemDetail = cbe.FirstOrDefault(x => x.Itemno == item.key);

                    Sheet.Cells[row, column].Value = rowItem;
                    column++;

                    Sheet.Cells[row, column].Value = itemDetail.Description;
                    Sheet.Cells[row, column, row, column + 2].Merge = true;
                    column = column + 3;

                    Sheet.Cells[row, column].Value = itemDetail.QtyRequest;
                    column++;

                    var lowestPrice = cbeGroupbySupplier.Where(x => x.key1 == item.key).Min(y => y.TotalPrice);

                    foreach (var supplier in cbeGroupbySupplier.OrderBy(x => x.TotalPrice))
                    {
                        var supplierDetail = cbe.FirstOrDefault(x => x.Itemno == itemDetail.Itemno && x.SupplierCode == supplier.key2);
                        if (supplierDetail != null)
                        {
                            Sheet.Cells[row, column].Value = supplierDetail.Qty;
                            column++;
                            Sheet.Cells[row, column].Value = supplierDetail.UoM;
                            column++;
                            Sheet.Cells[row, column].Value = supplierDetail.Currency;
                            column++;
                            Sheet.Cells[row, column].Value = supplierDetail.UnitPrice;
                            column++;
                            Sheet.Cells[row, column].Value = supplierDetail.TotalPrice_USD;
                            column++;
                            Sheet.Cells[row, column].Value = lowestPrice == 0 ? 0 : supplierDetail.TotalPrice_USD / lowestPrice * 100;
                            column++;
                        }
                        else
                        {
                            Sheet.Cells[row, column].Value = "-";
                            column++;
                            Sheet.Cells[row, column].Value = "-";
                            column++;
                            Sheet.Cells[row, column].Value = "-";
                            column++;
                            Sheet.Cells[row, column].Value = "-";
                            column++;
                            Sheet.Cells[row, column].Value = "-";
                            column++;
                            Sheet.Cells[row, column].Value = "-";
                            column++;
                        }
                    }

                    row++;
                    rowItem++;

                }

                #endregion

                #region Summary

                row = row + 2;

                var cbesummary = from a in CurrentDataContext.CurrentContext.CBESummaries
                                 join b in CurrentDataContext.CurrentContext.vw_CBE_Vendor on a.SupplierCode equals b.VENDORID into LeftJoin1
                                 from b in LeftJoin1.DefaultIfEmpty()
                                 join c in CurrentDataContext.CurrentContext.CBE_MsSupplier on a.SupplierCode equals c.SupplierCode into LeftJoin2
                                 from c in LeftJoin2.DefaultIfEmpty()
                                 where a.CBEId == CBEId && b.compid == CurrentUser.COMPANY
                                 select new
                                 {
                                     a.SupplierCode,
                                     SupplierName = b.VENDORID != null ? b.VENDNAME : c.SupplierName,
                                     a.TotalBeforeDiscount,
                                     Discount = a.Discount != null ? a.Discount : 0,
                                     a.PPN,
                                     a.NotForPPH,
                                     a.GT_Requirement,
                                     a.QuotationReceivedDate,
                                     a.TermsOfPayment,
                                     a.DeliveryTime,
                                     a.Incoterm,
                                     a.PriceValidity,
                                     a.ContactPerson
                                 };

                column = 1;

                Sheet.Cells[row, column].Value = "Total Before Discount :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;

                int Count = 0;
                int StartColumn = 6;
                int EndColumn = 5;

                Count = 1;
                foreach (var supplier in cbeGroupbySupplier.OrderBy(x => x.TotalPrice))
                {
                    var TotalBeforeDiscount = cbesummary.FirstOrDefault(x => x.SupplierCode == supplier.key2).TotalBeforeDiscount;
                    Sheet.Cells[row, StartColumn * Count].Value = TotalBeforeDiscount;
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "Discount :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;
                Count = 1;
                foreach (var supplier in cbeGroupbySupplier.OrderBy(x => x.TotalPrice))
                {
                    var TotalBeforeDiscount = cbesummary.FirstOrDefault(x => x.SupplierCode == supplier.key2).TotalBeforeDiscount;
                    var Discount = Convert.ToDecimal(cbesummary.FirstOrDefault(x => x.SupplierCode == supplier.key2).Discount);
                    Discount = Discount == 0 ? 0 : Convert.ToDecimal(TotalBeforeDiscount / Discount);
                    Sheet.Cells[row, StartColumn * Count].Value = Discount;
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "Total After Discount :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;
                Count = 1;
                foreach (var supplier in cbeGroupbySupplier.OrderBy(x => x.TotalPrice))
                {
                    var TotalBeforeDiscount = cbesummary.FirstOrDefault(x => x.SupplierCode == supplier.key2).TotalBeforeDiscount;
                    var Discount = Convert.ToDecimal(cbesummary.FirstOrDefault(x => x.SupplierCode == supplier.key2).Discount);
                    Discount = Discount == 0 ? 0 : Convert.ToDecimal(TotalBeforeDiscount / Discount);
                    Sheet.Cells[row, StartColumn * Count].Value = TotalBeforeDiscount - Discount;
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "Note for PPh / Witholding Tax :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;
                Count = 1;
                foreach (var supplier in cbeGroupbySupplier.OrderBy(x => x.TotalPrice))
                {
                    var NotforPPh = cbesummary.FirstOrDefault(x => x.SupplierCode == supplier.key2).NotForPPH;
                    Sheet.Cells[row, StartColumn * Count].Value = NotforPPh;
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "(VAT) PPN(%) :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;
                Count = 1;
                foreach (var supplier in cbeGroupbySupplier.OrderBy(x => x.TotalPrice))
                {
                    var VAT_PPN = cbesummary.FirstOrDefault(x => x.SupplierCode == supplier.key2).PPN;
                    Sheet.Cells[row, StartColumn * Count].Value = VAT_PPN + "%";
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "(VAT) PPN($) :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;
                Count = 1;
                foreach (var supplier in cbeGroupbySupplier.OrderBy(x => x.TotalPrice))
                {
                    var TotalBeforeDiscount = cbesummary.FirstOrDefault(x => x.SupplierCode == supplier.key2).TotalBeforeDiscount;
                    var Discount = Convert.ToDecimal(cbesummary.FirstOrDefault(x => x.SupplierCode == supplier.key2).Discount);
                    var VAT_PPN = cbesummary.FirstOrDefault(x => x.SupplierCode == supplier.key2).PPN;
                    Discount = Discount == 0 ? 0 : Convert.ToDecimal(TotalBeforeDiscount / Discount);
                    Sheet.Cells[row, StartColumn * Count].Value = (TotalBeforeDiscount - Discount) * VAT_PPN / 100;
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "Grand Total :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;
                Count = 1;
                foreach (var supplier in cbeGroupbySupplier.OrderBy(x => x.TotalPrice))
                {
                    var TotalBeforeDiscount = cbesummary.FirstOrDefault(x => x.SupplierCode == supplier.key2).TotalBeforeDiscount;
                    var Discount = Convert.ToDecimal(cbesummary.FirstOrDefault(x => x.SupplierCode == supplier.key2).Discount);
                    Discount = Discount == 0 ? 0 : Convert.ToDecimal(TotalBeforeDiscount / Discount);
                    var GT = (TotalBeforeDiscount - Discount) + ((TotalBeforeDiscount - Discount) * 10 / 100);
                    Sheet.Cells[row, StartColumn * Count].Value = GT;
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "Percent price comparison :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SpecialColumnColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;
                Count = 1;
                foreach (var supplier in cbeGroupbySupplier.OrderBy(x => x.TotalPrice))
                {
                    var Lowest_GT = Convert.ToDecimal(cbesummary.Min(x => x.GT_Requirement));
                    var GT = Convert.ToDecimal(cbesummary.FirstOrDefault(x => x.SupplierCode == supplier.key2).GT_Requirement);
                    GT = Lowest_GT == 0 ? 0 : GT / Lowest_GT;
                    Sheet.Cells[row, StartColumn * Count].Value = (GT * 100).ToString("0.##") + "%";
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    Sheet.Cells[row, StartColumn * Count].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    Sheet.Cells[row, StartColumn * Count].Style.Fill.BackgroundColor.SetColor(SpecialColumnColorBG);
                    Sheet.Cells[row, StartColumn * Count].Style.Font.Color.SetColor(HeaderColorFont);
                    Sheet.Cells[row, StartColumn * Count].Style.Font.Bold = true;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "Quotation Received Date :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;
                Count = 1;
                foreach (var supplier in cbeGroupbySupplier.OrderBy(x => x.TotalPrice))
                {
                    var Quotation = cbesummary.FirstOrDefault(x => x.SupplierCode == supplier.key2).QuotationReceivedDate;
                    Sheet.Cells[row, StartColumn * Count].Value = Quotation != null ? Quotation.Value.ToString("dd-M-yyyy") : "";
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "Terms of Payment & Delivery Time :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;
                Count = 1;
                foreach (var supplier in cbeGroupbySupplier.OrderBy(x => x.TotalPrice))
                {
                    var Terms = cbesummary.FirstOrDefault(x => x.SupplierCode == supplier.key2).TermsOfPayment;
                    var DeliveryTime = cbesummary.FirstOrDefault(x => x.SupplierCode == supplier.key2).DeliveryTime;
                    Sheet.Cells[row, StartColumn * Count].Value = Terms + " / " + DeliveryTime;
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "incoterm :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;
                Count = 1;
                foreach (var supplier in cbeGroupbySupplier.OrderBy(x => x.TotalPrice))
                {
                    var Incoterm = cbesummary.FirstOrDefault(x => x.SupplierCode == supplier.key2).Incoterm;
                    Sheet.Cells[row, StartColumn * Count].Value = Incoterm;
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "Price Validity (quoatation berlaku sampai kapan) :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;
                Count = 1;
                foreach (var supplier in cbeGroupbySupplier.OrderBy(x => x.TotalPrice))
                {
                    var PriceValidity = cbesummary.FirstOrDefault(x => x.SupplierCode == supplier.key2).PriceValidity;
                    Sheet.Cells[row, StartColumn * Count].Value = PriceValidity != null ? PriceValidity.Value.ToString("dd-M-yyyy") : "";
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "Contact Person :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;
                Count = 1;
                foreach (var supplier in cbeGroupbySupplier.OrderBy(x => x.TotalPrice))
                {
                    var ContactPerson = cbesummary.FirstOrDefault(x => x.SupplierCode == supplier.key2).ContactPerson;
                    Sheet.Cells[row, StartColumn * Count].Value = ContactPerson;
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "Technical Summary (Refer to the Appendix) :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;
                var TehnicalSummary = cbeH.TehnicalSummary;
                Count = 1;
                Sheet.Cells[row, StartColumn * Count].Value = TehnicalSummary;
                Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + endofcolumn].Merge = true;
                row++;

                #endregion

                #region Winner

                var cbewinner = from a in CurrentDataContext.CurrentContext.CBEBidSupplierWinners
                                join b in CurrentDataContext.CurrentContext.vw_CBE_Vendor on a.SupplierCode equals b.VENDORID into LeftJoin1
                                from b in LeftJoin1.DefaultIfEmpty()
                                join c in CurrentDataContext.CurrentContext.CBE_MsSupplier on a.SupplierCode equals c.SupplierCode into LeftJoin2
                                from c in LeftJoin2.DefaultIfEmpty()
                                where a.CBEId == CBEId && b.compid == CurrentUser.COMPANY
                                select new
                                {
                                    a.SupplierCode,
                                    SupplierName = b.VENDORID != null ? b.VENDNAME : c.SupplierName,
                                    a.IsProprietary,
                                    a.IsLowestCost,
                                    a.IsPriceStatedinContract,
                                    a.IsBetterDelivery,
                                    a.IsMeetSchedule,
                                    a.IsPaymentTerm,
                                    a.IsRepeatOrder,
                                    a.OrderNo,
                                    a.IsTechnicalAcceptable,
                                    a.IsBetterAfterSales,
                                    a.IsCommunityDevelopment,
                                    a.IsOthers,
                                    a.OthersRemark,
                                    a.Remark
                                };

                column = 1;

                row++;
                Sheet.Cells[row, column].Value = "Recommendation for Award";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;
                row++;

                Sheet.Cells[row, column].Value = "We Choose with basis of award are below :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SpecialColumnColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;

                Count = 1;

                var cbeGroupbySupplierWinner = from a in cbeGroupbySupplier
                                               join b in cbebidsupplierwinnerH on a.key2 equals b.SupplierCode
                                               where b.CBEId == cbeH.ID
                                               select new
                                               {
                                                   a.key1,
                                                   a.key2,
                                                   a.key3,
                                                   a.TotalPrice
                                               };

                foreach (var supplier in cbeGroupbySupplierWinner.OrderBy(x => x.TotalPrice))
                {
                    Sheet.Cells[row, StartColumn * Count].Value = supplier.key3.Trim();
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    Sheet.Cells[row, StartColumn * Count].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    Sheet.Cells[row, StartColumn * Count].Style.Fill.BackgroundColor.SetColor(SpecialColumnColorBG);
                    Sheet.Cells[row, StartColumn * Count].Style.Font.Color.SetColor(HeaderColorFont);
                    Sheet.Cells[row, StartColumn * Count].Style.Font.Bold = true;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "Proprietary :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;

                Count = 1;
                foreach (var supplier in cbeGroupbySupplierWinner.OrderBy(x => x.TotalPrice))
                {
                    var IsProprietary = cbewinner.FirstOrDefault(x => x.SupplierCode == supplier.key2).IsProprietary;
                    if (IsProprietary == true)
                    {
                        Sheet.Cells[row, StartColumn * Count].Value = "X";
                    }
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.Font.Bold = true;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "Lowest Cost :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;

                Count = 1;
                foreach (var supplier in cbeGroupbySupplierWinner.OrderBy(x => x.TotalPrice))
                {
                    var LowestCost = cbewinner.FirstOrDefault(x => x.SupplierCode == supplier.key2).IsLowestCost;
                    if (LowestCost == true)
                    {
                        Sheet.Cells[row, StartColumn * Count].Value = "X";
                    }
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.Font.Bold = true;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "Price Stated in Contractt :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;

                Count = 1;
                foreach (var supplier in cbeGroupbySupplierWinner.OrderBy(x => x.TotalPrice))
                {
                    var PriceStatedinContract = cbewinner.FirstOrDefault(x => x.SupplierCode == supplier.key2).IsPriceStatedinContract;
                    if (PriceStatedinContract == true)
                    {
                        Sheet.Cells[row, StartColumn * Count].Value = "X";
                    }
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.Font.Bold = true;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "Better Delivery :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;

                Count = 1;
                foreach (var supplier in cbeGroupbySupplierWinner.OrderBy(x => x.TotalPrice))
                {
                    var BetterDelivery = cbewinner.FirstOrDefault(x => x.SupplierCode == supplier.key2).IsBetterDelivery;
                    if (BetterDelivery == true)
                    {
                        Sheet.Cells[row, StartColumn * Count].Value = "X";
                    }
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.Font.Bold = true;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "Meet Schedule :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;

                Count = 1;
                foreach (var supplier in cbeGroupbySupplierWinner.OrderBy(x => x.TotalPrice))
                {
                    var MeetSchedule = cbewinner.FirstOrDefault(x => x.SupplierCode == supplier.key2).IsMeetSchedule;
                    if (MeetSchedule == true)
                    {
                        Sheet.Cells[row, StartColumn * Count].Value = "X";
                    }
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.Font.Bold = true;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "Payment Term :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;

                Count = 1;
                foreach (var supplier in cbeGroupbySupplierWinner.OrderBy(x => x.TotalPrice))
                {
                    var PaymentTerm = cbewinner.FirstOrDefault(x => x.SupplierCode == supplier.key2).IsPaymentTerm;
                    if (PaymentTerm == true)
                    {
                        Sheet.Cells[row, StartColumn * Count].Value = "X";
                    }
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.Font.Bold = true;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "Repeat Order :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;

                Count = 1;
                foreach (var supplier in cbeGroupbySupplierWinner.OrderBy(x => x.TotalPrice))
                {
                    var RepeatOrder = cbewinner.FirstOrDefault(x => x.SupplierCode == supplier.key2).IsRepeatOrder;
                    var OrderNo = cbewinner.FirstOrDefault(x => x.SupplierCode == supplier.key2).OrderNo;
                    if (RepeatOrder == true)
                    {
                        if (OrderNo != null || OrderNo != "")
                        {
                            Sheet.Cells[row, StartColumn * Count].Value = "X" + " | " + OrderNo;
                        }
                        else
                        {
                            Sheet.Cells[row, StartColumn * Count].Value = "X";
                        }
                    }
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.Font.Bold = true; ;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "Technical Acceptable :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;

                Count = 1;
                foreach (var supplier in cbeGroupbySupplierWinner.OrderBy(x => x.TotalPrice))
                {
                    var TechnicalAcceptable = cbewinner.FirstOrDefault(x => x.SupplierCode == supplier.key2).IsTechnicalAcceptable;
                    if (TechnicalAcceptable == true)
                    {
                        Sheet.Cells[row, StartColumn * Count].Value = "X";
                    }
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.Font.Bold = true;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "Better After Sales :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;

                Count = 1;
                foreach (var supplier in cbeGroupbySupplierWinner.OrderBy(x => x.TotalPrice))
                {
                    var BetterAfterSales = cbewinner.FirstOrDefault(x => x.SupplierCode == supplier.key2).IsBetterAfterSales;
                    if (BetterAfterSales == true)
                    {
                        Sheet.Cells[row, StartColumn * Count].Value = "X";
                    }
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.Font.Bold = true;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "Community Development :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;

                Count = 1;
                foreach (var supplier in cbeGroupbySupplierWinner.OrderBy(x => x.TotalPrice))
                {
                    var CommunityDevelopment = cbewinner.FirstOrDefault(x => x.SupplierCode == supplier.key2).IsCommunityDevelopment;
                    if (CommunityDevelopment == true)
                    {
                        Sheet.Cells[row, StartColumn * Count].Value = "X";
                    }
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.Font.Bold = true;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "Others :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;

                Count = 1;
                foreach (var supplier in cbeGroupbySupplierWinner.OrderBy(x => x.TotalPrice))
                {
                    var Others = cbewinner.FirstOrDefault(x => x.SupplierCode == supplier.key2).IsOthers;
                    var OthersRemark = cbewinner.FirstOrDefault(x => x.SupplierCode == supplier.key2).OthersRemark;
                    if (Others == true)
                    {
                        if (OthersRemark != null || OthersRemark != "")
                        {
                            Sheet.Cells[row, StartColumn * Count].Value = "X" + " | " + OthersRemark;
                        }
                        else
                        {
                            Sheet.Cells[row, StartColumn * Count].Value = "X";
                        }
                    }
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.Font.Bold = true;

                    Count++;
                }
                row++;

                Sheet.Cells[row, column].Value = "Remark :";
                Sheet.Cells[row, column, row, column + 4].Merge = true;
                Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                Sheet.Cells[row, column].Style.Font.Bold = true;

                Count = 1;
                foreach (var supplier in cbeGroupbySupplierWinner.OrderBy(x => x.TotalPrice))
                {
                    var Remark = cbewinner.FirstOrDefault(x => x.SupplierCode == supplier.key2).Remark;
                    Sheet.Cells[row, StartColumn * Count].Value = Remark;
                    Sheet.Cells[row, StartColumn * Count, row, (StartColumn * Count) + EndColumn].Merge = true;
                    Sheet.Cells[row, StartColumn * Count].Style.Font.Bold = true;

                    Count++;
                }
                row++;

                #endregion

                #region Approval

                var data = CurrentDataContext.CurrentContext.CBE_GEN_APPROVAL_LIST_View(CurrentUser.NIKSITE, cbeH.ApprovalSchema, companyH.CompanyCode);

                var approver = CurrentDataContext.CurrentContext.CBE_RoleApproval.Where(x => x.CreatedBy == CurrentUser.NIKSITE).ToList();

                row++;

                column = 1;
                if (approver != null)
                {
                    Sheet.Cells[row, column].Value = "Prepared By,";
                    Sheet.Cells[row, column, row, column + 3].Merge = true;
                    Sheet.Cells[row, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    Sheet.Cells[row, column].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    Sheet.Cells[row, column].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                    Sheet.Cells[row, column].Style.Font.Color.SetColor(HeaderColorFont);
                    Sheet.Cells[row, column].Style.Font.Bold = true;

                    Sheet.Cells[row + 1, column, row + 3, column + 3].Merge = true;

                    var ReqName = UserLogic.GetByNIK(cbeH.CreatedBy);
                    Sheet.Cells[row + 4, column].Value = "Name : " + ReqName.FullName;
                    Sheet.Cells[row + 4, column, row + 4, column + 3].Merge = true;
                    Sheet.Cells[row + 4, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    Sheet.Cells[row + 4, column].Style.Font.Bold = true;

                    Sheet.Cells[row + 5, column].Value = "Position : Requestor";
                    Sheet.Cells[row + 5, column, row + 5, column + 3].Merge = true;
                    Sheet.Cells[row + 5, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    Sheet.Cells[row + 5, column].Style.Font.Bold = true;

                    Sheet.Cells[row + 6, column].Value = "Date : " + cbeH.CreatedDate.ToString("dd-MMM-yyyy");
                    Sheet.Cells[row + 6, column, row + 6, column + 3].Merge = true;
                    Sheet.Cells[row + 6, column].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    Sheet.Cells[row + 6, column].Style.Font.Bold = true;

                    Sheet.Cells[row, (column * 4) + 1].Value = "Approved By,";
                    Sheet.Cells[row, (column * 4) + 1, row, (approver.Count() + 1) * 4].Merge = true;
                    Sheet.Cells[row, (column * 4) + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    Sheet.Cells[row, (column * 4) + 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    Sheet.Cells[row, (column * 4) + 1].Style.Fill.BackgroundColor.SetColor(SupplierSubSubHeaderColorBG);
                    Sheet.Cells[row, (column * 4) + 1].Style.Font.Color.SetColor(HeaderColorFont);
                    Sheet.Cells[row, (column * 4) + 1].Style.Font.Bold = true;

                    Count = 1;
                    foreach (var approverDetail in approver.OrderBy(x => x.Urut))
                    {
                        Sheet.Cells[row + 1, (Count * 4) + 1, row + 3, (Count * 4) + 1 + 3].Merge = true;

                        Sheet.Cells[row + 4, (Count * 4) + 1].Value = "Name : " + approverDetail.Nama;
                        Sheet.Cells[row + 4, (Count * 4) + 1, row + 4, (Count * 4) + 1 + 3].Merge = true;
                        Sheet.Cells[row + 4, (Count * 4) + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        Sheet.Cells[row + 4, (Count * 4) + 1].Style.Font.Bold = true;

                        Sheet.Cells[row + 5, (Count * 4) + 1].Value = "Position : " + approverDetail.Position;
                        Sheet.Cells[row + 5, (Count * 4) + 1, row + 5, (Count * 4) + 1 + 3].Merge = true;
                        Sheet.Cells[row + 5, (Count * 4) + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        Sheet.Cells[row + 5, (Count * 4) + 1].Style.Font.Bold = true;

                        Sheet.Cells[row + 6, (Count * 4) + 1].Value = "Date : " + cbeH.CreatedDate.ToString("dd-MM-yyyy");
                        Sheet.Cells[row + 6, (Count * 4) + 1, row + 6, (Count * 4) + 1 + 3].Merge = true;
                        Sheet.Cells[row + 6, (Count * 4) + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        Sheet.Cells[row + 6, (Count * 4) + 1].Style.Font.Bold = true;

                        Count++;
                    }
                }

                #endregion

                Sheet.View.ShowGridLines = false;
                Sheet.Cells["A:AZ"].AutoFitColumns();

                string fileName = cbeH.CBENo + ".xlsx";
                string filePathRFA = System.Configuration.ConfigurationManager.AppSettings["PathSaveRFA"].ToString() + "/";
                filePathRFA = filePathRFA + fileName.Replace("/", "_");

                if (GenerateType == "Submitted")
                {
                    FileInfo fi = new FileInfo(filePathRFA);
                    Ep.SaveAs(fi);
                }

                return Ep;
            }
            catch (Exception e)
            {
                ExcelPackage Ep_Error = new ExcelPackage();

                //return File(Ep_Error.GetAsByteArray(),   //The binary data of the XLS file
                //     "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",//MIME type of Excel files
                //     "");
                return Ep_Error;
            }
            //return isSuccess;
        }
        public virtual ActionResult DownloadAttachment(string FilePath, string FileName)
        {
            FilePath = FilePath + "/" + FileName;
            byte[] fileBytes = System.IO.File.ReadAllBytes(FilePath);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, FileName);
        }
    }
}
