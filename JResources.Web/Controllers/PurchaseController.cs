﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JResources.BusinessLogic;
using JResources.BusinessLogic.Accpac;
using JResources.Common;
using JResources.Common.Enum;
using JResources.Data;
using JResources.Data.Model;
using JResources.Data.Model.Accpac;

namespace JResources.Web.Controllers
{
    [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.PROCUREMENT })]
    public partial class PurchaseController : Controller
    {
        /// <summary>
        /// List Purchase
        /// </summary>
        /// <returns></returns>
        public virtual ActionResult Index()
        {
            var Model = new MSRIssuedSearchModel();
            List<CompanyDepartmentModel> CompanyDepartments = new List<CompanyDepartmentModel>() { new CompanyDepartmentModel { ID = Guid.Empty, CompanyName = "SELECT All COMPANY" } };
            CompanyDepartments.AddRange(CompanyLogic.GetFullCompanyWithDepartmentByCurrentRole(new string[] { ROLE_CODE.MSR_ENTRY, ROLE_CODE.MSR_ISSUED, ROLE_CODE.MSR_APPROVAL }));

            ViewData.Model = Model;
            ViewBag.CompanyDepartments = CompanyDepartments;
            return View();
        }

        /// <summary>
        /// Get JSON Purchase
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonResult GetListMSRPurchase(MSRIssuedSearchModel model)
        {
            model.ROLE_ACCESS = new string[] { ROLE_CODE.MSR_ISSUED, ROLE_CODE.PROCUREMENT };
            model.IssuedType = new string[] { RequestStatus.PURCHASING };
            model = WarehouseLogic.GetListIssued(model);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Page Create Purchase
        /// </summary>
        /// <returns></returns>
        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.PROCUREMENT })]
        public virtual ActionResult DetailPurchase(Guid Id)
        {
            var model = new MSRIssuedModel();
            PTPRHModel RQNModel = new PTPRHModel();

            if (Id != Guid.Empty)
            {
                model = WarehouseLogic.GetDataByPurchaseId(Id);

                if (!string.IsNullOrEmpty(model.RQNNumber))
                {
                    RQNModel = RQNLogic.GetRQN(model.RQNNumber);
                }
            }

            model.IssuedType = RequestStatus.PURCHASING;

            List<SelectListItem> listWorkFlowList = new List<SelectListItem>();
            var PTWFs = PTWF.GetAll().ToList();
            foreach (var item in PTWFs)
            {
                listWorkFlowList.Add(new SelectListItem() { Text = string.Format("{0}-{1}", item.WORKFLOW.Trim(), item.DESC.Trim()), Value = item.WORKFLOW });
            }

            List<SelectListItem> listCostCenter = new List<SelectListItem>();
            var PTCOSTs = PTCOST.GetAll().ToList();
            foreach (var item in PTCOSTs)
            {
                listCostCenter.Add(new SelectListItem() { Text = item.DESC, Value = item.COSTCTR });
            }


            MSRSearchModel MSRSearch = new MSRSearchModel();
            MSRSearch.IssuedType = RequestStatus.PURCHASING;
            ViewBag.MSRSearchModel = MSRSearch;

            ViewBag.WorkFlowCodeList = listWorkFlowList;
            ViewBag.CostCenterList = listCostCenter;
            ViewBag.RQNModel = RQNModel;
            ViewData.Model = model;
            return View();
        }

        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.PROCUREMENT })]
        public virtual ActionResult PurchaseTransfer(Guid Id)
        {
            var response = new ResponseModel();
            var model = WarehouseLogic.GetDataByPurchaseId(Id);

            if (model == null || string.IsNullOrEmpty(model.MSRIssuedNo))
            {
                response.SetError("PURCHASE NOT FOUND");
            }
            else
            {
                if (model.IsTransferAccpac)
                {
                    response.SetError("{0} IS ALREADY PURCHASE", model.MSRIssuedNo);
                }
            }

            if (!response.IsSuccess)
            {
                this.AddNotification(response.Message, NotificationType.ERROR);
                return RedirectToAction(MVC.Purchase.Index());
            }

            model.IssuedType = RequestStatus.PURCHASING;
            ViewData.Model = model;
            return View();
        }

        /// <summary>
        /// Save Document Issued
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.PROCUREMENT })]
        public virtual JsonResult PurchasedRequest(MSRIssuedModel model)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                response = WarehouseLogic.PurchasedRequest(model);
            }
            catch (Exception ex)
            {
                response.SetError("Error Exception handling {0}", ex.Message);
            }

            if (response.IsSuccess)
            {
                foreach (var item in response.MessageList)
                {
                    this.AddNotification(item, NotificationType.SUCCESS);
                }                              
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        
    }
}
