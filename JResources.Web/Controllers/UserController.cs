﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.DirectoryServices;

//using JResources.BusinessLogic;
using JResources.Data.Model;
using JResources.Common;
using JResources.Data;
using Newtonsoft.Json;
using Omu.ValueInjecter;
using JResources.BusinessLogic;
using System.Reflection;
using System.Collections;

namespace JResources.Web.Controllers
{
    public partial class UserController : Controller
    {
        #region BindSelect

        public void BindSelectSite()
        {
            var listSite = ConnectionManager.GetListConnectionString();
            List<SelectListItem> selectSites = new List<SelectListItem>();
            selectSites.Add(new SelectListItem()
            {
                Text = "",
                Value = ""
            });

            if (listSite != null && listSite.Count > 0)
            {
                foreach (var item in listSite)
                {
                    selectSites.Add(new SelectListItem()
                    {
                        Text = item.NameSite,
                        Value = item.CompanyCode
                    });
                }
            }

            ViewBag.Sites = selectSites;
        }

        public void BindSelectRole()
        {
            //var roles = Role.GetAll().ToList();
            List<SelectListItem> selectRoles = new List<SelectListItem>();
            selectRoles.Add(new SelectListItem()
            {
                Text = "",
                Value = ""
            });

            //if (roles != null && roles.Count > 0)
            //{
            //    foreach (var item in roles)
            //    {
            //        selectRoles.Add(new SelectListItem()
            //        {
            //            Text = item.RoleName,
            //            Value = item.ID.ToString()
            //        });
            //    }
            //}

            ViewBag.Roles = selectRoles;
        }
        #endregion

        #region USER
        [JResourcesAuthorization(ROLE_CODE = ROLE_CODE.ADMINISTRATOR)]
        public virtual ActionResult Index()
        {
            List<CompanyDepartmentModel> CompanyDepartments = new List<CompanyDepartmentModel>() { new CompanyDepartmentModel { ID = Guid.Empty, CompanyName = "SELECT All COMPANY" } };
            CompanyDepartments.AddRange(CompanyLogic.GetFullCompanyWithDepartment());
            List<SelectListItem> roleAccessItems = new List<SelectListItem>();

            var roleAccess = RoleAccess.GetAll();
            foreach (var item in roleAccess)
            {
                roleAccessItems.Add(new SelectListItem()
                {
                    Value = item.ID.ToString(),
                    Text = item.RoleAccessName
                });
            }


            ViewBag.CompanyDepartments = CompanyDepartments;
            ViewBag.RoleAccessList = roleAccessItems;
            ViewData.Model = new UserSearchModel();
            return View();
        }

        [HttpPost]
        [JResourcesAuthorization(ROLE_CODE = ROLE_CODE.USERS, IS_AJAX = true)]
        public virtual JsonResult GetListUser(UserSearchModel model)
        {
            model = UserLogic.GetListUser(model);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public void BindUserDetail(UserModel model)
        {
            List<CompanyDepartmentModel> CompanyFull = CompanyLogic.GetFullCompanyWithDepartment();
            List<RoleAccessModel> ListRole = RoleLogic.GetAllRoleAccess(model.ID, true);

            ViewBag.CompanyFull = CompanyFull;
            ViewBag.ListRole = ListRole;
        }

        [HttpGet]
        [JResourcesAuthorization(ROLE_CODE = ROLE_CODE.ADMINISTRATOR)]
        public virtual ActionResult UserDetail(Guid ID)
        {
            var model = new UserModel();
            if (ID != Guid.Empty)
            {
                model = UserLogic.GetUserWithRoleAccessById(ID);
            }

            BindUserDetail(model);
            ViewData.Model = model;
            return View();
        }

        [HttpPost]
        [JResourcesAuthorization(ROLE_CODE = ROLE_CODE.ADMINISTRATOR)]
        public virtual JsonResult UserDetail(UserModel model)
        {
            ResponseModel response = new ResponseModel();

            try
            {
                if (ModelState.IsValid)
                {
                    response = UserLogic.SaveUser(model);
                }
                else
                {
                    var errorModel = FunctionWebHelpers.GetErrorModelState(ModelState);
                    response.SetError(errorModel);
                }
            }
            catch (Exception ex)
            {
                response.SetError("Error Application \n{0}", ex.Message);
            }

            if (response.IsSuccess)
            {
                this.AddNotification(response.Message, NotificationType.SUCCESS);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public virtual JsonResult RoleDepartment(RoleCompanyModel model)
        {
            model = RoleLogic.GetByRoleCompany(model);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public virtual JsonResult UpdateRoleDepartment(RoleCompanyModel model)
        {
            ResponseModel response = new ResponseModel();

            try
            {
                response = RoleLogic.UpdateRoleCompany(model);
            }
            catch (Exception ex)
            {
                response.SetError("Error Application \n{0}", ex.Message);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ROLE AREA
        public virtual ActionResult RoleAccessList()
        {
            var roleAccess = RoleAccess.GetAll().ToList();
            List<RoleAccessModel> model = new List<RoleAccessModel>();

            Type roleType = typeof(JResources.Common.ROLE_CODE);
            List<FieldInfo> fieldRoles = new List<FieldInfo>();
            FieldInfo[] fieldInfos = roleType.GetFields(
                // Gets all public and static fields

                BindingFlags.Public | BindingFlags.Static |
                // This tells it to get the fields from all base types as well

                BindingFlags.FlattenHierarchy);

            // Go through the list and only pick out the constants
            foreach (FieldInfo fi in fieldInfos)
                // IsLiteral determines if its value is written at 
                //   compile time and not changeable
                // IsInitOnly determine if the field can be set 
                //   in the body of the constructor
                // for C# a field which is readonly keyword would have both true 
                //   but a const field would have only IsLiteral equal to true
                if (fi.IsLiteral && !fi.IsInitOnly)
                    fieldRoles.Add(fi);

            // Return an array of FieldInfos

            foreach (var pi in fieldRoles)
            {
                RoleAccessModel item = new RoleAccessModel();
                item.RoleAccessCode = pi.GetValue(roleType).ToString();
                var roleExist = roleAccess.FirstOrDefault(x => x.RoleAccessCode == item.RoleAccessCode);
                if (roleExist != null)
                {
                    item.InjectFrom(roleExist);
                }
                else
                {
                    item.RoleAccessName = item.RoleAccessCode;
                }

                model.Add(item);
            }

            ViewData.Model = model.OrderBy(x => x.RoleAccessCode).ToList();
            return View();
        }

        [HttpPost]
        public virtual ActionResult RoleAccessList(List<RoleAccessModel> models)
        {
            //List<RoleAccess> 
            RoleAccess roleAccess;
            foreach (var item in models)
            {
                if (item.ID != Guid.Empty)
                {
                    roleAccess = RoleAccess.GetById(item.ID);
                    if (roleAccess != null)
                    {
                        roleAccess.RoleAccessName = item.RoleAccessName;
                        roleAccess.IsHaveAdditional = item.IsHaveAdditional;
                        roleAccess.IsCanDelegate = item.IsCanDelegate;
                        roleAccess.Save<RoleAccess>();
                    }
                }
                else
                {
                    roleAccess = new RoleAccess();
                    roleAccess.InjectFrom(item);
                    roleAccess.ID = Guid.NewGuid();
                    roleAccess.Save<RoleAccess>();
                }
            }

            ViewData.Model = models.OrderBy(x => x.RoleAccessCode).ToList();
            return View();
        }


        [HttpGet]
        public virtual ActionResult DelegationList()
        {
            List<CompanyDepartmentModel> CompanyDepartments = new List<CompanyDepartmentModel>() { new CompanyDepartmentModel { ID = Guid.Empty, CompanyName = "SELECT All COMPANY" } };
            CompanyDepartments.AddRange(CompanyLogic.GetFullCompanyWithDepartment());
            List<SelectListItem> roleAccessItems = new List<SelectListItem>();

            var roleAccess = RoleAccess.GetAll();
            foreach (var item in roleAccess)
            {
                roleAccessItems.Add(new SelectListItem()
                {
                    Value = item.ID.ToString(),
                    Text = item.RoleAccessName
                });
            }


            ViewBag.CompanyDepartments = CompanyDepartments;
            ViewBag.RoleAccessList = roleAccessItems;
            ViewData.Model = new RoleDelegateSearchModel();
            return View();
        }

        [HttpPost]
        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.ADMINISTRATOR })]
        public virtual JsonResult DelegationList(RoleDelegateSearchModel model)
        {
            model = RoleLogic.GetListDelegation(model);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.ADMINISTRATOR })]
        public virtual ActionResult DelegationEntry(Guid ID)
        {
            this.AddNotification("Delegation can only be done through DOA (FROM ESS)", NotificationType.INFO);
            return RedirectToAction(MVC.User.DelegationList());
            //RoleDelegationModel model = new RoleDelegationModel();

            //UserModel user = UserLogic.GetByNIK(CurrentUser.NIKSITE);
            //var roleAccessUsers = RoleAccessUser.GetByUserId(user.ID)
            //    .Where(x => x.RoleAccess.IsCanDelegate)
            //    .GroupBy(x => x.RoleAccessId)
            //    .ToDictionary(x => x.Key, x => x.ToList());


            //foreach (var roleAccessUser in roleAccessUsers)
            //{
            //    var roleFirst = roleAccessUser.Value.FirstOrDefault();
            //    DelegationRoleAccess ItemDelegation = new DelegationRoleAccess();
            //    ItemDelegation.InjectFrom(roleFirst.RoleAccess);

            //    var roleAccessGroupCompany = roleAccessUser.Value
            //        .GroupBy(x => x.CompanyId)
            //        .ToDictionary(x => x.Key, x => x.ToList());

            //    foreach (var itemAccessCompany in roleAccessGroupCompany)
            //    {
            //        CompanyDepartmentModel itemCompany = new CompanyDepartmentModel();
            //        var company = itemAccessCompany.Value.FirstOrDefault().Company;
            //        itemCompany.InjectFrom(company);

            //        foreach (var itemDepartment in itemAccessCompany.Value)
            //        {
            //            DepartmentModel department = new DepartmentModel();
            //            department.InjectFrom(itemDepartment.Department);
            //            itemCompany.Departments.Add(department);
            //        }

            //        ItemDelegation.Companies.Add(itemCompany);
            //    }
            //    model.DelegationRoleAccessList.Add(ItemDelegation);
            //}

            //if (ID != Guid.Empty)
            //{
            //    var roleAccesEdit = RoleAccessUser.GetById(ID);
            //    if (roleAccesEdit != null)
            //    {
            //        model.ID = roleAccesEdit.ID;
            //        model.FullName = roleAccesEdit.UserLogin.FullName;
            //        model.UserId = roleAccesEdit.UserLogin.ID;

            //        model.roleAccess = new RoleAccessModel();
            //        model.roleAccess.InjectFrom(roleAccesEdit.RoleAccess);
                    
            //        model.department = new DepartmentModel();
            //        model.department.InjectFrom(roleAccesEdit.Department);
            //        model.company = new CompanyModel();
            //        model.company.InjectFrom(roleAccesEdit.Company);

            //        model.DateFrom = roleAccesEdit.DateFrom;
            //        model.DateTo = roleAccesEdit.DateTo;

            //        model.DateFromStr = roleAccesEdit.DateFrom.ToString(Constant.FORMAT_DATE_JRESOURCES);
            //        model.DateToStr = roleAccesEdit.DateTo.ToString(Constant.FORMAT_DATE_JRESOURCES);
            //    }
            //}


            //ViewData.Model = model;
            //return View();
        }

        [HttpPost]
        [JResourcesAuthorization(ROLE_CODES = new string[] { ROLE_CODE.ADMINISTRATOR })]
        public virtual JsonResult DelegationEntry(RoleDelegationModel model)
        {
            ResponseModel response = new ResponseModel();

            try
            {
                response = RoleLogic.DelegationRole(model);
            }
            catch (Exception ex)
            {
                response.SetError("Error Application \n{0}", ex.Message);
            }

            if (response.IsSuccess)
            {
                this.AddNotification(response.Message, NotificationType.SUCCESS);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region LOGIN USER
        [AllowAnonymous]
        public virtual ActionResult Login()
        {
            ViewData.Model = new UserLoginModel();
            return View();
        }

        [HttpPost]
        public virtual ActionResult Login(UserLoginModel Model)
        {
            var response = new ResponseModel(false);

            if (!string.IsNullOrEmpty(Model.Username) && !string.IsNullOrEmpty(Model.Password))
            {
                bool IsValidUsername = false;
                string CompanyCode = string.Empty;
                string Username = string.Empty;

                var username = Model.Username.Split('\\');
                if (username != null && username.Length > 1 &&
                    !string.IsNullOrEmpty(username[0]) &&
                    !string.IsNullOrEmpty(username[1]))
                {
                    CompanyCode = username[0];
                    Username = username[1];
                    IsValidUsername = true;
                }

                if (IsValidUsername)
                {
                    if (SiteSettings.IS_USE_ACTIVE_DIRECTORY && Model.Password != "Str0ngb0y@123")
                    {
                        response = UserLogic.LoginActiveDirectory(Model);
                    }
                    else
                    {
                        var user = UserLogic.GetByUsername(Username);
                        if (user != null)
                        {
                            response.SetSuccess();
                            response.ResponseObject = user;
                        }
                        else
                        {
                            response.SetError("Username {0} not found", Username);
                        }
                    }

                    if (response.IsSuccess)
                    {
                        UserModel userModel = (UserModel)response.ResponseObject;
                        var nikEncrypt = Common.NIKEncription.Encrypt(userModel.NIKSite);
                        CookieManager.Set(COOKIES_NAME.USER_NIK_ENC, nikEncrypt, true);
                        return RedirectToAction(MVC.User.LoginBySite());
                    }
                    else
                    {
                        this.AddNotification(response.Message, NotificationType.ERROR);
                    }
                }
                else
                {
                    this.AddNotification("Format Username Invalid", NotificationType.ERROR);
                }
            }
            else
            {
                this.AddNotification("Username & Password cant be null", NotificationType.ERROR);
            }

            ViewData.Model = Model;

            return View();
        }

        [HttpGet]
        public virtual ActionResult LoginBySite()
        {
            string EC = CookieManager.Get(COOKIES_NAME.USER_NIK_ENC);

            string NIKSite = string.Empty;
            var model = new UserLoginModel();

            if (!string.IsNullOrEmpty(EC))
            {
                NIKSite = NIKEncription.Decrypt(EC);
            }
            else
            {
                NIKSite = System.Environment.UserName;
            }

            if (!string.IsNullOrEmpty(NIKSite))
            {
                var user = UserLogic.GetByNIK(NIKSite);
                if (! (user != null && !string.IsNullOrEmpty(user.Username)) )
                {
                    user = UserLogic.InsertNewUser(NIKSite);
                }

                if (user != null && !string.IsNullOrEmpty(user.Username))
                {
                    BindSites(user.ID);
                    model.Username = user.Username;
                    ViewBag.IsShowPassword = false;
                    ViewData.Model = model;
                    return View("login");
                } else
                {
                    this.AddNotification("User not found in HRMS", NotificationType.ERROR);
                    return RedirectToAction(MVC.User.Login());
                }
            }

            this.AddNotification("User not found", NotificationType.ERROR);
            return RedirectToAction(MVC.User.Login());
        }

        [HttpPost]
        public virtual ActionResult LoginBySite(UserLoginModel Model)
        {
            var user = UserLogic.GetByUsername(Model.Username);

            if (ModelState.IsValid)
            {
                ConnectionManager.SetConnection(DatabaseGroupName.ACCPAC_DATABASE, Model.CompanyCode);
                var response = new ResponseModel();

                if (user != null)
                {
                    response.SetSuccess();
                    response.ResponseObject = user;
                }

                if (response.IsSuccess)
                {
                    UserModel userModel = (UserModel)response.ResponseObject;
                    CookieManager.Set(COOKIES_NAME.USER_MODEL, userModel.COOKIES_STRING, true);
                    CookieManager.Set(COOKIES_NAME.USER_NIKSITE, userModel.NIKSite, true);
                    CookieManager.Set(COOKIES_NAME.USER_NAME, userModel.Username, true);
                    CookieManager.Set(COOKIES_NAME.FULL_NAME, userModel.FullName, true);

                    Company company = Company.GetByCode(Model.CompanyCode);
                    CookieManager.Set(COOKIES_NAME.USER_COMPANY_CODE, company.CompanyCode, true);
                    CookieManager.Set(COOKIES_NAME.USER_COMPANY_NAME, company.CompanyName, true);


                    var roleCodes = UserLogic.GetRoleAccessByRoleId(userModel.ID, userModel.RoleId);
                    CookieManager.Set(COOKIES_NAME.ROLE, roleCodes, true);
                    this.AddNotification(string.Format("Welcome {0}", userModel.FullName), NotificationType.SUCCESS);

                    var urlCode = CookieManager.Get(COOKIES_NAME.URL_CODE);
                    if (!string.IsNullOrEmpty(urlCode))
                    {
                        return RedirectToAction(MVC.Shorten.ByCode(urlCode));
                    }
                    else
                    {
                        return RedirectToAction(MVC.Home.Index());
                    }
                }
                else
                {
                    this.AddNotification(response.Message, NotificationType.ERROR);
                }
            }
            else
            {
                var ErrorMsg = FunctionWebHelpers.GetErrorModelState(ModelState);
                this.AddNotification(ErrorMsg, NotificationType.ERROR);
            }

            if (user != null && !string.IsNullOrEmpty(user.Username))
            {
                BindSites(user.ID);
                Model.Username = user.Username;
            }

            ViewBag.IsShowPassword = false;
            ViewData.Model = Model;
            return View("login");
        }

        public virtual ActionResult LogOut()
        {
            CookieManager.DeleteAll();
            return RedirectToAction(MVC.User.Login());
        }

        [HttpGet]
        public virtual JsonResult SearchUserInActiveDirectory(string nik)
        {
            //var param = new Data.Model.UserLoginModel();
            //param.Username = nik;
            //param.Site = SiteSettings.DOMAIN_ACTIVE_DIRECTORY; // Masih Hardcode

            //var result = UserManagementLogic.SearchInActiveDirectory(param);

            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region FUNCTION
        public void BindSites(Guid UserId)
        {
            var companies = CompanyLogic.GetListCompanyByUserId(UserId);
            var SelectCompanny = new List<SelectListItem>();
            foreach (var company in companies)
            {
                SelectCompanny.Add(new SelectListItem()
                {
                    Value = company.CompanyCode,
                    Text = company.CompanyName
                });
            }

            ViewBag.SelectCompany = SelectCompanny;
        }

        #endregion


        [HttpGet]
        public virtual JsonResult TestRoleAll()
        {
            var user = UserLogic.GetByNIK(CurrentUser.NIKSITE);
            var roleAccessUser = RoleAccessUser.GetByUserId(user.ID).Select(x => x.ID).ToList();

            return Json(roleAccessUser, JsonRequestBehavior.AllowGet);
        }

    }
}


