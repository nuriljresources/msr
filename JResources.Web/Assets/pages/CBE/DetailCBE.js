﻿var app = angular.module('JResourcesApp', ['ngMessages', 'ui.bootstrap']);
app.controller('JResourcesController', function ($scope, $http) {

    $scope.amount = '25.00';

    var NewId = function () {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    var newRowBidSupplierModel = function () {
        var model = objBidSupplierModel();
        model.Id = NewId();

        return model;
    }

    var RowWinner = function () {
        var model = objWinnerModel();
        return model;
    }

    var resetListItemEmpty = function () {

        var item = $scope.Model.MSRIssuedDetails.length;
        for (var i = 0; i < item; i++) {

            var CBEBidSuppliers = [];

            for (var ix = 0; ix < 3; ix++) {
                CBEBidSuppliers.push(newRowBidSupplierModel());
            }

            $scope.Model.MSRIssuedDetails[i].CBEBidSuppliers = CBEBidSuppliers;

            console.log(CBEBidSuppliers);
        }
    }

    var resetLookupSupplierSearch = function () {

        $scope.FormLookupSupplier = objSupplierModel();
        var supplierModel = objSupplierModel();

        console.log(supplierModel);
        $scope.FormLookupSupplier.ListData = [];
        for (var i = 0; i < 5; i++) {
            $scope.FormLookupSupplier.ListData.push(supplierModel);
        }
    }

    var SearchLookupSupplier = function () {
        var elementBlock = '#supplier-lookup .modal-body';
        BlockElement(elementBlock);

        $scope.FormLookupSupplier.CostCenter = $scope.Model.CostCenter;
        $scope.FormLookupSupplier.DepartmentId = $scope.Model.DepartmentId;
        $scope.FormLookupSupplier.IsReOrderPoint = $scope.Model.IsReOrderPoint;
        $scope.FormLookupSupplier.IsInventory = true;

        $http({
            method: 'POST',
            url: URI_SEARCH_SUPPLIER,
            data: $scope.FormLookupSupplier
        }).then(function successCallback(response) {
            console.log(response.data);
            $scope.FormLookupSupplier = response.data;
            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });
    }

    var SearchLookupCurrency = function () {
        var elementBlock = '#currency-lookup .modal-body';
        BlockElement(elementBlock);

        $scope.FormLookupSupplier.CostCenter = $scope.Model.CostCenter;
        $scope.FormLookupSupplier.DepartmentId = $scope.Model.DepartmentId;
        $scope.FormLookupSupplier.IsReOrderPoint = $scope.Model.IsReOrderPoint;
        $scope.FormLookupSupplier.IsInventory = true;

        $http({
            method: 'POST',
            url: URI_SEARCH_CURRENCY,
            data: $scope.FormLookupCurrency
        }).then(function successCallback(response) {
            console.log(response.data);
            $scope.FormLookupCurrency = response.data;
            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });
    }

    var AddSupplier = function () {
        var elementBlock = '#supplier-lookup .modal-body';

        bootbox.confirm({
            message: "Are you sure to add new supplier",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {

                    BlockElement(elementBlock);

                    $http({
                        method: 'POST',
                        url: URI_ADD_SUPPLIER,
                        data: $scope.FormLookupSupplier
                    }).then(function successCallback(response) {
                        console.log(response);
                        if (response.data.Message == "") {
                            bootbox.alert("Successfully add new supplier");
                        }
                        else {
                            bootbox.alert(response.data.Message);
                        }
                        unBlockElement(elementBlock);
                    }, function errorCallback(response) {
                        console.log(response);
                        bootbox.alert(response.data.Message);
                        unBlockElement(elementBlock);
                    });

                    console.log('This was logged in the callback: ' + result);
                }
            }
        });
    }

    var resetLookupCurrencySearch = function () {

        $scope.FormLookupCurrency = objCurrencyModel();
        var currencyModel = objCurrencyModel();

        console.log(currencyModel);
        $scope.FormLookupCurrency.ListData = [];
        for (var i = 0; i < 5; i++) {
            $scope.FormLookupCurrency.ListData.push(currencyModel);
        }
    }

    $scope.Model = objModel();
    $scope.Modal = objModalMSR();

    $scope.lookupSearchSupplier = function (Parent, Index, idSupplierLookup) {
        $scope.CBEParentIndex = Parent;
        $scope.CBEIndex = Index;
        $scope.TargetMSRDetail = idSupplierLookup;
        resetLookupSupplierSearch();
        $('#supplier-lookup').modal('toggle');
    }

    $scope.btnSearchLookupSupplier = function () {
        $scope.FormLookupSupplier.CurrentPage = 1;
        SearchLookupSupplier();
    }

    $scope.btnAddSupplier = function () {
        AddSupplier();
    }

    $scope.btnInsertSupplierToList = function (item) {
        var supplierModel = objSupplierModel();

        $scope.Model.MSRIssuedDetails[$scope.CBEParentIndex].CBEBidSuppliers[$scope.CBEIndex].Supplier = item.SupplierCode;
        $scope.Model.MSRIssuedDetails[$scope.CBEParentIndex].CBEBidSuppliers[$scope.CBEIndex].SupplierName = item.SupplierName;

        if (item.Currency != null) {
            $scope.Model.MSRIssuedDetails[$scope.CBEParentIndex].CBEBidSuppliers[$scope.CBEIndex].Currency = item.Currency;
        }

        $('#supplier-lookup').modal('toggle');
    }

    $scope.lookupSearchCurrency = function (Parent, Index, idCurrencyLookup) {
        $scope.CBEParentIndex = Parent;
        $scope.CBEIndex = Index;
        $scope.TargetMSRDetail = idCurrencyLookup;
        resetLookupCurrencySearch();
        $('#currency-lookup').modal('toggle');
    }

    $scope.btnSearchLookupCurrency = function () {
        $scope.FormLookupCurrency.CurrentPage = 1;
        SearchLookupCurrency();
    }

    $scope.btnInsertCurrencyToList = function (item) {
        var currencyModel = objCurrencyModel();

        $scope.Model.MSRIssuedDetails[$scope.CBEParentIndex].CBEBidSuppliers[$scope.CBEIndex].Currency = item.CurrencyCode;

        $('#currency-lookup').modal('toggle');
    }

    $scope.FuncTotalPrice = function (parentindex, index, originValue) {

        var Qty = $scope.Model.MSRIssuedDetails[parentindex].CBEBidSuppliers[index].Qty;
        var UnitPrice = $scope.Model.MSRIssuedDetails[parentindex].CBEBidSuppliers[index].UnitPrice;
        var TotalPrice = Qty * UnitPrice;

        var QtyAcceptable = FuncValidationQty(parentindex, index);

        if (!QtyAcceptable) {
            bootbox.alert("Total qty offering cannot be greater than qty outstanding");
            $scope.Model.MSRIssuedDetails[parentindex].CBEBidSuppliers[index].Qty = originValue;
        }
        else {
            $scope.Model.MSRIssuedDetails[parentindex].CBEBidSuppliers[index].TotalPrice = TotalPrice;

            var homecur = $scope.Model.MSRIssuedDetails[parentindex].CBEBidSuppliers[index].Currency;
            var sourcecur = "USD";
            var pycostcode = "BAK";

            FuncPriceTotal_USD(homecur, sourcecur, pycostcode, parentindex, index, TotalPrice);
        }
    };

    $scope.FuncSetWinner = function (parentindex, index, originValue) {



        var QtyRequest = $scope.Model.MSRIssuedDetails[parentindex].QtyPurchaseOutstanding;
        var CountRow = $scope.Model.MSRIssuedDetails[parentindex].CBEBidSuppliers.length;
        var QtyOffer = 0;

        for (var i = 0; i < CountRow; i++) {
            if ($scope.Model.MSRIssuedDetails[parentindex].CBEBidSuppliers[i].SetAsWinner) {
                QtyOffer = QtyOffer + $scope.Model.MSRIssuedDetails[parentindex].CBEBidSuppliers[i].Qty;
            }
        }

        if (QtyOffer > QtyRequest) {
            bootbox.alert("Total qty offering(Set As Winner) cannot be greater than qty outstanding");
            $scope.Model.MSRIssuedDetails[parentindex].CBEBidSuppliers[index].SetAsWinner = false;
        }
    };

    $scope.FuncSupplier = function () {

        $scope.arrayText = [
        ];

        $scope.arrayFilter = [
        ];


        var CountItem = $scope.Model.MSRIssuedDetails.length;
        var CountSupplier = 0

        for (var i = 0; i < CountItem; i++) {

            var CountSupplier = $scope.Model.MSRIssuedDetails[i].CBEBidSuppliers.length;
            var ItemPart = $scope.Model.MSRIssuedDetails[i].ItemNo;
            var Description = $scope.Model.MSRIssuedDetails[i].Description;
            var QtyProcess = $scope.Model.MSRIssuedDetails[i].QtyProcess;

            for (var x = 0; x < CountSupplier; x++) {
                var UoM = $scope.Model.MSRIssuedDetails[i].CBEBidSuppliers[x].UoM;
                var Currency = $scope.Model.MSRIssuedDetails[i].CBEBidSuppliers[x].Currency;
                var QtyOffering = $scope.Model.MSRIssuedDetails[i].CBEBidSuppliers[x].Qty;

                var SupplierId = $scope.Model.MSRIssuedDetails[i].CBEBidSuppliers[x].Supplier;

                var Qty = $scope.Model.MSRIssuedDetails[i].CBEBidSuppliers[x].Qty;

                if (SupplierId != null) {
                    var SupplierName = $.grep($scope.Model.MSRSupplierDetails, function (loc) {
                        return loc.SupplierCode == SupplierId;
                    })[0].SupplierName.trim();

                    $scope.arrayText.push({
                        ItemNo: ItemPart,
                        Description: Description,
                        QtyProcess: QtyProcess,
                        UoM: UoM,
                        Currency: Currency,
                        Qty: QtyOffering,
                        SupplierName: SupplierName,
                        Qty: Qty,
                    });
                }
            }

        }

        var seen = {};
        var uniqueSupplier = $scope.arrayText.filter(function (item) {
            if (seen.hasOwnProperty(item)) {
                return false;
            } else {
                seen[item] = true;
                return true;
            }
        });

        $scope.Model.RecomendationDetails.push(uniqueSupplier);
    };

    for (var index = 0; index < $scope.Model.CBESummaryDetails.length; index++) {
        $scope.Model.CBESummaryDetails[index].TotalAfterDiscount = $scope.Model.CBESummaryDetails[index].TotalBeforeDiscount - ($scope.Model.CBESummaryDetails[index].Discount / 100 * $scope.Model.CBESummaryDetails[index].TotalBeforeDiscount);
        $scope.Model.CBESummaryDetails[index].PPNValue = $scope.Model.CBESummaryDetails[index].TotalAfterDiscount * $scope.Model.CBESummaryDetails[index].PPN / 100;
        $scope.Model.CBESummaryDetails[index].GT_Offering = $scope.Model.CBESummaryDetails[index].TotalAfterDiscount + $scope.Model.CBESummaryDetails[index].PPN;

        var CountSupplier = $scope.Model.CBESummaryDetails.length;

        for (var i = 0; i < CountSupplier; i++) {
            $scope.Model.CBESummaryDetails[i].Comparison1 = $scope.Model.CBESummaryDetails[0].GT_Offering == 0 ? 0 : $scope.Model.CBESummaryDetails[i].GT_Offering / $scope.Model.CBESummaryDetails[0].GT_Offering * 100;
            $scope.Model.CBESummaryDetails[i].Comparison2 = $scope.Model.CBESummaryDetails[0].GT_Requirement == 0 ? 0 : $scope.Model.CBESummaryDetails[i].GT_Requirement / $scope.Model.CBESummaryDetails[0].GT_Requirement * 100;
        }
    }


    $scope.FuncDiscount = function (index) {
        $scope.Model.CBESummaryDetails[index].TotalAfterDiscount = $scope.Model.CBESummaryDetails[index].TotalBeforeDiscount - ($scope.Model.CBESummaryDetails[index].Discount / 100 * $scope.Model.CBESummaryDetails[index].TotalBeforeDiscount);
        $scope.Model.CBESummaryDetails[index].PPNValue = $scope.Model.CBESummaryDetails[index].TotalAfterDiscount * $scope.Model.CBESummaryDetails[index].PPN / 100;
        $scope.Model.CBESummaryDetails[index].GT_Offering = $scope.Model.CBESummaryDetails[index].TotalAfterDiscount + $scope.Model.CBESummaryDetails[index].PPN;

        var CountSupplier = $scope.Model.CBESummaryDetails.length;

        for (var i = 0; i < CountSupplier; i++) {
            $scope.Model.CBESummaryDetails[i].Comparison1 = $scope.Model.CBESummaryDetails[0].GT_Offering == 0 ? 0 : $scope.Model.CBESummaryDetails[i].GT_Offering / $scope.Model.CBESummaryDetails[0].GT_Offering * 100;
            $scope.Model.CBESummaryDetails[i].Comparison2 = $scope.Model.CBESummaryDetails[0].GT_Requirement == 0 ? 0 : $scope.Model.CBESummaryDetails[i].GT_Requirement / $scope.Model.CBESummaryDetails[0].GT_Requirement * 100;
        }
    };

    $scope.ResetMSR = function () {
        $scope.Model = objModel();
    }

    $scope.addEmptyBidSupplierDetail = function (index) {
        $scope.Model.MSRIssuedDetails[index].CBEBidSuppliers.push(newRowBidSupplierModel())
    }

    $scope.removeWorkOrder = function () {
        $scope.Model = objModel();
        resetListItemEmpty();
    }

    $scope.LookupSearchMSR = function () {
        $('#MSR-lookup').modal('toggle');
    }

    $scope.SearchMSR = function () {
        $scope.Modal.CurrentPage = 1;
        SearchMSR();
    }

    $scope.SelectMSR = function (Id) {
        $scope.IsEnableSubmit = true;

        var elementBlock = '#MSR-lookup';
        BlockElement(elementBlock);
        $scope.Modal.ListData = [];

        $http({
            method: 'GET',
            url: URI_GET_MSR_DATA.replace(GUID_EMPTY, Id)
        }).then(function successCallback(response) {

            if (!response.data.isAcceptable) {
                bootbox.alert("Document CBE No. " + response.data.AcceptableStr + " has been created for the selected MSR. CBE form will open for you to proceed the CBE or Discarded");
            }
            else {
                $scope.Model = response.data;
                console.log($scope.Model);
                $scope.Model.IssuedType = IssuedType;

                $scope.Model.StatusCode = "NEW";

                $('#MSR-lookup').modal('toggle');

                resetListItemEmpty();

                if ($scope.Model.IssuedType != cancelLabel && $scope.Model.MSR.WorkOrderNo != "") {
                    var IsValidWO = validStatus.indexOf($scope.Model.MSR.WorkOrderStatus);
                    console.log(IsValidWO);

                    if (IsValidWO >= 0) {
                        $scope.IsEnableSubmit = false;
                    }
                }

                unBlockElement(elementBlock);
            }

        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert("Error Application");
            unBlockElement(elementBlock);
        });
    }

    $scope.LookupSetWinner = function (Id) {

        $http({
            method: 'GET',
            url: URI_GET_WINNER.replace(GUID_EMPTY, Id)
        }).then(function successCallback(data) {

            $scope.Model.CBESetWinners[0].ID = Id;
            $scope.Model.CBESetWinners[0].SupplierName = data.data[0].SupplierName;
            $scope.Model.CBESetWinners[0].IsProprietary = data.data[0].IsProprietary;
            $scope.Model.CBESetWinners[0].IsLowestCost = data.data[0].IsLowestCost;
            $scope.Model.CBESetWinners[0].IsPriceStatedinContract = data.data[0].IsPriceStatedinContract;
            $scope.Model.CBESetWinners[0].IsBetterDelivery = data.data[0].IsBetterDelivery;
            $scope.Model.CBESetWinners[0].IsMeetSchedule = data.data[0].IsMeetSchedule;
            $scope.Model.CBESetWinners[0].IsPaymentTerm = data.data[0].IsPaymentTerm;
            $scope.Model.CBESetWinners[0].IsRepeatOrder = data.data[0].IsRepeatOrder;
            $scope.Model.CBESetWinners[0].OrderNo = data.data[0].OrderNo;
            $scope.Model.CBESetWinners[0].IsTechnicalAcceptable = data.data[0].IsTechnicalAcceptable;
            $scope.Model.CBESetWinners[0].IsBetterAfterSales = data.data[0].IsBetterAfterSales;
            $scope.Model.CBESetWinners[0].IsCommunityDevelopment = data.data[0].IsCommunityDevelopment;
            $scope.Model.CBESetWinners[0].IsOthers = data.data[0].IsOthers;
            $scope.Model.CBESetWinners[0].OthersRemark = data.data[0].OthersRemark;
            $scope.Model.CBESetWinners[0].Remark = data.data[0].Remark;

            $('#SetWinner-lookup').modal('toggle');

        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert("Error Application");
            unBlockElement(elementBlock);
        });

    }

    $scope.pageChangedLookupItem = function () {
        SearchMSR();
    }

    $scope.lookupSearchLocation = function (item) {
        $scope.ItemLocationSelect = item;
        $('#location-lookup').modal('toggle');
    }

    $scope.btnSelectLocation = function (locationId) {

        for (var i = 0; i < $scope.Model.MSRIssuedDetails.length; i++) {
            if ($scope.Model.MSRIssuedDetails[i].LineNo == $scope.ItemLocationSelect.LineNo) {
                var ItemLine = $scope.Model.MSRIssuedDetails[i];

                for (var ii = 0; ii < ItemLine.ItemLocations.length; ii++) {
                    if (ItemLine.ItemLocations[ii].Location == locationId) {
                        ItemLine.LocationFrom = ItemLine.ItemLocations[ii].Location;
                        ItemLine.StochOnHand = ItemLine.ItemLocations[ii].QtyOnHand;
                    }
                }

            }
        }

        $('#location-lookup').modal('toggle');
    }

    $scope.resetItemMSRDetail = function (lineNo) {
        alert('remove');
    }

    $scope.CreateIssued = function () {
        var elementBlock = '#MSR-issued-form';

        bootbox.confirm({
            message: "By Save As Draft, you've confirm that CBE will to save or update",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {

                    var MessageValidation = FuncSaveValidation($scope.Model);

                    if (MessageValidation == "") {
                        $http({
                            method: 'POST',
                            url: URI_CREATE_MSR_ISSUED,
                            data: $scope.Model
                        }).then(function successCallback(response) {

                            if (response.data.IsSuccess) {

                                if ($scope.Model.CBEDocumentDetails.length > 0) {
                                    UpdateFileUpload(response.data.ResponseObject);
                                }

                                window.location = URI_MSR_LIST;
                            } else {
                                showAlert(response.data.Message);
                            }

                            unBlockElement(elementBlock);
                        }, function errorCallback(response) {

                            console.log(response);
                            if (response != null && response.data != null && response.data.Message != null) {
                                showAlert(response.data.Message);
                            } else {
                                bootbox.alert("Error In Application Process");
                            }

                            unBlockElement(elementBlock);
                        });
                    }
                    else {
                        bootbox.alert(MessageValidation);
                    }

                    console.log('This was logged in the callback: ' + result);
                }
            }
        });
    }

    $scope.GenerateRFAReport = function () {
        var elementBlock = '#MSR-issued-form';
        var uri_excel_summary = URI_RFA_REPORT.replace(GUID_EMPTY, $scope.Model.ID);
        window.open(uri_excel_summary, '_blank');
    }

    $scope.SetAsWinner = function () {
        var elementBlock = '#MSR-issued-form';

        bootbox.confirm({
            message: "By confirming, you've confirm that Supplier will set to winner",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    var MessageValidation = FuncSaveValidation($scope.Model);

                    if (MessageValidation == "") {
                        $http({
                            method: 'POST',
                            url: URI_SET_AS_WINNER,
                            data: $scope.Model
                        }).then(function successCallback(response) {

                            if (response.data.IsSuccess) {

                                if ($scope.Model.CBEDocumentDetails.length > 0) {
                                    UpdateFileUpload(response.data.ResponseObject);
                                }

                                window.location = URI_MSR_LIST;
                            } else {
                                showAlert(response.data.Message);
                            }

                            unBlockElement(elementBlock);
                        }, function errorCallback(response) {

                            console.log(response);
                            if (response != null && response.data != null && response.data.Message != null) {
                                showAlert(response.data.Message);
                            } else {
                                bootbox.alert("Error In Application Process");
                            }

                            unBlockElement(elementBlock);
                        });
                    }
                    else {
                        bootbox.alert(MessageValidation);
                    }

                    console.log('This was logged in the callback: ' + result);
                }
            }
        });
    }

    $scope.SubmitIssued = function () {
        var elementBlock = '#MSR-issued-form';

        bootbox.confirm({
            message: "By confirming, you've confirm that number of CBE will Submit For Approval",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    BlockElement(elementBlock);

                    $http({
                        method: 'POST',
                        url: URI_SUBMIT_MSR_ISSUED,
                        data: $scope.Model
                    }).then(function successCallback(response) {

                        if (response.data.IsSuccess) {

                            if ($scope.Model.CBEDocumentDetails.length > 0) {
                                UpdateFileUpload(response.data.ResponseObject);
                            }

                            window.location = URI_MSR_LIST;
                        } else {
                            showAlert(response.data.Message);
                        }

                        unBlockElement(elementBlock);
                    }, function errorCallback(response) {

                        console.log(response);
                        if (response != null && response.data != null && response.data.Message != null) {
                            showAlert(response.data.Message);
                        } else {
                            bootbox.alert("Error In Application Process");
                        }

                        unBlockElement(elementBlock);
                    });

                    console.log('This was logged in the callback: ' + result);
                }
            }
        });
    }

    $scope.DiscardIssued = function () {
        var elementBlock = '#MSR-issued-form';

        bootbox.confirm({
            message: "By confirming, you've confirm that number of CBE will set to Discard",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    BlockElement(elementBlock);

                    $http({
                        method: 'POST',
                        url: URI_DISCARD_MSR_ISSUED,
                        data: $scope.Model
                    }).then(function successCallback(response) {

                        if (response.data.IsSuccess) {

                            if ($scope.Model.CBEDocumentDetails.length > 0) {
                                UpdateFileUpload(response.data.ResponseObject);
                            }

                            window.location = URI_MSR_LIST;
                        } else {
                            showAlert(response.data.Message);
                        }

                        unBlockElement(elementBlock);
                    }, function errorCallback(response) {

                        console.log(response);
                        if (response != null && response.data != null && response.data.Message != null) {
                            showAlert(response.data.Message);
                        } else {
                            bootbox.alert("Error In Application Process");
                        }

                        unBlockElement(elementBlock);
                    });

                    console.log('This was logged in the callback: ' + result);
                }
            }
        });
    }

    $scope.CreateRequisition = function () {

        var elementBlock = '#MSR-form';

        bootbox.confirm({
            message: "By confirming, you've confirm that number of CBE Post to Requitition",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    BlockElement(elementBlock);

                    $http({
                        method: 'POST',
                        url: URI_POST_REQUISITION,
                        data: $scope.Model
                    }).then(function successCallback(response) {
                        if (response.data.IsSuccess) {

                            window.location = URI_MSR_LIST;
                        } else {
                            showAlert(response.data.Message);
                        }
                        unBlockElement(elementBlock);
                    }, function errorCallback(response) {

                        console.log(response);

                        if (response != null && response.data != null && response.data.Message != null) {
                            showAlert(response.data.Message);
                        } else {
                            bootbox.alert("Error In Application Process");
                        }

                        unBlockElement(elementBlock);
                    });

                    console.log('This was logged in the callback: ' + result);
                }
            }
        });

    }

    $scope.GridCssRow = function (Isfulfilled, IsRemove) {

        var addDisabled = ' bg-grey';
        if (Isfulfilled) {
            addDisabled = ' ';
        } else {
            if (IsRemove) {
                addDisabled = ' ';
            }
        }

        return 'form-control' + addDisabled;
    }

    $scope.GridCssRowInput = function (Isfulfilled, IsRemove) {

        var addDisabled = ' bg-yellow-lemon';
        if (Isfulfilled) {
            addDisabled = ' disabled';
        } else {
            if (IsRemove) {
                addDisabled = ' disabled';
            }
        }

        return 'form-control' + addDisabled;
    }

    var SearchMSR = function () {
        var elementBlock = '#MSR-lookup';
        BlockElement(elementBlock);
        $scope.Modal.ListData = [];


        $http({
            method: 'POST',
            url: URI_SEARCH_MSR,
            data: $scope.Modal
        }).then(function successCallback(response) {

            $scope.Modal = response.data;
            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });
    }

    var SearchSupplier = function () {
        var elementBlock = '#MSR-lookup';
        BlockElement(elementBlock);
        $scope.Modal.ListData = [];


        $http({
            method: 'POST',
            url: URI_SEARCH_MSR,
            data: $scope.Modal
        }).then(function successCallback(response) {

            $scope.Modal = response.data;
            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });
    }

    $scope.SelectSupplier = function (Id) {
        $scope.IsEnableSubmit = true;

        var elementBlock = '#EnumTable-lookup';
        BlockElement(elementBlock);
        $scope.Modal.ListData = [];

        $http({
            method: 'GET',
            url: URI_GET_MSR_DATA.replace(GUID_EMPTY, Id)
        }).then(function successCallback(response) {

            $scope.Model = response.data;
            $scope.Model.IssuedType = IssuedType;
            $('#EnumTable-lookup').modal('toggle');

            if ($scope.Model.IssuedType != cancelLabel && $scope.Model.MSR.WorkOrderNo != "") {
                var IsValidWO = validStatus.indexOf($scope.Model.MSR.WorkOrderStatus);
                console.log(IsValidWO);

                if (IsValidWO >= 0) {
                    $scope.IsEnableSubmit = false;
                }
            }

            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert("Error Application");
            unBlockElement(elementBlock);
        });
    }

    $scope.resetSupplierDetail = function (index, idSupplierLookup) {


        var CBEBidSuppliersList = [];

        for (var i = 0; i < $scope.Model.MSRIssuedDetails[index].CBEBidSuppliers.length; i++) {
            if ($scope.Model.MSRIssuedDetails[index].CBEBidSuppliers[i].Id == idSupplierLookup) {
                $scope.Model.MSRIssuedDetails[index].CBEBidSuppliers[i].Supplier = "";
            }

            if ($scope.Model.MSRIssuedDetails[index].CBEBidSuppliers[i].Supplier != null &&
                $scope.Model.MSRIssuedDetails[index].CBEBidSuppliers[i].Supplier != '') {
                CBEBidSuppliersList.push($scope.Model.MSRIssuedDetails[index].CBEBidSuppliers[i]);
            }
        }

        var totalRow = CBEBidSuppliersList.length;
        for (var i = 0; i < (3 - totalRow); i++) {
            CBEBidSuppliersList.push(newRowBidSupplierModel());
        }

        $scope.Model.MSRIssuedDetails[index].CBEBidSuppliers = CBEBidSuppliersList;
    }

    var DocumentKeyUpload = ($scope.Model.DocumentNo !== undefined && $scope.Model.DocumentNo !== null && $scope.Model.DocumentNo !== "" ? $scope.Model.DocumentNo : GUID_RANDOM);

    $scope.ProgressIndicator = 0;

    function LoadFileUpload() {

        var elementBlock = '#form-file-upload';
        BlockElement(elementBlock);

        $http({
            method: 'GET',
            url: URI_ASSET_API + "/api/file?DocumentNo=" + DocumentKeyUpload,
        }).then(function successCallback(response) {

            if (response.data != null) {
                $scope.Model.CBEDocumentDetails = response.data;
            }

            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });
    }

    //LoadFileUpload();

    $scope.UploadFile = function () {
        $scope.ProgressIndicator = 0;
        var FormFiles = document.getElementById('fileUpload').files;

        var formData = new FormData();
        formData.append('DocumentNo', DocumentKeyUpload);
        formData.append('AdditionalValue', '');
        formData.append('UploadBy', USER_CURRENT.NIKSITE);
        formData.append('IsTemp', $scope.Model.DocumentNo !== undefined && $scope.Model.DocumentNo !== null && $scope.Model.DocumentNo !== '' ? false : true);
        formData.append('FormFiles', FormFiles[0]);

        var elementBlock = '#upload-progress-control';
        BlockElement(elementBlock);

        //console.log(formData);

        startUpdatingProgressIndicator();

        $http({
            method: 'POST',
            url: URI_ASSET_API + "/api/file",
            data: formData,
            headers: { 'Content-Type': undefined }
        }).then(function successCallback(response) {
            console.log(response.data);
            console.log($scope.Model);
            var objData = response.data;
            if (objData.isSuccess) {
                //$scope.FileUploadDetails.push(objData.responseObject); //ntaps
                $scope.Model.CBEDocumentDetails.push(objData.responseObject)
                document.getElementById("upload-progress-success").style.display = 'block';
                document.getElementById("upload-progress-success").innerHTML = objData.message;
                document.getElementById('btn-upload-progress-remove').click();

                setTimeout(function () {
                    document.getElementById("upload-progress-success").style.display = 'none';
                }, 10000);

            } else {
                document.getElementById("upload-progress-error").style.display = 'block';
                document.getElementById("upload-progress-error").innerHTML = objData.message;

                setTimeout(function () {
                    document.getElementById("upload-progress-error").style.display = 'none';
                }, 10000);
            }

            stopUpdatingProgressIndicator();

            //$scope.FormLookupItem = response.data;
            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            //console.log(response);
            stopUpdatingProgressIndicator();
            document.getElementById("upload-progress-error").style.display = 'block';
            document.getElementById("upload-progress-error").innerHTML = "Error Client Upload";

            setTimeout(function () {
                document.getElementById("upload-progress-error").style.display = 'none';
            }, 10000);

            //bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });

    }

    //$scope.DeleteFileUpload = function (id) {

    //    var elementBlock = '#form-file-upload';
    //    BlockElement(elementBlock);

    //    $http({
    //        method: 'DELETE',
    //        url: URI_ASSET_API + "/api/file?Id=" + id,
    //    }).then(function successCallback(response) {
    //        var response = response.data;

    //        if (response.isSuccess) {

    //            $scope.Model.CBEDocumentDetails = $scope.Model.CBEDocumentDetails.filter(function (value, index, arr) {
    //                return value.id !== id;
    //            });

    //            document.getElementById("upload-progress-success").style.display = 'block';
    //            document.getElementById("upload-progress-success").innerHTML = response.message;

    //            setTimeout(function () {
    //                document.getElementById("upload-progress-success").style.display = 'none';
    //            }, 5000);

    //        } else {
    //            document.getElementById("upload-progress-error").style.display = 'block';
    //            document.getElementById("upload-progress-error").innerHTML = response.message;

    //            setTimeout(function () {
    //                document.getElementById("upload-progress-error").style.display = 'none';
    //            }, 5000);
    //        }

    //        unBlockElement(elementBlock);
    //    }, function errorCallback(response) {
    //        console.log(response);
    //        bootbox.alert(response.data.Message);
    //        unBlockElement(elementBlock);
    //    });


    //}

    $scope.DeleteFileUpload = function (id) {

        $scope.Model.CBEDocumentDetails = $scope.Model.CBEDocumentDetails.filter(function (value, index, arr) {
            return value.id !== id;
        });

    }

    function UpdateFileUpload(documentNo) {
        $http({
            method: 'PUT',
            url: URI_ASSET_API + "/api/file",
            data: {
                DocumentNo: documentNo,
                TempNo: GUID_RANDOM
            }
        }).then(function successCallback(response) {
            console.log(response);
        }, function errorCallback(response) {
            console.log(response);
        });
    }

    var intervalId;
    function startUpdatingProgressIndicator() {

        document.getElementById("upload-progress").style.display = 'block';
        document.getElementById("upload-progress-bar").setAttribute("style", "width: 0%;");
        document.getElementById("upload-progress-bar").setAttribute("aria-valuenow", "0");

        intervalId = setInterval(
            function () {

                var formData = {
                    DocumentNo: $scope.Model.DocumentNo !== undefined && $scope.Model.DocumentNo !== null && $scope.Model.DocumentNo !== '' ? $scope.Model.DocumentNo : GUID_RANDOM,
                    UploadBy: USER_CURRENT.NIKSITE
                };

                console.log(formData);


                $http({
                    method: 'POST',
                    url: URI_ASSET_API + "/api/file/progress",
                    data: formData,
                    //headers: { 'Content-Type': undefined }
                }).then(function successCallback(response) {
                    console.log(response.data);
                    $scope.ProgressIndicator = response.data;

                    document.getElementById("upload-progress-bar").setAttribute("style", "width: " + response.data + "%;");
                    document.getElementById("upload-progress-label").innerHTML = response.data + "%";
                    document.getElementById("upload-progress-bar").setAttribute("aria-valuenow", response.data);


                    //$scope.FormLookupItem = response.data;
                    //unBlockElement(elementBlock);
                }, function errorCallback(response) {
                    console.log(response);
                    clearInterval(intervalId);
                    //bootbox.alert(response.data.Message);
                    //unBlockElement(elementBlock);
                });

            },
            3000
        );


    }

    function stopUpdatingProgressIndicator() {
        clearInterval(intervalId);
        document.getElementById("upload-progress").style.display = 'none';
    }

    function FuncPriceTotal_USD(homecur, sourcecur, pycostcode, parentindex, index, TotalPrice) {

        $scope.Model.homecur = homecur;
        $scope.Model.sourcecur = sourcecur;
        $scope.Model.pycostcode = pycostcode;

        var a = 0;

        $http({
            method: 'GET',
            url: URI_GET_RATE.replace("HOMECURR", homecur)
        }).then(function successCallback(response) {


            var Rate = response.data.Message;
            if (homecur == "USD") {
                Rate = 1;
            }

            $scope.Model.MSRIssuedDetails[parentindex].CBEBidSuppliers[index].Rate = Rate;
            $scope.Model.MSRIssuedDetails[parentindex].CBEBidSuppliers[index].TotalPrice_USD = (TotalPrice / Rate).toFixed(2);

        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert("Error Application");
            unBlockElement(elementBlock);
        });

        return a;
    }

    function FuncValidationQty(parentindex, index) {

        var Acceptable = false;

        var QtyOutstanding = $scope.Model.MSRIssuedDetails[parentindex].QtyPurchaseOutstanding;
        var QtyTotal = $scope.Model.MSRIssuedDetails[parentindex].CBEBidSuppliers[index].Qty;


        if (QtyTotal <= QtyOutstanding) {
            Acceptable = true;
        }

        return Acceptable;
    }

    function FuncSaveValidation(model) {
        var Message = "";

        if (model.RequestorCostCenter == null) {
            Message = "Please select one <b>Cost Center</b>"
        }

        console.log(model.RequestorCostCenter);
        console.log(model.WorkFlowCode);
        return Message;
    }
});