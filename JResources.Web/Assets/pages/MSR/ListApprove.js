﻿jQuery('#ModalStatusRequest').pulsate({
    color: "#45B6AF"
});

var app = angular.module('JResourcesApp', ['ngMessages', 'ui.bootstrap']);
app.controller('JResourcesController', function ($scope, $http) {
    $scope.Model = objModel();
    $scope.Modal = objMSRModal();
    $scope.CompanyDepartments = objCompanyDepartments();
    $scope.DepartmentList = [];
    $scope.FileUploads = [];


    function LoadFileUpload(DocumentKeyUpload) {
        $scope.FileUploads = [];
        var elementBlock = '#form-file-upload';
        BlockElement(elementBlock);

        $http({
            method: 'GET',
            url: URI_ASSET_API + "/api/file?DocumentNo=" + DocumentKeyUpload,
        }).then(function successCallback(response) {

            if (response.data != null) {
                $scope.FileUploads = response.data;
            }

            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });
    }

    $scope.SetListDepartment = function () {
        $scope.DepartmentList = [];
        $scope.DepartmentList.push({ ID: GuidEmpty, DepartmentName: "SELECT ALL DEPARTMENT" });

        for (var i = 0; i < $scope.CompanyDepartments.length; i++) {
            if ($scope.CompanyDepartments[i].ID == $scope.Model.CompanyId) {
                for (var ii = 0; ii < $scope.CompanyDepartments[i].Departments.length; ii++) {
                    $scope.DepartmentList.push(
                        $scope.CompanyDepartments[i].Departments[ii]
                    );
                }
            }
        }
    }

    var SearchMSR = function () {
        var elementBlock = '#MSR-table';
        BlockElement(elementBlock);
        $scope.Model.ListData = [];

        $http({
            method: 'POST',
            url: URI_SEARCH_MSR,
            data: $scope.Model
        }).then(function successCallback(response) {

            $scope.Model = response.data;
            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });
    }

    $scope.SearchMSR = function () {
        $scope.Model.CurrentPage = 1;
        SearchMSR();
    }

    $scope.ResetMSR = function () {
        $scope.Model = objModel();
        SearchMSR();
    }

    $scope.pageChangedMSRList = function () {
        SearchMSR();
    };

    $scope.ResetMSR();

    $scope.MSRModal = function (Id) {
        $('#MSR-modal').modal('toggle');

        var elementBlock = '#MSR-modal .portlet';
        BlockElement(elementBlock);
        $scope.Modal = objMSRModal();
        $http({
            method: 'GET',
            url: URI_GET_MSR_MODAL + '/' + Id
        }).then(function successCallback(response) {

            $scope.Modal = response.data;
            $scope.Modal.StatusRequest = null;
            LoadFileUpload($scope.Modal.DocumentNo);
            unBlockElement(elementBlock);
            //$scope.ResetMSR();
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });
    }

    if (MSRIdModal != GuidEmpty) {
        $scope.MSRModal(MSRIdModal);
    }

    function ActionUpdateStatus() {

        var elementBlock = '#MSR-modal .portlet';
        BlockElement(elementBlock);

        var model = objMSRModal();
        model.ID = $scope.Modal.ID;
        model.StatusRequest = $scope.Modal.StatusRequest;
        model.Reason = $scope.Modal.Reason;

        $http({
            method: 'POST',
            url: URI_APPROVE_MSR,
            data: model
        }).then(function successCallback(response) {
            bootbox.alert({
                title: "MSR " + $scope.Modal.DocumentNo,
                message: response.data.Message
            });

            if (response.data.IsSuccess) {
                for (var i = 0; i < $scope.Model.ListData.length; i++) {
                    if ($scope.Model.ListData[i].DocumentNo == $scope.Modal.DocumentNo) {
                        $scope.Model.ListData[i].StatusRequestCode = $scope.Modal.StatusRequest;
                        $scope.SearchMSR();
                    }
                }
            }

            $('#MSR-modal').modal('toggle');
            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });

    }

    $scope.ApproveMSR = function () {

        var ACT = $('#ModalStatusRequest option:selected').val();
        var ACT_LABEL = $('#ModalStatusRequest option:selected').text();

        if (ACT == REQUEST_STATUS.APPROVED) {
            
            bootbox.confirm({
                title: "VIEW " + $scope.Modal.DocumentNo,
                message: "Are you sure to change the status into " + $('#ModalStatusRequest option:selected').text() + " ?",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Cancel'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Confirm'
                    }
                },
                callback: function (result) {
                    if (result) {
                        ActionUpdateStatus();
                    }
                }
            });

        } else {
            bootbox.prompt({
                title: "Give reasons why the MSR is " + ACT_LABEL,
                inputType: 'textarea',
                callback: function (result) {
                    if (result != null) {
                        if (result == '') {
                            bootbox.alert("Reasons cannot be blank!");
                        } else {
                            $scope.Modal.Reason = result;
                            ActionUpdateStatus();
                        }
                    }
                }
            });
        }









    }

    $scope.MSRPrintOut = function (_id) {
        var url = URL_PRINT_FORM + '?type=MSR_PRINT_OUT&id=' + _id;
        window.open(url, '_blank');
        window.focus();
    }
});