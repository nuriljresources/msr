﻿var app = angular.module('JResourcesApp', ['ngMessages', 'ui.bootstrap']);
app.controller('JResourcesController', function ($scope, $http) {

    $scope.TOTAL_PAGE_DEFAULT = TOTAL_PAGE;
    $scope.model = formData.model();
    $scope.modelItem = formData.DetailForm();

    // #region Search Form  -------------
    $scope.paramUser = formData.paramUser();
    $scope.listUser = [];
    $scope.selectUser = {};
    var txtUserSelect = '';

    $scope.SetPIC1 = function () {
        $scope.listUser = [];
        $scope.selectUser = {};
        txtUserSelect = 'PIC1';
    }

    $scope.SetPIC2 = function () {
        $scope.listUser = [];
        $scope.selectUser = {};
        txtUserSelect = 'PIC2';
    }

    $scope.trCheck = function (item) {
        $scope.selectUser = item;
    }

    $scope.Run = function () {
        if (txtUserSelect == 'PIC1') {
            $scope.model.PIC1User = $scope.selectUser;
        }

        if (txtUserSelect == 'PIC2') {
            $scope.model.PIC2User = $scope.selectUser;
        }
    }

    $scope.FirstCallUserList = true;
    $scope.GetUserList = function () {

        $http({
            method: 'POST',
            url: URL.GET_USERLIST,
            data: $scope.paramUser
        }).then(function successCallback(response) {
            $scope.listUser = response.data.result;

            // Agar tidak 2x Dipanggil
            if ($scope.FirstCallUserList) {
                var table = $('#user-lookup-table');
                var oTable = table.dataTable({
                    "scrollY": "300",
                    "ordering": false,
                    "deferRender": true,
                    "order": [
                        [0, 'asc']
                    ]
                });
            }

            $scope.FirstCallUserList = false


        }, function errorCallback(response) {
            Metronic.unblockUI();
        });
    }
    // #endregion           -------------


    // #region Search Item List  -------------

    $scope.totalItems = 0;
    $scope.currentPage = 1;

    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };

    $scope.paramPartItem = formData.PartItemSearch;
    $scope.FirstCallItemList = true;

    $scope.ClearListItem = function () {
        $scope.listItems = [];
        $scope.totalItems = 0;
        $scope.paramPartItem = formData.PartItemSearch();
    }

    function getPartJSON() {
        BlockElement('.modal-content');

        $scope.paramPartItem.CurrentPage = $scope.currentPage;
        $scope.paramPartItem.DateFrom = $scope.model.PSTDateStr;
        $scope.paramPartItem.IsZeroQty = $scope.model.IncludeZeroQty;

        var url = URL.GET_ITEM_LIST + "?" + encodeQueryData($scope.paramPartItem);

        $http({
            method: 'GET',
            url: url,
            data: $scope.paramPartItem
        }).then(function successCallback(response) {
            $scope.listItems = CompareList(response.data.result);
            

            var param = response.data.param;
            $scope.totalItems = Math.ceil(param.TotalPage / $scope.TOTAL_PAGE_DEFAULT) * 10;

            if ($scope.FirstCallItemList) {
                var tableSloc = $('#sloc-lookup-table');
                var oTableSloc = tableSloc.dataTable({
                    "scrollY": "300",
                    "ordering": false,
                    "deferRender": true,
                    "order": [
                        [0, 'asc']
                    ]
                });
            }

            $scope.FirstCallItemList = false;
            unBlockElement('.modal-content');
        }, function errorCallback(response) {
            unBlockElement('.modal-content');
        });

    }

    $scope.pageChanged = function () {
        getPartJSON();
    };

    $scope.GetPartItemList = function () {
        $scope.currentPage = 1;
        getPartJSON();
    }

    $scope.addItemToDetail = function (item) {
        item.IsSelected = true;
        $scope.model.PSTGenerateDetails.push(item);
    }

    $scope.removeItemToDetail = function (item) {
        item.IsSelected = false;

        var newList = [];
        for (var i = 0; i < $scope.model.PSTGenerateDetails.length; i++) {
            var newItem = $scope.model.PSTGenerateDetails[i];

            if (newItem.ItemKey != item.ItemKey) {
                newList.push(newItem);
            }            
        }

        $scope.model.PSTGenerateDetails = newList;
    }

    function CompareList(ITEM_JSON) {
        var LIST_ITEM = [];

        if (ITEM_JSON != undefined && ITEM_JSON != null && ITEM_JSON.length > 0) {

            for (var i = 0; i < ITEM_JSON.length; i++) {

                var ITEM = ITEM_JSON[i];

                $.each($scope.model.PSTGenerateDetails, function (index, value) {

                    if (ITEM.ItemKey == (value.ItemNo + "" + value.StorageLocation)) {
                        ITEM.IsSelected = true;
                        return true;
                    }

                });


                LIST_ITEM.push(ITEM);
            }
        }
        

        return LIST_ITEM;
    }


    // #endregion           -------------

    $scope.GetDetail = function () {
        BlockUI();

        $http({
            method: 'POST',
            url: URL.UPLOAD_SCHEDULER,
            data: $scope.model
        }).then(function successCallback(response) {
            var IsFailed = true;
            if (response.data != undefined) {
                if (response.data.IsSuccess) {
                    if (response.data.ResponseObject != undefined) {
                        IsFailed = false;
                        $scope.model.PSTGenerateDetails = response.data.ResponseObject.PSTGenerateDetails;
                        window.setTimeout(function () {
                            unBlockUI();
                        }, 2000);
                    }
                } else {
                    bootbox.alert(response.data.Message);
                }
            }

            if (IsFailed) {
                window.setTimeout(function () {
                    unBlockUI();
                }, 2000);
            }

        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockUI();
        });
    }

    $scope.ResetGenerate = function () {
        $scope.model.PSTGenerateDetails = [];
    }

    $scope.SubmitForm = function () {

        Metronic.blockUI({
            boxed: true
        });

        $scope.model.IncludeZeroQty = $('input[name="IncludeZeroQty"]').prop('checked');
        $scope.model.IsAllSloc = $('input[name="IsAllSloc"]').prop('checked');

        $http({
            method: 'POST',
            url: URL.POST_PST_TRANS,
            data: $scope.model
        }).then(function successCallback(response) {
            var IsFailed = true;
            var msgError = '';

            if (response.data != undefined) {
                if (response.data.IsSuccess) {
                    if (response.data.ResponseObject != undefined) {
                        IsFailed = false;
                        window.setTimeout(function () {
                            Metronic.unblockUI();
                        }, 2000);

                        window.location.href = URL.POST_SUCCESS;
                    }
                } else {
                    msgError = response.data.Message;
                }
            }

            if (IsFailed) {
                if (msgError != '') {
                    bootbox.alert(msgError);
                } else {
                    bootbox.alert("Failed Save PST Trans");
                }

                window.setTimeout(function () {
                    Metronic.unblockUI();
                }, 2000);
            }

        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert("Failed Save PST Trans");
            Metronic.unblockUI();
        });
    }
});