﻿var app = angular.module('JResourcesApp', ['ngMessages', 'ui.bootstrap']);
app.controller('JResourcesController', function ($scope, $http) {
    $scope.Model = objModel();
    $scope.Modal = objModalMSR();

    $scope.ResetMSR = function () {
        $scope.Model = objModel();
    }

    $scope.removeWorkOrder = function () {
        $scope.Model = objModel();
    }

    $scope.LookupSearchMSR = function () {
        $('#MSR-lookup').modal('toggle');
    }

    var SearchMSR = function () {
        var elementBlock = '#MSR-lookup';
        BlockElement(elementBlock);
        $scope.Modal.ListData = [];


        $http({
            method: 'POST',
            url: URI_SEARCH_MSR,
            data: $scope.Modal
        }).then(function successCallback(response) {

            $scope.Modal = response.data;
            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });
    }

    $scope.SearchMSR = function () {
        $scope.Modal.CurrentPage = 1;
        SearchMSR();
    }

    $scope.SelectMSR = function (Id) {
        $scope.IsEnableSubmit = true;

        var elementBlock = '#MSR-lookup';
        BlockElement(elementBlock);
        $scope.Modal.ListData = [];

        $http({
            method: 'GET',
            url: URI_GET_MSR_DATA.replace(GUID_EMPTY, Id)
        }).then(function successCallback(response) {

            $scope.Model = response.data;
            $scope.Model.IssuedType = IssuedType;
            $('#MSR-lookup').modal('toggle');

            if ($scope.Model.IssuedType != cancelLabel && $scope.Model.MSR.WorkOrderNo != "") {
                var IsValidWO = validStatus.indexOf($scope.Model.MSR.WorkOrderStatus);
                console.log(IsValidWO);

                if (IsValidWO >= 0) {
                    $scope.IsEnableSubmit = false;
                }
            }

            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert("Error Application");
            unBlockElement(elementBlock);
        });
    }

    $scope.pageChangedLookupItem = function () {
        SearchMSR();
    }


    $scope.lookupSearchLocation = function (item) {
        $scope.ItemLocationSelect = item;
        $('#location-lookup').modal('toggle');
    }

    $scope.btnSelectLocation = function (locationId) {

        for (var i = 0; i < $scope.Model.MSRIssuedDetails.length; i++) {
            if ($scope.Model.MSRIssuedDetails[i].LineNo == $scope.ItemLocationSelect.LineNo) {
                var ItemLine = $scope.Model.MSRIssuedDetails[i];

                for (var ii = 0; ii < ItemLine.ItemLocations.length; ii++) {
                    if (ItemLine.ItemLocations[ii].Location == locationId) {
                        ItemLine.LocationFrom = ItemLine.ItemLocations[ii].Location;
                        ItemLine.StochOnHand = ItemLine.ItemLocations[ii].QtyOnHand;
                    }
                }

            }
        }

        $('#location-lookup').modal('toggle');
    }

    $scope.resetItemMSRDetail = function (lineNo) {
        alert('remove');
    }

    $scope.CreateIssued = function () {
        var elementBlock = '#MSR-issued-form';
        BlockElement(elementBlock);

        $http({
            method: 'POST',
            url: URI_CREATE_MSR_ISSUED,
            data: $scope.Model
        }).then(function successCallback(response) {

            if (response.data.IsSuccess) {
                window.location = URI_MSR_LIST;
            } else {
                showAlert(response.data.Message);
            }

            unBlockElement(elementBlock);
        }, function errorCallback(response) {

            console.log(response);
            if (response != null && response.data != null && response.data.Message != null) {
                showAlert(response.data.Message);
            } else {
                bootbox.alert("Error In Application Process");
            }

            unBlockElement(elementBlock);
        });
    }

    $scope.GridCssRow = function (Isfulfilled, IsRemove) {

        var addDisabled = ' bg-grey';
        if (Isfulfilled) {
            addDisabled = ' ';
        } else {
            if (IsRemove) {
                addDisabled = ' ';
            }
        }

        return 'form-control' + addDisabled;
    }

    $scope.GridCssRowInput = function (Isfulfilled, IsRemove) {

        var addDisabled = ' bg-yellow-lemon';
        if (Isfulfilled) {
            addDisabled = ' disabled';
        } else {
            if (IsRemove) {
                addDisabled = ' disabled';
            }
        }

        return 'form-control' + addDisabled;
    }

});