﻿ 
var app = angular.module('JResourcesApp', ['ngMessages', 'ui.bootstrap']);
app.controller('JResourcesController', function ($scope, $http) {

    $scope.Model = objModel();
    $scope.CompanyDepartments = objCompanyDepartments();
    $scope.DepartmentList = [];

    $scope.SetListDepartment = function () {
        $scope.DepartmentList = [];
        $scope.DepartmentList.push({ ID: GUID_EMPTY, DepartmentName: "SELECT ALL DEPARTMENT" });

        for (var i = 0; i < $scope.CompanyDepartments.length; i++) {
            if ($scope.CompanyDepartments[i].ID == $scope.Model.CompanyId) {
                for (var ii = 0; ii < $scope.CompanyDepartments[i].Departments.length; ii++) {
                    $scope.DepartmentList.push(
                        $scope.CompanyDepartments[i].Departments[ii]
                    );
                }
            }
        }
    }

    var SearchMSR = function () {
        var elementBlock = '#MSR-table';
        BlockElement(elementBlock);
        $scope.Model.ListData = [];

        $http({
            method: 'POST',
            url: URI_SEARCH_MSRISSUED,
            data: $scope.Model
        }).then(function successCallback(response) {

            $scope.Model = response.data;
            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });
    }

    $scope.SearchMSR = function () {
        $scope.Model.CurrentPage = 1;
        SearchMSR();
    }

    $scope.ResetMSR = function () {
        $scope.Model = objModel();
        SearchMSR();

    }

    $scope.pageChangedMSRList = function () {
        SearchMSR();
    };

    $scope.ResetMSR();

    $scope.Log = '';
    $scope.LogData = '';

    $scope.MSRIssuedModal = function (log) {
        $('#MSR-issued-modal').modal('toggle');
        var elementBlock = '#MSR-issued-modal .portlet';
        BlockElement(elementBlock);
        $scope.Log = log.LogMessage;
        $scope.LogData = log.LogDataJson;
        unBlockElement(elementBlock);
    }

    $scope.LogDataModal = function () {
        $('#log-data-modal').modal('toggle');
        var elementBlock = '#log-data-modal .portlet';
        BlockElement(elementBlock);
        unBlockElement(elementBlock);
    }




    $scope.SendAccpac = function (id) {

        var elementBlock = '#MSR-table';
        BlockElementAccpac(elementBlock);

        var uri = URI_SEND_ACCPAC.replace("0", id);

        $http({
            method: 'GET',
            url: uri
        }).then(function successCallback(response) {
            if (response.data.IsSuccess) {
                showNotification("Document successfully posting to accpac");
            } else {
                showAlert("Document failed posting to accpac, to see the detail error can be seen on log");
            }

            $scope.ResetMSR();
            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            if (response != null && response.data != null && response.data.Message != null) {
                showAlert(response.data.Message);
            } else {
                bootbox.alert("Error In Application Process");
            }
            unBlockElement(elementBlock);
        });


    }



});
