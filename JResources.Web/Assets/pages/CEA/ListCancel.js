﻿jQuery('#ModalStatusRequest').pulsate({
    color: "#45B6AF"
});

var app = angular.module('JResourcesApp', ['ngMessages', 'ui.bootstrap']);
app.controller('JResourcesController', function ($scope, $http) {

    $scope.Model = objModel();
    var SearchMSR = function () {
        var elementBlock = '#MSR-table';
        BlockElement(elementBlock);
        $scope.Model.ListData = [];

        $http({
            method: 'POST',
            url: URI_SEARCH_MSRISSUED,
            data: $scope.Model
        }).then(function successCallback(response) {

            $scope.Model = response.data;
            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });
    }

    $scope.SearchMSR = function () {
        $scope.Model.CurrentPage = 1;
        SearchMSR();
    }

    $scope.ResetMSR = function () {
        $scope.Model = objModel();
        SearchMSR();
    }

    $scope.pageChangedMSRList = function () {
        SearchMSR();
    };

    $scope.ResetMSR();

    $scope.MSRIssuedModal = function (Id) {
        $('#MSR-issued-modal').modal('toggle');

        var elementBlock = '#MSR-issued-modal .portlet';
        BlockElement(elementBlock);

        $http({
            method: 'GET',
            url: URI_GET_MSRISSUED + '/' + Id
        }).then(function successCallback(response) {

            $scope.Modal = response.data;
            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });
    }

    $scope.ReverseTransfer = function () {

        var elementBlock = '#MSR-issued-modal .portlet';
        BlockElement(elementBlock);

        $http({
            method: 'POST',
            url: URI_REVERSE_TRANSFER,
            data: $scope.Modal
        }).then(function successCallback(response) {
            if (response.data.IsSuccess) {
                window.location = URI_LIST_ISSUED;
            } else {
                showAlert(response.data.Message);
            }

            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert("Error In Application");
            unBlockElement(elementBlock);
        });
    }

    $scope.ApproveCancel = function () {

        bootbox.confirm({
            title: "VIEW " + $scope.Modal.MSRIssuedNo,
            message: "Are you sure to change the status into " + $('#ModalStatusRequest option:selected').text() + " ?",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                if (result) {


                    var elementBlock = '#MSR-issued-modal .portlet';
                    BlockElement(elementBlock);

                    $http({
                        method: 'POST',
                        url: URI_APPROVE_CANCEL,
                        data: $scope.Modal
                    }).then(function successCallback(response) {

                        if (response.data.IsSuccess) {
                            window.location = URI_LIST_CANCEL;
                        } else {
                            bootbox.alert({
                                title: "Issued Cancel " + $scope.Modal.MSRIssuedNo,
                                message: response.data.Message
                            });

                            $scope.ResetMSR();
                            $('#MSR-issued-modal').modal('toggle');
                            unBlockElement(elementBlock);
                        }                        
                    }, function errorCallback(response) {
                        console.log(response);
                        bootbox.alert(response.data.Message);
                        unBlockElement(elementBlock);
                    });

                }
            }
        });




    }

});