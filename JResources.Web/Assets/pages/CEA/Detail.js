﻿var app = angular.module('JResourcesApp', ['ngMessages', 'ui.bootstrap']);
app.controller('JResourcesController', function ($scope, $http) {
    $scope.Model = objModel();
    $scope.IsWorkOrder = false;
    $scope.FormLookupItem = objFormLookupItem();
    $scope.FormLookupCostCode = objFormLookupCostCode();
    $scope.FormLookupWorkOrder = objFormLookupWorkOrder();
    $scope.ObjListLeadTime = ObjListLeadTime();
    $scope.IsInventory = true;
    $scope.ObjListDepartment = [];

    
    
    function IncludeDepartmentMaintenance() {
        var list = [];
        var listDept = ObjListDepartment();

        for (var i = 0; i < listDept.length; i++) {
            var dept = listDept[i];

            if ($scope.IsWorkOrder) {
                list.push(dept);
            } else {
                if (ObjDeptIdMaintenances.indexOf(dept.Value) < 0) {
                    list.push(dept);
                }
            }
        }

        $scope.ObjListDepartment = list;
    }

    IncludeDepartmentMaintenance();

    $scope.TargetMSRDetail = '';
    $scope.ItemLocationListSelect = {};
    $scope.WorkOrderListSelect = {};
    $scope.listCostCenter = [];

    $scope.ChangeJobType = function () {
        var MaintenanceCode = $scope.Model.MaintenanceCode;

        for (var i = 0; i < $scope.ObjListLeadTime.length; i++) {
            var itemLeadTime = $scope.ObjListLeadTime[i];
            if (itemLeadTime.MaintenanceType == MaintenanceCode) {
                $scope.Model.LeadTime = itemLeadTime;
                $scope.Model.LeadTimeNumber = 0;
                if (!$scope.Model.LeadTime.IsFreeNumber) {
                    $scope.Model.LeadTimeNumber = $scope.Model.LeadTime.MaxNumber;
                }
            }

        }

        

    }

    $scope.ChangeMSRType = function () {

        if ($scope.Model.MSRType == MSRTypeInventory) {
            $scope.IsInventory = true;
        } else {
            $scope.IsInventory = false;
        }

        $scope.IsWorkOrder = false;
        $scope.listCostCenter = [];
        resetListItemEmpty();
    }

    $scope.ChangeDepartment = function () {

        var elementBlock = '#MSR-form';
        BlockElement(elementBlock);

        var uri = URI_GET_COSTCENTER + "?DepartmentId=" + $scope.Model.DepartmentId;

        $http({
            method: 'GET',
            url: uri
        }).then(function successCallback(response) {

            if (response.data != null) {
                $scope.listCostCenter = response.data;
            }

            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });


        uri = URI_GET_APPROVAL + "?DepartmentId=" + $scope.Model.DepartmentId;
        $scope.Model.TotalWithApprovalGM = null;

        $http({
            method: 'GET',
            url: uri
        }).then(function successCallback(response) {

            $scope.Model.Manager = response.data.Manager;
            $scope.Model.GeneralManagerSite = response.data.GeneralManagerSite;
            $scope.Model.TotalWithApprovalGM = response.data.TotalWithApprovalGM;

            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });

    }

    var NewId = function () {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    var resetLookupSearch = function () {
        $scope.FormLookupItem = objFormLookupItem();
        var itemModel = objItemPartModel();
        $scope.FormLookupItem.ListData = [];
        for (var i = 0; i < 5; i++) {
            $scope.FormLookupItem.ListData.push(itemModel);
        }
    }

    var newRowMSRModel = function () {
        var model = objItemPartModel();
        model.IdLookup = NewId();
        return model;
    }

    var resetListItemEmpty = function () {
        var MSRDetail = [];

        for (var i = 0; i < 5; i++) {
            MSRDetail.push(newRowMSRModel());
        }

        $scope.Model.MSRDetail = MSRDetail;
    }

    $scope.resetItemMSRDetail = function (idItemLookup) {
        var MSRDetailList = [];

        for (var i = 0; i < $scope.Model.MSRDetail.length; i++) {
            if ($scope.Model.MSRDetail[i].IdLookup == idItemLookup) {
                $scope.Model.MSRDetail[i].ItemNo = "";
            }

            if ($scope.Model.MSRDetail[i].ItemNo != null && $scope.Model.MSRDetail[i].ItemNo != '') {
                MSRDetailList.push($scope.Model.MSRDetail[i]);
            }
        }

        var totalRow = MSRDetailList.length;
        for (var i = 0; i < (5 - totalRow) ; i++) {
            MSRDetailList.push(newRowMSRModel());
        }

        $scope.Model.MSRDetail = MSRDetailList;
    }

    $scope.addEmptyMSRDetail = function () {
        $scope.Model.MSRDetail.push(newRowMSRModel());
    }

    $scope.lookupSearch = function (idItemLookup) {
        $scope.TargetMSRDetail = idItemLookup;
        resetLookupSearch();
        $('#itempart-lookup').modal('toggle');
    }

    $scope.lookupSearchLocation = function (idItemLookup) {
        $scope.TargetMSRDetail = idItemLookup;

        for (var i = 0; i < $scope.Model.MSRDetail.length; i++) {
            if ($scope.Model.MSRDetail[i].IdLookup == idItemLookup) {
                $scope.ItemLocationSelect = $scope.Model.MSRDetail[i];
                $scope.ItemLocationSelect.IsCreated = true;
            }
        }

        $('#location-lookup').modal('toggle');
    }

    $scope.btnSelectLocation = function (locationId) {


        for (var i = 0; i < $scope.Model.MSRDetail.length; i++) {
            if ($scope.Model.MSRDetail[i].IdLookup == $scope.TargetMSRDetail) {
                for (var ii = 0; ii < $scope.Model.MSRDetail[i].ItemLocations.length; ii++) {
                    if ($scope.Model.MSRDetail[i].ItemLocations[ii].Location == locationId) {
                        $scope.Model.MSRDetail[i].Location = $scope.Model.MSRDetail[i].ItemLocations[ii].Location;
                        $scope.Model.MSRDetail[i].StockOnHand = $scope.Model.MSRDetail[i].ItemLocations[ii].QtyOnHand;
                        $scope.Model.MSRDetail[i].Qty = 0;

                        $scope.QtyChange($scope.Model.MSRDetail[i]);
                    }
                }
            }
        }

        $('#location-lookup').modal('toggle');
    }

    var SearchLookupItem = function () {
        var elementBlock = '#itempart-lookup .modal-body';
        BlockElement(elementBlock);

        $scope.FormLookupItem.CostCenter = $scope.Model.CostCenter;
        $scope.FormLookupItem.DepartmentId = $scope.Model.DepartmentId;
        $scope.FormLookupItem.IsInventory = true;
        if ($scope.Model.MSRType != '' && $scope.Model.MSRType != MSRTypeInventory) {
            $scope.FormLookupItem.IsInventory = false;
        }


        $http({
            method: 'POST',
            url: URI_SEARCH_ITEM,
            data: $scope.FormLookupItem
        }).then(function successCallback(response) {
            console.log(response.data);
            $scope.FormLookupItem = response.data;
            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });
    }

    $scope.btnSearchLookupItem = function () {
        $scope.FormLookupItem.CurrentPage = 1;
        SearchLookupItem();
    }

    $scope.pageChangedLookupItem = function () {
        SearchLookupItem();
    };

    $scope.btnInsertItemToList = function (item) {
        var itemModel = objMSRDetailModel();

        for (var i = 0; i < $scope.Model.MSRDetail.length; i++) {
            if ($scope.Model.MSRDetail[i].IdLookup == $scope.TargetMSRDetail) {
                $scope.Model.MSRDetail[i].ItemNo = item.ItemNo;
                $scope.Model.MSRDetail[i].Description = item.Description;
                $scope.Model.MSRDetail[i].Cost = item.TotalCost;
                $scope.Model.MSRDetail[i].UOM = item.UOM;

                $scope.Model.MSRDetail[i].Category = item.Category;
                $scope.Model.MSRDetail[i].CategoryLabel = item.CategoryLabel;             
                $scope.Model.MSRDetail[i].ItemLocations = item.ItemLocations;


                // For Budget
                $scope.Model.MSRDetail[i].QtyBudget = item.QtyBudget;
                $scope.Model.MSRDetail[i].SubTotalBudget = item.SubTotalBudget;
                $scope.Model.MSRDetail[i].QtyBudgetHistory = item.QtyBudgetHistory;
                $scope.Model.MSRDetail[i].SubTotalHistory = item.SubTotalHistory;

            }
        }

        $('#itempart-lookup').modal('toggle');
    }


    $scope.QtyChange = function (item) {
        console.log(item.Qty);

        if (item.Qty <= item.StockOnHand) {
            item.BudgetLabel = 'InStock';
        } else {
            var qtyBudgetStock = item.StockOnHand + item.QtyBudget;
            var qtyAll = item.Qty + item.QtyBudgetHistory;
            item.BudgetLabel = 'Budgeted';

            if (qtyAll > qtyBudgetStock) {

                var SubTotalBudget = item.SubTotalBudget;

                var SubTotal = (item.Qty * item.Cost);
                var SubTotalHistory = (item.QtyBudgetHistory * item.Cost);
                var total = SubTotal + SubTotalHistory;

                if (total > SubTotalBudget) {
                    item.BudgetLabel = 'UnBudgeted';
                }                
            }
        }

        if (item.Qty <= 0 || item.Qty === '' || item.Qty == null) {
            item.BudgetLabel = '';
        }
    }

    

    //$scope.ConfirmationBudget = function () {
    //    $scope.Model.TotalInStock = 0;
    //    $scope.Model.TotalBudgeted = 0;
    //    $scope.Model.TotalUnBudgeted = 0;

    //    for (var i = 0; i < $scope.Model.MSRDetail.length; i++) {

    //        if ($scope.Model.MSRDetail[i].BudgetLabel != undefined && $scope.Model.MSRDetail[i].BudgetLabel != '') {

    //            var Qty = $scope.Model.MSRDetail[i].Qty;
    //            var Cost = $scope.Model.MSRDetail[i].Cost;
    //            var StockOnHand = $scope.Model.MSRDetail[i].StockOnHand;

    //            var QtyBudget = $scope.Model.MSRDetail[i].QtyBudget;
    //            var QtyBudgetHistory = $scope.Model.MSRDetail[i].QtyBudgetHistory;




    //            if ($scope.Model.MSRDetail[i].BudgetLabel === 'InStock') {
    //                $scope.Model.TotalInStock += (Qty * Cost);
    //            }

    //            if ($scope.Model.MSRDetail[i].BudgetLabel === 'Budgeted') {

    //                if (StockOnHand > 0) {
    //                    $scope.Model.TotalInStock += (StockOnHand * Cost);
    //                }

    //                $scope.Model.TotalBudgeted += ((Qty - StockOnHand) * Cost);
    //            }

    //            if ($scope.Model.MSRDetail[i].BudgetLabel === 'UnBudgeted') {
    //                if (StockOnHand > 0) {
    //                    $scope.Model.TotalInStock += (StockOnHand * Cost);
    //                }

    //                var qtyBudgeted = QtyBudget - QtyBudgetHistory;
    //                if (qtyBudgeted > 0) {
    //                    $scope.Model.TotalBudgeted += (qtyBudgeted * Cost);
    //                }

    //                $scope.Model.TotalUnBudgeted += ((Qty - qtyBudgeted) * Cost);
    //            }


    //        }

    //    }
    //    $('#/*BudgetConfirmation*/').modal('toggle');
    //}


    $scope.lookupSearchWorkOrder = function () {
        $('#workorder-lookup').modal('toggle');
    }

    var searchLookupWorkOrder = function () {

        var elementBlock = '#workorder-lookup .modal-body';
        BlockElement(elementBlock);

        $http({
            method: 'POST',
            url: URI_SEARCH_WORKORDER,
            data: $scope.FormLookupWorkOrder
        }).then(function successCallback(response) {

            $scope.FormLookupWorkOrder = response.data;
            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });

    }

    $scope.btnSearchLookupWorkOrder = function () {
        $scope.FormLookupWorkOrder.CurrentPage = 1;
        searchLookupWorkOrder();
    }

    $scope.pageChangedLookupWorkOrder = function () {
        searchLookupWorkOrder();
    };



    $scope.btnSelectWorkOrder = function (item) {
        var elementBlock = '#workorder-lookup .modal-body';
        BlockElement(elementBlock);

        var uri = URI_GET_WORKORDER + "?WorkOrderNo=" + item;

        $http({
            method: 'GET',
            url: uri
        }).then(function successCallback(response) {

            if (response.data != null) {
                $scope.IsWorkOrder = true;
                IncludeDepartmentMaintenance();
                $scope.Model = response.data;

                if ($scope.Model.MSRType == MSRTypeInventory) {
                    $scope.IsInventory = true;
                } else {
                    $scope.IsInventory = false;
                }

                if (!$scope.Model.LeadTime.IsFreeNumber) {
                    $scope.Model.LeadTimeNumber = $scope.Model.LeadTime.MaxNumber;
                }
            }

            unBlockElement(elementBlock);
            $('#workorder-lookup').modal('toggle');
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });
    }

    $scope.removeWorkOrder = function () {
        $scope.Model = objModel();
        $scope.IsWorkOrder = false;
        $scope.listCostCenter = [];
        resetListItemEmpty();
        IncludeDepartmentMaintenance();
    }

    $scope.ResetMSR = function () {
        $scope.Model = objModel();
    }

    $scope.CreateMSR = function () {

        // $('#BudgetConfirmation').modal('toggle');

        var elementBlock = '#MSR-form';
        BlockElement(elementBlock);

        $http({
            method: 'POST',
            url: URI_CREATE_MSR,
            data: $scope.Model
        }).then(function successCallback(response) {

            if (response.data.IsSuccess) {
                window.location = URI_MSR_LIST;
            } else {
                showAlert(response.data.Message);
            }

            unBlockElement(elementBlock);
        }, function errorCallback(response) {

            console.log(response);

            if (response != null && response.data != null && response.data.Message != null) {
                showAlert(response.data.Message);
            } else {
                bootbox.alert("Error In Application Process");
            }

            unBlockElement(elementBlock);
        });
    }
        
    $scope.ChangeLeadTime = function (action) {

        if (action == 'up') {
            if ($scope.Model.LeadTimeNumber < $scope.Model.LeadTime.MaxNumber) {
                $scope.Model.LeadTimeNumber = $scope.Model.LeadTimeNumber + 1;
            }
        } else {

            if ($scope.Model.LeadTimeNumber > $scope.Model.LeadTime.MinNumber) {
                $scope.Model.LeadTimeNumber = $scope.Model.LeadTimeNumber - 1;
            }
        }
    }

    $scope.checkLeadTime = function () {
        if ($scope.Model.LeadTimeNumber > $scope.Model.LeadTime.MaxNumber) {
            $scope.Model.LeadTimeNumber = $scope.Model.LeadTime.MaxNumber;
        }

        if ($scope.Model.LeadTimeNumber < $scope.Model.LeadTime.MinNumber) {
            $scope.Model.LeadTimeNumber = $scope.Model.LeadTime.MinNumber;
        }

    }

    if ($scope.Model.ID != GuidEmpty) {
        $scope.ChangeDepartment();

        if ($scope.Model.WorkOrderNo != '') {
            $scope.IsWorkOrder = true;
        }

        IncludeDepartmentMaintenance();
    } else {
        resetListItemEmpty();
    }




    $scope.lookupCostCodeSearch = function (idItemLookup) {
        if ($scope.Model !== null && $scope.Model.DepartmentId !== undefined && $scope.Model.DepartmentId !== null && $scope.Model.DepartmentId !== GUID_EMPTY) {
            $scope.TargetMSRDetail = idItemLookup;
            resetLookupCostCodeSearch();
            $('#costcode-lookup').modal('toggle');
        } else {
            alert('Department must be selected');
        }
    }


    var resetLookupCostCodeSearch = function () {
        $scope.FormLookupCostCode = objFormLookupCostCode();
        var itemModel = { Text : "", Value : ""};
        $scope.FormLookupCostCode.ListDataGeneral = [];
        for (var i = 0; i < 5; i++) {
            $scope.FormLookupCostCode.ListDataGeneral.push(itemModel);
        }
    }


    var SearchLookupCostCode = function () {
        var elementBlock = '#costcode-lookup .modal-body';
        BlockElement(elementBlock);

        $scope.FormLookupCostCode.MoreCriteria = $scope.Model.DepartmentId;
        $scope.FormLookupCostCode.ListDataGeneral = [];

        $scope.FormLookupCostCode.IsSearchFilter = false;

        if (!$scope.IsInventory) {
            $scope.FormLookupCostCode.IsSearchFilter = true;
        }

        $http({
            method: 'POST',
            url: URI_SEARCH_COSTCODE,
            data: $scope.FormLookupCostCode
        }).then(function successCallback(response) {

            $scope.FormLookupCostCode = response.data;
            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });
    }

    $scope.btnSearchLookupCostCode = function () {
        $scope.FormLookupCostCode.CurrentPage = 1;
        SearchLookupCostCode();
    }

    $scope.pageChangedLookupCostCode = function () {
        SearchLookupCostCode();
    };


    $scope.btnSelectCostCode = function (item) {
        for (var i = 0; i < $scope.Model.MSRDetail.length; i++) {
            if ($scope.Model.MSRDetail[i].IdLookup == $scope.TargetMSRDetail) {
                $scope.Model.MSRDetail[i].CostCode = item.Value;
                $scope.Model.MSRDetail[i].CostCodeLabel = item.Value + " - " + item.Text;
            }
        }

        $('#costcode-lookup').modal('toggle');
    }



});