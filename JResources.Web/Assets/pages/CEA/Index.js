﻿var app = angular.module('JResourcesApp', ['ngMessages', 'ui.bootstrap', '720kb.tooltips']);
app.controller('JResourcesController', function ($scope, $http) {
    $scope.Model = objModel();
    $scope.Modal = objMSRModal();
    $scope.MSRStatus = objMSRStatus();
    $scope.CompanyDepartments = objCompanyDepartments();
    $scope.DepartmentList = [];

    $scope.SetListDepartment = function () {
        $scope.DepartmentList = [];
        $scope.DepartmentList.push({ ID: GUID_EMPTY, DepartmentName: "SELECT ALL DEPARTMENT" });

        for (var i = 0; i < $scope.CompanyDepartments.length; i++) {
            if ($scope.CompanyDepartments[i].ID == $scope.Model.CompanyId) {
                for (var ii = 0; ii < $scope.CompanyDepartments[i].Departments.length; ii++) {
                    $scope.DepartmentList.push(
                        $scope.CompanyDepartments[i].Departments[ii]
                    );
                }
            }
        }
    }

    var SearchCEA = function () {
        var elementBlock = '#MSR-table';
        BlockElement(elementBlock);
        $scope.Model.ListData = [];
        $scope.Model.IsRecentActivity = true;

        $http({
            method: 'POST',
            url: URI_SEARCH_CEA,
            data: $scope.Model
        }).then(function successCallback(response) {

            $scope.Model = response.data;
            unBlockElement(elementBlock);
            $('.tooltips').tooltip();
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });
    }

    $scope.SearchCEA = function () {
        $scope.Model.CurrentPage = 1;
        SearchCEA();
    }

    $scope.ResetCEA = function () {
        $scope.Model = objModel();
        SearchCEA();
    }

    $scope.pageChangedMSRList = function () {
        SearchCEA();
    };

    $scope.ResetCEA();

    $scope.MSRModal = function (Id) {
        $('#MSR-modal').modal('toggle');

        var elementBlock = '#MSR-modal .portlet';
        BlockElement(elementBlock);
        $scope.Modal = objMSRModal();
        $http({
            method: 'GET',
            url: URI_GET_MSR_MODAL + '/' + Id
        }).then(function successCallback(response) {

            $scope.Modal = response.data;
            $scope.Modal.IsNeedActionRequest = false;
            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });

    }

    $scope.showEdit = function (statusCode) {
        if (statusCode == REQUEST_STATUS.NEW || statusCode == REQUEST_STATUS.REVISION) {
            return true;
        }

        return false;
    }

    $scope.showView = function (statusCode) {
        if (statusCode == REQUEST_STATUS.NEW || statusCode == REQUEST_STATUS.REVISION) {
            return false;
        }

        return true;
    }

    $scope.Unload = function (Id, MaintenanceCode) {

        var messageDialog = "By confirming, you've confirm that number of items and qty are correct";
        if (MaintenanceCode == "P01" || MaintenanceCode == "P06" || MaintenanceCode == "P08") {
            messageDialog = "By confirming, you've done dismantling and confirmed number of items and qty are correct";
        }

        bootbox.confirm({
            message: messageDialog,
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {


                if (result) {
                    var elementBlock = '.page-content';
                    BlockElement(elementBlock);
                    $scope.Modal = objMSRModal();
                    $http({
                        method: 'GET',
                        url: URI_MSR_DISMANTLE + '/' + Id
                    }).then(function successCallback(response) {

                        if (response.data.IsSuccess) {

                            showNotification(response.data.Message);
                            $scope.ResetCEA();

                        } else {
                            showAlert(response.data.Message);
                        }

                        unBlockElement(elementBlock);
                    }, function errorCallback(response) {
                        console.log(response);
                        bootbox.alert(response.data);
                        unBlockElement(elementBlock);
                    });

                    console.log('This was logged in the callback: ' + result);
                }


            }
        });

    }

    $scope.MSRPrintOut = function (_id) {
        var url = URL_PRINT_FORM + '?type=MSR_PRINT_OUT&id=' + _id;
        window.open(url, '_blank');
        window.focus();
    }

    $scope.RequestRequisition = function (Id) {

        bootbox.confirm({
            message: "By confirming, you've confirm that number of items can be post to purchase",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {


                if (result) {
                    var elementBlock = '.page-content';
                    BlockElement(elementBlock);
                    $scope.Modal = objMSRModal();
                    $http({
                        method: 'GET',
                        url: URI_MSR_REQUISITION + '/' + Id
                    }).then(function successCallback(response) {

                        if (response.data.IsSuccess) {
                            showNotification(response.data.Message);
                            SearchCEA();

                        } else {
                            showAlert(response.data.Message);
                        }

                        unBlockElement(elementBlock);
                    }, function errorCallback(response) {
                        console.log(response);
                        bootbox.alert(response.data);
                        unBlockElement(elementBlock);
                    });

                    console.log('This was logged in the callback: ' + result);
                }


            }
        });

    }


});