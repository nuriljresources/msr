﻿var app = angular.module('JResourcesApp', ['ngMessages', 'ui.bootstrap']);
app.controller('JResourcesController', function ($scope, $http) {

    $scope.amount = '25.00';

    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;

            var sanitize = function (s) {
                return s.replace(/[^\d|\-+|\.+]/g, '');
            }

            var convert = function () {
                var plain = sanitize(ctrl.$viewValue);
                ctrl.$setViewValue(currencyFilter(plain));
                ctrl.$render();
            };

            elem.on('blur', convert);

            ctrl.$formatters.push(function (a) {
                return currencyFilter(a);
            });

            ctrl.$parsers.push(function (a) {
                return sanitize(a);
            });
        }
    };
});