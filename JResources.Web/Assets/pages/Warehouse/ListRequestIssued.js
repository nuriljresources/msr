﻿
var app = angular.module('JResourcesApp', ['ngMessages', 'ui.bootstrap']);
app.controller('JResourcesController', function ($scope, $http) {

    $scope.Model = objModel();
    $scope.CompanyDepartments = objCompanyDepartments();
    $scope.DepartmentList = [];

    $scope.SetListDepartment = function () {
        $scope.DepartmentList = [];
        $scope.DepartmentList.push({ ID: GUID_EMPTY, DepartmentName: "SELECT ALL DEPARTMENT" });

        for (var i = 0; i < $scope.CompanyDepartments.length; i++) {
            if ($scope.CompanyDepartments[i].ID == $scope.Model.CompanyId) {
                for (var ii = 0; ii < $scope.CompanyDepartments[i].Departments.length; ii++) {
                    $scope.DepartmentList.push(
                        $scope.CompanyDepartments[i].Departments[ii]
                    );
                }
            }
        }
    }

    var SearchMSR = function () {
        var elementBlock = '#MSR-table';
        BlockElement(elementBlock);
        $scope.Model.ListData = [];

        $http({
            method: 'POST',
            url: URI_SEARCH_MSRISSUED,
            data: $scope.Model
        }).then(function successCallback(response) {

            $scope.Model = response.data;
            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });
    }

    $scope.SearchMSR = function () {
        $scope.Model.CurrentPage = 1;
        SearchMSR();
    }

    $scope.ResetMSR = function () {
        $scope.Model = objModel();
        SearchMSR();
    }

    $scope.pageChangedMSRList = function () {
        SearchMSR();
    };

    $scope.ResetMSR();

    $scope.MSRIssuedModal = function (Id) {
        $('#MSR-issued-modal').modal('toggle');

        var elementBlock = '#MSR-issued-modal .portlet';
        BlockElement(elementBlock);

        $http({
            method: 'GET',
            url: URI_GET_MSRISSUED + '/' + Id
        }).then(function successCallback(response) {

            $scope.Modal = response.data;
            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });
    }

    $scope.ReverseTransfer = function () {

        var elementBlock = '#MSR-issued-modal .portlet';
        BlockElement(elementBlock);

        $http({
            method: 'POST',
            url: URI_REVERSE_TRANSFER,
            data: $scope.Modal
        }).then(function successCallback(response) {
            if (response.data.IsSuccess) {
                window.location = URI_LIST_ISSUED;
            } else {
                showAlert(response.data.Message);
            }

            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert("Error In Application");
            unBlockElement(elementBlock);
        });
    }

    $scope.PrintGoodIssued = function () {
        var url = URL_PRINT_FORM + '?type=GOOD_ISSUED&id=' + $scope.Modal.ID;
        window.open(url, '_blank');
        window.focus();
    }
});
