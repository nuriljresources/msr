﻿var app = angular.module('JResourcesApp', ['ngMessages', 'ui.bootstrap']);
app.controller('JResourcesController', function ($scope, $http) {
    $scope.Model = objModel();
    $scope.Modal = objModalMSR();
    $scope.IsEnableSubmit = true;
    $scope.IsEnablePickingList = false;
    $scope.FileUploads = [];

    if (IssuedType == REQUEST_STATUS.CANCEL) {
        $scope.IsEnableSubmit = false;
    }

    $scope.ResetMSR = function () {
        $scope.Model = objModel();
    }

    $scope.removeWorkOrder = function () {
        $scope.Model = objModel();
    }

    $scope.LookupSearchMSR = function () {
        $('#MSR-lookup').modal('toggle');
    }

    var SearchMSR = function () {
        var elementBlock = '#MSR-lookup';
        BlockElement(elementBlock);
        $scope.Modal.ListData = [];


        $http({
            method: 'POST',
            url: URI_SEARCH_MSR,
            data: $scope.Modal
        }).then(function successCallback(response) {

            $scope.Modal = response.data;
            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });
    }

    $scope.SearchMSR = function () {
        $scope.Modal.CurrentPage = 1;
        SearchMSR();
    }

    $scope.pageChangedLookupItem = function () {
        SearchMSR();
    }

    function LoadFileUpload(MSRNo) {

        var elementBlock = '#form-file-upload';
        BlockElement(elementBlock);

        $http({
            method: 'GET',
            url: URI_ASSET_API + "/api/file?DocumentNo=" + MSRNo,
        }).then(function successCallback(response) {

            if (response.data != null) {
                $scope.FileUploads = response.data;
            }

            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert(response.data.Message);
            unBlockElement(elementBlock);
        });
    }
    

    $scope.SelectMSR = function (Id) {
        var elementBlock = '#MSR-lookup';
        BlockElement(elementBlock);
        $scope.Modal.ListData = [];
        $scope.IsEnableSubmit = true;

        $http({
            method: 'GET',
            url: URI_GET_MSR_DATA.replace(GUID_EMPTY, Id)
        }).then(function successCallback(response) {

            $scope.Model = response.data;
            $scope.Model.IssuedType = IssuedType;

            $scope.IsEnablePickingList = false;
            if (IssuedType == REQUEST_STATUS.ISSUED && !($scope.Model.MSR.WorkOrderNo)) {
                $scope.IsEnablePickingList = true;
            }

            if (IssuedType == REQUEST_STATUS.TRANSFER && $scope.Model.MSR.WorkOrderNo != '') {
                $scope.IsEnablePickingList = true;
            }

            if ($scope.Model.IssuedType != cancelLabel && $scope.Model.MSR.WorkOrderNo != "") {
                var IsValidWO = validStatus.indexOf($scope.Model.MSR.WorkOrderStatus);
                console.log(IsValidWO);

                if (IsValidWO >= 0) {
                    $scope.IsEnableSubmit = false;
                }
            }


            LoadFileUpload($scope.Model.MSRNo);

            $('#MSR-lookup').modal('toggle');
            unBlockElement(elementBlock);
        }, function errorCallback(response) {
            console.log(response);
            bootbox.alert("Error Application");
            unBlockElement(elementBlock);
        });
    }


    $scope.lookupSearchLocation = function (item) {
        $scope.ItemLocationSelect = item;
        $('#location-lookup').modal('toggle');
    }

    $scope.btnSelectLocation = function (locationId) {

        for (var i = 0; i < $scope.Model.MSRIssuedDetails.length; i++) {
            if ($scope.Model.MSRIssuedDetails[i].LineNo == $scope.ItemLocationSelect.LineNo) {
                var ItemLine = $scope.Model.MSRIssuedDetails[i];

                for (var ii = 0; ii < ItemLine.ItemLocations.length; ii++) {
                    if (ItemLine.ItemLocations[ii].Location == locationId) {
                        ItemLine.LocationFrom = ItemLine.ItemLocations[ii].Location;
                        ItemLine.StochOnHand = ItemLine.ItemLocations[ii].QtyOnHand;
                    }
                }

            }
        }

        $('#location-lookup').modal('toggle');
    }

    $scope.resetItemMSRDetail = function (lineNo) {
        alert('remove');
    }

    $scope.CreateIssued = function () {
        var elementBlock = '#MSR-issued-form';
        BlockElement(elementBlock);

        $http({
            method: 'POST',
            url: URI_CREATE_MSR_ISSUED,
            data: $scope.Model
        }).then(function successCallback(response) {

            if (response.data.IsSuccess) {
                window.location = URI_MSR_LIST;
            } else {
                showAlert(response.data.Message);
            }

            unBlockElement(elementBlock);
        }, function errorCallback(response) {

            console.log(response);
            if (response != null && response.data != null) {
                bootbox.alert(response.data);
            } else {
                bootbox.alert("Error In Application Process");
            }
            unBlockElement(elementBlock);
        });
    }


    $scope.PrintPickingList = function () {
        var url = URL_PRINT_FORM + '?type=PICKING_SLIP&id=' + $scope.Model.MSRId + '&IssuedType=' + IssuedType;
        window.open(url, '_blank');
        window.focus();
    }


    $scope.GridCssRow = function (Isfulfilled, IsRemove) {

        var addDisabled = ' ';
        if (Isfulfilled) {
            addDisabled = ' bg-grey';
        } else {
            if (IsRemove) {
                addDisabled = ' bg-grey';
            }
        }

        return 'form-control' + addDisabled;
    }

    $scope.GridCssRowInput = function (Isfulfilled, IsRemove) {

        var addDisabled = ' bg-grey-cararra';
        if (Isfulfilled) {
            addDisabled = ' disabled bg-grey-cararra';
        } else {
            if (IsRemove) {
                addDisabled = ' disabled bg-grey-cararra';
            }
        }

        return 'form-control' + addDisabled;
    }

});