﻿

function BlockUI() {
    Metronic.blockUI({
        boxed: true
    });
}

function unBlockUI() {
    Metronic.unblockUI();
}

function BlockElement(Id) {
    Metronic.blockUI({
        target: Id,
        boxed: true,
        animate: true
    });
}

function BlockElementAccpac(Id) {
    Metronic.blockUI({
        target: Id,
        boxed: true,
        message: 'Post To Accpac, <br />The process takes a while'
    });
}



function unBlockElement(Id) {

    Metronic.unblockUI(Id);

}

function encodeQueryData(data) {
    var ret = [];
    for (var d in data) {

        //console.log(d + '=' + data[d]);

        if (data[d] != undefined && data[d] != null && data[d] != '') {
            ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
        }

    }

    if (ret.length > 0) {
        return ret.join('&');
    } else {
        return '';
    }
}


function getAjaxToDropDown(url, comboBox, fieldValue, fieldLabel, valueSelected) {

    $.getJSON(url, function (data, status) {
        var optionStr = [];

        if (status === "success") {
            
            $.each(data, function (key, value) {
                optionStr.push('<option value="' + value[fieldValue] + '">' + value[fieldLabel] + '</option>');
            });

        }

        $(comboBox)
        .find('option')
        .remove()
        .end()
        .append(optionStr.join(''))
        .val(valueSelected);

    });

}

function setDataToDropDown(data, comboBox, fieldValue, fieldLabel, valueSelected) {

    var optionStr = [];

    $.each(data, function (key, value) {
        optionStr.push('<option value="' + value[fieldValue] + '">' + value[fieldLabel] + '</option>');
    });

    $(comboBox)
    .find('option')
    .remove()
    .end()
    .append(optionStr.join(''))
    .val(valueSelected);

}


function showAlert(message) {
    var strDialog = [];
    strDialog.push('<div class="portlet">');
    strDialog.push('<div class="portlet-title"><div class="caption text-danger">Error Message</div></div>');
    strDialog.push('<div class="portlet-body">');
    //strDialog.push('<ul class="">');

    var listMessage = message.split('\r\n');
    for (var i = 0; i < listMessage.length; i++) {
        if (listMessage[i] != null && listMessage[i] != '') {
            //strDialog.push('<li class="">');
            //strDialog.push(listMessage[i]);
            strDialog.push('<div class="alert alert-danger" style="padding: 5px; margin-bottom: 5px;">' + listMessage[i] + '</div>');
            

            //strDialog.push('</li>');
        }
    }
    //strDialog.push('</ul>');
    strDialog.push('</div>');
    strDialog.push('</div>');

    bootbox.alert(strDialog.join(''));

    //bootbox.alert('</div><ul class="list-group">
    //    <li class="list-group-item">
    //         Cras justo odio
    //    </li>
    //    <li class="list-group-item">
    //         Dapibus ac facilisis in
    //    </li>
    //    <li class="list-group-item">
    //         Morbi leo risus
    //    </li>
    //    <li class="list-group-item">
    //         Porta ac consectetur ac
    //    </li>
    //    <li class="list-group-item">
    //         Vestibulum at eros
    //    </li>
    //</ul>' + response.data.Message);
}

function showNotification(message) {
    var strDialog = [];
    strDialog.push('<div class="portlet">');
    strDialog.push('<div class="portlet-title"><div class="caption text-success">Notification Message</div></div>');
    strDialog.push('<div class="portlet-body">');
    //strDialog.push('<ul class="">');

    var listMessage = message.split('\r\n');
    for (var i = 0; i < listMessage.length; i++) {
        if (listMessage[i] != null && listMessage[i] != '') {
            //strDialog.push('<li class="">');
            //strDialog.push(listMessage[i]);
            strDialog.push('<div class="success alert-success" style="padding: 5px; margin-bottom: 5px;">' + listMessage[i] + '</div>');


            //strDialog.push('</li>');
        }
    }
    //strDialog.push('</ul>');
    strDialog.push('</div>');
    strDialog.push('</div>');

    bootbox.alert(strDialog.join(''));

    //bootbox.alert('</div><ul class="list-group">
    //    <li class="list-group-item">
    //         Cras justo odio
    //    </li>
    //    <li class="list-group-item">
    //         Dapibus ac facilisis in
    //    </li>
    //    <li class="list-group-item">
    //         Morbi leo risus
    //    </li>
    //    <li class="list-group-item">
    //         Porta ac consectetur ac
    //    </li>
    //    <li class="list-group-item">
    //         Vestibulum at eros
    //    </li>
    //</ul>' + response.data.Message);
}


$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});