﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JResources.Data;
using JResources.Data.Model;
using JResources.BusinessLogic;
using JResources.Common.Enum;
using Microsoft.Reporting.WebForms;
using JResources.Common;

namespace JResources.Web.Reports
{
    public partial class PrintForm : System.Web.UI.Page
    {
        public class FORM_TYPE_LIST
        {
            public const string PICKING_SLIP = "Reports/PickingSlipForm.rdlc";
            public const string GOODS_ISSUE = "Reports/GoodsIssueForm.rdlc";
            public const string MSR_PRINTOUT = "Reports/MSRPrint.rdlc";
            public const string MSR_STATUS = "Reports/ReportMSRStatus.rdlc";
        }

        public string FORM_TYPE { get; set; }
        public Guid DocumentId { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetParameter();

                switch (FORM_TYPE)
                {
                    case PRINT_FORM_TYPE.PICKING_SLIP :
                        PrintPickingSlip();
                        break;

                    case PRINT_FORM_TYPE.GOOD_ISSUED:
                        PrintGoodIssued();
                        break;

                    case PRINT_FORM_TYPE.MSR_PRINT_OUT:
                        PrintOutMSR();
                        break;

                    case PRINT_FORM_TYPE.MSR_STATUS:
                        MSRStatus();
                        break;

                    case PRINT_FORM_TYPE.IC_LOCATION:
                        ICLocation();
                        break;

                    case PRINT_FORM_TYPE.CHECK_MSR_ACCPAC:
                        CheckMSRAccpac();
                        break;

                    case PRINT_FORM_TYPE.PO_RECONCILE:
                        POReconcile();
                        break;

                    default:
                        Response.Redirect(string.Format("{0}/Error/NotFound", SiteSettings.BASE_URL));
                        break;
                }
            }
        }

        protected void SetParameter()
        {
            if (Request.QueryString != null && Request.QueryString.Count > 0)
            {
                if (Request.QueryString["type"] != null && !string.IsNullOrEmpty(Request.QueryString["type"]))
                {
                    FORM_TYPE = Request.QueryString["type"];
                }

                Guid _documentId = Guid.Empty;
                if (Request.QueryString["id"] != null && !string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    string id = Request.QueryString["id"];
                    Guid.TryParse(id, out _documentId);
                    DocumentId = _documentId;
                }
            }
        }

        protected void PrintPickingSlip()
        {
            MSRIssuedModel model = WarehouseLogic.GetDataByMSRId(DocumentId, RequestStatus.TRANSFER);
            if (model.MSRId != Guid.Empty)
            {
                DataSetReport.PickingListHeaderDataTable TableHeader = new DataSetReport.PickingListHeaderDataTable();
                TableHeader.AddPickingListHeaderRow(
                    model.MSR.DocumentNo,
                    model.CostCenter,
                    model.MSR.DateCreatedStr,
                    model.MSR.Reference,
                    model.MSR.StatusRequestLabel,
                    model.MSR.EquipmentNo,
                    model.MSR.Reason,
                    (model.MSR.Requestor != null ? model.MSR.Requestor.FullName :""));


                DataSetReport.PickingListDetailDataTable TableDetail = new DataSetReport.PickingListDetailDataTable();
                var details = model.MSRIssuedDetails.Where(x => !x.Isfulfilled).ToList();
                foreach (var item in model.MSRIssuedDetails)
                {
                    int i = 1;
                    foreach (var itemLoc in item.ItemLocations)
                    {
                        TableDetail.AddPickingListDetailRow(
                            item.ItemNo,
                            item.Description,
                            item.QtyRequest.ToString(),
                            item.UOM,
                            itemLoc.QtyOnHand.ToString("#0.####"),
                            item.QtyIssuedOutstanding.ToString("#0.####"),
                            itemLoc.Location,
                            "BIN " + i.ToString());
                        i++;
                    }


                }

                RptViewer.LocalReport.ReportPath = FORM_TYPE_LIST.PICKING_SLIP;
                RptViewer.LocalReport.DataSources.Clear();

                RptViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSetPickingListHeader", TableHeader as object));
                RptViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSetPickingListDetail", TableDetail as object));

                RptViewer.DataBind();
                RptViewer.LocalReport.Refresh();
            }
            else
            {
                Response.Redirect(string.Format("{0}/Error/NotFound", SiteSettings.BASE_URL));
            }
        }

        protected void PrintGoodIssued()
        {
            MSRIssuedModel model = WarehouseLogic.GetMSRIssuedId(DocumentId);

            if (model.ID != Guid.Empty)
            {
                DataSetReport.GoodsIssueHeaderDataTable TableHeader = new DataSetReport.GoodsIssueHeaderDataTable();
                TableHeader.AddGoodsIssueHeaderRow(
                    model.CompanyName,
                    model.MSRNo,
                    (string.IsNullOrEmpty(model.WorkOrderNo) ? "" : model.WorkOrderNo),
                    model.MSR.EquipmentNo,
                    model.Remarks,
                    (model.Creator != null ? model.Creator.FullName : ""),
                    model.MSRIssuedNo,
                    model.MSRIssuedDate.ToString(Constant.FORMAT_DATE_JRESOURCES_TEXT),
                    model.MSR.ApprovedDate.ToString(Constant.FORMAT_DATE_JRESOURCES_TEXT),
                    (model.MSR.ApprovedBy != null ? model.MSR.ApprovedBy.FullName : ""),
                    (model.MSR.Requestor != null ? model.MSR.Requestor.FullName : ""),
                    model.MSR.Requestor.FullName,
                    model.MSR.DepartmentName,
                    model.MSR.Requestor.NIKSite,
                    model.MSR.Reason);

                DataSetReport.GoodsIssueDetailDataTable TableDetail = new DataSetReport.GoodsIssueDetailDataTable();
                foreach (var item in model.MSRIssuedDetails)
                {
                    TableDetail.AddGoodsIssueDetailRow(
                        item.ItemNo,
                        item.Description,
                        item.QtyDelivered.ToString("#0.####"),
                        item.UOM,
                        item.Location,
                        (string.IsNullOrEmpty(item.PickingSeq) ? "" : item.PickingSeq),
                        item.StockOnHand.ToString("#0.####"));
                }


                RptViewer.LocalReport.ReportPath = FORM_TYPE_LIST.GOODS_ISSUE;
                RptViewer.LocalReport.DataSources.Clear();

                RptViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSetGoodsIssueHeader", TableHeader as object));
                RptViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSetGoodsIssueDetail", TableDetail as object));

                RptViewer.DataBind();
                RptViewer.LocalReport.Refresh();
            }
            else
            {
                Response.Redirect(string.Format("{0}/Error/NotFound", SiteSettings.BASE_URL));
            }
        }

        protected void PrintOutMSR()
        {
            MSRHistoryModel model = MSRLogic.GetMSRHistoryModel(DocumentId);

            if (model.ID != Guid.Empty)
            {
                DataSetReport.MSRPrintOutHeaderDataTable TableHeader = new DataSetReport.MSRPrintOutHeaderDataTable();
                TableHeader.AddMSRPrintOutHeaderRow(
                    model.CompanyName,
                    model.DateCreatedStr,
                    model.ExpectedDateStr,
                    model.StatusRequestLabel,
                    model.WorkOrderNo,
                    model.Reason,
                    model.DepartmentName,
                    model.CostCenter,
                    model.MaintenanceCode,
                    model.LeadTimeNumber.ToString(),
                    model.EquipmentNo,
                    (model.Requestor != null ? model.Requestor.FullName : ""),
                    (model.Manager != null ? model.Manager.FullName : ""),
                    (model.GeneralManagerSite != null ? model.GeneralManagerSite.FullName : ""),
                    "",
                    model.DocumentNo
                    );

                DataSetReport.MSRPrintOutDetailDataTable TableDetail = new DataSetReport.MSRPrintOutDetailDataTable();
                foreach (var item in model.MSRDetail)
                {
                    var itemMovement = model.MSRLast.MSRIssuedDetails.FirstOrDefault(x => x.LineNo == item.LineNo);
                    decimal qtyTransfer = 0;
                    decimal qtyPurchase = 0;
                    decimal qtyIssued = 0;
                    decimal qtyCancel = 0;
                    if (itemMovement != null)
                    {
                        qtyTransfer = itemMovement.QtyTransfered;
                        qtyPurchase = itemMovement.QtyPurchase;
                        qtyIssued = itemMovement.QtyIssued;
                        qtyCancel = itemMovement.QtyCancel;
                    }

                    TableDetail.AddMSRPrintOutDetailRow(

                        item.ItemNo,
                        item.Description,
                        item.Qty.ToString("#0.####"),
                        item.UOM,
                        item.Location,
                        item.StockOnHand.ToString("#0.####"),
                        qtyTransfer.ToString("#0.####"),
                        qtyPurchase.ToString("#0.####"),
                        qtyIssued.ToString("#0.####"),
                        qtyCancel.ToString("#0.####")
                        );
                }


                RptViewer.LocalReport.ReportPath = FORM_TYPE_LIST.MSR_PRINTOUT;
                RptViewer.LocalReport.DataSources.Clear();

                RptViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSetMSRPrintOutHeader", TableHeader as object));
                RptViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSetMSRPrintOutDetail", TableDetail as object));

                RptViewer.DataBind();
                RptViewer.LocalReport.Refresh();
            }
            else
            {
                Response.Redirect(string.Format("{0}/Error/NotFound", SiteSettings.BASE_URL));
            }
        }

        protected void MSRStatus()
        {
            RptViewer.ProcessingMode = ProcessingMode.Remote;
            ServerReport serverReport = RptViewer.ServerReport;

            // Set the report server URL and report path  
            serverReport.ReportServerUrl =
                new Uri(SiteSettings.REPORT_SERVER_URL);
            serverReport.ReportPath = "/JResources.Report/ReportMSRStatus";

            // Create the sales order number report parameter  
            ReportParameter paramReport = new ReportParameter();
            paramReport.Name = "CompanyCode";
            paramReport.Values.Add(CurrentUser.COMPANY);

            // Set the report parameters for the report  
            RptViewer.ServerReport.SetParameters(new ReportParameter[] { paramReport });

            string css = @"<link href=""" + this.ResolveUrl("~/Assets/global/css/StyleSheetReportFull.css") +             @""" type=""text/css"" rel=""stylesheet"" />";

            ScriptManager.RegisterClientScriptBlock(this.Page, GetType(), "_calcss", css, false);
        }


        protected void ICLocation()
        {
            RptViewer.ProcessingMode = ProcessingMode.Remote;
            ServerReport serverReport = RptViewer.ServerReport;

            // Set the report server URL and report path  
            serverReport.ReportServerUrl =
                new Uri(SiteSettings.REPORT_SERVER_URL);
            serverReport.ReportPath = "/JResources.Report/ICLocation";

            // Create the sales order number report parameter  
            ReportParameter paramReport = new ReportParameter();
            paramReport.Name = "CompanyCode";
            paramReport.Values.Add(CurrentUser.COMPANY);

            ReportParameter paramReportUserLogon = new ReportParameter();
            paramReportUserLogon.Name = "UserLogon";
            paramReportUserLogon.Values.Add(CurrentUser.FULLNAME);

            ReportParameter paramReportTimeLogon = new ReportParameter();
            paramReportTimeLogon.Name = "TimeLogon";
            paramReportTimeLogon.Values.Add(DateTime.Now.JResourcesDateTime());

            // Set the report parameters for the report  
            RptViewer.ServerReport.SetParameters(new ReportParameter[] { paramReport, paramReportUserLogon, paramReportTimeLogon });

            string css = @"<link href=""" + this.ResolveUrl("~/Assets/global/css/StyleSheetReportFull.css") + @""" type=""text/css"" rel=""stylesheet"" />";

            ScriptManager.RegisterClientScriptBlock(this.Page, GetType(), "_calcss", css, false);
        }

        protected void POReconcile()
        {
            RptViewer.ProcessingMode = ProcessingMode.Remote;
            ServerReport serverReport = RptViewer.ServerReport;

            // Set the report server URL and report path  
            serverReport.ReportServerUrl =
                new Uri(SiteSettings.REPORT_SERVER_URL);
            serverReport.ReportPath = "/JResources.Report/ReportPOReconcile";

            // Create the sales order number report parameter  
            ReportParameter paramReport = new ReportParameter();
            paramReport.Name = "CompanyCode";
            paramReport.Values.Add(CurrentUser.COMPANY);

            // Set the report parameters for the report  
            RptViewer.ServerReport.SetParameters(new ReportParameter[] { paramReport });

            string css = @"<link href=""" + this.ResolveUrl("~/Assets/global/css/StyleSheetReportFull.css") + @""" type=""text/css"" rel=""stylesheet"" />";

            ScriptManager.RegisterClientScriptBlock(this.Page, GetType(), "_calcss", css, false);
        }

        protected void CheckMSRAccpac()
        {
            RptViewer.ProcessingMode = ProcessingMode.Remote;
            ServerReport serverReport = RptViewer.ServerReport;

            // Set the report server URL and report path  
            serverReport.ReportServerUrl =
                new Uri(SiteSettings.REPORT_SERVER_URL);
            serverReport.ReportPath = "/JResources.Report/ReportComparisonMSRAccpac";

            // Create the sales order number report parameter  
            ReportParameter paramReport = new ReportParameter();
            paramReport.Name = "CompanyID";

            Company CurrentCompany = Company.GetByCode(CurrentUser.COMPANY);
            paramReport.Values.Add(CurrentCompany.ID.ToString());

            // Set the report parameters for the report  
            RptViewer.ServerReport.SetParameters(new ReportParameter[] { paramReport });

            string css = @"<link href=""" + this.ResolveUrl("~/Assets/global/css/StyleSheetReportFull.css") + @""" type=""text/css"" rel=""stylesheet"" />";

            ScriptManager.RegisterClientScriptBlock(this.Page, GetType(), "_calcss", css, false);
        }

    }
}