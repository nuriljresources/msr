﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ViewMasterReport.Master" AutoEventWireup="true" CodeBehind="PrintForm.aspx.cs" Inherits="JResources.Web.Reports.PrintForm" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Form Page
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <rsweb:reportviewer id="RptViewer" runat="server" width="100%" height="29.7cm"></rsweb:reportviewer>
    </form>
</asp:Content>
