﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using JResources.Common;
using JResources.Data;
using JResources.Data.Model;
using Omu.ValueInjecter;

namespace JResources.BusinessLogic
{
    public class RoleLogic
    {
        public static RoleAccessUserModel DefaultAccess()
        {
            RoleAccessUserModel model = new RoleAccessUserModel();
            var role = RoleAccess.GetByCode(ROLE_CODE.USERS);
            model.ID = Guid.NewGuid();
            model.RoleAccessCode = role.RoleAccessCode;
            model.RoleAccessName = role.RoleAccessName;

            return model;
        }

        public static bool CheckRoleAccess(string NIKSite, string roleCode, Guid CompanyId, Guid DepartmentId)
        {
            var user = UserLogic.GetByNIK(NIKSite);
            string[] RoleCodes = new string[] { roleCode };
            var roleAccessList = RoleAccessUser.GetByDepartmentList(user.Username, RoleCodes);

            if (CompanyId != Guid.Empty)
            {
                var checkAccs = roleAccessList.Where(x => x.CompanyId == CompanyId);
                if (checkAccs != null)
                {
                    return true;
                }
            }
            else
            {
                var checkAccs = roleAccessList.Where(x => x.DepartmentId == DepartmentId);
                if (checkAccs != null)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool CheckRoleAccess(string NIKSite, string roleCode, Guid DepartmentId)
        {
            return CheckRoleAccess(NIKSite, roleCode, Guid.Empty, DepartmentId);
        }

        public static List<RoleAccessModel> GetAllRoleAccess(Guid UserId, bool ExcludeAdministrator = false)
        {
            List<RoleAccessModel> list = new List<RoleAccessModel>();
            var roleAccess = RoleAccess.GetAll().Where(x => !x.IsDeleted);

            var roleAccessUser = RoleAccessUser.GetByUserId(UserId)
                .Select(x => x.RoleAccessId)
                .Distinct()
                .ToList();

            if (ExcludeAdministrator)
            {
                var ROLE_ADMIN = RoleAccess.GetByCode(ROLE_CODE.ADMINISTRATOR);
                roleAccessUser = roleAccessUser.Where(x => x != ROLE_ADMIN.ID).ToList();
            }

            foreach (var item in roleAccess)
            {
                var checkIsExist = roleAccessUser.FirstOrDefault(x => x == item.ID);
                bool IsSelected = (checkIsExist != null && checkIsExist != Guid.Empty ? true : false);

                list.Add(new RoleAccessModel()
                {
                    ID = item.ID,
                    IsSelected = IsSelected,
                    RoleAccessCode = item.RoleAccessCode,
                    RoleAccessName = item.RoleAccessName
                });
            }

            return list;
        }

        public static List<UserModel> GetAllUserByRoleAndDepartmentId(string roleCode, Guid DepartmentId)
        {
            List<UserModel> list = new List<UserModel>();
            var userIdList = RoleAccessUser.GetAll().Where(x => x.RoleAccess.RoleAccessCode == roleCode
                && x.DepartmentId == DepartmentId).Select(x => x.UserId).ToList();

            if (userIdList != null && userIdList.Count > 0)
            {
                var users = UserLogin.GetAll().Where(x => userIdList.Contains(x.ID)).ToList();
                foreach (var item in users)
                {
                    UserModel userModel = new UserModel();
                    userModel.InjectFrom(item);
                    if (!string.IsNullOrEmpty(item.FullName) && !string.IsNullOrEmpty(item.Email))
                    {
                        userModel.FullName = item.FullName;
                        userModel.Email = item.Email;

                        list.Add(userModel);
                    }
                }
            }

            return list;
        }

        public static List<SelectListItem> GetListDepartmentUserByRole(string username, string companyCode, string[] roleAccessCode)
        {
            var list = new List<SelectListItem>();

            var user = UserLogin.GetByUsername(username);
            if (user != null && user.ID != Guid.Empty)
            {
                bool IsAdmin = UserLogic.IsAdministrator(user.ID, companyCode);

                if (IsAdmin)
                {
                    var depatments = Department.GetAll().Where(x => x.Company.CompanyCode == companyCode).ToList();
                    if (depatments != null && depatments.Count > 0)
                    {
                        foreach (var depatment in depatments)
                        {
                            SelectListItem item = new SelectListItem();
                            if (depatment.ID == user.DepartmentId)
                            {
                                item.Selected = true;
                            }

                            item.Value = depatment.ID.ToString().Trim();
                            item.Text = depatment.DepartmentName;
                            list.Add(item);
                        }
                    }
                }
                else
                {
                    DateTime dtNow = DateTime.Now;
                    List<RoleAccessUser> roleAccessUser = RoleAccessUser.GetByUserId(user.ID)
                        .Where(x => 
                            x.Company.CompanyCode == companyCode && 
                            roleAccessCode.Contains(x.RoleAccess.RoleAccessCode) && (x.DateFrom <= dtNow && x.DateTo >= dtNow)
                        ).ToList();

                    if (roleAccessUser != null && roleAccessUser.Count > 0)
                    {
                        var depatmentIds = roleAccessUser.Select(x => x.DepartmentId).Distinct().ToList();
                        var depatments = Department.GetAll().Where(x => depatmentIds.Contains(x.ID)).ToList();
                        if (depatments != null && depatments.Count > 0)
                        {
                            foreach (var depatment in depatments)
                            {
                                SelectListItem item = new SelectListItem();
                                if (depatment.ID == user.DepartmentId)
                                {
                                    item.Selected = true;
                                }

                                item.Value = depatment.ID.ToString().Trim();
                                item.Text = depatment.DepartmentName;
                                list.Add(item);
                            }
                        }
                    }
                }
            }

            return list;
        }

        public static List<CompanyModel> GetListCompanyAccess(string username, string companyCode, string[] roleAccessCode)
        {
            var list = new List<CompanyModel>();

            var user = UserLogin.GetByUsername(username);
            if (user != null && user.ID != Guid.Empty)
            {
                bool IsAdmin = UserLogic.IsAdministrator(user.ID, companyCode);

                if (IsAdmin)
                {
                    DateTime dtNow = DateTime.Now;
                    List<RoleAccessUser> roleAccessUser = RoleAccessUser.GetByUserId(user.ID)
                        .Where(x => 
                        roleAccessCode.Contains(x.RoleAccess.RoleAccessCode) || 
                        x.RoleAccess.RoleAccessCode == ROLE_CODE.ADMINISTRATOR)
                        .Where(x => x.DateFrom <= dtNow && x.DateTo >= dtNow).ToList();

                    if (roleAccessUser != null && roleAccessUser.Count > 0)
                    {
                        var listCompanyId = roleAccessUser.Select(x => x.CompanyId).Distinct().ToList();
                        var companies = Company.GetAll().Where(x => listCompanyId.Contains(x.ID)).ToList();
                        if (companies != null && companies.Count > 0)
                        {
                            foreach (var company in companies)
                            {
                                var companyModel = new CompanyModel();
                                companyModel.InjectFrom(company);
                                list.Add(companyModel);
                            }
                        }
                    }
                }
                else
                {
                    DateTime dtNow = DateTime.Now;
                    List<RoleAccessUser> roleAccessUser = RoleAccessUser.GetByUserId(user.ID)
                        .Where(x => roleAccessCode.Contains(x.RoleAccess.RoleAccessCode)).ToList();

                    if (roleAccessUser != null && roleAccessUser.Count > 0)
                    {
                        var listCompanyId = roleAccessUser.Select(x => x.CompanyId).Distinct().ToList();
                        var companies = Company.GetAll().Where(x => listCompanyId.Contains(x.ID)).ToList();
                        if (companies != null && companies.Count > 0)
                        {
                            foreach (var company in companies)
                            {
                                var companyModel = new CompanyModel();
                                companyModel.InjectFrom(company);
                                list.Add(companyModel);
                            }
                        }
                    }
                }
            }

            return list;
        }

        public static RoleCompanyModel GetByRoleCompany(RoleCompanyModel model)
        {
            var departments = Department.GetByCompanyId(model.CompanyId).ToList();
            List<Guid> listDepartmentIds = departments.Select(x => x.ID).ToList();

            var roleAccess = RoleAccessUser.GetByUserId(model.UserId)
                .Where(x => x.RoleAccessId == model.RoleAccessId && listDepartmentIds.Contains(x.DepartmentId)).ToList();

            if (model.RoleAccessId != Guid.Empty)
            {
                var role = RoleAccess.GetById(model.RoleAccessId);
                model.IsHaveAdditional = role.IsHaveAdditional;

                model.RoleCompanyDetails = (from department in departments
                                            join roleAcc in roleAccess on department.ID equals roleAcc.DepartmentId
                                            into joinData
                                            from left in joinData.DefaultIfEmpty()
                                            select new RoleCompanyDetailModel()
                                            {
                                                DepartmentId = department.ID,
                                                DepartmentName = department.DepartmentName,
                                                AdditionalValue = left != null ? left.AdditionalValue : string.Empty,
                                                IsSelected = left != null ? true : false
                                            }).ToList();
            }

            return model;
        }

        public static ResponseModel UpdateRoleCompany(RoleCompanyModel model)
        {
            ResponseModel response = new ResponseModel();

            var departments = Department.GetByCompanyId(model.CompanyId).ToList();
            List<Guid> listDepartmentIds = departments.Select(x => x.ID).ToList();

            List<RoleAccessUser> listToDelete = RoleAccessUser.GetByUserId(model.UserId)
                .Where(x => listDepartmentIds.Contains(x.DepartmentId) && x.RoleAccessId == model.RoleAccessId).ToList();

            foreach (var item in listToDelete)
            {
                item.Delete<RoleAccessUser>();
            }

            var listToInsert = model.RoleCompanyDetails.Where(x => x.IsSelected).ToList();
            foreach (var item in listToInsert)
            {
                RoleAccessUser defaultRoleAccessUser = new RoleAccessUser();
                defaultRoleAccessUser.ID = Guid.NewGuid();
                defaultRoleAccessUser.UserId = model.UserId;
                defaultRoleAccessUser.RoleAccessId = model.RoleAccessId;
                defaultRoleAccessUser.CompanyId = model.CompanyId;
                defaultRoleAccessUser.DepartmentId = item.DepartmentId;
                defaultRoleAccessUser.AdditionalValue = (!string.IsNullOrEmpty(item.AdditionalValue) ? item.AdditionalValue : "");
                defaultRoleAccessUser.IsDefault = true;
                defaultRoleAccessUser.DateFrom = DateTime.Now;
                defaultRoleAccessUser.DateTo = DateTime.Now.AddYears(100);
                defaultRoleAccessUser.Save<RoleAccessUser>();
            }

            
            var roleDefault = RoleAccess.GetByCode(ROLE_CODE.USERS);
            RoleAccessUser.GetByUserId(model.UserId).Where(x => x.RoleAccessId == roleDefault.ID);

            response.SetSuccess("Success To Save Role");
            return response;
        }

        public static ResponseModel DelegationRole(RoleDelegationModel model)
        {
            var response = new ResponseModel();
            StringBuilder sbMessage = new StringBuilder();

            #region Validation
            if (model.roleAccess == null || model.roleAccess.ID == Guid.Empty)
            {
                sbMessage.AppendLine("Must be select role");
            }

            if (model.company == null || model.company.ID == Guid.Empty)
            {
                sbMessage.AppendLine("Company must be select");
            }

            if (model.department == null || model.department.ID == Guid.Empty)
            {
                sbMessage.AppendLine("Department must be select");
            }

            DateTime dtFrom = DateTime.MinValue;
            DateTime dtTo = DateTime.MinValue;
            if (!string.IsNullOrEmpty(model.DateFromStr))
            {
                model.DateFrom = CommonFunction.DateTimeJResources(model.DateFromStr);
            }

            if (!string.IsNullOrEmpty(model.DateToStr))
            {
                model.DateTo = CommonFunction.DateTimeJResources(model.DateToStr);
            }

            if (model.DateFrom == DateTime.MinValue && model.DateTo == DateTime.MinValue)
            {
                sbMessage.AppendLine("Wrong format date from and to");
            }

            if (model.DateTo < model.DateFrom)
            {
                sbMessage.AppendLine("Date To must be over from date From");
            }
            #endregion

            if (sbMessage.Length > 0)
            {
                response.SetError(sbMessage.ToString());
            }
            else
            {
                var CheckRoleAccess = RoleAccessUser.GetByUserId(model.UserId)
                    .FirstOrDefault(x => x.ID != model.ID &&
                        x.RoleAccessId == model.roleAccess.ID && x.DepartmentId == model.department.ID 
                        && x.DateFrom >= model.DateFrom 
                        && x.DateTo <= model.DateTo);

                if (CheckRoleAccess != null)
                {
                    response.SetError(string.Format("Role {0} been there for a related user from {1} to {2}", 
                    CheckRoleAccess.RoleAccess.RoleAccessName, 
                    CheckRoleAccess.DateFrom.ToString(Common.Constant.FORMAT_DATE_JRESOURCES_TEXT), 
                    CheckRoleAccess.DateTo.ToString(Common.Constant.FORMAT_DATE_JRESOURCES_TEXT)));
                }
                else
                {
                    RoleAccessUser roletoDelete = RoleAccessUser.GetById(model.ID);
                    if (roletoDelete != null)
                    {
                        roletoDelete.Delete<RoleAccessUser>();
                    }

                    RoleAccessUser newRoleAccUser = new RoleAccessUser();
                    newRoleAccUser.ID = Guid.NewGuid();

                    var user = UserLogic.GetByNIK(CurrentUser.NIKSITE);
                    RoleAccessUser roleAccessUser = RoleAccessUser.GetByUserId(user.ID).FirstOrDefault(x =>
                        x.RoleAccessId == model.roleAccess.ID &&
                        x.CompanyId == model.company.ID &&
                        x.DepartmentId == model.department.ID);

                    if (roleAccessUser != null)
                    {
                        newRoleAccUser.UserId = model.UserId;
                        newRoleAccUser.RoleAccessId = roleAccessUser.RoleAccessId;
                        newRoleAccUser.CompanyId = roleAccessUser.CompanyId;
                        newRoleAccUser.DepartmentId = roleAccessUser.DepartmentId;
                        newRoleAccUser.AdditionalValue = roleAccessUser.AdditionalValue;
                        newRoleAccUser.IsDefault = false;
                        newRoleAccUser.DateFrom = model.DateFrom;
                        newRoleAccUser.DateTo = model.DateTo;
                        newRoleAccUser.Save<RoleAccessUser>();

                        response.SetSuccess(string.Format("Role {0} success to delegate from {1} to {2}", model.roleAccess.RoleAccessName, model.DateFromStr, model.DateToStr));
                    }
                    else
                    {
                        response.SetError("Dont Have Authorize");
                    }
                }
            }

            return response;
        }

        public static RoleDelegateSearchModel GetListDelegation(RoleDelegateSearchModel search)
        {
            List<RoleDelegationModel> list = new List<RoleDelegationModel>();
            IQueryable<RoleAccessUser> delegationList = RoleAccessUser.GetAll();
            delegationList = delegationList.Where(x => !x.IsDefault && x.CreatedBy == CurrentUser.NIKSITE);

            if (!string.IsNullOrEmpty(search.SearchCriteria))
            {
                delegationList = delegationList.Where(x => x.UserLogin.FullName.Contains(search.SearchCriteria));
            }

            if (search.RoleAccessId != Guid.Empty)
            {
                RoleAccess roleAccess = RoleAccess.GetById(search.RoleAccessId);
                delegationList = delegationList.Where(x => x.RoleAccess.RoleAccessCode == roleAccess.RoleAccessCode);
            }

            search.SetPager(delegationList.Count());
            IQueryable<RoleAccessUser> listDelegation = delegationList.OrderBy(x => x.UpdatedDate)
                .Skip((search.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
                .Take(Common.SiteSettings.PAGE_SIZE_PAGING);

            foreach (var item in listDelegation)
            {
                RoleDelegationModel itemDelegation = new RoleDelegationModel();
                itemDelegation.ID = item.ID;
                itemDelegation.UserId = item.UserLogin.ID;
                itemDelegation.FullName = item.UserLogin.FullName;
                itemDelegation.roleAccess = new RoleAccessModel();
                itemDelegation.roleAccess.InjectFrom(item.RoleAccess);
                itemDelegation.department = new DepartmentModel();
                itemDelegation.department.InjectFrom(item.Department);
                itemDelegation.company = new CompanyModel();
                itemDelegation.company.InjectFrom(item.Company);
                itemDelegation.DateFromStr = item.DateFrom.ToString(Constant.FORMAT_DATE_JRESOURCES_TEXT);
                itemDelegation.DateToStr = item.DateTo.ToString(Constant.FORMAT_DATE_JRESOURCES_TEXT);

                list.Add(itemDelegation);
            }

            search.ListData = list;

            return search;
        }

        public static bool IsAdministrator(string NIKSite, Guid CompanyId)
        {
            var user = UserLogic.GetByNIK(NIKSite);
            if (user != null && user.ID != Guid.Empty)
            {
                RoleAccessUser roleAcc = RoleAccessUser.GetAll().FirstOrDefault(x => x.UserId == user.ID && x.CompanyId == CompanyId && x.RoleAccess.RoleAccessCode == ROLE_CODE.ADMINISTRATOR);
                if (roleAcc != null)
                {
                    return true;
                }
            }

            return false;
        }
    }
}

