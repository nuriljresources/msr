﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using JResources.Data;


namespace JResources.BusinessLogic
{
    public class EquipmentLogic
    {
        public static List<SelectListItem> GetListEquipment(string equipmentCodeSelected = "")
        {
            return GetListEquipment(Guid.Empty, equipmentCodeSelected);
        }

        public static List<SelectListItem> GetListEquipment(Guid departmentId, string equipmentCodeSelected = "")
        {
            List<SelectListItem> list = new List<SelectListItem>();
            List<CSOPTFD> EquipmentList = AccpacDataContext.CurrentContext.CSOPTFDs.Where(x => x.OPTFIELD == "CCCODE" && !x.VDESC.Contains("DO NOT USE")).ToList();
            List<string> beginCode = new List<string>();
            List<string> beginCodeExclude = new List<string>();

            if (departmentId != Guid.Empty)
            {
                var department = CompanyLogic.GetDepartmentById(departmentId);

                if (department != null && department.CostCenterCode.Count > 0)
                {
                    beginCode = department.CostCenterCode;
                    beginCodeExclude = department.CostCenterCodeExclude;
                }
            }


            foreach (var item in EquipmentList)
            {
                SelectListItem listItem = new SelectListItem();
                listItem.Value = item.VALUE;
                listItem.Text = item.VDESC;

                if (item.VALUE == equipmentCodeSelected)
                {
                    listItem.Selected = true;
                }

                if (beginCode.Count > 0)
                {
                    if (beginCode.Contains(item.VALUE.Substring(0, 1)))
                    {
                        list.Add(listItem);
                    }
                }
                else
                {
                    list.Add(listItem);
                }

                if (beginCodeExclude.Count > 0)
                {
                    if (!beginCodeExclude.Contains(item.VALUE.Substring(0, 1)))
                    {
                        list.Add(listItem);
                    }
                }
                else
                {
                    list.Add(listItem);
                }
            }

            return list;
        }
    }
}
