﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JResources.Common;
using JResources.Data;
using JResources.Data.Model;

namespace JResources.BusinessLogic
{
    public class DocumentApprovalLogic
    {
        public static void MSRApprovalFlow(string MSRNo, Guid DepartmentId, decimal Total)
        {
            RoleAccess roleAccessMSR = RoleAccess.GetByCode(ROLE_CODE.MSR_APPROVAL);
            List<RoleAccessUser> roleAccessUserApprovals = RoleAccessUser.GetByRoleAccessId(roleAccessMSR.ID)
                .Where(x => x.DepartmentId == DepartmentId).ToList();

            if (roleAccessUserApprovals != null && roleAccessUserApprovals.Count > 0)
            {
                DocumentApproval docApproval = new DocumentApproval();
                docApproval.ID = Guid.NewGuid();
                docApproval.DocumentType = DocumentType.MSR;
                docApproval.DocumentNo = MSRNo;
                docApproval.Sequence = 1;
                docApproval.DepartmentId = DepartmentId;
                docApproval.ApprovalStatus = "";
                docApproval.RoleId = roleAccessMSR.ID;
                docApproval.Save<DocumentApproval>();
            }

            RoleAccess roleAccessMSRGM = RoleAccess.GetByCode(ROLE_CODE.MSR_APPROVAL_GM);
            roleAccessUserApprovals = RoleAccessUser.GetByRoleAccessId(roleAccessMSRGM.ID)
                .Where(x => x.DepartmentId == DepartmentId).ToList();

            if (roleAccessUserApprovals != null && roleAccessUserApprovals.Count > 0)
            {
                var roleAccessUserApprovalsGM = roleAccessUserApprovals.FirstOrDefault(x => !string.IsNullOrEmpty(x.AdditionalValue));
                if (roleAccessUserApprovalsGM != null)
                {
                    decimal limitTotal = 0;
                    decimal.TryParse(roleAccessUserApprovalsGM.AdditionalValue, out limitTotal);
                    if (Total >= limitTotal)
                    {
                        DocumentApproval docApproval = new DocumentApproval();
                        docApproval.ID = Guid.NewGuid();
                        docApproval.DocumentType = DocumentType.MSR;
                        docApproval.DocumentNo = MSRNo;
                        docApproval.Sequence = 2;
                        docApproval.DepartmentId = DepartmentId;
                        docApproval.ApprovalStatus = "";
                        docApproval.RoleId = roleAccessMSRGM.ID;
                        docApproval.Save<DocumentApproval>();
                    }
                }
            }
        }

        public static void ApproveByDocumentNo(string DocumentNo, string NIKSIte, string RoleAcccessCode)
        {
            var roleAccess = RoleAccess.GetByCode(RoleAcccessCode);
            var roleAccessAdmin = RoleAccess.GetByCode(ROLE_CODE.ADMINISTRATOR);
            var user = UserLogic.GetByNIK(NIKSIte);
            var roleAccessUser = RoleAccessUser.GetByUserId(user.ID).Where(x => x.RoleAccessId == roleAccess.ID || x.RoleAccessId == roleAccessAdmin.ID).ToList();
            if (roleAccessUser != null && roleAccessUser.Count > 0)
            {
                var departmentId = roleAccessUser.Select(x => x.DepartmentId).ToList();
                DocumentApproval docApprove = DocumentApproval.GetByDocumentNo(DocumentNo).FirstOrDefault(x => x.RoleId == roleAccess.ID && departmentId.Contains(x.DepartmentId));

                if (docApprove != null)
                {
                    if (!docApprove.IsFinish)
                    {
                        docApprove.IsFinish = true;
                        docApprove.Save<DocumentApproval>();
                    }
                }
            }            
        }

        public static ResponseModel IsCompleteByNo(string DocumentNo)
        {
            var response = new ResponseModel();

            var docs = DocumentApproval.GetByDocumentNo(DocumentNo);
            var docPending = docs.FirstOrDefault(x => !x.IsFinish);
            if (docPending != null)
            {
                var roleAccessPending = RoleAccess.GetById(docPending.RoleId);
                if (roleAccessPending != null)
                {
                    response.SetError("Next Approval To {0}", roleAccessPending.RoleAccessName);
                }
            }
            else
            {
                response.SetSuccess();
            }

            return response;
        }

        public static void CancelApprovalFlow(string MSRIssuedNo, Guid DepartmentId)
        {
            RoleAccess roleAccessMSR = RoleAccess.GetByCode(ROLE_CODE.MSR_APPROVAL);
            DocumentApproval docApproval = new DocumentApproval();
            docApproval.ID = Guid.NewGuid();
            docApproval.DocumentType = DocumentType.MSR;
            docApproval.DocumentNo = MSRIssuedNo;
            docApproval.Sequence = 1;
            docApproval.DepartmentId = DepartmentId;
            docApproval.RoleId = roleAccessMSR.ID;
            docApproval.ApprovalStatus = JResources.Common.Enum.RequestStatus.APPROVED;
            docApproval.IsFinish = true;
            docApproval.Save<DocumentApproval>();


            RoleAccess roleAccessMSRISSUED = RoleAccess.GetByCode(ROLE_CODE.MSR_ISSUED);
            docApproval = new DocumentApproval();
            docApproval.ID = Guid.NewGuid();
            docApproval.DocumentType = DocumentType.MSR;
            docApproval.DocumentNo = MSRIssuedNo;
            docApproval.Sequence = 2;
            docApproval.DepartmentId = DepartmentId;
            docApproval.RoleId = roleAccessMSRISSUED.ID;
            docApproval.ApprovalStatus = JResources.Common.Enum.RequestStatus.APPROVED;
            docApproval.IsFinish = true;
            docApproval.Save<DocumentApproval>();
        }
    }
}
