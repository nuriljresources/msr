﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using JResources.Common;
using JResources.Data;
using JResources.Data.Model;
using Omu.ValueInjecter;
using JResources.Common.Enum;

namespace JResources.BusinessLogic
{
    public class FRFLogic
    {
        public static FRFSearchModel GetListFRF(FRFSearchModel model)
        {
            model.SetDateRange();
            IQueryable<tfrf_master> FRFs = tfrf_master.GetAll();
            List<CompanyModel> ListDepartment = RoleLogic.GetListCompanyAccess(CurrentUser.USERNAME, CurrentUser.COMPANY, model.ROLE_ACCESS);

            List<string> listCompanyCode = ListDepartment.Select(x => x.CompanyCode).Distinct().ToList();

            FRFs = FRFs.Where(x => listCompanyCode.Contains(x.compid));


            if (!string.IsNullOrEmpty(model.FRFNo))
            {
                FRFs = FRFs.Where(x => x.frf_no.Contains(model.FRFNo));
            }

            if (model.MSR_STATUS != null && model.MSR_STATUS.Length > 0)
            {
                FRFs = FRFs.Where(x => model.MSR_STATUS.Contains(x.status_id));
            }

            if (model.DateFrom.HasValue)
            {
                var dtFrom = Common.DateHelper.DateFromMinimun(model.DateFrom.Value);
                FRFs = FRFs.Where(x => x.frf_dt >= dtFrom);
            }

            if (model.DateTo.HasValue)
            {
                var dtTo = Common.DateHelper.DateToMax(model.DateTo.Value);
                FRFs = FRFs.Where(x => x.frf_dt <= dtTo);
            }

            if (!string.IsNullOrEmpty(model.FRFStatus))
            {
                FRFs = FRFs.Where(x => x.status_id == model.FRFStatus);
            }

            if (!string.IsNullOrEmpty(model.Requestor))
            {
                FRFs = FRFs.Where(x => x.requestor == model.Requestor);
            }

            if (model.CompanyId != Guid.Empty)
            {
                Company companyFilter = Company.GetById(model.CompanyId);
                if (companyFilter != null)
                {
                    FRFs = FRFs.Where(x => x.compid == companyFilter.CompanyCode);
                }
            }

            model.SetPager(FRFs.Count());
            var FRFList = FRFs.OrderByDescending(x => x.frf_dt)
                .Skip((model.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
                .Take(Common.SiteSettings.PAGE_SIZE_PAGING).ToList();

            List<string> CompCodeList = FRFList.Select(x => x.compid.Trim()).Distinct().ToList();
            var CompanyList = Company.GetAll().Where(x => CompCodeList.Contains(x.CompanyCode)).ToList();

            var statusFRFs = EnumTable.GetByCode(EnumTable.EnumType.STATUS_MSR).ToList();
            foreach (var item in FRFList)
            {
                var modelItem = new FRFModel();
                modelItem.ID = item.ID;
                modelItem.FRFNo = item.frf_no;
                modelItem.FRFDate = item.frf_dt;
                modelItem.FRFDateStr = (item.frf_dt.HasValue ? item.frf_dt.Value.ToString(Constant.FORMAT_DATE_JRESOURCES) : string.Empty);
                modelItem.CompanyName = item.compid;
                Company companySelect = CompanyList.FirstOrDefault(x => x.CompanyCode == item.compid.Trim());
                if (companySelect != null)
                {
                    modelItem.CompanyId = companySelect.ID;
                    modelItem.CompanyName = companySelect.CompanyName;
                }

                modelItem.UserCreated.FullName = item.requestor;
                var userRequest = UserLogic.GetByNIK(item.requestor);
                if (userRequest != null)
                {
                    modelItem.UserCreated = userRequest;
                }

                modelItem.RowCssColor = CommonFunction.RowStatusCssHelper(item.status_id);
                modelItem.LabelCssColor = CommonFunction.LabelStatusCssHelper(item.status_id);

                modelItem.FRFStatus = item.status_id;
                var statusFRF = statusFRFs.FirstOrDefault(x => x.EnumValue == item.status_id);
                if (statusFRF != null)
                {
                    modelItem.FRFStatusLabel = statusFRF.EnumLabel;
                }

                model.ListData.Add(modelItem);
            }

            return model;
        }

        public static FRFModel GetDataById(Guid Id)
        {
            FRFModel model = new FRFModel();
            tfrf_master frf = tfrf_master.GetById(Id);
            if (frf != null)
            {
                model.InjectFrom(frf);
                var userRequest = UserLogic.GetByNIK(frf.requestor);
                if (userRequest != null)
                {
                    model.UserCreated = userRequest;
                }

                model.ID = frf.ID;
                model.FRFNo = frf.frf_no;
                model.FRFDate = frf.frf_dt;
                model.FRFDateStr = (frf.frf_dt.HasValue ? frf.frf_dt.Value.ToString(Constant.FORMAT_DATE_JRESOURCES) : string.Empty);
                model.FRFStatus = frf.status_id;
                var frfStatus = EnumTable.GetDescriptionByCodeValue(EnumTable.EnumType.STATUS_MSR, frf.status_id);
                if (frfStatus != null)
                {
                    model.FRFStatusLabel = frfStatus.EnumLabel;
                }

                

                model.CompanyName = frf.compid;
                Company companySelect = Company.GetByCode(frf.compid);
                if (companySelect != null)
                {
                    model.CompanyId = companySelect.ID;
                    model.CompanyName = companySelect.CompanyName;
                }

                var tfrf_details = tfrf_detail.GetByFRFNo(frf.frf_no);
                if (tfrf_details != null)
                {
                    var listItemNo = tfrf_details.Select(x => x.stock_no.Trim()).ToList();
                    var listItemLocation = AccpacDataContext.CurrentContext.ICILOCs.Where(x => x.ACTIVE == 1 && listItemNo.Contains(x.ITEMNO)).ToList();

                    foreach (var item in tfrf_details)
                    {
                        var detailItem = new FRFDetailModel();
                        detailItem.InjectFrom(item);
                        detailItem.LogSheetDate = item.log_sheet_dt;
                        if (item.log_sheet_dt.HasValue)
                        {
                            detailItem.LogSheetDateStr = item.log_sheet_dt.Value.ToString(Common.Constant.FORMAT_DATE_JRESOURCES);
                        }

                        detailItem.ItemPart = ItemLogic.GetByItemNo(item.stock_no);
                        detailItem.ItemNo = item.stock_no;
                        detailItem.ItemDescription = item.item_desc;
                        detailItem.LocationId = item.loc_id;
                        detailItem.Qty = item.qty;
                        var csoptfd = CSOPTFD.GetEquipmentByValue(item.eccode);
                        var equipmentModel = new FRFItemEquipment();
                        if (csoptfd != null)
                        {
                            equipmentModel.EquipmentCode = csoptfd.VALUE.Trim();
                            equipmentModel.EquipmentLabel = csoptfd.VDESC.Trim();
                        }
                        equipmentModel.CostCenter = item.cccode;

                        detailItem.Equipment = equipmentModel;
                        detailItem.EquipmentCode = item.eccode;
                        detailItem.CostCode = item.cccode;
                        detailItem.MaterialCode = item.mtcode;
                        detailItem.EndOfKM = item.end_of_km;
                        detailItem.OperatorName = item.oper_nm;
                        detailItem.Category = (!string.IsNullOrEmpty(item.cat_code) ? item.cat_code : item.category);

                        var itemInfo = listItemLocation.FirstOrDefault(x => x.ITEMNO.Trim() == item.stock_no.Trim());
                        if (itemInfo != null)
                        {
                            detailItem.Unit = itemInfo.COSTUNIT;
                        }

                        model.FRFDetails.Add(detailItem);
                    }
                }

            }


            return model;
        }

        /// <summary>
        /// Update Status Approve, Rejected or Revision
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="StatusRequest"></param>
        /// <returns></returns>
        public static ResponseModel SetActionFRFStatusApproval(Guid Id, string StatusRequest)
        {
            var response = new ResponseModel();
            tfrf_master frf = tfrf_master.GetById(Id);
            if (frf != null)
            {
                var company = Company.GetByCode(frf.compid);
                if (company != null && RoleLogic.CheckRoleAccess(CurrentUser.NIKSITE, ROLE_CODE.FRF_APPROVAL, company.ID, Guid.Empty))
                {
                    if (frf.status_id == RequestStatus.NEW
                        || frf.status_id == RequestStatus.REVISION
                        || frf.status_id == RequestStatus.ENTRY_FUEL)
                    {
                        DocumentApprovalLogic.ApproveByDocumentNo(frf.frf_no, CurrentUser.NIKSITE, ROLE_CODE.FRF_APPROVAL);

                        frf.approver = CurrentUser.NIKSITE;
                        frf.status_id = StatusRequest;
                        frf.UpdateSave<MSR>();
                        response.SetSuccess("MSR {0} has been update status", frf.frf_no);
                    }
                    else
                    {
                        response.SetError("MSR {0} status must be New OR Revision", frf.frf_no);
                    }
                }
                else
                {
                    response.SetError("{0} dont have permission to approved", CurrentUser.FULLNAME);
                }
            }
            else
            {
                response.SetError("MSR Not Found");
            }

            return response;
        }
    }
}
