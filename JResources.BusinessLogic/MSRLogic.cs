﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JResources.Data;
using JResources.Data.Model;
using JResources.BusinessLogic;
using JResources.Common;
using System.Transactions;
using Omu.ValueInjecter;
using JResources.Common.Enum;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Data.SqlClient;

namespace JResources.BusinessLogic
{
    public class MSRLogic
    {
        /// <summary>
        /// Get MSR Model
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static MSRModel GetMSRModel(Guid Id)
        {
            MSRModel model = new MSRModel();
            MSR msr = MSR.GetById(Id);
            model.InjectFrom(msr);
            model.DocumentNo = msr.MSRNo;
            model.CostCenter = msr.CostCode;
            model.MSRType = msr.RequestType;
            var MSRType = JResources.Common.EnumUtils<JResources.Common.MSRType>.GetDropDownList().FirstOrDefault(x => x.Value == msr.RequestType);
            model.MSRTypeLabel = MSRType.Text;
            model.DateCreated = msr.CreatedDate;
            model.DateCreatedStr = msr.CreatedDate.ToString(Constant.FORMAT_DATE_JRESOURCES);
            model.DateUpdate = msr.UpdatedDate;
            model.DateUpdateStr = msr.UpdatedDate.ToString(Constant.FORMAT_DATE_JRESOURCES);

            model.ExpectedDate = msr.ExpectedDate;
            model.ExpectedDateStr = msr.ExpectedDate.ToString(Constant.FORMAT_DATE_JRESOURCES);
            model.LeadTimeNumber = msr.LeadTime;
            model.LeadTime = MSRLogic.GetLeadTimeByCode(msr.MaintenanceCode);
            model.CompanyId = msr.CompanyID;
            model.CompanyName = msr.Company.CompanyName;
            model.DepartmentId = msr.DepartmentId;
            model.Reason = msr.Remark;
            model.EquipmentNo = msr.EquipmentNo;
            model.IsUnloading = msr.IsUnloading;
            model.IsDataMigration = msr.IsDataMigration;

            if (!string.IsNullOrEmpty(msr.WorkOrderNo))
            {
                var vmdh = AccpacDataContext.CurrentContext.VMDHs.FirstOrDefault(x => x.TXDOCID.Contains(msr.WorkOrderNo));
                if (vmdh != null)
                {
                    model.WorkOrderStatus = vmdh.WDSTATUS;
                    var VMSTHD = AccpacDataContext.CurrentContext.VMSTHDs.FirstOrDefault(x => x.WDSTATUS == vmdh.WDSTATUS);
                    if (VMSTHD != null)
                    {
                        model.WorkOrderStatusLabel = VMSTHD.TXSTATUS.Trim();
                    }
                }
            }

            var docApprovals = DocumentApproval.GetByDocumentNo(msr.MSRNo).Where(x => x.IsFinish).ToList();
            if (docApprovals != null && docApprovals.Count > 0)
            {
                var docApproved = docApprovals.OrderByDescending(x => x.Sequence).FirstOrDefault(x => x.IsFinish);
                if (docApproved != null)
                {
                    model.ApprovedDate = docApproved.UpdatedDate.AddDays(model.LeadTimeNumber);
                    model.ApprovedDateStr = model.ApprovedDate.ToString(Constant.FORMAT_DATE_JRESOURCES);
                    model.ApprovedBy = UserLogic.GetByNIK(docApproved.UpdatedBy);
                }
            }

            var department = Department.GetById(msr.DepartmentId);
            if (department != null)
            {
                model.DepartmentName = department.DepartmentName;
            }

            var costcenter = CostCenterLogic.GetCostCenterByDepartmentId(model.DepartmentId);
            var costCenterSelect = costcenter.FirstOrDefault(x => x.Value == model.CostCenter);
            if (costCenterSelect != null)
            {
                model.CostCenterLabel = costCenterSelect.Text;
            }

            var statusMSRs = EnumTable.GetByCode(EnumTable.EnumType.STATUS_MSR).ToList();
            model.StatusRequest = msr.MSRStatus;
            var statusMSR = statusMSRs.FirstOrDefault(x => x.EnumValue == msr.MSRStatus);
            if (statusMSR != null)
            {
                model.StatusRequestLabel = statusMSR.EnumLabel;
            }

            Guid UserId = CurrentUser.USERID;
            RoleAccess RoleManager = RoleAccess.GetByCode(ROLE_CODE.MSR_APPROVAL);
            RoleAccess RoleGM = RoleAccess.GetByCode(ROLE_CODE.MSR_APPROVAL_GM);
            bool IsAdmin = RoleLogic.IsAdministrator(CurrentUser.NIKSITE, msr.CompanyID);

            var IsManager = RoleLogic.CheckRoleAccess(CurrentUser.NIKSITE, ROLE_CODE.MSR_APPROVAL, msr.DepartmentId);
            if (IsManager || IsAdmin)
            {
                if (msr.MSRStatus == Common.Enum.RequestStatus.NEW)
                {
                    model.IsNeedActionRequest = true;
                }
                else
                {
                    if (msr.MSRStatus == Common.Enum.RequestStatus.REVISION)
                    {
                        if (msr.MSRStatus == Common.Enum.RequestStatus.NEW)
                        {
                            model.IsNeedActionRequest = true;
                        }
                    }
                }
            }

            var IsGM = RoleLogic.CheckRoleAccess(CurrentUser.NIKSITE, ROLE_CODE.MSR_APPROVAL_GM, msr.DepartmentId);
            if (IsGM || IsAdmin)
            {
                model.IsWaitingApprovelBefore = true;
                if (msr.MSRStatus == Common.Enum.RequestStatus.PARTIAL_APPROVED)
                {
                    model.IsNeedActionRequest = true;
                }
                else
                {
                    if (msr.MSRStatus == Common.Enum.RequestStatus.REVISION)
                    {
                        if (msr.MSRStatus == Common.Enum.RequestStatus.PARTIAL_APPROVED)
                        {
                            model.IsNeedActionRequest = true;
                        }
                    }
                }
            }

            model.MaintenanceCode = msr.MaintenanceCode;
            var CompanyCode = Common.CookieManager.Get(COOKIES_NAME.USER_COMPANY_CODE);

            if (CompanyCode == COMPANY_CODE.ASA || CompanyCode == COMPANY_CODE.GSM || CompanyCode == COMPANY_CODE.LANUT)
            {
                switch (msr.MaintenanceCode)
                {
                    case "P01":
                        model.MaintenanceCodeLabel = "P01-UNSCHEDULE MACHINE DOWN / TOP PRIORITY*";
                        break;

                    case "P02":
                        model.MaintenanceCodeLabel = "P02-SCHEDULED IMMINENT FAILURE / URGENT*";
                        break;

                    case "P03":
                        model.MaintenanceCodeLabel = "P03-SCHEDULED BACKLOG / NORMAL*";
                        break;

                    case "P04":
                        model.MaintenanceCodeLabel = "P04-SCHEDULED PCR / SCHEDULED*";
                        break;
                }
            }
            else
            {
                var vmmtype = AccpacDataContext.CurrentContext.VMTYPEs.FirstOrDefault(x => x.TXTYPE == msr.MaintenanceCode);
                if (vmmtype != null)
                {
                    model.MaintenanceCodeLabel = string.Format("{0} - {1}", vmmtype.TXTYPE.Trim(), vmmtype.TXDESC.Trim());
                }
            }

            var MSRDetails = MSRDetail.GetAllByMSRId(msr.ID).ToList();
            List<string> catList = MSRDetails.Select(x => x.CategoryCode.Trim()).ToList();

            var iccatgs = AccpacDataContext.CurrentContext.ICCATGs.Where(x => catList.Contains(x.CATEGORY)).ToList();

            foreach (var item in MSRDetails.OrderBy(x => x.LineNo))
            {
                MSRDetailModel detail = new MSRDetailModel();
                detail.ID = item.ID;
                detail.IdLookup = Guid.NewGuid().ToString().Replace("-", "");
                detail.InjectFrom(item);
                detail.LineNo = item.LineNo;
                detail.Qty = item.Qty;
                detail.Location = item.LocationId;
                if (string.IsNullOrEmpty(detail.Location))
                {
                    detail.Location = (!string.IsNullOrEmpty(item.LocationIdFrom) ? item.LocationIdFrom : "");
                }

                detail.Cost = item.UnitCost;
                detail.Category = item.CategoryCode;
                detail.CostCode = item.CostCode;
                var glamf = GLAMF.GetByCode(item.CostCode);
                if (glamf != null)
                {
                    detail.CostCodeLabel = string.Format("{0} - {1}", glamf.ACCTID, glamf.ACCTDESC);
                }

                var iccatg = iccatgs.FirstOrDefault(x => x.CATEGORY.Trim() == item.CategoryCode.Trim());
                if (iccatg != null)
                {
                    detail.CategoryLabel = iccatg.CATEGORY.Trim() + " - " + iccatg.DESC.Trim();
                }

                model.MSRDetail.Add(detail);
            }

            ItemLogic.SetQtyByLocation(model.MSRDetail);
            model.Requestor = UserLogic.GetByNIK(msr.CreatedBy);
            if (model.Requestor == null)
            {
                model.Requestor = new UserModel();
                model.Requestor.NIKSite = msr.CreatedBy;
                model.Requestor.FullName = msr.CreatedBy;
            }

            model.TotalAmount = model.MSRDetail.Sum(x => x.Qty * x.Cost);

            MSRHistoryModel MSRHistory = new MSRHistoryModel();
            MSRHistory.DocumentNo = model.DocumentNo;
            MSRHistory = GetMSRApprovel(MSRHistory);
            model.Manager = MSRHistory.Manager;
            model.GeneralManagerSite = MSRHistory.GeneralManagerSite;

            return model;
        }

        /// <summary>
        /// Get Detail All MSR Model
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static MSRHistoryModel GetMSRHistoryModel(Guid Id)
        {
            MSRHistoryModel model = new MSRHistoryModel();
            MSRModel MSRModel = GetMSRModel(Id);
            model.InjectFrom(MSRModel);
            model = GetMSRApprovel(model);

            var MSRIssuedsId = MSRIssued.GetByMSRId(Id).OrderBy(x => x.CreatedDate).Select(x => x.ID).ToList();
            foreach (var MSRIssuedId in MSRIssuedsId)
            {
                MSRIssuedModel MSRIssuedModel = WarehouseLogic.GetMSRIssuedId(MSRIssuedId);
                model.MSRIssueds.Add(MSRIssuedModel);
            }


            model.MSRLast = WarehouseLogic.GetDataByMSRId(Id, "");
            return model;
        }

        public static MSRHistoryModel GetMSRApprovel(MSRHistoryModel model)
        {
            var MSRApproval = RoleAccess.GetByCode(ROLE_CODE.MSR_APPROVAL);
            var MSRApprovalGM = RoleAccess.GetByCode(ROLE_CODE.MSR_APPROVAL_GM);

            var docApprovals = DocumentApproval.GetByDocumentNo(model.DocumentNo).ToList();
            if (docApprovals != null && docApprovals.Count > 0)
            {
                if (docApprovals[0] != null && docApprovals[0].IsFinish)
                {
                    var approveManager = UserLogic.GetByNIK(docApprovals[0].UpdatedBy);
                    if (approveManager != null)
                    {
                        model.DateApproveManager = docApprovals[0].UpdatedDate;
                        model.Manager = approveManager;
                    }
                }

                if (docApprovals.Count > 1)
                {
                    if (docApprovals[1] != null && docApprovals[1].IsFinish)
                    {
                        var approveSiteManager = UserLogic.GetByNIK(docApprovals[1].UpdatedBy);
                        if (approveSiteManager != null)
                        {
                            model.DateApproveSiteManager = docApprovals[1].UpdatedDate;
                            model.GeneralManagerSite = approveSiteManager;
                        }
                    }
                }
            }

            return model;
        }

        /// <summary>
        /// Get List MSR
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static MSRSearchModel GetListMSR(MSRSearchModel model)
        {
            model.SetDateRange();

            var MSRs = MSR.GetAll();

            if (model.IsExcludeDataMigration)
            {
                MSRs = MSRs.Where(x => !x.IsDataMigration);
            }

            var roleCookies = CookieManager.Get(COOKIES_NAME.ROLE);
            var user = CurrentUser.Model;
            user.SetRolesFromCookies(roleCookies);

            if (model.IsRequisition)
            {
                MSRs = MSRs.Where(x => x.RequisitionDate.HasValue);
            }

            if (model.IsLookup)
            {
                if (model.IsReOrderPoint)
                {
                    MSRs = MSRs.Where(x => x.IsReOrderPoint == true);
                }
                else
                {
                    MSRs = MSRs.Where(x => x.IsReOrderPoint == false);
                }
            }

            if (!user.IsAdministrator)
            {
                List<Guid> listDepartmentManager = new List<Guid>();
                List<Guid> listDepartmentGM = new List<Guid>();

                List<Guid> ListDepartmentId = new List<Guid>();
                foreach (var item in model.ROLE_ACCESS)
                {
                    string[] RoleCodes = new string[] { item };
                    List<SelectListItem> ListDepartment = RoleLogic.GetListDepartmentUserByRole(CurrentUser.USERNAME, CurrentUser.COMPANY, RoleCodes);
                    ListDepartmentId.AddRange(ListDepartment
                        .Where(x => !string.IsNullOrEmpty(x.Value))
                        .Where(x => x.Value != Guid.Empty.ToString())
                        .Select(x => Guid.Parse(x.Value)).ToList());

                    if (item == ROLE_CODE.MSR_APPROVAL)
                    {
                        listDepartmentManager = ListDepartment
                        .Where(x => !string.IsNullOrEmpty(x.Value))
                        .Where(x => x.Value != Guid.Empty.ToString())
                        .Select(x => Guid.Parse(x.Value)).ToList();
                    }

                    if (item == ROLE_CODE.MSR_APPROVAL_GM)
                    {
                        listDepartmentGM = ListDepartment
                        .Where(x => !string.IsNullOrEmpty(x.Value))
                        .Where(x => x.Value != Guid.Empty.ToString())
                        .Select(x => Guid.Parse(x.Value)).ToList();
                    }
                }

                MSRs = MSRs.Where(x => ListDepartmentId.Contains(x.DepartmentId));

                if (!model.IsRecentActivity)
                {
                    if (listDepartmentManager.Count > 0 && listDepartmentGM.Count > 0)
                    {
                        MSRs = MSRs.Where(x => (listDepartmentManager.Contains(x.DepartmentId) && x.MSRStatus == RequestStatus.NEW)
                            || (listDepartmentGM.Contains(x.DepartmentId) && x.MSRStatus == RequestStatus.PARTIAL_APPROVED));
                    }
                    else if (listDepartmentManager.Count > 0)
                    {
                        MSRs = MSRs.Where(x => listDepartmentManager.Contains(x.DepartmentId) && x.MSRStatus == RequestStatus.NEW);
                    }
                    else if (listDepartmentGM.Count > 0)
                    {
                        MSRs = MSRs.Where(x => listDepartmentGM.Contains(x.DepartmentId) && x.MSRStatus == RequestStatus.PARTIAL_APPROVED);
                    }
                }

            }

            if (!string.IsNullOrEmpty(model.IssuedType))
            {
                if (model.IssuedType == RequestStatus.TRANSFER)
                {
                    MSRs = MSRs.Where(x => x.WorkOrderNo != "");
                }
            }

            if (model.MSR_STATUS != null && model.MSR_STATUS.Length > 0)
            {
                MSRs = MSRs.Where(x => model.MSR_STATUS.Contains(x.MSRStatus));
            }

            if (!string.IsNullOrEmpty(model.MSRNo))
            {
                MSRs = MSRs.Where(x => x.MSRNo.Contains(model.MSRNo));
            }

            if (!string.IsNullOrEmpty(model.WorkOrderNo))
            {
                MSRs = MSRs.Where(x => x.WorkOrderNo.Contains(model.WorkOrderNo));
            }

            if (model.DateFrom.HasValue)
            {
                MSRs = MSRs.Where(x => x.ExpectedDate >= model.DateFrom.Value);
            }

            if (model.DateTo.HasValue)
            {
                MSRs = MSRs.Where(x => x.ExpectedDate <= model.DateTo.Value);
            }

            if (!string.IsNullOrEmpty(model.RequestStatus))
            {
                MSRs = MSRs.Where(x => x.MSRStatus == model.RequestStatus);
            }

            if (!string.IsNullOrEmpty(model.CodeCostCenter))
            {
                MSRs = MSRs.Where(x => x.CostCode == model.CodeCostCenter);
            }

            if (!string.IsNullOrEmpty(model.Reason))
            {
                MSRs = MSRs.Where(x => x.Remark.Contains(model.Reason));
            }

            if (!string.IsNullOrEmpty(model.EquipmentNo))
            {
                MSRs = MSRs.Where(x => x.EquipmentNo.Contains(model.EquipmentNo));
            }

            if (!string.IsNullOrEmpty(model.MaintenanceCode))
            {
                MSRs = MSRs.Where(x => x.MaintenanceCode.Contains(model.MaintenanceCode));
            }

            if (model.CompanyId != null && model.CompanyId != Guid.Empty)
            {
                var department = Department.GetByCompanyId(model.CompanyId);
                var listDepartmentId = department.Select(x => x.ID).ToList();
                MSRs = MSRs.Where(x => listDepartmentId.Contains(x.DepartmentId));
            }

            if (model.DepartmentId != null && model.DepartmentId != Guid.Empty)
            {
                MSRs = MSRs.Where(x => x.DepartmentId == model.DepartmentId);
            }

            if(model.IssuedType == "CBE")
            {
                MSRs = MSRs.Where(x => x.MSRStatus != "COM");
                MSRs = MSRs.Where(x => x.MSRStatus != "FUP");
            }

            model.SetPager(MSRs.Count());
            List<MSR> listData = MSRs.OrderByDescending(x => x.CreatedDate)
                .Skip((model.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
                .Take(Common.SiteSettings.PAGE_SIZE_PAGING).ToList();

            List<string> CostCodeList = listData.Select(x => x.CostCode.Trim()).Distinct().ToList();
            List<string> EquipmentNoList = listData.Select(x => x.EquipmentNo.Trim()).Distinct().ToList();
            List<string> RequestorList = listData.Select(x => x.CreatedBy).Distinct().ToList();
            List<Guid> DepartmentIdList = listData.Select(x => x.DepartmentId).Distinct().ToList();

            var costCenterList = CSOPTFD.GetCostCenterByValueList(CostCodeList).ToList();
            var equipmentList = CSOPTFD.GetEquipmentByValueList(EquipmentNoList).ToList();
            var statusMSRs = EnumTable.GetByCode(EnumTable.EnumType.STATUS_MSR).ToList();
            var requestors = UserLogin.GetAll().Where(x => RequestorList.Contains(x.NIKSite)).ToList();
            var departments = Department.GetAll().Where(x => DepartmentIdList.Contains(x.ID)).ToList();

            foreach (var msr in listData)
            {
                var MSRList = new MSRListModel();
                MSRList.ID = msr.ID;
                MSRList.DocumentNo = msr.MSRNo;
                MSRList.WorkOrderNo = msr.WorkOrderNo;

                if (msr.RequisitionDate.HasValue && !msr.IsDataMigration)
                {
                    MSRList.IsDoneRequisition = true;
                }
                else
                {
                    string[] statusCantRequisition = new string[] { RequestStatus.COMPLETED, RequestStatus.REVISION, RequestStatus.PARTIAL_APPROVED, RequestStatus.NEW, RequestStatus.REVISION, RequestStatus.REJECTED, RequestStatus.CANCEL };
                    if (!statusCantRequisition.Contains(msr.MSRStatus) && !msr.IsDataMigration)
                    {
                        MSRList.IsCanRequisition = true;
                    }
                }

                if (msr.IsUnloading && !msr.IsDataMigration)
                {
                    MSRList.IsDoneUnloading = true;
                }
                else
                {
                    if ((
                        msr.MSRStatus == RequestStatus.APPROVED ||
                        msr.MSRStatus == RequestStatus.PARTIAL_COMPLETED ||
                        msr.MSRStatus == RequestStatus.PARTIAL_TRANSFER ||
                        msr.MSRStatus == RequestStatus.FULL_TRANSFER)
                        && !string.IsNullOrEmpty(msr.WorkOrderNo)
                        && !msr.IsDataMigration)
                    {
                        MSRList.IsUnloading = true;
                    }
                }

                var department = departments.FirstOrDefault(x => x.ID == msr.DepartmentId);
                if (department != null)
                {
                    MSRList.DepartmentName = department.DepartmentName;
                }

                MSRList.DateCreated = msr.CreatedDate;
                MSRList.DateCreatedStr = msr.CreatedDate.ToString(Constant.FORMAT_DATE_JRESOURCES);
                MSRList.ExpectedDateStr = msr.ExpectedDate.ToString(Constant.FORMAT_DATE_JRESOURCES);

                MSRList.CostCenter = msr.CostCode;
                var costCenter = costCenterList.FirstOrDefault(x => x.VALUE.Trim() == msr.CostCode.Trim());
                if (costCenter != null && !string.IsNullOrEmpty(costCenter.VDESC))
                {
                    costCenter.VDESC = Regex.Replace(costCenter.VDESC, "(\\B[A-Z])", " $1");
                    //MSRList.CostCenter = string.Format("{0} - {1}", costCenter.VALUE, costCenter.VDESC);
                }

                MSRList.MaintenanceCode = msr.MaintenanceCode.Trim();
                MSRList.ExpectedDate = msr.ExpectedDate;
                MSRList.Reason = msr.Remark;

                if (MSRList.Reason.Length > 20)
                {
                    MSRList.Reason = MSRList.Reason.Substring(0, 20) + "[..]";
                }

                MSRList.EquipmentNo = msr.EquipmentNo;
                var equipment = equipmentList.FirstOrDefault(x => x.VALUE.Trim() == msr.EquipmentNo.Trim());
                if (equipment != null && !string.IsNullOrEmpty(equipment.VDESC))
                {
                    equipment.VDESC = Regex.Replace(equipment.VDESC, "(\\B[A-Z])", " $1");
                    MSRList.EquipmentNo = string.Format("{0} - {1}", equipment.VALUE, equipment.VDESC);
                }

                MSRList.RowCssColor = CommonFunction.RowStatusCssHelper(msr.MSRStatus);
                MSRList.LabelCssColor = CommonFunction.LabelStatusCssHelper(msr.MSRStatus);
                MSRList.StatusRequestCode = msr.MSRStatus;
                var statusMSR = statusMSRs.FirstOrDefault(x => x.EnumValue == msr.MSRStatus);
                if (statusMSR != null)
                {
                    MSRList.StatusRequestLabel = statusMSR.EnumLabel;
                }

                MSRList.Requestor = msr.CreatedBy;
                var requestor = requestors.FirstOrDefault(x => x.NIKSite == msr.CreatedBy);
                if (requestor != null)
                {
                    MSRList.Requestor = requestor.FullName;
                }


                MSRList.NextApproval = "";



                model.ListData.Add(MSRList);
            }

            return model;
        }

        ///// <summary>
        /// <summary>
        /// Create Document MSR
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static ResponseModel CreateMSR(MSRModel model)
        {
            model.SetDateFrom();
            ResponseModel response = new ResponseModel();

            Department department = Department.GetById(model.DepartmentId);
            response = ValidationMSR(model);

            if(response.IsSuccess)
            {
                // Adding New Validation for description Item
                List<MSRDetailModel> ListModel = model.MSRDetail.Where(x => !string.IsNullOrEmpty(x.ItemNo)).ToList();
                List<String> ListItemNo = ListModel.Select(x => x.ItemNo).ToList();
                if (ListItemNo != null && ListItemNo.Count > 0)
                {
                    var AccpacItems = AccpacDataContext.CurrentContext.ICITEMs.Where(x => ListItemNo.Contains(x.ITEMNO) || ListItemNo.Contains(x.FMTITEMNO)).ToList();
                    if (AccpacItems != null && AccpacItems.Count > 0)
                    {
                        ListModel.ForEach(x =>
                        {
                            var AccpacItem = AccpacItems.FirstOrDefault(y => (y.ITEMNO == x.ItemNo || y.FMTITEMNO == x.ItemNo) && y.DESC != "");
                            if (AccpacItem != null)
                            {
                                x.Description = AccpacItem.DESC.Trim();
                            }
                        });
                    }
                }

                using (var transaction = new TransactionScope())
                {
                    if (response.IsSuccess)
                    {
                        MSR msr = new MSR();
                        msr.ID = Guid.NewGuid();
                        if (model.IsReOrderPoint)
                        {
                            msr.MSRNo = DocumentNumberLogic.GetReplenishmentDocumentNo(department.CompanyId);
                        }
                        else
                        {
                            msr.MSRNo = DocumentNumberLogic.GetMSRDocumentNo(department.CompanyId);
                        }

                        msr.CompanyID = department.CompanyId;
                        msr.DepartmentId = department.ID;
                        msr.RequestType = model.MSRType;
                        model.DocumentNo = msr.MSRNo;
                        msr.CostCode = model.CostCenter;
                        msr.WorkOrderNo = string.Empty;
                        msr.EquipmentNo = string.Empty;
                        msr.IsReOrderPoint = model.IsReOrderPoint;
                        msr.MaintenanceCode = model.MaintenanceCode;
                        msr.MSRStatus = RequestStatus.NEW;
                        msr.MSRDate = model.DateCreated;
                        msr.ExpectedDate = model.ExpectedDate;

                        msr.LeadTime = model.LeadTimeNumber;
                        msr.Remark = (!string.IsNullOrEmpty(model.Reason) ? model.Reason : string.Empty);
                        msr.IsUnloading = true;
                        if (!string.IsNullOrEmpty(model.WorkOrderNo))
                        {
                            msr.WorkOrderNo = model.WorkOrderNo;
                            msr.EquipmentNo = model.EquipmentNo;
                            msr.IsUnloading = false;
                        }

                        msr.MSRDetails = new System.Data.Objects.DataClasses.EntityCollection<MSRDetail>();
                        int iRow = 1;

                        foreach (var item in ListModel)
                        {
                            MSRDetail detail = new MSRDetail();
                            item.CostCode = (!string.IsNullOrEmpty(item.CostCode) ? item.CostCode : "");
                            detail.InjectFrom(item);
                            detail.ID = Guid.NewGuid();
                            detail.MSRId = msr.ID;
                            detail.LineNo = iRow;
                            if (!string.IsNullOrEmpty(model.WorkOrderNo))
                            {
                                detail.LineNo = item.LineNo;
                            }

                            detail.ItemNo = item.ItemNo;
                            detail.UOM = item.UOM;
                            detail.UnitCost = item.Cost;
                            detail.CostCode = item.CostCode;
                            if (!string.IsNullOrEmpty(detail.CostCode))
                            {
                                detail.CategoryCode = detail.CostCode.Trim();
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(department.CategoryCode))
                                {
                                    detail.CategoryCode = string.Format("{0}{1}", department.CategoryCode, item.Category);
                                }
                                else
                                {
                                    detail.CategoryCode = item.Category;
                                }
                            }


                            detail.equ_accpac = string.Empty;
                            detail.LocationId = string.Empty;
                            detail.LocationIdFrom = string.Empty;

                            if (!string.IsNullOrEmpty(model.WorkOrderNo))
                            {
                                detail.LocationId = item.Location;
                            }
                            else
                            {
                                detail.LocationIdFrom = item.Location;
                            }


                            EntityObjectExtension.SetPropertyUpdated(detail, true);
                            msr.MSRDetails.Add(detail);

                            iRow++;
                        }

                        msr.Save<MSR>();
                        DocumentApprovalLogic.MSRApprovalFlow(msr.MSRNo, msr.DepartmentId, msr.MSRDetails.Sum(x => (x.Qty * x.UnitCost)));
                        EmailLogic.EmailMSR(model);
                        transaction.Complete();
                        response.SetSuccess("MSR : {0} Has Been Saved", msr.MSRNo);
                        response.ResponseObject = msr.MSRNo;
                    }
                }

            }






            return response;
        }

        public static ResponseModel EditMSR(MSRModel model)
        {
            model.SetDateFrom();
            model.MSRDetail = model.MSRDetail.Where(x => !string.IsNullOrEmpty(x.ItemNo)).ToList();
            ResponseModel response = ValidationMSR(model);

            if (response.IsSuccess)
            {
                MSR msr = MSR.GetById(model.ID);
                Department department = Department.GetById(model.DepartmentId);
                var MSRDetails = MSRDetail.GetAllByMSRId(msr.ID);

                if (msr != null)
                {
                    List<Guid> ListIDExisting = new List<Guid>();
                    List<MSRDetail> MSRDetailNew = new List<MSRDetail>();
                    List<MSRDetail> MSRDetailUpdate = new List<MSRDetail>();

                    if (msr.MSRStatus == RequestStatus.NEW || msr.MSRStatus == RequestStatus.REVISION)
                    {
                        using (var transaction = new TransactionScope())
                        {
                            msr.CompanyID = department.CompanyId;
                            msr.DepartmentId = department.ID;
                            msr.CostCode = model.CostCenter;
                            msr.MSRStatus = RequestStatus.NEW;
                            msr.ExpectedDate = model.ExpectedDate;
                            msr.RequestType = model.MSRType;
                            msr.LeadTime = model.LeadTimeNumber;
                            msr.Remark = (!string.IsNullOrEmpty(model.Reason) ? model.Reason : string.Empty);
                            if (!string.IsNullOrEmpty(model.WorkOrderNo))
                            {
                                msr.WorkOrderNo = model.WorkOrderNo;
                                msr.EquipmentNo = model.EquipmentNo;
                                msr.MaintenanceCode = model.MaintenanceCode;
                            }


                            List<String> ListItemNo = model.MSRDetail.Select(x => x.ItemNo).ToList();
                            if (ListItemNo != null && ListItemNo.Count > 0)
                            {
                                var AccpacItems = AccpacDataContext.CurrentContext.ICITEMs.Where(x => ListItemNo.Contains(x.ITEMNO) || ListItemNo.Contains(x.FMTITEMNO)).ToList();
                                if (AccpacItems != null && AccpacItems.Count > 0)
                                {
                                    model.MSRDetail.ForEach(x =>
                                    {
                                        var AccpacItem = AccpacItems.FirstOrDefault(y => (y.ITEMNO == x.ItemNo || y.FMTITEMNO == x.ItemNo) && y.DESC != "");
                                        if (AccpacItem != null)
                                        {
                                            x.Description = AccpacItem.DESC.Trim();
                                        }
                                    });
                                }
                            }

                            var iRow = 1;
                            foreach (var item in model.MSRDetail)
                            {
                                MSRDetail detail = MSRDetails.FirstOrDefault(x => x.ID == item.ID);
                                if (detail == null)
                                {
                                    detail = new MSRDetail();
                                    detail.InjectFrom(item);
                                    detail.ID = Guid.NewGuid();
                                    detail.MSRId = msr.ID;
                                }

                                item.CostCode = (!string.IsNullOrEmpty(item.CostCode) ? item.CostCode : "");
                                detail.LineNo = iRow;
                                if (!string.IsNullOrEmpty(model.WorkOrderNo))
                                {
                                    detail.LineNo = item.LineNo;
                                }
                                detail.ItemNo = item.ItemNo;
                                detail.UOM = item.UOM;
                                detail.Qty = item.Qty;
                                detail.UnitCost = item.Cost;
                                detail.CostCode = item.CostCode;
                                if (!string.IsNullOrEmpty(detail.CostCode))
                                {
                                    detail.CategoryCode = detail.CostCode;
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(department.CategoryCode) && item.Category.Length < 4)
                                    {
                                        detail.CategoryCode = string.Format("{0}{1}", department.CategoryCode, item.Category);
                                    }
                                    else
                                    {
                                        detail.CategoryCode = item.Category;
                                    }
                                }

                                detail.equ_accpac = string.Empty;
                                detail.LocationId = string.Empty;
                                detail.LocationIdFrom = string.Empty;

                                if (!string.IsNullOrEmpty(model.WorkOrderNo))
                                {
                                    detail.LocationId = item.Location;
                                }
                                else
                                {
                                    detail.LocationIdFrom = item.Location;
                                }


                                EntityObjectExtension.SetPropertyUpdated(detail, true);
                                if (detail.ID == Guid.Empty)
                                {
                                    detail.ID = Guid.NewGuid();
                                    MSRDetailNew.Add(detail);
                                }
                                else
                                {
                                    MSRDetailUpdate.Add(detail);
                                    ListIDExisting.Add(detail.ID);
                                }

                                iRow++;
                            }

                            var detailToDelete = MSRDetails.Where(x => !ListIDExisting.Contains(x.ID)).ToList();
                            for (int i = 0; i < detailToDelete.Count; i++)
                            {
                                detailToDelete[i].Delete<MSRDetail>();
                            }

                            for (int i = 0; i < MSRDetailUpdate.Count; i++)
                            {
                                MSRDetailUpdate[i].UpdateSave<MSRDetail>();
                            }

                            for (int i = 0; i < MSRDetailNew.Count; i++)
                            {
                                MSRDetailNew[i].InsertSave<MSRDetail>();
                            }

                            msr.UpdateSave<MSR>();
                            EmailLogic.EmailMSR(model);
                            transaction.Complete();
                            response.SetSuccess("MSR : {0} Has Been Saved", msr.MSRNo);
                            response.ResponseObject = msr.MSRNo;
                        }
                    }
                    else
                    {
                        response.SetError("MSR {0} CANNOT BE EDIT", msr.MSRNo);
                    }
                }
            }

            return response;
        }

        /// <summary>
        /// Validation MSR When Create
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        static ResponseModel ValidationMSR(MSRModel model)
        {
            ResponseModel response = new ResponseModel();
            StringBuilder sbError = new StringBuilder();

            if (!RoleLogic.CheckRoleAccess(CurrentUser.NIKSITE, ROLE_CODE.MSR_ENTRY, model.DepartmentId))
            {
                sbError.AppendLine(string.Format("{0} Dont Have Access Created Document", CurrentUser.FULLNAME));
            }

            if (model.DepartmentId == Guid.Empty)
            {
                sbError.AppendLine("Department must be selected");
            }

            if (string.IsNullOrEmpty(model.CostCenter))
            {
                sbError.AppendLine("Cost Center must be selected");
            }

            if (string.IsNullOrEmpty(model.MaintenanceCode))
            {
                sbError.AppendLine("Maintenance Code must be selected");
            }

            if (!string.IsNullOrEmpty(model.WorkOrderNo))
            {
                if (string.IsNullOrEmpty(model.EquipmentNo))
                {
                    sbError.AppendLine("Equipment No must be selected");
                }
            }

            if (string.IsNullOrEmpty(model.ExpectedDateStr))
            {
                sbError.AppendLine("Expected Date must be selected");
            }

            if (string.IsNullOrEmpty(model.Manager.FullName))
            {
                sbError.AppendLine("Approval Manager not define");
            }

            var MSRDetails = model.MSRDetail.Where(x => !string.IsNullOrEmpty(x.ItemNo)).ToList();

            if (MSRDetails.Count < 1)
            {
                sbError.AppendLine("MSR dont have detail item");
            }
            else
            {
                foreach (var item in MSRDetails)
                {
                    if (model.MSRType == ((int)MSRType.Inventory).ToString())
                    {
                        if (string.IsNullOrEmpty(item.Location))
                        {
                            sbError.AppendLine(string.Format("{0} must be select location", item.ItemNo));
                        }
                        else
                        {
                            if (!(item.Qty > 0))
                            {
                                sbError.AppendLine(string.Format("{0} must be input qty", item.ItemNo));
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(model.WorkOrderNo))
                                {
                                    if (item.Category.Length < 5)
                                    {
                                        sbError.AppendLine(string.Format("{0} must be input qty", item.ItemNo));
                                    }
                                }
                            }
                        }

                    }
                    else
                    {
                        if (!(item.Qty > 0))
                        {
                            sbError.AppendLine(string.Format("{0} must be input qty", item.ItemNo));
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(item.CostCode))
                            {
                                sbError.AppendLine(string.Format("{0} must be select cost code", item.ItemNo));
                            }
                        }

                        item.Location = string.Empty;
                        item.LocationFrom = string.Empty;
                    }

                }
            }

            if (sbError.Length > 0)
            {
                response.SetError(sbError.ToString());
            }
            else
            {
                response.SetSuccess();
            }

            return response;
        }

        /// <summary>
        /// Get Approval For Document MSR
        /// </summary>
        /// <param name="DepartmentId"></param>
        /// <returns></returns>
        public static MSRModel GetApproval(Guid DepartmentId)
        {
            MSRModel model = new MSRModel();
            var department = Department.GetById(DepartmentId);

            var MSRApproval = RoleAccess.GetByCode(ROLE_CODE.MSR_APPROVAL);
            var MSRApprovalGM = RoleAccess.GetByCode(ROLE_CODE.MSR_APPROVAL_GM);

            var MSRApprovalUser = RoleAccessUser.GetByRoleAccessId(MSRApproval.ID)
                .Where(x => x.DepartmentId == department.ID)
                .Where(x => (x.IsDefault || x.DateFrom >= DateTime.Now && x.DateTo <= DateTime.Now))
                .OrderByDescending(x => x.UpdatedDate).FirstOrDefault();

            if (MSRApprovalUser != null)
            {
                model.Manager = UserLogic.GetByUserId(MSRApprovalUser.UserId);
            }

            var MSRApprovalUserGm = RoleAccessUser.GetByRoleAccessId(MSRApprovalGM.ID)
                .Where(x => x.DepartmentId == department.ID)
                .Where(x => (x.IsDefault || x.DateFrom >= DateTime.Now && x.DateTo <= DateTime.Now))
                .OrderByDescending(x => x.UpdatedDate).FirstOrDefault();

            if (MSRApprovalUserGm != null)
            {
                decimal TotalWithApprovalGM = 0;
                decimal.TryParse(MSRApprovalUserGm.AdditionalValue, out TotalWithApprovalGM);

                model.TotalWithApprovalGM = TotalWithApprovalGM;
                model.GeneralManagerSite = UserLogic.GetByUserId(MSRApprovalUserGm.UserId);
            }

            return model;
        }

        /// <summary>
        /// Update Status Approve, Rejected or Revision
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="StatusRequest"></param>
        /// <returns></returns>
        public static ResponseModel SetActionMSRStatusApproval(MSRModel param)
        {
            var response = new ResponseModel();
            MSR msr = MSR.GetById(param.ID);
            if (msr != null)
            {
                MSRModel model = MSRLogic.GetMSRModel(param.ID);
                using (var transaction = new TransactionScope())
                {
                    bool IsApproval = RoleLogic.CheckRoleAccess(CurrentUser.NIKSITE, ROLE_CODE.MSR_APPROVAL, msr.DepartmentId);
                    bool IsApprovalGM = RoleLogic.CheckRoleAccess(CurrentUser.NIKSITE, ROLE_CODE.MSR_APPROVAL_GM, msr.DepartmentId);
                    model.Manager = UserLogic.GetByNIK(CurrentUser.NIKSITE);

                    if (IsApproval || IsApprovalGM)
                    {
                        if (msr.MSRStatus == RequestStatus.NEW || msr.MSRStatus == RequestStatus.PARTIAL_APPROVED || msr.MSRStatus == RequestStatus.REVISION)
                        {
                            var RequestNew = EnumTable.GetDescriptionByCodeValue(EnumTable.EnumType.STATUS_MSR, param.StatusRequest);
                            if (param.StatusRequest == RequestStatus.APPROVED)
                            {
                                DocumentApprovalLogic.ApproveByDocumentNo(msr.MSRNo, CurrentUser.NIKSITE, ROLE_CODE.MSR_APPROVAL);
                                DocumentApprovalLogic.ApproveByDocumentNo(msr.MSRNo, CurrentUser.NIKSITE, ROLE_CODE.MSR_APPROVAL_GM);

                                var responseApproval = DocumentApprovalLogic.IsCompleteByNo(msr.MSRNo);
                                if (responseApproval.IsSuccess)
                                {
                                    msr.MSRStatus = param.StatusRequest;
                                    response.SetSuccess("MSR {0} has been update status", msr.MSRNo);
                                }
                                else
                                {
                                    if (IsApproval)
                                    {
                                        msr.MSRStatus = RequestStatus.PARTIAL_APPROVED;
                                        response.SetSuccess(responseApproval.Message);
                                    }
                                }

                                msr.UpdateSave<MSR>();
                                EmailLogic.EmailMSRIssued(model);
                            }
                            else
                            {
                                StringBuilder sbRemarks = new StringBuilder();
                                msr.MSRStatus = param.StatusRequest;
                                if (!string.IsNullOrEmpty(msr.Remark))
                                {
                                    sbRemarks.AppendLine(msr.Remark);
                                }

                                sbRemarks.AppendLine("");
                                sbRemarks.AppendLine(string.Format("[{0}] - {1}", RequestNew.EnumLabel, param.Reason));
                                msr.Remark = sbRemarks.ToString();
                                msr.UpdateSave<MSR>();

                                if (param.StatusRequest == RequestStatus.REVISION)
                                {
                                    EmailLogic.EmailMSRRevision(model);
                                }
                            }

                            transaction.Complete();
                            response.SetSuccess("MSR {0} has been update status {1}", msr.MSRNo, RequestNew.EnumLabel);
                        }
                        else
                        {
                            response.SetError("MSR {0} status must be New OR Revision", msr.MSRNo);
                        }
                    }
                    else
                    {
                        response.SetError("{0} dont have permission to approved", CurrentUser.FULLNAME);
                    }
                }
            }
            else
            {
                response.SetError("MSR Not Found");
            }

            return response;
        }

        /// <summary>
        /// Update Status Issued Complete or Partial
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="StatusRequest"></param>
        /// <returns></returns>
        public static ResponseModel SetActionMSRStatusIssued(Guid Id, string StatusRequest)
        {
            var response = new ResponseModel();
            MSR msr = MSR.GetById(Id);
            if (msr != null)
            {
                if (RoleLogic.CheckRoleAccess(CurrentUser.NIKSITE, ROLE_CODE.MSR_ISSUED, msr.DepartmentId))
                {
                    if (msr.MSRStatus == RequestStatus.PARTIAL_COMPLETED ||
                        msr.MSRStatus == RequestStatus.PARTIAL_TRANSFER ||
                        msr.MSRStatus == RequestStatus.PARTIAL_ISSUED ||
                        msr.MSRStatus == RequestStatus.FULL_TRANSFER ||
                        msr.MSRStatus == RequestStatus.PARTIAL_PURCHASING ||
                        msr.MSRStatus == RequestStatus.FULL_PURCHASING ||
                        msr.MSRStatus == RequestStatus.APPROVED)
                    {
                        msr.MSRStatus = StatusRequest;
                        msr.UpdateSave<MSR>();
                        response.SetSuccess("MSR {0} has been update status", msr.MSRNo);
                    }
                    else
                    {
                        response.SetError("MSR {0} status must be Complete OR Cancel", msr.MSRNo);
                    }
                }
                else
                {
                    response.SetError("{0} dont have permission to Issued", CurrentUser.FULLNAME);
                }
            }
            else
            {
                response.SetError("MSR Not Found");
            }

            return response;
        }

        public static LeadTimeModel GetLeadTimeByCode(string MaintenanceType)
        {
            var enumTable = EnumTable.GetDescriptionByCodeValue(CurrentUser.COMPANY, EnumTable.EnumType.JOB_PRIORITY, MaintenanceType);
            return GetLeadTime(enumTable);
        }

        /// <summary>
        /// Get Lead Time
        /// </summary>
        /// <param name="MaintenanceType"></param>
        /// <returns></returns>
        public static LeadTimeModel GetLeadTime(EnumTable enumTable)
        {
            LeadTimeModel model = new LeadTimeModel();
            if (enumTable != null)
            {
                model.MaintenanceType = enumTable.EnumValue;
                model.MinNumber = 0;
                model.MaxNumber = 0;

                if (!string.IsNullOrEmpty(enumTable.EnumLabel) &&
                    enumTable.EnumLabel.Contains("-"))
                {
                    var rangeNumber = enumTable.EnumLabel.Split('-');
                    if (rangeNumber.Length > 1)
                    {
                        int minValue = 0;
                        int.TryParse(rangeNumber[0], out minValue);
                        if (minValue > 0)
                        {
                            model.MinNumber = minValue;
                        }

                        int maxValue = 0;
                        int.TryParse(rangeNumber[1], out maxValue);
                        if (maxValue > 0)
                        {
                            model.MaxNumber = maxValue;
                        }
                    }
                }
                else
                {
                    int defaultValue = 0;
                    int.TryParse(enumTable.EnumLabel, out defaultValue);
                    if (defaultValue > 0)
                    {
                        model.MinNumber = defaultValue;
                        model.MaxNumber = defaultValue;
                    }
                }

                if (enumTable.EnumLabel.Contains("*"))
                {
                    model.IsFreeNumber = true;
                }
            }

            return model;
        }

        public static MSRIssuedSearchModel GetListIssued(MSRIssuedSearchModel model)
        {
            model.SetDateRange();
            var MSRIssueds = MSRIssued.GetAll();
            List<SelectListItem> ListDepartment = RoleLogic.GetListDepartmentUserByRole(CurrentUser.USERNAME, CurrentUser.COMPANY, model.ROLE_ACCESS);
            List<Guid> ListDepartmentId = ListDepartment
                .Where(x => !string.IsNullOrEmpty(x.Value))
                .Where(x => x.Value != Guid.Empty.ToString())
                .Select(x => Guid.Parse(x.Value)).ToList();

            MSRIssueds = MSRIssueds.Where(x => ListDepartmentId.Contains(x.DepartmentId));

            if (!string.IsNullOrEmpty(model.MSRNo))
            {
                var msr = MSR.GetByNo(model.MSRNo);
                if (msr != null)
                {
                    MSRIssueds = MSRIssueds.Where(x => x.MSRId == msr.ID);
                }
            }

            if (model.DateFrom.HasValue)
            {
                MSRIssueds = MSRIssueds.Where(x => x.CreatedDate >= model.DateFrom.Value);
            }

            if (model.DateTo.HasValue)
            {
                MSRIssueds = MSRIssueds.Where(x => x.CreatedDate <= model.DateTo.Value);
            }

            if (!string.IsNullOrEmpty(model.RequestStatus))
            {
                MSRIssueds = MSRIssueds.Where(x => x.IssuedType == model.RequestStatus);
            }

            if (model.CompanyId != null && model.CompanyId != Guid.Empty)
            {
                var department = Department.GetByCompanyId(model.CompanyId);
                var listDepartmentId = department.Select(x => x.ID).ToList();
                MSRIssueds = MSRIssueds.Where(x => listDepartmentId.Contains(x.DepartmentId));
            }

            if (model.DepartmentId != null && model.DepartmentId != Guid.Empty)
            {
                MSRIssueds = MSRIssueds.Where(x => x.DepartmentId == model.DepartmentId);
            }

            model.SetPager(MSRIssueds.Count());
            List<MSRIssuedListmodel> listData = MSRIssueds.OrderByDescending(x => x.CreatedDate)
                .Skip((model.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
                .Take(Common.SiteSettings.PAGE_SIZE_PAGING)
                .Select(x => new MSRIssuedListmodel()
                {
                    ID = x.ID,
                    MSRIssuedNo = x.MSRIssuedNo,
                    MSRId = x.MSRId,
                    MSRNo = x.MSR.MSRNo,
                    MSRIssuedDate = x.CreatedDate,
                    MSRIssuedDateStr = "",
                    CompanyId = x.CompanyId,
                    CompanyName = x.Company.CompanyName,
                    DepartmentId = x.DepartmentId,
                    DepartmentName = x.Department.DepartmentName,
                    Requestor = x.CreatedBy,
                    IssuedType = x.IssuedType,
                    Remarks = x.Remarks
                }).ToList();

            List<string> RequestorList = listData.Select(x => x.Requestor).Distinct().ToList();
            var requestors = UserLogin.GetAll().Where(x => RequestorList.Contains(x.NIKSite)).ToList();
            var statusMSRs = EnumTable.GetByCode(EnumTable.EnumType.STATUS_MSR).ToList();

            foreach (var item in listData)
            {
                var checkUser = requestors.FirstOrDefault(x => x.NIKSite == item.Requestor);
                if (checkUser != null)
                {
                    item.Requestor = checkUser.FullName;
                }

                item.LabelCssColor = CommonFunction.LabelStatusCssHelper(item.IssuedType);
                var statusMSR = statusMSRs.FirstOrDefault(x => x.EnumValue == item.IssuedType);
                if (statusMSR != null)
                {
                    item.IssuedType = statusMSR.EnumLabel;
                }

                item.MSRIssuedDateStr = item.MSRIssuedDate.JResourcesDate();
            }

            model.ListData = listData;

            return model;
        }

        /// <summary>
        /// Dismantle Process
        /// </summary>
        /// <param name="MSRId"></param>
        /// <returns></returns>
        public static ResponseModel DismantleProcess(Guid MSRId)
        {
            ResponseModel response = new ResponseModel();

            // Get Detail With Progress
            MSRIssuedModel MSRIssued = WarehouseLogic.GetDataByMSRId(MSRId, RequestStatus.TRANSFER);
            List<MSRIssuedDetailModel> MSRIssuedDetails = MSRIssued.MSRIssuedDetails;

            // Get Detail From Accpac 
            MSRModel MSRWorkOrder = WorkOrderLogic.GetWorkOrderById(MSRIssued.WorkOrderNo, MSRId);

            StringBuilder sbMessage = new StringBuilder();
            foreach (MSRDetailModel item in MSRWorkOrder.MSRDetail)
            {
                MSRIssuedDetailModel ItemMSRIssued = MSRIssuedDetails.FirstOrDefault(x => x.LineNo == item.LineNo);

                if (ItemMSRIssued != null)
                {
                    if (ItemMSRIssued.QtyTransfered > item.Qty)
                    {
                        sbMessage.AppendLine(string.Format("Item {0} has qty demand ({1}) exceeds that's been transferred ({2})",
                            item.ItemNo, item.Qty, ItemMSRIssued.QtyTransfered));
                    }
                    else
                    {
                        if (ItemMSRIssued.QtyRequest < item.Qty)
                        {
                            sbMessage.AppendLine(string.Format("Item {0} have changed the number of items exceeds the amount previously (Qty Before ({1})), Qty Update ({2}))",
                            item.ItemNo, ItemMSRIssued.QtyRequest, item.Qty));
                        }
                        else
                        {
                            item.QtyUnloading = ItemMSRIssued.QtyRequest;
                        }
                    }
                }
            }

            if (sbMessage.Length > 0)
            {
                response.SetError(sbMessage.ToString());
            }


            if (response.IsSuccess)
            {
                List<Guid> msrDetailIdDelete = new List<Guid>();
                foreach (var item in MSRIssuedDetails)
                {
                    MSRDetailModel msrDetail = MSRWorkOrder.MSRDetail.FirstOrDefault(x => x.LineNo == item.LineNo);
                    if (msrDetail == null)
                    {
                        msrDetailIdDelete.Add(item.ID);
                    }
                }

                MSR msr = MSR.GetById(MSRId);
                List<MSRDetail> MSRDetails = MSRDetail.GetAllByMSRId(msr.ID).ToList();
                Department department = Department.GetById(msr.DepartmentId);

                using (var transaction = new TransactionScope())
                {
                    foreach (var item in MSRDetails)
                    {
                        if (msrDetailIdDelete.Contains(item.ID))
                        {
                            item.IsDeleted = true;
                            item.Save<MSRDetail>();
                        }
                        else
                        {
                            item.Delete<MSRDetail>();
                        }
                    }

                    foreach (var item in MSRWorkOrder.MSRDetail)
                    {
                        MSRDetail detail = new MSRDetail();
                        detail.InjectFrom(item);
                        detail.ID = Guid.NewGuid();
                        detail.MSRId = MSRId;
                        detail.LineNo = item.LineNo;
                        detail.ItemNo = item.ItemNo;
                        detail.UOM = item.UOM;
                        detail.UnitCost = item.Cost;
                        detail.CostCode = item.CostCode;
                        detail.CategoryCode = string.Format("{0}{1}", department.CategoryCode, item.CostCode.Trim());
                        detail.equ_accpac = string.Empty;
                        detail.LocationIdFrom = string.Empty;
                        detail.LocationId = item.Location;

                        detail.Save<MSRDetail>();
                    }

                    msr.DismantleDate = DateTime.Now;
                    msr.IsUnloading = true;
                    msr.UpdatedDate = DateTime.Now;
                    msr.Save<MSR>();
                    //WarehouseLogic.MSRChangeStatusById(msr.ID);
                    transaction.Complete();
                    response.SetSuccess("MSR {0} Success to Dismantle", msr.MSRNo);
                }
            }


            return response;
        }


    }
}
