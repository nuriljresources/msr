﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JResources.Common;
using JResources.Common.Enum;
using JResources.Data;
using JResources.Data.Model;
using JResources.Data.Model.Accpac;

namespace JResources.BusinessLogic
{
    public class WorkOrderLogic
    {
        public static WorkOrderSearchModel SearchWorkOrder(WorkOrderSearchModel model)
        {
            model.ListData = new List<WorkOrderModel>();
            var vmdhs = AccpacDataContext.CurrentContext.VMDHs;

            var data = vmdhs.Where(x => x.WDSTATUS != 3 && x.WDSTATUS != 4 && x.WDSTATUS != 7);

            if (!string.IsNullOrEmpty(model.WorkOrderNo))
            {
                data = data.Where(x => x.TXDOCID.Contains(model.WorkOrderNo));
            }

            model.SetPager(data.Count());
            data = data.OrderByDescending(x => x.AUDTDATE)
                .Skip((model.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
                .Take(Common.SiteSettings.PAGE_SIZE_PAGING);

            List<decimal> listNMDOCID = data.Select(x => x.NMDOCID).ToList();
            var LISTWO = AccpacDataContext.CurrentContext.VMDTITs.Where(x => listNMDOCID.Contains(x.NMDOCID)).ToList();
            var LISTWO_SERVICEs = AccpacDataContext.CurrentContext.VMDTSVs.Where(x => listNMDOCID.Contains(x.NMDOCID)).ToList();
            if (LISTWO_SERVICEs != null && LISTWO_SERVICEs.Count > 0)
            {
                foreach (var LISTWO_SERVICE in LISTWO_SERVICEs)
                {
                    VMDTIT newItem = new VMDTIT();
                    newItem.NMDOCID = LISTWO_SERVICE.NMDOCID;
                    LISTWO.Add(newItem);
                }

            }

            foreach (var item in data)
            {
                WorkOrderModel workOrder = new WorkOrderModel();
                workOrder.WorkOrderId = item.NMDOCID;
                workOrder.WorkOrderNo = item.TXDOCID.Trim();
                workOrder.MaintainType = item.TXSITE;
                workOrder.AuditDateDecimal = item.AUDTDATE;

                var MSRCMMSs = MSR.GetByWorkOrderNo(workOrder.WorkOrderNo).Where(x => x.MSRStatus != RequestStatus.REJECTED);
                if (MSRCMMSs != null && MSRCMMSs.Count() > 0)
                {
                    int totalMSR = 0;
                    foreach (var MSRCMMS in MSRCMMSs)
                    {
                        totalMSR += MSRCMMS.MSRDetails.Count;
                    }

                    var WOSelect = LISTWO.Where(x => x.NMDOCID == item.NMDOCID);
                    if (WOSelect.Count() == totalMSR)
                    {
                        workOrder.IsUseComplete = true;
                    }
                }

                model.ListData.Add(workOrder);
            }

            return model;
        }

        public static MSRModel GetWorkOrderById(string WorkOrderNo, Guid DismantleId)
        {
            var model = new MSRModel();

            var vmdh = AccpacDataContext.CurrentContext.VMDHs.FirstOrDefault(x => x.TXDOCID.Contains(WorkOrderNo));
            if (vmdh != null)
            {
                model.WorkOrderNo = vmdh.TXDOCID.Trim();
                model.WorkOrderStatus = vmdh.WDSTATUS;

                var VMSTHD = AccpacDataContext.CurrentContext.VMSTHDs.FirstOrDefault(x => x.WDSTATUS == vmdh.WDSTATUS);
                if (VMSTHD != null)
                {
                    model.WorkOrderStatusLabel = VMSTHD.TXSTATUS.Trim();
                }

                DateTime ExpectedDate = DateTime.MinValue;
                if (DateHelper.DateFromAccpac(vmdh.DTEXPIRES, out ExpectedDate))
                {
                    model.ExpectedDate = ExpectedDate;
                    model.ExpectedDateStr = model.ExpectedDate.JResourcesDate();
                }

                model.Reason = (!string.IsNullOrEmpty(vmdh.TXREF) ? vmdh.TXREF : string.Empty);
                model.MaintenanceCode = vmdh.TXTYPE;
                var vmmtype = AccpacDataContext.CurrentContext.VMTYPEs.FirstOrDefault(x => x.TXTYPE == vmdh.TXTYPE);
                if (vmmtype != null)
                {
                    model.MaintenanceCodeLabel = string.Format("{0} - {1}", vmmtype.TXTYPE.Trim(), vmmtype.TXDESC.Trim());
                }

                Department department = Department.GetByMaintenanceCode(vmdh.TXSITE.Trim());
                model.CostCenter = vmdh.TXSHIPVIA.Trim();

                

                if (department != null)
                {
                    model.CompanyId = department.CompanyId;
                    model.DepartmentId = department.ID;

                    var costcenter = CSOPTFD.GetCostCenterByValue(vmdh.TXSHIPVIA);
                    if (costcenter != null)
                    {
                        model.CostCenter = costcenter.VALUE.Trim();
                        model.CostCenterLabel = CommonFunction.AddSpacesToSentence(costcenter.VDESC.Trim());
                    }

                }


                var vmdteqs = AccpacDataContext.CurrentContext.VMDTEQs;
                var vmdtits = AccpacDataContext.CurrentContext.VMDTITs;
                var vmdtphs = AccpacDataContext.CurrentContext.VMDTPHs;
                var icitems = AccpacDataContext.CurrentContext.ICITEMs;
                var vmdtsvs = AccpacDataContext.CurrentContext.VMDTSVs;
                var iccatgs = AccpacDataContext.CurrentContext.ICCATGs;

                var data = (from vmdtit in vmdtits
                            join vmdteq in vmdteqs on vmdtit.NMDOCID equals vmdteq.NMDOCID
                            join vmdtph in vmdtphs on vmdh.NMDOCID equals vmdtph.NMDOCID
                            join icitem in icitems on vmdtit.TXITEM equals icitem.FMTITEMNO
                            join iccatg in iccatgs on icitem.CATEGORY equals iccatg.CATEGORY
                            where vmdteq.NMDOCID == vmdh.NMDOCID
                            select new MSRDetailModel()
                            {
                                LineNo = vmdtit.WDTRANNUM,
                                EquipmentNo = vmdteq.TXEQUP,
                                ItemNo = icitem.ITEMNO,
                                Description = icitem.DESC,
                                Location = vmdtit.TXLOCATION,
                                Qty = vmdtit.QTESTIMATE,
                                CostCode = icitem.CATEGORY,
                                Category = icitem.CATEGORY,
                                CategoryLabel = iccatg.CATEGORY.Trim() + " - " + iccatg.DESC.Trim(),
                                UOM = icitem.STOCKUNIT
                            }).ToList();

                if (data != null && data.Count > 0)
                {
                    model.MSRType = ((int)MSRType.Inventory).ToString();
                }
                else
                {
                    data = (from vmdtsv in vmdtsvs
                            join vmdteq in vmdteqs on vmdtsv.NMDOCID equals vmdteq.NMDOCID
                            join vmdtph in vmdtphs on vmdh.NMDOCID equals vmdtph.NMDOCID
                            join icitem in icitems on vmdtsv.TXSERV equals icitem.FMTITEMNO
                            join iccatg in iccatgs on icitem.CATEGORY equals iccatg.CATEGORY
                            where vmdteq.NMDOCID == vmdh.NMDOCID
                            select new MSRDetailModel()
                            {
                                LineNo = vmdtsv.WDTRANNUM,
                                EquipmentNo = vmdteq.TXEQUP,
                                ItemNo = icitem.ITEMNO,
                                Description = icitem.DESC,
                                Location = "",
                                Qty = vmdtsv.QTCOSTEST,
                                CostCode = department.CategoryCode + icitem.CATEGORY.Trim(),
                                Category = department.CategoryCode + icitem.CATEGORY.Trim(),
                                CategoryLabel = iccatg.CATEGORY.Trim() + " - " + iccatg.DESC.Trim(),
                                UOM = icitem.STOCKUNIT
                            }).ToList();

                    if(data != null && data.Count > 0)
                    {
                        List<string> listCategory = data.Select(x => x.Category).ToList();
                        if(listCategory != null && listCategory.Count > 0)
                        {
                            var listCOA = ICCATG.GetAll().Where(x => listCategory.Contains(x.CATEGORY)).ToList();
                            if(listCOA != null && listCOA.Count > 0)
                            {
                                data.ForEach(x =>
                                {
                                    var categoryItem = listCOA.FirstOrDefault(y => y.CATEGORY.Trim() == x.Category.Trim());
                                    if (categoryItem != null)
                                    {
                                        x.CostCode = categoryItem.DAMAGEACCT;
                                    }
                                });

                                var costcodes = data.Select(x => x.CostCode.Trim()).ToList();
                                var glamfs = GLAMF.GetAll().Where(x => costcodes.Contains(x.ACCTFMTTD)).ToList();
                                if(glamfs != null && glamfs.Count > 0)
                                {

                                    data.ForEach(x =>
                                    {
                                        var costCodeItem = glamfs.FirstOrDefault(y => y.ACCTFMTTD.Trim() == x.CostCode.Trim());
                                        if (costCodeItem != null)
                                        {
                                            if(!string.IsNullOrEmpty(x.CostCode) && x.CostCode.Length > 5)
                                            {
                                                x.CostCodeLabel = costCodeItem.ACCTFMTTD.Trim() + " - " + costCodeItem.ACCTDESC.Trim();
                                            }
                                        }
                                    });
                                }
                            }
                        }

                    }

                    model.MSRType = ((int)MSRType.NonInventory).ToString();
                }


                var msrCheck = MSR.GetByWorkOrderNo(model.WorkOrderNo).Where(x => x.MSRStatus != RequestStatus.REJECTED);
                if (DismantleId != Guid.Empty)
                {
                    msrCheck = msrCheck.Where(x => x.ID != DismantleId);
                }

                if (msrCheck != null && msrCheck.Count() > 0)
                {
                    List<int> LineNoList = new List<int>();
                    foreach (var msr in msrCheck)
                    {
                        foreach (var msrDetail in msr.MSRDetails)
                        {
                            LineNoList.Add(msrDetail.LineNo);
                        }
                    }

                    data = data.Where(x => !LineNoList.Contains(x.LineNo)).ToList();
                }

                ItemLogic.SetQtyByLocation(data);


                if (data != null && data.Count > 0)
                {
                    data.ForEach(x => {
                        x.IdLookup = Guid.NewGuid().ToString().Replace("-", "");
                    });
                    model.EquipmentNo = data.FirstOrDefault().EquipmentNo;
                    data.ForEach(x => {
                        x.IdLookup = Guid.NewGuid().ToString().Replace("-", "");
                    });

                    model.MSRDetail = data;
                }

                model.LeadTime = MSRLogic.GetLeadTimeByCode(vmdh.TXTYPE);
                return model;
            }


            return null;
        }

        public static VM0050 GetWorkOrderAccpac(string WorkOrderNo)
        {
            VM0050 model = new VM0050();

            var vmdh = AccpacDataContext.CurrentContext.VMDHs.FirstOrDefault(x => x.TXDOCID.Contains(WorkOrderNo));

            if (vmdh != null)
            {
                model.TXDOCID = vmdh.TXDOCID;
                model.WDDOCTYPE = vmdh.WDDOCTYPE;

                var VMDTITs = AccpacDataContext.CurrentContext.VMDTITs.Where(x => x.NMDOCID == vmdh.NMDOCID).ToList();
                if (VMDTITs != null)
                {
                    foreach (var vmDTIT in VMDTITs)
                    {
                        VM0069 modelItem = new VM0069();

                        modelItem.WDTRANNUM = vmDTIT.WDTRANNUM;
                        modelItem.TXITEM = vmDTIT.TXITEM;
                        modelItem.ITEMNO = vmDTIT.TXITEM.Replace("-", "");
                        modelItem.TXDESC = vmDTIT.TXDESC;
                        modelItem.QTBACKORD = vmDTIT.QTBACKORD;
                        modelItem.QTESTIMATE = vmDTIT.QTESTIMATE;
                        modelItem.QTSUPPLIED = vmDTIT.QTSUPPLIED;
                        modelItem.QTINVOICED = vmDTIT.QTINVOICED;
                        
                        model.VM0069List.Add(modelItem);
                    }
                }

                //var VMDTSVs = AccpacDataContext.CurrentContext.VMDTSVs.Where(x => x.NMDOCID == vmdh.NMDOCID).ToList();
                //if (VMDTSVs != null)
                //{
                //    foreach (var wmDTSV in VMDTSVs)
                //    {
                //        VM0069 modelItem = new VM0069();

                //        modelItem.WDTRANNUM = wmDTSV.WDTRANNUM;
                //        modelItem.TXITEM = wmDTSV.TXSERV;
                //        modelItem.ITEMNO = wmDTSV.TXSERV.Replace("-", "");
                //        modelItem.TXDESC = wmDTSV.TXDESC;
                //        modelItem.QTBACKORD = wmDTSV.QTESTIMATE;
                //        modelItem.QTESTIMATE = wmDTSV.QTESTIMATE;
                //        modelItem.QTSUPPLIED = wmDTSV.QTSUPPLIED;
                //        model.VM0069List.Add(modelItem);
                //    }
                //}
            }

            return model;
        }
    }
}
