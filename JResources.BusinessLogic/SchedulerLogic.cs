﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using JResources.BusinessLogic.Accpac;
using JResources.BusinessLogic.AccpacConnect;
using JResources.Common;
using JResources.Common.Enum;
using JResources.Data;
using JResources.Data.Model;
using JResources.Data.Model.Accpac;

namespace JResources.BusinessLogic
{
    public class SchedulerLogic
    {
        public int SchedulerId { get; set; }
        public Guid CompanyId { get; set; }
        public string SchedulerType { get; set; }
        public string Data { get; set; }
        public string DataParameter { get; set; }
        public string LogException { get; set; }
        public StringBuilder LogMessage { get; set; }
        public string DocumentNo { get; set; }
        public string DocumentRefNo { get; set; }
        public bool IsManualPost { get; set; }

        public bool IsLastTry { get; set; }
        public string SchedulerByNIK { get; set; }


        // Additional Information for Report
        public bool IsFirstRunning { get; set; }
        public bool IsFoundAccpac { get; set; }
        public bool IsPosted { get; set; }
        public string LastLogMessage { get; set; }


        public SchedulerLogic(int _schedulerId)
        {
            SchedulerId = _schedulerId;
            LogMessage = new StringBuilder();
            Data = string.Empty;
            LogException = string.Empty;

            SchedulerTask item = SchedulerTask.GetById(SchedulerId);
            SchedulerType = item.SchedulerType;
            DocumentNo = item.DocumentNo;
            DataParameter = item.DataParameter;
            CompanyId = item.CompanyId;

            if (item.TryCount > 0) IsFirstRunning = false;
        }

        public void SetStart()
        {
            var task = SchedulerTask.GetById(SchedulerId);
            task.SchedulerStatus = SCHEDULER_STATUS.PROCESS;
            task.TryCount = task.TryCount + 1;
            task.Save<SchedulerTask>();

            SchedulerByNIK = task.CreatedBy;
        }

        public void SetFailed()
        {
            var task = SchedulerTask.GetById(SchedulerId);
            if (task.SchedulerStatus != SCHEDULER_STATUS.DONE)
            {
                if (IsManualPost)
                {
                    task.ManualPostDate = DateTime.Now;
                }

                task.SchedulerStatus = SCHEDULER_STATUS.FAILED;
                string logError = LogMessage.ToString();
                var enumError = CurrentDataContext.CurrentContext.EnumTables.FirstOrDefault(x => x.EnumCode == "ERROR_TYPE" && logError.Contains(x.EnumValue));
                if (enumError != null)
                {
                    task.LastLog = enumError.EnumLabel.Replace("[DOCUMENT_NO]", DocumentNo);
                }
                else
                {
                    task.LastLog = "Not identified";
                }

                task.Save<SchedulerTask>();
            }

            CreateLog();

            if (task.TryCount == SiteSettings.SIZE_SCHEDULER_TRY)
            {
                IsLastTry = true;
            }
        }

        public void SetSuccess()
        {
            var task = SchedulerTask.GetById(SchedulerId);
            task.SchedulerStatus = SCHEDULER_STATUS.DONE;

            if (IsManualPost)
            {
                task.ManualPostDate = DateTime.Now;
            }

            task.LastLog = string.Empty;

            if(!IsFoundAccpac)
            {
                var enumError = CurrentDataContext.CurrentContext.EnumTables.FirstOrDefault(x => x.EnumCode == "ERROR_TYPE" && x.EnumValue == "NOT_FOUND");
                if (enumError != null)
                {
                    task.LastLog = enumError.EnumLabel.Replace("[DOCUMENT_NO]", task.DocumentNo);
                }
                else
                {
                    task.LastLog = string.Format("Document {0} not found", task.DocumentNo);
                }

                task.SchedulerStatus = SCHEDULER_STATUS.FAILED;
            }

            if(!IsPosted)
            {
                var enumError = CurrentDataContext.CurrentContext.EnumTables.FirstOrDefault(x => x.EnumCode == "ERROR_TYPE" && x.EnumValue == "NOT_POSTED");
                if (enumError != null)
                {
                    task.LastLog = enumError.EnumLabel.Replace("[DOCUMENT_NO]", task.DocumentNo);
                }
                else
                {
                    task.LastLog = string.Format("Document {0} not found", task.DocumentNo);
                }
                task.TryCount = 3;
                task.SchedulerStatus = SCHEDULER_STATUS.FAILED;
            }

            task.Save<SchedulerTask>();

            if (SchedulerType == DocumentType.MSR)
            {
                MSRIssued msrIssued = MSRIssued.GetByIssuedNo(DocumentNo);
                msrIssued.IsTransferAccpac = true;
                if (!string.IsNullOrEmpty(DocumentRefNo))
                {
                    msrIssued.RQNNumber = DocumentRefNo;
                }
                msrIssued.Save<MSRIssued>();
            }
            else if (SchedulerType == DocumentType.FRF)
            {
                tfrf_master frf = tfrf_master.GetByFRFNo(DocumentNo);
                frf.is_transfer_accpac = true;
                frf.UpdateSave<tfrf_master>();
            }

            CreateLog();
        }

        public static void Create(Guid _companyId, string _schedulerType, string _documentNo)
        {
            Create(_companyId, _schedulerType, _documentNo, string.Empty);
        }

        public static void Create(Guid _companyId, string _schedulerType, string _documentNo, string dataParameter)
        {
            SchedulerTask task = new SchedulerTask();
            task.CompanyId = _companyId;
            task.DocumentNo = _documentNo;
            task.SchedulerType = _schedulerType;
            task.DataParameter = dataParameter;
            task.Save<SchedulerTask>();
        }

        private void CreateLog()
        {
            SchedulerLog log = new SchedulerLog();
            log.SchedulerTaskId = SchedulerId;
            log.Data = Data;
            log.Exception = LogException;
            log.LogMessage = LogMessage.ToString();

            log.Save<SchedulerLog>();
        }

        public ResponseModel RunningScheduler()
        {
            var response = new ResponseModel(false);
            SetStart();
            string RQNNumber = string.Empty;
            LogMessage.AppendLine("Start");
            IsFoundAccpac = true;
            IsPosted = true;

            try
            {
                if (SchedulerType == DocumentType.MSR)
                {
                    
                    MSRIssued msrIssued = MSRIssued.GetByIssuedNo(DocumentNo);
                    Company company = Company.GetById(msrIssued.CompanyId);
                    Console.WriteLine("Set Company " + company.CompanyCode);
                    Console.WriteLine("Start Item " + msrIssued.MSRIssuedNo);

                    DataRepositoryStoreAccpac.CurrentDataStore[DataRepositoryStoreAccpac.KEY_DATACONTEXT] = new AccpacEntities(JResources.Common.ConnectionManager.GetConnectionStringByCompanyCode(Common.DatabaseGroupName.ACCPAC_DATABASE, company.CompanyCode));

                    string IssuedType = msrIssued.IssuedType;

                    #region MSR ISSUED TYPE
                    switch (IssuedType)
                    {
                        case RequestStatus.ISSUED:
                            LogMessage.AppendLine("Issued Process");
                            Console.WriteLine("Issued Process");

                            if (string.IsNullOrEmpty(msrIssued.MSR.WorkOrderNo))
                            {
                                LogMessage.AppendLine("Issued Accpac");
                                Console.WriteLine("Issued Accpac");

                                IC0640 ic0640 = ShipmentLogic.GetAccpacModel(DocumentNo);
                                Data = Newtonsoft.Json.JsonConvert.SerializeObject(ic0640);

                                var icsheh = ICSHEH.GetByTransNo(DocumentNo);
                                if (icsheh != null && icsheh.STATUS == 20)
                                {
                                    response.SetSuccess();
                                }
                                else
                                {
                                    response = Shipment.Save(ic0640);

                                    if(response.IsSuccess)
                                    {
                                        var SHP = AccpacDataContext.CurrentContext.ICSHEHs.FirstOrDefault(x => x.DOCNUM.Contains(DocumentNo));
                                        if(SHP != null)
                                        {
                                            if(SHP.STATUS<3)
                                            {
                                                IsPosted = false;
                                            }
                                        } else
                                        {
                                            IsFoundAccpac = false;
                                        }
                                    }
                                }                               
                            }
                            else
                            {
                                LogMessage.AppendLine("Issued CMMS");
                                Console.WriteLine("Issued CMMS");

                                VM0050 vm0050 = VMDocumentLogic.GetAccpacModel(DocumentNo);
                                Data = Newtonsoft.Json.JsonConvert.SerializeObject(vm0050);
                                response = CMMS.Save(vm0050);

                                if (response.IsSuccess)
                                {
                                    System.Threading.Thread.Sleep(10000);
                                    var responseCompare = VMDocumentLogic.ComparasionCheck(DocumentNo, vm0050.DOCNUM);
                                    if (!responseCompare.IsSuccess)
                                    {
                                        LogMessage.AppendLine(responseCompare.Message);
                                        //IsPosted = false;
                                        SchedulerTask taskCheck = CurrentDataContext.CurrentContext.SchedulerTasks.FirstOrDefault(x => x.DocumentNo == DocumentNo && x.SchedulerType == DocumentType.ACCPAC_RECHECK);
                                        if(!(taskCheck != null && taskCheck.ID > 0))
                                        {
                                            SchedulerLogic.Create(CompanyId, DocumentType.ACCPAC_RECHECK, DocumentNo);
                                        }
                                    }
                                }


                            }
                            break;

                        case RequestStatus.TRANSFER:
                        case RequestStatus.REVERSE_TRANSFER:
                            LogMessage.AppendLine("Transfer Accpac");
                            Console.WriteLine("Transfer Accpac");

                            IC0740 ic0740 = TransferLogic.GetAccpacModel(DocumentNo);
                            Data = Newtonsoft.Json.JsonConvert.SerializeObject(ic0740);

                            var ictran = ICTRANH.GetByTransNo(DocumentNo);
                            if (ictran != null && ictran.POSTDATE > 0)
                            {
                                response.SetSuccess();
                            }
                            else
                            {
                                response = Transfer.Save(ic0740);
                                if (response.IsSuccess)
                                {
                                    var TRF = AccpacDataContext.CurrentContext.ICTREHs.FirstOrDefault(x => x.DOCNUM.Contains(DocumentNo));
                                    if (TRF != null)
                                    {
                                        if (TRF.STATUS < 3)
                                        {
                                            IsPosted = false;
                                        }
                                    }
                                    else
                                    {
                                        IsFoundAccpac = false;
                                    }
                                }

                            }

                            break;

                        case RequestStatus.PURCHASING:
                            LogMessage.AppendLine("RQN Accpac");
                            Console.WriteLine("RQN Accpac");

                            PT0040 pt0040 = JResources.BusinessLogic.Accpac.PurchasingLogic.GetAccpacModel(DocumentNo);
                            Data = Newtonsoft.Json.JsonConvert.SerializeObject(pt0040);
                            response = Purchasing.SaveRequisition(pt0040);

                            var RQN = AccpacDataContext.CurrentContext.PTPRHs.FirstOrDefault(x => x.REFERENCE.Contains(DocumentNo));
                            if (RQN == null)
                            {
                                IsFoundAccpac = false;
                            }

                            if (response.ResponseObject != null && !string.IsNullOrEmpty(response.ResponseObject.ToString()))
                            {
                                RQNNumber = response.ResponseObject.ToString();
                            }

                            break;
                    }
                    #endregion
                }
                else if(SchedulerType == DocumentType.FRF)
                {
                    #region FRF ISSUED
                    LogMessage.AppendLine("Issued Fuel");
                    Console.WriteLine("Issued Fuel");
                    tfrf_master frf = tfrf_master.GetByFRFNo(DocumentNo);
                    Company company = Company.GetByCode(frf.compid);
                    Console.WriteLine("Set Company " + company.CompanyCode);
                    Console.WriteLine("Start Item " + frf.frf_no);

                    DataRepositoryStoreAccpac.CurrentDataStore[DataRepositoryStoreAccpac.KEY_DATACONTEXT] = new AccpacEntities(JResources.Common.ConnectionManager.GetConnectionStringByCompanyCode(Common.DatabaseGroupName.ACCPAC_DATABASE, company.CompanyCode));

                    IC0640 ic0640 = ShipmentLogic.GetFuelModel(frf.ID);
                    Data = Newtonsoft.Json.JsonConvert.SerializeObject(ic0640);
                    response = Shipment.Save(ic0640);
                    if (response.IsSuccess)
                    {
                        var SHP = AccpacDataContext.CurrentContext.ICSHEHs.FirstOrDefault(x => x.DOCNUM.Contains(DocumentNo));
                        if (SHP != null)
                        {
                            if (SHP.STATUS < 3)
                            {
                                IsPosted = false;
                            }
                        }
                        else
                        {
                            IsFoundAccpac = false;
                        }
                    }

                    #endregion
                }
                else if(SchedulerType == DocumentType.FUEL)
                {
                    FuelProcessLogic process = new FuelProcessLogic(CompanyId, DocumentNo);

                    switch (process.TypeTransaction)
                    {
                        case "Refuelling":
                            IC0640 ic0640 = process.GetModelRefuel();
                            Data = Newtonsoft.Json.JsonConvert.SerializeObject(ic0640);
                            response = Shipment.Save(ic0640);
                            if(response.IsSuccess)
                            {
                                var SHP = AccpacDataContext.CurrentContext.ICSHEHs.FirstOrDefault(x => x.DOCNUM.Contains(DocumentNo));
                                if (SHP != null)
                                {
                                    if (SHP.STATUS < 3)
                                    {
                                        IsPosted = false;
                                    }
                                }
                                else
                                {
                                    IsFoundAccpac = false;
                                }
                            }

                            break;

                        case "Transfer":
                            IC0740 ic0740 = process.GetModelTransfer();
                            Data = Newtonsoft.Json.JsonConvert.SerializeObject(ic0740);
                            response = Transfer.Save(ic0740);
                            if (response.IsSuccess)
                            {
                                var TRF = AccpacDataContext.CurrentContext.ICTREHs.FirstOrDefault(x => x.DOCNUM.Contains(DocumentNo));
                                if (TRF != null)
                                {
                                    if (TRF.STATUS < 3)
                                    {
                                        IsPosted = false;
                                    }
                                }
                                else
                                {
                                    IsFoundAccpac = false;
                                }
                            }
                            break;

                        case "Receiving":
                            IC0RCV ic0RCV = process.GetModelReceipt();
                            Data = Newtonsoft.Json.JsonConvert.SerializeObject(ic0RCV);
                            response = Receipt.Save(ic0RCV);
                            break;
                        default:
                            response.SetError("Cannot find data from FTS");
                            break;
                    }


                }
                else if(SchedulerType == DocumentType.REPORT)
                {
                    //if(DocumentNo.Contains(REPORT_TYPE.OTIF))
                    //{
                    //    response = ReportLogic.OTIF(DataParameter, DocumentNo);
                    //}

                    if (DocumentNo.Contains(REPORT_TYPE.MSR_STATUS))
                    {
                        response = ReportLogic.MSR_STATUS_NEW(DataParameter, DocumentNo);
                    }

                }
                else if (SchedulerType == DocumentType.CEA_CLOSED)
                {
                    CEALogic.CloseOutstandingCEA();
                }
                else if(SchedulerType == DocumentType.CEA_REMINDER)
                {
                    var CEAs = V_CEA_HEADER.GetOutstanding(2).ToList();
                }
                else if (SchedulerType == DocumentType.ACCPAC_RECHECK)
                {
                    MSRIssued Issued = MSRIssued.GetByIssuedNo(DocumentNo);
                    MSR msr = MSR.GetById(Issued.MSRId);
                    Company company = Company.GetById(Issued.CompanyId);
                    DataRepositoryStoreAccpac.CurrentDataStore[DataRepositoryStoreAccpac.KEY_DATACONTEXT] = new AccpacEntities(JResources.Common.ConnectionManager.GetConnectionStringByCompanyCode(Common.DatabaseGroupName.ACCPAC_DATABASE, company.CompanyCode));

                    response = VMDocumentLogic.ComparasionCheck(DocumentNo, msr.WorkOrderNo);
                    SchedulerTask task = CurrentDataContext.CurrentContext.SchedulerTasks.FirstOrDefault(x => x.DocumentNo == DocumentNo && x.SchedulerType == DocumentType.MSR);
                    task.SchedulerStatus = response.IsSuccess ? SCHEDULER_STATUS.DONE : SCHEDULER_STATUS.FAILED;
                    task.UpdateSave<SchedulerTask>();
                }
                
                Console.WriteLine("Status : " + response.IsSuccess);
                if (response.IsSuccess)
                {
                    if (!string.IsNullOrEmpty(response.Message))
                    {
                        LogMessage.AppendLine(response.Message);
                    }

                    LogMessage.AppendLine("Accpac Process Done Success");
                    if (!string.IsNullOrEmpty(RQNNumber))
                    {
                        DocumentRefNo = RQNNumber;
                    }
                    response.SetSuccess(LogMessage.ToString());
                    SetSuccess();
                }
                else
                {
                    LogMessage.AppendLine(response.Message);
                    response.SetError(LogMessage.ToString());
                    SetFailed();

                    if (IsLastTry && SchedulerType == DocumentType.REPORT)
                    {
                        EmailLogic.EmailFailedError(SchedulerType, SchedulerByNIK, LogMessage.ToString());
                    }
                }

                Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(response));
            }
            catch (Exception ex)
            {
                LogMessage.AppendLine("Failed Exception : ");
                LogException = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                LogMessage.Append(LogException);
                SetFailed();
            }

            if (SchedulerType == DocumentType.FUEL)
            {
                #region Push To FTS
                try
                {
                    if (response.IsSuccess)
                    {
                        #region Condition Of Success
                        ADOHelper ExternalConnection = new ADOHelper(ConfigurationManager.ConnectionStrings[Constant.FTS_DATABSE].ConnectionString);
                        ExternalConnection.ExecNonQueryProc("sp_tdaily_transaction_fts_Return_MSR",
                            "@frf_no", DocumentNo,
                            "@posting_status", 3,
                            "@posting_log", LogMessage.ToString()
                            );
                        #endregion
                    }
                    else
                    {
                        #region Condition Failed
                        if (IsLastTry)
                        {
                            ADOHelper ExternalConnection = new ADOHelper(ConfigurationManager.ConnectionStrings[Constant.FTS_DATABSE].ConnectionString);
                            ExternalConnection.ExecNonQueryProc("sp_tdaily_transaction_fts_Return_MSR",
                                "@frf_no", DocumentNo,
                                "@posting_status", 2,
                                "@posting_log", LogMessage.ToString()
                                );
                        }
                        #endregion
                    }
                }
                catch
                {

                }
                #endregion
            }

            return response;
        }
    }
}
