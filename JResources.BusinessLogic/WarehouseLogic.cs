﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web.Mvc;
using JResources.BusinessLogic.Accpac;
using JResources.Common;
using JResources.Common.Enum;
using JResources.Data;
using JResources.Data.Model;
using JResources.Data.Model.Accpac;

using Omu.ValueInjecter;

namespace JResources.BusinessLogic
{
    public class WarehouseLogic
    {
        /// <summary>
        /// Get Data Full For Issued
        /// </summary>
        /// <param name="MSRId"></param>
        /// <returns></returns>
        public static MSRIssuedModel GetDataByMSRId(Guid MSRId, string IssuedType)
        {
            MSRIssuedModel model = new MSRIssuedModel();
            model.MSR = MSRLogic.GetMSRModel(MSRId);
            model.MSRId = model.MSR.ID;
            model.MSRNo = model.MSR.DocumentNo;
            model.WorkOrderNo = model.MSR.WorkOrderNo;
            model.CompanyId = model.MSR.CompanyId;
            model.DepartmentId = model.MSR.DepartmentId;
            model.CostCenter = model.MSR.CostCenter;

            bool isAcceptable = true;
            model.isAcceptable = isAcceptable;
            if (IssuedType == RequestStatus.CBE)
            {
                var isExistMSR = CurrentDataContext.CurrentContext.CBEs.Where(x => x.MSRId == model.MSR.ID && (x.DocumentStatus != CBEStatus.DISCARD || x.DocumentStatus != CBEStatus.REJECT)).ToList();

                if (isExistMSR != null)
                {
                    foreach (var msrDetail in isExistMSR)
                    {
                        var isExistCBE = CurrentDataContext.CurrentContext.MSRIssueds.FirstOrDefault(x => x.MSRIssuedNo == msrDetail.CBENo && x.RQNNumber == "");

                        if (isExistCBE != null)
                        {
                            isAcceptable = false;
                            model.isAcceptable = isAcceptable;
                            model.AcceptableStr = msrDetail.CBENo;
                        }

                        var isExistCBEOnIssued = CurrentDataContext.CurrentContext.MSRIssueds.FirstOrDefault(x => x.MSRIssuedNo == msrDetail.CBENo);

                        if (isExistCBEOnIssued == null)
                        {
                            isAcceptable = false;
                            model.isAcceptable = isAcceptable;
                            model.AcceptableStr = msrDetail.CBENo;
                        }
                    }
                }
            }

            if (IssuedType == RequestStatus.PURCHASING)
            {
                model.Remarks = model.MSR.Reason;
            }

            if (model.MSR.MSRType == ((int)JResources.Common.MSRType.NonInventory).ToString())
            {
                model.IsNonInventory = true;
            }

            SelectListItem ListMSRType = JResources.Common.EnumUtils<JResources.Common.MSRType>.GetDropDownList()
                .FirstOrDefault(x => x.Value == model.MSR.MSRType);
            model.MSR.MSRType = ListMSRType.Text;

            var costcenter = CostCenterLogic.GetCostCenterByDepartmentId(model.MSR.DepartmentId);
            if (costcenter != null)
            {
                var costSelect = costcenter.FirstOrDefault(x => x.Value.Trim() == model.MSR.CostCenter.Trim());
                if (costSelect != null)
                {
                    model.MSR.CostCenter = costSelect.Value;
                    model.MSR.CostCenterLabel = costSelect.Text;
                }
            }

            var MSRApproval = MSRLogic.GetApproval(model.MSR.DepartmentId);
            model.MSR.Manager = MSRApproval.Manager;
            model.MSR.TotalWithApprovalGM = MSRApproval.TotalWithApprovalGM;
            model.MSR.GeneralManagerSite = MSRApproval.GeneralManagerSite;

            MSR MSR = MSR.GetById(MSRId);
            var MSRDetails = MSR.MSRDetails.Where(x => !x.IsDeleted).ToList();

            foreach (var item in MSRDetails)
            {
                MSRIssuedDetailModel IssuedDetail = new MSRIssuedDetailModel();
                IssuedDetail.ID = item.ID;
                IssuedDetail.LineNo = item.LineNo;
                IssuedDetail.ItemNo = item.ItemNo;
                IssuedDetail.Description = item.Description;
                IssuedDetail.QtyRequest = item.Qty;
                IssuedDetail.Cost = item.UnitCost;
                IssuedDetail.CostCode = item.CostCode;
                IssuedDetail.Category = item.CostCode;
                IssuedDetail.UOM = item.UOM;

                if (!string.IsNullOrEmpty(MSR.WorkOrderNo))
                {
                    IssuedDetail.Location = item.LocationId;
                }

                if (string.IsNullOrEmpty(IssuedDetail.Location))
                {
                    IssuedDetail.Location = item.LocationIdFrom;
                }

                model.MSRIssuedDetails.Add(IssuedDetail);
            }

            #region SET QTY PROGRESS
            var MSRIssueds = MSRIssued.GetByMSRId(MSR.ID);
            if (MSRIssueds != null && MSRIssueds.Count() > 0)
            {
                foreach (var msrIssued in MSRIssueds)
                {
                    if (msrIssued.MSRIssuedDetails != null && msrIssued.MSRIssuedDetails.Count > 0)
                    {
                        foreach (var item in msrIssued.MSRIssuedDetails)
                        {
                            model.MSRIssuedDetails.ForEach(x =>
                            {
                                if (x.LineNo == item.LineNo)
                                {
                                    switch (msrIssued.IssuedType)
                                    {
                                        case RequestStatus.ISSUED:
                                            x.QtyDelivered += item.Qty;
                                            break;

                                        case RequestStatus.TRANSFER:
                                            x.QtyTransfered += item.Qty;
                                            break;

                                        case RequestStatus.REVERSE_TRANSFER:
                                            x.QtyTransfered -= item.Qty;
                                            break;

                                        case RequestStatus.CANCEL:
                                            x.QtyCancel += item.Qty;
                                            break;

                                        case RequestStatus.PURCHASING:
                                            x.QtyPurchase += item.Qty;
                                            break;
                                        case RequestStatus.CBE:
                                            x.QtyPurchase += item.Qty;
                                            break;
                                    }

                                }

                            });
                        }
                    }
                }
            }
            #endregion

            bool IsWorkOrder = false;
            if (IssuedType == RequestStatus.ISSUED && !string.IsNullOrEmpty(model.MSR.WorkOrderNo))
            {
                IsWorkOrder = true;
            }
            
            ItemLogic.SetLocationItem(model.MSRIssuedDetails, IsWorkOrder);
            model.MSRIssuedDetails = model.MSRIssuedDetails.OrderBy(x => x.LineNo).ToList();
            model.MSRIssuedDetails.ForEach(x =>
            {
                if (x.QtyIssuedOutstanding <= 0)
                {
                    x.Isfulfilled = true;
                }

                if (IssuedType == RequestStatus.TRANSFER)
                {
                    if (x.QtyTransferOutstanding <= 0)
                    {
                        x.Isfulfilled = true;
                    }
                }

                if (IssuedType == RequestStatus.CANCEL)
                {
                    if (x.QtyCancelOutstanding <= 0)
                    {
                        x.Isfulfilled = true;
                    }
                }

                if (IssuedType == RequestStatus.PURCHASING)
                {
                    if (x.QtyPurchaseOutstanding <= 0)
                    {
                        x.Isfulfilled = true;
                    }
                }

            });

            if (IssuedType == "CBE")
            {
                if (model.MSRIssuedDetails != null && model.MSRIssuedDetails.Count > 0)
                {
                    //model.MSRIssuedDetails = model.MSRIssuedDetails.Where(x => !x.Isfulfilled).ToList();
                    model.MSRIssuedDetails.ToList().ForEach(x =>
                    {
                        model.MSRIssuedDetails.ForEach(y =>
                        {
                            y.IsView = true;
                            if (x.LineNo == y.LineNo)
                            {
                                y.QtyPurchase += x.Qty;
                                y.QtyProcess += x.Qty;
                            }
                        });
                    });

                }

                //model.MSRIssuedDetails = model.MSRIssuedDetails.Where(x => x.Isfulfilled = false).ToList();
                model.MSRIssuedDetails = model.MSRIssuedDetails.Where(x => x.QtyRequest >= x.QtyPurchaseOutstanding).ToList();
                model.MSRIssuedDetails = model.MSRIssuedDetails.Where(x => x.QtyRequest >= x.QtyProcess).ToList();
            }

            if (IssuedType == RequestStatus.PURCHASING || IssuedType == RequestStatus.CBE)
            {
                #region LOCATION DROP ITEM
                string[] LOCATION_DROP = new string[] { "BWH01", "LWH01", "SWH01", "PWH01", "PWH02" };
                if (model.MSRIssuedDetails != null && model.MSRIssuedDetails.Count > 0)
                {
                    model.MSRIssuedDetails.ForEach(x => {

                        if (x.ItemLocations != null && x.ItemLocations.Count > 0)
                        {
                            var LocationFilter = new List<ItemLocationModel>();
                            foreach (var ItemLocation in x.ItemLocations)
                            {
                                if (LOCATION_DROP.Contains(ItemLocation.Location.Trim()))
                                {
                                    LocationFilter.Add(ItemLocation);
                                }
                            }

                            x.ItemLocations = LocationFilter;
                        }

                    
                    });
                }
                #endregion

            }

            var SupplierDetails = CurrentDataContext.CurrentContext.vw_CBE_Vendor.Where(x => x.compid == CurrentUser.COMPANY).ToList();
            foreach (var item in SupplierDetails)
            {
                MSRSupplier detail = new MSRSupplier();
                detail.SupplierCode = item.VENDORID;
                detail.SupplierName = item.VENDNAME;

                model.MSRSupplierDetails.Add(detail);
            }

            var CurrencyDetails = CurrentDataContext.CurrentContext.vw_CBE_Currency.ToList();
            foreach (var item in CurrencyDetails)
            {
                MSRCurrency detail = new MSRCurrency();
                detail.CurrencyCode = item.currtyp;
                detail.CurrencyName = item.currtyp;

                model.MSRCurrencyDetails.Add(detail);
            }

            return model;
        }

        /// <summary>
        /// Save Issued
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static ResponseModel CreateIssued(MSRIssuedModel model)
        {
            ResponseModel response = new ResponseModel();
            
            response = ValidationMSR(model);
            string Process = string.Empty;

            if (response.IsSuccess)
            {
                model.MSR = MSRLogic.GetMSRModel(model.MSRId);
                var IssuedTypeLabel = EnumTable.GetDescriptionByCodeValue(EnumTable.EnumType.STATUS_MSR, model.IssuedType).EnumLabel;
                
                VM0050 vm0050 = new VM0050();
                if (!string.IsNullOrEmpty(model.MSR.WorkOrderNo))
                {
                    vm0050 = WorkOrderLogic.GetWorkOrderAccpac(model.MSR.WorkOrderNo);
                }

                string RQNNumber = string.Empty;

                try
                {
                    using (var transaction = new TransactionScope())
                    {
                        #region DOCUMENT NUMBER RUNNING
                        Process = "DOCUMENT NUMBER RUNNING";
                        switch (model.IssuedType)
                        {
                            case RequestStatus.ISSUED:
                                model.MSRIssuedNo = DocumentNumberLogic.GetMSRIssuedNo(model.CompanyId);
                                break;

                            case RequestStatus.TRANSFER:
                                model.MSRIssuedNo = DocumentNumberLogic.GetMSRTransferNo(model.CompanyId);
                                break;

                            case RequestStatus.PURCHASING:
                                model.MSRIssuedNo = DocumentNumberLogic.GetMSRPurchaseNo(model.CompanyId);
                                break;

                            case RequestStatus.CANCEL:
                                model.MSRIssuedNo = DocumentNumberLogic.GetMSRCancelNo(model.CompanyId);
                                break;
                        }
                        #endregion

                        #region INITIAL DOCUMENT ISSUED
                        Process = "INITIAL DOCUMENT ISSUED";

                        MSRIssued newMSRIssued = new MSRIssued();
                        newMSRIssued.ID = Guid.NewGuid();
                        newMSRIssued.MSRIssuedNo = model.MSRIssuedNo;
                        newMSRIssued.CompanyId = model.CompanyId;
                        newMSRIssued.DepartmentId = model.DepartmentId;
                        newMSRIssued.MSRId = model.MSRId;
                        newMSRIssued.IssuedType = model.IssuedType;
                        newMSRIssued.Remarks = (string.IsNullOrEmpty(model.Remarks) ? string.Empty : model.Remarks);
                        newMSRIssued.RQNNumber = string.Empty;
                        newMSRIssued.WorkFlowCode = string.Empty;
                        newMSRIssued.CostCenter = string.Empty;
                        newMSRIssued.IsTransferAccpac = false;
                        if (model.IssuedType == RequestStatus.PURCHASING)
                        {
                            newMSRIssued.WorkFlowCode = model.WorkFlowCode;
                            newMSRIssued.CostCenter = model.CostCenter;
                        }

                        newMSRIssued.MSRIssuedDetails = new System.Data.Objects.DataClasses.EntityCollection<MSRIssuedDetail>();
                        foreach (var item in model.MSRIssuedDetails)
                        {
                            MSRIssuedDetail newMSRIssuedDetail = new MSRIssuedDetail();
                            newMSRIssuedDetail.ID = Guid.NewGuid();
                            newMSRIssuedDetail.MSRIssuedId = newMSRIssued.ID;
                            newMSRIssuedDetail.LineNo = item.LineNo;
                            newMSRIssuedDetail.ItemNo = item.ItemNo;
                            newMSRIssuedDetail.Description = item.Description;
                            newMSRIssuedDetail.LocationIdFrom = (string.IsNullOrEmpty(item.LocationFrom) ? string.Empty : item.LocationFrom);
                            newMSRIssuedDetail.LocationIdTo = (string.IsNullOrEmpty(item.Location) ? string.Empty : item.Location);
                            newMSRIssuedDetail.Qty = item.QtyProcess;
                            EntityObjectExtension.SetPropertyUpdated(newMSRIssuedDetail, true);
                            newMSRIssued.MSRIssuedDetails.Add(newMSRIssuedDetail);
                        }
                        #endregion

                        #region DOCUMENT ISSUED FINISH
                        Process = "DOCUMENT ISSUED FINISH";

                        if (response.IsSuccess)
                        {
                            if (newMSRIssued.IssuedType == RequestStatus.CANCEL)
                            {
                                newMSRIssued.IsTransferAccpac = true;
                            }

                            if (!string.IsNullOrEmpty(RQNNumber))
                            {
                                newMSRIssued.RQNNumber = RQNNumber;
                            }

                            if (newMSRIssued.IssuedType == RequestStatus.CANCEL)
                            {
                                DocumentApprovalLogic.CancelApprovalFlow(newMSRIssued.MSRIssuedNo, newMSRIssued.DepartmentId);
                            }
                            else
                            {
                                SchedulerLogic.Create(newMSRIssued.CompanyId, DocumentType.MSR, newMSRIssued.MSRIssuedNo);
                            }

                            newMSRIssued.Save<MSRIssued>();
                            string StatusMSR = MSRChangeStatusById(model.MSRId);

                            transaction.Complete();
                            response.AddMessageList(string.Format("MSR {0} NO : {1} has been created", IssuedTypeLabel, newMSRIssued.MSRIssuedNo));
                            response.AddMessageList(string.Format("MSR NO : {0} change status to {1}", model.MSRNo, StatusMSR));
                            response.SetSuccess();
                        }
                        else
                        {
                            response.AddMessageList(response.Message);
                            response.AddMessageList("Error When Submit To Accpac");
                            response.MessageToString();
                            response.SetError();
                        }
                        #endregion                       
                    }
                }
                catch (Exception ex)
                {
                    response.AddMessageList(Process);
                    if (ex.InnerException != null && ex.InnerException.Message != null)
                    {
                        response.AddMessageList(ex.InnerException.Message);
                    }
                    else
                    {
                        if (ex.Message != null)
                        {
                            response.AddMessageList(ex.Message);
                        }
                    }                    

                    response.MessageToString();
                    response.SetError();
                }
            }

            return response;
        }

        /// <summary>
        /// Process To Accpac
        /// </summary>
        /// <param name="IssuedId"></param>
        /// <returns></returns>
        public static ResponseModel ProcessToAccpac(Guid IssuedId)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                MSRIssued issued = MSRIssued.GetById(IssuedId);

                MSRIssuedModel model = GetMSRIssuedId(IssuedId);
                model.MSR = MSRLogic.GetMSRModel(model.MSRId);
                string RQNNumber = string.Empty;

                VM0050 vm0050 = new VM0050();
                if (!string.IsNullOrEmpty(model.MSR.WorkOrderNo))
                {
                    vm0050 = WorkOrderLogic.GetWorkOrderAccpac(model.MSR.WorkOrderNo);
                }

                switch (model.IssuedType)
                {
                    case RequestStatus.ISSUED:
                        if (string.IsNullOrEmpty(model.MSR.WorkOrderNo))
                        {
                            response = Accpac.ShipmentLogic.Save(model);
                        }
                        else
                        {
                            response = Accpac.VMDocumentLogic.Save(model, vm0050);
                        }
                        break;

                    case RequestStatus.TRANSFER:
                        response = Accpac.TransferLogic.Save(model);
                        break;

                    case RequestStatus.PURCHASING:
                        response = Accpac.PurchasingLogic.SaveRequisition(model);
                        if (response.ResponseObject != null && !string.IsNullOrEmpty(response.ResponseObject.ToString()))
                        {
                            RQNNumber = response.ResponseObject.ToString();
                        }
                        break;
                }

                if (response.IsSuccess)
                {
                    issued.IsTransferAccpac = true;
                    if (!string.IsNullOrEmpty(RQNNumber))
                    {
                        issued.RQNNumber = RQNNumber;
                    }

                    issued.UpdateSave<MSRIssued>();
                }
            }
            catch (Exception ex)
            {
                response.SetError(ex.Message);
            }

            return response;
        }

        /// <summary>
        /// For Reverse Transfer
        /// </summary>
        /// <param name="MSRIssuedId"></param>
        /// <returns></returns>
        public static ResponseModel ReverseTransfer(Guid MSRIssuedId)
        {
            ResponseModel response = new ResponseModel();

            try
            {
                MSRIssuedModel model = WarehouseLogic.GetMSRIssuedId(MSRIssuedId);
                MSRIssued msrIssued = MSRIssued.GetById(MSRIssuedId);

                if (model.IsCanReverse)
                {
                    foreach (var IssuedDetail in model.MSRIssuedDetails)
                    {
                        string LocationFrom = IssuedDetail.LocationFrom;
                        IssuedDetail.LocationFrom = IssuedDetail.Location;
                        IssuedDetail.Location = LocationFrom;
                        IssuedDetail.QtyRequest = IssuedDetail.QtyDelivered;
                        IssuedDetail.QtyProcess = IssuedDetail.QtyDelivered;
                    }

                    using (var transaction = new TransactionScope())
                    {
                        model.MSRIssuedNo = DocumentNumberLogic.GetMSRReverseTransferNo(model.CompanyId);

                        MSRIssued newMSRIssued = new MSRIssued();
                        newMSRIssued.ID = Guid.NewGuid();
                        newMSRIssued.MSRIssuedNo = model.MSRIssuedNo;
                        newMSRIssued.CompanyId = model.CompanyId;
                        newMSRIssued.DepartmentId = model.DepartmentId;
                        newMSRIssued.MSRId = model.MSRId;
                        newMSRIssued.IssuedType = RequestStatus.REVERSE_TRANSFER;
                        newMSRIssued.Remarks = (string.IsNullOrEmpty(model.Remarks) ? string.Empty : model.Remarks);
                        newMSRIssued.RQNNumber = string.Empty;
                        newMSRIssued.WorkFlowCode = string.Empty;
                        newMSRIssued.CostCenter = string.Empty;

                        newMSRIssued.MSRIssuedDetails = new System.Data.Objects.DataClasses.EntityCollection<MSRIssuedDetail>();
                        foreach (var item in model.MSRIssuedDetails)
                        {
                            MSRIssuedDetail newMSRIssuedDetail = new MSRIssuedDetail();
                            newMSRIssuedDetail.ID = Guid.NewGuid();
                            newMSRIssuedDetail.MSRIssuedId = newMSRIssued.ID;
                            newMSRIssuedDetail.LineNo = item.LineNo;
                            newMSRIssuedDetail.ItemNo = item.ItemNo;
                            newMSRIssuedDetail.Description = item.Description;
                            newMSRIssuedDetail.LocationIdFrom = (string.IsNullOrEmpty(item.LocationFrom) ? string.Empty : item.LocationFrom);
                            newMSRIssuedDetail.LocationIdTo = (string.IsNullOrEmpty(item.Location) ? string.Empty : item.Location);
                            newMSRIssuedDetail.Qty = item.QtyDelivered;
                            EntityObjectExtension.SetPropertyUpdated(newMSRIssuedDetail, true);
                            newMSRIssued.MSRIssuedDetails.Add(newMSRIssuedDetail);
                        }

                        newMSRIssued.Save<MSRIssued>();
                        msrIssued.ReverseId = newMSRIssued.ID;
                        msrIssued.UpdateSave<MSRIssued>();

                        SchedulerLogic.Create(newMSRIssued.CompanyId, DocumentType.MSR, newMSRIssued.MSRIssuedNo);
                        transaction.Complete();
                        response.SetSuccess("Reverse Transfer NO : {0} has been created", newMSRIssued.MSRIssuedNo);
                    }
                }
                else
                {
                    response.SetError("Already there is a reversal");
                }
            }
            catch (Exception ex)
            {
                response.SetError(ex.Message);
            }



            return response;
        }

        /// <summary>
        /// PURCHASE MODULE
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static ResponseModel PurchasedRequest(MSRIssuedModel model)
        {
            ResponseModel response = new ResponseModel();
            MSRIssued MSRIssued = MSRIssued.GetByIssuedNo(model.MSRIssuedNo);
            if (MSRIssued.IsTransferAccpac)
            {
                response.SetError("Purchase No {0} Is Already Transfer", MSRIssued.MSRIssuedNo);
            }

            if (response.IsSuccess)
            {
                response = ValidationMSR(model);
                if (response.IsSuccess)
                {
                    using (var transaction = new TransactionScope())
                    {
                        if (model.IssuedType == RequestStatus.PURCHASING)
                        {
                            if (!string.IsNullOrEmpty(model.MSR.WorkOrderNo))
                            {
                                response = Accpac.PurchasingLogic.SaveRequisition(model);
                            }
                            else
                            {
                                //response = AccpacLogic.MSRSaveShipment(model);
                            }

                            if (response.IsSuccess)
                            {
                                MSRIssued.IsTransferAccpac = true;
                                MSRIssued.Save<MSRIssued>();
                                string StatusMSR = MSRChangeStatusById(model.MSRId);

                                transaction.Complete();
                                response.AddMessageList(string.Format("MSR ISSUED NO : {0} has been created", MSRIssued.MSRIssuedNo));
                                response.AddMessageList(string.Format("MSR NO : {0} change status to {1}", model.MSRNo, StatusMSR));
                                response.SetSuccess();
                            }
                            else
                            {
                                response.SetError(response.Message + " " + "Error When Submit to Accpac");
                            }
                        }
                    }
                }
            }

            return response;
        }

        /// <summary>
        /// Validation MSR Detail
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static ResponseModel ValidationMSR(MSRIssuedModel model)
        {
            var response = new ResponseModel();
            StringBuilder sbError = new StringBuilder();

            if (model.MSRIssuedDetails != null)
            {
                MSR msrCheckMigration = MSR.GetById(model.MSRId);
                if (msrCheckMigration.IsDataMigration)
                {
                    sbError.AppendLine("MSR Migration From MSR 1.0 Cannot be process");
                }

                model.MSRIssuedDetails = model.MSRIssuedDetails.Where(x => !x.Isfulfilled && !x.IsRemove).ToList();
                var MSRIssuedDetails = model.MSRIssuedDetails.Where(x => !string.IsNullOrEmpty(x.ItemNo)).ToList();
                if (MSRIssuedDetails.Count < 1)
                {
                    sbError.AppendLine("MSR Issued dont have detail item");
                }
                else
                {
                    if (model.IssuedType == RequestStatus.ISSUED)
                    {
                        if (!string.IsNullOrEmpty(model.WorkOrderNo))
                        {
                            if (!model.MSR.IsUnloading)
                            {
                                sbError.AppendLine("MSR must be dismantle");
                            }
                        }
                    }

                    if (model.IssuedType == RequestStatus.PURCHASING)
                    {
                        if (string.IsNullOrEmpty(model.CostCenter))
                        {
                            sbError.AppendLine("Purchase Must Select Cost Center");
                        }

                        if (string.IsNullOrEmpty(model.WorkFlowCode))
                        {
                            sbError.AppendLine("Purchase Must Selected WorkFlow");
                        }
                    }

                    if (model.IssuedType == RequestStatus.CANCEL)
                    {
                        if (string.IsNullOrEmpty(model.Remarks))
                        {
                            sbError.AppendLine("Reason must fill");
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(model.WorkOrderNo))
                        {
                            List<int> validStatus = new List<int>() { WorkOrderStatus.COMPLETE, WorkOrderStatus.COMPLETE_ADMIN, WorkOrderStatus.REJECTED };
                            var vmdh = AccpacDataContext.CurrentContext.VMDHs.FirstOrDefault(x => x.TXDOCID.Contains(model.WorkOrderNo));
                            if (!(vmdh != null && !validStatus.Contains(vmdh.WDSTATUS)))
                            {
                                sbError.AppendLine("Work Order Status not valid to Issued/Transfer");
                            }
                        }
                    }

                    MSRIssuedDetails.ForEach(x =>
                    {
                        x.QtyDelivered = 0;
                        x.QtyTransfered = 0;
                        x.QtyCancel = 0;
                        x.QtyPurchase = 0;
                    });

                    #region SET QUANTITY
                    var msrIssueds = MSRIssued.GetByMSRId(model.MSRId);
                    if (msrIssueds != null && msrIssueds.Count() > 0)
                    {
                        foreach (var msrIssued in msrIssueds)
                        {
                            if (msrIssued.MSRIssuedDetails != null && msrIssued.MSRIssuedDetails.Count > 0)
                            {
                                foreach (var item in msrIssued.MSRIssuedDetails)
                                {
                                    MSRIssuedDetails.ForEach(x =>
                                    {
                                        if (x.LineNo == item.LineNo)
                                        {
                                            switch (msrIssued.IssuedType)
                                            {
                                                case RequestStatus.ISSUED:
                                                    x.QtyIssued += item.Qty;
                                                    x.QtyDelivered += item.Qty;
                                                    break;

                                                case RequestStatus.TRANSFER:
                                                    x.QtyTransfered += item.Qty;
                                                    break;

                                                case RequestStatus.REVERSE_TRANSFER:
                                                    x.QtyTransfered -= item.Qty;
                                                    break;
                                                
                                                case RequestStatus.PURCHASING:
                                                    x.QtyPurchase += item.Qty;
                                                    //x.QtyDelivered += item.Qty;
                                                    break;
                                                
                                                case RequestStatus.CANCEL:
                                                    x.QtyCancel += item.Qty;
                                                    x.QtyDelivered += item.Qty;
                                                    break;
                                            }

                                            
                                        }
                                    });
                                }
                            }
                        }
                    }
                    #endregion

                    List<ItemOutstandingPost_Result> listItemOutstandingMSR = new List<ItemOutstandingPost_Result>();
                    if ((model.IssuedType == RequestStatus.ISSUED && string.IsNullOrEmpty(model.MSR.WorkOrderNo)) || model.IssuedType == RequestStatus.TRANSFER)
                    {
                        List<string> listItemNo = MSRIssuedDetails.Select(x => x.ItemNo).ToList();
                        listItemOutstandingMSR = ICITEM.GetItemOutStandingMSR(listItemNo);
                    }


                    #region VALIDATION LINE DETAIL
                    foreach (var item in MSRIssuedDetails)
                    {
                        var itemGroups = MSRIssuedDetails.Where(x => x.ItemNo == item.ItemNo).ToList();
                        decimal TotalGroupItem = itemGroups.Sum(x => x.QtyProcess);

                        if (!(item.QtyProcess > 0))
                        {
                            sbError.AppendLine(string.Format("Line no {0} - Item {1} must be input qty process", item.LineNo, item.ItemNo));
                        }
                        else
                        {
                            if (model.IssuedType == RequestStatus.ISSUED)
                            {
                                #region ISSUED VALIDATION
                                if (string.IsNullOrEmpty(item.LocationFrom))
                                {
                                    sbError.AppendLine(string.Format("Line no {0} - Item {1} must be select location", item.LineNo, item.ItemNo));
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(model.MSR.WorkOrderNo))
                                    {
                                        #region WORK ORDER
                                        decimal TotalCanBeIssued = item.QtyTransfered - item.QtyIssued;
                                        if (item.QtyProcess > TotalCanBeIssued)
                                        {
                                            sbError.AppendLine(string.Format("Line no {0} - Item {1} qty cannot be more from qty transfered", item.LineNo, item.ItemNo));
                                        }
                                        else
                                        {
                                            ICILOC iciLOC = AccpacDataContext.CurrentContext.ICILOCs.FirstOrDefault(x => x.ITEMNO == item.ItemNo && x.LOCATION == item.LocationFrom);
                                            if (iciLOC != null)
                                            {
                                                if (item.QtyProcess > iciLOC.QTYONHAND)
                                                {
                                                    sbError.AppendLine(string.Format("Line no {0} - Item {1} is negative quantity to issued from location {2}, quantity stock only {3}", item.LineNo, item.ItemNo, item.LocationFrom, (iciLOC.QTYONHAND > 0 ? iciLOC.QTYONHAND.ToString("#.##") : "0")));
                                                }
                                                else
                                                {
                                                    if (itemGroups.Count > 1)
                                                    {
                                                        if (TotalGroupItem > iciLOC.QTYONHAND)
                                                        {
                                                            string msgGroupTotal = string.Format("Group Item {0} is negative quantity to issued from location {1}, quantity stock only {2}", item.ItemNo, item.LocationFrom, (iciLOC.QTYONHAND > 0 ? iciLOC.QTYONHAND.ToString("#.##") : "0"));
                                                            if (!sbError.ToString().Contains(msgGroupTotal))
                                                            {
                                                                sbError.AppendLine(msgGroupTotal);
                                                            }
                                                            break;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        decimal QtyOnHand = iciLOC.QTYONHAND;
                                                        var listItemOutstandings = listItemOutstandingMSR.Where(x => x.ItemNo == item.ItemNo && x.LocationIdFrom == item.LocationFrom);
                                                        foreach (var itemOutstanding in listItemOutstandings)
                                                        {
                                                            QtyOnHand = QtyOnHand - itemOutstanding.Qty;
                                                            if (item.QtyProcess > QtyOnHand)
                                                            {
                                                                sbError.AppendLine(string.Format("Line no {0} - Item {1} is negative quantity to issued from location {2}, because issued no {3} quantity stock only {4}", item.LineNo, item.ItemNo, item.LocationFrom, itemOutstanding.MSRIssuedNo, (QtyOnHand > 0 ? QtyOnHand.ToString("#.##") : "0")));
                                                                break;
                                                            }
                                                            else
                                                            {
                                                                if (TotalGroupItem > QtyOnHand)
                                                                {
                                                                    string msgGroupTotal = string.Format("Group Item {0} is negative quantity to issued from location {1}, quantity stock only {2}", item.ItemNo, item.LocationFrom, (iciLOC.QTYONHAND > 0 ? iciLOC.QTYONHAND.ToString("#.##") : "0"));
                                                                    if (!sbError.ToString().Contains(msgGroupTotal))
                                                                    {
                                                                        sbError.AppendLine(msgGroupTotal);
                                                                    }
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                sbError.AppendLine(string.Format("Line no {0} - Item {1} qty cannot be found quantity on location {2}", item.LineNo, item.ItemNo, item.LocationFrom));
                                            }
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        #region NON WORK ORDER
                                        if ((item.QtyDelivered + item.QtyProcess) > item.QtyRequest)
                                        {
                                            sbError.AppendLine(string.Format("Line no {0} - Item {1} qty cannot be more from qty request", item.LineNo, item.ItemNo));
                                        }
                                        else
                                        {
                                            ICILOC iciLOC = AccpacDataContext.CurrentContext.ICILOCs.FirstOrDefault(x => x.ITEMNO == item.ItemNo && x.LOCATION == item.LocationFrom);
                                            if (iciLOC != null)
                                            {
                                                if (item.QtyProcess > iciLOC.QTYONHAND)
                                                {
                                                    sbError.AppendLine(string.Format("Line no {0} - Item {1} is negative quantity to issued from location {2}, quantity stock only {3}", item.LineNo, item.ItemNo, item.LocationFrom, (iciLOC.QTYONHAND > 0 ? iciLOC.QTYONHAND.ToString("#.##") : "0")));
                                                }
                                                else
                                                {
                                                    if (itemGroups.Count > 1)
                                                    {
                                                        if (TotalGroupItem > iciLOC.QTYONHAND)
                                                        {
                                                            string msgGroupTotal = string.Format("Group Item {0} is negative quantity to issued from location {1}, quantity stock only {2}", item.ItemNo, item.LocationFrom, (iciLOC.QTYONHAND > 0 ? iciLOC.QTYONHAND.ToString("#.##") : "0"));
                                                            if (!sbError.ToString().Contains(msgGroupTotal))
                                                            {
                                                                sbError.AppendLine(msgGroupTotal);
                                                            }
                                                            break;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        decimal QtyOnHand = iciLOC.QTYONHAND;
                                                        var listItemOutstandings = listItemOutstandingMSR.Where(x => x.ItemNo == item.ItemNo && x.LocationIdFrom == item.LocationFrom);
                                                        foreach (var itemOutstanding in listItemOutstandings)
                                                        {
                                                            QtyOnHand = QtyOnHand - itemOutstanding.Qty;
                                                            if (item.QtyProcess > QtyOnHand)
                                                            {
                                                                sbError.AppendLine(string.Format("Line no {0} - Item {1} is negative quantity to issued from location {2}, because issued no {3} quantity stock only {4}", item.LineNo, item.ItemNo, item.LocationFrom, itemOutstanding.MSRIssuedNo, (QtyOnHand > 0 ? QtyOnHand.ToString("#.##") : "0")));
                                                                break;
                                                            }
                                                            else
                                                            {
                                                                if (TotalGroupItem > QtyOnHand)
                                                                {
                                                                    string msgGroupTotal = string.Format("Group Item {0} is negative quantity to issued from location {1}, quantity stock only {2}", item.ItemNo, item.LocationFrom, (iciLOC.QTYONHAND > 0 ? iciLOC.QTYONHAND.ToString("#.##") : "0"));
                                                                    if (!sbError.ToString().Contains(msgGroupTotal))
                                                                    {
                                                                        sbError.AppendLine(msgGroupTotal);
                                                                    }
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                sbError.AppendLine(string.Format("Line no {0} - Item {1} qty cannot be found quantity on location {2}", item.LineNo, item.ItemNo, item.LocationFrom));
                                            }
                                        }
                                        #endregion
                                    }
                                }
                                #endregion
                            }
                            else if (model.IssuedType == RequestStatus.PURCHASING)
                            {
                                #region PURCHASING
                                if ((item.QtyDelivered + item.QtyPurchase + item.QtyProcess) > item.QtyRequest)
                                {
                                    sbError.AppendLine(string.Format("Line no {0} - Item {1} qty cannot be more from qty purchase outstanding", item.LineNo, item.ItemNo));
                                }
                                #endregion
                            }
                            else if (model.IssuedType == RequestStatus.CANCEL)
                            {
                                #region CANCEL VALIDATION
                                decimal Qty = item.QtyIssued;
                                if (item.QtyIssued != item.QtyTransfered)
                                {
                                    if (item.QtyIssued > item.QtyTransfered)
                                    {
                                        Qty = item.QtyIssued;
                                    }
                                    else
                                    {
                                        Qty = item.QtyTransfered;
                                    }
                                }

                                if ((Qty + item.QtyCancel + item.QtyProcess) > item.QtyRequest)
                                {
                                    sbError.AppendLine(string.Format("Line no {0} - Item {1} qty cannot be more from qty cancel outstanding", item.LineNo, item.ItemNo));
                                }
                                #endregion
                            }
                            else if (model.IssuedType == RequestStatus.TRANSFER)
                            {
                                #region TRANSFER VALIDATION
                                if (string.IsNullOrEmpty(item.LocationFrom))
                                {
                                    sbError.AppendLine(string.Format("Line no {0} - Item {1} must be select location from", item.LineNo, item.ItemNo));
                                }

                                if (string.IsNullOrEmpty(item.Location))
                                {
                                    sbError.AppendLine(string.Format("Line no {0} - Item {1} must be select location from", item.LineNo, item.ItemNo));
                                }


                                if (!string.IsNullOrEmpty(item.LocationFrom) && !string.IsNullOrEmpty(item.Location))
                                {

                                    if (item.LocationFrom == item.Location)
                                    {
                                        sbError.AppendLine(string.Format("Line no {0} - Item {1} You cannot transfer within the same location", item.LineNo, item.ItemNo));
                                    }
                                    else
                                    {
                                        if ((item.QtyTransfered + item.QtyCancel + item.QtyProcess) > item.QtyRequest)
                                        {
                                            sbError.AppendLine(string.Format("Line no {0} - Item {1} qty cannot be more from qty transfered outstanding", item.LineNo, item.ItemNo));
                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(item.LocationFrom) && !string.IsNullOrEmpty(item.Location))
                                            {
                                                if ((item.QtyDelivered + item.QtyProcess) > item.QtyRequest)
                                                {
                                                    sbError.AppendLine(string.Format("Line no {0} - Item {1} qty cannot be more from qty request", item.LineNo, item.ItemNo));
                                                }
                                                else
                                                {
                                                    ICILOC iciLOC = AccpacDataContext.CurrentContext.ICILOCs.FirstOrDefault(x => x.ITEMNO == item.ItemNo && x.LOCATION == item.LocationFrom);
                                                    if (iciLOC != null)
                                                    {
                                                        if (item.QtyProcess > iciLOC.QTYONHAND)
                                                        {
                                                            sbError.AppendLine(string.Format("Line no {0} - Item {1} is insufficient quantity to transfer from location {2}, quantity stock only {3}", item.LineNo, item.ItemNo, item.LocationFrom, (iciLOC.QTYONHAND > 0 ? iciLOC.QTYONHAND.ToString("#.##") : "0")));
                                                        }
                                                        else
                                                        {
                                                            if (itemGroups.Count > 1)
                                                            {
                                                                if (TotalGroupItem > iciLOC.QTYONHAND)
                                                                {
                                                                    string msgGroupTotal = string.Format("Group Item {0} is negative quantity to issued from location {1}, quantity stock only {2}", item.ItemNo, item.LocationFrom, (iciLOC.QTYONHAND > 0 ? iciLOC.QTYONHAND.ToString("#.##") : "0"));
                                                                    if (!sbError.ToString().Contains(msgGroupTotal))
                                                                    {
                                                                        sbError.AppendLine(msgGroupTotal);
                                                                    }
                                                                    break;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                decimal QtyOnHand = iciLOC.QTYONHAND;
                                                                var listItemOutstandings = listItemOutstandingMSR.Where(x => x.ItemNo == item.ItemNo && x.LocationIdFrom == item.LocationFrom);
                                                                foreach (var itemOutstanding in listItemOutstandings)
                                                                {
                                                                    QtyOnHand = QtyOnHand - itemOutstanding.Qty;
                                                                    if (item.QtyProcess > QtyOnHand)
                                                                    {
                                                                        sbError.AppendLine(string.Format("Line no {0} - Item {1} is negative quantity to transfer from location {2}, because transfer no {3} quantity stock only {4}", item.LineNo, item.ItemNo, item.LocationFrom, itemOutstanding.MSRIssuedNo, (QtyOnHand > 0 ? QtyOnHand.ToString("#.##") : "0")));
                                                                    }
                                                                    else
                                                                    {
                                                                        if (TotalGroupItem > QtyOnHand)
                                                                        {
                                                                            string msgGroupTotal = string.Format("Group Item {0} is negative quantity to issued from location {1}, quantity stock only {2}", item.ItemNo, item.LocationFrom, (iciLOC.QTYONHAND > 0 ? iciLOC.QTYONHAND.ToString("#.##") : "0"));
                                                                            if (!sbError.ToString().Contains(msgGroupTotal))
                                                                            {
                                                                                sbError.AppendLine(msgGroupTotal);
                                                                            }
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                            }                                                            
                                                        }
                                                    }
                                                    else
                                                    {
                                                        sbError.AppendLine(string.Format("Line no {0} - Item {1} qty cannot be found quantity on location {2}", item.LineNo, item.ItemNo, item.LocationFrom));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                if ((item.QtyDelivered + item.QtyProcess) > item.QtyRequest)
                                {
                                    sbError.AppendLine(string.Format("Line no {0} - Item {1} qty cannot be more from qty request", item.LineNo, item.ItemNo));
                                }
                            }
                        }
                    }
                    #endregion

                }
            }
            else
            {
                sbError.AppendLine("MSR Issued dont have detail item");
            }

            if (sbError.Length > 0)
            {
                response.SetError(sbError.ToString());
            }

            return response;
        }

        public static ResponseModel ValidationFRF(List<tfrf_detail> list)
        {
            var response = new ResponseModel();
            Dictionary<string, decimal> dicCheckItems = new Dictionary<string, decimal>();
            foreach (var item in list)
            {

            }

            return response;
        }

        /// <summary>
        /// Change Status MSR To Issued Or Cancel
        /// </summary>
        /// <param name="MSRId"></param>
        /// <returns></returns>
        public static string MSRChangeStatusById(Guid MSRId)
        {
            var MSRData = MSR.GetById(MSRId);
            var MSRDetails = MSRData.MSRDetails.Where(x => !x.IsDeleted).ToList();
            var MSRIssueds = MSRIssued.GetByMSRId(MSRId);
            List<MSRIssuedDetailModel> ITEMS = new List<MSRIssuedDetailModel>();

            bool IsHavePurchased = false;
            bool IsHaveTransfer = false;
            bool IsHaveIssued = false;

            MSRDetails.ForEach(x =>
            {
                MSRIssuedDetailModel ItemIssued = new MSRIssuedDetailModel();
                ItemIssued.LineNo = x.LineNo;
                ItemIssued.ItemNo = x.ItemNo;
                ItemIssued.QtyRequest = x.Qty;

                ITEMS.Add(ItemIssued);
            });

            foreach (var msrIssued in MSRIssueds)
            {
                foreach (var item in msrIssued.MSRIssuedDetails)
                {
                    ITEMS.ForEach(x =>
                    {
                        if (x.LineNo == item.LineNo)
                        {
                            if (msrIssued.IssuedType == RequestStatus.TRANSFER)
                            {
                                x.QtyTransfered += item.Qty;
                                IsHaveTransfer = true;
                            }

                            if (msrIssued.IssuedType == RequestStatus.CANCEL)
                            {
                                //x.QtyCancel += item.Qty;
                                //x.QtyDelivered += item.Qty;

                                var docCancelApps = DocumentApproval.GetByDocumentNo(msrIssued.MSRIssuedNo).ToList();
                                if (docCancelApps != null && docCancelApps.Count > 0)
                                {
                                    var docCancelAppsPending = docCancelApps.Where(y => !y.IsFinish).ToList();
                                    if (!(docCancelAppsPending != null && docCancelAppsPending.Count > 0))
                                    {
                                        x.QtyCancel += item.Qty;
                                        x.QtyDelivered += item.Qty;
                                    }
                                }                                
                            }

                            if (msrIssued.IssuedType == RequestStatus.ISSUED)
                            {
                                x.QtyProcess += item.Qty;
                                x.QtyDelivered += item.Qty;

                                IsHaveIssued = true;
                            }

                            if (msrIssued.IssuedType == RequestStatus.PURCHASING)
                            {
                                if (msrIssued.IsTransferAccpac)
                                {
                                    x.QtyProcess += item.Qty;
                                }
                                else
                                {
                                    x.QtyPurchase += item.Qty;
                                }

                                IsHavePurchased = true;
                                //x.QtyDelivered += item.Qty;
                            }
                            
                        }
                    });

                }
            }

            bool IsCompleted = true;
            bool IsCancelAll = true;
            bool IsFullPurchased = true;
            bool IsFullTransfer = true;
            bool IsHaveOutstanding = false;

            ITEMS.ForEach(x =>
            {
                if (MSRData.RequestType == ((int)MSRType.Inventory).ToString() && !MSRData.IsReOrderPoint)
                {
                    if (x.QtyDelivered != x.QtyRequest)
                    {
                        IsCompleted = false;
                    }
                }
                else
                {
                    if (x.QtyPurchase != x.QtyRequest)
                    {
                        IsCompleted = false;
                    }
                }

                if (!MSRData.IsReOrderPoint && x.QtyProcess > 0)
                {
                    IsCancelAll = false;
                }

                if (x.QtyPurchase > 0)
                {
                    IsHavePurchased = true;
                }

                if (x.QtyIssuedOutstanding > 0)
                {
                    IsHaveOutstanding = true;
                }

                if (x.QtyTransferOutstanding > 0)
                {
                    IsFullTransfer = false;
                }

                if (x.QtyPurchaseOutstanding > 0)
                {
                    IsFullPurchased = false;
                }
            });

            string STATUS = RequestStatus.PARTIAL_ISSUED;
            if (IsCompleted)
            {
                if (IsCancelAll)
                {
                    STATUS = RequestStatus.COMPLETED;
                }
                else
                {
                    if (!IsHaveOutstanding)
                    {
                        STATUS = RequestStatus.COMPLETED;
                    }
                }


                if (MSRData.IsReOrderPoint && IsHavePurchased)
                {
                    STATUS = RequestStatus.COMPLETED;
                }

                if (MSRData.RequestType == ((int)MSRType.NonInventory).ToString())
                {
                    STATUS = RequestStatus.COMPLETED;
                }
            }
            else
            {
                if (IsHavePurchased)
                {
                    if (IsFullPurchased)
                    {
                        STATUS = RequestStatus.FULL_PURCHASING;
                    }
                    else
                    {
                        STATUS = RequestStatus.PARTIAL_PURCHASING;
                    }
                }

                if (IsHaveTransfer)
                {
                    if (IsFullTransfer)
                    {
                        STATUS = RequestStatus.FULL_TRANSFER;
                    }
                    else
                    {
                        STATUS = RequestStatus.PARTIAL_TRANSFER;
                    }
                }

                if (IsHaveIssued)
                {
                    STATUS = RequestStatus.PARTIAL_ISSUED;
                }
            }


            MSRLogic.SetActionMSRStatusIssued(MSRId, STATUS);

            var RequestNew = EnumTable.GetDescriptionByCodeValue(EnumTable.EnumType.STATUS_MSR, STATUS);
            return RequestNew.EnumLabel;
        }

        /// <summary>
        /// Get List MSR Issued
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static MSRIssuedSearchModel GetListIssued(MSRIssuedSearchModel model)
        {
            model.SetDateRange();
            var MSRIssueds = MSRIssued.GetAll();

            var roleCookies = CookieManager.Get(COOKIES_NAME.ROLE);
            var user = CurrentUser.Model;
            user.SetRolesFromCookies(roleCookies);

            if (!user.IsAdministrator)
            {
                List<SelectListItem> ListDepartment = RoleLogic.GetListDepartmentUserByRole(CurrentUser.USERNAME, CurrentUser.COMPANY, model.ROLE_ACCESS);
                List<Guid> ListDepartmentId = ListDepartment
                    .Where(x => !string.IsNullOrEmpty(x.Value))
                    .Where(x => x.Value != Guid.Empty.ToString())
                    .Select(x => Guid.Parse(x.Value)).ToList();

                MSRIssueds = MSRIssueds.Where(x => ListDepartmentId.Contains(x.DepartmentId));
            }

            if (model.IsPendingCancel)
            {
                MSRIssueds = MSRIssueds.Where(x => x.IssuedType == RequestStatus.CANCEL && x.IssuedStatus != 3 && x.IssuedStatus != 2);
            }

            if (!string.IsNullOrEmpty(model.MSRIssuedNo))
            {
                MSRIssueds = MSRIssueds.Where(x => x.MSRIssuedNo.Contains(model.MSRIssuedNo));
            }

            if (!string.IsNullOrEmpty(model.MSRNo))
            {
                var msr = MSR.GetByNo(model.MSRNo);
                if (msr != null)
                {
                    MSRIssueds = MSRIssueds.Where(x => x.MSRId == msr.ID);
                }
            }

            if (model.IssuedType != null && model.IssuedType.Length > 0)
            {
                MSRIssueds = MSRIssueds.Where(x => model.IssuedType.Contains(x.IssuedType));
            }

            if (model.DateFrom.HasValue)
            {
                MSRIssueds = MSRIssueds.Where(x => x.CreatedDate >= model.DateFrom.Value);
            }

            if (model.DateTo.HasValue)
            {
                MSRIssueds = MSRIssueds.Where(x => x.CreatedDate <= model.DateTo.Value);
            }

            if (!string.IsNullOrEmpty(model.RequestStatus))
            {
                MSRIssueds = MSRIssueds.Where(x => x.IssuedType == model.RequestStatus);
            }

            if (model.CompanyId != null && model.CompanyId != Guid.Empty)
            {
                var department = Department.GetByCompanyId(model.CompanyId);
                var listDepartmentId = department.Select(x => x.ID).ToList();
                MSRIssueds = MSRIssueds.Where(x => listDepartmentId.Contains(x.DepartmentId));
            }

            if (model.DepartmentId != null && model.DepartmentId != Guid.Empty)
            {
                MSRIssueds = MSRIssueds.Where(x => x.DepartmentId == model.DepartmentId);
            }

            model.SetPager(MSRIssueds.Count());
            List<MSRIssuedListmodel> listData = MSRIssueds.OrderByDescending(x => x.CreatedDate)
                .Skip((model.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
                .Take(Common.SiteSettings.PAGE_SIZE_PAGING)
                .Select(x => new MSRIssuedListmodel()
                {
                    ID = x.ID,
                    MSRIssuedNo = x.MSRIssuedNo,
                    MSRId = x.MSRId,
                    MSRNo = x.MSR.MSRNo,
                    MSRIssuedDate = x.CreatedDate,
                    MSRIssuedDateStr = "",
                    CompanyId = x.CompanyId,
                    CompanyName = x.Company.CompanyName,
                    DepartmentId = x.DepartmentId,
                    RQNNo = x.RQNNumber,
                    DepartmentName = x.Department.DepartmentName,
                    Requestor = x.CreatedBy,
                    IssuedType = x.IssuedType,
                    Remarks = x.Remarks,
                    IsPurchased = (x.IssuedType == RequestStatus.PURCHASING && x.IsTransferAccpac)
                }).ToList();

            List<string> RequestorList = listData.Select(x => x.Requestor).Distinct().ToList();
            var requestors = UserLogin.GetAll().Where(x => RequestorList.Contains(x.NIKSite)).ToList();
            var statusMSRs = EnumTable.GetByCode(EnumTable.EnumType.STATUS_MSR).ToList();

            foreach (var item in listData)
            {
                var checkUser = requestors.FirstOrDefault(x => x.NIKSite == item.Requestor);
                if (checkUser != null)
                {
                    item.Requestor = checkUser.FullName;
                }

                item.LabelCssColor = CommonFunction.LabelStatusCssHelper(item.IssuedType);
                var statusMSR = statusMSRs.FirstOrDefault(x => x.EnumValue == item.IssuedType);
                if (statusMSR != null)
                {
                    item.IssuedType = statusMSR.EnumLabel;
                }

                item.MSRIssuedDateStr = item.MSRIssuedDate.JResourcesDate();
            }

            model.ListData = listData;
            return model;
        }

        /// <summary>
        /// Get MSR Issued By MSRIssuedId
        /// </summary>
        /// <param name="MSRIssuedId"></param>
        /// <returns></returns>
        public static MSRIssuedModel GetMSRIssuedId(Guid MSRIssuedId)
        {
            MSRIssuedModel model = new MSRIssuedModel();
            MSRIssued MSRIssued = MSRIssued.GetById(MSRIssuedId);
            if (MSRIssued != null)
            {
                model.MSR = MSRLogic.GetMSRModel(MSRIssued.MSRId);
                model.ID = MSRIssued.ID;
                model.MSRIssuedNo = MSRIssued.MSRIssuedNo;
                model.CompanyId = MSRIssued.CompanyId;
                model.CompanyName = MSRIssued.Company.CompanyName;
                model.DepartmentId = MSRIssued.DepartmentId;
                model.DepartmentName = MSRIssued.Department.DepartmentName;
                model.CostCenter = MSRIssued.MSR.CostCode;
                model.MSRIssuedDate = MSRIssued.CreatedDate;
                model.MSRIssuedDateStr = MSRIssued.CreatedDate.ToString(Constant.FORMAT_DATE_JRESOURCES);
                model.IsTransferAccpac = MSRIssued.IsTransferAccpac;

                var statusIssued = EnumTable.GetDescriptionByCodeValue(EnumTable.EnumType.STATUS_MSR, MSRIssued.IssuedType);
                model.IssuedType = MSRIssued.IssuedType;
                if (statusIssued != null)
                {
                    model.IssuedTypeLabel = statusIssued.EnumLabel;
                }

                model.Remarks = MSRIssued.Remarks;
                model.IsTransferAccpac = MSRIssued.IsTransferAccpac;
                model.MSRId = MSRIssued.MSRId;
                model.MSRNo = MSRIssued.MSR.MSRNo;
                model.Creator = UserLogic.GetByNIK(MSRIssued.CreatedBy);
                if (model.Creator == null)
                {
                    model.Creator = new UserModel();
                    model.Creator.NIKSite = MSRIssued.CreatedBy;
                    model.Creator.FullName = MSRIssued.CreatedBy;
                }

                //if (MSRIssued.IssuedType == RequestStatus.TRANSFER && !model.MSR.IsDataMigration && model.MSR.StatusRequest != RequestStatus.COMPLETED && MSRIssued.IsTransferAccpac)
                    if (MSRIssued.IssuedType == RequestStatus.TRANSFER && !model.MSR.IsDataMigration && model.MSR.StatusRequest != RequestStatus.COMPLETED)
                {
                    if (!MSRIssued.ReverseId.HasValue)
                    {
                        var IsAuth = RoleLogic.CheckRoleAccess(CurrentUser.NIKSITE, ROLE_CODE.MSR_REVERSE_TRANSFER, model.DepartmentId);
                        if (IsAuth)
                        {
                            model.IsCanReverse = true;
                        }
                    }
                }

                #region Set View Approval Cancel
                if (MSRIssued.IssuedType == RequestStatus.CANCEL)
                {
                    DocumentApproval docCancelApprove = DocumentApproval.GetByDocumentNo(MSRIssued.MSRIssuedNo).OrderBy(x => x.Sequence)
                        .FirstOrDefault(x => !x.IsFinish);

                    if (docCancelApprove != null)
                    {
                        var user = UserLogic.GetByNIK(CurrentUser.NIKSITE);
                        var department = Department.GetById(docCancelApprove.DepartmentId);
                        var IsAdmin = UserLogic.IsAdministrator(user.ID, department.Company.CompanyCode);

                        if (!IsAdmin)
                        {
                            var roleAccess = RoleAccessUser.GetByUserId(user.ID).FirstOrDefault(x =>
                            x.RoleAccessId == docCancelApprove.RoleId &&
                            x.DepartmentId == docCancelApprove.DepartmentId);

                            if (roleAccess != null)
                            {
                                model.IsApproveCancelForm = true;
                            }
                        }
                        else
                        {
                            model.IsApproveCancelForm = true;                            
                        }
                        
                    }

                }
                #endregion

                foreach (var item in MSRIssued.MSRIssuedDetails)
                {
                    MSRIssuedDetailModel ItemDetail = new MSRIssuedDetailModel();
                    ItemDetail.ID = item.ID;
                    ItemDetail.MSRIssuedId = item.MSRIssuedId;
                    ItemDetail.LineNo = item.LineNo;
                    ItemDetail.ItemNo = item.ItemNo;
                    ItemDetail.Description = item.Description;

                    if (string.IsNullOrEmpty(MSRIssued.MSR.WorkOrderNo))
                    {
                        ItemDetail.Location = item.LocationIdFrom;
                    }
                    else
                    {
                        ItemDetail.Location = item.LocationIdTo;
                    }

                    if (MSRIssued.IssuedType == RequestStatus.TRANSFER 
                        || MSRIssued.IssuedType == RequestStatus.REVERSE_TRANSFER)
                    {
                        ItemDetail.LocationFrom = item.LocationIdFrom;
                        ItemDetail.QtyTransfered = item.Qty;
                    }

                    if (MSRIssued.IssuedType == RequestStatus.ISSUED)
                    {
                        ItemDetail.QtyProcess = item.Qty;
                    }

                    if (MSRIssued.IssuedType == RequestStatus.CANCEL)
                    {
                        ItemDetail.QtyCancel = item.Qty;
                    }

                    if (MSRIssued.IssuedType == RequestStatus.PURCHASING)
                    {
                        ItemDetail.QtyPurchase = item.Qty;
                    }

                    if (MSRIssued.IssuedType == RequestStatus.ISSUED)
                    {
                        ItemDetail.QtyIssued = item.Qty;
                    }

                    var MSRItem = MSRIssued.MSR.MSRDetails.FirstOrDefault(x => x.LineNo == item.LineNo);
                    if (MSRItem != null)
                    {
                        ItemDetail.CostCode = MSRItem.CostCode;
                        ItemDetail.UOM = MSRItem.UOM;
                    }

                    ItemDetail.QtyDelivered = item.Qty;
                    model.MSRIssuedDetails.Add(ItemDetail);
                }

                model.MSRIssuedDetails = model.MSRIssuedDetails.OrderBy(x => x.LineNo).ToList();
            }

            bool IsWorkOrder = false;
            if (model.IssuedType == RequestStatus.ISSUED && !string.IsNullOrEmpty(model.MSR.WorkOrderNo))
            {
                IsWorkOrder = true;
            }

            ItemLogic.SetLocationItem(model.MSRIssuedDetails, IsWorkOrder);
            return model;
        }

        /// <summary>
        /// Get Issued FRF By MSRId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static ResponseModel IssuedFRF(FRFModel model)
        {
            var response = new ResponseModel();
            tfrf_master frf = tfrf_master.GetById(model.ID);
            model = FRFLogic.GetDataById(model.ID);
            if (model != null && !string.IsNullOrEmpty(model.FRFNo))
            {
                using (var transaction = new TransactionScope())
                {
                    var company = Company.GetByCode(frf.compid);
                    if (company != null && RoleLogic.CheckRoleAccess(CurrentUser.NIKSITE, ROLE_CODE.FRF_ISSUED, company.ID, Guid.Empty))
                    {


                        if (model.FRFStatus == RequestStatus.APPROVED)
                        {
                            DocumentApprovalLogic.ApproveByDocumentNo(frf.frf_no, CurrentUser.NIKSITE, ROLE_CODE.FRF_APPROVAL);
                            frf.status_id = RequestStatus.ISSUED;
                            frf.UpdateSave<tfrf_master>();
                            SchedulerLogic.Create(company.ID, DocumentType.FRF, frf.frf_no);
                            transaction.Complete();
                            response.SetSuccess("Fuel {0} has been update status", frf.frf_no);
                        }
                        else
                        {
                            response.SetError("Fuel {0} status must be Approved", frf.frf_no);
                        }
                    }
                    else
                    {
                        response.SetError("{0} dont have permission to approved", CurrentUser.FULLNAME);
                    }
                }
            }
            else
            {
                response.SetError("Fuel Not Found");
            }

            return response;
        }

        public static MSRIssuedModel GetDataByPurchaseId(Guid PurchaseId)
        {
            MSRIssued MSRIssued = MSRIssued.GetById(PurchaseId);
            MSRIssuedModel model = null;
            if (MSRIssued != null)
            {
                model = WarehouseLogic.GetDataByMSRId(MSRIssued.MSRId, "");
                model.ID = MSRIssued.ID;
                model.MSRIssuedNo = MSRIssued.MSRIssuedNo;
                model.RQNNumber = MSRIssued.RQNNumber;
                model.WorkFlowCode = MSRIssued.WorkFlowCode;
                model.CostCenter = MSRIssued.CostCenter;

                if (model.MSRIssuedDetails != null && model.MSRIssuedDetails.Count > 0)
                {
                    //model.MSRIssuedDetails = model.MSRIssuedDetails.Where(x => !x.Isfulfilled).ToList();
                    MSRIssued.MSRIssuedDetails.ToList().ForEach(x =>
                    {
                        model.MSRIssuedDetails.ForEach(y =>
                        {
                            y.IsView = true;
                            if (x.LineNo == y.LineNo)
                            {
                                y.QtyPurchase += x.Qty;
                                y.QtyProcess += x.Qty;
                            }
                        });
                    });
                }
            }


            return model;
        }

        public static ResponseModel IssuedCancelApprove(Guid MSRIssuedId, string CancelStatus)
        {
            var response = new ResponseModel(false);
            MSRIssued msrIssued = MSRIssued.GetById(MSRIssuedId);
            var user = UserLogic.GetByNIK(CurrentUser.NIKSITE);

            if (msrIssued != null)
            {
                using (var transaction = new TransactionScope())
                {
                    List<DocumentApproval> docCancelApproves = DocumentApproval.GetByDocumentNo(msrIssued.MSRIssuedNo)
                        .OrderBy(x => x.Sequence).ToList();

                    for (int i = 0; i < docCancelApproves.Count; i++)
                    {
                        var docCancelApprove = docCancelApproves[i];
                        var role = RoleAccess.GetById(docCancelApprove.RoleId);
                        if (!docCancelApprove.IsFinish)
                        {
                            bool IsValid = RoleLogic.CheckRoleAccess(CurrentUser.NIKSITE, role.RoleAccessCode, docCancelApprove.DepartmentId);
                            if (IsValid)
                            {
                                response.AddMessageList(string.Format("Cancel {0} Has Approve Cancel <br />", msrIssued.MSRIssuedNo));

                                docCancelApprove.IsFinish = true;
                                docCancelApprove.ApprovalStatus = CancelStatus;
                                docCancelApprove.UpdateSave<DocumentApproval>();

                                if (CancelStatus == RequestStatus.APPROVED)
                                {
                                    msrIssued.IssuedStatus = 1;
                                }
                                else
                                {
                                    msrIssued.IssuedStatus = 3;
                                }


                                response.SetSuccess();
                            }
                            else
                            {
                                if (response.IsSuccess)
                                {
                                    response.AddMessageList(string.Format("Next Approval To Role {0}  <br />", role.RoleAccessName));
                                }
                                else
                                {
                                    response.AddMessageList(string.Format("Dont have Access Role {0}  <br />", role.RoleAccessName));
                                }

                                if (CancelStatus == RequestStatus.REJECTED)
                                {
                                    msrIssued.IssuedStatus = 3;
                                }
                            }
                        }
                    }

                    var docs = DocumentApproval.GetByDocumentNo(msrIssued.MSRIssuedNo).Where(x => x.IsFinish && x.ApprovalStatus == RequestStatus.APPROVED).ToList();
                    if (docs != null && docs.Count == 2)
                    {
                        msrIssued.IssuedStatus = 2;
                    }

                    msrIssued.UpdateSave<MSRIssued>();
                    string StatusMSR = MSRChangeStatusById(msrIssued.MSRId);
                    response.AddMessageList(string.Format("MSR NO : {0} change status to {1}", msrIssued.MSR.MSRNo, StatusMSR));
                    transaction.Complete();

                    response.SetSuccess();
                }
            }

            return response;
        }

        //public static BidEvaluationModel GetForBidEvaluation(Guid MSRId)
        //{
        //    BidEvaluationModel model = new BidEvaluationModel();
        //    model.MSRId = MSRId;
        //    model.MSR = MSRLogic.GetMSRModel(MSRId);
        //    model.CompanyId = model.MSR.CompanyId;
        //    model.DepartmentId = model.MSR.DepartmentId;
        //    model.Remarks = model.MSR.Reason;

        //    SelectListItem ListMSRType = JResources.Common.EnumUtils<JResources.Common.MSRType>.GetDropDownList()
        //        .FirstOrDefault(x => x.Value == model.MSR.MSRType);
        //    model.MSR.MSRType = ListMSRType.Text;

        //    var costcenter = CostCenterLogic.GetCostCenterByDepartmentId(model.MSR.DepartmentId);
        //    if (costcenter != null)
        //    {
        //        var costSelect = costcenter.FirstOrDefault(x => x.Value.Trim() == model.MSR.CostCenter.Trim());
        //        if (costSelect != null)
        //        {
        //            model.MSR.CostCenter = costSelect.Value;
        //            model.MSR.CostCenterLabel = costSelect.Text;
        //        }
        //    }

        //    var MSRApproval = MSRLogic.GetApproval(model.MSR.DepartmentId);
        //    model.MSR.Manager = MSRApproval.Manager;
        //    model.MSR.TotalWithApprovalGM = MSRApproval.TotalWithApprovalGM;
        //    model.MSR.GeneralManagerSite = MSRApproval.GeneralManagerSite;

        //    MSR MSR = MSR.GetById(MSRId);
        //    var MSRDetails = MSR.MSRDetails.Where(x => !x.IsDeleted).ToList();
        //    foreach (var item in MSRDetails)
        //    {
        //        BidEvaluationDetailModel IssuedDetail = new BidEvaluationDetailModel();
        //        IssuedDetail.ID = item.ID;
        //        IssuedDetail.LineNo = item.LineNo;
        //        IssuedDetail.ItemNo = item.ItemNo;
        //        IssuedDetail.Description = item.Description;
        //        IssuedDetail.QtyRequest = item.Qty;
        //        IssuedDetail.Cost = item.UnitCost;
        //        IssuedDetail.CostCode = item.CostCode;
        //        IssuedDetail.Category = item.CostCode;
        //        IssuedDetail.UOM = item.UOM;

        //        if (!string.IsNullOrEmpty(MSR.WorkOrderNo))
        //        {
        //            IssuedDetail.Location = item.LocationId;
        //        }

        //        if (string.IsNullOrEmpty(IssuedDetail.Location))
        //        {
        //            IssuedDetail.Location = item.LocationIdFrom;
        //        }

        //        model.BidEvaluationDetails.Add(IssuedDetail);
        //    }

        //    #region SET QTY PROGRESS
        //    //var MSRIssueds = MSRIssued.GetByMSRId(MSR.ID);
        //    //if (MSRIssueds != null && MSRIssueds.Count() > 0)
        //    //{
        //    //    foreach (var msrIssued in MSRIssueds)
        //    //    {
        //    //        if (msrIssued.MSRIssuedDetails != null && msrIssued.MSRIssuedDetails.Count > 0)
        //    //        {
        //    //            foreach (var item in msrIssued.MSRIssuedDetails)
        //    //            {
        //    //                model.BidEvaluationDetails.ForEach(x =>
        //    //                {
        //    //                    if (x.LineNo == item.LineNo)
        //    //                    {
        //    //                        switch (msrIssued.IssuedType)
        //    //                        {
        //    //                            case RequestStatus.PURCHASING:
        //    //                                x.QtyRequest += item.Qty;
        //    //                                break;
        //    //                        }

        //    //                    }

        //    //                });
        //    //            }
        //    //        }
        //    //    }
        //    //}
        //    #endregion

        //    bool IsWorkOrder = false;
        //    if (!string.IsNullOrEmpty(model.MSR.WorkOrderNo))
        //    {
        //        IsWorkOrder = true;
        //    }

        //    List<MSRIssuedDetailModel> listBidEval = new List<MSRIssuedDetailModel>();
        //    model.BidEvaluationDetails.ForEach(x => {
        //        var itemBid = new MSRIssuedDetailModel();
        //        itemBid.InjectFrom(x);
        //        listBidEval.Add(itemBid);
        //    });

        //    ItemLogic.SetLocationItem(listBidEval, IsWorkOrder);
        //    model.BidEvaluationDetails = new List<BidEvaluationDetailModel>();

        //    listBidEval.ForEach(x => {
        //        var itemBid = new BidEvaluationDetailModel();
        //        itemBid.InjectFrom(x);
        //        model.BidEvaluationDetails.Add(itemBid);
        //    });

        //    model.BidEvaluationDetails = model.BidEvaluationDetails.OrderBy(x => x.LineNo).ToList();

        //    return model;

        //}

        //public static BidEvaluationModel GetBidEvaluationById(Guid Id)
        //{
        //    BidEvaluationModel model = new BidEvaluationModel();

        //    BidEvaluation bidEvaluation = BidEvaluation.GetById(Id);
        //    model.InjectFrom(bidEvaluation);
        //    model.MSR = MSRLogic.GetMSRModel(bidEvaluation.MSRId);
        //    model.MSRId = bidEvaluation.MSRId;
        //    model.MSRNo = bidEvaluation.MSR.MSRNo;

        //    Department dep = Department.GetById(bidEvaluation.DepartmentId);
        //    model.DepartmentId = dep.ID;
        //    model.DepartmentName = dep.DepartmentName;

        //    model.CompanyId = dep.CompanyId;
        //    model.CompanyName = dep.Company.CompanyName;
        //    model.CreatedDate = bidEvaluation.CreatedDate;
        //    model.CreatedDateStr = bidEvaluation.CreatedDate.ToString(Constant.FORMAT_DATE_JRESOURCES_PICKER);

        //    model.Requestor = string.IsNullOrEmpty(bidEvaluation.CreatedBy) ? "" : bidEvaluation.CreatedBy;
        //    var requestor = UserLogic.GetByNIK(model.Requestor);
        //    if (requestor != null)
        //    {
        //        model.Requestor = requestor.FullName;
        //    }

        //    bool IsAddVendor = true;
        //    foreach (var BidDetail in bidEvaluation.BidEvaluationDetails)
        //    {
        //        BidEvaluationDetailModel itemDetailModel = new BidEvaluationDetailModel();
        //        itemDetailModel.ID = BidDetail.ID;
        //        itemDetailModel.BidEvaluationId = bidEvaluation.ID;
        //        itemDetailModel.LineNo = BidDetail.LineNo;
        //        itemDetailModel.ItemNo = BidDetail.ItemNo;
        //        itemDetailModel.Description = BidDetail.Description;
        //        itemDetailModel.Qty = BidDetail.Qty;
        //        itemDetailModel.IsDeleted = BidDetail.IsDeleted;
        //        itemDetailModel.IsExpand = !BidDetail.IsDeleted;

        //        if (BidDetail.BidEvaluationVendors != null && BidDetail.BidEvaluationVendors.Count > 0)
        //        {
        //            foreach (var item in BidDetail.BidEvaluationVendors)
        //            {
        //                BidEvaluationVendorModel itemVendor = new BidEvaluationVendorModel();
        //                itemVendor.ID = item.ID;
        //                itemVendor.BidEvaluationDetailID = BidDetail.ID;
        //                itemVendor.LineNo = item.LineNo;
        //                itemVendor.VendorId = item.VendorId;
        //                itemVendor.VendorName = item.VendorName;
        //                itemVendor.UnitPrice = item.UnitPrice;
        //                itemVendor.IsSelected = item.IsSelected;

        //                itemDetailModel.Vendors.Add(itemVendor);

        //                if (IsAddVendor)
        //                {
        //                    APVEN vendor = new APVEN();
        //                    vendor.VENDORID = item.VendorId;
        //                    vendor.VENDNAME = item.VendorName;

        //                    model.Vendors.Add(vendor);
        //                }
        //            }

        //            IsAddVendor = false;
        //        }

        //        model.BidEvaluationDetails.Add(itemDetailModel);
        //    }



        //    return model;
        //}


    }
}

