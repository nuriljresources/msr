﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.EntityClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using JResources.Common;
using JResources.Common.Enum;
using JResources.Data;
using JResources.Data.Model;
using JResources.Data.Model.Report;


namespace JResources.BusinessLogic
{
    public class ReportLogic
    {
        /// <summary>
        /// REPORT OTIF
        /// </summary>
        /// <param name="DataParameter"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        //public static ResponseModel OTIF(string DataParameter, string fileName)
        //{
        //    ResponseModel response = new ResponseModel();
        //    MSRSearchModel param = Newtonsoft.Json.JsonConvert.DeserializeObject<MSRSearchModel>(DataParameter);
        //    int rowIndex = 0;
        //    int TotalRow = 0;

        //    if (param.DateFrom != DateTime.MinValue && param.DateTo != DateTime.MinValue && param.DateTo > param.DateFrom)
        //    {

        //        try
        //        {
        //            string locationSave = Path.Combine(SiteSettings.PATH_DOWNLOAD, fileName);

        //            Company company = Company.GetById(param.CompanyId);
        //            DataRepositoryStoreAccpac.CurrentDataStore[DataRepositoryStoreAccpac.KEY_DATACONTEXT] = new AccpacEntities(ConnectionManager.GetConnectionStringByCompanyCode(DatabaseGroupName.ACCPAC_DATABASE, company.CompanyCode));

        //            ADOHelper adoHelper = new ADOHelper(ConfigurationManager.ConnectionStrings["JResourcesConnection"].ConnectionString);
        //            var reader = adoHelper.ExecDataReaderProc("report_msr_otif",
        //                "@companyId", param.CompanyId,
        //                "@DepartmentId", param.DepartmentId,
        //                "@dateFrom", param.DateFrom,
        //                "@dateTo", param.DateTo,
        //                "@MSRNo", !string.IsNullOrEmpty(param.MSRNo) ? param.MSRNo : "",
        //                "@WorkOrderNo", !string.IsNullOrEmpty(param.ItemNo) ? param.WorkOrderNo : "",
        //                "@PriorityOrder", !string.IsNullOrEmpty(param.ItemNo) ? param.MaintenanceCode : "",
        //                "@ItemNo", !string.IsNullOrEmpty(param.ItemNo) ? param.ItemNo : ""
        //                );

        //            using (CsvFileWriter csvWriter = new CsvFileWriter(locationSave))
        //            {
        //                #region PROCESS DRAW CSV
        //                CsvRow csvRow = new CsvRow();

        //                if (reader.HasRows)
        //                {
        //                    string[] headerColoums = new string[] {
        //                    "No",
        //                    "Item No",
        //                    "Description",
        //                    "MSR No",
        //                    "Header Status",
        //                    "MSR Create Date",
        //                    "MSR App. Date",
        //                    "Target Fullfillment Date",
        //                    "Expected Date",
        //                    "Priority Order",
        //                    "LT Priority",
        //                    "WONo",
        //                    "Department",
        //                    "Cost Centre",
        //                    "Equipment No",
        //                    "UOM",
        //                    "CIFD",
        //                    "Qty Request",
        //                    "Qty Transfered",
        //                    "Qty Cancel",
        //                    "Qty Purchase",
        //                    "Qty Issued",
        //                    "Request Purchase Date",
        //                    "Last Transfer Date",
        //                    "Last Cancel Date",
        //                    "Last Purchase Date",
        //                    "Last Issue Date",
        //                    "Detail MSR Status",
        //                    "Requestor",
        //                    "Remark",
        //                    "Month TRD",
        //                    "Year TFD",
        //                    "Delta Qty",
        //                    "ON TIME",
        //                    "IN FULL",
        //                    "OTIF STS"
        //                };


        //                    foreach (var item in headerColoums)
        //                    {
        //                        csvRow.Add(item);
        //                    }
        //                    csvWriter.WriteRow(csvRow);


        //                    while (reader.Read())
        //                    {
        //                        //try
        //                        //{
        //                        TotalRow = reader.GetInt32(38);
        //                        string ItemNo = reader.GetString(0);
        //                        string ItemDescription = reader.GetString(1);
        //                        string MSRNo = reader.GetString(2);
        //                        string HeaderStatus = reader.GetString(3);
        //                        DateTime MSRCreatedDate = reader.GetDateTime(4);


        //                        Nullable<DateTime> ApproveDate = null;
        //                        if (!string.IsNullOrEmpty(reader.GetValue(5).ToString()))
        //                            ApproveDate = reader.GetDateTime(5);


        //                        Nullable<DateTime> FullfillmentDate = null;
        //                        if (!string.IsNullOrEmpty(reader.GetValue(6).ToString()))
        //                            FullfillmentDate = reader.GetDateTime(6);

        //                        DateTime ExpectedDate = reader.GetDateTime(7);
        //                        string PriorityOrder = reader.GetString(8);
        //                        int LeadTime = reader.GetInt32(9);
        //                        string WorkOrderNo = reader.GetString(10);
        //                        string DepartmentName = reader.GetString(11);
        //                        string CostCode = reader.GetString(12);
        //                        string EquipmentNo = reader.GetString(13);
        //                        string UOM = reader.GetString(14);

        //                        Nullable<DateTime> RequestPurchaseDate = null;
        //                        if (!string.IsNullOrEmpty(reader.GetValue(15).ToString()))
        //                            RequestPurchaseDate = reader.GetDateTime(15);

        //                        Decimal ItemQtyRequest = reader.GetDecimal(16);
        //                        Decimal ItemQtyTransfer = reader.GetDecimal(17);
        //                        Decimal ItemQtyIssued = reader.GetDecimal(18);
        //                        Decimal ItemQtyPurchase = reader.GetDecimal(19);
        //                        Decimal ItemQtyCancel = reader.GetDecimal(20);


        //                        Nullable<DateTime> ItemDateTransfer = null;
        //                        if (!string.IsNullOrEmpty(reader.GetValue(21).ToString()))
        //                            ItemDateTransfer = reader.GetDateTime(21);


        //                        Nullable<DateTime> ItemDateIssued = null;
        //                        if (!string.IsNullOrEmpty(reader.GetValue(22).ToString()))
        //                            ItemDateIssued = reader.GetDateTime(22);


        //                        Nullable<DateTime> ItemDatePurchase = null;
        //                        if (!string.IsNullOrEmpty(reader.GetValue(23).ToString()))
        //                            ItemDatePurchase = reader.GetDateTime(23);

        //                        Nullable<DateTime> ItemDateCancel = null;
        //                        if (!string.IsNullOrEmpty(reader.GetValue(24).ToString()))
        //                            RequestPurchaseDate = reader.GetDateTime(24);

        //                        Decimal MSRQtyRequest = reader.GetDecimal(25);
        //                        Decimal MSRQtyTransfer = reader.GetDecimal(26);
        //                        Decimal MSRQtyIssued = reader.GetDecimal(27);
        //                        Decimal MSRQtyPurchase = reader.GetDecimal(28);
        //                        Decimal MSRQtyCancel = reader.GetDecimal(29);

        //                        Nullable<DateTime> MSRDateTransfer = null;
        //                        if (!string.IsNullOrEmpty(reader.GetValue(30).ToString()))
        //                            MSRDateTransfer = reader.GetDateTime(30);


        //                        Nullable<DateTime> MSRDateIssued = null;
        //                        if (!string.IsNullOrEmpty(reader.GetValue(31).ToString()))
        //                            MSRDateIssued = reader.GetDateTime(31);


        //                        Nullable<DateTime> MSRDatePurchase = null;
        //                        if (!string.IsNullOrEmpty(reader.GetValue(32).ToString()))
        //                            MSRDatePurchase = reader.GetDateTime(32);


        //                        Nullable<DateTime> MSRDateCancel = null;
        //                        if (!string.IsNullOrEmpty(reader.GetValue(33).ToString()))
        //                            MSRDateCancel = reader.GetDateTime(33);

        //                        string Requestor = reader.GetString(34);
        //                        string Remark = reader.GetString(35);
        //                        int MonthTRD = 0;
        //                        if (!string.IsNullOrEmpty(reader.GetValue(36).ToString()))
        //                        {
        //                            MonthTRD = reader.GetInt32(36);
        //                        }

        //                        int YearTRD = 0;
        //                        if (!string.IsNullOrEmpty(reader.GetValue(37).ToString()))
        //                        {
        //                            YearTRD = reader.GetInt32(37);
        //                        }

        //                        decimal _deltaQty = 0;
        //                        decimal _onTime = 0;
        //                        decimal _InFull = 0;
        //                        string _STS = string.Empty;

        //                        if (!string.IsNullOrEmpty(WorkOrderNo))
        //                        {
        //                            _deltaQty = ItemQtyTransfer - (ItemQtyRequest - ItemQtyCancel);
        //                        }
        //                        else
        //                        {
        //                            _deltaQty = ItemQtyIssued - (ItemQtyRequest - ItemQtyCancel);
        //                        }

        //                        if (FullfillmentDate.HasValue)
        //                        {
        //                            if (!string.IsNullOrEmpty(WorkOrderNo))
        //                            {
        //                                if (!ItemDateTransfer.HasValue)
        //                                {
        //                                    _onTime = DateTime.Now.Subtract(FullfillmentDate.Value).Days;
        //                                }
        //                                else
        //                                {
        //                                    _onTime = ItemDateTransfer.Value.Subtract(FullfillmentDate.Value).Days;
        //                                }
        //                            }
        //                            else
        //                            {
        //                                if (!ItemDateIssued.HasValue)
        //                                {
        //                                    _onTime = DateTime.Now.Subtract(FullfillmentDate.Value).Days;
        //                                }
        //                                else
        //                                {
        //                                    _onTime = ItemDateIssued.Value.Subtract(FullfillmentDate.Value).Days;
        //                                }
        //                            }
        //                        }


        //                        if (!string.IsNullOrEmpty(WorkOrderNo))
        //                        {
        //                            if (ItemQtyTransfer > 0 && (ItemQtyRequest - ItemQtyCancel) > 0)
        //                            {
        //                                _InFull = ItemQtyTransfer / (ItemQtyRequest - ItemQtyCancel);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            if (ItemQtyIssued > 0 && (ItemQtyRequest - ItemQtyCancel) > 0)
        //                            {
        //                                _InFull = ItemQtyIssued / (ItemQtyRequest - ItemQtyCancel);
        //                            }
        //                        }

        //                        if (ItemQtyRequest == ItemQtyCancel)
        //                        {
        //                            _STS = "EXCLUDE";
        //                        }
        //                        else if (_InFull == 100 && _onTime <= 0)
        //                        {
        //                            _STS = "OTIF";
        //                        }
        //                        else if (_InFull < 100 && _onTime <= 0)
        //                        {
        //                            _STS = "OT";
        //                        }
        //                        else if (_InFull == 100 && _onTime > 0)
        //                        {
        //                            _STS = "IF";
        //                        }
        //                        else
        //                        {
        //                            _STS = "NOT OTIF";
        //                        }

        //                        string Ranks = string.Empty;
        //                        ZItem_Rank_Process itemRank = AccpacDataContext.CurrentContext.ZItem_Rank_Process.FirstOrDefault(x => x.itemno == ItemNo && x.datex == MSRCreatedDate);
        //                        if (itemRank != null && !string.IsNullOrEmpty(itemRank.Ranks))
        //                        {
        //                            Ranks = itemRank.Ranks;
        //                        }

        //                        string[] strValues = new string[] {
        //                        (rowIndex + 1).ToString(),
        //                        ItemNo, /*"Item No",*/
        //                        ItemDescription,//"Description",
        //                        MSRNo,//"MSR No",
        //                        HeaderStatus,//"Header Status",
        //                        MSRCreatedDate.ToString(Constant.FORMAT_DATE_JRESOURCES),//"MSR Create Date",
        //                        ApproveDate.HasValue ? ApproveDate.Value.ToString(Constant.FORMAT_DATE_JRESOURCES) : "",//"MSR App. Date",
        //                        FullfillmentDate.HasValue ? FullfillmentDate.Value.ToString(Constant.FORMAT_DATE_JRESOURCES) : "",//"Target Fullfillment Date",
        //                        ExpectedDate.ToString(Constant.FORMAT_DATE_JRESOURCES),//"Expected Date",
        //                        PriorityOrder,//"Priority Order",
        //                        LeadTime.ToString("#.##"),//"LT Priority",
        //                        WorkOrderNo,//"WONo",
        //                        DepartmentName,//"Department",
        //                        CostCode,//"Cost Centre",
        //                        EquipmentNo,//"Equipment No",
        //                        UOM,//"UOM",
        //                        Ranks,//"CIFD",
        //                        ItemQtyRequest.ToString("#.##"),//"Qty Request",
        //                        ItemQtyTransfer.ToString("#.##"),//"Qty Transfered",
        //                        ItemQtyCancel.ToString("#.##"),//"Qty Cancel",
        //                        ItemQtyPurchase.ToString("#.##"),//"Qty Purchase",
        //                        ItemQtyIssued.ToString("#.##"),//"Qty Issued",

        //                        RequestPurchaseDate.HasValue ? RequestPurchaseDate.Value.ToString(Constant.FORMAT_DATE_JRESOURCES) : "",//"Request Purchase Date",
        //                        ItemDateTransfer.HasValue ? ItemDateTransfer.Value.ToString(Constant.FORMAT_DATE_JRESOURCES) : "",//"Last Transfer Date",
        //                        ItemDateCancel.HasValue ? ItemDateCancel.Value.ToString(Constant.FORMAT_DATE_JRESOURCES) : "",//"Last Cancel Date",
        //                        ItemDatePurchase.HasValue ? ItemDatePurchase.Value.ToString(Constant.FORMAT_DATE_JRESOURCES) : "",//"Last Purchase Date",
        //                        ItemDateIssued.HasValue ? ItemDateIssued.Value.ToString(Constant.FORMAT_DATE_JRESOURCES) : "",//"Last Issue Date",

        //                        "", //"Detail MSR Status",
        //                        Requestor,//"Requestor",
        //                        Regex.Replace(Remark, @"\t|\n|\r", " "), //"Remark",
        //                        MonthTRD.ToString(),//"Month TRD",
        //                        YearTRD.ToString(),//"Year TFD",
        //                        _deltaQty == 0 ? "0" : _deltaQty.ToString("#.##"),//"Delta Qty",
        //                        _onTime == 0 ? "0" : _onTime.ToString("#.##"),//"ON TIME",
        //                        Math.Round(_InFull * 100) == 0 ? "0" :string.Format("{0}%", Math.Round(_InFull * 100).ToString("#.##")),//"IN FULL",
        //                        _STS//"OTIF STS",
        //                    };

        //                        csvRow = new CsvRow();
        //                        foreach (var strItem in strValues)
        //                        {
        //                            csvRow.Add(strItem);
        //                        }
        //                        csvWriter.WriteRow(csvRow);

        //                        //}
        //                        //catch (Exception ex)
        //                        //{
        //                        //    response.SetError(ex.Message);
        //                        //}



        //                        rowIndex++;
        //                        CommonFunction.DrawTextProgressBar(rowIndex, TotalRow);
        //                    }

        //                }
        //                #endregion
        //                response.SetSuccess("");
        //                EmailLogic.EmailReport(fileName, REPORT_TYPE.OTIF, param.MoreCriteria, param.DateFrom.Value, param.DateTo.Value);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            response.SetError(ex.Message);
        //        }

        //        Console.WriteLine(string.Format("RESULT IS {0}", response.IsSuccess ? "SUCCESS" : "FAILED"));
        //        if (!response.IsSuccess)
        //        {
        //            Console.WriteLine(string.Format("BECAUSE IS {0}", response.Message));
        //        }
        //    }
        //    else
        //    {
        //        response.SetError("Parameter date range not valid");
        //    }

        //    return response;
        //}


        /// <summary>
        /// REPORT MSR STATUS
        /// </summary>
        /// <param name="DataParameter"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static ResponseModel MSR_STATUS(string DataParameter, string fileName)
        {
            ResponseModel response = new ResponseModel();
            MSRSearchModel param = Newtonsoft.Json.JsonConvert.DeserializeObject<MSRSearchModel>(DataParameter);

            int TotalRow = 0;
            int rowIndex = 0;
            if (param.DateFrom != DateTime.MinValue && param.DateTo != DateTime.MinValue && param.DateTo > param.DateFrom)
            {
                try
                {
                    string locationSave = Path.Combine(SiteSettings.PATH_DOWNLOAD, fileName);
                    var fileInfo = new FileInfo(locationSave);
                    if (!(fileInfo.Exists && fileInfo.Length > 0))
                    {
                        #region CREATE FILE
                        using (CsvFileWriter csvWriter = new CsvFileWriter(locationSave))
                        {
                            CsvRow row = new CsvRow();
                            if (param.DateFrom != DateTime.MinValue
                                && param.DateTo != DateTime.MinValue
                                && param.DateTo > param.DateFrom)
                            {

                                Console.WriteLine("Get Database Process");
                                Console.WriteLine(string.Format("Paramter From Data : {0} Until {1}", param.DateFrom.Value.ToString(Constant.FORMAT_DATE_JRESOURCES), param.DateTo.Value.ToString(Constant.FORMAT_DATE_JRESOURCES)));

                                Company company = Company.GetById(param.CompanyId);
                                DataRepositoryStoreAccpac.CurrentDataStore[DataRepositoryStoreAccpac.KEY_DATACONTEXT] = new AccpacEntities(ConnectionManager.GetConnectionStringByCompanyCode(DatabaseGroupName.ACCPAC_DATABASE, company.CompanyCode));

                                ADOHelper adoHelper = new ADOHelper(ConfigurationManager.ConnectionStrings["JResourcesConnection"].ConnectionString);

                                var reader = adoHelper.ExecDataReaderProc("report_msr_status",
                                    "@companyId", param.CompanyId,
                                    "@MSRNo", !string.IsNullOrEmpty(param.MSRNo) ? param.MSRNo : "",
                                    "@dateFrom", param.DateFrom,
                                    "@dateTo", param.DateTo,
                                    "@departmentId", param.DepartmentId,
                                    "@equipmentCode", !string.IsNullOrEmpty(param.EquipmentNo) ? param.EquipmentNo : ""
                                    );

                                if (reader.HasRows)
                                {

                                    string[] headerColoums = new string[] {
                                        "No",
                                        "MSR No",
                                        "Header Status",
                                        "MSR Create Date",
                                        "MSR App. Date",
                                        "Target Fullfillment Date",
                                        "Expected Date",
                                        "Confirmation Date",
                                        "Priority Order",
                                        "Lead Time",
                                        "WONo",
                                        "Department",
                                        "Cost Centre",
                                        "Equipment No",
                                        "Item No",
                                        "Description",
                                        "UOM",
                                        "Qty Request",
                                        "Qty Transfered",
                                        "Qty Cancel",
                                        "Qty Purchase",
                                        "Qty Issued",
                                        "Request Purchase Date",
                                        "Last Transfer Date",
                                        "Last Cancel Date",
                                        "Last Purchase Date",
                                        "Last Issue Date",
                                        "Detail MSR Status",
                                        "Requestor",
                                        "Remark"
                                    };

                                    foreach (var item in headerColoums)
                                    {
                                        row.Add(item);
                                    }

                                    csvWriter.WriteRow(row);

                                    while (reader.Read())
                                    {
                                        TotalRow = reader.GetInt32(35);
                                        string MSRNo = reader.GetString(1);
                                        string HeaderStatusLabel = reader.GetString(3);

                                        string CreatedDate = string.Empty;
                                        if (!string.IsNullOrEmpty(reader.GetValue(5).ToString()))
                                            CreatedDate = reader.GetDateTime(5).ToString(Constant.FORMAT_DATE_JRESOURCES);

                                        string ApproveDate = string.Empty;
                                        if (!string.IsNullOrEmpty(reader.GetValue(21).ToString()))
                                            ApproveDate = reader.GetDateTime(21).ToString(Constant.FORMAT_DATE_JRESOURCES);

                                        string FullfillmentDate = string.Empty;
                                        if (!string.IsNullOrEmpty(reader.GetValue(22).ToString()))
                                            FullfillmentDate = reader.GetDateTime(22).ToString(Constant.FORMAT_DATE_JRESOURCES);

                                        string ExpectedDate = string.Empty;
                                        if (!string.IsNullOrEmpty(reader.GetValue(6).ToString()))
                                            ExpectedDate = reader.GetDateTime(6).ToString(Constant.FORMAT_DATE_JRESOURCES);

                                        string DismantleDate = string.Empty;
                                        if (!string.IsNullOrEmpty(reader.GetValue(7).ToString()))
                                            DismantleDate = reader.GetDateTime(7).ToString(Constant.FORMAT_DATE_JRESOURCES);

                                        string MaintenanceCode = reader.GetString(8);
                                        string LeadTime = reader.GetInt32(9).ToString("#.##");
                                        string WorkOrderNo = reader.GetString(10);
                                        string DepartmentName = reader.GetString(13);
                                        string CostCode = reader.GetString(11);
                                        string EquipmentNo = reader.GetString(12).Replace(',', ' ');
                                        string ItemNo = reader.GetString(15);
                                        string Description = reader.GetString(16).Replace(',', ' ');
                                        string UOM = reader.GetString(17);
                                        string QtyRequest = reader.GetDecimal(18).ToString("#.##");
                                        string QtyTransfer = reader.GetDecimal(23).ToString("#.##");
                                        string QtyCancel = reader.GetDecimal(25).ToString("#.##");
                                        string QtyPurchasing = reader.GetDecimal(26).ToString("#.##");
                                        string QtyIssued = reader.GetDecimal(27).ToString("#.##");

                                        string RequisitionDate = string.Empty;
                                        if (!string.IsNullOrEmpty(reader.GetValue(19).ToString()))
                                            RequisitionDate = reader.GetDateTime(19).ToString(Constant.FORMAT_DATE_JRESOURCES);

                                        string DateTransfer = string.Empty;
                                        if (!string.IsNullOrEmpty(reader.GetValue(28).ToString()))
                                            DateTransfer = reader.GetDateTime(28).ToString(Constant.FORMAT_DATE_JRESOURCES);

                                        string DateCancel = string.Empty;
                                        if (!string.IsNullOrEmpty(reader.GetValue(30).ToString()))
                                            DateCancel = reader.GetDateTime(30).ToString(Constant.FORMAT_DATE_JRESOURCES);

                                        string DatePurchasing = string.Empty;
                                        if (!string.IsNullOrEmpty(reader.GetValue(31).ToString()))
                                            DatePurchasing = reader.GetDateTime(31).ToString(Constant.FORMAT_DATE_JRESOURCES);

                                        string DateIssued = string.Empty;
                                        if (!string.IsNullOrEmpty(reader.GetValue(32).ToString()))
                                            DateIssued = reader.GetDateTime(32).ToString(Constant.FORMAT_DATE_JRESOURCES);

                                        string DetailStatusLabel = reader.GetString(34);
                                        string Requestor = reader.GetString(4);
                                        string Remark = reader.GetString(20);
                                        Remark = Regex.Replace(Remark, @"\t|\n|\r", " ");

                                        string[] strValues = new string[] 
                                        {
                                            (rowIndex + 1).ToString(),
                                            MSRNo,
                                            HeaderStatusLabel,
                                            CreatedDate,
                                            ApproveDate,
                                            FullfillmentDate,
                                            ExpectedDate,
                                            DismantleDate,
                                            MaintenanceCode,
                                            LeadTime,
                                            WorkOrderNo,
                                            DepartmentName,
                                            CostCode,
                                            EquipmentNo.Replace(',', ' '),
                                            ItemNo,
                                            Description.Replace(',', ' '),
                                            UOM,
                                            QtyRequest,
                                            QtyTransfer,
                                            QtyCancel,
                                            QtyPurchasing,
                                            QtyIssued,
                                            RequisitionDate,
                                            DateTransfer,
                                            DateCancel,
                                            DatePurchasing,
                                            DateIssued,
                                            DetailStatusLabel,
                                            Requestor,
                                            Remark
                                        };

                                        rowIndex++;
                                        row = new CsvRow();
                                        foreach (var strItem in strValues)
                                        {
                                            row.Add(strItem);
                                        }
                                        csvWriter.WriteRow(row);
                                        CommonFunction.DrawTextProgressBar(rowIndex, TotalRow);
                                    }

                                    response = EmailLogic.EmailReport(fileName, REPORT_TYPE.MSR_STATUS, param.MoreCriteria, param.DateFrom.Value, param.DateTo.Value);

                                }
                                else
                                {
                                    row.Add("Dont have data");
                                }
                            }
                            else
                            {
                                row.Add("Parameter date not valid");
                            }
                        }

                        #endregion
                    }
                    else
                    {
                        //KONDISI JIKA File BERHASIL DIBUAT NAMUN EMAIL GAGAL
                        response = EmailLogic.EmailReport(fileName, REPORT_TYPE.MSR_STATUS, param.MoreCriteria, param.DateFrom.Value, param.DateTo.Value);
                    }

                }
                catch (Exception ex)
                {
                    response.SetError(ex.Message);
                }
            }
            else
            {
                response.SetError("Parameter date range not valid");
            }


            if (response.IsSuccess)
            {

            }

            return response;
        }

        ///// <summary>
        ///// REPORT MSR STATUS
        ///// </summary>
        ///// <param name="DataParameter"></param>
        ///// <param name="fileName"></param>
        ///// <returns></returns>
        //public static ResponseModel MSR_STATUS_2(string DataParameter, string fileName)
        //{
        //    ResponseModel response = new ResponseModel();
        //    MSRSearchModel param = Newtonsoft.Json.JsonConvert.DeserializeObject<MSRSearchModel>(DataParameter);

        //    int TotalRow = 0;
        //    int rowIndex = 0;
        //    if (param.DateFrom != DateTime.MinValue && param.DateTo != DateTime.MinValue && param.DateTo > param.DateFrom)
        //    {
        //        try
        //        {
        //            string locationSave = Path.Combine(SiteSettings.PATH_DOWNLOAD, fileName);
        //            var fileInfo = new FileInfo(locationSave);
        //            if (!(fileInfo.Exists && fileInfo.Length > 0))
        //            {
        //                #region CREATE FILE
        //                using (CsvFileWriter csvWriter = new CsvFileWriter(locationSave))
        //                {
        //                    CsvRow row = new CsvRow();
        //                    if (param.DateFrom != DateTime.MinValue
        //                        && param.DateTo != DateTime.MinValue
        //                        && param.DateTo > param.DateFrom)
        //                    {

        //                        Console.WriteLine("Get Database Process");
        //                        Console.WriteLine(string.Format("Paramter From Data : {0} Until {1}", param.DateFrom.Value.ToString(Constant.FORMAT_DATE_JRESOURCES), param.DateTo.Value.ToString(Constant.FORMAT_DATE_JRESOURCES)));

        //                        Company company = Company.GetById(param.CompanyId);
        //                        DataRepositoryStoreAccpac.CurrentDataStore[DataRepositoryStoreAccpac.KEY_DATACONTEXT] = new AccpacEntities(ConnectionManager.GetConnectionStringByCompanyCode(DatabaseGroupName.ACCPAC_DATABASE, company.CompanyCode));

        //                        ADOHelper adoHelper = new ADOHelper(ConfigurationManager.ConnectionStrings["JResourcesConnection"].ConnectionString);

        //                        var reader = adoHelper.ExecDataReaderProc("report_msr_status",
        //                            "@companyId", param.CompanyId,
        //                            "@MSRNo", !string.IsNullOrEmpty(param.MSRNo) ? param.MSRNo : "",
        //                            "@dateFrom", param.DateFrom,
        //                            "@dateTo", param.DateTo,
        //                            "@departmentId", param.DepartmentId,
        //                            "@equipmentCode", !string.IsNullOrEmpty(param.EquipmentNo) ? param.EquipmentNo : ""
        //                            );

        //                        if (reader != null)
        //                        {

        //                            var list_data = CurrentDataContext.CurrentContext.TemporaryMSRStatus.ToList();

        //                            #region Header CSV

        //                            string[] headerColoums = new string[] {
        //                                "No",
        //                                "MSR No",
        //                                "Header Status",
        //                                "MSR Create Date",
        //                                "MSR App. Date",
        //                                "Target Fullfillment Date",
        //                                "Expected Date",
        //                                "Confirmation Date",
        //                                "Priority Order",
        //                                "Lead Time",
        //                                "WONo",
        //                                "Department",
        //                                "Cost Centre",
        //                                "Equipment No",
        //                                "Item No",
        //                                "Description",
        //                                "UOM",
        //                                "Qty Request",
        //                                "Qty Transfered",
        //                                "Qty Cancel",
        //                                "Qty Purchase",
        //                                "Qty Issued",
        //                                "Request Purchase Date",
        //                                "Last Transfer Date",
        //                                "Last Cancel Date",
        //                                "Last Purchase Date",
        //                                "Last Issue Date",
        //                                "Detail MSR Status",
        //                                "Requestor",
        //                                "Remark"
        //                            };

        //                            foreach (var item in headerColoums)
        //                            {
        //                                row.Add(item);
        //                            }

        //                            csvWriter.WriteRow(row);
        //                            #endregion

        //                            #region Data CSV
        //                            TotalRow = list_data.Count;

        //                            foreach (var item in list_data)
        //                            {
        //                                string MSRNo = item.MSRNo;
        //                                string HeaderStatusLabel = item.HeaderStatusLabel;

        //                                string CreatedDate = item.CreatedDate.ToString(Constant.FORMAT_DATE_JRESOURCES);

        //                                string ApproveDate = string.Empty;
        //                                if (item.ApproveDate.HasValue)
        //                                    ApproveDate = item.ApproveDate.Value.ToString(Constant.FORMAT_DATE_JRESOURCES);

        //                                string FullfillmentDate = string.Empty;
        //                                if (item.FullfillmentDate.HasValue)
        //                                    FullfillmentDate = item.FullfillmentDate.Value.ToString(Constant.FORMAT_DATE_JRESOURCES);

        //                                string ExpectedDate = item.ExpectedDate.ToString(Constant.FORMAT_DATE_JRESOURCES);

        //                                string DismantleDate = string.Empty;
        //                                if (item.DismantleDate.HasValue)
        //                                    DismantleDate = item.DismantleDate.Value.ToString(Constant.FORMAT_DATE_JRESOURCES);

        //                                string MaintenanceCode = item.MaintenanceCode;
        //                                string LeadTime = item.LeadTime.ToString();
        //                                string WorkOrderNo = item.WorkOrderNo;
        //                                string DepartmentName = item.DepartmentName;
        //                                string CostCode = item.CostCode;
        //                                string EquipmentNo = item.EquipmentNo;
        //                                string ItemNo = item.ItemNo;
        //                                string Description = item.Description.Replace(',', ' ');
        //                                string UOM = item.UOM;
        //                                string QtyRequest = item.QtyRequest.ToString("#.##");
        //                                string QtyTransfer = item.QtyTransfer.ToString("#.##");
        //                                string QtyCancel = item.QtyCancel.ToString("#.##");
        //                                string QtyPurchasing = item.QtyPurchasing.ToString("#.##");
        //                                string QtyIssued = item.QtyIssued.ToString("#.##");

        //                                string RequisitionDate = string.Empty;
        //                                if (item.RequisitionDate.HasValue)
        //                                    RequisitionDate = item.RequisitionDate.Value.ToString(Constant.FORMAT_DATE_JRESOURCES);

        //                                string DateTransfer = string.Empty;
        //                                if (item.DateTransfer.HasValue)
        //                                    DateTransfer = item.DateTransfer.Value.ToString(Constant.FORMAT_DATE_JRESOURCES);

        //                                string DateCancel = string.Empty;
        //                                if (item.DateCancel.HasValue)
        //                                    DateCancel = item.DateCancel.Value.ToString(Constant.FORMAT_DATE_JRESOURCES);

        //                                string DatePurchasing = string.Empty;
        //                                if (item.DatePurchasing.HasValue)
        //                                    DatePurchasing = item.DatePurchasing.Value.ToString(Constant.FORMAT_DATE_JRESOURCES);

        //                                string DateIssued = string.Empty;
        //                                if (item.DateIssued.HasValue)
        //                                    DateIssued = item.DateIssued.Value.ToString(Constant.FORMAT_DATE_JRESOURCES);

        //                                string DetailStatusLabel = item.DetailStatusLabel;
        //                                string Requestor = item.Requestor;
        //                                string Remark = item.Remark;
        //                                Remark = Regex.Replace(Remark, @"\t|\n|\r", " ");

        //                                string[] strValues = new string[] 
        //                                {
        //                                    (rowIndex + 1).ToString(),
        //                                    MSRNo,
        //                                    HeaderStatusLabel,
        //                                    CreatedDate,
        //                                    ApproveDate,
        //                                    FullfillmentDate,
        //                                    ExpectedDate,
        //                                    DismantleDate,
        //                                    MaintenanceCode,
        //                                    LeadTime,
        //                                    WorkOrderNo,
        //                                    DepartmentName,
        //                                    CostCode,
        //                                    EquipmentNo.Replace(',', ' '),
        //                                    ItemNo,
        //                                    Description.Replace(',', ' '),
        //                                    UOM,
        //                                    QtyRequest,
        //                                    QtyTransfer,
        //                                    QtyCancel,
        //                                    QtyPurchasing,
        //                                    QtyIssued,
        //                                    RequisitionDate,
        //                                    DateTransfer,
        //                                    DateCancel,
        //                                    DatePurchasing,
        //                                    DateIssued,
        //                                    DetailStatusLabel,
        //                                    Requestor,
        //                                    Remark
        //                                };

        //                                rowIndex++;
        //                                row = new CsvRow();
        //                                foreach (var strItem in strValues)
        //                                {
        //                                    row.Add(strItem);
        //                                }
        //                                csvWriter.WriteRow(row);
        //                                CommonFunction.DrawTextProgressBar(rowIndex, TotalRow);
        //                            }

        //                            response = EmailLogic.EmailReport(fileName, REPORT_TYPE.MSR_STATUS, param.MoreCriteria, param.DateFrom.Value, param.DateTo.Value);

        //                            #endregion

        //                            response = EmailLogic.EmailReport(fileName, REPORT_TYPE.MSR_STATUS, param.MoreCriteria, param.DateFrom.Value, param.DateTo.Value);
        //                        }
        //                        else
        //                        {
        //                            row.Add("Dont have data");
        //                        }
        //                    }
        //                    else
        //                    {
        //                        row.Add("Parameter date not valid");
        //                    }
        //                }

        //                #endregion
        //            }     
        //            else
        //            {
        //                //KONDISI JIKA File BERHASIL DIBUAT NAMUN EMAIL GAGAL
        //                response = EmailLogic.EmailReport(fileName, REPORT_TYPE.MSR_STATUS, param.MoreCriteria, param.DateFrom.Value, param.DateTo.Value);
        //            }

        //        }
        //        catch (Exception ex)
        //        {
        //            response.SetError(ex.Message);
        //        }
        //    }
        //    else
        //    {
        //        response.SetError("Parameter date range not valid");
        //    }


        //    if (response.IsSuccess)
        //    {

        //    }

        //    return response;
        //}

        /// <summary>
        /// REPORT MSR STATUS
        /// </summary>
        /// <param name="DataParameter"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static ResponseModel MSR_STATUS_NEW(string DataParameter, string fileName)
        {
            ResponseModel response = new ResponseModel();
            DateTime dtStart = DateTime.Now;
            MSRSearchModel param = Newtonsoft.Json.JsonConvert.DeserializeObject<MSRSearchModel>(DataParameter);

            int TotalRow = 0;
            int rowIndex = 0;
            if (param.DateFrom != DateTime.MinValue && param.DateTo != DateTime.MinValue && param.DateTo > param.DateFrom)
            {
                try
                {
                    string locationSave = Path.Combine(SiteSettings.PATH_DOWNLOAD, fileName);
                    var fileInfo = new FileInfo(locationSave);
                    if (!(fileInfo.Exists && fileInfo.Length > 0))
                    {
                        #region Proses Get Data From Database
                        //DateTime DateFrom = DateTime.Now.AddMonths(-9);
                        //DateTime DateTo = DateTime.Now;

                        var MSRtemps = MSR.GetAll().Where(x =>
                            x.CreatedDate >= param.DateFrom &&
                            x.CreatedDate <= param.DateTo &&
                            x.CompanyID == param.CompanyId);

                        if (!string.IsNullOrEmpty(param.RequestStatus))
                        {
                            MSRtemps = MSRtemps.Where(x => x.MSRStatus == param.RequestStatus);
                        }

                        if (param.DepartmentId != Guid.Empty)
                        {
                            MSRtemps = MSRtemps.Where(x => x.DepartmentId == param.DepartmentId);
                        }

                        if (!string.IsNullOrEmpty(param.EquipmentNo))
                        {
                            MSRtemps = MSRtemps.Where(x => x.EquipmentNo == param.EquipmentNo);
                        }

                        if (!string.IsNullOrEmpty(param.MSRNo))
                        {
                            MSRtemps = MSRtemps.Where(x => x.MSRNo == param.MSRNo);
                        }

                        List<MSR> MSRs = MSRtemps.OrderBy(x => x.CreatedDate).ToList();


                        List<EnumTable> enumHearStatus = EnumTable.GetByCode(EnumTable.EnumType.STATUS_MSR).ToList();

                        TotalRow = MSRs.Count;

                        List<MSRStatusModel> reportItems = new List<MSRStatusModel>();
                        foreach (var itemMSR in MSRs)
                        {
                            List<MSRDetail> MSRDetails = MSRDetail.GetAllByMSRId(itemMSR.ID).ToList();
                            var Issueds = MSRIssued.GetByMSRId(itemMSR.ID).ToList();
                            List<MSRStatusDetailModel> reportItemDetails = new List<MSRStatusDetailModel>();

                            EnumTable enumHeaderStatus = enumHearStatus.FirstOrDefault(x => x.EnumValue == itemMSR.MSRStatus);
                            DocumentApproval docApproval = DocumentApproval.GetByDocumentNo(itemMSR.MSRNo).OrderByDescending(x => x.Sequence).FirstOrDefault(x => x.IsFinish == true);
                            Department department = Department.GetById(itemMSR.DepartmentId);
                            UserLogin userLogin = UserLogin.GetByNIKSite(itemMSR.CreatedBy);

                            foreach (var issued in Issueds)
                            {
                                List<MSRIssuedDetail> IssuedDetails = MSRIssuedDetail.GetByMSRIssuedId(issued.ID).ToList();

                                #region Processing Detail
                                foreach (var issuedDetail in IssuedDetails)
                                {
                                    MSRStatusDetailModel reportItemDetail = new MSRStatusDetailModel();
                                    reportItemDetail.LineNo = issuedDetail.LineNo;
                                    reportItemDetail.ItemNo = issuedDetail.ItemNo;

                                    switch (issued.IssuedType)
                                    {
                                        case RequestStatus.TRANSFER:
                                            reportItemDetail.QtyTransfer = issuedDetail.Qty;
                                            reportItemDetail.DateTransfer = issued.CreatedDate;
                                            break;

                                        case RequestStatus.REVERSE_TRANSFER:
                                            reportItemDetail.QtyTransfer = issuedDetail.Qty;
                                            reportItemDetail.DateReverseTransfer = issued.CreatedDate;
                                            break;

                                        case RequestStatus.CANCEL:
                                            reportItemDetail.QtyCancel = issuedDetail.Qty;
                                            reportItemDetail.DateCancel = issued.CreatedDate;
                                            break;

                                        case RequestStatus.PURCHASING:
                                            reportItemDetail.QtyPurchasing = issuedDetail.Qty;
                                            reportItemDetail.DatePurchasing = issued.CreatedDate;
                                            break;

                                        case RequestStatus.ISSUED:
                                            reportItemDetail.QtyIssued = issuedDetail.Qty;
                                            reportItemDetail.DateIssued = issued.CreatedDate;
                                            break;
                                    }

                                    reportItemDetails.Add(reportItemDetail);
                                }
                                #endregion
                            }

                            MSRDetails = MSRDetails.OrderBy(x => x.LineNo).ToList();
                            foreach (var msrDetail in MSRDetails)
                            {
                                MSRStatusModel reportItem = new MSRStatusModel();
                                reportItem.MSRNo = itemMSR.MSRNo;
                                reportItem.HeaderStatusLabel = enumHeaderStatus.EnumLabel;
                                reportItem.CreatedDate = itemMSR.CreatedDate;
                                reportItem.MaintenanceCode = itemMSR.MaintenanceCode;
                                reportItem.WorkOrderNo = itemMSR.WorkOrderNo;
                                reportItem.LeadTime = itemMSR.LeadTime;
                                reportItem.DepartmentName = department.DepartmentName;
                                reportItem.CostCode = itemMSR.CostCode;
                                reportItem.EquipmentNo = itemMSR.EquipmentNo;
                                reportItem.Remark = (!String.IsNullOrEmpty(itemMSR.Remark) ? itemMSR.Remark : "");
                                reportItem.ExpectedDate = itemMSR.ExpectedDate;
                                reportItem.DismantleDate = itemMSR.DismantleDate;
                                reportItem.RequisitionDate = itemMSR.RequisitionDate;

                                if (userLogin != null && userLogin.ID != Guid.Empty)
                                {
                                    reportItem.Requestor = userLogin.FullName;
                                }
                                else
                                {
                                    reportItem.Requestor = string.Empty;
                                }

                                if (docApproval != null && docApproval.ID != Guid.Empty)
                                {
                                    reportItem.ApproveDate = docApproval.UpdatedDate;
                                }

                                if (reportItem.ApproveDate.HasValue)
                                {
                                    reportItem.FullfillmentDate = reportItem.ApproveDate.Value.AddDays(reportItem.LeadTime);
                                }

                                reportItem.LineNo = msrDetail.LineNo;
                                reportItem.ItemNo = msrDetail.ItemNo;
                                reportItem.Description = msrDetail.Description;
                                reportItem.UOM = msrDetail.UOM;
                                reportItem.QtyRequest = msrDetail.Qty;
                                reportItem.DetailStatusLabel = "Not Issued";

                                var groupByItem = reportItemDetails.Where(x => x.LineNo == reportItem.LineNo).ToList();
                                if (groupByItem != null && groupByItem.Count > 0)
                                {
                                    reportItem.QtyTransfer = groupByItem.Sum(x => x.QtyTransfer);
                                    reportItem.QtyCancel = groupByItem.Sum(x => x.QtyCancel);
                                    reportItem.QtyPurchasing = groupByItem.Sum(x => x.QtyPurchasing);
                                    reportItem.QtyIssued = groupByItem.Sum(x => x.QtyIssued);
                                    reportItem.DateTransfer = groupByItem.Max(x => x.DateTransfer);
                                    reportItem.DateCancel = groupByItem.Max(x => x.DateCancel);
                                    reportItem.DatePurchasing = groupByItem.Max(x => x.DatePurchasing);
                                    reportItem.DateIssued = groupByItem.Max(x => x.DateIssued);

                                    if (reportItem.QtyTransfer == reportItem.QtyIssued)
                                    {
                                        reportItem.DetailStatusLabel = "COMPLETED";
                                    }

                                    else if (reportItem.QtyTransfer == (reportItem.QtyTransfer + reportItem.QtyReverseTransfer))
                                    {
                                        reportItem.DetailStatusLabel = "COMPLETED";
                                    }

                                    else if (reportItem.QtyTransfer == reportItem.QtyCancel)
                                    {
                                        reportItem.DetailStatusLabel = "CANCEL";
                                    }

                                    else if ((reportItem.QtyTransfer + reportItem.QtyCancel + reportItem.QtyPurchasing + reportItem.QtyIssued) > 0)
                                    {
                                        reportItem.DetailStatusLabel = "PARTIAL PROCESS";
                                    }
                                }

                                reportItems.Add(reportItem);
                            }


                            rowIndex++;
                            CommonFunction.DrawTextProgressBar(rowIndex, TotalRow);
                        }
                        #endregion

                        #region CREATE FILE
                        using (CsvFileWriter csvWriter = new CsvFileWriter(locationSave))
                        {
                            CsvRow row = new CsvRow();
                            if (param.DateFrom != DateTime.MinValue
                                && param.DateTo != DateTime.MinValue
                                && param.DateTo > param.DateFrom)
                            {

                                Console.WriteLine("Get Database Process");
                                Console.WriteLine(string.Format("Paramter From Data : {0} Until {1}", param.DateFrom.Value.ToString(Constant.FORMAT_DATE_JRESOURCES), param.DateTo.Value.ToString(Constant.FORMAT_DATE_JRESOURCES)));

                                #region Header CSV

                                string[] headerColoums = new string[] {
                                        "No",
                                        "MSR No",
                                        "Header Status",
                                        "MSR Create Date",
                                        "MSR App. Date",
                                        "Target Fullfillment Date",
                                        "Expected Date",
                                        "Confirmation Date",
                                        "Priority Order",
                                        "Lead Time",
                                        "WONo",
                                        "Department",
                                        "Cost Centre",
                                        "Equipment No",
                                        "Item No",
                                        "Description",
                                        "UOM",
                                        "Qty Request",
                                        "Qty Transfered",
                                        "Qty Cancel",
                                        "Qty Purchase",
                                        "Qty Issued",
                                        "Request Purchase Date",
                                        "Last Transfer Date",
                                        "Last Cancel Date",
                                        "Last Purchase Date",
                                        "Last Issue Date",
                                        "Detail MSR Status",
                                        "Requestor",
                                        "Remark"
                                    };

                                foreach (var item in headerColoums)
                                {
                                    row.Add(item);
                                }

                                csvWriter.WriteRow(row);
                                #endregion

                                #region Data CSV
                                Console.WriteLine("");
                                rowIndex = 0;
                                TotalRow = reportItems.Count;

                                //reportItems = reportItems.OrderBy(x => x.MSRNo).OrderBy(x => x.LineNo).ToList();
                                foreach (var item in reportItems)
                                {
                                    string MSRNo = item.MSRNo;
                                    string HeaderStatusLabel = item.HeaderStatusLabel;

                                    string CreatedDate = item.CreatedDate.ToString(Constant.FORMAT_DATE_JRESOURCES);

                                    string ApproveDate = string.Empty;
                                    if (item.ApproveDate.HasValue)
                                        ApproveDate = item.ApproveDate.Value.ToString(Constant.FORMAT_DATE_JRESOURCES);

                                    string FullfillmentDate = string.Empty;
                                    if (item.FullfillmentDate.HasValue)
                                        FullfillmentDate = item.FullfillmentDate.Value.ToString(Constant.FORMAT_DATE_JRESOURCES);

                                    string ExpectedDate = item.ExpectedDate.ToString(Constant.FORMAT_DATE_JRESOURCES);

                                    string DismantleDate = string.Empty;
                                    if (item.DismantleDate.HasValue)
                                        DismantleDate = item.DismantleDate.Value.ToString(Constant.FORMAT_DATE_JRESOURCES);

                                    string MaintenanceCode = item.MaintenanceCode;
                                    string LeadTime = item.LeadTime.ToString();
                                    string WorkOrderNo = item.WorkOrderNo;
                                    string DepartmentName = item.DepartmentName;
                                    string CostCode = item.CostCode;
                                    string EquipmentNo = item.EquipmentNo;
                                    string ItemNo = item.ItemNo;
                                    string Description = item.Description.Replace(',', ' ');
                                    string UOM = item.UOM;
                                    string QtyRequest = item.QtyRequest.ToString("#.##");
                                    string QtyTransfer = item.QtyTransfer.ToString("#.##");
                                    string QtyCancel = item.QtyCancel.ToString("#.##");
                                    string QtyPurchasing = item.QtyPurchasing.ToString("#.##");
                                    string QtyIssued = item.QtyIssued.ToString("#.##");

                                    string RequisitionDate = string.Empty;
                                    if (item.RequisitionDate.HasValue)
                                        RequisitionDate = item.RequisitionDate.Value.ToString(Constant.FORMAT_DATE_JRESOURCES);

                                    string DateTransfer = string.Empty;
                                    if (item.DateTransfer != DateTime.MinValue)
                                        DateTransfer = item.DateTransfer.ToString(Constant.FORMAT_DATE_JRESOURCES);

                                    string DateCancel = string.Empty;
                                    if (item.DateCancel != DateTime.MinValue)
                                        DateCancel = item.DateCancel.ToString(Constant.FORMAT_DATE_JRESOURCES);

                                    string DatePurchasing = string.Empty;
                                    if (item.DatePurchasing != DateTime.MinValue)
                                        DatePurchasing = item.DatePurchasing.ToString(Constant.FORMAT_DATE_JRESOURCES);

                                    string DateIssued = string.Empty;
                                    if (item.DateIssued != DateTime.MinValue)
                                        DateIssued = item.DateIssued.ToString(Constant.FORMAT_DATE_JRESOURCES);

                                    string DetailStatusLabel = item.DetailStatusLabel;
                                    string Requestor = item.Requestor;
                                    string Remark = item.Remark;
                                    Remark = Regex.Replace(Remark, @"\t|\n|\r", " ");

                                    string[] strValues = new string[] 
                                        {
                                            (rowIndex + 1).ToString(),
                                            MSRNo,
                                            HeaderStatusLabel,
                                            CreatedDate,
                                            ApproveDate,
                                            FullfillmentDate,
                                            ExpectedDate,
                                            DismantleDate,
                                            MaintenanceCode,
                                            LeadTime,
                                            WorkOrderNo,
                                            DepartmentName,
                                            CostCode,
                                            EquipmentNo.Replace(',', ' '),
                                            ItemNo,
                                            Description.Replace(',', ' '),
                                            UOM,
                                            QtyRequest,
                                            QtyTransfer,
                                            QtyCancel,
                                            QtyPurchasing,
                                            QtyIssued,
                                            RequisitionDate,
                                            DateTransfer,
                                            DateCancel,
                                            DatePurchasing,
                                            DateIssued,
                                            DetailStatusLabel,
                                            Requestor,
                                            Remark
                                        };

                                    rowIndex++;
                                    row = new CsvRow();
                                    foreach (var strItem in strValues)
                                    {
                                        row.Add(strItem);
                                    }
                                    csvWriter.WriteRow(row);
                                    CommonFunction.DrawTextProgressBar(rowIndex, TotalRow);
                                }

                                response = EmailLogic.EmailReport(fileName, REPORT_TYPE.MSR_STATUS, param.MoreCriteria, param.DateFrom.Value, param.DateTo.Value);

                                #endregion
                            }
                            else
                            {
                                row.Add("Parameter date not valid");
                            }
                        }

                        #endregion
                    }
                    else
                    {
                        //KONDISI JIKA File BERHASIL DIBUAT NAMUN EMAIL GAGAL
                        response = EmailLogic.EmailReport(fileName, REPORT_TYPE.MSR_STATUS, param.MoreCriteria, param.DateFrom.Value, param.DateTo.Value);
                    }

                }
                catch (Exception ex)
                {
                    response.SetError(ex.Message);
                }
            }
            else
            {
                response.SetError("Parameter date range not valid");
            }

            string EstimateTime = String.Format("Range Process Time : ({0}-{1}) --- Process Time: ({2} Minute) ", dtStart.ToString(Constant.FORMAT_DATETIME_JRESOURCES), DateTime.Now.ToString(Constant.FORMAT_TIME_JRESOURCES), (int)DateTime.Now.Subtract(dtStart).TotalMinutes);
            response.Message = EstimateTime + response.Message;

            return response;
        }

        /// <summary>
        /// Delete old file report
        /// </summary>
        public static void DeleteReport()
        {
            string FileLocation = SiteSettings.PATH_DOWNLOAD;

            if(!string.IsNullOrEmpty(FileLocation))
            {
                DirectoryInfo d = new DirectoryInfo(FileLocation);//Assuming Test is your Folder
                                                                  // Get the files in the directory and print out some information about them.
                System.IO.FileInfo[] fileNames = d.GetFiles("*.csv");


                foreach (System.IO.FileInfo fi in fileNames)
                {
                    double limitFileDate = DateTime.Now.Subtract(fi.CreationTime).TotalDays;
                    if (limitFileDate >= 7)
                    {
                        Console.WriteLine("Delete File {0}: {1}: {2}", fi.Name, fi.CreationTime, fi.Length);
                        File.Delete(Path.Combine(FileLocation, fi.FullName));
                    }
                }
            }
        }
    }



}
