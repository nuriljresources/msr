﻿using JResources.Common;
using JResources.Data;
using JResources.Data.Model;
using JResources.Data.Model.Accpac;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace JResources.BusinessLogic
{
    public class FuelProcessLogic
    {
        public string FRFNo { get; set; }
        public Guid CompanyId { get; set; }
        public String TypeTransaction { get; set; }

        public List<FuelModel> ListModel { get; set; }
        public FuelProcessLogic(Guid companyId, string frfNo)
        {
            CompanyId = companyId;
            FRFNo = frfNo;
            ListModel = new List<FuelModel>();
            SetModelFromFTS();
        }

        public void SetModelFromFTS()
        {
            ADOHelper ConnectionCheck = new ADOHelper(ConfigurationManager.ConnectionStrings[Constant.FTS_DATABSE].ConnectionString);
            string queryFRFData = string.Format("SELECT * FROM [tdaily_transaction_fts] WHERE frf_no = '{0}'", FRFNo);
            SqlDataReader reader = ConnectionCheck.ExecDataReader(queryFRFData, "@FRFNo", FRFNo);

            while (reader.Read())
            {
                FuelModel model = new FuelModel();
                model.IDS = reader.GetGuid(0);
                model.filling_typ = reader.GetString(1);
                model.filling_dttm = reader.GetDateTime(2);
                model.fuelman_id = reader.GetString(3);
                model.comp_id = reader.GetString(4);
                model.fuelstation_id = reader.GetString(5);
                model.item_no = reader.GetString(6);
                model.equ_tank_id = reader.GetString(7);
                model.operator_id = reader.GetString(8);
                model.prev_hmkm = reader.GetDecimal(9);
                model.last_hmkm = reader.GetDecimal(10);
                model.start_totalisator = reader.GetDecimal(11);
                model.end_totalisator = reader.GetDecimal(12);
                model.quantity = reader.GetDecimal(13);
                model.prev_stock = reader.GetDecimal(14);
                model.end_stock = reader.GetDecimal(15);
                model.remark = reader.GetString(16);
                model.frf_no = reader.GetString(17);
                model.is_posted = (reader.IsDBNull(18) ? false : reader.GetBoolean(18));
                model.shift = (reader.IsDBNull(19) ? string.Empty : reader.GetString(19)); 
                model.posting_status = (reader.IsDBNull(20) ? string.Empty : reader.GetString(20));
                model.posting_log = (reader.IsDBNull(21) ? string.Empty : reader.GetString(21));
                model.fuelstation_id_to = (reader.IsDBNull(22) ? string.Empty : reader.GetString(22));
                model.cat_code = (reader.IsDBNull(23) ? string.Empty : reader.GetString(23));
                model.cccode = (reader.IsDBNull(24) ? string.Empty : reader.GetString(24));
                model.mt_code = (reader.IsDBNull(25) ? string.Empty : reader.GetString(25));
                model.po_no = (reader.IsDBNull(26) ? string.Empty : reader.GetString(26));
                model.do_no = (reader.IsDBNull(27) ? string.Empty : reader.GetString(27));

                ListModel.Add(model);
                TypeTransaction = model.filling_typ;
            }
        }

        /// <summary>
        /// GET MODEL FOR REFUEL
        /// </summary>
        /// <returns></returns>
        public IC0640 GetModelRefuel()
        {
            IC0640 ic0640 = new IC0640();
            Company company = Company.GetById(CompanyId);
            ConnectionManager.SetConnection("", company.CompanyCode);
            DataRepositoryStoreAccpac.CurrentDataStore[DataRepositoryStoreAccpac.KEY_DATACONTEXT] = new AccpacEntities(JResources.Common.ConnectionManager.GetConnectionStringByCompanyCode(Common.DatabaseGroupName.ACCPAC_DATABASE, company.CompanyCode));
            var dataSource = AccpacDataContext.GetDatabaseName(company.CompanyCode);

            var HeaderFuel = ListModel.FirstOrDefault();

            ic0640.DBUsername = SiteSettings.ACCPAC_USERNAME;
            ic0640.DBPassword = SiteSettings.ACCPAC_PASSWORD;
            ic0640.DBName = dataSource;
            ic0640.DOCNUM = HeaderFuel.frf_no;
            ic0640.HDRDESC = string.Format("{0} FRF Import to Accpac automatically", HeaderFuel.frf_no);
            ic0640.REFERENCE = "";
            ic0640.TRANSDATE = HeaderFuel.filling_dttm;
            ic0640.DATEBUS = HeaderFuel.filling_dttm;
            ic0640.CUSTNO = string.Empty;
            ic0640.CONTACT = string.Empty;
            ic0640.STATUS = "1";
            ic0640.CURRENCY = company.Currency;
            ic0640.OPTFIELD = "IRFNO";
            ic0640.SWSET = "1";
            ic0640.VALIFTEXT = HeaderFuel.frf_no;

            foreach (var item in ListModel)
            {
                IC0630 ic0630 = new IC0630();


                var ICITEM = AccpacDataContext.CurrentContext.ICITEMs.FirstOrDefault(x => x.ITEMNO == item.item_no);
                var ICILOC = AccpacDataContext.CurrentContext.ICILOCs.FirstOrDefault(x => x.ACTIVE == 1 && x.ITEMNO == ICITEM.ITEMNO);

                ic0630.ITEMNO = ICITEM.ITEMNO;
                ic0630.CATEGORY = item.cat_code;
                ic0630.UNIT = ICILOC.COSTUNIT;

                ic0630.LOCATION = item.fuelstation_id;
                ic0630.QUANTITY = item.quantity;

                ic0630.IC0635List.Add(new IC0635()
                {
                    OPTFIELD = "CCCODE",
                    SWSET = "1",
                    VALIFTEXT = item.cccode,
                });

                ic0630.IC0635List.Add(new IC0635()
                {
                    OPTFIELD = "ECCODE",
                    SWSET = "1",
                    VALIFTEXT = item.equ_tank_id,
                });

                ic0630.IC0635List.Add(new IC0635()
                {
                    OPTFIELD = "MTCODE",
                    SWSET = "1",
                    VALIFTEXT = item.mt_code,
                });

                ic0630.COMMENTS = "12345";
                ic0630.MANITEMNO = "12345";
                ic0630.FUNCTION = "100";

                ic0640.IC0630List.Add(ic0630);
            }

            return ic0640;
        }


        /// <summary>
        /// GET MODEL FOR TRANSFER
        /// </summary>
        /// <param name="TransferNo"></param>
        /// <returns></returns>
        public IC0740 GetModelTransfer()
        {
            IC0740 ic0740 = new IC0740();

            var HeaderModel = ListModel.FirstOrDefault();

            Company company = Company.GetById(CompanyId);
            string DataSource = AccpacDataContext.GetDatabaseName(company.CompanyCode);
            DataRepositoryStoreAccpac.CurrentDataStore[DataRepositoryStoreAccpac.KEY_DATACONTEXT] = new AccpacEntities(JResources.Common.ConnectionManager.GetConnectionStringByCompanyCode(Common.DatabaseGroupName.ACCPAC_DATABASE, company.CompanyCode));
            ic0740.DBName = DataSource;
            ic0740.DBUsername = SiteSettings.ACCPAC_USERNAME;
            ic0740.DBPassword = SiteSettings.ACCPAC_PASSWORD;

            ic0740.DOCNUM = HeaderModel.frf_no;
            ic0740.TRANSDATE = HeaderModel.filling_dttm;
            ic0740.DATEBUS = HeaderModel.filling_dttm;
            ic0740.EXPARDATE = HeaderModel.filling_dttm.AddDays(1);
            ic0740.HDRDESC = HeaderModel.remark;
            ic0740.REFERENCE = HeaderModel.frf_no;
            ic0740.ADDCOST = "0";
            ic0740.PRORMETHOD = "3";
            ic0740.STATUS = "1";

            int lineNo = 0;
            foreach (var item in ListModel)
            {
                IC0730 ic0730 = new IC0730();
                ic0730.LINENO = lineNo++;
                ic0730.ITEMNO = item.item_no;
                ic0730.FROMLOC = item.fuelstation_id;
                ic0730.TOLOC = item.fuelstation_id_to;
                ic0730.QTYREQ = item.quantity;
                ic0730.QUANTITY = item.quantity;
                ic0730.UNITREQ = "LITRE";
                ic0730.UNIT = "LITRE";
                ic0730.COMMENTS = (string.IsNullOrEmpty(item.remark) ? string.Empty : item.remark);
                ic0730.MANITEMNO = "M101";
                ic0730.EXTWEIGHT = "200.0000";
                ic0730.FUNCTION = "100";

                ic0740.IC0730List.Add(ic0730);
            }

            return ic0740;
        }


        public IC0RCV GetModelReceipt()
        {
            IC0RCV ic0RCV = new IC0RCV();

            var HeaderModel = ListModel.FirstOrDefault();

            Company company = Company.GetById(CompanyId);
            string DataSource = AccpacDataContext.GetDatabaseName(company.CompanyCode);
            DataRepositoryStoreAccpac.CurrentDataStore[DataRepositoryStoreAccpac.KEY_DATACONTEXT] = new AccpacEntities(JResources.Common.ConnectionManager.GetConnectionStringByCompanyCode(Common.DatabaseGroupName.ACCPAC_DATABASE, company.CompanyCode));

            ic0RCV.DBName = DataSource;
            ic0RCV.DBUserName = SiteSettings.ACCPAC_USERNAME;
            ic0RCV.DBPassword = SiteSettings.ACCPAC_PASSWORD;

            ic0RCV.receiptNo = HeaderModel.frf_no;
            ic0RCV.receiptType = "1";
            ic0RCV.desc = HeaderModel.remark; // REMARKS
            ic0RCV.reference = "REF01"; // NOMOR DO
            ic0RCV.receiptDate = HeaderModel.filling_dttm;
            ic0RCV.postingDate = HeaderModel.filling_dttm;
            ic0RCV.PONo = HeaderModel.po_no; // NOMOR PO Dari Table
            ic0RCV.vendorNo = ""; // KONFIRM YADI, Harusnya Otomatis berdasarkan PO
            ic0RCV.receiptCurr = ""; //company.Currency;
            ic0RCV.receiptRate = "";
            ic0RCV.addCostCurr = "";
            ic0RCV.addCost = "0,000.00";
            ic0RCV.do_no = HeaderModel.do_no;

        //    AddDataTableItem("J01-000139", "W03", "4,000.0000", "PC", "1.000000", "1", "COM-01", "ITEM-001"
        //            , "CCCODE", "1", "A0102", "", "1", "", "", "1", ""
        //        );

            //    gvItem.Refresh();

            //}

            foreach (var item in ListModel)
            {
                
                
                var icITEM = AccpacDataContext.CurrentContext.ICITEMs.FirstOrDefault(x => x.ITEMNO == item.item_no);
                //var icILOC = AccpacDataContext.CurrentContext.ICILOCs.FirstOrDefault(x => x.ACTIVE == 1 && x.ITEMNO == item.item_no);

                ICRCVItem icRCVItem = new ICRCVItem();
                icRCVItem.itemNo = icITEM.FMTITEMNO;
                icRCVItem.itemLocation = item.fuelstation_id;
                icRCVItem.qtyReceipt = item.quantity.ToString();
                icRCVItem.UOM = icITEM.STOCKUNIT;
                icRCVItem.unitCost = "0.000000";

                icRCVItem.labels = "1";
                icRCVItem.comments = "COM-01";
                icRCVItem.manItemNo = "ITEM-001";
                icRCVItem.optField1 = "CCCODE";
                icRCVItem.optField1ValSet = "1";
                icRCVItem.optField1Val = "A0102";
                icRCVItem.optField2 = "";
                icRCVItem.optField2ValSet = "1";
                icRCVItem.optField2Val = "";
                icRCVItem.optField3 = "";
                icRCVItem.optField3ValSet = "1";
                icRCVItem.optField3Val = "";

                ic0RCV.ICRCVList.Add(icRCVItem);
            }

            return ic0RCV;
        }
    }
}
