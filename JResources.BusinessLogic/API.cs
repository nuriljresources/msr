﻿using JResources.Data.Model;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Net;

namespace JResources.BusinessLogic
{
    public class API
    {
        public string BASE_URL { get; set; }
        public bool IsTrigger { get; set; }

        public ResponseModel Post(string uri, Object obj = null)
        {
            ResponseModel responseObject = new ResponseModel();

            try
            {
                using (var client = new HttpClient(new HttpClientHandler() { UseDefaultCredentials = true }))
                {
                    if (IsTrigger)
                    {
                        client.Timeout = new TimeSpan(0, 0, 1);
                    }
                    else
                    {
                        client.Timeout = new TimeSpan(0, 3, 0);
                    }

                    client.BaseAddress = new Uri(BASE_URL + "/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var data = JsonConvert.SerializeObject(obj);
                    var result = client.PostAsync(uri, new StringContent(data, Encoding.UTF8, "application/json")).Result;
                    string resultContent = result.Content.ReadAsStringAsync().Result;

                    if (result.IsSuccessStatusCode)
                    {
                        if (!string.IsNullOrEmpty(resultContent))
                        {
                            responseObject = JsonConvert.DeserializeObject<ResponseModel>(resultContent);
                        }
                    }
                    else
                    {
                        responseObject.SetError(resultContent);
                    }
                }
            }
            catch (WebException webEx)
            {
                if (webEx.Status == WebExceptionStatus.Timeout)
                {
                    if (IsTrigger)
                    {
                        responseObject.SetSuccess(webEx.Message);
                    }
                    else
                    {
                        responseObject.SetError(webEx.Message);
                    }

                    responseObject.IsTimeOut = true;
                }
                else
                {
                    responseObject.SetError(webEx.Message);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null && ex.InnerException.Message.Contains("A task was canceled"))
                {
                    if (IsTrigger)
                    {
                        responseObject.SetSuccess(ex.Message);
                    }
                    else
                    {
                        responseObject.SetError(ex.Message);
                    }
                    
                    responseObject.IsTimeOut = true;
                }
                else
                {
                    responseObject.SetError(ex.Message);
                }
            }

            if (IsTrigger)
            {
                responseObject.SetSuccess();
            }

            return responseObject;
        }
    }
}
