﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JResources.Data;
using JResources.Common;

namespace JResources.BusinessLogic
{
    public class DashboardLogic
    {
        public static int GetTotalNewRequest(Guid _companyID)
        {
            var MSRTOtal = MSR.GetAll().Where(x => x.CompanyID == _companyID && x.MSRStatus == JResources.Common.Enum.RequestStatus.NEW).Take(10).Count();
            return MSRTOtal;
        }

        public static int GetTotalApproveRequest(Guid _companyID)
        {
            var MSRTOtal = MSR.GetAll().Where(x => x.CompanyID == _companyID && x.MSRStatus == JResources.Common.Enum.RequestStatus.APPROVED).Take(10).Count();
            return MSRTOtal;
        }

        public static int GetTotalTransferWarehouse(Guid _companyID)
        {
            var MSRTOtal = MSRIssued.GetAll().Where(x => x.CompanyId == _companyID && x.IssuedType == JResources.Common.Enum.RequestStatus.ISSUED).Take(10).Count();
            return MSRTOtal;
        }

        public static int GetTotalPendingToAccpac()
        {
            return 0;
        }
    }
}
