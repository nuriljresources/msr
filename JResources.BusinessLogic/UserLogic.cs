﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JResources.Data;
using JResources.Data.Model;
using Omu.ValueInjecter;
using JResources.Common;
using System.DirectoryServices;
using System.Web.Mvc;
using System.Transactions;

namespace JResources.BusinessLogic
{
    public class UserLogic
    {
        public static ResponseModel SaveUser(UserModel model)
        {
            ResponseModel response = ValidationSaveUser(model);
            if (response.IsSuccess)
            {
                List<RoleAccessUser> listRoleAccessUserNew = new List<RoleAccessUser>();
                List<RoleAccessUser> listRoleAccessUserDelete = new List<RoleAccessUser>();

                var userLogin = UserLogin.GetByUsername(model.Username);
                if (userLogin != null)
                {
                    #region UPDATE USER
                    userLogin.NIKSite = model.NIKSite;
                    userLogin.FullName = model.FullName;
                    userLogin.DepartmentId = model.DepartmentId;
                    userLogin.Email = model.Email;
                    userLogin.Save<UserLogin>();
                    #endregion
                }
                else
                {
                    #region INSERT USER
                    userLogin = new UserLogin();
                    userLogin.InjectFrom(model);
                    userLogin.ID = Guid.NewGuid();
                    userLogin.NIKSite = model.NIKSite;
                    userLogin.Username = model.Username;
                    userLogin.Password = string.Empty;
                    userLogin.FullName = model.FullName;
                    userLogin.Email = model.Email;
                    userLogin.DepartmentId = model.DepartmentId;

                    // Default Role
                    var roleDefault = RoleAccess.GetByCode(ROLE_CODE.USERS);
                    RoleAccessUser defaultRoleAccessUser = new RoleAccessUser();
                    defaultRoleAccessUser.ID = Guid.NewGuid();
                    defaultRoleAccessUser.UserId = userLogin.ID;
                    defaultRoleAccessUser.RoleAccessId = roleDefault.ID;
                    defaultRoleAccessUser.CompanyId = model.CompanyId;
                    defaultRoleAccessUser.DepartmentId = model.DepartmentId;
                    defaultRoleAccessUser.AdditionalValue = "";
                    defaultRoleAccessUser.IsDefault = true;
                    defaultRoleAccessUser.DateFrom = DateTime.Now;
                    defaultRoleAccessUser.DateTo = DateTime.Now.AddYears(100);
                    listRoleAccessUserNew.Add(defaultRoleAccessUser);

                    #endregion
                }

                #region SET ROLE ACCESS
                if (model.ListRoleDepartment != null && model.ListRoleDepartment.Count > 0)
                {
                    List<Guid> listGuidExist = new List<Guid>();
                    List<RoleAccessUserModel> listUpdate = new List<RoleAccessUserModel>();

                    var RoleAccessUsers = RoleAccessUser.GetByUserId(userLogin.ID).ToList();

                    #region Populate From String
                    model.ListRoleDepartment = model.ListRoleDepartment.Distinct().ToList();
                    foreach (var itemRoleDepartment in model.ListRoleDepartment)
                    {
                        string[] splitStr = itemRoleDepartment.Split('|');
                        if (splitStr != null && splitStr.Length > 2)
                        {
                            Guid roleId = Guid.Empty;
                            Guid companyId = Guid.Empty;
                            Guid departmentId = Guid.Empty;

                            Guid.TryParse(splitStr[0], out roleId);
                            Guid.TryParse(splitStr[1], out companyId);
                            Guid.TryParse(splitStr[2], out departmentId);

                            if (roleId != Guid.Empty && companyId != Guid.Empty && departmentId != Guid.Empty)
                            {
                                RoleAccessUserModel roleAccessUser = new RoleAccessUserModel();
                                roleAccessUser.RoleAccessID = roleId;
                                roleAccessUser.CompanyId = companyId;
                                roleAccessUser.DepartmentId = departmentId;

                                var checkdouble = listUpdate.FirstOrDefault(x => x.RoleAccessID == roleAccessUser.RoleAccessID
                                    && x.CompanyId == roleAccessUser.CompanyId
                                    && x.DepartmentId == roleAccessUser.DepartmentId);

                                if(checkdouble == null)
                                    listUpdate.Add(roleAccessUser);
                            }
                        }
                    }

                    #endregion

                    foreach (var item in listUpdate)
                    {
                        var checkExistingRole = RoleAccessUsers.FirstOrDefault(x => x.RoleAccessId == item.RoleAccessID
                            && x.CompanyId == item.CompanyId
                            && x.DepartmentId == item.DepartmentId);

                        if (checkExistingRole != null)
                        {
                            listGuidExist.Add(checkExistingRole.ID);
                        }
                        else
                        {
                            RoleAccessUser roleAccessUser = new RoleAccessUser();
                            roleAccessUser.ID = Guid.NewGuid();
                            roleAccessUser.UserId = userLogin.ID;
                            roleAccessUser.RoleAccessId = item.RoleAccessID;
                            roleAccessUser.CompanyId = item.CompanyId;
                            roleAccessUser.DepartmentId = item.DepartmentId;
                            roleAccessUser.AdditionalValue = "";
                            roleAccessUser.IsDefault = true;
                            roleAccessUser.DateFrom = DateTime.Now;
                            roleAccessUser.DateTo = DateTime.Now.AddYears(100);
                            listRoleAccessUserNew.Add(roleAccessUser);
                        }
                    }

                    listRoleAccessUserDelete = RoleAccessUsers.Where(x => !listGuidExist.Contains(x.ID)).ToList();
                }
                #endregion

                
                using (var transaction = new TransactionScope())
                {
                    userLogin.Save<UserLogin>();

                    if (listRoleAccessUserDelete != null && listRoleAccessUserDelete.Count > 0)
                    {
                        foreach (var item in listRoleAccessUserDelete)
                        {
                            item.Delete<RoleAccessUser>();
                        }
                    }

                    if (listRoleAccessUserNew != null && listRoleAccessUserNew.Count > 0)
                    {
                        foreach (var item in listRoleAccessUserNew)
                        {
                            item.Save<RoleAccessUser>();
                        }
                    }

                    transaction.Complete();
                }

                response.SetSuccess("User : {0} Has been saved", userLogin.FullName);
            }

            return response;
        }

        public static ResponseModel ValidationSaveUser(UserModel model)
        {
            ResponseModel response = new ResponseModel(true);
            StringBuilder sbMessage = new StringBuilder();

            if (string.IsNullOrEmpty(model.Username))
            {
                sbMessage.AppendLine("Username can't be empty");
            }

            if (string.IsNullOrEmpty(model.NIKSite))
            {
                sbMessage.AppendLine("NIKSite can't be empty");
            }

            if (string.IsNullOrEmpty(model.FullName))
            {
                sbMessage.AppendLine("FullName can't be empty");
            }

            if (model.CompanyId == Guid.Empty)
            {
                sbMessage.AppendLine("Company must be select");
            }

            if (model.DepartmentId == Guid.Empty)
            {
                sbMessage.AppendLine("Department must be select");
            }


            if (sbMessage.Length > 0)
            {
                response.SetError(sbMessage.ToString());
            }

            return response;
        }

        public static Data.Model.ResponseModel LoginActiveDirectory(Data.Model.UserLoginModel model, bool IsSearch = false)
        {
            ResponseModel response = new ResponseModel(false);
            var username = model.Username.Split('\\');

            var user = new Data.Model.UserModel();
            DirectoryEntry _entry;

            if (IsSearch)
            {
                _entry = new DirectoryEntry(string.Format("LDAP://{0}", username[0]));
            }
            else
            {
                _entry = new DirectoryEntry(string.Format("LDAP://{0}", username[0]), username[1], model.Password);
            }


            DirectorySearcher _searcher = new DirectorySearcher(_entry);
            _searcher.Filter = string.Format("(samAccountName={0})", username[1]);

            try
            {
                SearchResult _sr = _searcher.FindOne();
                if (_sr != null)
                {
                    //string jsonUser = Newtonsoft.Json.JsonConvert.SerializeObject(_sr);
                    user.EmployeeId = _sr.Properties["employeeid"][0].ToString();
                    user.NIKSite = _sr.Properties["employeeid"][0].ToString();

                    var propertyCompany = _sr.Properties["company"];
                    if (propertyCompany != null && propertyCompany.Count > 0)
                    {
                        if (propertyCompany[0] != null && !string.IsNullOrEmpty(propertyCompany[0].ToString()))
                        {
                            user.CompanyName = propertyCompany[0].ToString();
                        }
                    }

                    user.FullName = _sr.Properties["name"][0].ToString();
                    user.Username = _sr.Properties["samaccountname"][0].ToString();
                    user.Email = _sr.Properties["mail"][0].ToString();
                    user.CompanyCode = username[0];

                    //InsertFromActiveDirectory(user);
                    response.SetSuccess();
                    response.ResponseObject = user;
                }
            }
            catch (Exception ex)
            {
                //Logger.Error(LogCategory.SettingUser, "User Detail", model, ex);
                response.SetError(ex.Message);
            }

            return response;
        }

        public static UserModel GetByUserId(Guid Id)
        {
            var userLogin = UserLogin.GetById(Id);
            return MappingFromUserLogin(userLogin);
        }

        public static UserModel GetByUsername(string username)
        {
            var userLogin = UserLogin.GetByUsername(username);
            return MappingFromUserLogin(userLogin);
        }

        public static UserModel GetByNIK(string nik)
        {
            var userLogin = CurrentDataContext.CurrentContext.UserLogins.FirstOrDefault(x => x.NIKSite == nik);
            return MappingFromUserLogin(userLogin);
        }

        public static UserModel MappingFromUserLogin(UserLogin userLogin)
        {
            var model = new UserModel();
            if (userLogin != null)
            {
                model.InjectFrom(userLogin);

                if (userLogin.Department != null && userLogin.Department.Company != null)
                {
                    model.CompanyId = userLogin.Department.CompanyId;
                    model.CompanyCode = userLogin.Department.Company.CompanyCode;
                    model.CompanyName = userLogin.Department.Company.CompanyName;

                    model.DepartmentId = userLogin.DepartmentId;
                    model.DepartmentName = userLogin.Department.DepartmentName;
                }

                return model;
            }

            return null;
        }

        public static List<UserModel> GetUserAllSite(string username)
        {
            var users = new List<UserModel>();

            var user = UserLogic.GetByUsername(username);
            users.Add(user);

            return users;
        }

        public static string GetRoleAccessByRoleId(Guid UserId, Guid RoleId)
        {
            var RoleAccess = new List<string>();

            var RoleAccesUsers = RoleAccessUser.GetByUserId(UserId);
            foreach (var item in RoleAccesUsers.ToList())
            {
                RoleAccess.Add(item.RoleAccess.RoleAccessCode);
            }

            return string.Join("#", RoleAccess);
        }

        public static Boolean IsAdministrator(string username, string companyCode)
        {
            var user = UserLogin.GetByUsername(username);
            RoleAccessUser roleAccessAdmin = RoleAccessUser.GetByUserId(user.ID).FirstOrDefault(x => x.Company.CompanyCode == companyCode && x.RoleAccess.RoleAccessCode == ROLE_CODE.ADMINISTRATOR);
            if (roleAccessAdmin != null)
            {
                return true;
            }

            return false;
        }

        public static Boolean IsAdministrator(Guid userId, string companyCode)
        {
            RoleAccessUser roleAccessAdmin = RoleAccessUser.GetByUserId(userId)
                .FirstOrDefault(x => x.Company.CompanyCode == companyCode && x.RoleAccess.RoleAccessCode == ROLE_CODE.ADMINISTRATOR);

            if (roleAccessAdmin != null)
            {
                return true;
            }

            return false;
        }

        public static UserModel GetFullDataByNik(string nikSite)
        {
            var model = GetByNIK(nikSite);
            if (model != null)
            {
                var roleAccessUsers = RoleAccessUser.GetByUserId(model.ID);
                if (roleAccessUsers != null)
                {
                    foreach (var item in roleAccessUsers)
                    {
                        RoleAccessUserModel roleAccess = new RoleAccessUserModel();
                        roleAccess.ID = item.ID;
                        roleAccess.RoleAccessID = item.RoleAccessId;
                        roleAccess.RoleAccessCode = item.RoleAccess.RoleAccessCode;
                        roleAccess.RoleAccessName = item.RoleAccess.RoleAccessName;
                        roleAccess.CompanyId = item.CompanyId;
                        roleAccess.CompanyName = item.Company.CompanyName;
                        roleAccess.DepartmentId = item.DepartmentId;
                        roleAccess.DepartmentName = item.Department.DepartmentName;
                        model.RoleAccessList.Add(roleAccess);
                    }
                }
            }

            return model;
        }

        public static UserModel GetUserWithRoleAccessById(Guid UserId)
        {
            UserModel user = GetByUserId(UserId);
            user.ListRoleDepartment = new List<string>();

            if (user != null)
            {
                user.RoleAccessList = new List<RoleAccessUserModel>();
                user.RoleAccessListForView = new List<RoleAccessUserModel>();

                var roleAccessList = RoleAccessUser.GetByUserId(user.ID);
                if (roleAccessList != null)
                {
                    foreach (var item in roleAccessList.OrderBy(x => x.CreatedDate))
                    {
                        RoleAccessUserModel roleAccessUser = new RoleAccessUserModel();
                        roleAccessUser.ID = item.ID;
                        roleAccessUser.RoleAccessID = item.RoleAccessId;
                        roleAccessUser.RoleAccessCode = item.RoleAccess.RoleAccessCode;
                        roleAccessUser.RoleAccessName = item.RoleAccess.RoleAccessName;
                        roleAccessUser.IsDefault = item.IsDefault;
                        roleAccessUser.CompanyId = item.CompanyId;
                        roleAccessUser.CompanyName = item.Company.CompanyName;
                        roleAccessUser.DepartmentId = item.DepartmentId;
                        roleAccessUser.DepartmentName = item.Department.DepartmentName;
                        roleAccessUser.IsHaveAccess = true;

                        user.ListRoleDepartment.Add(string.Format("{0}|{1}", item.RoleAccessId, item.DepartmentId));
                        user.RoleAccessList.Add(roleAccessUser);
                    }

                    var roleGroupByRole = roleAccessList.GroupBy(x => x.RoleAccessId);
                    foreach (var itemByRoles in roleGroupByRole)
                    {
                        var itemByRole = itemByRoles.FirstOrDefault();
                        RoleAccessUserModel roleAccessUser = new RoleAccessUserModel();
                        roleAccessUser.ID = itemByRole.ID;
                        roleAccessUser.RoleAccessID = itemByRole.RoleAccessId;
                        roleAccessUser.RoleAccessCode = itemByRole.RoleAccess.RoleAccessCode;
                        roleAccessUser.RoleAccessName = itemByRole.RoleAccess.RoleAccessName;
 
                        roleAccessUser.IsHaveAccess = true;
                        user.RoleAccessListForView.Add(roleAccessUser);
                    }
                }
            }

            return user;
        }

        public static UserSearchModel GetListUser(UserSearchModel search)
        {
            List<UserModel> list = new List<UserModel>();
            IQueryable<UserLogin> userList = UserLogin.GetAll();

            if (!string.IsNullOrEmpty(search.Name))
            {
                userList = userList.Where(x => x.FullName.Contains(search.Name));
            }

            if (!string.IsNullOrEmpty(search.NIKSITE))
            {
                userList = userList.Where(x => x.NIKSite.Contains(search.NIKSITE));
            }

            if (search.DepartmentId != Guid.Empty)
            {
                userList = userList.Where(x => x.DepartmentId == search.DepartmentId);
            }
            else
            {
                if (search.CompanyId != Guid.Empty)
                {
                    var departments = Department.GetByCompanyId(search.CompanyId);
                    List<Guid> departmentListId = departments.Select(x => x.ID).ToList();
                    userList = userList.Where(x => departmentListId.Contains(x.DepartmentId));
                }
            }

            if (search.RoleAccessId != Guid.Empty)
            {
                IQueryable<RoleAccessUser> roleAccessUsers = RoleAccessUser.GetByRoleAccessId(search.RoleAccessId);

                if (search.DepartmentId != Guid.Empty)
                {
                    roleAccessUsers = roleAccessUsers.Where(x => x.DepartmentId == search.DepartmentId);
                }
                else
                {
                    if (search.CompanyId != Guid.Empty)
                    {
                        var departments = Department.GetByCompanyId(search.CompanyId);
                        List<Guid> departmentListId = departments.Select(x => x.ID).ToList();
                        roleAccessUsers = roleAccessUsers.Where(x => departmentListId.Contains(x.DepartmentId));
                    }
                }


                userList = (from user in userList
                            join roleAccessUser in roleAccessUsers on user.ID equals roleAccessUser.UserId
                            select user);
            }

            search.SetPager(userList.Count());
            List<UserLogin> listUser = userList.OrderBy(x => x.UpdatedDate)
                .Skip((search.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
                .Take(Common.SiteSettings.PAGE_SIZE_PAGING).ToList();

            foreach (var item in listUser)
            {
                UserModel itemUser = new UserModel();
                itemUser.ID = item.ID;
                itemUser.FullName = item.FullName;
                itemUser.Username = item.Username;
                itemUser.NIKSite = item.NIKSite;
                itemUser.CompanyId = item.Department.CompanyId;
                itemUser.CompanyName = item.Department.Company.CompanyName;
                itemUser.DepartmentId = item.DepartmentId;
                itemUser.DepartmentName = item.Department.DepartmentName;

                list.Add(itemUser);
            }

            search.ListData = list;

            return search;
        }

        public static UserModel InsertNewUser(String NIKSite)
        {
            CurrentDataContext.CurrentContext.InsertNewUser(NIKSite);
            return GetByNIK(NIKSite);
        }

    }
}

