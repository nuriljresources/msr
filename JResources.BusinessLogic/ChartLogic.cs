﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using JResources.Common.Enum;
using JResources.Data;
using JResources.Data.Model.Chart;

namespace JResources.BusinessLogic
{
    public class ChartLogic
    {
        public static List<ChartBaseModel> GetChartHome(Guid _companyId)
        {
            List<ChartBaseModel> list = new List<ChartBaseModel>();

            decimal TotalRequest = 0;
            decimal QtyPending = 0;
            decimal QtyTransfer = 0;
            decimal QtyIssued = 0;
            decimal QtyCancel = 0;
            decimal QtyPurchase = 0;

            var dataStart = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            dataStart = Common.DateHelper.DateFromMinimun(dataStart);
            var dataEnd = dataStart.AddMonths(1).AddDays(-1);
            dataEnd = Common.DateHelper.DateToMax(dataEnd);


            var MSRs = MSR.GetAll().Where(x => x.CompanyID == _companyId);
            MSRs = MSRs.Where(x => x.CreatedDate >= dataStart && x.CreatedDate <= dataEnd).Take(10);

            foreach (var msr in MSRs)
            {
                TotalRequest += msr.MSRDetails.Sum(x => x.Qty);
                foreach (var msrIssued in msr.MSRIssueds)
                {
                    switch (msrIssued.IssuedType)
                    {
                        case RequestStatus.TRANSFER:
                            QtyTransfer += msrIssued.MSRIssuedDetails.Sum(x => x.Qty);
                            break;

                        case RequestStatus.ISSUED:
                            QtyIssued += msrIssued.MSRIssuedDetails.Sum(x => x.Qty);
                            break;

                        case RequestStatus.CANCEL :
                            QtyCancel += msrIssued.MSRIssuedDetails.Sum(x => x.Qty);
                            break;

                        case RequestStatus.PURCHASING:
                            QtyPurchase += msrIssued.MSRIssuedDetails.Sum(x => x.Qty);
                            break;
                    }
                }
            }

            QtyPending = TotalRequest - (QtyIssued + QtyPurchase + QtyCancel);
            list.AddRange(
                new List<ChartBaseModel>(){
                    new ChartBaseModel() {
                        Label = "Pending",
                        Value = QtyPending.ToString(),
                        Color = "#EEEEEE"
                    },

                    new ChartBaseModel() {
                        Label = "Transfer",
                        Value = QtyTransfer.ToString(),
                        Color = "#22313F"
                    },

                    new ChartBaseModel() {
                        Label = "Issued",
                        Value = QtyIssued.ToString(),
                        Color = "#67B7DC"
                    },

                    new ChartBaseModel() {
                        Label = "Cancel",
                        Value = QtyCancel.ToString(),
                        Color = "#CC4748"
                    },

                    new ChartBaseModel() {
                        Label = "Purchase",
                        Value = QtyPurchase.ToString(),
                        Color = "#FDD400"
                    },
                }
            );

            return list;
        }


    }
}
