﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using JResources.Common;
using JResources.Common.Enum;
using JResources.Data;
using JResources.Data.Model;

namespace JResources.BusinessLogic
{
    public class ItemLogic
    {
        public static PartItemSearchModel GetItemList(PartItemSearchModel search)
        {
            List<ItemPartModel> list = new List<ItemPartModel>();
            IQueryable<ICITEM> icITEM = AccpacDataContext.CurrentContext.ICITEMs.Where(x => x.INACTIVE == 0);

            if (search.IsInventory)
            {
                icITEM = icITEM.Where(x => x.STOCKITEM == 1);
            }
            else
            {
                icITEM = icITEM.Where(x => x.STOCKITEM == 0);
            }

            if (!string.IsNullOrEmpty(search.ItemNo))
            {
                icITEM = icITEM.Where(x => x.ITEMNO.Contains(search.ItemNo));
            }

            if (search.ListItemNo != null && search.ListItemNo.Count > 0)
            {
                icITEM = icITEM.Where(x => search.ListItemNo.Contains(x.ITEMNO));
            }

            if (!string.IsNullOrEmpty(search.ItemDesc))
            {
                icITEM = icITEM.Where(x => x.DESC.Contains(search.ItemDesc));
            }

            if (search.IsReOrderPoint)
            {
                var iCITEMOS = ICITEMO.GetITEMCIFD();
                icITEM = from icITEMs in icITEM
                         join iCITEMO in iCITEMOS on icITEMs.ITEMNO equals iCITEMO.ITEMNO
                         select icITEMs;
            }

            search.SetPager(icITEM.Count());
            List<ICITEM> listItem = new List<ICITEM>();
            if (!search.IsGetAllItem)
            {
                listItem = icITEM.OrderBy(x => x.ITEMNO).Skip((search.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
                .Take(Common.SiteSettings.PAGE_SIZE_PAGING).ToList();
            }
            else
            {
                listItem = icITEM.ToList();
            }

            List<string> ItemNoList = new List<string>();
            if (listItem != null & listItem.Count > 0)
            {
                ItemNoList = listItem.Select(x => x.ITEMNO).ToList();
            }

            List<string> CategoryList = new List<string>();
            if (listItem != null & listItem.Count > 0)
            {
                CategoryList = listItem.Select(x => x.CATEGORY).ToList();
            }

            List<ICCATG> listICCATG = AccpacDataContext.CurrentContext.ICCATGs.Where(x => CategoryList.Contains(x.CATEGORY)).ToList();

            foreach (var item in listItem)
            {
                ItemPartModel itemPart = new ItemPartModel();
                itemPart.ItemNo = item.ITEMNO;
                itemPart.Description = item.DESC;
                itemPart.Category = item.CATEGORY;
                itemPart.CategoryLabel = item.CATEGORY;
                itemPart.UOM = item.STOCKUNIT;

                var icCATG = listICCATG.FirstOrDefault(x => x.CATEGORY.Trim() == item.CATEGORY.Trim());
                if (icCATG != null)
                {
                    itemPart.CategoryLabel = string.Format("{0}-{1}", icCATG.CATEGORY.Trim(), icCATG.DESC.Trim());
                }

                list.Add(itemPart);
            }

            SetLocationItem(list);
            search.ListData = list;

            return search;
        }

        public static void SetQtyByLocation(List<MSRDetailModel> list)
        {
            if (list != null && list.Count > 0)
            {
                List<string> listItemNo = list.Select(x => x.ItemNo).ToList();
                List<string> listLocation = list.Select(x => x.Location).ToList();

                IQueryable<ICILOC> icILOC = AccpacDataContext.CurrentContext.ICILOCs;
                List<ICILOC> LOCATIONS = icILOC.Where(x => listItemNo.Contains(x.ITEMNO)).ToList();

                foreach (var item in list)
                {
                    item.ItemNo = item.ItemNo.Trim();
                    var locationsPerTime = LOCATIONS.Where(x => x.ITEMNO.Trim() == item.ItemNo.Trim()).ToList();
                    if (locationsPerTime != null && locationsPerTime.Count > 0)
                    {
                        item.ItemLocations = new List<ItemLocationModel>();
                        foreach (var itemLocation in locationsPerTime)
                        {
                            item.ItemLocations.Add(new ItemLocationModel()
                            {
                                Location = itemLocation.LOCATION,
                                LastCost = itemLocation.LASTCOST,
                                AuditDateDecimal = itemLocation.AUDTDATE,
                                QtyOnHand = itemLocation.QTYONHAND
                            });
                        }

                        item.StockOnHand = locationsPerTime.Sum(x => x.QTYONHAND);
                        item.Cost = locationsPerTime.Max(x => x.LASTCOST);
                    }
                }
            }
        }

        public static void SetLocationItem(List<ItemPartModel> list)
        {
            if (list != null && list.Count > 0)
            {
                List<string> itemnoList = list.Select(x => x.ItemNo).ToList();
                var listLocation = AccpacDataContext.CurrentContext.ICILOCs.Where(x => itemnoList.Contains(x.ITEMNO)
                    && !x.LOCATION.Contains(Common.Enum.SlocType.RSVMNT) && !x.LOCATION.Contains(Common.Enum.SlocType.RSVPLT)).ToList();

                foreach (var item in list)
                {
                    List<ICILOC> listLocationItem = listLocation.Where(x => x.ITEMNO == item.ItemNo).ToList();
                    if (listLocationItem != null && listLocationItem.Count > 0)
                    {
                        foreach (var locationItem in listLocationItem)
                        {
                            ItemLocationModel ItemLocation = new ItemLocationModel();
                            ItemLocation.Location = locationItem.LOCATION.Trim();
                            ItemLocation.QtyOnHand = locationItem.QTYONHAND;
                            ItemLocation.QtyOnOrder = locationItem.QTYONORDER;
                            ItemLocation.LastCost = locationItem.LASTCOST;
                            ItemLocation.AuditDateDecimal = locationItem.AUDTDATE;

                            item.ItemLocations.Add(ItemLocation);
                        }

                        //item.TotalQtyOnHand = listLocationItem.Sum(x => x.QTYONHAND);
                    }
                    else
                    {
                        ICITEMO icITEMOptional = ICITEMO.GetByItemNo(item.ItemNo, "NONSTKPR");
                        if (icITEMOptional != null)
                        {
                            decimal totalCost = 0;
                            if (decimal.TryParse(icITEMOptional.VALUE, out totalCost))
                            {
                                item.TotalQtyOnHand = 1;
                                item.TotalCost = totalCost;
                            }
                        }
                    }
                }


            }

        }

        public static void SetLocationItem(List<MSRIssuedDetailModel> list, bool IsWorkOrder = false)
        {
            if (list != null && list.Count > 0)
            {
                List<string> itemnoList = list.Select(x => x.ItemNo).ToList();
                var listItems = AccpacDataContext.CurrentContext.ICITEMs.Where(x => itemnoList.Contains(x.ITEMNO)).ToList();
                var listLocation = AccpacDataContext.CurrentContext.ICILOCs.Where(x => itemnoList.Contains(x.ITEMNO)).ToList();

                List<string> listCNTLACCT = listItems.Select(x => x.CNTLACCT).ToList();
                var listAccounts = AccpacDataContext.CurrentContext.ICACCTs.Where(x => listCNTLACCT.Contains(x.CNTLACCT)).ToList();

                List<string> CategoryList = new List<string>();
                if (listItems != null & listItems.Count > 0)
                {
                    CategoryList = listItems.Select(x => x.CATEGORY).ToList();
                }

                List<ICCATG> listICCATG = AccpacDataContext.CurrentContext.ICCATGs.Where(x => CategoryList.Contains(x.CATEGORY)).ToList();


                if (IsWorkOrder)
                {
                    listLocation = listLocation.Where(x => x.LOCATION.Trim() == Common.Enum.SlocType.RSVMNT || x.LOCATION.Trim() == Common.Enum.SlocType.RSVPLT).ToList();
                }
                else
                {
                    listLocation = listLocation.Where(x => !x.LOCATION.Contains(Common.Enum.SlocType.RSVMNT)
                        && !x.LOCATION.Contains(Common.Enum.SlocType.RSVPLT)).ToList();
                }

                foreach (var item in list)
                {
                    if (!string.IsNullOrEmpty(item.Category))
                    {
                        var icCATG = listICCATG.FirstOrDefault(x => x.CATEGORY.Trim() == item.Category.Trim());
                        if (icCATG != null)
                        {
                            item.CategoryLabel = string.Format("{0}-{1}", icCATG.CATEGORY.Trim(), icCATG.DESC.Trim());
                        }

                        var ItemPart = listItems.FirstOrDefault(x => x.ITEMNO.Trim() == item.ItemNo.Trim());
                        if (ItemPart != null)
                        {
                            item.Category = ItemPart.CATEGORY.Trim();
                            if (!string.IsNullOrEmpty(item.Category))
                            {
                                var itemCategory = listAccounts.FirstOrDefault(x => x.CNTLACCT.Trim() == ItemPart.CNTLACCT.Trim());
                                if (itemCategory != null)
                                {
                                    item.GLAccount = itemCategory.INVACCT;
                                }
                            }
                        }
                    }

                    List<ICILOC> listLocationItem = listLocation.Where(x => x.ITEMNO.Trim() == item.ItemNo.Trim()).ToList();
                    if (listLocationItem != null && listLocationItem.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(item.Location))
                        {
                            var locationSelect = listLocationItem.FirstOrDefault(x => x.LOCATION.Trim() == item.Location.Trim());
                            if (locationSelect != null)
                            {
                                item.PickingSeq = locationSelect.PICKINGSEQ.Trim();
                            }
                        }

                        foreach (var locationItem in listLocationItem)
                        {
                            ItemLocationModel ItemLocation = new ItemLocationModel();
                            ItemLocation.Location = locationItem.LOCATION;
                            ItemLocation.QtyOnHand = locationItem.QTYONHAND;
                            ItemLocation.LastCost = locationItem.LASTCOST;
                            ItemLocation.AuditDateDecimal = locationItem.AUDTDATE;

                            item.ItemLocations.Add(ItemLocation);
                        }
                    }
                }
            }

        }

        public static ItemPartModel GetByItemNo(string ItemNo)
        {
            ItemPartModel itemPart = new ItemPartModel();

            var ICITEM = AccpacDataContext.CurrentContext.ICITEMs.FirstOrDefault(x => x.ITEMNO == ItemNo);
            if (ICITEM != null)
            {
                itemPart.ItemNo = ICITEM.ITEMNO;
                itemPart.Description = ICITEM.DESC;
                itemPart.Category = ICITEM.CATEGORY;
                itemPart.CategoryLabel = ICITEM.CATEGORY;
                itemPart.UOM = ICITEM.STOCKUNIT;

                var itemParts = new List<ItemPartModel>();
                itemParts.Add(itemPart);
                ItemLogic.SetLocationItem(itemParts);
                itemPart = itemParts.FirstOrDefault();
            }
            return itemPart;
        }
    }
}
