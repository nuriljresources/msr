﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACCPAC.Advantage;
using JResources.Data.Model;
using JResources.Data.Model.Accpac;

namespace JResources.BusinessLogic.AccpacConnect
{
    public class Shipment
    {
        public static ResponseModel Save(IC0640 ic0640)
        {
            StringBuilder sbMessage = new StringBuilder();
            ResponseModel responseModel = new ResponseModel(false);
            string lineItemNo = string.Empty;
            Session session = new Session();

            try
            {
                //sbMessage.AppendLine("Start Connect to Accpac");
                session.Init("", "XY", "XY1000", "61A");
                session.Open(ic0640.DBUsername, ic0640.DBPassword, ic0640.DBName, DateTime.Today, 0);
                DBLink link = session.OpenDBLink(DBLinkType.Company, DBLinkFlags.ReadWrite);
                //session.OpenDBLink(DBLinkType.System, DBLinkFlags.ReadWrite);

                View IC0640View = link.OpenView("IC0640");
                View IC0630View = link.OpenView("IC0630");
                View IC0645View = link.OpenView("IC0645");
                View IC0635View = link.OpenView("IC0635");
                View IC0632View = link.OpenView("IC0632");
                View IC0636View = link.OpenView("IC0636");

                //sbMessage.AppendLine("Open Shipment");
                View[] views = new View[3];
                views[0] = IC0630View;
                views[2] = IC0645View;
                IC0640View.Compose(views);

                View[] viewArray2 = new View[10];
                viewArray2[0] = IC0640View;
                viewArray2[7] = IC0635View;
                viewArray2[8] = IC0632View;
                viewArray2[9] = IC0636View;
                IC0630View.Compose(viewArray2);
                IC0645View.Compose(new View[] { IC0640View });
                IC0635View.Compose(new View[] { IC0630View });
                IC0632View.Compose(new View[] { IC0630View });
                IC0640View.Order = 3;
                IC0640View.FilterSelect("(DELETED = 0 )", true, 3, (ViewFilterOrigin)0);
                IC0640View.Order = 3;
                IC0640View.Order = 0;
                IC0640View.Fields.FieldByName("SEQUENCENO").SetValue("0", true);
                IC0640View.Init();
                bool exists = IC0630View.Exists;
                IC0630View.RecordClear();
                IC0640View.Order = 3;
                if (!string.IsNullOrEmpty(ic0640.DOCNUM))
                {
                    IC0640View.Fields.FieldByName("DOCNUM").SetValue(ic0640.DOCNUM, true);
                }
                bool flag2 = IC0640View.Exists;
                IC0640View.Fields.FieldByName("PROCESSCMD").SetValue("0", false);
                IC0640View.Fields.FieldByName("CURRENCY").SetValue(ic0640.CURRENCY, true);
                IC0640View.Process();
                bool flag3 = IC0630View.Exists;
                IC0630View.RecordClear();

                int iRow = 0;
                foreach (var ic0630 in ic0640.IC0630List)
                {
                    //sbMessage.AppendLine(string.Format("Line {0} - Item No {1}", iRow, ic0630.ITEMNO));

                    lineItemNo = ic0630.ITEMNO;
                    bool flag4 = IC0630View.Exists;
                    IC0630View.RecordCreate(ViewRecordCreate.NoInsert);
                    IC0630View.Fields.FieldByName("ITEMNO").SetValue(ic0630.ITEMNO, true);
                    IC0630View.Fields.FieldByName("PROCESSCMD").SetValue("1", false);
                    IC0630View.Process();
                    if (!string.IsNullOrEmpty(ic0630.CATEGORY))
                    {
                        IC0630View.Fields.FieldByName("CATEGORY").SetValue(ic0630.CATEGORY, true);
                    }

                    IC0630View.Fields.FieldByName("LOCATION").SetValue(ic0630.LOCATION, true);
                    IC0630View.Fields.FieldByName("QUANTITY").SetValue(ic0630.QUANTITY, true);
                    IC0630View.Fields.FieldByName("UNIT").SetValue(ic0630.UNIT, true);

                    foreach (var ic0635 in ic0630.IC0635List)
                    {
                        IC0635View.Fields.FieldByName("OPTFIELD").SetValue(ic0635.OPTFIELD, true);
                        IC0635View.Read(false);
                        IC0635View.Fields.FieldByName("SWSET").SetValue(ic0635.SWSET, true);
                        bool flag5 = IC0635View.Exists;
                        IC0635View.Fields.FieldByName("VALIFTEXT").SetValue(ic0635.VALIFTEXT, false);
                        IC0635View.Update();
                    }

                    IC0630View.Fields.FieldByName("COMMENTS").SetValue(ic0630.COMMENTS, true);
                    IC0630View.Fields.FieldByName("MANITEMNO").SetValue(ic0630.MANITEMNO, true);
                    IC0630View.Fields.FieldByName("FUNCTION").SetValue(ic0630.FUNCTION, true);
                    IC0630View.Process();
                    IC0630View.Insert();
                    IC0630View.Fields.FieldByName("LINENO").SetValue("-" + iRow, false);
                    IC0630View.Read(false);

                    iRow++;
                }

                IC0645View.RecordClear();
                IC0645View.RecordCreate(ViewRecordCreate.NoInsert);
                IC0645View.Fields.FieldByName("OPTFIELD").SetValue(ic0640.OPTFIELD, true);
                IC0645View.Fields.FieldByName("SWSET").SetValue(ic0640.SWSET, true);
                IC0645View.Fields.FieldByName("VALIFTEXT").SetValue(ic0640.VALIFTEXT, true);
                IC0645View.Insert();
                IC0645View.Fields.FieldByName("OPTFIELD").SetValue(ic0640.OPTFIELD, false);
                IC0645View.Read(false);
                IC0640View.Fields.FieldByName("TRANSDATE").SetValue(ic0640.TRANSDATE, true);
                IC0640View.Fields.FieldByName("DATEBUS").SetValue(ic0640.DATEBUS, true);
                IC0640View.Fields.FieldByName("HDRDESC").SetValue(ic0640.HDRDESC, false);
                IC0640View.Fields.FieldByName("REFERENCE").SetValue(ic0640.REFERENCE, false);
                IC0640View.Fields.FieldByName("CUSTNO").SetValue(ic0640.CUSTNO, true);
                IC0640View.Fields.FieldByName("CONTACT").SetValue(ic0640.CONTACT, false);
                IC0640View.Fields.FieldByName("STATUS").SetValue("2", false);
                IC0640View.Insert();
                IC0640View.Order = 0;
                IC0640View.Fields.FieldByName("SEQUENCENO").SetValue("0", false);
                IC0640View.Init();
                bool flag8 = IC0630View.Exists;
                IC0630View.RecordClear();
                IC0640View.Order = 3;
                session.Dispose();

                sbMessage.AppendLine("Success Shipment");
                responseModel.SetSuccess(sbMessage.ToString());
            }
            catch (Exception exception)
            {
                if (session != null && session.Errors != null)
                {

                    int TotalError = session.Errors.Count;
                    for (int i = 0; i < TotalError; i++)
                    {
                        if (session.Errors[i] != null && !string.IsNullOrEmpty(session.Errors[i].Message))
                        {
                            if (!string.IsNullOrEmpty(lineItemNo))
                            {
                                lineItemNo = lineItemNo + "-";
                            }

                            sbMessage.AppendLine(lineItemNo + session.Errors[i].Message);
                        }
                    }
                }

                sbMessage.Append(exception.Message);
                responseModel.Message = sbMessage.ToString();
            }


            if (session != null)
            {
                session.Dispose();
            }

            if (responseModel.Message.Contains("Record already exists"))
            {
                responseModel.SetSuccess();
            }

            return responseModel;
        }
    }
}
