﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using ACCPAC.Advantage;
using JResources.Data.Model;
using JResources.Data.Model.Accpac;
using Newtonsoft.Json;

namespace JResources.BusinessLogic.AccpacConnect
{
    public class Purchasing
    {
        public static ResponseModel SaveRequisition(PT0040 pt0040)
        {
            StringBuilder sbMessage = new StringBuilder();

            ResponseModel responseModel = new ResponseModel(false);
            Session session = new Session();
            string lineItemNo = string.Empty;
            string lastItemNo = string.Empty;

            try
            {
                session.Init("", "XY", "XY1000", "61A");
                session.Open(pt0040.DBUsername, pt0040.DBPassword, pt0040.DBName, DateTime.Today, 0);
                DBLink link = session.OpenDBLink(DBLinkType.Company, DBLinkFlags.ReadWrite);
                session.OpenDBLink(DBLinkType.System, DBLinkFlags.ReadWrite);

                View view = link.OpenView("PT0040");
                ViewFields fields = view.Fields;
                View view2 = link.OpenView("PT0041");
                ViewFields fields2 = view2.Fields;
                View view3 = link.OpenView("PT0816");
                ViewFields fields3 = view3.Fields;
                View view4 = link.OpenView("PT0042");
                ViewFields fields4 = view4.Fields;
                View view5 = link.OpenView("PT0818");
                ViewFields fields5 = view5.Fields;
                View[] views = new View[3];
                views[0] = view2;
                views[1] = view3;
                
                view.Compose(views);
                view2.Compose(new View[] { view, view4, view5 });
                view3.Compose(new View[] { view });
                view4.Compose(new View[] { view2 });
                view5.Compose(new View[] { view2 });
                view2.RecordCreate(ViewRecordCreate.NoInsert);
                View view6 = link.OpenView("PT0045");
                ViewFields fields6 = view6.Fields;
                View view7 = link.OpenView("PT0046");
                ViewFields fields7 = view7.Fields;
                view6.Compose(new View[] { view7 });
                view7.Compose(new View[] { view6 });
                View view8 = link.OpenView("PT0035");
                ViewFields fields8 = view8.Fields;
                view8.Order = 1;
                bool exists = view.Exists;
                view.Init();
                exists = view.Exists;
                view.Fields.FieldByName("PROCESSCMD").SetValue("1", true);
                view.Process();
                exists = view.Exists;
                exists = view.Exists;
                view.Order = 0;
                view.Fields.FieldByName("RQNHSEQ").SetValue("0", true);
                exists = view.Exists;
                exists = view.Exists;
                view.Init();
                exists = view.Exists;
                exists = view.Exists;
                view.Fields.FieldByName("PROCESSCMD").SetValue("1", true);
                view.Process();
                if (!string.IsNullOrEmpty(pt0040.DOCNUM))
                {
                    view.Fields.FieldByName("RQNNUMBER").SetValue(pt0040.DOCNUM, true);
                    view.Order = 1;
                    exists = view.Exists;
                }
                view.Fields.FieldByName("WORKFLOW").SetValue(pt0040.WORKFLOW, true);
                view.Fields.FieldByName("COSTCTR").SetValue(pt0040.COSTCTR, true);
                view.Fields.FieldByName("STCODE").SetValue(pt0040.STCODE, true);
                view.Fields.FieldByName("BTCODE").SetValue(pt0040.BTCODE, true);
                view.Fields.FieldByName("RQRDDATE").SetValue(pt0040.RQRDDATE, true);
                view.Fields.FieldByName("DESCRIPTIO").SetValue(pt0040.DESCRIPTIO, true);
                view.Fields.FieldByName("REFERENCE").SetValue(pt0040.REFERENCE, true);
                view.Fields.FieldByName("COMMENT").SetValue(pt0040.COMMENT, true);
                view.Fields.FieldByName("HASJOB").SetValue(pt0040.HASJOB, true);
                exists = view.Exists;
                exists = view.Exists;
                exists = view.Exists;
                view2.RecordClear();
                //sbMessage.AppendLine("Header Insert RQN");
                int num = 1;
                foreach (var pt0041 in pt0040.PT0041List.OrderBy(x => x.LineNo))
                {
                    lineItemNo = pt0041.FMTITEMNO;
                    exists = view2.Exists;
                    view2.RecordCreate(ViewRecordCreate.NoInsert);
                    if (pt0040.HASJOB == 1)
                    {
                        view2.Fields.FieldByName("CONTRACT").SetValue(pt0041.CONTRACT, true);
                        view2.Fields.FieldByName("PROJECT").SetValue(pt0041.PROJECT, true);
                        view2.Fields.FieldByName("CCATEGORY").SetValue(pt0041.CCATEGORY, true);

                        view2.Fields.FieldByName("BILLRATE").SetValue(pt0041.BILLRATE, true);
                        view2.Fields.FieldByName("ARITEMNO").SetValue(pt0041.ARITEMNO, true);
                        view2.Fields.FieldByName("ARUNIT").SetValue(pt0041.ARUNIT, true);
                    }

                    view2.Fields.FieldByName("FMTITEMNO").SetValue(pt0041.FMTITEMNO, true);
                    view2.Fields.FieldByName("PROCESSCMD").SetValue("1", true);
                    view2.Process();

                    if (pt0041.ISVENDOR)
                    {
                        view2.Fields.FieldByName("VDCODE").SetValue(pt0041.VDCODE, true);
                    }

                    view2.Fields.FieldByName("LOCATION").SetValue(pt0041.LOCATION, true);
                    view2.Fields.FieldByName("REQQTY").SetValue(pt0041.REQQTY, true);
                    if (pt0041.ISSTOCK)
                    {
                        sbMessage.AppendLine("GL Account : " + pt0041.GLACCTFULL);
                        view2.Fields.FieldByName("GLACCTFULL").SetValue(pt0041.GLACCTFULL, true);
                    }
                    view2.Fields.FieldByName("RQRDDATE").SetValue(pt0041.RQRDDATE, true);
                    view2.Fields.FieldByName("UNITCOST").SetValue(pt0041.UNITCOST, true);
                    view2.Fields.FieldByName("ORDERUNIT").SetValue(pt0041.ORDERUNIT, true);
                    view2.Fields.FieldByName("VENDITEMNO").SetValue(pt0041.VENDITEMNO, true);
                    view4.Fields.FieldByName("COMMENT").SetValue(pt0041.COMMENT, true);
                    view4.Insert();
                    sbMessage.AppendLine("Insert Detail Item  " + pt0041.FMTITEMNO);
                    foreach (var ic0635 in pt0041.IC0635List)
                    {
                        sbMessage.AppendLine(string.Format("Header Insert Optional {0} - {1}", ic0635.OPTFIELD, ic0635.VALIFTEXT));
                        view5.Fields.FieldByName("OPTFIELD").SetValue(ic0635.OPTFIELD, false);
                        view5.Read(false);
                        view5.Fields.FieldByName("SWSET").SetValue(ic0635.SWSET, true);
                        exists = view5.Exists;
                        view5.Fields.FieldByName("VALIFTEXT").SetValue(ic0635.VALIFTEXT, true);
                        view5.Update();
                    }

                    view5.Read(false);
                    exists = view2.Exists;
                    lastItemNo = "";
                    view2.Insert();
                    view2.Fields.FieldByName("RQNLREV").SetValue("-" + num, false);
                    view2.Read(false);
                    num++;
                }

                //sbMessage.AppendLine("RQN Detail Done");

                //sbMessage.AppendLine(string.Format("OPTION FIELD {0}-{1}-{2}", pt0040.OPTFIELD, pt0040.SWSET, pt0040.VALIFTEXT));
                view3.Fields.FieldByName("OPTFIELD").SetValue(pt0040.OPTFIELD, false);
                view3.Read(false);
                view3.Fields.FieldByName("SWSET").SetValue(pt0040.SWSET, true);
                view3.Fields.FieldByName("VALIFTEXT").SetValue(pt0040.VALIFTEXT, true);
                view3.Update();

                //sbMessage.AppendLine("OPTION FIELD CEA DONE");
                // ADDITIONAL VALUE FOR TERMS
                view3.Fields.FieldByName("OPTFIELD").SetValue("TERMS", false);
                view3.Read(false);
                view3.Fields.FieldByName("SWSET").SetValue("1", true);
                view3.Fields.FieldByName("VALIFTEXT").SetValue("", true);
                view3.Update();

                //sbMessage.AppendLine("Dont additional Option Save");
                view3.Read(false);
                exists = view.Exists;
                view.Insert();
                responseModel.SetSuccess();
                string RQNNo = view.Fields.FieldByName("RQNNUMBER").Value.ToString();
                responseModel.ResponseObject = RQNNo;
                view.Post();
                view.Read(false);
                session.Dispose();
                //sbMessage.AppendLine("RQN Save");
                responseModel.SetSuccess(sbMessage.ToString());

                AccpacPostPurchaseModel AccpacPost = new AccpacPostPurchaseModel();
                AccpacPost.DBName = pt0040.DBName;
                AccpacPost.DBUsername = pt0040.DBUsername;
                AccpacPost.DBPassword = pt0040.DBPassword;
                AccpacPost.DOCNUM = RQNNo;
                AccpacPost.WorkFlow = pt0040.WORKFLOW;
                AccpacPost.Sequence = RQNNo;
                AccpacPost.Comment = (string.IsNullOrEmpty(pt0040.COMMENT) ? "" : pt0040.COMMENT);
                AccpacPost.ApproveStatus = "1";

                responseModel = UpdateRequisition(AccpacPost);

                if (string.IsNullOrEmpty(RQNNo))
                {
                    responseModel.SetError();
                }
            }
            catch (Exception exception)
            {
                sbMessage.Append("EXCEPTION");
                if (session != null && session.Errors != null)
                {

                    int TotalError = session.Errors.Count;
                    for (int i = 0; i < TotalError; i++)
                    {
                        if (session.Errors[i] != null && !string.IsNullOrEmpty(session.Errors[i].Message))
                        {
                            if (!string.IsNullOrEmpty(lineItemNo))
                            {
                                lineItemNo = lineItemNo + "-";
                            }

                            sbMessage.AppendLine(lineItemNo + session.Errors[i].Message);
                        }
                    }
                }

                sbMessage.Append(exception.Message);
                responseModel.Message = sbMessage.ToString();
            }


            if (session != null)
            {
                session.Dispose();
            }

            return responseModel;
        }

        public static ResponseModel CEARequisition(PT0040 pt0040)
        {
            StringBuilder sbMessage = new StringBuilder();

            ResponseModel responseModel = new ResponseModel(false);
            Session session = new Session();
            string lineItemNo = string.Empty;
            string lastItemNo = string.Empty;

            try
            {
                session.Init("", "XY", "XY1000", "61A");
                session.Open(pt0040.DBUsername, pt0040.DBPassword, pt0040.DBName, DateTime.Today, 0);
                DBLink link = session.OpenDBLink(DBLinkType.Company, DBLinkFlags.ReadWrite);
                session.OpenDBLink(DBLinkType.System, DBLinkFlags.ReadWrite);

                View PT0040View = link.OpenView("PT0040");
                View PT0041View = link.OpenView("PT0041");
                View PT0816View = link.OpenView("PT0816");
                View view4 = link.OpenView("PT0042");
                View view5 = link.OpenView("PT0818");
                View[] views = new View[3];
                views[0] = PT0041View;
                views[1] = PT0816View;

                PT0040View.Compose(views);
                PT0041View.Compose(new View[] { PT0040View, view4, view5 });
                PT0816View.Compose(new View[] { PT0040View });
                view4.Compose(new View[] { PT0041View });
                view5.Compose(new View[] { PT0041View });
                PT0041View.RecordCreate(ViewRecordCreate.NoInsert);
                View view6 = link.OpenView("PT0045");
                ViewFields fields6 = view6.Fields;
                View view7 = link.OpenView("PT0046");
                ViewFields fields7 = view7.Fields;
                view6.Compose(new View[] { view7 });
                view7.Compose(new View[] { view6 });
                View view8 = link.OpenView("PT0035");
                ViewFields fields8 = view8.Fields;
                view8.Order = 1;
                bool exists = PT0040View.Exists;
                PT0040View.Init();
                exists = PT0040View.Exists;
                PT0040View.Fields.FieldByName("PROCESSCMD").SetValue("1", true);
                PT0040View.Process();
                exists = PT0040View.Exists;
                exists = PT0040View.Exists;
                PT0040View.Order = 0;
                PT0040View.Fields.FieldByName("RQNHSEQ").SetValue("0", true);
                exists = PT0040View.Exists;
                exists = PT0040View.Exists;
                PT0040View.Init();
                exists = PT0040View.Exists;
                exists = PT0040View.Exists;
                PT0040View.Fields.FieldByName("PROCESSCMD").SetValue("1", true);
                PT0040View.Process();

                if (!string.IsNullOrEmpty(pt0040.DOCNUM))
                {
                    PT0040View.Fields.FieldByName("RQNNUMBER").SetValue(pt0040.DOCNUM, true);
                    PT0040View.Order = 1;
                    exists = PT0040View.Exists;
                }

                PT0040View.Fields.FieldByName("WORKFLOW").SetValue(pt0040.WORKFLOW, true);
                PT0040View.Fields.FieldByName("COSTCTR").SetValue(pt0040.COSTCTR, true);
                PT0040View.Fields.FieldByName("STCODE").SetValue(pt0040.STCODE, true);
                PT0040View.Fields.FieldByName("BTCODE").SetValue(pt0040.BTCODE, true);
                PT0040View.Fields.FieldByName("RQRDDATE").SetValue(pt0040.RQRDDATE, true);
                PT0040View.Fields.FieldByName("DESCRIPTIO").SetValue(pt0040.DESCRIPTIO, true);
                PT0040View.Fields.FieldByName("REFERENCE").SetValue(pt0040.REFERENCE, true);
                PT0040View.Fields.FieldByName("COMMENT").SetValue(pt0040.COMMENT, true);
                PT0040View.Fields.FieldByName("HASJOB").SetValue(pt0040.HASJOB, true);
                exists = PT0040View.Exists;
                exists = PT0040View.Exists;
                exists = PT0040View.Exists;
                PT0041View.RecordClear();
                //sbMessage.AppendLine("Header Insert RQN");
                int num = 1;
                foreach (var pt0041 in pt0040.PT0041List.OrderBy(x => x.LineNo))
                {
                    lineItemNo = pt0041.FMTITEMNO;
                    exists = PT0041View.Exists;
                    PT0041View.RecordCreate(ViewRecordCreate.NoInsert);
                    if (pt0040.HASJOB == 1)
                    {
                        PT0041View.Fields.FieldByName("CONTRACT").SetValue(pt0041.CONTRACT, true);
                        PT0041View.Fields.FieldByName("PROJECT").SetValue(pt0041.PROJECT, true);
                        PT0041View.Fields.FieldByName("CCATEGORY").SetValue(pt0041.CCATEGORY, true);

                        PT0041View.Fields.FieldByName("BILLRATE").SetValue(pt0041.BILLRATE, true);
                        PT0041View.Fields.FieldByName("ARITEMNO").SetValue(pt0041.ARITEMNO, true);
                        PT0041View.Fields.FieldByName("ARUNIT").SetValue(pt0041.ARUNIT, true);
                    }

                    PT0041View.Fields.FieldByName("FMTITEMNO").SetValue(pt0041.FMTITEMNO, true);
                    PT0041View.Fields.FieldByName("PROCESSCMD").SetValue("1", true);
                    PT0041View.Process();

                    if (pt0041.ISVENDOR)
                    {
                        PT0041View.Fields.FieldByName("VDCODE").SetValue(pt0041.VDCODE, true);
                    }

                    PT0041View.Fields.FieldByName("LOCATION").SetValue(pt0041.LOCATION, true);
                    PT0041View.Fields.FieldByName("REQQTY").SetValue(pt0041.REQQTY, true);
                    PT0041View.Fields.FieldByName("GLACCTFULL").SetValue(pt0041.GLACCTFULL, true);
                    PT0041View.Fields.FieldByName("RQRDDATE").SetValue(pt0041.RQRDDATE, true);
                    PT0041View.Fields.FieldByName("UNITCOST").SetValue(pt0041.UNITCOST, true);
                    PT0041View.Fields.FieldByName("ORDERUNIT").SetValue(pt0041.ORDERUNIT, true);
                    PT0041View.Fields.FieldByName("VENDITEMNO").SetValue(pt0041.VENDITEMNO, true);
                    view4.Fields.FieldByName("COMMENT").SetValue(pt0041.COMMENT, true);
                    view4.Insert();
                    sbMessage.AppendLine("Insert Detail Item  " + pt0041.FMTITEMNO);
                    foreach (var ic0635 in pt0041.IC0635List)
                    {
                        sbMessage.AppendLine(string.Format("Header Insert Optional {0} - {1}", ic0635.OPTFIELD, ic0635.VALIFTEXT));
                        view5.Fields.FieldByName("OPTFIELD").SetValue(ic0635.OPTFIELD, false);
                        view5.Read(false);
                        view5.Fields.FieldByName("SWSET").SetValue(ic0635.SWSET, true);
                        exists = view5.Exists;
                        view5.Fields.FieldByName("VALIFTEXT").SetValue(ic0635.VALIFTEXT, true);
                        view5.Update();
                    }

                    view5.Read(false);
                    exists = PT0041View.Exists;
                    lastItemNo = "";
                    
                    PT0041View.Insert();
                    PT0041View.Fields.FieldByName("RQNLREV").SetValue("-" + num, false);
                    PT0041View.Read(false);
                    PT0041View.Update();
                    num++;
                }



                sbMessage.AppendLine("RQN Detail Done");

                sbMessage.AppendLine(string.Format("OPTION FIELD {0}-{1}-{2}", pt0040.OPTFIELD, pt0040.SWSET, pt0040.VALIFTEXT));
                PT0816View.Fields.FieldByName("OPTFIELD").SetValue(pt0040.OPTFIELD, false);
                PT0816View.Read(false);
                PT0816View.Fields.FieldByName("SWSET").SetValue(pt0040.SWSET, true);
                PT0816View.Fields.FieldByName("VALIFTEXT").SetValue(pt0040.VALIFTEXT, true);
                PT0816View.Update();

                sbMessage.AppendLine("OPTION FIELD CEA DONE");
                // ADDITIONAL VALUE FOR TERMS
                //PT0816View.Fields.FieldByName("OPTFIELD").SetValue("TERMS", false);
                //PT0816View.Read(false);
                //PT0816View.Fields.FieldByName("SWSET").SetValue("1", true);
                //PT0816View.Fields.FieldByName("VALIFTEXT").SetValue("", true);
                //PT0816View.Update();

                sbMessage.AppendLine("PT0816View Read");
                PT0816View.Read(false);
                exists = PT0040View.Exists;
                sbMessage.AppendLine("Insert RQN Step");
                PT0040View.Insert();
                sbMessage.AppendLine("Success Insert");
                responseModel.SetSuccess();
                string RQNNo = PT0040View.Fields.FieldByName("RQNNUMBER").Value.ToString();
                responseModel.ResponseObject = RQNNo;
                sbMessage.AppendLine("RQN POST");
                PT0040View.Post();
                sbMessage.AppendLine("RQN POST DONE");
                PT0040View.Read(false);
                session.Dispose();
                
                responseModel.SetSuccess(sbMessage.ToString());

                AccpacPostPurchaseModel AccpacPost = new AccpacPostPurchaseModel();
                AccpacPost.DBName = pt0040.DBName;
                AccpacPost.DBUsername = pt0040.DBUsername;
                AccpacPost.DBPassword = pt0040.DBPassword;
                AccpacPost.DOCNUM = RQNNo;
                AccpacPost.WorkFlow = pt0040.WORKFLOW;
                AccpacPost.Sequence = RQNNo;
                AccpacPost.Comment = (string.IsNullOrEmpty(pt0040.COMMENT) ? "" : pt0040.COMMENT);
                AccpacPost.ApproveStatus = "1";

                responseModel = UpdateRequisition(AccpacPost);

                if (string.IsNullOrEmpty(RQNNo))
                {
                    responseModel.SetError();
                }
            }
            catch (Exception exception)
            {
                sbMessage.Append("EXCEPTION");
                if (session != null && session.Errors != null)
                {

                    int TotalError = session.Errors.Count;
                    for (int i = 0; i < TotalError; i++)
                    {
                        if (session.Errors[i] != null && !string.IsNullOrEmpty(session.Errors[i].Message))
                        {
                            if (!string.IsNullOrEmpty(lineItemNo))
                            {
                                lineItemNo = lineItemNo + "-";
                            }

                            sbMessage.AppendLine(lineItemNo + session.Errors[i].Message);
                        }
                    }
                }

                sbMessage.Append(exception.Message);
                responseModel.Message = sbMessage.ToString();
            }


            if (session != null)
            {
                session.Dispose();
            }

            return responseModel;
        }


        public static ResponseModel UpdateRequisition(AccpacPostPurchaseModel model)
        {
            ResponseModel responseModel = new ResponseModel();
            Session session = new Session();
            session.Init("", "XY", "XY1000", "61A");
            session.Open(model.DBUsername, model.DBPassword, model.DBName, DateTime.Today, 0);

            try
            {

                DBLink link = session.OpenDBLink(DBLinkType.Company, DBLinkFlags.ReadWrite);
                DBLink link2 = session.OpenDBLink(DBLinkType.System, DBLinkFlags.ReadWrite);
                View view = link.OpenView("PT0030");
                ViewFields fields = view.Fields;
                bool flag2 = view.GoTop();
                string str = "";
                string newValue = "";
                string filter = "DOCNUMBER = " + model.DOCNUM + " ";

                view.Browse(filter, true);
                for (bool flag = view.GoTop(); flag; flag = view.GoNext())
                {
                    str = (string)view.Fields.FieldByName("DOCNUMBER").Value;
                    if (str == model.DOCNUM)
                    {
                        newValue = Convert.ToString(view.Fields.FieldByName("SEQUENCE").Value);
                        break;
                    }
                }

                View view2 = link.OpenView("PT0032");
                ViewFields fields2 = view2.Fields;
                view.Compose(new View[] { view2 });
                view2.Compose(new View[] { view });
                View view3 = link.OpenView("PT0902");
                ViewFields fields3 = view3.Fields;
                view.Fields.FieldByName("WORKFLOW").SetValue(model.WorkFlow, true);
                view.Fields.FieldByName("SEQUENCE").SetValue(newValue, true);
                view.Read(false);
                view.Fields.FieldByName("COMMENT").SetValue(model.Comment, false);
                view.Update();
                view.Read(false);
                view.Fields.FieldByName("APPRSTATUS").SetValue(model.ApproveStatus, false);
                view.Update();
                view3.Process();
                session.Dispose();
                responseModel.SetSuccess();
                responseModel.ResponseObject = model.DOCNUM;
            }
            catch (Exception exception)
            {
                StringBuilder sbError = new StringBuilder();
                if (session != null && session.Errors != null)
                {

                    int TotalError = session.Errors.Count;
                    for (int i = 0; i < TotalError; i++)
                    {
                        if (session.Errors[i] != null && !string.IsNullOrEmpty(session.Errors[i].Message))
                        {
                            sbError.AppendLine(session.Errors[i].Message);
                        }
                    }
                }

                sbError.Append(exception.Message);
                responseModel.SetError(sbError.ToString());
            }

            if (session != null)
            {
                session.Dispose();
            }


            return responseModel;
        }


    }
}
