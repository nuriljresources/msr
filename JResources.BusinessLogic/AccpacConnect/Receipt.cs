﻿using ACCPAC.Advantage;
using EIEN.Sage.Inventory;
using JResources.Data.Model;
using JResources.Data.Model.Accpac;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace JResources.BusinessLogic.AccpacConnect
{
    public class Receipt
    {
        public static ResponseModel Save(IC0RCV ic0RCV)
        {
            ResponseModel responseModel = new ResponseModel(false);
            ResultPWReceipt result = new ResultPWReceipt();

            try
            {
                // Default di kosongin dulu coba
                string _vendorNum = "H0002IDR";
                string _fOBPoint = (!string.IsNullOrEmpty(ic0RCV.do_no) ? ic0RCV.do_no : string.Empty);
                string _termsCode = "";
                string _endAcct = "";
                string _billToLoc = "";
                string _shipToLoc = "";
                string _shipVia = "";
                string _discPct = "";

                EIEN.Sage.Inventory.Receipt receipt = new EIEN.Sage.Inventory.Receipt();
                DataSet dsPO = receipt.GetDataPO(ic0RCV.DBUserName, ic0RCV.DBPassword, ic0RCV.DBName, _vendorNum, ic0RCV.PONo);

                DataSet dsItem = new DataSet();
                //dsItem.Tables["tblItem"].Merge(dsPO.Tables["dtItem"]);



                DataTable dtItem = new DataTable("dtItem");
                dtItem.Columns.Add("RCPLREV", typeof(int));
                dtItem.Columns.Add("ITEMNO", typeof(string));
                dtItem.Columns.Add("ITEMDESC", typeof(string));
                dtItem.Columns.Add("POCOMPLETE", typeof(int));
                dtItem.Columns.Add("RQRECEIVED", typeof(decimal));
                dtItem.Columns.Add("LOCATION", typeof(string));
                dtItem.Columns.Add("UNITCOST", typeof(decimal));
                dtItem.Columns.Add("HASDROPSHI", typeof(int));
                dtItem.Columns.Add("DISCPCT", typeof(decimal));
                dtItem.Columns.Add("WEIGHTUNIT", typeof(string));
                dtItem.Columns.Add("UNITWEIGHT", typeof(decimal));
                dtItem.Columns.Add("RQCANCELED", typeof(decimal));
                dtItem.Columns.Add("RCPUNIT", typeof(string));
                dtItem.Columns.Add("RQOUTSTAND", typeof(decimal));
                dtItem.Columns.Add("LABELCOUNT", typeof(string));
                dtItem.Columns.Add("DTARRIVAL", typeof(string));
                dtItem.Columns.Add("OEONUMBER", typeof(string));
                dtItem.Columns.Add("HASCOMMENT", typeof(string));
                dtItem.Columns.Add("MANITEMNO", typeof(string));

                dtItem.Columns.Add("OPTFIELD1", typeof(string));
                dtItem.Columns.Add("VALIFTEXT1", typeof(string));
                dtItem.Columns.Add("OPTFIELD2", typeof(string));
                dtItem.Columns.Add("VALIFTEXT2", typeof(string));

                //Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(dsPO));

                DataTable poItems = dsPO.Tables["dtItem"];

                foreach (var item in ic0RCV.ICRCVList)
                {
                    if (dsPO != null && poItems != null)
                    {
                        var queryItem =
                            from itemPO in poItems.AsEnumerable()
                            where itemPO.Field<string>("ITEMNO") == item.itemNo.Trim()
                            select itemPO;

                        //Console.WriteLine(item.itemNo);

                        if (queryItem != null)
                        {
                            Console.WriteLine("MAPPING ITEM " + item.itemNo.Trim());

                            DataRow drItem = dtItem.NewRow();
                            var itemPO = queryItem.FirstOrDefault();

                            for (int i = 0; i < poItems.Columns.Count; i++)
                            {
                                drItem[poItems.Columns[i].ColumnName] = itemPO[dtItem.Columns[i].ColumnName];
                            }

                            

                            drItem["RQRECEIVED"] = item.qtyReceipt;
                            drItem["LOCATION"] = item.itemLocation;


                            //drItem["RCPLREV"] = queryItem.FirstOrDefault()["RCPLREV"];
                            //drItem["ITEMNO"] = queryItem.FirstOrDefault()["ITEMNO"];
                            //drItem["ITEMDESC"] = queryItem.FirstOrDefault()["ITEMDESC"];
                            //drItem["POCOMPLETE"] = queryItem.FirstOrDefault()["POCOMPLETE"];
                            //drItem["RQRECEIVED"] = item.qtyReceipt;
                            //drItem["LOCATION"] = queryItem.FirstOrDefault()["LOCATION"];
                            //drItem["UNITCOST"] = queryItem.FirstOrDefault()["UNITCOST"];
                            //drItem["HASDROPSHI"] = queryItem.FirstOrDefault()["HASDROPSHI"];
                            //drItem["DISCPCT"] = queryItem.FirstOrDefault()["DISCPCT"];
                            //drItem["WEIGHTUNIT"] = queryItem.FirstOrDefault()["WEIGHTUNIT"];
                            //drItem["UNITWEIGHT"] = queryItem.FirstOrDefault()["UNITWEIGHT"];
                            //drItem["RQCANCELED"] = queryItem.FirstOrDefault()["RQCANCELED"];
                            //drItem["RCPUNIT"] = queryItem.FirstOrDefault()["RCPUNIT"];
                            //drItem["RQOUTSTAND"] = queryItem.FirstOrDefault()["RQOUTSTAND"];
                            //drItem["LABELCOUNT"] = queryItem.FirstOrDefault()["LABELCOUNT"];
                            //drItem["DTARRIVAL"] = queryItem.FirstOrDefault()["DTARRIVAL"];
                            //drItem["OEONUMBER"] = queryItem.FirstOrDefault()["OEONUMBER"];
                            //drItem["HASCOMMENT"] = queryItem.FirstOrDefault()["HASCOMMENT"];
                            //drItem["MANITEMNO"] = queryItem.FirstOrDefault()["MANITEMNO"];

                            drItem["OPTFIELD1"] = "CCCODE";
                            drItem["VALIFTEXT1"] = "00";
                            drItem["OPTFIELD2"] = "PONO";
                            drItem["VALIFTEXT2"] = ic0RCV.PONo;

                            dtItem.Rows.Add(drItem);
                            
                        }
                    }


                }

                dsItem.Tables.Add(dtItem);

                string _txGroup = "VPIDR";
                _termsCode = "N60";
                string vdAcctSet = "APIDR";
                string addtCost = "0";



                //Console.WriteLine("Header Addtional Cost - BUILD");
                DataSet dsHeaderAddtCost = new DataSet();

                DataTable dtHeaderAddtCost = new DataTable("tblHeaderAddtCost");
                dtHeaderAddtCost.Columns.Add("VDCODE", typeof(string));
                dtHeaderAddtCost.Columns.Add("TAXGROUP", typeof(string));
                dtHeaderAddtCost.Columns.Add("TERMSCODE", typeof(string));
                dtHeaderAddtCost.Columns.Add("VDACCTSET", typeof(string));

                //DataRow drHeader = dtHeaderAddtCost.NewRow();
                //drHeader["VDCODE"] = _vendorNum;
                //drHeader["TAXGROUP"] = _txGroup;
                //drHeader["TERMSCODE"] = "";
                //drHeader["VDACCTSET"] = vdAcctSet;
                //dtHeaderAddtCost.Rows.Add(drHeader);

                dsHeaderAddtCost.Tables.Add(dtHeaderAddtCost);

                //Console.WriteLine("Header Addtional Cost - DONE");



                //Console.WriteLine("Header Addtional Detail Cost - BUILD");
                DataSet dsDetailAddtCost = new DataSet();
                DataTable dtDetailAddtCost = new DataTable("tblDetailAddtCost");
                dtDetailAddtCost.Columns.Add("VDCODE", typeof(string));
                dtDetailAddtCost.Columns.Add("ADDCOST", typeof(string));
                dtDetailAddtCost.Columns.Add("AMOUNT", typeof(string));
                dtDetailAddtCost.Columns.Add("PRORMETHOD", typeof(string));
                dtDetailAddtCost.Columns.Add("REFERENCE", typeof(string));
                dtDetailAddtCost.Columns.Add("COMMENT", typeof(string));

                //DataRow drDetail = dtDetailAddtCost.NewRow();
                //drDetail["VDCODE"] = _vendorNum;
                //drDetail["ADDCOST"] = addtCost;
                //drDetail["AMOUNT"] = "0";
                //drDetail["PRORMETHOD"] = "";
                //drDetail["REFERENCE"] = "";
                //drDetail["COMMENT"] = "";
                //dtDetailAddtCost.Rows.Add(drDetail);

                dsDetailAddtCost.Tables.Add(dtDetailAddtCost);
                //Console.WriteLine("Header Addtional Detail Cost - DONE");



                //Console.WriteLine("TAX TABLE - BUILD");
                DataSet dsTax = new DataSet();
                DataTable dtTax = new DataTable("tblTax");

                string _taxGroup = "VPIDR";
                DataSet ds = receipt.CalculateTax(ic0RCV.DBUserName, ic0RCV.DBPassword, ic0RCV.DBName, ic0RCV.PONo, _taxGroup, _discPct, dsItem, dsTax);

                if (receipt.status)
                {
                    dtTax.Merge(ds.Tables["dtTax"]);
                }

                dsTax.Tables.Add(dtTax);

                //Console.WriteLine("TAX TABLE - DONE");

                result = PostReceivePW(
                    ic0RCV.DBUserName,
                    ic0RCV.DBPassword,
                    ic0RCV.DBName,
                    ic0RCV.receiptNo,
                    _vendorNum,
                    ic0RCV.PONo,
                    ic0RCV.receiptDate,
                    ic0RCV.postingDate,
                    _fOBPoint,
                    _termsCode,
                    _endAcct,
                    _billToLoc,
                    _shipToLoc,
                    _shipVia,
                    ic0RCV.desc,
                    ic0RCV.reference,
                    _discPct, dsItem, dsTax, dsHeaderAddtCost, dsDetailAddtCost);


                if (result.message != "")
                {
                    responseModel.SetError(result.message);
                }
                else
                {
                    responseModel.SetSuccess("Save Succeeded! " + ic0RCV.receiptNo);
                }

            }
            catch (Exception ex)
            {
                responseModel.SetError(ex.Message);
            }

            return responseModel;
        }



        public static ResultPWReceipt PostReceivePW(string DBUserName, string DBPassword, string DBName,
               string receiptNo, string vendorNumber, string PONumber, DateTime receiptDate, DateTime postingDate
               , string FOBPoint, string termsCode, string vendorAcctSet, string billToLoc, string shipToLoc, string shipVia
               , string Desc, string reference, string discPct, DataSet dsItem, DataSet dsTax
               , DataSet dsHeaderAddtCost
               , DataSet dsDetailAddtCost

   )
        {
            //Dim temp As Boolean
            Session session;
            DBLink mDBLinkCmpRW;
            DBLink mDBLinkSysRW;

            //Added by Erda on 20171216
            ResultPWReceipt result = new ResultPWReceipt();

            try
            {
                // Create, initialize and open a session.
                session = new Session();
            }
            catch (Exception ex)
            {
                //Modified by Erda on 20171216
                //return ex.Message;
                result.message = ex.Message;
                return result;
            }

            try
            {
                // Create, initialize and open a session.
                //session = new Session();

                session.Init("", "XY", "XY1000", "61A");
                session.Open(DBUserName, DBPassword, DBName, DateTime.Today, 0);

                // Open a database link.
                mDBLinkCmpRW = session.OpenDBLink(DBLinkType.Company, DBLinkFlags.ReadWrite);
                mDBLinkSysRW = session.OpenDBLink(DBLinkType.System, DBLinkFlags.ReadWrite);

                Boolean temp;

                ACCPAC.Advantage.View PORCP1header;
                ACCPAC.Advantage.ViewFields PORCP1headerFields;
                PORCP1header = mDBLinkCmpRW.OpenView("PO0700");
                PORCP1headerFields = PORCP1header.Fields;

                ACCPAC.Advantage.View PORCP1detail1;
                ACCPAC.Advantage.ViewFields PORCP1detail1Fields;
                PORCP1detail1 = mDBLinkCmpRW.OpenView("PO0710");
                PORCP1detail1Fields = PORCP1detail1.Fields;

                ACCPAC.Advantage.View PORCP1detail2;
                ACCPAC.Advantage.ViewFields PORCP1detail2Fields;
                PORCP1detail2 = mDBLinkCmpRW.OpenView("PO0695");
                PORCP1detail2Fields = PORCP1detail2.Fields;

                ACCPAC.Advantage.View PORCP1detail3;
                ACCPAC.Advantage.ViewFields PORCP1detail3Fields;
                PORCP1detail3 = mDBLinkCmpRW.OpenView("PO0718");
                PORCP1detail3Fields = PORCP1detail3.Fields;

                ACCPAC.Advantage.View PORCP1detail4;
                ACCPAC.Advantage.ViewFields PORCP1detail4Fields;
                PORCP1detail4 = mDBLinkCmpRW.OpenView("PO0714");
                PORCP1detail4Fields = PORCP1detail4.Fields;

                ACCPAC.Advantage.View PORCP1detail5;
                ACCPAC.Advantage.ViewFields PORCP1detail5Fields;
                PORCP1detail5 = mDBLinkCmpRW.OpenView("PO0699");
                PORCP1detail5Fields = PORCP1detail5.Fields;

                ACCPAC.Advantage.View PORCP1detail6;
                ACCPAC.Advantage.ViewFields PORCP1detail6Fields;
                PORCP1detail6 = mDBLinkCmpRW.OpenView("PO0705");
                PORCP1detail6Fields = PORCP1detail6.Fields;

                ACCPAC.Advantage.View PORCP1detail7;
                ACCPAC.Advantage.ViewFields PORCP1detail7Fields;
                PORCP1detail7 = mDBLinkCmpRW.OpenView("PO0703");
                PORCP1detail7Fields = PORCP1detail7.Fields;

                ACCPAC.Advantage.View PORCP1detail8;
                ACCPAC.Advantage.ViewFields PORCP1detail8Fields;
                PORCP1detail8 = mDBLinkCmpRW.OpenView("PO0696");
                PORCP1detail8Fields = PORCP1detail8.Fields;

                ACCPAC.Advantage.View PORCP1detail9;
                ACCPAC.Advantage.ViewFields PORCP1detail9Fields;
                PORCP1detail9 = mDBLinkCmpRW.OpenView("PO0717");
                PORCP1detail9Fields = PORCP1detail9.Fields;

                ACCPAC.Advantage.View PORCP1detail10;
                ACCPAC.Advantage.ViewFields PORCP1detail10Fields;
                PORCP1detail10 = mDBLinkCmpRW.OpenView("PO0721");
                PORCP1detail10Fields = PORCP1detail10.Fields;

                ACCPAC.Advantage.View PORCP1detail11;
                ACCPAC.Advantage.ViewFields PORCP1detail11Fields;
                PORCP1detail11 = mDBLinkCmpRW.OpenView("PO0719");
                PORCP1detail11Fields = PORCP1detail11.Fields;

                ACCPAC.Advantage.View PORCP1detail12;
                ACCPAC.Advantage.ViewFields PORCP1detail12Fields;
                PORCP1detail12 = mDBLinkCmpRW.OpenView("PO0697");
                PORCP1detail12Fields = PORCP1detail12.Fields;

                ACCPAC.Advantage.View PORCP1detail13;
                ACCPAC.Advantage.ViewFields PORCP1detail13Fields;
                PORCP1detail13 = mDBLinkCmpRW.OpenView("PO0704");
                PORCP1detail13Fields = PORCP1detail13.Fields;

                ACCPAC.Advantage.View PORCP1detail14;
                ACCPAC.Advantage.ViewFields PORCP1detail14Fields;
                PORCP1detail14 = mDBLinkCmpRW.OpenView("PO0789");
                PORCP1detail14Fields = PORCP1detail14.Fields;

                ACCPAC.Advantage.View PORCP1detail15;
                ACCPAC.Advantage.ViewFields PORCP1detail15Fields;
                PORCP1detail15 = mDBLinkCmpRW.OpenView("PO0780");
                PORCP1detail15Fields = PORCP1detail15.Fields;


                //ICREE1header.Compose(new ACCPAC.Advantage.View[] { ICREE1detail1, ICREE1detail2 });
                //ICREE1detail1.Compose(new ACCPAC.Advantage.View[] { ICREE1header, null, null, null, null, null, ICREE1detail3, ICREE1detail5, ICREE1detail4 });
                //ICREE1detail2.Compose(new ACCPAC.Advantage.View[] { ICREE1header });
                //ICREE1detail3.Compose(new ACCPAC.Advantage.View[] { ICREE1detail1 });
                //ICREE1detail4.Compose(new ACCPAC.Advantage.View[] { ICREE1detail1 });
                //ICREE1detail5.Compose(new ACCPAC.Advantage.View[] { ICREE1detail1 });

                PORCP1header.Compose(new ACCPAC.Advantage.View[] { PORCP1detail2, PORCP1detail1, PORCP1detail3, PORCP1detail4
                                                    , PORCP1detail5, PORCP1detail6, PORCP1detail7, PORCP1detail8 });
                PORCP1detail1.Compose(new ACCPAC.Advantage.View[] { PORCP1header, PORCP1detail2, PORCP1detail5, null, null, PORCP1detail9, PORCP1detail14, PORCP1detail15 });
                PORCP1detail2.Compose(new ACCPAC.Advantage.View[] { PORCP1header, PORCP1detail1 });
                PORCP1detail3.Compose(new ACCPAC.Advantage.View[] { PORCP1header, PORCP1detail4, PORCP1detail5, PORCP1detail10 });
                PORCP1detail4.Compose(new ACCPAC.Advantage.View[] { PORCP1detail3, PORCP1detail5, PORCP1header, null, null, PORCP1detail11, PORCP1detail8 });
                PORCP1detail5.Compose(new ACCPAC.Advantage.View[] { PORCP1header, PORCP1detail2, PORCP1detail1, PORCP1detail4, PORCP1detail3, PORCP1detail6, PORCP1detail8 });
                PORCP1detail6.Compose(new ACCPAC.Advantage.View[] { PORCP1header, PORCP1detail5 });
                PORCP1detail7.Compose(new ACCPAC.Advantage.View[] { PORCP1header });
                PORCP1detail8.Compose(new ACCPAC.Advantage.View[] { PORCP1detail4, PORCP1detail3, PORCP1header, PORCP1detail5, PORCP1detail12 });
                PORCP1detail9.Compose(new ACCPAC.Advantage.View[] { PORCP1detail1 });
                PORCP1detail10.Compose(new ACCPAC.Advantage.View[] { PORCP1detail3 });
                PORCP1detail11.Compose(new ACCPAC.Advantage.View[] { PORCP1detail4 });
                PORCP1detail12.Compose(new ACCPAC.Advantage.View[] { null, PORCP1detail8, PORCP1detail4 });
                PORCP1detail13.Compose(new ACCPAC.Advantage.View[] { PORCP1detail8, PORCP1detail1 });
                PORCP1detail14.Compose(new ACCPAC.Advantage.View[] { PORCP1detail1, null, null });
                PORCP1detail15.Compose(new ACCPAC.Advantage.View[] { PORCP1detail1, null, null });



                PORCP1header.Order = 1;
                PORCP1header.Order = 0;


                PORCP1header.Fields.FieldByName("RCPHSEQ").SetValue("0", false); //            ' Receipt Sequence Key

                PORCP1header.Init();
                PORCP1header.Order = 1;
                temp = PORCP1detail1.Exists;
                PORCP1detail1.RecordClear();
                PORCP1detail3.RecordClear();
                temp = PORCP1detail4.Exists;
                PORCP1detail4.RecordClear();
                PORCP1detail6.Init();
                PORCP1detail2.Init();


                //Added by Erda on 20171216
                if (!string.IsNullOrEmpty(receiptNo))
                {
                    PORCP1header.Fields.FieldByName("RCPNUMBER").SetValue(receiptNo, true);
                }


                PORCP1header.Fields.FieldByName("PONUMBER").SetValue(PONumber, true);  //                  ' Purchase Order Number
                PORCP1header.Order = 0;
                PORCP1detail5.Fields.FieldByName("LOADPORNUM").SetValue(PONumber, true);  //               ' Purchase Order Number

                //PORCP1header.Fields.FieldByName("VDCODE").SetValue(vendorNumber, true);

                PORCP1detail5.Fields.FieldByName("FUNCTION").SetValue("4", true);          //          ' Function

                PORCP1detail5.Process();

                PORCP1header.Order = 1;
                PORCP1detail3.Fields.FieldByName("PROCESSCMD").SetValue("1", false); //        ' Command
                PORCP1detail3.Process();
                //PORCP1header.Fields.FieldByName("VDCODE").SetValue("1200", true);
                //PORCP1detail3.Fields.FieldByName("VDCODE").SetValue("1200", false); //         ' Vendor

                PORCP1header.Fields.FieldByName("DATE").SetValue(receiptDate, true); //          ' receipt Date
                PORCP1header.Fields.FieldByName("DATEBUS").SetValue(postingDate, true); //          ' Posting Date

                //DateTime dateReceipt = DateTime.ParseExact(receiptDate, "yyyy,MM,dd",
                //                       System.Globalization.CultureInfo.InvariantCulture);
                //DateTime datePosting = DateTime.ParseExact(postingDate, "yyyy,MM,dd",
                //                       System.Globalization.CultureInfo.InvariantCulture);
                DateTime now = DateTime.ParseExact(System.DateTime.Today.ToString("MM/dd/yyyy"), "MM/dd/yyyy",
                                       System.Globalization.CultureInfo.InvariantCulture);

                if (receiptDate > now)
                {
                    //Modified by Erda on 20171216
                    //return "Invalid Receipt Date!";
                    result.message = "Invalid Receipt Date!";
                    return result;
                }
                //else if ((Int32)Microsoft.VisualBasic.DateAndTime.DateDiff(Microsoft.VisualBasic.DateInterval.Day, now, datePosting) > 0)
                //{
                //    return "Invalid Posting Date!";
                //}

                //PORCP1detail5.Fields.FieldByName("FUNCTION").SetValue("61", true); //                          ' Function
                //PORCP1detail5.Process();

                PORCP1header.Fields.FieldByName("BTCODE").SetValue(billToLoc, true);  //   ' Bill-To Location

                PORCP1header.Fields.FieldByName("STCODE").SetValue(shipToLoc, true); // ' Ship-To Location

                PORCP1header.Fields.FieldByName("FOBPOINT").SetValue(FOBPoint, true); // ' FOB Point

                PORCP1header.Fields.FieldByName("VIACODE").SetValue(shipVia, true); // ' Ship-Via
                PORCP1header.Fields.FieldByName("DESCRIPTIO").SetValue(Desc, true); //        ' Description
                PORCP1header.Fields.FieldByName("REFERENCE").SetValue(reference, true); //              ' Reference

                if (!string.IsNullOrEmpty(vendorAcctSet))
                {
                    PORCP1header.Fields.FieldByName("VDCOUNTRY").SetValue(vendorAcctSet, true); //              ' vd acct set
                }

                if (!string.IsNullOrEmpty(termsCode))
                {
                    PORCP1header.Fields.FieldByName("TERMSCODE").SetValue(termsCode, true); //              ' terms Code
                }

                #region || Tax
                string taxNo = "0";
                if (dsTax.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsTax.Tables[0].Rows.Count; i++)
                    {
                        taxNo = dsTax.Tables[0].Rows[i]["TAXNO"].ToString();
                        //PORCP1header.Fields.FieldByName("TAXGROUP").SetValue(dsTax.Tables[0].Rows[i]["TAXGROUP"], true);
                        PORCP1header.Fields.FieldByName("TAXCLASS" + taxNo).SetValue(dsTax.Tables[0].Rows[i]["TAXCLASS"], true); //                           ' Tax Class 1
                        temp = PORCP1detail3.Exists;
                        PORCP1header.Fields.FieldByName("TAXBASE" + taxNo).SetValue(dsTax.Tables[0].Rows[i]["TAXBASE"], true); //                           ' Tax Class 2
                        temp = PORCP1detail3.Exists;
                        //PORCP1header.Fields.FieldByName("TXINCLUDE" + taxNo).SetValue(dsTax.Tables[0].Rows[i]["TXINCLUDE"], true); //                           ' Tax Class 2
                        //temp = PORCP1detail3.Exists;
                        //PORCP1header.Fields.FieldByName("TXEXCLUDE" + taxNo).SetValue(dsTax.Tables[0].Rows[i]["TXINCLUDE"], true); //                           ' Tax Class 2
                        //temp = PORCP1detail3.Exists;
                        PORCP1header.Fields.FieldByName("TAXAMOUNT" + taxNo).SetValue(dsTax.Tables[0].Rows[i]["TAXAMOUNT"], true); //                   ' Tax Amount 2
                        temp = PORCP1detail3.Exists;
                        //PORCP1header.Fields.FieldByName("TAXBASE" + taxNo).SetValue(dsTax.Tables[0].Rows[i]["TAXBASE"], true); //                   ' Tax Amount 1
                        temp = PORCP1detail3.Exists;
                        temp = PORCP1detail3.Exists;
                    }
                }

                #endregion

                #region || Item

                DataTable dtPO = new DataTable();
                DataRow drPO;
                dtPO.Columns.Add("RCPLREV", typeof(int));
                dtPO.Columns.Add("RQRECEIVED", typeof(decimal));
                dtPO.Columns.Add("LOCATION", typeof(string));
                dtPO.Columns.Add("UNITCOST", typeof(decimal));
                if (dsItem.Tables[0].Rows.Count > 0)
                {
                    PORCP1detail1.Read(false);
                    PORCP1detail5.Fields.FieldByName("FUNCTION").SetValue("6", true);  //
                    PORCP1detail5.Process();
                    //PORCP1detail1.Browse("RCPLREV = " + dsItem.Tables[0].Rows[i]["RCPLREV"] + "", true);
                    bool PORCP1headerGotOne = PORCP1detail1.GoTop();
                    while (PORCP1headerGotOne)
                    {
                        drPO = dtPO.NewRow();
                        drPO["RCPLREV"] = PORCP1detail1.Fields.FieldByName("RCPLREV").Value;
                        drPO["RQRECEIVED"] = PORCP1detail1.Fields.FieldByName("RQRECEIVED").Value;
                        drPO["UNITCOST"] = PORCP1detail1.Fields.FieldByName("UNITCOST").Value;

                        drPO["LOCATION"] = PORCP1detail1.Fields.FieldByName("LOCATION").Value;

                        dtPO.Rows.Add(drPO);

                        PORCP1headerGotOne = PORCP1detail1.GoNext();
                    }
                }

                //Reset function
                PORCP1detail5.Fields.FieldByName("FUNCTION").SetValue("4", true);          //          ' Function
                PORCP1detail5.Process();
                DataRow[] drPOSelected;
                if (dsItem.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsItem.Tables[0].Rows.Count; i++)
                    {

                        #region || Validation QTY and Unit Cost
                        drPOSelected = dtPO.Select("RCPLREV = " + dsItem.Tables[0].Rows[i]["RCPLREV"] + "");
                        if (drPOSelected.Length > 0)
                        {
                            if (Convert.ToDecimal(dsItem.Tables[0].Rows[i]["RQRECEIVED"]) > Convert.ToDecimal(drPOSelected[0]["RQRECEIVED"]))
                            {
                                //Modified by Erda on 20171216
                                //return "Quantity Received Can't be greater than PO Order Quantity (Item Number : " + dsItem.Tables[0].Rows[i]["ITEMNO"].ToString() + ") !";
                                result.message = "Quantity Received Can't be greater than PO Order Quantity (Item Number : " + dsItem.Tables[0].Rows[i]["ITEMNO"].ToString() + ") !";
                                return result;

                            }
                            else if (Convert.ToDecimal(dsItem.Tables[0].Rows[i]["UNITCOST"]) > Convert.ToDecimal(drPOSelected[0]["UNITCOST"]))
                            {
                                //Modified by Erda on 20171216
                                //return "Unit Cost Can't be greater than Unit Cost of PO Order(Item Number : " + dsItem.Tables[0].Rows[i]["ITEMNO"].ToString() + ") !";
                                result.message = "Unit Cost Can't be greater than Unit Cost of PO Order(Item Number : " + dsItem.Tables[0].Rows[i]["ITEMNO"].ToString() + ") !";
                                return result;

                            }
                        }
                        #endregion

                        PORCP1detail1.Fields.FieldByName("RCPLREV").SetValue(dsItem.Tables[0].Rows[i]["RCPLREV"], false); //          ' Line Number
                        //PORCP1detail1.Fields.FieldByName("ITEMNO").SetValue(dsItem.Tables[0].Rows[i]["ITEMNO"], true); //          ' ITEM NOr
                        PORCP1detail1.Read(false);
                        PORCP1detail1.Fields.FieldByName("POCOMPLETE").SetValue(dsItem.Tables[0].Rows[i]["POCOMPLETE"], true); //' Completes PO


                        PORCP1detail1.Fields.FieldByName("RQRECEIVED").SetValue(dsItem.Tables[0].Rows[i]["RQRECEIVED"], true); //' qty received PO
                        PORCP1detail1.Fields.FieldByName("UNITCOST").SetValue(dsItem.Tables[0].Rows[i]["UNITCOST"], true); //' unit cost
                        PORCP1detail1.Fields.FieldByName("LOCATION").SetValue(dsItem.Tables[0].Rows[i]["LOCATION"], true); //' qty received PO

                        //PORCP1detail1.Read(false);
                        PORCP1detail1.Fields.FieldByName("HASDROPSHI").SetValue(dsItem.Tables[0].Rows[i]["HASDROPSHI"], true); //        ' Drop-Ship
                        //temp = PORCP1detail1.Exists;

                        PORCP1detail1.Fields.FieldByName("DISCPCT").SetValue(dsItem.Tables[0].Rows[i]["DISCPCT"], true);    //' Discount Percentage

                        PORCP1detail1.Fields.FieldByName("WEIGHTUNIT").SetValue(dsItem.Tables[0].Rows[i]["WEIGHTUNIT"], true); //                        ' Weight Unit of Measure

                        //temp = PORCP1detail1.Exists;

                        PORCP1detail1.Fields.FieldByName("UNITWEIGHT").SetValue(dsItem.Tables[0].Rows[i]["UNITWEIGHT"], true); //                    ' Unit Weight

                        PORCP1detail1.Fields.FieldByName("RQCANCELED").SetValue(dsItem.Tables[0].Rows[i]["RQCANCELED"], true); //                   ' Quantity Canceled
                        PORCP1detail1.Fields.FieldByName("RCPUNIT").SetValue(dsItem.Tables[0].Rows[i]["RCPUNIT"], true); //                   ' UOM

                        //PORCP1detail1.Fields.FieldByName("RQOUTSTAND").SetValue(dsItem.Tables[0].Rows[i]["RQOUTSTAND"], true); //                    ' Quantity Outstanding

                        ////PORCP1detail1.Fields.FieldByName("VENDITEMNO").SetValue(dsItem.Tables[0].Rows[i]["VENDITEMNO"], true); //                      ' Vendor Item Number

                        PORCP1detail1.Fields.FieldByName("LABELCOUNT").SetValue(dsItem.Tables[0].Rows[i]["LABELCOUNT"], true); //                       ' Number of Labels

                        PORCP1detail1.Fields.FieldByName("DTARRIVAL").SetValue(dsItem.Tables[0].Rows[i]["DTARRIVAL"], true); //      ' Arrival Date

                        PORCP1detail1.Fields.FieldByName("OEONUMBER").SetValue(dsItem.Tables[0].Rows[i]["OEONUMBER"], false); //            ' Order Number

                        PORCP1detail1.Fields.FieldByName("HASCOMMENT").SetValue(dsItem.Tables[0].Rows[i]["HASCOMMENT"], true); //                         ' Comments

                        PORCP1detail1.Fields.FieldByName("MANITEMNO").SetValue(dsItem.Tables[0].Rows[i]["MANITEMNO"], true); //                       ' Manufacturer's Item Number


                        //PORCP1detail9.Read(false);
                        //PORCP1detail9.RecordCreate(0);
                        PORCP1detail9.Fields.FieldByName("OPTFIELD").SetValue(dsItem.Tables[0].Rows[i]["OPTFIELD1"], true); //   
                        PORCP1detail9.Fields.FieldByName("VALIFTEXT").SetValue(dsItem.Tables[0].Rows[i]["VALIFTEXT1"], true); //   

                        //PORCP1detail9.Insert();

                        //commented by Aditya on 20171215
                        ////PORCP1detail9.Read(false);
                        ////PORCP1detail9.RecordCreate(0);
                        //PORCP1detail9.Fields.FieldByName("OPTFIELD").SetValue(dsItem.Tables[0].Rows[i]["OPTFIELD2"], true); //   
                        //PORCP1detail9.Fields.FieldByName("VALIFTEXT").SetValue(dsItem.Tables[0].Rows[i]["VALIFTEXT2"], true); //   
                        ////PORCP1detail9.Insert();

                        ///temp = PORCP1detail1.Exists;
                        PORCP1detail1.Update();
                        //PORCP1detail1.Fields.FieldByName("RCPLREV").SetValue(dsItem.Tables[0].Rows[i]["RCPLREV"], false); //          ' Line Number
                    }
                }
                #endregion


                //PORCP1detail5.Browse("PONUMBER = PO000000013", true);

                //bool VMDHGotOne = PORCP1detail5.GoTop();
                //while (VMDHGotOne)
                //{

                //    VMDHGotOne = PORCP1detail5.GoNext();
                //}

                temp = PORCP1detail3.Exists;
                PORCP1detail5.Fields.FieldByName("FUNCTION").SetValue("10", false); //         ' Function
                PORCP1detail5.Process();
                temp = PORCP1detail3.Exists;

                #region additional cost


                //PORCP1detail3.Browse("RCPHSEQ = 18500", true);
                //PORCP1detail3.RecordClear();
                //PORCP1detail3.Browse("RCPHSEQ = 18500", true);

                PORCP1detail3.RecordClear();
                DataRow[] drAdttCostDetail;
                if (dsHeaderAddtCost.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < dsHeaderAddtCost.Tables[0].Rows.Count; i++)
                    {
                        PORCP1detail3.RecordCreate(0);
                        PORCP1detail3.Fields.FieldByName("VDCODE").SetValue(dsHeaderAddtCost.Tables[0].Rows[i]["VDCODE"], false); //             ' Vendor

                        PORCP1detail3.Fields.FieldByName("TAXGROUP").SetValue(dsHeaderAddtCost.Tables[0].Rows[i]["TAXGROUP"], false); //             ' Vendor
                        PORCP1detail3.Fields.FieldByName("TERMSCODE").SetValue(dsHeaderAddtCost.Tables[0].Rows[i]["TERMSCODE"], false); //             ' Vendor
                        PORCP1detail3.Fields.FieldByName("VDACCTSET").SetValue(dsHeaderAddtCost.Tables[0].Rows[i]["VDACCTSET"], false); //             ' Vendor
                        //PORCP1detail3.Browse("(RCPHSEQ = 18500)", true); 
                        //PORCP1detail3.RecordClear();

                        //PORCP1detail3.Fields.FieldByName("VDCODE").SetValue("1200", false); //                          ' Vendor

                        //temp = PORCP1detail3.Exists;
                        //PORCP1detail3.RecordCreate(0);

                        //PORCP1detail3.Fields.FieldByName("VDCODE").SetValue("1200", false); //                          ' Vendor
                        //temp = PORCP1detail3.Exists;

                        PORCP1detail3.Fields.FieldByName("PROCESSCMD").SetValue("1", false);     //    ' Command

                        PORCP1detail3.Process();
                        temp = PORCP1detail3.Exists;
                        PORCP1detail3.Insert();


                        drAdttCostDetail = dsDetailAddtCost.Tables[0].Select("VDCODE = " + dsHeaderAddtCost.Tables[0].Rows[i]["VDCODE"] + "");
                        temp = PORCP1detail4.Exists;
                        PORCP1detail4.RecordClear();
                        for (int j = 0; j < drAdttCostDetail.Length; j++)
                        {
                            //temp = PORCP1detail4.Exists;
                            //temp = PORCP1detail4.Exists;
                            PORCP1detail4.RecordCreate(0);
                            //temp = PORCP1detail4.Exists;

                            PORCP1detail4.Fields.FieldByName("ADDCOST").SetValue(drAdttCostDetail[j]["ADDCOST"], false); //' Additional Cost
                            PORCP1detail4.Fields.FieldByName("AMOUNT").SetValue(drAdttCostDetail[j]["AMOUNT"], true); //                        ' Amount
                            PORCP1detail4.Fields.FieldByName("PRORMETHOD").SetValue(drAdttCostDetail[j]["PRORMETHOD"], true); //                         ' Proration Method
                            PORCP1detail4.Fields.FieldByName("REFERENCE").SetValue(drAdttCostDetail[j]["REFERENCE"], true); //      ' Reference
                            PORCP1detail4.Fields.FieldByName("COMMENT").SetValue(drAdttCostDetail[j]["COMMENT"], true); //                         ' Comment
                            PORCP1detail4.Insert();
                            //temp = PORCP1detail3.Exists;
                            PORCP1detail3.Update();
                            // temp = PORCP1detail4.Exists;

                            PORCP1detail4.Fields.FieldByName("RCPSREV").SetValue("-" + (j + 1).ToString(), false); //          ' Line Number

                            PORCP1detail4.Read(false);
                            temp = PORCP1detail4.Exists;
                            temp = PORCP1detail4.Exists;
                        }

                        temp = PORCP1detail3.Exists;


                    }

                }
                #endregion

                //PORCP1header.Fields.FieldByName("RATE").SetValue("0.9000000",false); //                        ' Exchange Rate
                temp = PORCP1detail3.Exists;
                temp = PORCP1detail3.Exists;
                PORCP1detail5.Fields.FieldByName("FUNCTION").SetValue("10", false); //         ' Function
                PORCP1detail5.Process();
                temp = PORCP1detail3.Exists;

                //PORCP1detail3.Fields.FieldByName("VDCODE").SetValue(vendorNumber, false); //         ' Vendor

                PORCP1detail3.Read(false);
                temp = PORCP1detail3.Exists;
                if (!string.IsNullOrEmpty(discPct))
                {
                    PORCP1header.Fields.FieldByName("DISCPCT").SetValue(discPct, true); //                       ' Discount Percentage
                }
                temp = PORCP1detail3.Exists;
                PORCP1detail5.Fields.FieldByName("FUNCTION").SetValue("10", false); //         ' Function
                PORCP1detail5.Process();
                temp = PORCP1detail3.Exists;

                //PORCP1detail3.Fields.FieldByName("VDCODE").SetValue("1200",false); //         ' Vendor

                //PORCP1detail3.Read(false); 
                //temp = PORCP1detail3.Exists;
                //PORCP1detail1.Fields.FieldByName("RCPLREV").SetValue("-3", false); //          ' Line Number
                //PORCP1detail1.Read(false);
                //PORCP1detail3.Fields.FieldByName("VDCODE").SetValue("1200", false); //         ' Vendor
                //PORCP1detail3.Read(false);
                //temp = PORCP1detail3.Exists;
                //PORCP1detail3.Browse("(RCPHSEQ = 18500)", true);

                //PORCP1detail4.Fields.FieldByName("RCPSREV").SetValue("-1", false); //          ' Line Number
                //PORCP1detail4.Read(false);
                //temp = PORCP1detail4.Exists;

                //PORCP1detail3.Browse("(RCPHSEQ = 18500)", true);

                //PORCP1detail5.Fields.FieldByName("FUNCTION").SetValue("10",false); //         ' Function
                PORCP1detail5.Process();
                temp = PORCP1header.Exists;


                PORCP1header.Fields.FieldByName("FOBPOINT").SetValue(FOBPoint, true); // ' FOB Point
                PORCP1header.Fields.FieldByName("DATE").SetValue(receiptDate, true); //          ' receipt Date
                PORCP1header.Fields.FieldByName("DATEBUS").SetValue(postingDate, true); //          ' Posting Date
                PORCP1header.Insert();
                //PORCP1detail5.Fields.FieldByName("RCPHSEQ").SetValue("18500", false); //       ' Receipt Sequence Key

                //Added by Erda on 20171216 || Get Last Receipt Number
                result.lastReceiptNumber = PORCP1header.Fields.FieldByName("RCPNUMBER").Value.ToString();

                PORCP1detail5.Fields.FieldByName("FUNCTION").SetValue("2", false); //          ' Function

                PORCP1detail5.Process();
                PORCP1header.Init();
                PORCP1header.Order = 0;

                PORCP1header.Fields.FieldByName("RCPHSEQ").SetValue("0", false); //            ' Receipt Sequence Key

                PORCP1header.Init();
                PORCP1header.Order = 1;
                temp = PORCP1detail1.Exists;
                PORCP1detail1.RecordClear();
                PORCP1detail3.RecordClear();
                temp = PORCP1detail4.Exists;
                PORCP1detail4.RecordClear();
                PORCP1detail6.Init();
                PORCP1detail2.Init();

                session.Dispose();

                //Modified by Erda on 20171216
                //if (session.Errors != null)
                //{
                //    return session.Errors[0].Message;
                //}
                //else
                //{
                //    return "";
                //}
                if (session.Errors != null)
                {
                    result.message = session.Errors[0].Message;
                }
                else
                {
                    result.message = "";
                }
                //return result;
                //return "";

            }
            catch (Exception ex)
            {
                if (session == null)
                {
                    //Modified by Erda on 20171216
                    //return ex.Message;
                }
                else if (session.Errors != null)
                {
                    string sessionError = "";
                    if (session.Errors.Count != 0)
                    {
                        sessionError = session.Errors[0].Message;
                    }
                    else
                    {
                        sessionError = session.Errors.ToString();
                    }
                    session.Dispose();

                    //Modified by Erda on 20171216
                    //return sessionError;
                    result.message = sessionError;
                }
                else
                {
                    session.Dispose();

                    //Modified by Erda on 20171216
                    //if (ex.InnerException != null)
                    //    return ex.InnerException.Message;
                    //else
                    //    return ex.Message;
                    if (ex.InnerException != null)
                        result.message = ex.InnerException.Message;
                    else
                        result.message = ex.Message;

                }

            }

            return result;
        }








    }
}
