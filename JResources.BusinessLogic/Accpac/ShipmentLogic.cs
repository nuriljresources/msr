﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JResources.Common;
using JResources.Data;
using JResources.Data.Model;
using JResources.Data.Model.Accpac;

namespace JResources.BusinessLogic.Accpac
{
    public class ShipmentLogic
    {
        public static ResponseModel Save(MSRIssuedModel IssuedModel)
        {
            ResponseModel response = new ResponseModel();
            if (!string.IsNullOrEmpty(IssuedModel.MSRIssuedNo))
            {
                Company company = Company.GetById(IssuedModel.CompanyId);
                string DataSource = AccpacDataContext.GetDatabaseName(company.CompanyCode);

                IC0640 ic0640 = new IC0640();
                ic0640.DBName = DataSource;
                ic0640.DBUsername = SiteSettings.ACCPAC_USERNAME;
                ic0640.DBPassword = SiteSettings.ACCPAC_PASSWORD;

                ic0640.DOCNUM = IssuedModel.MSRIssuedNo;
                ic0640.HDRDESC = (string.IsNullOrEmpty(IssuedModel.Remarks) ? string.Empty : IssuedModel.Remarks);
                ic0640.REFERENCE = IssuedModel.MSR.DocumentNo;
                ic0640.TRANSDATE = DateTime.Now;
                ic0640.DATEBUS = DateTime.Now;
                ic0640.CUSTNO = string.Empty;
                ic0640.CONTACT = string.Empty;
                ic0640.STATUS = "1";
                ic0640.CURRENCY = company.Currency;
                ic0640.OPTFIELD = "IRFNO";
                ic0640.SWSET = "1";
                ic0640.VALIFTEXT = IssuedModel.MSR.DocumentNo;
                ic0640.IC0630List = new List<IC0630>();

                foreach (var IssuedDetail in IssuedModel.MSRIssuedDetails)
                {
                    IC0630 ic0630 = new IC0630();
                    ic0630.LINENO = IssuedDetail.LineNo;
                    ic0630.ITEMNO = IssuedDetail.ItemNo;
                    ic0630.CATEGORY = IssuedDetail.CostCode;
                    ic0630.LOCATION = IssuedDetail.LocationFrom;
                    ic0630.QUANTITY = IssuedDetail.QtyProcess;
                    ic0630.UNIT = IssuedDetail.UOM;
                    ic0630.COMMENTS = ic0640.HDRDESC;
                    ic0630.MANITEMNO = "12345";
                    ic0630.FUNCTION = "100";


                    ic0630.IC0635List.Add(new IC0635() {
                        OPTFIELD = "CCCODE",
                        SWSET = "1",
                        VALIFTEXT = IssuedModel.CostCenter,
                    });

                    ic0630.IC0635List.Add(new IC0635()
                    {
                        OPTFIELD = "ECCODE",
                        SWSET = "1",
                        VALIFTEXT = "00",
                    });

                    ic0630.IC0635List.Add(new IC0635()
                    {
                        OPTFIELD = "MTCODE",
                        SWSET = "1",
                        VALIFTEXT = "00",
                    });

                    ic0640.IC0630List.Add(ic0630);
                }

                API api = new API();
                api.BASE_URL = SiteSettings.ACCPAC_URL;
                response = api.Post(URI_ACCPAC.SHIPMENT_SAVE, ic0640);

                //if (response.IsSuccess)
                //{
                //    AccpacPostBaseModel AccpacPost = new AccpacPostBaseModel();
                //    AccpacPost.DBName = ic0640.DBName;
                //    AccpacPost.DBUsername = ic0640.DBUsername;
                //    AccpacPost.DBPassword = ic0640.DBPassword;
                //    AccpacPost.DOCNUM = ic0640.DOCNUM;

                //    response = api.Post(URI_ACCPAC.SHIPMENT_POST, AccpacPost);
                //}
            }

            return response;
        }

        public static ResponseModel FuelSave(FRFModel frfModel)
        {
            ResponseModel response = new ResponseModel();

            IC0640 ic0640 = new IC0640();
            Company company = Company.GetById(frfModel.CompanyId);
            var dataSource = AccpacDataContext.GetDatabaseName(company.CompanyCode);

            ic0640.DBUsername = SiteSettings.ACCPAC_USERNAME;
            ic0640.DBPassword = SiteSettings.ACCPAC_PASSWORD;
            ic0640.DBName = dataSource;
            ic0640.DOCNUM = frfModel.FRFNo;
            ic0640.HDRDESC = string.Format("{0} FRF Import to Accpac automatically", frfModel.FRFNo);
            ic0640.REFERENCE = "";
            ic0640.TRANSDATE = frfModel.FRFDate.Value;
            ic0640.DATEBUS = frfModel.FRFDate.Value;
            ic0640.CUSTNO = string.Empty;
            ic0640.CONTACT = string.Empty;
            ic0640.STATUS = "1";
            ic0640.CURRENCY = company.Currency;
            ic0640.OPTFIELD = "IRFNO";
            ic0640.SWSET = "1";
            ic0640.VALIFTEXT = frfModel.FRFNo;

            foreach (var item in frfModel.FRFDetails)
            {
                IC0630 ic0630 = new IC0630();
                ic0630.ITEMNO = item.ItemNo;
                ic0630.CATEGORY = item.Category;
                ic0630.UNIT = item.Unit;

                ic0630.LOCATION = item.LocationId;
                ic0630.QUANTITY = (item.Qty.HasValue ? item.Qty.Value : 0);

                ic0630.IC0635List.Add(new IC0635()
                {
                    OPTFIELD = "CCCODE",
                    SWSET = "1",
                    VALIFTEXT = item.CostCode,
                });

                ic0630.IC0635List.Add(new IC0635()
                {
                    OPTFIELD = "ECCODE",
                    SWSET = "1",
                    VALIFTEXT = item.EquipmentCode,
                });

                ic0630.IC0635List.Add(new IC0635()
                {
                    OPTFIELD = "MTCODE",
                    SWSET = "1",
                    VALIFTEXT = item.MaterialCode,
                });

                ic0630.COMMENTS = "12345";
                ic0630.MANITEMNO = "12345";
                ic0630.FUNCTION = "100";

                ic0640.IC0630List.Add(ic0630);
            }

            API api = new API();
            api.BASE_URL = SiteSettings.ACCPAC_URL;
            response = api.Post(URI_ACCPAC.SHIPMENT_SAVE, ic0640);

            //if (response.IsSuccess)
            //{
            //    AccpacPostBaseModel AccpacPost = new AccpacPostBaseModel();
            //    AccpacPost.DBName = ic0640.DBName;
            //    AccpacPost.DBUsername = ic0640.DBUsername;
            //    AccpacPost.DBPassword = ic0640.DBPassword;
            //    AccpacPost.DOCNUM = ic0640.DOCNUM;

            //    response = api.Post(URI_ACCPAC.SHIPMENT_POST, AccpacPost);
            //}

            return response;
        }

        public static IC0640 GetAccpacModel(string IssuedNo)
        {
            MSRIssued msrIssued = MSRIssued.GetByIssuedNo(IssuedNo);
            List<MSRDetail> MSRDetail = msrIssued.MSR.MSRDetails.ToList();

            Company company = Company.GetById(msrIssued.CompanyId);
            string DataSource = AccpacDataContext.GetDatabaseName(company.CompanyCode);

            IC0640 ic0640 = new IC0640();
            ic0640.DBName = DataSource;
            ic0640.DBUsername = SiteSettings.ACCPAC_USERNAME;
            ic0640.DBPassword = SiteSettings.ACCPAC_PASSWORD;

            ic0640.DOCNUM = msrIssued.MSRIssuedNo;
            ic0640.HDRDESC = (string.IsNullOrEmpty(msrIssued.Remarks) ? string.Empty : msrIssued.Remarks);
            if (string.IsNullOrEmpty(ic0640.HDRDESC))
            {
                if (!string.IsNullOrEmpty(msrIssued.MSR.Remark))
                {
                    ic0640.HDRDESC = msrIssued.MSR.Remark;
                }
            }

            ic0640.REFERENCE = msrIssued.MSR.MSRNo;
            ic0640.TRANSDATE = msrIssued.CreatedDate;
            ic0640.DATEBUS = msrIssued.CreatedDate;
            ic0640.CUSTNO = string.Empty;
            ic0640.CONTACT = string.Empty;
            ic0640.STATUS = "1";
            ic0640.CURRENCY = company.Currency;
            ic0640.OPTFIELD = "IRFNO";
            ic0640.SWSET = "1";
            ic0640.VALIFTEXT = msrIssued.MSR.MSRNo;
            ic0640.IC0630List = new List<IC0630>();

            foreach (var IssuedDetail in msrIssued.MSRIssuedDetails)
            {
                var msrItem = MSRDetail.FirstOrDefault(x => x.LineNo == IssuedDetail.LineNo && x.ItemNo == IssuedDetail.ItemNo);

                IC0630 ic0630 = new IC0630();
                ic0630.LINENO = IssuedDetail.LineNo;
                ic0630.ITEMNO = IssuedDetail.ItemNo;
                ic0630.CATEGORY = msrItem.CategoryCode;
                ic0630.LOCATION = IssuedDetail.LocationIdFrom;
                ic0630.QUANTITY = IssuedDetail.Qty;
                ic0630.UNIT = msrItem.UOM;
                ic0630.COMMENTS = ic0640.HDRDESC;
                ic0630.MANITEMNO = "12345";
                ic0630.FUNCTION = "100";


                ic0630.IC0635List.Add(new IC0635()
                {
                    OPTFIELD = "CCCODE",
                    SWSET = "1",
                    VALIFTEXT = msrIssued.MSR.CostCode,
                });

                ic0630.IC0635List.Add(new IC0635()
                {
                    OPTFIELD = "ECCODE",
                    SWSET = "1",
                    VALIFTEXT = "00",
                });

                ic0630.IC0635List.Add(new IC0635()
                {
                    OPTFIELD = "MTCODE",
                    SWSET = "1",
                    VALIFTEXT = "00",
                });

                ic0640.IC0630List.Add(ic0630);
            }

            return ic0640;
        }

        public static IC0640 GetFuelModel(Guid FRFId)
        {
            FRFModel frfModel = FRFLogic.GetDataById(FRFId);        

            IC0640 ic0640 = new IC0640();
            Company company = Company.GetById(frfModel.CompanyId);
            var dataSource = AccpacDataContext.GetDatabaseName(company.CompanyCode);

            ic0640.DBUsername = SiteSettings.ACCPAC_USERNAME;
            ic0640.DBPassword = SiteSettings.ACCPAC_PASSWORD;
            ic0640.DBName = dataSource;
            ic0640.DOCNUM = frfModel.FRFNo;
            ic0640.HDRDESC = string.Format("{0} FRF Import to Accpac automatically", frfModel.FRFNo);
            ic0640.REFERENCE = "";
            ic0640.TRANSDATE = frfModel.FRFDate.Value;
            ic0640.DATEBUS = frfModel.FRFDate.Value;
            ic0640.CUSTNO = string.Empty;
            ic0640.CONTACT = string.Empty;
            ic0640.STATUS = "1";
            ic0640.CURRENCY = company.Currency;
            ic0640.OPTFIELD = "IRFNO";
            ic0640.SWSET = "1";
            ic0640.VALIFTEXT = frfModel.FRFNo;

            foreach (var item in frfModel.FRFDetails)
            {
                IC0630 ic0630 = new IC0630();
                ic0630.ITEMNO = item.ItemNo;
                ic0630.CATEGORY = (item.Category.Length > 0 ? item.Category.Trim() : item.Category);
                ic0630.UNIT = item.Unit;

                ic0630.LOCATION = item.LocationId;
                ic0630.QUANTITY = (item.Qty.HasValue ? item.Qty.Value : 0);

                ic0630.IC0635List.Add(new IC0635()
                {
                    OPTFIELD = "CCCODE",
                    SWSET = "1",
                    VALIFTEXT = item.CostCode,
                });

                ic0630.IC0635List.Add(new IC0635()
                {
                    OPTFIELD = "ECCODE",
                    SWSET = "1",
                    VALIFTEXT = item.EquipmentCode,
                });

                ic0630.IC0635List.Add(new IC0635()
                {
                    OPTFIELD = "MTCODE",
                    SWSET = "1",
                    VALIFTEXT = item.MaterialCode,
                });

                ic0630.COMMENTS = "12345";
                ic0630.MANITEMNO = "12345";
                ic0630.FUNCTION = "100";

                ic0640.IC0630List.Add(ic0630);
            }

            return ic0640;
        }
    }
}

