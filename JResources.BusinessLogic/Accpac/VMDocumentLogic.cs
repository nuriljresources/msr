﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JResources.Common;
using JResources.Common.Enum;
using JResources.Data;
using JResources.Data.Model;
using JResources.Data.Model.Accpac;

namespace JResources.BusinessLogic.Accpac
{
    public class VMDocumentLogic
    {
        /// <summary>
        /// Save VM Document
        /// </summary>
        /// <param name="IssuedModel"></param>
        /// <returns></returns>
        public static ResponseModel Save(MSRIssuedModel IssuedModel, VM0050 vm0050)
        {
            ResponseModel response = new ResponseModel();

            if (vm0050 != null)
            {
                Company company = Company.GetById(IssuedModel.CompanyId);
                string DataSource = AccpacDataContext.GetDatabaseName(company.CompanyCode);
                vm0050.DBName = DataSource;
                vm0050.DBUsername = SiteSettings.ACCPAC_USERNAME;
                vm0050.DBPassword = SiteSettings.ACCPAC_PASSWORD;
                vm0050.DOCNUM = vm0050.TXDOCID;

                vm0050.VM0069List.ForEach(x =>
                {
                    var IssuedItem = IssuedModel.MSRIssuedDetails.FirstOrDefault(y => y.LineNo == x.WDTRANNUM
                        && y.ItemNo.Trim() == x.ITEMNO.Trim());
                    if (IssuedItem != null)
                    {
                        x.QTSUPPLIED = IssuedItem.QtyProcess;
                    }
                });


                API api = new API();
                api.BASE_URL = SiteSettings.ACCPAC_URL;
                response = api.Post(URI_ACCPAC.VMDOCUMENT_SAVE, vm0050);

                if (response.IsSuccess)
                {
                    AccpacPostBaseModel AccpacPost = new AccpacPostBaseModel();
                    AccpacPost.DBName = vm0050.DBName;
                    AccpacPost.DBUsername = vm0050.DBUsername;
                    AccpacPost.DBPassword = vm0050.DBPassword;
                    AccpacPost.DOCNUM = vm0050.DOCNUM;

                    response = api.Post(URI_ACCPAC.VMDOCUMENT_POST, AccpacPost);
                }
            }
            else
            {
                response.SetError("Work Order Accpac Failed Get");
            }

            return response;
        }

        /// <summary>
        /// Get VM Document
        /// </summary>
        /// <param name="DocumentNo"></param>
        /// <param name="CompanyId"></param>
        /// <returns></returns>
        public static ResponseModel GetVMDocument(string DocumentNo, Guid CompanyId)
        {
            ResponseModel response = new ResponseModel();

            AccpacPostBaseModel accpacPost = new AccpacPostBaseModel();
            Company company = Company.GetById(CompanyId);
            string DataSource = AccpacDataContext.GetDatabaseName(company.CompanyCode);
            accpacPost.DBName = DataSource;
            accpacPost.DBUsername = SiteSettings.ACCPAC_USERNAME;
            accpacPost.DBPassword = SiteSettings.ACCPAC_PASSWORD;
            accpacPost.DOCNUM = DocumentNo;

            API api = new API();
            api.BASE_URL = SiteSettings.ACCPAC_URL;
            return api.Post(URI_ACCPAC.VMDOCUMENT_VIEW, accpacPost);
        }

        public static VM0050 GetAccpacModel(string IssuedNo)
        {
            MSRIssued issued = MSRIssued.GetByIssuedNo(IssuedNo);
            var vm0050 = WorkOrderLogic.GetWorkOrderAccpac(issued.MSR.WorkOrderNo);

            Company company = Company.GetById(issued.CompanyId);
            string DataSource = AccpacDataContext.GetDatabaseName(company.CompanyCode);
            vm0050.DBName = DataSource;
            vm0050.DBUsername = SiteSettings.ACCPAC_USERNAME;
            vm0050.DBPassword = SiteSettings.ACCPAC_PASSWORD;
            vm0050.DOCNUM = vm0050.TXDOCID;

            vm0050.VM0069List.ForEach(x =>
            {
                var IssuedItem = issued.MSRIssuedDetails.FirstOrDefault(y => y.LineNo == x.WDTRANNUM
                    && y.ItemNo.Trim() == x.ITEMNO.Trim());
                if (IssuedItem != null)
                {
                    x.QTSUPPLIED = IssuedItem.Qty;
                    var QTINVOICED = x.QTINVOICED + IssuedItem.Qty;
                    if (QTINVOICED > x.QTESTIMATE)
                    {
                        if (IssuedItem.Qty > x.QTESTIMATE)
                        {
                            x.QTINVOICED = x.QTESTIMATE;
                        }
                        else
                        {
                            x.QTINVOICED = IssuedItem.Qty;
                        }
                    }
                    else
                    {
                        x.QTINVOICED = QTINVOICED;
                    }
                }
            });

            return vm0050;
        }

        public static string Compare(VM0050 modelUpdate)
        {
            StringBuilder sbMessageError = new StringBuilder();
            var vm0050 = WorkOrderLogic.GetWorkOrderAccpac(modelUpdate.TXDOCID);

            foreach (var item in modelUpdate.VM0069List)
            {
                var itemCheck = vm0050.VM0069List.FirstOrDefault(x => x.WDTRANNUM == item.WDTRANNUM);
                if (item != null)
                {
                    if (item.QTINVOICED != itemCheck.QTINVOICED)
                    {
                        string msg = string.Format("Line {0} - item {1} Qty Supplied not update", item.WDTRANNUM, item.ITEMNO);
                        sbMessageError.AppendLine(msg);
                    }
                }
            }

            return sbMessageError.ToString();
        }


        public static ResponseModel ComparasionCheck(string IssuedNo, string WorkOrderNo)
        {
            var response = new ResponseModel();
            //response.SetError("Not found in Work Order Accpac");


            StringBuilder sbMessageError = new StringBuilder();
            Dictionary<int, decimal> dicItem = new Dictionary<int, decimal>();

            var IssuedData = CurrentDataContext.CurrentContext.MSRIssueds.FirstOrDefault(x => x.MSRIssuedNo == IssuedNo);
            if(IssuedData != null)
            {
                foreach (var IssuedDetail in IssuedData.MSRIssuedDetails)
                {
                    if (dicItem.ContainsKey(IssuedDetail.LineNo))
                    {
                        dicItem[IssuedDetail.LineNo] += IssuedDetail.Qty;
                    }
                    else
                    {
                        dicItem.Add(IssuedDetail.LineNo, IssuedDetail.Qty);
                    }
                }
            }

            VMDH vmdh = AccpacDataContext.CurrentContext.VMDHs.FirstOrDefault(x => x.TXDOCID.Contains(WorkOrderNo));
            foreach (var itemIssued in dicItem)
            {
                VMDTIT vmdtit = AccpacDataContext.CurrentContext.VMDTITs.FirstOrDefault(x => x.NMDOCID == vmdh.NMDOCID && x.WDTRANNUM == itemIssued.Key);
                if(vmdtit != null)
                {
                    if (vmdtit.QTINVOICED < itemIssued.Value)
                    {
                        sbMessageError.AppendLine(string.Format("Line item {0} with Item {1} different qty posted. MSR ({2}) == ACCPAC ({3})", itemIssued.Key, vmdtit.TXITEM.Trim(), itemIssued.Value, vmdtit.QTINVOICED));
                    }
                } else
                {
                    sbMessageError.AppendLine(string.Format("Line item {0} not found in Accpac", itemIssued.Key));
                }
            }

            if (sbMessageError.Length > 0)
                response.SetError(sbMessageError.ToString());
            else
                response.SetSuccess();

            return response;
        }
    }
}
