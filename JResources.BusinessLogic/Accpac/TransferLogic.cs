﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JResources.Common;
using JResources.Data;
using JResources.Data.Model;
using JResources.Data.Model.Accpac;

namespace JResources.BusinessLogic.Accpac
{
    public class TransferLogic
    {
        public static ResponseModel Save(MSRIssuedModel IssuedModel)
        {
            ResponseModel response = new ResponseModel();

            if (!string.IsNullOrEmpty(IssuedModel.MSRIssuedNo))
            {
                IC0740 ic0740 = new IC0740();
                Company company = Company.GetById(IssuedModel.CompanyId);
                string DataSource = AccpacDataContext.GetDatabaseName(company.CompanyCode);
                ic0740.DBName = DataSource;
                ic0740.DBUsername = SiteSettings.ACCPAC_USERNAME;
                ic0740.DBPassword = SiteSettings.ACCPAC_PASSWORD;

                ic0740.DOCNUM = IssuedModel.MSRIssuedNo;
                ic0740.TRANSDATE = IssuedModel.MSR.DateCreated;
                ic0740.DATEBUS = DateTime.Now;
                ic0740.EXPARDATE = IssuedModel.MSR.ExpectedDate;
                ic0740.HDRDESC = IssuedModel.Remarks;
                ic0740.REFERENCE = string.Format("{0};{1};{2}", IssuedModel.MSRIssuedNo, IssuedModel.MSR.DocumentNo, IssuedModel.MSR.WorkOrderNo);
                ic0740.ADDCOST = "0";
                ic0740.PRORMETHOD = "3";
                ic0740.STATUS = "1";

                foreach (var issuedDetail in IssuedModel.MSRIssuedDetails)
                {
                    IC0730 ic0730 = new IC0730();
                    ic0730.LINENO = issuedDetail.LineNo;
                    ic0730.ITEMNO = issuedDetail.ItemNo;
                    ic0730.FROMLOC = issuedDetail.LocationFrom;
                    ic0730.TOLOC = issuedDetail.Location;
                    ic0730.QTYREQ = issuedDetail.QtyRequest;
                    ic0730.QUANTITY = issuedDetail.QtyProcess;
                    ic0730.UNITREQ = issuedDetail.UOM;
                    ic0730.UNIT = issuedDetail.UOM;
                    ic0730.COMMENTS = (string.IsNullOrEmpty(IssuedModel.Remarks) ? string.Empty : IssuedModel.Remarks);
                    ic0730.MANITEMNO = "M101";
                    ic0730.EXTWEIGHT = "200.0000";
                    ic0730.FUNCTION = "100";

                    ic0740.IC0730List.Add(ic0730);
                }

                API api = new API();
                api.BASE_URL = SiteSettings.ACCPAC_URL;
                response = api.Post(URI_ACCPAC.TRANSFER_SAVE, ic0740);

                if (response.IsSuccess)
                {
                    AccpacPostBaseModel AccpacPost = new AccpacPostBaseModel();
                    AccpacPost.DBName = ic0740.DBName;
                    AccpacPost.DBUsername = ic0740.DBUsername;
                    AccpacPost.DBPassword = ic0740.DBPassword;
                    AccpacPost.DOCNUM = ic0740.DOCNUM;

                    response = api.Post(URI_ACCPAC.TRANSFER_POST, AccpacPost);
                }
            }


            return response;
        }

        public static IC0740 GetAccpacModel(string TransferNo)
        {
            IC0740 ic0740 = new IC0740();

            MSRIssued transferDoc = MSRIssued.GetByIssuedNo(TransferNo);
            List<MSRDetail> MSRDetails = transferDoc.MSR.MSRDetails.ToList();

            Company company = Company.GetById(transferDoc.CompanyId);
            string DataSource = AccpacDataContext.GetDatabaseName(company.CompanyCode);
            ic0740.DBName = DataSource;
            ic0740.DBUsername = SiteSettings.ACCPAC_USERNAME;
            ic0740.DBPassword = SiteSettings.ACCPAC_PASSWORD;

            ic0740.DOCNUM = transferDoc.MSRIssuedNo;
            ic0740.TRANSDATE = transferDoc.CreatedDate;
            ic0740.DATEBUS = transferDoc.CreatedDate;
            ic0740.EXPARDATE = transferDoc.MSR.ExpectedDate;
            ic0740.HDRDESC = transferDoc.Remarks;
            ic0740.REFERENCE = string.Format("{0};{1};{2}", transferDoc.MSRIssuedNo, transferDoc.MSR.MSRNo, transferDoc.MSR.WorkOrderNo);
            ic0740.ADDCOST = "0";
            ic0740.PRORMETHOD = "3";
            ic0740.STATUS = "1";

            foreach (var issuedDetail in transferDoc.MSRIssuedDetails)
            {
                MSRDetail msrItem = MSRDetails.FirstOrDefault(x => x.LineNo == issuedDetail.LineNo && x.ItemNo == issuedDetail.ItemNo);

                IC0730 ic0730 = new IC0730();
                ic0730.LINENO = issuedDetail.LineNo;
                ic0730.ITEMNO = issuedDetail.ItemNo;
                ic0730.FROMLOC = issuedDetail.LocationIdFrom;
                ic0730.TOLOC = issuedDetail.LocationIdTo;
                ic0730.QTYREQ = msrItem.Qty;
                ic0730.QUANTITY = issuedDetail.Qty;
                ic0730.UNITREQ = msrItem.UOM;
                ic0730.UNIT = msrItem.UOM;
                ic0730.COMMENTS = (string.IsNullOrEmpty(transferDoc.Remarks) ? string.Empty : transferDoc.Remarks);
                ic0730.MANITEMNO = "M101";
                ic0730.EXTWEIGHT = "200.0000";
                ic0730.FUNCTION = "100";

                ic0740.IC0730List.Add(ic0730);
            }

            return ic0740;
        }
    }
}


