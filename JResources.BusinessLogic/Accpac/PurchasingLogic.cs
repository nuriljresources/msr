﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JResources.Common;
using JResources.Data;
using JResources.Data.Model;
using JResources.Data.Model.Accpac;

namespace JResources.BusinessLogic.Accpac
{
    public class PurchasingLogic
    {
        public static ResponseModel SaveRequisition(MSRIssuedModel IssuedModel)
        {
            ResponseModel response = new ResponseModel();

            PT0040 pt0040 = new PT0040();

            Company company = Company.GetById(IssuedModel.CompanyId);
            string DataSource = AccpacDataContext.GetDatabaseName(company.CompanyCode);
            pt0040.DBName = DataSource;
            pt0040.DBUsername = SiteSettings.RQN_USERNAME;
            pt0040.DBPassword = SiteSettings.RQN_PASSWORD;
            //pt0040.DOCNUM = IssuedModel.MSRIssuedNo;
            pt0040.DOCNUM = "";

            pt0040.WORKFLOW = IssuedModel.WorkFlowCode;
            pt0040.COSTCTR = IssuedModel.CostCenter;
            pt0040.RQRDDATE = DateTime.Now;
            pt0040.STCODE = "";
            pt0040.BTCODE = "";
            pt0040.DESCRIPTIO = IssuedModel.MSR.MaintenanceCodeLabel;
            pt0040.REFERENCE = string.Format("{0}|{1}", IssuedModel.MSR.DocumentNo, IssuedModel.MSRIssuedNo);
            pt0040.COMMENT = IssuedModel.Remarks;
            pt0040.HASJOB = 0;
            pt0040.OPTFIELD = "CEA";
            pt0040.SWSET = "1";
            pt0040.VALIFTEXT = "00";

            if (pt0040.RQRDDATE < IssuedModel.MSR.ExpectedDate)
            {
                pt0040.RQRDDATE = IssuedModel.MSR.ExpectedDate;
            }

            foreach (var detailItem in IssuedModel.MSRIssuedDetails)
            {
                PT0041 pt0041 = new PT0041();

                pt0041.FMTITEMNO = detailItem.ItemNo;
                pt0041.CONTRACT = "";
                pt0041.PROJECT = "";
                pt0041.CCATEGORY = "";
                pt0041.BILLRATE = "";
                pt0041.ARITEMNO = "";
                pt0041.ARUNIT = "";
                pt0041.LOCATION = detailItem.Location;
                pt0041.COMMENT = string.Format("RQNFROMMSR/{0}", IssuedModel.MSR.DocumentNo);
                pt0041.VDCODE = "";
                pt0041.REQQTY = detailItem.QtyProcess;
                pt0041.RQRDDATE = IssuedModel.MSR.ExpectedDate;
                pt0041.ORDERUNIT = detailItem.UOM;
                pt0041.UNITCOST = 0;
                pt0041.VENDITEMNO = "";

                pt0041.ISSTOCK = (!string.IsNullOrEmpty(detailItem.GLAccount) ? true : false);
                pt0041.GLACCTFULL = (!string.IsNullOrEmpty(detailItem.GLAccount) ? detailItem.GLAccount : string.Empty);
                pt0041.ISVENDOR = false;

                pt0041.IC0635List.Add(new IC0635() {
                    OPTFIELD = "CCCODE",
                    SWSET = "1",
                    VALIFTEXT = IssuedModel.MSR.CostCenter,
                });

                pt0041.IC0635List.Add(new IC0635()
                {
                    OPTFIELD = "IRFNO",
                    SWSET = "1",
                    VALIFTEXT = IssuedModel.MSR.DocumentNo,
                });

                pt0041.IC0635List.Add(new IC0635()
                {
                    OPTFIELD = "MPCODE",
                    SWSET = "1",
                    VALIFTEXT = "Other",
                });

                pt0040.PT0041List.Add(pt0041);
            }


            API api = new API();
            api.BASE_URL = SiteSettings.ACCPAC_URL;
            response = api.Post(URI_ACCPAC.PURCHASING_SAVE_REQUISITION, pt0040);

            if (response.IsSuccess)
            {
                string DOCNUM = response.ResponseObject.ToString();
                AccpacPostPurchaseModel AccpacPost = new AccpacPostPurchaseModel();
                AccpacPost.DBName = pt0040.DBName;
                AccpacPost.DBUsername = pt0040.DBUsername;
                AccpacPost.DBPassword = pt0040.DBPassword;
                AccpacPost.DOCNUM = DOCNUM;
                AccpacPost.WorkFlow = IssuedModel.WorkFlowCode;
                AccpacPost.Sequence = DOCNUM;
                AccpacPost.Comment = (string.IsNullOrEmpty(IssuedModel.Remarks) ? "" : IssuedModel.Remarks);
                AccpacPost.ApproveStatus = "1";

                api.IsTrigger = true;

                response = api.Post(URI_ACCPAC.PURCHASING_UPDATE_REQUISITION, AccpacPost);
                response.ResponseObject = DOCNUM;
            }


            return response;
        }

        public static PT0040 GetAccpacModel(string IssuedNo)
        {
            PT0040 pt0040 = new PT0040();
            MSRIssued issued = MSRIssued.GetByIssuedNo(IssuedNo);
            List<MSRDetail> MSRDetails = issued.MSR.MSRDetails.ToList();
            bool IsNonInventory = false;
            if (issued.MSR.RequestType == ((int)MSRType.NonInventory).ToString())
            {
                IsNonInventory = true;
            }

            Company company = Company.GetById(issued.CompanyId);
            string DataSource = AccpacDataContext.GetDatabaseName(company.CompanyCode);
            pt0040.DBName = DataSource;
            pt0040.DBUsername = SiteSettings.ACCPAC_USERNAME;
            pt0040.DBPassword = SiteSettings.ACCPAC_PASSWORD;
            //pt0040.DOCNUM = IssuedModel.MSRIssuedNo;
            pt0040.DOCNUM = "";

            pt0040.WORKFLOW = issued.WorkFlowCode;
            pt0040.COSTCTR = issued.CostCenter;
            pt0040.RQRDDATE = issued.CreatedDate;
            pt0040.STCODE = "";
            pt0040.BTCODE = "";
            pt0040.COMMENT = (string.IsNullOrEmpty(issued.Remarks) ? "" : issued.Remarks);
            if (!string.IsNullOrEmpty(issued.MSR.EquipmentNo))
            {
                pt0040.COMMENT = string.Format("{0}{1}Equipment : {2}", pt0040.COMMENT, Environment.NewLine, issued.MSR.EquipmentNo);
            }

            if (pt0040.COMMENT.Length > 60)
            {
                pt0040.DESCRIPTIO = pt0040.COMMENT.Substring(0, 59);
            }
            else
            {
                pt0040.DESCRIPTIO = pt0040.COMMENT;
            }

            pt0040.REFERENCE = string.Format("{0}|{1}|{2}", issued.MSR.MaintenanceCode.Trim(), issued.MSR.MSRNo, issued.MSRIssuedNo);
            pt0040.HASJOB = 0;
            pt0040.OPTFIELD = "CEA";
            pt0040.SWSET = "1";
            pt0040.VALIFTEXT = "00";

            if (pt0040.RQRDDATE < issued.MSR.ExpectedDate)
            {
                pt0040.RQRDDATE = issued.MSR.ExpectedDate;
            }

            foreach (var detailItem in issued.MSRIssuedDetails.OrderBy(x => x.LineNo))
            {
                MSRDetail msrItem = MSRDetails.FirstOrDefault(x => x.LineNo == detailItem.LineNo && x.ItemNo == detailItem.ItemNo);
                PT0041 pt0041 = new PT0041();

                pt0041.LineNo = detailItem.LineNo;
                pt0041.FMTITEMNO = detailItem.ItemNo;
                pt0041.CONTRACT = "";
                pt0041.PROJECT = "";
                pt0041.CCATEGORY = "";
                pt0041.BILLRATE = "";
                pt0041.ARITEMNO = "";
                pt0041.ARUNIT = "";
                pt0041.LOCATION = detailItem.LocationIdTo;
                pt0041.COMMENT = string.Format("RQNFROMMSR/{0}", issued.MSR.MSRNo);
                pt0041.VDCODE = "";
                pt0041.REQQTY = detailItem.Qty;
                pt0041.RQRDDATE = issued.MSR.ExpectedDate;
                pt0041.ORDERUNIT = msrItem.UOM;
                pt0041.UNITCOST = 0;
                pt0041.VENDITEMNO = "";

                string GLAccount = string.Empty;

                if (IsNonInventory && msrItem != null)
                {
                    if (!string.IsNullOrEmpty(msrItem.CostCode) && !msrItem.CostCode.Contains("&amp;") && msrItem.CostCode.Length > 5)
                    {
                        GLAccount = msrItem.CostCode.Trim().Replace("-", "");
                    } else
                    {
                        msrItem.CategoryCode = (String.IsNullOrEmpty(msrItem.CategoryCode) ? "" : msrItem.CategoryCode.Replace("-", ""));
                        if (msrItem.CategoryCode.Length > 5)
                        {
                            GLAccount = msrItem.CategoryCode;
                        } else
                        {
                            if(msrItem.CategoryCode.Trim().Length < 5)
                            {
                                MSR msr = MSR.GetById(msrItem.ID);
                                Department department = Department.GetById(msr.DepartmentId);
                                msrItem.CategoryCode = department.CategoryCode + msrItem.CategoryCode;
                            }

                            ICCATG iccatg = ICCATG.GetByCategoryNo(msrItem.CategoryCode);
                            if (iccatg != null)
                            {
                                GLAccount = iccatg.DAMAGEACCT;
                            }
                        }
                    }                    
                }
                else
                {
                    var icItem = AccpacDataContext.CurrentContext.ICITEMs.FirstOrDefault(x => x.ITEMNO.Contains(detailItem.ItemNo));
                    if (icItem != null)
                    {
                        var icACCT = AccpacDataContext.CurrentContext.ICACCTs.FirstOrDefault(x => x.CNTLACCT.Contains(icItem.CNTLACCT));
                        if (icACCT != null)
                        {
                            GLAccount = icACCT.INVACCT;
                        }
                    }
                }

                pt0041.ISSTOCK = (!string.IsNullOrEmpty(GLAccount) ? true : false);
                pt0041.GLACCTFULL = (!string.IsNullOrEmpty(GLAccount) ? GLAccount : string.Empty);
                pt0041.ISVENDOR = false;

                pt0041.IC0635List.Add(new IC0635()
                {
                    OPTFIELD = "CCCODE",
                    SWSET = "1",
                    VALIFTEXT = issued.MSR.CostCode,
                });

                pt0041.IC0635List.Add(new IC0635()
                {
                    OPTFIELD = "IRFNO",
                    SWSET = "1",
                    VALIFTEXT = issued.MSR.MSRNo,
                });

                pt0041.IC0635List.Add(new IC0635()
                {
                    OPTFIELD = "MPCODE",
                    SWSET = "1",
                    VALIFTEXT = "Other",
                });

                pt0040.PT0041List.Add(pt0041);
            }

            return pt0040;
        }

        public static PT0040 MappingCEARequisition(CEAModel cea)
        {
            PT0040 pt0040 = new PT0040();
            Company company = Company.GetByCode(cea.CompanyCode);
            string DataSource = AccpacDataContext.GetDatabaseName(company.CompanyCode);
            pt0040.DBName = DataSource;
            pt0040.DBUsername = SiteSettings.ACCPAC_USERNAME;
            pt0040.DBPassword = SiteSettings.ACCPAC_PASSWORD;
            pt0040.DOCNUM = "";

            pt0040.WORKFLOW = cea.WorkFlow;
            pt0040.COSTCTR = cea.CostCenter;
            pt0040.RQRDDATE = DateTime.Now;
            pt0040.STCODE = "";
            pt0040.BTCODE = "";
            pt0040.COMMENT = (string.IsNullOrEmpty(cea.Remarks) ? "" : cea.Remarks);

            pt0040.REFERENCE = cea.CEANo;
            pt0040.HASJOB = 0;
            pt0040.OPTFIELD = "CEA";
            pt0040.SWSET = "1";
            pt0040.VALIFTEXT = cea.CEANo;

            if (pt0040.COMMENT.Length > 60)
            {
                pt0040.DESCRIPTIO = pt0040.COMMENT.Substring(0, 59);
            }
            else
            {
                pt0040.DESCRIPTIO = pt0040.COMMENT;
            }


            foreach (var item in cea.CEAItems.OrderBy(x => x.LineNo))
            {
                PT0041 pt0041 = new PT0041();

                pt0041.LineNo = item.LineNo;
                pt0041.FMTITEMNO = item.ItemNo;
                pt0041.CONTRACT = "";
                pt0041.PROJECT = "";
                pt0041.CCATEGORY = "";
                pt0041.BILLRATE = "";
                pt0041.ARITEMNO = "";
                pt0041.ARUNIT = "";
                pt0041.LOCATION = item.Location;
                pt0041.COMMENT = string.Format("RQNFROMMSR/{0}", cea.CEANo);
                pt0041.VDCODE = "";
                pt0041.REQQTY = item.Qty;
                pt0041.RQRDDATE = pt0040.RQRDDATE;
                pt0041.ORDERUNIT = item.UOM;
                pt0041.UNITCOST = item.UnitCost;
                pt0041.VENDITEMNO = "";


                pt0041.ISSTOCK = true;
                pt0041.GLACCTFULL = item.GLAccount;
                pt0041.ISVENDOR = false;

                pt0041.IC0635List.Add(new IC0635()
                {
                    OPTFIELD = "CCCODE",
                    SWSET = "1",
                    VALIFTEXT = item.CCCODE,
                });

                pt0041.IC0635List.Add(new IC0635()
                {
                    OPTFIELD = "IRFNO",
                    SWSET = "1",
                    VALIFTEXT = "00",
                });

                pt0041.IC0635List.Add(new IC0635()
                {
                    OPTFIELD = "MPCODE",
                    SWSET = "1",
                    VALIFTEXT = "Other",
                });

                pt0040.PT0041List.Add(pt0041);
            }



            return pt0040;
        }
    }
}
