﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JResources.Data;
using JResources.Data.Model.Accpac;
using Omu.ValueInjecter;

namespace JResources.BusinessLogic.Accpac
{
    public class RQNLogic
    {
        public static PTPRHModel GetRQN(string RQNNumber)
        {
            var model = new PTPRHModel();

            var ptprh = PTPRH.GetByRQNNUMBER(RQNNumber);
            if (ptprh != null)
            {
                model.InjectFrom(ptprh);

                var ptprds = PTPRD.GetByRQNHSEQ(ptprh.RQNHSEQ);
                if (ptprds != null && ptprds.Count() > 0)
                {
                    foreach (var ptprd in ptprds)
                    {
                        PTPRDModel PTPRDItem = new PTPRDModel();
                        PTPRDItem.InjectFrom(ptprd);
                        if (!string.IsNullOrEmpty(ptprd.PONUMBER.Trim()))
                        {
                            POPORH1 poporh1 = POPORH1.GetByPONumber(ptprd.PONUMBER);
                            var poporls = POPORL.GetByPONumber(poporh1.PORHSEQ).Where(x => x.ITEMNO == ptprd.ITEMNO);
                            if (poporls != null && poporls.Count() > 0)
                            {
                                foreach (var poporl in poporls)
                                {
                                    POPORH1Model poporh11Item = new POPORH1Model();
                                    poporh11Item.InjectFrom(poporl);

                                    PTPRDItem.POPORH1s.Add(poporh11Item);
                                }                                
                            }
                        }

                        model.PTPRDs.Add(PTPRDItem);
                    }

                }
            }


            return model;
        }

    }
}
