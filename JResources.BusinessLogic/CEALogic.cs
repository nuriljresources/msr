﻿using System;
using System.Collections.Generic;
using System.Linq;
using JResources.Data;
using JResources.Data.Model;
using JResources.Common;
using JResources.Common.Enum;

namespace JResources.BusinessLogic
{
    public class CEALogic
    {/// <summary>
     /// Get List MSR
     /// </summary>
     /// <param name="model"></param>
     /// <returns></returns>
        public static CEASearchModel GetListCEA(CEASearchModel model)
        {
            model.SetDateRange();

            var CEAs = V_CEA_HEADER.GetAll();

            if (!string.IsNullOrEmpty(model.CEANo))
            {
                CEAs = CEAs.Where(x => x.cea_number.Contains(model.CEANo));
            }            

            var roleCookies = CookieManager.Get(COOKIES_NAME.ROLE);
            var user = CurrentUser.Model;
            CEAs = CEAs.Where(x => x.company == CurrentUser.COMPANY);

            user.SetRolesFromCookies(roleCookies);

            model.SetPager(CEAs.Count());
            List<V_CEA_HEADER> listData = CEAs.OrderByDescending(x => x.created_date)
                .Skip((model.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
                .Take(Common.SiteSettings.PAGE_SIZE_PAGING).ToList();

            List<CEA> _ceaMapping = new List<CEA>();
            if (listData != null && listData.Count > 0) _ceaMapping = CEA.GetByListId(listData.Select(x => Guid.Parse(x.sys_id)).ToList());
            DateTime DateNow = DateTime.Now.Date;
            foreach (var cea in listData)
            {
                var CEAList = new CEAListModel();
                CEAList.ID = Guid.Parse(cea.sys_id);
                CEAList.sys_id = cea.sys_id;
                CEAList.cer_draft_number = cea.cer_draft_number;
                CEAList.cer_number = cea.cer_number;
                CEAList.CERDate = cea.cer_date;
                CEAList.CEADate = cea.cea_date;
                CEAList.cea_number = cea.cea_number;
                CEAList.DateCreatedStr = cea.cea_date.HasValue ? cea.cea_date.Value.ToString(Constant.FORMAT_DATE_JRESOURCES) : "";
                CEAList.StatusRequestCode = CEAStatus.NEW;
                CEAList.Reason = cea.subject.Trim();
                var _ceaMappingItem = _ceaMapping.FirstOrDefault(x => x.ID == CEAList.ID);
                if(_ceaMappingItem != null)
                {
                    CEAList.StatusRequestCode = _ceaMappingItem.StatusCEA;
                    CEAList.IsSync = true;
                }

                CEAList.StatusRequestCode = CheckClosed(CEAList.StatusRequestCode, CEAList.CEADate);
                model.ListData.Add(CEAList);
            }

            return model;
        }

        public static CEAModel GetCEAModel(Guid CEAId)
        {
            CEAModel model = new CEAModel();
            CEA _cea = CEA.GetById(CEAId);

            if (_cea != null)
            {
                model.CEAId = CEAId;
                model.CEANo = _cea.CEANo;
                model.CEADate = _cea.CEADate;
                model.CEADateStr = _cea.CEADate.ToString(Constant.FORMAT_DATE_JRESOURCES);
                model.Status = _cea != null ? _cea.StatusCEA : CEAStatus.NEW;
                model.CreatedDateStr = DateTime.Now.ToString(Constant.FORMAT_DATE_JRESOURCES);
                model.RQNDate = DateTime.Now;
                model.CompanyCode = _cea.CompanyCode;
                model.Remarks = _cea.Remarks;
                model.CreatedBy = CurrentUser.NIKSITE;
                model.CreatedByCEA = _cea.CreatedBy;
            }

            CEALogic.SetStatusCEA(model);
            return model;
        }

        public static void CloseOutstandingCEA()
        {
            var V_CEA_HEADERs = V_CEA_HEADER.GetOutstanding(3).ToList();

            if (V_CEA_HEADERs != null && V_CEA_HEADERs.Count > 0)
            {
                foreach (var v_CEA_HEADER in V_CEA_HEADERs)
                {
                    CEA _cea = CEA.GetById(Guid.Parse(v_CEA_HEADER.sys_id));
                    if(_cea != null)
                    {
                        if (_cea.StatusCEA != CEAStatus.COMPLETED && _cea.StatusCEA != CEAStatus.CLOSED)
                        {
                            _cea.StatusCEA = CEAStatus.CLOSED;
                            _cea.UpdateSave<CEA>();
                        }
                    } else
                    {
                        _cea = new CEA();
                        _cea.ID = Guid.Parse(v_CEA_HEADER.sys_id);
                        _cea.CEANo = v_CEA_HEADER.cea_number.Trim();
                        _cea.StatusCEA = CEAStatus.CLOSED;
                        _cea.IsDeleted = false;
                        _cea.CreatedBy = "SYSTEM-SCHEDULER";
                        _cea.CreatedDate = DateTime.Now;
                        _cea.UpdatedBy = "SYSTEM-SCHEDULER";
                        _cea.UpdatedDate = DateTime.Now;
                        _cea.Save<CEA>();
                    }
                }
            }
        }

        public static void ReminderOutstandingCEA()
        {
            var CEAs = V_CEA_HEADER.GetOutstanding(2).ToList();

            if (CEAs != null && CEAs.Count > 0)
            {
                var CEANumbers = CEAs.Select(x => Guid.Parse(x.sys_id)).ToList();
                foreach (var CEA in CEAs)
                {
                    CEAModel ceaModel = GetCEAModel(Guid.Parse(CEA.sys_id));
                    if (ceaModel.Status != CEAStatus.COMPLETED && ceaModel.Status != CEAStatus.CLOSED)
                    {
                        EmailLogic.ReminderCEA(ceaModel);
                    }
                }
            }
        }

        public static void SetStatusCEA(CEAModel model)
        {
            bool IsCompleted = true;
            var Requisitions = model.History.Where(x => x.RequisitionType == REQUISITION_TYPE.PURCHASE);
            var Contracts = model.History.Where(x => x.RequisitionType == REQUISITION_TYPE.CONTRACT);

            List<CEARequistionItemModel> CEARequistionItems = new List<CEARequistionItemModel>();
            List<CEARequistionItemModel> CEAContractItems = new List<CEARequistionItemModel>();

            foreach (var item in Requisitions)
            {
                foreach (var itemCEA in item.Items)
                {
                    CEARequistionItems.Add(itemCEA);
                }
            }

            foreach (var item in Contracts)
            {
                foreach (var itemCEA in item.Items)
                {
                    CEAContractItems.Add(itemCEA);
                }
            }

            foreach (var item in model.CEAItems)
            {
                item.QtyOutstanding = item.QtyRequest;
                var itemSum = CEARequistionItems.Where(x => x.LineNo == item.LineNo).ToList();
                if(itemSum != null && itemSum.Count > 0)
                {
                    item.QtyOutstanding = item.QtyRequest - itemSum.Sum(x => x.Qty);
                    item.IsCompleted = item.QtyOutstanding == 0 ? true : false;
                }

                var itemContract = CEAContractItems.Where(x => x.LineNo == item.LineNo).ToList();
                if(itemContract != null && itemContract.Count > 0)
                {
                    item.IsExclude = true;
                    item.IsContract = true;
                    item.IsCompleted = true;
                }

                if (item.QtyOutstanding > 0) IsCompleted = false;
            }

            foreach (var item in model.CEAAdditionalItems)
            {
                var itemSum = CEARequistionItems.Where(x => x.LineNo == item.LineNo).ToList();
                if (itemSum != null && itemSum.Count > 0)
                {
                    item.QtyOutstanding = item.QtyRequest - itemSum.Sum(x => x.Qty);
                    item.IsCompleted = item.QtyOutstanding == 0 ? true : false;
                }
            }

            model.Status = IsCompleted ? CEAStatus.COMPLETED : CEAStatus.IN_PROGRESS;
            model.Status = CheckClosed(model.Status, model.CEADate);

            if(CEARequistionItems != null && CEARequistionItems.Count > 0)
            {
                model.TotalAmountRQN += CEARequistionItems.Sum(x => x.Qty * x.UnitCost);
            }

            //if (model.CEAAdditionalItems != null && model.CEAAdditionalItems.Count > 0)
            //{
            //    model.TotalAmountRQN += model.CEAAdditionalItems.Sum(x => x.Qty * x.UnitCost);
            //}
        }

        public static List<CEARequistionModel> GetRequistion(Guid CEAId)
        {
            var _list = new List<CEARequistionModel>();
            var requistions = CEARequisition.GetByCEAId(CEAId);
            if(requistions != null && requistions.Count() > 0)
            {
                foreach (var requistion in requistions)
                {
                    CEARequistionModel model = new CEARequistionModel();
                    model.RequisitionNo = requistion.RequisitionNo;
                    model.RequisitionDate = requistion.RequisitionDate.ToString(Constant.FORMAT_DATE_JRESOURCES);
                    model.WorkFlowName = requistion.WorkFlow;
                    model.RequisitionType = requistion.RequisitionNo.Contains("RQN") ? REQUISITION_TYPE.PURCHASE : REQUISITION_TYPE.CONTRACT;

                    foreach (var requisitionItem in requistion.CEARequisitionDetails)
                    {
                        CEARequistionItemModel item = new CEARequistionItemModel();
                        item.LineNo = requisitionItem.LineNumber;
                        item.ItemNo = requisitionItem.ItemNo;
                        item.ItemDescription = requisitionItem.ItemDescription;
                        item.ItemType = requisitionItem.ItemType;
                        item.Qty = requisitionItem.Qty;
                        item.UnitCost = requisitionItem.UnitCost;
                        if(!requisitionItem.IsDeleted)
                        {
                            model.Items.Add(item);

                            if (requisitionItem.ItemType == REQUISITION_TYPE.CONTRACT)
                                model.RequisitionType = REQUISITION_TYPE.CONTRACT;
                        }
                        
                    }

                    _list.Add(model);
                }
            }

            return _list;
        }

        public static string CheckClosed(string CurrentStatus, DateTime? DateApprove)
        {
            if (DateApprove.HasValue && CurrentStatus != CEAStatus.COMPLETED)
            {
                int months = (DateApprove.Value.Year - DateTime.Now.Year) * 12 + DateApprove.Value.Month - DateTime.Now.Month;
                if (months >= 3)
                {
                    return CEAStatus.CLOSED;
                }
            }

            return CurrentStatus;
        }
    }
}
