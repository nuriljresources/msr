﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JResources.Data.Model;
using JResources.Common;
using JResources.Data;
using JResources.Common.Enum;
using System.Net;
using System.Net.Mail;
using System.IO;
using NPOI.SS.Util;

namespace JResources.BusinessLogic
{
    public class EmailLogic
    {
        public static string PathEmailTemplate(string EmailCode)
        {
            return Path.Combine(SiteSettings.EMAIL_TEMPLATE_PATH, string.Format("{0}.html", EmailCode));
        }

        public static void EmailMSR(MSRModel MSRModel)
        {
            if (!SiteSettings.IS_EMAIL_ACTIVE)
                return;

            EmailModel model = new EmailModel();
            decimal TotalAll = 0;
            var Body = File.ReadAllText(PathEmailTemplate(EmailType.MSR_NEW));

            model.From = SiteSettings.EMAIL_USERNAME;
            model.FromName = "noreply@jresources.com";
            model.Subject = string.Format("MSR {0} NEED APPROVE", MSRModel.DocumentNo);

            StringBuilder sbItemList = new StringBuilder();
            var MSRDetails = MSRModel.MSRDetail.Where(x => !string.IsNullOrEmpty(x.ItemNo));
            foreach (var item in MSRDetails)
            {
                string rowTable = @"
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <table width='100%' border='0' cellspacing='0' cellpadding='0' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'><tr><td height='8' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'>&nbsp;</td></tr></table>

                                <div class='text' style='color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left'>{0}</div>
                                <table width='100%' border='0' cellspacing='0' cellpadding='0' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'><tr><td height='8' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'>&nbsp;</td></tr></table>

                            </td>
                            <td>
                                <table width='100%' border='0' cellspacing='0' cellpadding='0' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'><tr><td height='8' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'>&nbsp;</td></tr></table>

                                <div class='text' style='color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left'>{1}</div>
                                <table width='100%' border='0' cellspacing='0' cellpadding='0' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'><tr><td height='8' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'>&nbsp;</td></tr></table>

                            </td>
                            <td>&nbsp;</td>
                            <td>
                                <table width='100%' border='0' cellspacing='0' cellpadding='0' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'><tr><td height='8' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'>&nbsp;</td></tr></table>

                                <div class='text' style='color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left'>{2}</div>
                                <table width='100%' border='0' cellspacing='0' cellpadding='0' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'><tr><td height='8' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'>&nbsp;</td></tr></table>

                            </td>
                            <td>&nbsp;</td>
                            <td>
                                <table width='100%' border='0' cellspacing='0' cellpadding='0' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'><tr><td height='8' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'>&nbsp;</td></tr></table>

                                <div class='text-center' style='color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:right'>{3}</div>
                                <table width='100%' border='0' cellspacing='0' cellpadding='0' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'><tr><td height='8' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'>&nbsp;</td></tr></table>
                            </td>
                            <td>&nbsp;</td>
                        </tr>";

                rowTable = string.Format(rowTable, item.ItemNo, item.Description, item.Qty, item.Cost.ToString("#.##"));
                sbItemList.Append(rowTable);

                TotalAll += item.Qty * item.Cost;
            }

            string urlCode = URLShortenLogic.CreateMSRApprove(MSRModel);

            model.Body = Body
                .Replace("[MSR_NO]", MSRModel.DocumentNo)
                .Replace("[MSR_COST_CENTER]", MSRModel.DocumentNo)
                .Replace("[MSR_URL]", string.Format("{0}/Shorten/{1}", SiteSettings.BASE_URL, urlCode))
                .Replace("[MSR_REQUESTOR]", MSRModel.Requestor.FullName)
                .Replace("[MSR_COMPANY]", MSRModel.CompanyName)
                .Replace("[MSR_REASON]", (string.IsNullOrEmpty(MSRModel.Reason) ? "-" : MSRModel.Reason))
                .Replace("[MSR_DATE_EX]", MSRModel.ExpectedDateStr)
                .Replace("[MSR_ITEM_LIST]", sbItemList.ToString())
                .Replace("[MSR_TOTALALL]", TotalAll.ToString("#.##"));


            var userList = RoleLogic.GetAllUserByRoleAndDepartmentId(ROLE_CODE.MSR_APPROVAL, MSRModel.DepartmentId);
            foreach (var item in userList)
            {
                model.EmailAddress.Add(new EmailAddressModel()
                {
                    EmailTo = item.Email,
                    EmailToName = item.FullName
                });
            }

            //model.EmailAddress.Add(new EmailAddressModel()
            //{
            //    EmailTo = "nuril.umam@jresources.com",
            //    EmailToName = "Nuril Umam"
            //});

            SendEmail(model);
        }

        public static void EmailMSRRevision(MSRModel MSRModel)
        {
            EmailModel model = new EmailModel();
            decimal TotalAll = 0;
            var Body = File.ReadAllText(PathEmailTemplate(EmailType.MSR_REVISION));

            model.From = SiteSettings.EMAIL_USERNAME;
            model.FromName = "noreply@jresources.com";
            model.Subject = string.Format("MSR {0} NEED APPROVE", MSRModel.DocumentNo);

            StringBuilder sbItemList = new StringBuilder();
            var MSRDetails = MSRModel.MSRDetail.Where(x => !string.IsNullOrEmpty(x.ItemNo));
            string urlCode = URLShortenLogic.CreateMSRevision(MSRModel);

            model.Body = Body
                .Replace("[MSR_NO]", MSRModel.DocumentNo)
                .Replace("[MSR_COST_CENTER]", MSRModel.DocumentNo)
                .Replace("[MSR_URL]", string.Format("{0}/Shorten/{1}", SiteSettings.BASE_URL, urlCode))
                .Replace("[MSR_REQUESTOR]", MSRModel.Requestor.FullName)
                .Replace("[MSR_COMPANY]", MSRModel.CompanyName)
                .Replace("[MSR_REASON]", (string.IsNullOrEmpty(MSRModel.Reason) ? "-" : MSRModel.Reason))
                .Replace("[MSR_DATE_EX]", MSRModel.ExpectedDateStr)
                .Replace("[MSR_ITEM_LIST]", sbItemList.ToString())
                .Replace("[MSR_REASON]", MSRModel.Reason)
                .Replace("[MSR_TOTALALL]", TotalAll.ToString("#.##"));


            var msr = MSR.GetById(MSRModel.ID);
            var user = UserLogic.GetByNIK(msr.CreatedBy);

            if (!string.IsNullOrEmpty(user.Email))
            {
                model.EmailAddress.Add(new EmailAddressModel()
                {
                    EmailTo = user.Email,
                    EmailToName = user.FullName
                });
            }


            model.EmailAddress.Add(new EmailAddressModel()
            {
                EmailTo = "nuril.umam@jresources.com",
                EmailToName = "Nuril Umam"
            });

            SendEmail(model);
        }

        public static void EmailMSRIssued(MSRModel MSRModel)
        {
            if (!SiteSettings.IS_EMAIL_ACTIVE)
                return;

            EmailModel model = new EmailModel();
            decimal TotalAll = 0;

            var Body = File.ReadAllText(PathEmailTemplate(EmailType.MSR_APPROVED));

            model.From = SiteSettings.EMAIL_USERNAME;
            model.FromName = "noreply@jresources.com";
            model.Subject = string.Format("MSR {0} NEED ISSUED", MSRModel.DocumentNo);

            StringBuilder sbItemList = new StringBuilder();

            foreach (var item in MSRModel.MSRDetail)
            {
                string rowTable = @"
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <table width='100%' border='0' cellspacing='0' cellpadding='0' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'><tr><td height='8' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'>&nbsp;</td></tr></table>

                                <div class='text' style='color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left'>{0}</div>
                                <table width='100%' border='0' cellspacing='0' cellpadding='0' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'><tr><td height='8' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'>&nbsp;</td></tr></table>

                            </td>
                            <td>
                                <table width='100%' border='0' cellspacing='0' cellpadding='0' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'><tr><td height='8' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'>&nbsp;</td></tr></table>

                                <div class='text' style='color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left'>{1}</div>
                                <table width='100%' border='0' cellspacing='0' cellpadding='0' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'><tr><td height='8' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'>&nbsp;</td></tr></table>

                            </td>
                            <td>&nbsp;</td>
                            <td>
                                <table width='100%' border='0' cellspacing='0' cellpadding='0' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'><tr><td height='8' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'>&nbsp;</td></tr></table>

                                <div class='text' style='color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left'>{2}</div>
                                <table width='100%' border='0' cellspacing='0' cellpadding='0' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'><tr><td height='8' class='spacer' style='font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%'>&nbsp;</td></tr></table>

                            </td>
                        </tr>";

                rowTable = string.Format(rowTable, item.ItemNo, item.Description, item.Qty, item.Cost.ToString("#.##"));
                sbItemList.Append(rowTable);

                TotalAll += item.Qty * item.Cost;
            }

            string urlCode = URLShortenLogic.CreateMSRIssued(MSRModel);

            model.Body = Body
                .Replace("[MSR_NO]", MSRModel.DocumentNo)
                .Replace("[MSR_COST_CENTER]", MSRModel.DocumentNo)
                .Replace("[MSR_URL]", string.Format("{0}/Shorten/{1}", SiteSettings.BASE_URL, urlCode))
                .Replace("[MSR_REQUESTOR]", MSRModel.Requestor.FullName)
                .Replace("[MSR_APPROVAL]", MSRModel.Manager.FullName)
                .Replace("[MSR_COMPANY]", MSRModel.CompanyName)
                .Replace("[MSR_REASON]", (string.IsNullOrEmpty(MSRModel.Reason) ? "-" : MSRModel.Reason))
                .Replace("[MSR_DATE_EX]", MSRModel.ExpectedDateStr)
                .Replace("[MSR_ITEM_LIST]", sbItemList.ToString());


            var userList = RoleLogic.GetAllUserByRoleAndDepartmentId(ROLE_CODE.MSR_ISSUED, MSRModel.DepartmentId);
            //foreach (var item in userList)
            //{
            //    model.EmailAddress.Add(new EmailAddressModel()
            //    {
            //        EmailTo = item.Email,
            //        EmailToName = item.FullName
            //    });
            //}

            model.EmailAddress.Add(new EmailAddressModel()
            {
                EmailTo = "nuril.umam@jresources.com",
                EmailToName = "Nuril Umam"
            });

            SendEmail(model);
        }

        public static ResponseModel EmailReport(string fileName, string _reportType, string NIKSite, DateTime dateFrom, DateTime dateTo)
        {
            EmailModel model = new EmailModel();

            var Body = File.ReadAllText(PathEmailTemplate(EmailType.REPORT));

            model.From = SiteSettings.EMAIL_USERNAME;
            model.FromName = "noreply@jresources.com";
            model.Subject = string.Format("REPORT {0} DONE PROCESS", _reportType.ToUpper());

            string urlCode = URLShortenLogic.CreateReportTest(fileName);

            model.Body = Body
                .Replace("[REPORT_TYPE]", _reportType)
                .Replace("[URL_DOWNLOAD]", string.Format("{0}/Shorten/{1}", SiteSettings.BASE_URL, urlCode))
                .Replace("[URL_DOWNLOAD_LABEL]", string.Format("{0}/Shorten/{1}", SiteSettings.BASE_URL, urlCode))
                .Replace("[REPORT_DATE]", string.Format("{0} - {1}", dateFrom.ToString(Constant.FORMAT_DATE_JRESOURCES), dateTo.ToString(Constant.FORMAT_DATE_JRESOURCES)));

            var user = UserLogic.GetByNIK(NIKSite);
            model.EmailAddress.Add(new EmailAddressModel()
            {
                EmailTo = user.Email,
                EmailToName = user.FullName
            });

            return SendEmail(model);
        }

        public static ResponseModel EmailFailedError(string SchedulerType, string NIKSite, string errorMessage)
        {
            EmailModel model = new EmailModel();

            model.From = SiteSettings.EMAIL_USERNAME;
            model.FromName = "noreply@jresources.com";
            model.Subject = string.Format("SCHEDULER {0} ERROR PROCESS", SchedulerType);

            StringBuilder sbBody = new StringBuilder();
            sbBody.AppendLine("Error Scheduler Proses For : " + SchedulerType + " <br /> <br />");
            sbBody.AppendLine("Message Error : <br />");
            sbBody.AppendLine(errorMessage);

            model.Body = sbBody.ToString();
            var user = UserLogic.GetByNIK(NIKSite);
            model.EmailAddress.Add(new EmailAddressModel()
            {
                EmailTo = user.Email,
                EmailToName = user.FullName
            });

            return SendEmail(model);
        }

        public static void ReminderCEA(CEAModel CEAModel)
        {
            EmailModel model = new EmailModel();

            model.From = SiteSettings.EMAIL_USERNAME;
            model.FromName = "noreply@jresources.com";
            model.Subject = string.Format("CEA REMINDER {0}", CEAModel.CEANo);
            model.Body = string.Format("<p>Dear,<br />CEA dengan nomor {0} belum selesai atau belum Completed.</p><p>Mohon agar dapat disegera di proses, sebelum masuk pada tanggal {1}, karena akan otomatis status CEA menjadi Closed.<br />Terima Kasih</p>", CEAModel.CEANo, DateTime.Now.AddMonths(1).ToString(Constant.FORMAT_DATE_JRESOURCES));

            var UserMail = CurrentDataContext.CurrentContext.ESS_USER_EMAIL(CEAModel.CreatedByCEA).FirstOrDefault();
            if(UserMail != null)
            {
                
            }


            //foreach (var item in userList)
            //{
            //    model.EmailAddress.Add(new EmailAddressModel()
            //    {
            //        EmailTo = item.Email,
            //        EmailToName = item.FullName
            //    });
            //}

            //model.EmailAddress.Add(new EmailAddressModel()
            //{
            //    EmailTo = "nuril.umam@jresources.com",
            //    EmailToName = "Nuril Umam"
            //});

            SendEmail(model);
        }
        public static ResponseModel SendEmail(EmailModel model)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                var fromAddress = new MailAddress(model.From, model.FromName);
                
                var smtp = new SmtpClient
                {
                    Host = SiteSettings.EMAIL_SMTP,
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(SiteSettings.EMAIL_USERNAME, SiteSettings.EMAIL_PASSWORD)
                };

                var message = new MailMessage();
                message.IsBodyHtml = true;
                message.From = fromAddress;
                foreach (var item in model.EmailAddress)
                {
                    message.To.Add(new MailAddress(item.EmailTo, item.EmailToName));
                }                

                message.Subject = model.Subject;
                message.Body = model.Body;
                if (SiteSettings.IS_EMAIL_ACTIVE)
                {
                    smtp.Send(message);
                    Console.WriteLine("SUCCESS TO EMAIL");
                }

                response.SetSuccess();
            }
            catch (Exception ex)
            {
                response.SetError(ex.Message);
                Console.WriteLine("ERROR EMAIL : " + ex.Message);
            }

            return response;
        }
    }
}
