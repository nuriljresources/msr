﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using JResources.Common;
using JResources.Common.Enum;
using JResources.Data;
using JResources.Data.Model;

namespace JResources.BusinessLogic
{
    public class CurrencyLogic
    {
        public static CurrencySearchModel GetCurrencyList(CurrencySearchModel search)
        {
            List<CurrencyModel> list = new List<CurrencyModel>();           
            IQueryable<vw_CBE_Currency> currency = CurrentDataContext.CurrentContext.vw_CBE_Currency;

            if (!string.IsNullOrEmpty(search.CurrencyName))
            {
                currency = currency.Where(x => x.currtyp.Contains(search.CurrencyName));
            }

            search.SetPager(currency.Count());
            List<vw_CBE_Currency> listCurrency = new List<vw_CBE_Currency>();

            if (!search.IsGetAllItem)
            {
                listCurrency = currency.OrderBy(x => x.currtyp).Skip((search.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
                .Take(Common.SiteSettings.PAGE_SIZE_PAGING).ToList();
            }
            else
            {
                listCurrency = currency.ToList();
            }

            List<string> CurrencyNoList = new List<string>();
            if (listCurrency != null & listCurrency.Count > 0)
            {
                CurrencyNoList = listCurrency.Select(x => x.currtyp).ToList();
            }

            
            foreach (var item in listCurrency)
            {
                CurrencyModel msrCurrency = new CurrencyModel();
                msrCurrency.CurrencyCode = item.currtyp;
                msrCurrency.CurrencyName = item.currtyp;

                list.Add(msrCurrency);
            }

            search.ListData = list.OrderBy(x => x.CurrencyCode).Take(Common.SiteSettings.PAGE_SIZE_PAGING).ToList();

            return search;
        }
        public static ItemPartModel GetByItemNo(string ItemNo)
        {
            ItemPartModel itemPart = new ItemPartModel();

            var ICITEM = AccpacDataContext.CurrentContext.ICITEMs.FirstOrDefault(x => x.ITEMNO == ItemNo);
            if (ICITEM != null)
            {
                itemPart.ItemNo = ICITEM.ITEMNO;
                itemPart.Description = ICITEM.DESC;
                itemPart.Category = ICITEM.CATEGORY;
                itemPart.CategoryLabel = ICITEM.CATEGORY;
                itemPart.UOM = ICITEM.STOCKUNIT;

                var itemParts = new List<ItemPartModel>();
                itemParts.Add(itemPart);
                ItemLogic.SetLocationItem(itemParts);
                itemPart = itemParts.FirstOrDefault();
            }
            return itemPart;
        }
    }
}
