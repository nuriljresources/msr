﻿using JResources.Common;
using JResources.Data;
using JResources.Data.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace JResources.BusinessLogic
{
    public class BudgetLogic
    {
        public BudgetLogic(int year, string costCenter)
        {
            Year = year;
            CostCenter = costCenter;
        }
        public int Year { get; set; }
        public string CostCenter { get; set; }

        public BudgetMatrixModel GetBudget()
        {
            BudgetMatrixModel model = new BudgetMatrixModel();
            model.CompanyCode = CurrentUser.GetCurrentCompany().CompanyCode;
            model.Year = Year;
            model.CostCenter = CostCenter;

            switch (CurrentUser.GetCurrentCompany().CompanyCode)
            {
                case COMPANY_CODE.SPP:
                    model.CompanyCode = "SER";
                    break;
                case COMPANY_CODE.BAKAN:
                    model.CompanyCode = "BAK";
                    break;
                case COMPANY_CODE.SRSB:
                    model.CompanyCode = "PEN";
                    break;
                case COMPANY_CODE.LANUT:
                    model.CompanyCode = "LAN";
                    break;
                case COMPANY_CODE.ASA:
                    model.CompanyCode = "ASA";
                    break;
                case COMPANY_CODE.GSM:
                    model.CompanyCode = "PAN";
                    break;
            }

            ADOHelper ConnectionCheck = new ADOHelper(ConfigurationManager.ConnectionStrings[Constant.JOBS_DATABSE].ConnectionString);
            SqlDataReader reader = ConnectionCheck.ExecDataReaderProc("sp_getMatrixBudgetCostCenter"
                , "Year", model.Year
                , "CompanyCode", model.CompanyCode
                , "CostCenter", model.CostCenter);

            while (reader.Read())
            {
                model.Details.Add(new BudgetMatrixDetailModel() {
                    ItemCategory = reader.GetString(0),
                    ItemCode = reader.GetString(1),
                    Qty = reader.GetDecimal(2),
                    Price = reader.GetDecimal(3),
                    Total = reader.GetDecimal(4)
                });
            }


            return model;
        }

    }
}
