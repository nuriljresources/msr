﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JResources.Common.Enum;
using JResources.Data;
using JResources.Data.Model;
using Newtonsoft.Json;

namespace JResources.BusinessLogic
{
    public class URLShortenLogic
    {
        public static string CreateMSRApprove(MSRModel model)
        {
            URLShorten urlShorten = new URLShorten();
            urlShorten.ID = Guid.NewGuid();
            urlShorten.UrlCode = urlShorten.ID.ToString().Replace("-", "");
            urlShorten.UrlType = URLShortenType.MSR_NEW;
            urlShorten.DataModel = model.DocumentNo;
            urlShorten.Save<URLShorten>();

            return urlShorten.UrlCode;
        }

        public static string CreateMSRIssued(MSRModel model)
        {
            URLShorten urlShorten = new URLShorten();
            urlShorten.ID = Guid.NewGuid();
            urlShorten.UrlCode = urlShorten.ID.ToString().Replace("-", "");
            urlShorten.UrlType = URLShortenType.MSR_ISSUED;
            urlShorten.DataModel = model.DocumentNo;
            urlShorten.Save<URLShorten>();

            return urlShorten.UrlCode;
        }

        public static string CreateMSRevision(MSRModel model)
        {
            URLShorten urlShorten = new URLShorten();
            urlShorten.ID = Guid.NewGuid();
            urlShorten.UrlCode = urlShorten.ID.ToString().Replace("-", "");
            urlShorten.UrlType = URLShortenType.MSR_REVISION;
            urlShorten.DataModel = model.DocumentNo;
            urlShorten.Save<URLShorten>();

            return urlShorten.UrlCode;
        }

        public static string CreateReportTest(string DocumentName)
        {
            URLShorten urlShorten = new URLShorten();
            urlShorten.ID = Guid.NewGuid();
            urlShorten.UrlCode = urlShorten.ID.ToString().Split('-')[0];
            urlShorten.UrlType = URLShortenType.REPORT;
            urlShorten.DataModel = DocumentName;
            urlShorten.Save<URLShorten>();

            return urlShorten.UrlCode;
        }
    }
}
