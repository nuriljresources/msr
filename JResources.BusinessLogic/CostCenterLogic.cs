﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using JResources.Data;

namespace JResources.BusinessLogic
{
    public class CostCenterLogic
    {
        public static List<SelectListItem> GetCostCenterByDepartmentId(Guid departmentId, string equipmentCodeSelected = "")
        {
            List<SelectListItem> list = new List<SelectListItem>();
            List<CSOPTFD> CostCenterList = CSOPTFD.GetCostCenter().ToList();

            if (CostCenterList != null && CostCenterList.Count > 0)
            {
                if (departmentId != Guid.Empty)
                {
                    var department = CompanyLogic.GetDepartmentById(departmentId);
                    if (department != null)
                    {
                        foreach (var item in CostCenterList)
                        {
                            SelectListItem listItem = new SelectListItem();
                            listItem.Value = item.VALUE.Trim();
                            if(!string.IsNullOrEmpty(item.VDESC))
                                listItem.Text = string.Format("{0}-{1}", listItem.Value, Regex.Replace(item.VDESC.Trim(), "(\\B[A-Z])", " $1"));
                            

                            if (item.VALUE == equipmentCodeSelected)
                            {
                                listItem.Selected = true;
                            }

                            if (department.CostCenterCode.Count > 0)
                            {
                                foreach (var coa in department.CostCenterCode)
                                {
                                    if (item.VALUE.Contains(coa))
                                    {
                                        list.Add(listItem);
                                    }
                                }
                            }
                        }
                    }
                }

            }


            return list;
        }

        
    }
}
