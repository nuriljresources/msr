﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using JResources.Data;
using JResources.Data.Model;
using Omu.ValueInjecter;


namespace JResources.BusinessLogic
{
    public class CompanyLogic
    {
        public static List<CompanyDepartmentModel> GetFullCompanyWithDepartment()
        {
            List<CompanyDepartmentModel> list = new List<CompanyDepartmentModel>();
            var companies = Company.GetAll().ToList();
            foreach (var company in companies)
            {
                CompanyDepartmentModel companyModel = new CompanyDepartmentModel();
                companyModel.InjectFrom(company);
                var deparments = Department.GetByCompanyId(company.ID).ToList();
                foreach (var deparment in deparments)
                {
                    DepartmentModel departmentModel = new DepartmentModel();
                    departmentModel.InjectFrom(deparment);

                    companyModel.Departments.Add(departmentModel);
                }

                list.Add(companyModel);
            }

            return list;
        }

        public static List<CompanyDepartmentModel> GetFullCompanyWithDepartmentByCurrentRole(string[] ROLE_CODES)
        {
            List<CompanyDepartmentModel> list = new List<CompanyDepartmentModel>();

            var userLogin = UserLogic.GetByNIK(CurrentUser.NIKSITE);
            var roleAccessUser = RoleAccessUser.GetByDepartmentList(userLogin.Username, ROLE_CODES);

            if (roleAccessUser != null)
            {
                List<Guid> listCompanyId = roleAccessUser.Select(x => x.CompanyId).Distinct().ToList();
                List<Guid> listDepartmentId = roleAccessUser.Select(x => x.DepartmentId).ToList();

                var Companies = Company.GetAll().Where(x => listCompanyId.Contains(x.ID)).ToList();
                var Departments = Department.GetAll().Where(x => listDepartmentId.Contains(x.ID)).ToList();

                foreach (var company in Companies)
                {
                    CompanyDepartmentModel companyModel = new CompanyDepartmentModel();
                    companyModel.InjectFrom(company);

                    var deparments = Departments.Where(x => x.CompanyId == company.ID);
                    foreach (var deparment in deparments)
                    {
                        DepartmentModel departmentModel = new DepartmentModel();
                        departmentModel.InjectFrom(deparment);

                        companyModel.Departments.Add(departmentModel);
                    }

                    list.Add(companyModel);
                }
            }

            return list;
        }

        public static List<SelectListItem> GetDepartmentListByCurrentCompany(string departmentId = "")
        {
            var list = new List<SelectListItem>();
            var listDepartments = Department.GetByCurrentCompany();
            foreach (var department in listDepartments)
            {
                bool IsSelected = false;
                if (!string.IsNullOrEmpty(departmentId) && department.ID.ToString() == departmentId)
                {
                    IsSelected = true;
                }

                list.Add(new SelectListItem()
                {
                    Text = department.DepartmentName,
                    Value = department.ID.ToString(),
                    Selected = IsSelected
                });
            }

            return list;
        }

        public static DepartmentModel GetDepartmentById(Guid Id)
        {
            var department = Department.GetById(Id);
            if (department != null)
            {
                DepartmentModel model = new DepartmentModel();
                model.InjectFrom(department);

                return model;
            }

            return null;
        }

        public static List<Company> GetListCompanyByUserId(Guid UserId)
        {
            List<Guid> companyIds = new List<Guid>();
            var user = UserLogin.GetById(UserId);
            if (user != null && user.Department != null)
            {
                companyIds.Add(user.Department.CompanyId);
            }

            var RoleAccessUses = RoleAccessUser.GetAll().Where(x => x.UserId == UserId).ToList();
            if (RoleAccessUses != null && RoleAccessUses.Count > 0)
            {
                companyIds.AddRange(RoleAccessUses.Select(x => x.CompanyId).Distinct().ToList());
            }

            return Company.GetAll().Where(x => companyIds.Contains(x.ID)).Distinct().ToList();
        }
    }
}
