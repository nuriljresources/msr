﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using JResources.Common;
using JResources.Common.Enum;
using JResources.Data;
using JResources.Data.Model;

namespace JResources.BusinessLogic
{
    public class SupplierLogic
    {
        public static SupplierSearchModel GetSupplierList(SupplierSearchModel search)
        {
            List<MSRSupplier> list = new List<MSRSupplier>();           
            IQueryable<vw_CBE_Vendor> supplier = CurrentDataContext.CurrentContext.vw_CBE_Vendor.Where(x => x.compid == CurrentUser.COMPANY);
            IQueryable<CBE_MsSupplier> cbesupplier = CurrentDataContext.CurrentContext.CBE_MsSupplier.Where(x => x.IsAccpacSync == false);

            if (!string.IsNullOrEmpty(search.SupplierCode))
            {
                supplier = supplier.Where(x => x.VENDORID.Contains(search.SupplierCode));
            }

            if (!string.IsNullOrEmpty(search.SupplierName))
            {
                supplier = supplier.Where(x => x.VENDNAME.Contains(search.SupplierName));
                cbesupplier = cbesupplier.Where(x => x.SupplierName.Contains(search.SupplierName));
            }


            search.SetPager(supplier.Count());
            List<vw_CBE_Vendor> listSupplier = new List<vw_CBE_Vendor>();
            List<CBE_MsSupplier> listCBESupplier = new List<CBE_MsSupplier>();

            if (!search.IsGetAllItem)
            {
                listSupplier = supplier.OrderBy(x => x.VENDNAME).Skip((search.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
                .Take(Common.SiteSettings.PAGE_SIZE_PAGING).ToList();

                listCBESupplier = cbesupplier.OrderBy(x => x.SupplierName).Skip((search.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
                .Take(Common.SiteSettings.PAGE_SIZE_PAGING).ToList();
            }
            else
            {
                listSupplier = supplier.ToList();

                listCBESupplier = cbesupplier.ToList();
            }

            List<string> SupplierNoList = new List<string>();
            if (listSupplier != null & listSupplier.Count > 0)
            {
                SupplierNoList = listSupplier.Select(x => x.VENDORID).ToList();
            }

            
            foreach (var item in listSupplier)
            {
                MSRSupplier msrSupplier = new MSRSupplier();
                msrSupplier.SupplierCode = item.VENDORID;
                msrSupplier.SupplierName = item.VENDNAME;
                msrSupplier.Currency = item.CURNCODE;

                list.Add(msrSupplier);
            }

            foreach (var item in listCBESupplier)
            {
                MSRSupplier msrSupplier = new MSRSupplier();
                msrSupplier.SupplierCode = item.SupplierCode;
                msrSupplier.SupplierName = item.SupplierName;
                msrSupplier.Currency = "NOT FOUND";

                list.Add(msrSupplier);
            }

            search.ListData = list.OrderBy(x => x.SupplierName).Take(Common.SiteSettings.PAGE_SIZE_PAGING).ToList();

            return search;
        }
        public static ItemPartModel GetByItemNo(string ItemNo)
        {
            ItemPartModel itemPart = new ItemPartModel();

            var ICITEM = AccpacDataContext.CurrentContext.ICITEMs.FirstOrDefault(x => x.ITEMNO == ItemNo);
            if (ICITEM != null)
            {
                itemPart.ItemNo = ICITEM.ITEMNO;
                itemPart.Description = ICITEM.DESC;
                itemPart.Category = ICITEM.CATEGORY;
                itemPart.CategoryLabel = ICITEM.CATEGORY;
                itemPart.UOM = ICITEM.STOCKUNIT;

                var itemParts = new List<ItemPartModel>();
                itemParts.Add(itemPart);
                ItemLogic.SetLocationItem(itemParts);
                itemPart = itemParts.FirstOrDefault();
            }
            return itemPart;
        }
    }
}
