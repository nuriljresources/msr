﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using JResources.Common;
using JResources.Data;
using JResources.Data.Model;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace JResources.BusinessLogic
{
    public class PSTPlanningLogic
    {
        public UploadDocumentModel model { get; set; }
        public ResponseModel response { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }

        public ResponseModel SavePlanning()
        {
            response = new ResponseModel();

            try
            {
                ValidationModel();

                #region CHECK ITEM ON ACCPAC
                if (response.IsSuccess)
                {
                    foreach (var item in model.DocumentDetails)
                    {
                        var icItem = ICITEM.GetByItemNo(item.ItemNo);
                        if (icItem == null)
                        {
                            response.SetError(string.Format("Item No {0} not found", item.ItemNo));
                            break;
                        }
                        else
                        {
                            var iciLOC = ICILOC.GetByItemNoAndLocation(item.ItemNo, item.StorageLocation);
                            if (iciLOC != null)
                            {
                                item.ItemDescription = string.IsNullOrEmpty(icItem.DESC) ? "" : icItem.DESC.Trim();
                                item.BookedQty = iciLOC.QTYONHAND;
                                item.UOM = string.IsNullOrEmpty(icItem.STOCKUNIT) ? "" : icItem.STOCKUNIT.Trim();
                            }
                            else
                            {
                                response.SetError(string.Format("Item No {0} With location {1} not found", item.ItemNo, item.StorageLocation));
                                break;
                            }
                            
                        }
                    }
                }
                #endregion

                if(response.IsSuccess)
                    InsertToDatabase();
            }
            catch (Exception ex)
            {
                response.SetError(ex.Message);
            }

            return response;
        }

        public void ValidationModel()
        {
            #region VALIDATION MODEL
            DateStart = model.DocumentDetails.OrderBy(x => x.PSTDate).FirstOrDefault().PSTDate;
            DateEnd = model.DocumentDetails.OrderByDescending(x => x.PSTDate).FirstOrDefault().PSTDate;

            PSTPlanning checkPlanning = PSTPlanning.GetByRangeDate(DateStart, DateEnd);
            if (checkPlanning != null)
            {
                response.SetError("Period of {0} until {1} already exists {2} With Document No {3}",
                    checkPlanning.DateFrom.ToString(Constant.FORMAT_DATE_JRESOURCES_TEXT), checkPlanning.DateTo.ToString(Constant.FORMAT_DATE_JRESOURCES_TEXT), Environment.NewLine, checkPlanning.PlanningNo);
            }
            else
            {
                int rangeDay = (int)DateEnd.Subtract(DateStart).TotalDays;

                for (int i = 0; i < rangeDay; i++)
                {
                    DateTime dtCheck = DateStart.AddDays(i);
                    UploadDetailModel checkDetail = model.DocumentDetails
                        .FirstOrDefault(x => x.PSTDate.Date == dtCheck.Date);

                    if (checkDetail == null)
                    {
                        response.SetError(
                            string.Format("Date {0} is not missed in the excel list ",
                            dtCheck.ToString(Constant.FORMAT_DATE_JRESOURCES)));

                        break;
                    }
                }
            }
            #endregion
        }

        public void InsertToDatabase()
        {
            PSTPlanning PlanningHeader = new PSTPlanning()
            {
                ID = Guid.NewGuid(),
                PlanningNo = DocumentNumberLogic.GetPSTPlanning(model.CompanyId),
                CompanyId = model.CompanyId,
                DateFrom = DateStart,
                DateTo = DateEnd,
                CreatedBy = CurrentUser.USERNAME,
                CreatedDate = DateTime.Now,
                UpdatedBy = CurrentUser.USERNAME,
                UpdatedDate = DateTime.Now,
                PSTPlanningDetails = new System.Data.Objects.DataClasses.EntityCollection<PSTPlanningDetail>()
            };

            foreach (var item in model.DocumentDetails)
            {
                PlanningHeader.PSTPlanningDetails.Add(
                    new PSTPlanningDetail()
                    {
                        ID = Guid.NewGuid(),
                        PSTPlanningId = PlanningHeader.ID,
                        ItemNo = item.ItemNo,
                        ItemDescription = item.ItemDescription,
                        Location = item.StorageLocation,
                        Bin = item.StorageBin,
                        PlanningDate = item.PSTDate,
                        UOM = item.UOM,
                        BookedQty = item.BookedQty,
                        CreatedBy = CurrentUser.USERNAME,
                        CreatedDate = DateTime.Now,
                        UpdatedBy = CurrentUser.USERNAME,
                        UpdatedDate = DateTime.Now
                    });
            }

            PlanningHeader.Save<PSTPlanning>();
            response.ResponseObject = PlanningHeader.ID;
            response.SetSuccess(string.Format("PST Planning No {0} has been saved", PlanningHeader.PlanningNo));
        }

    }
}
