﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JResources.Common;
using JResources.Data;

namespace JResources.BusinessLogic
{
    public class DocumentNumberLogic
    {
        /// <summary>
        /// Create DocumentNo If Not Exits
        /// </summary>
        /// <param name="DocumentType"></param>
        /// <param name="CompanyId"></param>
        /// <param name="DepartmentId"></param>
        /// <returns></returns>
        public static DocumentNumber CreateDocument(string DocumentType, Guid CompanyId, Guid DepartmentId)
        {
            DocumentNumber newDocument = new DocumentNumber();
            newDocument.ID = Guid.NewGuid();
            newDocument.DocumentType = DocumentType;
            newDocument.CompanyId = CompanyId;
            newDocument.Year = DateTime.Now.Year;
            newDocument.Month = DateTime.Now.Month;
            newDocument.RunningNumber = 1;

            if (DepartmentId != Guid.Empty)
            {
                newDocument.DepartmentId = DepartmentId;
            }

            newDocument.Save<DocumentNumber>();
            return newDocument;
        }

        public static string GetMSRDocumentNo(Guid CompanyId)
        {
            DocumentNumber documentNo = DocumentNumber.GetDocumentNoByTypeYear(DocumentType.MSR, CompanyId);
            if (documentNo != null)
            {
                documentNo.RunningNumber = documentNo.RunningNumber + 1;
                //documentNo.UpdateSave<DocumentNumber>();
            }
            else
            {
                documentNo = CreateDocument(DocumentType.MSR, CompanyId, Guid.Empty);
            }

            string documentCode = documentNo.Company.CompanyCode;
            if (!string.IsNullOrEmpty(documentNo.Company.DocumentCode))
            {
                documentCode = documentNo.Company.DocumentCode;
            }

            string MSRNo = string.Format("MSR-{0}/{1}/{2}", documentCode, documentNo.Year, documentNo.RunningNumber.ToString("D4"));
            
            bool SearchNo = true;
            while (SearchNo)
            {
                MSR msr = MSR.GetByNo(MSRNo);
                if (msr != null)
                {
                    documentNo.RunningNumber = documentNo.RunningNumber + 1;
                    MSRNo = string.Format("MSR-{0}/{1}/{2}", documentCode, documentNo.Year, documentNo.RunningNumber.ToString("D4"));
                }
                else
                {
                    SearchNo = false;
                }
            }

            if (documentNo.RunningNumber > 1)
                documentNo.UpdateSave<DocumentNumber>();

            return MSRNo;
        }

        public static string GetReplenishmentDocumentNo(Guid CompanyId)
        {
            DocumentNumber documentNo = DocumentNumber.GetDocumentNoByTypeYear(DocumentType.MSR_ROP, CompanyId);
            if (documentNo != null)
            {
                documentNo.RunningNumber = documentNo.RunningNumber + 1;
                //documentNo.UpdateSave<DocumentNumber>();
            }
            else
            {
                documentNo = CreateDocument(DocumentType.MSR_ROP, CompanyId, Guid.Empty);
            }

            string documentCode = documentNo.Company.CompanyCode;
            if (!string.IsNullOrEmpty(documentNo.Company.DocumentCode))
            {
                documentCode = documentNo.Company.DocumentCode;
            }

            string MSRNo = string.Format("SRO-{0}/{1}/{2}", documentCode, documentNo.Year, documentNo.RunningNumber.ToString("D4"));
            
            bool SearchNo = true;
            while (SearchNo)
            {
                MSR msr = MSR.GetByNo(MSRNo);
                if (msr != null)
                {
                    documentNo.RunningNumber = documentNo.RunningNumber + 1;
                    MSRNo = string.Format("SRO-{0}/{1}/{2}", documentCode, documentNo.Year, documentNo.RunningNumber.ToString("D4"));
                }
                else
                {
                    SearchNo = false;
                }
            }

            if (documentNo.RunningNumber > 1)
                documentNo.UpdateSave<DocumentNumber>();

            return MSRNo;
        }

        public static string GetMSRIssuedNo(Guid CompanyId)
        {
            DocumentNumber documentNo = DocumentNumber.GetDocumentNoByTypeYear(DocumentType.ISSUED, CompanyId);
            if (documentNo != null)
            {
                documentNo.RunningNumber = documentNo.RunningNumber + 1;
            }
            else
            {
                documentNo = CreateDocument(DocumentType.ISSUED, CompanyId, Guid.Empty);
            }

            string documentCode = documentNo.Company.CompanyCode;
            if (!string.IsNullOrEmpty(documentNo.Company.DocumentCode))
            {
                documentCode = documentNo.Company.DocumentCode;
            }

            string IssuedNo = string.Format("ISS-{0}/{1}/{2}", documentCode, documentNo.Year, documentNo.RunningNumber.ToString("D4"));

            bool SearchNo = true;
            while (SearchNo)
            {
                MSRIssued msr = MSRIssued.GetByIssuedNo(IssuedNo);
                if (msr != null)
                {
                    documentNo.RunningNumber = documentNo.RunningNumber + 1;
                    IssuedNo = string.Format("ISS-{0}/{1}/{2}", documentCode, documentNo.Year, documentNo.RunningNumber.ToString("D4"));
                }
                else
                {
                    SearchNo = false;
                }
            }

            if (documentNo.RunningNumber > 1)
                documentNo.UpdateSave<DocumentNumber>();

            return IssuedNo;
        }

        public static string GetMSRPurchaseNo(Guid CompanyId)
        {
            DocumentNumber documentNo = DocumentNumber.GetDocumentNoByTypeYear(DocumentType.PURCHASE, CompanyId);
            if (documentNo != null)
            {
                documentNo.RunningNumber = documentNo.RunningNumber + 1;
                //documentNo.UpdateSave<DocumentNumber>();
            }
            else
            {
                documentNo = CreateDocument(DocumentType.PURCHASE, CompanyId, Guid.Empty);
            }

            string documentCode = documentNo.Company.CompanyCode;
            if (!string.IsNullOrEmpty(documentNo.Company.DocumentCode))
            {
                documentCode = documentNo.Company.DocumentCode;
            }

            string PurchaseNo = string.Format("PRC-{0}/{1}/{2}", documentCode, documentNo.Year, documentNo.RunningNumber.ToString("D4"));
            
            bool SearchNo = true;
            while (SearchNo)
            {
                MSRIssued issued = MSRIssued.GetByIssuedNo(PurchaseNo);
                if (issued != null)
                {
                    documentNo.RunningNumber = documentNo.RunningNumber + 1;
                    PurchaseNo = string.Format("PRC-{0}/{1}/{2}", documentCode, documentNo.Year, documentNo.RunningNumber.ToString("D4"));
                }
                else
                {
                    SearchNo = false;
                }
            }

            if (documentNo.RunningNumber > 1)
                documentNo.UpdateSave<DocumentNumber>();

            return PurchaseNo;
        }

        public static string GetMSRTransferNo(Guid CompanyId)
        {
            DocumentNumber documentNo = DocumentNumber.GetDocumentNoByTypeYear(DocumentType.TRANSFER, CompanyId);
            if (documentNo != null)
            {
                documentNo.RunningNumber = documentNo.RunningNumber + 1;
            }
            else
            {
                documentNo = CreateDocument(DocumentType.TRANSFER, CompanyId, Guid.Empty);
            }

            string documentCode = documentNo.Company.CompanyCode;
            if (!string.IsNullOrEmpty(documentNo.Company.DocumentCode))
            {
                documentCode = documentNo.Company.DocumentCode;
            }

            string TransferNo = string.Format("TRF-{0}/{1}/{2}", documentCode, documentNo.Year, documentNo.RunningNumber.ToString("D4"));

            bool SearchNo = true;
            while (SearchNo)
            {
                MSRIssued msr = MSRIssued.GetByIssuedNo(TransferNo);
                if (msr != null)
                {
                    documentNo.RunningNumber = documentNo.RunningNumber + 1;
                    TransferNo = string.Format("TRF-{0}/{1}/{2}", documentCode, documentNo.Year, documentNo.RunningNumber.ToString("D4"));
                }
                else
                {
                    SearchNo = false;
                }
            }

            if (documentNo.RunningNumber > 1)
                documentNo.UpdateSave<DocumentNumber>();

            return TransferNo;
        }

        public static string GetMSRReverseTransferNo(Guid CompanyId)
        {
            DocumentNumber documentNo = DocumentNumber.GetDocumentNoByTypeYear(DocumentType.REVERSE, CompanyId);
            if (documentNo != null)
            {
                documentNo.RunningNumber = documentNo.RunningNumber + 1;
                documentNo.UpdateSave<DocumentNumber>();
            }
            else
            {
                documentNo = CreateDocument(DocumentType.REVERSE, CompanyId, Guid.Empty);
            }

            string documentCode = documentNo.Company.CompanyCode;
            if (!string.IsNullOrEmpty(documentNo.Company.DocumentCode))
            {
                documentCode = documentNo.Company.DocumentCode;
            }

            return string.Format("RVS-{0}/{1}/{2}", documentCode, documentNo.Year, documentNo.RunningNumber.ToString("D4"));
        }

        public static string GetMSRCancelNo(Guid CompanyId)
        {
            DocumentNumber documentNo = DocumentNumber.GetDocumentNoByTypeYear(DocumentType.CANCEL, CompanyId);
            if (documentNo != null)
            {
                documentNo.RunningNumber = documentNo.RunningNumber + 1;
                documentNo.UpdateSave<DocumentNumber>();
            }
            else
            {
                documentNo = CreateDocument(DocumentType.CANCEL, CompanyId, Guid.Empty);
            }


            string documentCode = documentNo.Company.CompanyCode;
            if (!string.IsNullOrEmpty(documentNo.Company.DocumentCode))
            {
                documentCode = documentNo.Company.DocumentCode;
            }

            string cancelNo = string.Format("CNL-{0}/{1}/{2}", documentCode, documentNo.Year, documentNo.RunningNumber.ToString("D4"));
            
            bool SearchNo = true;
            while (SearchNo)
            {
                MSRIssued issued = MSRIssued.GetByIssuedNo(cancelNo);
                if (issued != null)
                {
                    documentNo.RunningNumber = documentNo.RunningNumber + 1;
                    cancelNo = string.Format("CNL-{0}/{1}/{2}", documentCode, documentNo.Year, documentNo.RunningNumber.ToString("D4"));
                }
                else
                {
                    SearchNo = false;
                }
            }

            if (documentNo.RunningNumber > 1)
                documentNo.UpdateSave<DocumentNumber>();

            return cancelNo;
        }

        public static string GetFRFDocumentNo(Guid CompanyId)
        {
            string MSRNo = string.Empty;
            DocumentNumber documentNo = DocumentNumber.GetDocumentNoByTypeYear(DocumentType.FRF, CompanyId);
            if (documentNo != null)
            {
                documentNo.RunningNumber = documentNo.RunningNumber + 1;
                documentNo.UpdateSave<DocumentNumber>();
            }
            else
            {
                documentNo = CreateDocument(DocumentType.FRF, CompanyId, Guid.Empty);
            }

            string documentCode = documentNo.Company.CompanyCode;
            if (!string.IsNullOrEmpty(documentNo.Company.DocumentCode))
            {
                documentCode = documentNo.Company.DocumentCode;
            }

            return string.Format("FRF-{0}/{1}/{2}", documentCode, documentNo.Year, documentNo.RunningNumber.ToString("D4"));
        }

        public static string GetContractCEANo(Guid CompanyId)
        {
            DocumentNumber documentNo = DocumentNumber.GetDocumentNoByTypeYear(DocumentType.CONTRACT, CompanyId);
            if (documentNo != null)
            {
                documentNo.RunningNumber = documentNo.RunningNumber + 1;
                //documentNo.UpdateSave<DocumentNumber>();
            }
            else
            {
                documentNo = CreateDocument(DocumentType.CONTRACT, CompanyId, Guid.Empty);
            }

            string documentCode = documentNo.Company.CompanyCode;
            if (!string.IsNullOrEmpty(documentNo.Company.DocumentCode))
            {
                documentCode = documentNo.Company.DocumentCode;
            }

            string ContractNo = string.Format("CTC-{0}/{1}/{2}", documentCode, documentNo.Year, documentNo.RunningNumber.ToString("D4"));

            bool SearchNo = true;
            while (SearchNo)
            {
                CEARequisition issued = CEARequisition.GetByRQNNo(ContractNo);
                if (issued != null)
                {
                    documentNo.RunningNumber = documentNo.RunningNumber + 1;
                    ContractNo = string.Format("CTC-{0}/{1}/{2}", documentCode, documentNo.Year, documentNo.RunningNumber.ToString("D4"));
                }
                else
                {
                    SearchNo = false;
                }
            }

            if (documentNo.RunningNumber > 1)
                documentNo.UpdateSave<DocumentNumber>();

            return ContractNo;
        }

        public static string GetCBENo(Guid CompanyId)
        {
            DocumentNumber documentNo = DocumentNumber.GetDocumentNoByTypeYear(DocumentType.CBE, CompanyId);
            if (documentNo != null)
            {
                documentNo.RunningNumber = documentNo.RunningNumber + 1;
                //documentNo.UpdateSave<DocumentNumber>();
            }
            else
            {
                documentNo = CreateDocument(DocumentType.CBE, CompanyId, Guid.Empty);
            }

            string documentCode = documentNo.Company.CompanyCode;
            if (!string.IsNullOrEmpty(documentNo.Company.DocumentCode))
            {
                documentCode = documentNo.Company.DocumentCode;
            }

            string CBENo = string.Format("CBE-{0}/{1}/{2}", documentCode, documentNo.Year, documentNo.RunningNumber.ToString("D4"));

            bool SearchNo = true;
            while (SearchNo)
            {
                CBE issued = CBE.GetByNo(CBENo);
                if (issued != null)
                {
                    documentNo.RunningNumber = documentNo.RunningNumber + 1;
                    CBENo = string.Format("CBE-{0}/{1}/{2}", documentCode, documentNo.Year, documentNo.RunningNumber.ToString("D4"));
                }
                else
                {
                    SearchNo = false;
                }
            }

            if (documentNo.RunningNumber > 1)
                documentNo.UpdateSave<DocumentNumber>();

            return CBENo;
        }

        public static string GetPSTPlanning(Guid CompanyId)
        {
            string DocType = DocumentType.PST_PLANNING;
            DocumentNumber documentNo = DocumentNumber.GetDocumentNoByTypeYear(DocType, CompanyId);
            if (documentNo != null)
            {
                documentNo.RunningNumber = documentNo.RunningNumber + 1;
                documentNo.UpdateSave<DocumentNumber>();
            }
            else
            {
                documentNo = CreateDocument(DocType, CompanyId, Guid.Empty);
            }

            string documentCode = documentNo.Company.CompanyCode;
            if (!string.IsNullOrEmpty(documentNo.Company.DocumentCode))
            {
                documentCode = documentNo.Company.DocumentCode;
            }

            return string.Format("{0}-UPL-{1}-{2}", documentCode, DateTime.Now.ToString("yy"), documentNo.RunningNumber.ToString("D4"));
        }

        public static string GetPSTNo(Guid CompanyId, string _PSTType)
        {
            string DocType = string.Empty;
            string pstCodeDocument = string.Empty;

            switch (_PSTType)
            {
                case PSTType.SCHEDULE:
                    DocType = DocumentType.PST_SCHEDULE;
                    pstCodeDocument = "SCH";
                    break;

                case PSTType.TRANSACTED:
                    DocType = DocumentType.PST_TRANSACTED;
                    pstCodeDocument = "TRI";
                    break;

                case PSTType.RANDOM :
                    DocType = DocumentType.PST_RANDOM;
                    pstCodeDocument = "RND";
                    break;
            }

            DocumentNumber documentNo = DocumentNumber.GetDocumentNoByTypeYear(DocType, CompanyId);
            if (documentNo != null)
            {
                documentNo.RunningNumber = documentNo.RunningNumber + 1;
                documentNo.UpdateSave<DocumentNumber>();
            }
            else
            {
                documentNo = CreateDocument(DocType, CompanyId, Guid.Empty);
            }

            string documentCode = documentNo.Company.CompanyCode;
            if (!string.IsNullOrEmpty(documentNo.Company.DocumentCode))
            {
                documentCode = documentNo.Company.DocumentCode;
            }

            return string.Format("{0}-{1}-{2}-{3}", documentCode, pstCodeDocument, DateTime.Now.ToString("yy"), documentNo.RunningNumber.ToString("D4"));
        }
    }
}
