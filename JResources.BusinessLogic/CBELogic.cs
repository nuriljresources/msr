﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web.Mvc;
using JResources.BusinessLogic.Accpac;
using JResources.Common;
using JResources.Common.Enum;
using JResources.Data;
using JResources.Data.Model;
using JResources.Data.Model.Accpac;

using Omu.ValueInjecter;

namespace JResources.BusinessLogic
{
    public class CBELogic
    {
        /// <summary>
        /// Save Issued
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static ResponseModel AddSupplierCBE(MSRSupplier model)
        {
            ResponseModel response = new ResponseModel();

            if (response.IsSuccess)
            {
                try
                {
                    CBE_MsSupplier newCBESupplier = CurrentDataContext.CurrentContext.CBE_MsSupplier.FirstOrDefault(x => x.SupplierName == model.AddSupplierName.Trim());

                    if (newCBESupplier == null)
                    {
                        Guid ID = Guid.NewGuid();
                        CBE_MsSupplier cbe_mssupplier = new CBE_MsSupplier();
                        
                        cbe_mssupplier.ID = ID;
                        cbe_mssupplier.SupplierCode = ID.ToString().Split('-')[0];
                        cbe_mssupplier.SupplierName = model.AddSupplierName;
                        cbe_mssupplier.ContactPerson = model.AddSupplierContactPerson;
                        cbe_mssupplier.Address = model.AddSupplierAddress;
                        cbe_mssupplier.TermOfPayment = model.AddSupplierPayment;
                        cbe_mssupplier.IsAccpacSync = false;

                        cbe_mssupplier.Save<CBE_MsSupplier>();
                    }
                    else
                    {
                        response.Message = "Supplier already exist";
                        response.IsSuccess = false;

                    }
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null && ex.InnerException.Message != null)
                    {
                        response.AddMessageList(ex.InnerException.Message);
                    }
                    else
                    {
                        if (ex.Message != null)
                        {
                            response.AddMessageList(ex.Message);
                        }
                    }

                    response.MessageToString();
                    response.SetError();
                }
            }

            return response;
        }
        public static ResponseModel CreateCBE(CBEModel model)
        {
            ResponseModel response = new ResponseModel();

            response.IsSuccess = true;
            response = ValidationCBE(model);
            string Process = string.Empty;

            if (response.IsSuccess)
            {
                model.MSR = MSRLogic.GetMSRModel(model.MSRId);
                var IssuedTypeLabel = EnumTable.GetDescriptionByCodeValue(EnumTable.EnumType.STATUS_MSR, "CBE").EnumLabel;

                VM0050 vm0050 = new VM0050();
                if (!string.IsNullOrEmpty(model.MSR.WorkOrderNo))
                {
                    vm0050 = WorkOrderLogic.GetWorkOrderAccpac(model.MSR.WorkOrderNo);
                }

                string RQNNumber = string.Empty;

                try
                {

                    #region DOCUMENT NUMBER RUNNING
                    Process = "DOCUMENT NUMBER RUNNING";
                    model.CBENo = DocumentNumberLogic.GetCBENo(model.CompanyId);
                    #endregion

                    #region INITIAL DOCUMENT CBE HEADER
                    Process = "INITIAL DOCUMENT CBE HEADER";

                    CBE newCBE = new CBE();
                    newCBE.ID = Guid.NewGuid();
                    newCBE.CompanyId = model.CompanyId;
                    newCBE.DepartmentId = model.DepartmentId;
                    newCBE.MSRId = model.MSRId;
                    newCBE.CBENo = model.CBENo;
                    newCBE.DocumentStatus = CBEStatus.DRAFT;
                    newCBE.CostCenter = model.RequestorCostCenter;
                    newCBE.WorkFlowCode = model.WorkFlowCode;
                    newCBE.Reason = model.Remarks == null ? "" : model.Remarks;
                    newCBE.TehnicalSummary = model.TechnicalSummary;
                    newCBE.ApprovalSchema = GetSchema(model);
                    newCBE.IsDeleted = false;

                    #endregion

                    #region INITIAL DOCUMENT CBE DETAIL ITEM

                    CBEBid newCBEBid = new CBEBid();

                    newCBE.CBEBids = new System.Data.Objects.DataClasses.EntityCollection<CBEBid>();

                    CBESummarySupplier supplier = new CBESummarySupplier();
                    List<CBESummarySupplier> Listsuppliers = new List<CBESummarySupplier>();

                    foreach (var item in model.MSRIssuedDetails)
                    {
                        Process = "INITIAL DOCUMENT CBE ITEMS";

                        newCBEBid = new CBEBid();

                        newCBEBid.ID = Guid.NewGuid();
                        newCBEBid.CBEid = newCBE.ID;
                        newCBEBid.LineNo = item.LineNo;
                        newCBEBid.Itemno = item.ItemNo;
                        newCBEBid.UoM = item.UOM;
                        newCBEBid.Description = item.Description;
                        newCBEBid.LocationCode = item.Location;
                        newCBEBid.QtyRequest = item.QtyPurchaseOutstanding;
                        newCBEBid.IsDeleted = false;

                        EntityObjectExtension.SetPropertyUpdated(newCBEBid, true);

                        newCBEBid.CBEBidSuppliers = new System.Data.Objects.DataClasses.EntityCollection<CBEBidSupplier>();

                        Process = "INITIAL DOCUMENT CBE OFFERING ITEMS";
                        foreach (var Bid in item.CBEBidSuppliers)
                        {
                            CBEBidSupplier newCBEBidSupplier = new CBEBidSupplier();

                            if (Bid.Supplier != null)
                            {
                                newCBEBidSupplier.ID = Guid.NewGuid();
                                newCBEBidSupplier.CBEId = newCBE.ID;
                                newCBEBidSupplier.CBEBidId = newCBEBid.ID;
                                newCBEBidSupplier.SupplierCode = Bid.Supplier;
                                newCBEBidSupplier.Qty = Bid.Qty;
                                newCBEBidSupplier.Currency = Bid.Currency;
                                newCBEBidSupplier.UnitPrice = Bid.UnitPrice;
                                newCBEBidSupplier.TotalPrice = Bid.TotalPrice;
                                newCBEBidSupplier.TotalPrice_USD = Bid.TotalPrice_USD;
                                newCBEBidSupplier.Rate = Bid.Rate;
                                newCBEBidSupplier.SetAsWinner = Bid.SetAsWinner != true ? false : Bid.SetAsWinner;
                                newCBEBidSupplier.Remark = Bid.Remark;

                                EntityObjectExtension.SetPropertyUpdated(newCBEBidSupplier, true);

                                newCBEBid.CBEBidSuppliers.Add(newCBEBidSupplier);

                                supplier = new CBESummarySupplier();

                                supplier.Supplier = Bid.Supplier;
                                supplier.TotalBeforeDiscount = Bid.TotalPrice_USD;

                                var LowestQtyOffering = item.CBEBidSuppliers.Min(x => x.Qty);
                                supplier.GT_Requirement = LowestQtyOffering * Bid.TotalPrice_USD;
                                supplier.IsWinner = Bid.SetAsWinner != true ? 0 : 1;

                                Listsuppliers.Add(supplier);
                            }
                        }

                        newCBE.CBEBids.Add(newCBEBid);
                    }

                    var SupplierGroup = Listsuppliers.GroupBy(d => d.Supplier)
                        .Select(
                        g => new
                        {
                            key = g.Key,
                            beforediscount = g.Sum(s => s.TotalBeforeDiscount),
                            requirement = g.Sum(s => s.GT_Requirement),
                            winner = g.Max(s => s.IsWinner)
                        });

                    newCBE.CBESummaries = new System.Data.Objects.DataClasses.EntityCollection<CBESummary>();
                    newCBE.CBEBidSupplierWinners = new System.Data.Objects.DataClasses.EntityCollection<CBEBidSupplierWinner>();

                    foreach (var Supplier in SupplierGroup)
                    {
                        Process = "INITIAL DOCUMENT CBE SUMMARY ITEMS";
                        CBESummary newCBESummary = new CBESummary();
                        CBEBidSupplierWinner newCBEWinner = new CBEBidSupplierWinner();

                        Guid Id = Guid.NewGuid();

                        newCBESummary.ID = Id;
                        newCBESummary.CBEId = newCBE.ID;
                        newCBESummary.SupplierCode = Supplier.key;
                        newCBESummary.TotalBeforeDiscount = Supplier.beforediscount;
                        newCBESummary.GT_Requirement = Supplier.requirement;
                        newCBESummary.IsWinner = Supplier.winner == 1 ? true : false;

                        newCBE.CBESummaries.Add(newCBESummary);

                        Process = "INITIAL DOCUMENT CBE WINNERS ITEMS";
                        newCBEWinner.ID = Id;
                        newCBEWinner.CBEId = newCBE.ID;
                        newCBEWinner.SupplierCode = Supplier.key;

                        newCBE.CBEBidSupplierWinners.Add(newCBEWinner);
                    }

                    #endregion

                    #region INITIAL DOCUMENT CBE UPLOAD DOCUMENTS

                    CBEDocument newCBEDocument = new CBEDocument();

                    newCBE.CBEDocuments = new System.Data.Objects.DataClasses.EntityCollection<CBEDocument>();

                    Process = "INITIAL DOCUMENT CBE UPLOAD DOCUMENTS";

                    foreach (var document in model.CBEDocumentDetails)
                    {
                        newCBEDocument = new CBEDocument();

                        newCBEDocument.Id = Guid.NewGuid();
                        newCBEDocument.CBEId = newCBE.ID;
                        newCBEDocument.FilePath = document.uriFile;
                        newCBEDocument.FileName = document.fileName;
                        newCBEDocument.FileSize = document.fileSize;
                        newCBEDocument.Type = "01";

                        newCBE.CBEDocuments.Add(newCBEDocument);
                    }

                    #endregion

                    #region DOCUMENT ISSUED FINISH
                    Process = "DOCUMENT ISSUED FINISH";

                    if (response.IsSuccess)
                    {
                        if (!string.IsNullOrEmpty(RQNNumber))
                        {
                            newCBE.RQNNumber = RQNNumber;
                        }

                        newCBE.Save<CBE>();
                        response.AddMessageList(string.Format("CBE NO : {0} has been created", newCBE.CBENo));
                        response.SetSuccess();
                    }
                    else
                    {
                        response.AddMessageList(response.Message);
                        response.AddMessageList("Error When Submit To Accpac");
                        response.MessageToString();
                        response.SetError();
                    }
                    #endregion

                }
                catch (Exception ex)
                {
                    response.AddMessageList(Process);
                    if (ex.InnerException != null && ex.InnerException.Message != null)
                    {
                        response.AddMessageList(ex.InnerException.Message);
                    }
                    else
                    {
                        if (ex.Message != null)
                        {
                            response.AddMessageList(ex.Message);
                        }
                    }

                    response.MessageToString();
                    response.SetError();
                }
            }

            return response;
        }
        public static ResponseModel UpdateCBE(CBEModel model)
        {
            ResponseModel response = new ResponseModel();

            response = ValidationCBE(model);
            string Process = string.Empty;

            if (response.IsSuccess)
            {
                model.MSR = MSRLogic.GetMSRModel(model.MSRId);
                var IssuedTypeLabel = EnumTable.GetDescriptionByCodeValue(EnumTable.EnumType.STATUS_MSR, "CBE").EnumLabel;

                VM0050 vm0050 = new VM0050();
                if (!string.IsNullOrEmpty(model.MSR.WorkOrderNo))
                {
                    vm0050 = WorkOrderLogic.GetWorkOrderAccpac(model.MSR.WorkOrderNo);
                }

                string RQNNumber = string.Empty;

                try
                {
                    #region INITIAL DOCUMENT CBE HEADER
                    Process = "INITIAL DOCUMENT CBE HEADER";

                    CBE updateCBE = CurrentDataContext.CurrentContext.CBEs.FirstOrDefault(x => x.ID == model.ID);

                    if (updateCBE != null)
                    {
                        updateCBE.CostCenter = model.RequestorCostCenter;
                        updateCBE.WorkFlowCode = model.WorkFlowCode;
                        updateCBE.Reason = model.Remarks != null ? model.Remarks : "";
                        updateCBE.TehnicalSummary = model.TechnicalSummary;
                        updateCBE.ApprovalSchema = GetSchema(model);

                        CBEBidSupplier bidSupplier = new CBEBidSupplier();
                        CBESummarySupplier supplier = new CBESummarySupplier();
                        List<CBESummarySupplier> Listsuppliers = new List<CBESummarySupplier>();

                        foreach (var updateItem in model.MSRIssuedDetails)
                        {
                            foreach (var updateBidSupplier in updateItem.CBEBidSuppliers.Where(x => x.SupplierName != "" || x.SupplierName != null))
                            {
                                CBEBidSupplier bidsupplierDetail = CurrentDataContext.CurrentContext.CBEBidSuppliers.FirstOrDefault(x => x.ID == updateBidSupplier.Id);

                                supplier = new CBESummarySupplier();

                                supplier.Supplier = updateBidSupplier.Supplier;
                                supplier.TotalBeforeDiscount = updateBidSupplier.TotalPrice_USD;
                                supplier.IsWinner = updateBidSupplier.SetAsWinner == true ? 1 : 0;

                                var LowestQtyOffering = updateItem.CBEBidSuppliers.Min(x => x.Qty);
                                supplier.GT_Requirement = LowestQtyOffering * updateBidSupplier.TotalPrice_USD;

                                Listsuppliers.Add(supplier);

                                if (bidsupplierDetail != null)
                                {
                                    bidsupplierDetail.SupplierCode = updateBidSupplier.Supplier;
                                    bidsupplierDetail.Qty = updateBidSupplier.Qty;
                                    bidsupplierDetail.Currency = updateBidSupplier.Currency;
                                    bidsupplierDetail.UnitPrice = updateBidSupplier.UnitPrice;
                                    bidsupplierDetail.TotalPrice = updateBidSupplier.TotalPrice;
                                    bidsupplierDetail.Rate = updateBidSupplier.Rate;
                                    bidsupplierDetail.TotalPrice_USD = updateBidSupplier.TotalPrice_USD;
                                    bidsupplierDetail.SetAsWinner = updateBidSupplier.SetAsWinner != true ? false : updateBidSupplier.SetAsWinner;
                                    bidsupplierDetail.Remark = updateBidSupplier.Remark;

                                    bidsupplierDetail.UpdateSave<CBEBidSupplier>();
                                }
                                else
                                {
                                    bidSupplier = new CBEBidSupplier();

                                    Guid newIdBidSupplier = Guid.NewGuid();

                                    updateBidSupplier.Id = newIdBidSupplier;
                                    bidSupplier.ID = newIdBidSupplier;
                                    bidSupplier.CBEId = model.ID;
                                    bidSupplier.CBEBidId = updateItem.ID;
                                    bidSupplier.SupplierCode = updateBidSupplier.Supplier;
                                    bidSupplier.Qty = updateBidSupplier.Qty;
                                    bidSupplier.Currency = updateBidSupplier.Currency;
                                    bidSupplier.UnitPrice = updateBidSupplier.UnitPrice;
                                    bidSupplier.TotalPrice = updateBidSupplier.TotalPrice;
                                    bidSupplier.TotalPrice_USD = updateBidSupplier.TotalPrice_USD;
                                    bidSupplier.Rate = updateBidSupplier.Rate;
                                    bidSupplier.SetAsWinner = updateBidSupplier.SetAsWinner != true ? false : updateBidSupplier.SetAsWinner;

                                    bidSupplier.Save<CBEBidSupplier>();
                                }
                            }

                            var CBEBidSupplierList = CurrentDataContext.CurrentContext.CBEBidSuppliers.Where(x => x.CBEId == model.ID && x.CBEBidId == updateItem.ID).ToList();

                            if (CBEBidSupplierList != null)
                            {
                                foreach (var bidsupplier in CBEBidSupplierList)
                                {
                                    var countBidSupplier = updateItem.CBEBidSuppliers.Where(x => x.Id == bidsupplier.ID);

                                    if (countBidSupplier.Count() == 0)
                                    {
                                        CBEBidSupplier cbebidsupplierDetail = CurrentDataContext.CurrentContext.CBEBidSuppliers.FirstOrDefault(x => x.ID == bidsupplier.ID);

                                        if (cbebidsupplierDetail != null)
                                        {
                                            cbebidsupplierDetail.Delete<CBEBidSupplier>();
                                        }
                                    }
                                }
                            }

                        }

                        var SupplierGroup = Listsuppliers.GroupBy(d => d.Supplier)
                                            .Select(
                                            g => new
                                            {
                                                key = g.Key,
                                                beforediscount = g.Sum(s => s.TotalBeforeDiscount),
                                                requirement = g.Sum(s => s.GT_Requirement),
                                                winner = g.Max(s => s.IsWinner)
                                            });


                        CBESummary newCBESummary = new CBESummary();
                        CBEBidSupplierWinner newCBEWinner = new CBEBidSupplierWinner();

                        foreach (var supplierDetail in SupplierGroup)
                        {
                            Guid Id = Guid.NewGuid();

                            Process = "INITIAL DOCUMENT CBE SUMMARY ITEMS";
                            var existSupplierSummary = CurrentDataContext.CurrentContext.CBESummaries.FirstOrDefault(x => x.CBEId == model.ID && x.SupplierCode == supplierDetail.key);
                            
                            if (existSupplierSummary == null)
                            {
                                newCBESummary = new CBESummary();

                                newCBESummary.ID = Id;
                                newCBESummary.CBEId = model.ID;
                                newCBESummary.SupplierCode = supplierDetail.key;
                                newCBESummary.TotalBeforeDiscount = supplierDetail.beforediscount;
                                newCBESummary.GT_Requirement = supplierDetail.requirement;
                                newCBESummary.IsWinner = supplierDetail.winner == 1 ? true : false;
                                newCBESummary.Save<CBESummary>();
                            }
                            else
                            {
                                string Year = string.Empty;
                                string Month = string.Empty;
                                string Day = string.Empty;

                                foreach (var supplierWinner in model.CBESummaryDetails.Where(x => x.SupplierCode == supplierDetail.key))
                                {
                                    existSupplierSummary.TotalBeforeDiscount = supplierDetail.beforediscount;
                                    existSupplierSummary.Discount = supplierWinner.Discount;
                                    existSupplierSummary.PPN = supplierWinner.PPN;
                                    existSupplierSummary.FreightCost = supplierWinner.FreightCost;
                                    existSupplierSummary.NotForPPH = supplierWinner.NotForPPH;
                                    existSupplierSummary.GT_Requirement = supplierDetail.requirement;

                                    if (supplierWinner.QuotationReceivedDateStr != null)
                                    {
                                        Year = supplierWinner.QuotationReceivedDateStr.Substring(6, 4);
                                        Month = supplierWinner.QuotationReceivedDateStr.Substring(3, 2);
                                        Day = supplierWinner.QuotationReceivedDateStr.Substring(0, 2);
                                        DateTime dateQuotationReceivedDate = DateTime.Parse(Year + "-" + Month + "-" + Day);
                                        existSupplierSummary.QuotationReceivedDate = dateQuotationReceivedDate;
                                    }
                                    if (supplierWinner.PriceValidityStr != null)
                                    {
                                        Year = supplierWinner.PriceValidityStr.Substring(6, 4);
                                        Month = supplierWinner.PriceValidityStr.Substring(3, 2);
                                        Day = supplierWinner.PriceValidityStr.Substring(0, 2);
                                        DateTime datePriceValidity = DateTime.Parse(Year + "-" + Month + "-" + Day);
                                        existSupplierSummary.PriceValidity = datePriceValidity;
                                    }
                                    existSupplierSummary.ContactPerson = supplierWinner.ContactPerson;
                                    existSupplierSummary.TermsOfPayment = supplierWinner.TermsOfPayment;
                                    existSupplierSummary.DeliveryTime = supplierWinner.DeliveryTime;
                                    existSupplierSummary.Incoterm = supplierWinner.Incoterm;
                                }

                                existSupplierSummary.IsWinner = supplierDetail.winner == 1 ? true : false;
                                existSupplierSummary.UpdateSave<CBESummary>();
                            }

                            Process = "INITIAL DOCUMENT CBE WINNERS ITEMS";
                            var existSupplierWinner = CurrentDataContext.CurrentContext.CBEBidSupplierWinners.FirstOrDefault(x => x.CBEId == model.ID && x.SupplierCode == supplierDetail.key);

                            if (existSupplierWinner == null)
                            {
                                newCBEWinner.ID = Id;
                                newCBEWinner.CBEId = model.ID;
                                newCBEWinner.SupplierCode = supplierDetail.key;
                                newCBEWinner.Save<CBEBidSupplierWinner>();
                            }
                        }

                        var CBESummaryList = CurrentDataContext.CurrentContext.CBESummaries.Where(x => x.CBEId == model.ID).ToList();

                        if (CBESummaryList != null)
                        {
                            foreach (var summary in CBESummaryList)
                            {
                                var countSummary = SupplierGroup.Where(x => x.key == summary.SupplierCode);

                                if (countSummary.Count() == 0)
                                {
                                    CBESummary cbesummaryDetail = CurrentDataContext.CurrentContext.CBESummaries.FirstOrDefault(x => x.CBEId == model.ID && x.SupplierCode == summary.SupplierCode);

                                    if (cbesummaryDetail != null)
                                    {
                                        cbesummaryDetail.Delete<CBESummary>();
                                    }

                                    CBEBidSupplierWinner cbewinnerDetail = CurrentDataContext.CurrentContext.CBEBidSupplierWinners.FirstOrDefault(x => x.CBEId == model.ID && x.SupplierCode == summary.SupplierCode);

                                    if (cbewinnerDetail != null)
                                    {
                                        cbewinnerDetail.Delete<CBESummary>();
                                    }
                                }
                            }
                        }

                        CBEDocument newCBEDocument = new CBEDocument();

                        foreach (var document in model.CBEDocumentDetails)
                        {
                            var existDocument = CurrentDataContext.CurrentContext.CBEDocuments.FirstOrDefault(x => x.Id == document.id);

                            if (existDocument == null)
                            {
                                newCBEDocument = new CBEDocument();

                                newCBEDocument.Id = Guid.NewGuid();
                                newCBEDocument.CBEId = model.ID;
                                newCBEDocument.FilePath = document.uriFile;
                                newCBEDocument.FileName = document.fileName;
                                newCBEDocument.FileSize = document.fileSize;
                                newCBEDocument.Type = "01";

                                newCBEDocument.Save<CBEDocument>();
                            }
                        }

                        var CBEDocumentList = CurrentDataContext.CurrentContext.CBEDocuments.Where(x => x.CBEId == model.ID).ToList();

                        if (CBEDocumentList != null)
                        {
                            foreach (var document in CBEDocumentList)
                            {
                                var countDocument = model.CBEDocumentDetails.Where(x => x.id == document.Id);

                                if (countDocument.Count() == 0)
                                {
                                    CBEDocument cbedocumentDetail = CurrentDataContext.CurrentContext.CBEDocuments.FirstOrDefault(x => x.Id == document.Id);

                                    if (cbedocumentDetail != null)
                                    {
                                        cbedocumentDetail.Delete<CBEDocument>();
                                    }
                                }
                            }
                        }

                        updateCBE.UpdateSave<CBE>();
                    }

                    #endregion

                    #region INITIAL DOCUMENT CBE DETAIL ITEM

                    foreach (var cbebidDetails in model.MSRIssuedDetails)
                    {
                        CBEBid updateCBEBid = CurrentDataContext.CurrentContext.CBEBids.FirstOrDefault(x => x.CBEid == cbebidDetails.ID);
                    }

                    #endregion

                    #region DOCUMENT ISSUED FINISH
                    Process = "DOCUMENT ISSUED FINISH";

                    if (response.IsSuccess)
                    {
                        response.AddMessageList(string.Format("CBE NO : {0} has been updated", model.CBENo));
                        response.SetSuccess();
                    }
                    else
                    {
                        response.AddMessageList(response.Message);
                        response.AddMessageList("Error When Updating CBE");
                        response.MessageToString();
                        response.SetError();
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    response.AddMessageList(Process);
                    if (ex.InnerException != null && ex.InnerException.Message != null)
                    {
                        response.AddMessageList(ex.InnerException.Message);
                    }
                    else
                    {
                        if (ex.Message != null)
                        {
                            response.AddMessageList(ex.Message);
                        }
                    }

                    response.MessageToString();
                    response.SetError();
                }
            }

            return response;
        }
        public static ResponseModel UpdateWinnerCBE(CBEModel model)
        {
            ResponseModel response = new ResponseModel();

            response.IsSuccess = true;
            //response = null;//ValidationMSR(model);
            string Process = string.Empty;

            if (response.IsSuccess)
            {
                try
                {
                    #region INITIAL DOCUMENT CBE HEADER
                    Process = "INITIAL DOCUMENT CBE HEADER";

                    CBE updateCBE = CurrentDataContext.CurrentContext.CBEs.FirstOrDefault(x => x.ID == model.ID);

                    if (updateCBE != null)
                    {
                        var updateCBEWinner = updateCBE.CBEBidSupplierWinners.FirstOrDefault(x => x.ID == model.CBESetWinners[0].ID);

                        if (updateCBEWinner != null)
                        {
                            updateCBEWinner.IsProprietary = model.CBESetWinners[0].IsProprietary;
                            updateCBEWinner.IsLowestCost = model.CBESetWinners[0].IsLowestCost;
                            updateCBEWinner.IsPriceStatedinContract = model.CBESetWinners[0].IsPriceStatedinContract;
                            updateCBEWinner.IsBetterDelivery = model.CBESetWinners[0].IsBetterDelivery;
                            updateCBEWinner.IsMeetSchedule = model.CBESetWinners[0].IsMeetSchedule;
                            updateCBEWinner.IsPaymentTerm = model.CBESetWinners[0].IsPaymentTerm;
                            updateCBEWinner.IsRepeatOrder = model.CBESetWinners[0].IsRepeatOrder;
                            updateCBEWinner.OrderNo = model.CBESetWinners[0].OrderNo;
                            updateCBEWinner.IsTechnicalAcceptable = model.CBESetWinners[0].IsTechnicalAcceptable;
                            updateCBEWinner.IsBetterAfterSales = model.CBESetWinners[0].IsBetterAfterSales;
                            updateCBEWinner.IsCommunityDevelopment = model.CBESetWinners[0].IsCommunityDevelopment;
                            updateCBEWinner.IsOthers = model.CBESetWinners[0].IsOthers;
                            updateCBEWinner.OthersRemark = model.CBESetWinners[0].OthersRemark;
                            updateCBEWinner.Remark = model.CBESetWinners[0].Remark;
                            updateCBEWinner.IsWinner = true;

                            updateCBEWinner.UpdateSave<CBEBidSupplierWinner>();
                        }

                    }

                    #endregion
                }
                catch (Exception ex)
                {
                    response.AddMessageList(Process);
                    if (ex.InnerException != null && ex.InnerException.Message != null)
                    {
                        response.AddMessageList(ex.InnerException.Message);
                    }
                    else
                    {
                        if (ex.Message != null)
                        {
                            response.AddMessageList(ex.Message);
                        }
                    }

                    response.MessageToString();
                    response.SetError();
                }
            }

            return response;
        }
        public static ResponseModel SubmitCBE(CBEModel model)
        {
            ResponseModel response = new ResponseModel();

            response.IsSuccess = true;
            response = ValidationSubmitCBE(model);
            string Process = string.Empty;
            bool IsItem = false;

            if (response.IsSuccess)
            {
                try
                {
                    CBE cbe = new CBE();

                    cbe = CurrentDataContext.CurrentContext.CBEs.FirstOrDefault(x => x.ID == model.ID);

                    CBEDocument cbedocument = new CBEDocument();

                    cbedocument = CurrentDataContext.CurrentContext.CBEDocuments.FirstOrDefault(x => x.CBEId == model.ID && x.Type == "02");

                    if(cbedocument == null)
                    {
                        CBEDocument newCBEDocument = new CBEDocument();

                        string serverPath = "http://msr.jresources.com:3000/CBE/DownloadAttachment?FilePath=";
                        string fileName = model.CBENo + ".xlsx";
                        string filePathRFA = System.Configuration.ConfigurationManager.AppSettings["PathSaveRFA"].ToString();
                        filePathRFA = serverPath + filePathRFA.Replace("\\", "%5C") + "&FileName=" + fileName.Replace("/", "_");

                        newCBEDocument = new CBEDocument()
                        {
                            Id = Guid.NewGuid(),
                            CBEId = model.ID,
                            FilePath = filePathRFA,
                            FileName = fileName.Replace("/", "_"),
                            FileSize = 0,
                            Type = "02"
                        };

                        newCBEDocument.Save<CBEDocument>();
                    }

                    if (cbe != null)
                    {
                        cbe.DocumentStatus = CBEStatus.SUBMITED;
                        //cbe.ApprovalSchema = Schema;

                        CBE_WF wf = new CBE_WF();

                        wf = CurrentDataContext.CurrentContext.CBE_WF.FirstOrDefault(x => x.CBENo == cbe.CBENo);
                        
                        if(wf == null)
                        {
                            wf = new CBE_WF()
                            {
                                CBENo = model.CBENo,
                                StatusWF = "INT",
                                NxtActionr = cbe.CreatedBy,
                                kdsite = cbe.Company.ADCode,
                                skemaWF = cbe.ApprovalSchema
                            };

                            wf.Save<CBE_WF>();
                        }

                        var data = CurrentDataContext.CurrentContext.CBE_SUBMIT_PROCESS(model.CBENo);
                    }

                    #region DOCUMENT ISSUED FINISH
                    Process = "DOCUMENT ISSUED FINISH";

                    if (response.IsSuccess)
                    {
                        cbe.Save<CBE>();
                        response.AddMessageList(string.Format("CBE NO : {1} has been submited", "CBE", model.CBENo));
                        response.SetSuccess();
                    }
                    else
                    {
                        response.AddMessageList(response.Message);
                        response.AddMessageList("Error When Submit To Accpac");
                        response.MessageToString();
                        response.SetError();
                    }
                    #endregion                       
                }
                catch (Exception ex)
                {
                    response.AddMessageList(Process);
                    if (ex.InnerException != null && ex.InnerException.Message != null)
                    {
                        response.AddMessageList(ex.InnerException.Message);
                    }
                    else
                    {
                        if (ex.Message != null)
                        {
                            response.AddMessageList(ex.Message);
                        }
                    }

                    response.MessageToString();
                    response.SetError();
                }
            }

            return response;
        }
        public static ResponseModel DiscardCBE(CBEModel model)
        {
            ResponseModel response = new ResponseModel();

            response.IsSuccess = true;
            //response = null;//ValidationMSR(model);
            string Process = string.Empty;
            bool IsItem = false;

            if (response.IsSuccess)
            {

                try
                {
                    CBE cbe = new CBE();

                    cbe = CurrentDataContext.CurrentContext.CBEs.FirstOrDefault(x => x.ID == model.ID);

                    if (cbe != null)
                    {
                        cbe.DocumentStatus = CBEStatus.DISCARD;
                    }

                    #region DOCUMENT ISSUED FINISH
                    Process = "DOCUMENT ISSUED FINISH";

                    if (response.IsSuccess)
                    {
                        cbe.Save<CBE>();
                        response.AddMessageList(string.Format("CBE NO : {0} has been {1}", model.CBENo, cbe.DocumentStatus));
                        response.SetSuccess();
                    }
                    else
                    {
                        response.AddMessageList(response.Message);
                        response.AddMessageList("Error When Submit To Accpac");
                        response.MessageToString();
                        response.SetError();
                    }
                    #endregion                       
                }
                catch (Exception ex)
                {
                    response.AddMessageList(Process);
                    if (ex.InnerException != null && ex.InnerException.Message != null)
                    {
                        response.AddMessageList(ex.InnerException.Message);
                    }
                    else
                    {
                        if (ex.Message != null)
                        {
                            response.AddMessageList(ex.Message);
                        }
                    }

                    response.MessageToString();
                    response.SetError();
                }
            }

            return response;
        }
        public static ResponseModel PostRQNCBE(CBEModel model)
        {
            ResponseModel response = new ResponseModel();

            response = ValidationCBE(model);
            response.IsSuccess = true;
            string Process = string.Empty;

            if (response.IsSuccess)
            {
                model.MSR = MSRLogic.GetMSRModel(model.MSRId);
                //var IssuedTypeLabel = EnumTable.GetDescriptionByCodeValue(EnumTable.EnumType.STATUS_MSR, model.IssuedType).EnumLabel;
                var IssuedTypeLabel = RequestStatus.PURCHASING;
                VM0050 vm0050 = new VM0050();

                if (!string.IsNullOrEmpty(model.MSR.WorkOrderNo))
                {
                    vm0050 = WorkOrderLogic.GetWorkOrderAccpac(model.MSR.WorkOrderNo);
                }

                string RQNNumber = string.Empty;

                try
                {
                    var cbe = CurrentDataContext.CurrentContext.CBEs.FirstOrDefault(x => x.ID == model.ID);
                    var msr = CurrentDataContext.CurrentContext.MSRs.FirstOrDefault(x => x.ID == cbe.MSRId);

                    cbe.DocumentStatus = CBEStatus.POSTING;

                    #region DOCUMENT ISSUED FINISH
                    Process = "DOCUMENT ISSUED FINISH";

                    if (response.IsSuccess)
                    {
                        SchedulerLogic.Create(cbe.CompanyId, DocumentType.MSR, cbe.CBENo);

                        cbe.Save<CBE>();
                        string StatusMSR = MSRChangeStatusById(cbe.MSRId);

                        response.AddMessageList(string.Format("CBE NO : {0} has been posting", cbe.CBENo));
                        response.AddMessageList(string.Format("MSR NO : {0} change status to {1}", msr.MSRNo, StatusMSR));
                        response.SetSuccess();
                    }
                    else
                    {
                        response.AddMessageList(response.Message);
                        response.AddMessageList("Error When Submit To Accpac");
                        response.MessageToString();
                        response.SetError();
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    response.AddMessageList(Process);
                    if (ex.InnerException != null && ex.InnerException.Message != null)
                    {
                        response.AddMessageList(ex.InnerException.Message);
                    }
                    else
                    {
                        if (ex.Message != null)
                        {
                            response.AddMessageList(ex.Message);
                        }
                    }

                    response.MessageToString();
                    response.SetError();
                }
            }

            return response;
        }
        public static ResponseModel ValidationCBE(CBEModel model)
        {
            var response = new ResponseModel();
            StringBuilder sbError = new StringBuilder();

            bool SetAsWinner = false;

            var cbe = CurrentDataContext.CurrentContext.CBEs.Where(x => x.ID == model.ID).ToList();

            if (cbe != null)
            {
                foreach (var a in model.MSRIssuedDetails)
                {
                    var CountWinner = a.CBEBidSuppliers.Where(x => x.SetAsWinner == true && x.Supplier != null);

                    if (CountWinner.Count() > 0)
                    {
                        SetAsWinner = true;
                    }
                }

                if (model.StatusCode == CBEStatus.APPROVED)
                {
                    var IsAccpacSync = from a in CurrentDataContext.CurrentContext.CBEBidSuppliers
                                       join b in CurrentDataContext.CurrentContext.CBE_MsSupplier on a.SupplierCode equals b.SupplierCode
                                       where a.CBEId == model.ID && b.IsAccpacSync == false
                                       select new
                                       {
                                           a.ID,
                                           MsSupplierId = b.ID,
                                           b.SupplierName
                                       };

                    if(IsAccpacSync != null)
                    {
                        foreach(var IsAccpacSyncDetail in IsAccpacSync)
                        {
                            var Supplier = CurrentDataContext.CurrentContext.vw_CBE_Vendor.FirstOrDefault(x => x.VENDNAME == IsAccpacSyncDetail.SupplierName);

                            if(Supplier != null)
                            {
                                CBEBidSupplier bidsupplierDetail = CurrentDataContext.CurrentContext.CBEBidSuppliers.FirstOrDefault(x => x.ID == IsAccpacSyncDetail.ID);
                                bidsupplierDetail.SupplierCode = Supplier.VENDORID;

                                CBE_MsSupplier mssupplier = CurrentDataContext.CurrentContext.CBE_MsSupplier.FirstOrDefault(x => x.ID == IsAccpacSyncDetail.MsSupplierId);
                                mssupplier.SupplierCode = Supplier.VENDORID;
                                mssupplier.IsAccpacSync = true;

                                bidsupplierDetail.UpdateSave<CBEBidSupplier>();
                                mssupplier.UpdateSave<CBE_MsSupplier>();
                            }
                            else {
                                response.SetError("Supplier with name " + IsAccpacSyncDetail.SupplierName + " not exist on accpac");
                            }
                            
                        }
                    }

                }
            }
            if(!SetAsWinner)
            {
                response.SetError("Please check one supplier for winner");
            }

            if (sbError.Length > 0)
            {
                response.SetError(sbError.ToString());
            }

            return response;
        }
        public static ResponseModel ValidationSubmitCBE(CBEModel model)
        {
            var response = new ResponseModel();
            StringBuilder sbError = new StringBuilder();

            var cekWinner = from a in CurrentDataContext.CurrentContext.CBEBidSuppliers
                            join b in CurrentDataContext.CurrentContext.CBEBidSupplierWinners on a.SupplierCode equals b.SupplierCode
                            where a.CBEId == model.ID && a.SetAsWinner == true && (b.IsWinner == false || b.IsWinner == null)
                            select a;

            if (cekWinner.Count() > 0)
            {
                response.SetError("Please Set Basis Of Award for each winning vendor before Submit For Approval");
            }

            if (sbError.Length > 0)
            {
                response.SetError(sbError.ToString());
            }

            return response;
        }
        public static CBESearchModel GetListCBE(CBESearchModel model)
        {
            model.SetDateRange();
            var MSRCBEs = CBE.GetAll();
            var MSRISSUEDs = MSRIssued.GetAll();

            var roleCookies = CookieManager.Get(COOKIES_NAME.ROLE);
            var user = CurrentUser.Model;
            user.SetRolesFromCookies(roleCookies);

            if (!user.IsAdministrator)
            {
                List<SelectListItem> ListDepartment = RoleLogic.GetListDepartmentUserByRole(CurrentUser.USERNAME, CurrentUser.COMPANY, model.ROLE_ACCESS);
                List<Guid> ListDepartmentId = ListDepartment
                    .Where(x => !string.IsNullOrEmpty(x.Value))
                    .Where(x => x.Value != Guid.Empty.ToString())
                    .Select(x => Guid.Parse(x.Value)).ToList();

                MSRCBEs = MSRCBEs.Where(x => ListDepartmentId.Contains(x.DepartmentId));
            }

            if (!string.IsNullOrEmpty(model.CBENo))
            {
                MSRCBEs = MSRCBEs.Where(x => x.CBENo.Contains(model.CBENo));
            }

            if (!string.IsNullOrEmpty(model.MSRNo))
            {
                var msr = MSR.GetByNo(model.MSRNo);
                if (msr != null)
                {
                    MSRCBEs = MSRCBEs.Where(x => x.MSRId == msr.ID);
                }
            }

            if (model.DateFrom.HasValue)
            {
                MSRCBEs = MSRCBEs.Where(x => x.CreatedDate >= model.DateFrom.Value);
            }

            if (model.DateTo.HasValue)
            {
                MSRCBEs = MSRCBEs.Where(x => x.CreatedDate <= model.DateTo.Value);
            }

            if (model.CompanyId != null && model.CompanyId != Guid.Empty)
            {
                var department = Department.GetByCompanyId(model.CompanyId);
                var listDepartmentId = department.Select(x => x.ID).ToList();
                MSRCBEs = MSRCBEs.Where(x => listDepartmentId.Contains(x.DepartmentId));
            }

            if (model.DepartmentId != null && model.DepartmentId != Guid.Empty)
            {
                MSRCBEs = MSRCBEs.Where(x => x.DepartmentId == model.DepartmentId);
            }

            model.SetPager(MSRCBEs.Count());
            List<CBEListmodel> listData = MSRCBEs.OrderByDescending(x => x.CreatedDate)
                .Skip((model.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
                .Take(Common.SiteSettings.PAGE_SIZE_PAGING)
                .Select(x => new CBEListmodel()
                {
                    ID = x.ID,
                    CBENo = x.CBENo,
                    MSRId = x.MSRId,
                    MSRNo = x.MSR.MSRNo,
                    Status = x.DocumentStatus,
                    MSRIssuedDate = x.CreatedDate,
                    MSRIssuedDateStr = "",
                    CompanyId = x.CompanyId,
                    CompanyName = x.Company.CompanyName,
                    DepartmentId = x.DepartmentId,
                    RQNNo = (MSRISSUEDs.FirstOrDefault(a => a.MSRIssuedNo == x.CBENo) != null ? MSRISSUEDs.FirstOrDefault(a => a.MSRIssuedNo == x.CBENo).RQNNumber : ""),
                    DepartmentName = x.Department.DepartmentName,
                    Requestor = x.CreatedBy,
                    Remarks = x.Reason
                }).ToList();

            List<string> RequestorList = listData.Select(x => x.Requestor).Distinct().ToList();
            var requestors = UserLogin.GetAll().Where(x => RequestorList.Contains(x.NIKSite)).ToList();
            var statusMSRs = EnumTable.GetByCode(EnumTable.EnumType.STATUS_MSR).ToList();

            foreach (var item in listData)
            {
                var checkUser = requestors.FirstOrDefault(x => x.NIKSite == item.Requestor);
                if (checkUser != null)
                {
                    item.Requestor = checkUser.FullName;
                }

                item.LabelCssColor = CommonFunction.LabelStatusCssHelper(item.Status);
                var statusMSR = statusMSRs.FirstOrDefault(x => x.EnumValue == item.IssuedType);
                if (statusMSR != null)
                {
                    item.IssuedType = statusMSR.EnumLabel;
                }

                item.MSRIssuedDateStr = item.MSRIssuedDate.JResourcesDate();
            }

            model.ListData = listData;
            return model;
        }
        public static CBEModel GetDataByCBEId(Guid CBEId)
        {
            CBE cbe = CBE.GetById(CBEId);
            var MSRISSUEDs = MSRIssued.GetAll();
            var Supplier = CurrentDataContext.CurrentContext.vw_CBE_Vendor.ToList();
            var SupplierCBE = CurrentDataContext.CurrentContext.CBE_MsSupplier.Where(x => x.IsAccpacSync == false).ToList();

            CBEModel model = new CBEModel();
            List<CBERecomendationModel> recList = new List<CBERecomendationModel>();
            List<CBEDocument> docList = new List<CBEDocument>();

            Guid MSRId = new Guid();
            MSRId = cbe.MSRId;

            if (cbe != null)
            {
                model.ID = CBEId;
                model.CBENo = cbe.CBENo;
                model.MSR = MSRLogic.GetMSRModel(MSRId);
                model.MSRId = model.MSR.ID;
                model.MSRNo = model.MSR.DocumentNo;
                model.WorkOrderNo = model.MSR.WorkOrderNo;
                model.CompanyId = model.MSR.CompanyId;
                model.DepartmentId = model.MSR.DepartmentId;
                model.CostCenter = model.MSR.CostCenter;
                model.WorkFlowCode = cbe.WorkFlowCode;
                model.Remarks = model.MSR.Reason;
                model.RequestorCostCenter = cbe.CostCenter;
                model.TechnicalSummary = cbe.TehnicalSummary;
                model.StatusCode = cbe.DocumentStatus;
                model.RQNNumber = (MSRISSUEDs.FirstOrDefault(a => a.MSRIssuedNo == cbe.CBENo) != null ? MSRISSUEDs.FirstOrDefault(a => a.MSRIssuedNo == cbe.CBENo).RQNNumber : "");

                SelectListItem ListMSRType = JResources.Common.EnumUtils<JResources.Common.MSRType>.GetDropDownList()
                    .FirstOrDefault(x => x.Value == model.MSR.MSRType);
                model.MSR.MSRType = ListMSRType.Text;

                var costcenter = CostCenterLogic.GetCostCenterByDepartmentId(model.MSR.DepartmentId);
                if (costcenter != null)
                {
                    var costSelect = costcenter.FirstOrDefault(x => x.Value.Trim() == model.MSR.CostCenter.Trim());
                    if (costSelect != null)
                    {
                        model.MSR.CostCenter = costSelect.Value;
                        model.MSR.CostCenterLabel = costSelect.Text;
                    }
                }

                var MSRApproval = MSRLogic.GetApproval(model.MSR.DepartmentId);
                model.MSR.Manager = MSRApproval.Manager;
                model.MSR.TotalWithApprovalGM = MSRApproval.TotalWithApprovalGM;
                model.MSR.GeneralManagerSite = MSRApproval.GeneralManagerSite;

                var CBEBidDetails = cbe.CBEBids.Where(x => Convert.ToBoolean(!x.IsDeleted));

                foreach (var item in CBEBidDetails.OrderBy(x => x.LineNo))
                {
                    MSRIssuedDetailModel IssuedDetail = new MSRIssuedDetailModel();
                    IssuedDetail.ID = item.ID;
                    IssuedDetail.LineNo = Convert.ToInt32(item.LineNo);
                    IssuedDetail.ItemNo = item.Itemno;
                    IssuedDetail.UOM = item.UoM;
                    IssuedDetail.Description = item.Description;
                    IssuedDetail.Location = item.LocationCode;
                    IssuedDetail.QtyRequest = Convert.ToDecimal(item.QtyRequest);
                    IssuedDetail.CostCode = item.CostCode;
                    IssuedDetail.Category = item.CostCode;

                    var CBEBidSupplierDetails = item.CBEBidSuppliers;

                    foreach (var itemBidSupplier in CBEBidSupplierDetails)
                    {
                        CBEBidSupplierModel bidsupplierDetail = new CBEBidSupplierModel();

                        bidsupplierDetail.Id = itemBidSupplier.ID;
                        bidsupplierDetail.CBEBidId = item.ID;
                        bidsupplierDetail.ItemNo = item.Itemno;
                        bidsupplierDetail.Supplier = itemBidSupplier.SupplierCode;

                        var SupplierName = Supplier.FirstOrDefault(x => x.VENDORID == itemBidSupplier.SupplierCode);
                        var SupplierCBEName = SupplierCBE.FirstOrDefault(x => x.SupplierCode == itemBidSupplier.SupplierCode);

                        bidsupplierDetail.SupplierName = SupplierName != null ? SupplierName.VENDNAME : SupplierCBEName.SupplierName;

                        bidsupplierDetail.Qty = itemBidSupplier.Qty;
                        bidsupplierDetail.Currency = itemBidSupplier.Currency;
                        bidsupplierDetail.UnitPrice = itemBidSupplier.UnitPrice;
                        bidsupplierDetail.TotalPrice = itemBidSupplier.TotalPrice;
                        bidsupplierDetail.TotalPrice_USD = itemBidSupplier.TotalPrice_USD;
                        bidsupplierDetail.Rate = itemBidSupplier.Rate;
                        bidsupplierDetail.SetAsWinner = itemBidSupplier.SetAsWinner;
                        bidsupplierDetail.Remark = itemBidSupplier.Remark;

                        IssuedDetail.CBEBidSuppliers.Add(bidsupplierDetail);
                    }

                    model.MSRIssuedDetails.Add(IssuedDetail);
                }

                if (model.MSR.MSRDetail.Count() > 0)
                {
                    model.MSRIssuedDetails[0].ItemLocations = model.MSR.MSRDetail[0].ItemLocations;
                }

                var recomendation = from a in CurrentDataContext.CurrentContext.CBEs
                                    join b in CurrentDataContext.CurrentContext.CBEBids on a.ID equals b.CBEid
                                    join c in CurrentDataContext.CurrentContext.CBEBidSuppliers on b.ID equals c.CBEBidId
                                    join d in CurrentDataContext.CurrentContext.MSRs on a.MSRId equals d.ID
                                    join e in CurrentDataContext.CurrentContext.vw_CBE_Vendor on c.SupplierCode equals e.VENDORID
                                    where a.ID == model.ID && e.compid == CurrentUser.COMPANY
                                    select new
                                    {
                                        a.ID,
                                        a.CBENo,
                                        a.MSRId,
                                        MSRNo = d.MSRNo,
                                        SupplierCode = c.SupplierCode,  
                                        SupplierName = e.VENDNAME,  
                                        b.Itemno,  
                                        b.Description,  
                                        b.QtyRequest,  
                                        c.Currency,  
                                        c.Qty,  
                                        c.UnitPrice,
                                        c.TotalPrice,
                                        c.TotalPrice_USD,
                                        c.SetAsWinner
                                    };

                CBERecomendationModel recModel = new CBERecomendationModel();

                foreach (var rec in recomendation.OrderBy(x => x.Itemno).ThenBy(x => x.TotalPrice_USD))
                {
                    recModel = new CBERecomendationModel();

                    recModel.SupplierCode = rec.SupplierCode;
                    recModel.SupplierName = rec.SupplierName;
                    recModel.ItemNo = rec.Itemno;
                    recModel.Description = rec.Description;
                    recModel.QtyProcess = rec.QtyRequest;
                    recModel.Currency = rec.Currency;
                    recModel.QtyOffering = rec.Qty;
                    recModel.UnitPrice = rec.UnitPrice;
                    recModel.TotalPrice = rec.TotalPrice;

                    var lowestPrice = recomendation.Where(x => x.Itemno == rec.Itemno).Min(x => x.TotalPrice_USD);
                    recModel.PersenTotalPrice_USD = lowestPrice == 0 ? "0%" : (Math.Round(Convert.ToDouble(rec.TotalPrice_USD / lowestPrice * 100), 2)).ToString() + "%";
                    
                    recModel.TotalPrice_USD = rec.TotalPrice_USD;
                    recModel.IsWinner = rec.SetAsWinner;
                    recModel.IsWinnerText = rec.SetAsWinner == true ? "Yes" : "No";
                    recModel.LabelCssColor = CommonFunction.LabelStatusCssHelper(recModel.IsWinner == true ? "WON" : "LOSE");

                    model.CBERecomendationDetails.Add(recModel);
                }

                var CBEBidSupplierWinnerDetails = cbe.CBEBidSupplierWinners;

                foreach (var winner in CBEBidSupplierWinnerDetails)
                {
                    CBESetWinnerModel winnerDetail = new CBESetWinnerModel();

                    winnerDetail.ID = winner.ID;
                    winnerDetail.CBEId = winner.CBEId;
                    winnerDetail.SupplierCode = winner.SupplierCode;
                    winnerDetail.IsProprietary = winner.IsProprietary;
                    winnerDetail.IsLowestCost = winner.IsLowestCost;
                    winnerDetail.IsPriceStatedinContract = winner.IsPriceStatedinContract;
                    winnerDetail.IsBetterDelivery = winner.IsBetterDelivery;
                    winnerDetail.IsMeetSchedule = winner.IsMeetSchedule;
                    winnerDetail.IsPaymentTerm = winner.IsPaymentTerm;
                    winnerDetail.IsRepeatOrder = winner.IsRepeatOrder;
                    winnerDetail.OrderNo = winner.OrderNo;
                    winnerDetail.IsTechnicalAcceptable = winner.IsTechnicalAcceptable;
                    winnerDetail.IsBetterAfterSales = winner.IsBetterAfterSales;
                    winnerDetail.IsCommunityDevelopment = winner.IsCommunityDevelopment;
                    winnerDetail.IsOthers = winner.IsOthers;
                    winnerDetail.OthersRemark = winner.OthersRemark;
                    winnerDetail.IsWinner = winner.IsWinner;

                    model.CBESetWinners.Add(winnerDetail);

                }

                var summary = cbe.CBESummaries.Where(X => X.IsWinner == true);

                CBESummaryModel sumModel = new CBESummaryModel();

                foreach(var sum in summary.OrderBy(x => x.TotalBeforeDiscount))
                {
                    sumModel = new CBESummaryModel();

                    sumModel.ID = sum.ID;
                    sumModel.CBEBid = model.ID;
                    sumModel.SupplierCode = sum.SupplierCode;
                    var supplierName = CurrentDataContext.CurrentContext.vw_CBE_Vendor.FirstOrDefault(x => x.VENDORID == sum.SupplierCode).VENDNAME;sumModel.SupplierCode = sum.SupplierCode;
                    sumModel.SupplierName = supplierName;
                    sumModel.TotalBeforeDiscount = sum.TotalBeforeDiscount;
                    sumModel.Discount = sum.Discount;
                    sumModel.PPN = sum.PPN;
                    sumModel.FreightCost = sum.FreightCost;
                    sumModel.NotForPPH = sum.NotForPPH;
                    sumModel.QuotationReceivedDate = sum.QuotationReceivedDate;
                    sumModel.QuotationReceivedDateStr = sumModel.QuotationReceivedDate.HasValue ? sumModel.QuotationReceivedDate.Value.ToString(Constant.FORMAT_DATE_JRESOURCES) : "";
                    sumModel.PriceValidity = sum.PriceValidity;
                    sumModel.PriceValidityStr = sumModel.PriceValidity.HasValue ? sumModel.PriceValidity.Value.ToString(Constant.FORMAT_DATE_JRESOURCES) : "";
                    sumModel.ContactPerson = sum.ContactPerson;
                    sumModel.TermsOfPayment = sum.TermsOfPayment;
                    sumModel.DeliveryTime = sum.DeliveryTime;
                    sumModel.Incoterm = sum.Incoterm;
                    sumModel.GT_Requirement = sum.GT_Requirement;
                    sumModel.IsWinner = CBEBidSupplierWinnerDetails.Where(x => x.ID == sum.ID).FirstOrDefault().IsWinner;

                    model.CBESummaryDetails.Add(sumModel);

                }

                var document = cbe.CBEDocuments.Where(x => x.Type == "01");

                if (document != null)
                {
                    CBEDocumentModel docModel = new CBEDocumentModel();

                    foreach(var doc in document)
                    {
                        docModel = new CBEDocumentModel();

                        docModel.id = doc.Id;
                        docModel.uriFile = doc.FilePath;
                        docModel.fileName = doc.FileName;
                        docModel.fileSize = doc.FileSize;

                        model.CBEDocumentDetails.Add(docModel);
                    }
                }
            }

            var currency = CurrentDataContext.CurrentContext.vw_CBE_Currency.ToList();

            MSRCurrency curModel = new MSRCurrency();

            foreach(var cur in currency)
            {
                curModel = new MSRCurrency();

                curModel.Currency = cur.currtyp;
                curModel.CurrencyCode = cur.currtyp;
                curModel.CurrencyName = cur.currtyp;

                model.MSRCurrencyDetails.Add(curModel);
            }

            var supplier = CurrentDataContext.CurrentContext.vw_CBE_Vendor.Where(x=>x.compid== CurrentUser.COMPANY).ToList();

            MSRSupplier supModel = new MSRSupplier();

            foreach (var sup in supplier)
            {
                supModel = new MSRSupplier();

                supModel.Supplier = sup.VENDORID;
                supModel.SupplierCode = sup.VENDORID;
                supModel.SupplierName = sup.VENDNAME;

                model.MSRSupplierDetails.Add(supModel);
            }

            PPNModel PPNs = new PPNModel();
            List<PPNModel> listPPNs = new List<PPNModel>();
            List<EnumTable> PPN_CBE = EnumTable.GetByCode(EnumTable.EnumType.CBESummary_PPN).ToList();

            foreach (var item in PPN_CBE.OrderBy(x => x.Sequence))
            {
                PPNs = new PPNModel
                {
                    Value = Convert.ToDecimal(item.EnumValue),
                    Text = item.EnumLabel
                };

                model.PPNs.Add(PPNs);
            }

            return model;
        }
        public static CBESearchModel GetListEnumTableCBE(CBESearchModel model)
        {
            model.SetDateRange();

            var EnumTables = EnumTable.GetAll();

            var roleCookies = CookieManager.Get(COOKIES_NAME.ROLE);
            var user = CurrentUser.Model;
            user.SetRolesFromCookies(roleCookies);

            if (model.SearchCriteria != "")
            {
                EnumTables = EnumTables.Where(x => x.EnumCode == model.SearchCriteria);
            }

            model.SetPager(EnumTables.Count());
            List<EnumTable> listData = EnumTables.OrderBy(x => x.Sequence)
                .Skip((model.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
                .Take(Common.SiteSettings.PAGE_SIZE_PAGING).ToList();

            foreach (var enumtable in listData)
            {
                var EnumTableList = new CBEListmodel();

                EnumTableList.ID = enumtable.ID;
                EnumTableList.CompanyCode = enumtable.CompanyCode;
                EnumTableList.EnumCode = enumtable.EnumCode;
                EnumTableList.Sequence = enumtable.Sequence;
                EnumTableList.EnumValue = enumtable.EnumValue;
                EnumTableList.EnumLabel = enumtable.EnumLabel;
                EnumTableList.IsDeleted = enumtable.IsDeleted;

                model.ListData.Add(EnumTableList);
            }

            return model;
        }
        public static decimal GetDataRate(string homecur, string sourcecur, string pycostcode)
        {
            decimal Rate = 0.00m;

            var rateModel = CurrentDataContext.CurrentContext.vw_CBE_GetRate.FirstOrDefault(x => x.homecur == homecur &&
                                                                                                 x.sourcecur == sourcecur &&
                                                                                                 x.pycostcode == pycostcode);

            if(rateModel != null)
            {
                Rate = rateModel.rate;
            }

            return Rate;
        }
        public static CBEModel GetDataWinner(Guid Id)
        {
            CBEModel model = new CBEModel();
            CBESetWinnerModel setWinner = new CBESetWinnerModel();
            List<CBESetWinnerModel> listsetWinner = new List<CBESetWinnerModel>();

            var winnerModel = CurrentDataContext.CurrentContext.CBEBidSupplierWinners.FirstOrDefault(x => x.ID == Id);

            if (winnerModel != null)
            {
                setWinner = new CBESetWinnerModel();

                setWinner.ID = winnerModel.ID;

                setWinner.SupplierCode = winnerModel.SupplierCode;
                setWinner.SupplierCode = winnerModel.SupplierCode;
                var supplierName = CurrentDataContext.CurrentContext.vw_CBE_Vendor.FirstOrDefault(x => x.VENDORID == winnerModel.SupplierCode).VENDNAME;
                setWinner.SupplierName = supplierName;
                setWinner.IsProprietary = winnerModel.IsProprietary;
                setWinner.IsLowestCost = winnerModel.IsLowestCost;
                setWinner.IsPriceStatedinContract = winnerModel.IsPriceStatedinContract;
                setWinner.IsBetterDelivery = winnerModel.IsBetterDelivery;
                setWinner.IsMeetSchedule = winnerModel.IsMeetSchedule;
                setWinner.IsPaymentTerm = winnerModel.IsPaymentTerm;
                setWinner.IsRepeatOrder = winnerModel.IsRepeatOrder;
                setWinner.OrderNo = winnerModel.OrderNo;
                setWinner.IsTechnicalAcceptable = winnerModel.IsTechnicalAcceptable;
                setWinner.IsBetterAfterSales = winnerModel.IsBetterAfterSales;
                setWinner.IsCommunityDevelopment = winnerModel.IsCommunityDevelopment;
                setWinner.IsOthers = winnerModel.IsOthers;
                setWinner.OthersRemark = winnerModel.OthersRemark;
                setWinner.Remark = winnerModel.Remark;

                model.CBESetWinners.Add(setWinner);
            }

            return model;
        }
        public static string MSRChangeStatusById(Guid MSRId)
        {
            var MSRData = MSR.GetById(MSRId);
            var MSRDetails = MSRData.MSRDetails.Where(x => !x.IsDeleted).ToList();
            var MSRIssueds = MSRIssued.GetByMSRId(MSRId);
            List<MSRIssuedDetailModel> ITEMS = new List<MSRIssuedDetailModel>();

            bool IsHavePurchased = false;
            bool IsHaveTransfer = false;
            bool IsHaveIssued = false;

            MSRDetails.ForEach(x =>
            {
                MSRIssuedDetailModel ItemIssued = new MSRIssuedDetailModel();
                ItemIssued.LineNo = x.LineNo;
                ItemIssued.ItemNo = x.ItemNo;
                ItemIssued.QtyRequest = x.Qty;

                ITEMS.Add(ItemIssued);
            });

            foreach (var msrIssued in MSRIssueds)
            {
                foreach (var item in msrIssued.MSRIssuedDetails)
                {
                    ITEMS.ForEach(x =>
                    {
                        if (x.LineNo == item.LineNo)
                        {
                            if (msrIssued.IssuedType == RequestStatus.TRANSFER)
                            {
                                x.QtyTransfered += item.Qty;
                                IsHaveTransfer = true;
                            }

                            if (msrIssued.IssuedType == RequestStatus.CANCEL)
                            {
                                //x.QtyCancel += item.Qty;
                                //x.QtyDelivered += item.Qty;

                                var docCancelApps = DocumentApproval.GetByDocumentNo(msrIssued.MSRIssuedNo).ToList();
                                if (docCancelApps != null && docCancelApps.Count > 0)
                                {
                                    var docCancelAppsPending = docCancelApps.Where(y => !y.IsFinish).ToList();
                                    if (!(docCancelAppsPending != null && docCancelAppsPending.Count > 0))
                                    {
                                        x.QtyCancel += item.Qty;
                                        x.QtyDelivered += item.Qty;
                                    }
                                }
                            }

                            if (msrIssued.IssuedType == RequestStatus.ISSUED)
                            {
                                x.QtyProcess += item.Qty;
                                x.QtyDelivered += item.Qty;

                                IsHaveIssued = true;
                            }

                            if (msrIssued.IssuedType == RequestStatus.PURCHASING)
                            {
                                if (msrIssued.IsTransferAccpac)
                                {
                                    x.QtyProcess += item.Qty;
                                }
                                else
                                {
                                    x.QtyPurchase += item.Qty;
                                }

                                IsHavePurchased = true;
                                //x.QtyDelivered += item.Qty;
                            }

                        }
                    });

                }
            }

            bool IsCompleted = true;
            bool IsCancelAll = true;
            bool IsFullPurchased = true;
            bool IsFullTransfer = true;
            bool IsHaveOutstanding = false;

            ITEMS.ForEach(x =>
            {
                if (MSRData.RequestType == ((int)MSRType.Inventory).ToString() && !MSRData.IsReOrderPoint)
                {
                    if (x.QtyDelivered != x.QtyRequest)
                    {
                        IsCompleted = false;
                    }
                }
                else
                {
                    if (x.QtyPurchase != x.QtyRequest)
                    {
                        IsCompleted = false;
                    }
                }

                if (!MSRData.IsReOrderPoint && x.QtyProcess > 0)
                {
                    IsCancelAll = false;
                }

                if (x.QtyPurchase > 0)
                {
                    IsHavePurchased = true;
                }

                if (x.QtyIssuedOutstanding > 0)
                {
                    IsHaveOutstanding = true;
                }

                if (x.QtyTransferOutstanding > 0)
                {
                    IsFullTransfer = false;
                }

                if (x.QtyPurchaseOutstanding > 0)
                {
                    IsFullPurchased = false;
                }
            });

            string STATUS = RequestStatus.PARTIAL_ISSUED;
            if (IsCompleted)
            {
                if (IsCancelAll)
                {
                    STATUS = RequestStatus.COMPLETED;
                }
                else
                {
                    if (!IsHaveOutstanding)
                    {
                        STATUS = RequestStatus.COMPLETED;
                    }
                }


                if (MSRData.IsReOrderPoint && IsHavePurchased)
                {
                    STATUS = RequestStatus.COMPLETED;
                }

                if (MSRData.RequestType == ((int)MSRType.NonInventory).ToString())
                {
                    STATUS = RequestStatus.COMPLETED;
                }
            }
            else
            {
                if (IsHavePurchased)
                {
                    if (IsFullPurchased)
                    {
                        STATUS = RequestStatus.FULL_PURCHASING;
                    }
                    else
                    {
                        STATUS = RequestStatus.PARTIAL_PURCHASING;
                    }
                }

                if (IsHaveTransfer)
                {
                    if (IsFullTransfer)
                    {
                        STATUS = RequestStatus.FULL_TRANSFER;
                    }
                    else
                    {
                        STATUS = RequestStatus.PARTIAL_TRANSFER;
                    }
                }

                if (IsHaveIssued)
                {
                    STATUS = RequestStatus.PARTIAL_ISSUED;
                }
            }


            MSRLogic.SetActionMSRStatusIssued(MSRId, STATUS);

            var RequestNew = EnumTable.GetDescriptionByCodeValue(EnumTable.EnumType.STATUS_MSR, STATUS);
            return RequestNew.EnumLabel;
        }
        public static string GetSchema(CBEModel model)
        {
            string Schema = string.Empty;

            decimal TotalAmount = 0.00m;
            foreach (var msrIssued in model.MSRIssuedDetails)
            {
                foreach (var bidSupplierdetail in msrIssued.CBEBidSuppliers.Where(x => x.Supplier != null && x.SetAsWinner == true))
                {
                    TotalAmount = TotalAmount + Convert.ToDecimal(bidSupplierdetail.TotalPrice_USD);
                }
            }

            var enumtable = CurrentDataContext.CurrentContext.EnumTables.Where(x => x.EnumCode == "CBE_Item").ToList();

            bool IsItem = false;
            foreach (var itemDetail in model.MSRIssuedDetails)
            {
                var CBEItem = enumtable.FirstOrDefault(x => x.EnumValue == itemDetail.ItemNo);

                if (CBEItem != null)
                {
                    IsItem = true;
                }
            }

            Schema = CurrentDataContext.CurrentContext.CBE_MsSchema.FirstOrDefault(x => TotalAmount >= x.AmountFrom && x.AmountTo >= TotalAmount && x.IsItem == IsItem).SchemaName;

            return Schema;
        }
    }
}

