del /Q /S JResources.AccpacAPI\bin\*
del /Q /S JResources.AccpacAPI\obj\Debug\*
del /Q /S JResources.AccpacAPI\obj\Release\*

del /Q /S JResources.BusinessLogic\bin\Debug\*
del /Q /S JResources.BusinessLogic\bin\Release\*
del /Q /S JResources.BusinessLogic\obj\Debug\*
del /Q /S JResources.BusinessLogic\obj\Release\*

del /Q /S JResources.Common\bin\Debug\*
del /Q /S JResources.Common\bin\Release\*
del /Q /S JResources.Common\obj\Debug\*
del /Q /S JResources.Common\obj\Release\*

del /Q /S JResources.Console\bin\Debug\*
del /Q /S JResources.Console\bin\Release\*
del /Q /S JResources.Console\obj\Debug\*
del /Q /S JResources.Console\obj\Release\*

del /Q /S JResources.Data\bin\Debug\*
del /Q /S JResources.Data\bin\Release\*
del /Q /S JResources.Data\obj\Debug\*
del /Q /S JResources.Data\obj\Release\*

del /Q /S JResources.Web\bin\app.publish\*