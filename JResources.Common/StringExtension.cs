﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace JResources.Common
{
    public static class StringExtension
    {
        /// <summary>
        /// Use the current thread's culture info for conversion
        /// </summary>
        public static string ToTitleCase(this string str)
        {
            var cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
            return cultureInfo.TextInfo.ToTitleCase(str.ToLower());
        }

        /// <summary>
        /// Overload which uses the culture info with the specified name
        /// </summary>
        public static string ToTitleCase(this string str, string cultureInfoName)
        {
            var cultureInfo = new CultureInfo(cultureInfoName);
            return cultureInfo.TextInfo.ToTitleCase(str.ToLower());
        }

        /// <summary>
        /// Overload which uses the specified culture info
        /// </summary>
        public static string ToTitleCase(this string str, CultureInfo cultureInfo)
        {
            return cultureInfo.TextInfo.ToTitleCase(str.ToLower());
        }

        public static string Right(this string value, int length)
        {
            return value.Substring(value.Length - length);
        }

        public static int ToInt(this string value)
        {
            int result = 0;
            if (int.TryParse(value, out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }

        public static decimal ToDecimal(this string value)
        {
            decimal result = 0;
            if (decimal.TryParse(value, NumberStyles.Any, new CultureInfo("en-US"), out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }

        public static bool ToBool(this string value)
        {
            bool result = false;
            if (bool.TryParse(value, out result))
            {
                return result;
            }
            else
            {
                return false;//throw new Exception("Error when parsing value to boolean");
            }
        }

        public static bool IsInt(this string value)
        {
            int result = 0;
            return int.TryParse(value, out result);
        }

        public static Guid ToGuid(this string value)
        {
            Guid guid = Guid.Empty;
            if (Guid.TryParse(value, out guid))
            {
                return guid;
            }
            else
            {
                //throw new Exception("Error when parsing value to guid (unique identifier)");
                return Guid.Empty;
            }
        }

        public static DateTime ToDate(this string value)
        {
            DateTime returnedDate = DateTime.Now;
            value = value.Replace('/', '-');
            value = value.Replace('.', '-');
            List<string> format = new List<string>();
            format.Add("ddMMyyyy");
            format.Add("MMddyyyy");
            format.Add("dd-MM-yyyy");
            format.Add("ddMMyy");
            try
            {

                returnedDate = DateTime.ParseExact(value, format.ToArray(), System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None);
            }
            catch
            {
                try
                {
                    returnedDate = DateTime.ParseExact(value, "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                }
                catch
                {
                    try
                    {
                        returnedDate = DateTime.ParseExact(value, "dd-MMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    }
                    catch
                    {
                        try
                        {
                            returnedDate = DateTime.ParseExact(value, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                        }
                        catch
                        {
                            try
                            {
                                returnedDate = DateTime.ParseExact(value, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                            }
                            catch
                            {
                                DateTime.TryParse(value, out returnedDate);
                            }
                        }
                    }
                }
            }

            return returnedDate;
        }


        public static string ToDecimalFormat(this decimal _number)
        {
            var s = string.Format("{0:0.0000}", _number);

            if (s.EndsWith("0000"))
            {
                return ((int)_number).ToString();
            }
            else if (s.EndsWith("00"))
            {
                return _number.ToString("#.##");
            }
            else
            {
                return s;
            }
        }
    }
}
