﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;

namespace JResources.Common
{
    public class NPOIStandard
    {
        public HSSFWorkbook workBook { get; set; }

        public ICellStyle StandardRowStyle { get; set; }
        public ICellStyle StandardNumberRowStyle { get; set; }
        public ICellStyle HeaderRowStyle { get; set; }
        public ICellStyle HeaderCompletedStyle { get; set; }
        public ICellStyle HeaderPartialStyle { get; set; }
        public ICellStyle HeaderNoneStyle { get; set; }       

        public NPOIStandard(HSSFWorkbook hssfWorkbook)
        {
            workBook = hssfWorkbook;
            StandardRowStyle  = CellStandardRowStyle();
            StandardNumberRowStyle = CellStandardNumberRowStyle();
            HeaderRowStyle = CellHeaderRowStyle();
            HeaderCompletedStyle = CellHeaderCompletedStyle();
            HeaderPartialStyle = CellHeaderPartialStyle();
            HeaderNoneStyle = CellHeaderNoneStyle(); 
        }

        private ICellStyle CellStandardRowStyle()
        {
            ICellStyle styleStandard = workBook.CreateCellStyle();
            styleStandard.BorderRight = BorderStyle.Thin;
            styleStandard.BorderBottom = BorderStyle.Thin;
            styleStandard.Alignment = HorizontalAlignment.Left;
            styleStandard.VerticalAlignment = VerticalAlignment.Top;

            return styleStandard;
        }

        private ICellStyle CellStandardNumberRowStyle()
        {
            ICellStyle styleStandard = workBook.CreateCellStyle();
            styleStandard.BorderRight = BorderStyle.Thin;
            styleStandard.BorderBottom = BorderStyle.Thin;
            styleStandard.Alignment = HorizontalAlignment.Right;
            styleStandard.VerticalAlignment = VerticalAlignment.Top;

            return styleStandard;
        }

        private ICellStyle CellHeaderRowStyle()
        {
            ICellStyle cellStyle = workBook.CreateCellStyle();
            cellStyle.BorderRight = BorderStyle.Thin;
            cellStyle.BorderBottom = BorderStyle.Thin;
            cellStyle.Alignment = HorizontalAlignment.Left;
            cellStyle.FillForegroundColor = IndexedColors.Black.Index;
            cellStyle.FillPattern = FillPattern.SolidForeground;
            cellStyle.VerticalAlignment = VerticalAlignment.Top;

            IFont fontHeader = workBook.CreateFont();
            fontHeader.Color = NPOI.HSSF.Util.HSSFColor.White.Index;
            fontHeader.IsBold = true;
            fontHeader.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            cellStyle.SetFont(fontHeader);

            return cellStyle;
        }

        private ICellStyle CellHeaderCompletedStyle()
        {
            ICellStyle cellStyle = workBook.CreateCellStyle();
            cellStyle.BorderRight = BorderStyle.Thin;
            cellStyle.BorderBottom = BorderStyle.Thin;
            cellStyle.Alignment = HorizontalAlignment.Left;
            cellStyle.FillPattern = FillPattern.SolidForeground;
            cellStyle.FillForegroundColor = IndexedColors.Green.Index;
            cellStyle.VerticalAlignment = VerticalAlignment.Top;

            IFont fontHeader = workBook.CreateFont();
            fontHeader.Color = NPOI.HSSF.Util.HSSFColor.White.Index;
            fontHeader.IsBold = true;
            fontHeader.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            cellStyle.SetFont(fontHeader);

            return cellStyle;
        }

        private ICellStyle CellHeaderPartialStyle()
        {
            ICellStyle cellStyle = workBook.CreateCellStyle();
            cellStyle.BorderRight = BorderStyle.Thin;
            cellStyle.BorderBottom = BorderStyle.Thin;
            cellStyle.Alignment = HorizontalAlignment.Left;
            cellStyle.FillPattern = FillPattern.SolidForeground;
            cellStyle.FillForegroundColor = IndexedColors.LightGreen.Index;
            cellStyle.VerticalAlignment = VerticalAlignment.Top;

            IFont fontHeader = workBook.CreateFont();
            fontHeader.Color = NPOI.HSSF.Util.HSSFColor.Black.Index;
            fontHeader.IsBold = true;
            fontHeader.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            cellStyle.SetFont(fontHeader);

            return cellStyle;
        }

        private ICellStyle CellHeaderNoneStyle()
        {
            ICellStyle cellStyle = workBook.CreateCellStyle();
            cellStyle.BorderRight = BorderStyle.Thin;
            cellStyle.BorderBottom = BorderStyle.Thin;
            cellStyle.Alignment = HorizontalAlignment.Left;
            cellStyle.VerticalAlignment = VerticalAlignment.Top;
            cellStyle.FillForegroundColor = IndexedColors.White.Index;
            cellStyle.FillPattern = FillPattern.SolidForeground;

            IFont fontHeader = workBook.CreateFont();
            fontHeader.Color = NPOI.HSSF.Util.HSSFColor.Black.Index;
            //fontHeader.IsBold = true;
            //fontHeader.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            cellStyle.SetFont(fontHeader);

            return cellStyle;
        }
    }
}
