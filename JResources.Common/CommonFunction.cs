﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace JResources.Common
{
    /// <summary>
    /// Common All Function
    /// </summary>
    public class CommonFunction
    {
        public static DateTime DateTimeJResources(string datestring)
        {
            DateTime dtConvert = DateTime.MinValue;

            if (!string.IsNullOrEmpty(datestring))
            {
                DateTime.TryParseExact(datestring, Constant.FORMAT_DATE_JRESOURCES, null, System.Globalization.DateTimeStyles.None, out dtConvert);
            }

            return dtConvert;
        }

        public static string RowStatusCssHelper(string status)
        {
            switch (status)
            {
                case Enum.RequestStatus.NEW:
                case Enum.RequestStatus.ENTRY_FUEL:
                    return "bg-grey-cararra";

                case Enum.RequestStatus.REVISION:
                    return "bg-grey-steel";

                case Enum.RequestStatus.PARTIAL_APPROVED:
                    return "bg-blue-madison";

                case Enum.RequestStatus.APPROVED:
                    return "bg-blue-steel";

                case Enum.RequestStatus.CANCEL:
                    return "bg-red-pink";

                case Enum.RequestStatus.REJECTED:
                    return "bg-red-sunglo";

                case Enum.RequestStatus.PARTIAL_PURCHASING:
                    return "bg-yellow-lemon";

                case Enum.RequestStatus.FULL_PURCHASING:
                    return "bg-yellow-crusta";
                
                case Enum.RequestStatus.PARTIAL_TRANSFER:
                    return "bg-blue-chambray";

                case Enum.RequestStatus.FULL_TRANSFER:
                    return "bg-blue-ebonyclay";

                case Enum.RequestStatus.PARTIAL_ISSUED:
                case Enum.RequestStatus.PARTIAL_COMPLETED:
                    return "bg-green-haze";

                case Enum.RequestStatus.COMPLETED:
                case Enum.RequestStatus.ISSUED:
                    return "bg-green-meadow";

                default:
                    return "label-default";
            }
        }

        public static string LabelStatusCssHelper(string status)
        {
            switch (status)
            {
                case Enum.RequestStatus.NEW:
                case Enum.RequestStatus.ENTRY_FUEL:
                    return "progress-bar bg-grey-cascade";

                case Enum.RequestStatus.REVISION:
                    return "progress-bar bg-grey-gallery";

                case Enum.RequestStatus.PARTIAL_APPROVED:
                    return "progress-bar bg-blue-madison";

                case Enum.RequestStatus.APPROVED:
                    return "progress-bar bg-blue-steel";

                case Enum.RequestStatus.CANCEL:
                    return "progress-bar bg-red-thunderbird";

                case Enum.RequestStatus.REJECTED:
                case Enum.RequestStatus.REJECT:
                case Enum.RequestStatus.DISCARD:
                    return "progress-bar bg-red-flamingo";

                case Enum.RequestStatus.PARTIAL_PURCHASING:
                    return "progress-bar bg-yellow-lemon";

                case Enum.RequestStatus.FULL_PURCHASING:
                    return "progress-bar bg-yellow-crusta";

                case Enum.RequestStatus.PARTIAL_TRANSFER:
                    return "progress-bar bg-blue-chambray";

                case Enum.RequestStatus.FULL_TRANSFER:
                    return "progress-bar bg-blue-ebonyclay";

                case Enum.RequestStatus.PARTIAL_ISSUED:
                case Enum.RequestStatus.PARTIAL_COMPLETED:
                    return "progress-bar bg-green-haze";

                case Enum.RequestStatus.ISSUED:
                case Enum.RequestStatus.COMPLETED:
                    return "progress-bar bg-green-seagreen";

                case Enum.CBEStatus.WON:
                    return "progress-bar bg-green-jungle";

                case Enum.CBEStatus.LOSE:
                    return "progress-bar bg-red-flamingo";

                case Enum.RequestStatus.TRANSFER:
                    return "progress-bar bg-blue-chambray";

                case DocumentType.FUEL:
                    return "progress-bar bg-yellow";

                case Enum.RequestStatus.POSTING:
                    return "progress-bar purple";

                default:
                    return "progress-bar";
            }
        }

        public static string TargetStatusCssHelper(string status)
        {
            switch (status)
            {
                case Enum.RequestStatus.NEW:
                case Enum.RequestStatus.PARTIAL_APPROVED:
                    return "link-status";

                case Enum.RequestStatus.REVISION:
                    return "link-status";

                default:
                    return "";
            }
        }

        public static string SchedulerCssHelper(int status)
        {
            switch (status)
            {
                case Enum.SCHEDULER_STATUS.NEW:
                    return "progress-bar bg-grey-cascade";

                case Enum.SCHEDULER_STATUS.PROCESS:
                    return "progress-bar bg-blue-steel";

                case Enum.SCHEDULER_STATUS.DONE:
                    return "progress-bar bg-green-seagreen";

                case Enum.SCHEDULER_STATUS.FAILED:
                    return "progress-bar bg-red-thunderbird";

                default:
                    return "progress-bar";
            }
        }

        public static string SchedulerTextHelper(int status)
        {
            switch (status)
            {
                case Enum.SCHEDULER_STATUS.NEW:
                    return "WAITING PROCESS";

                case Enum.SCHEDULER_STATUS.PROCESS:
                    return "PROCESS";

                case Enum.SCHEDULER_STATUS.DONE:
                    return "DONE";

                case Enum.SCHEDULER_STATUS.FAILED:
                    return "FAILED";

                default:
                    return "NONE";
            }
        }

        public static string AddSpacesToSentence(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return "";
            StringBuilder newText = new StringBuilder(text.Length * 2);
            newText.Append(text[0]);
            for (int i = 1; i < text.Length; i++)
            {
                if (char.IsUpper(text[i]) && text[i - 1] != ' ')
                    newText.Append(' ');
                newText.Append(text[i]);
            }
            return newText.ToString();
        }

        public static void DrawTextProgressBar(int progress, int total)
        {
            //draw empty progress bar
            Console.CursorLeft = 0;
            Console.Write("["); //start
            Console.CursorLeft = 32;
            Console.Write("]"); //end
            Console.CursorLeft = 1;
            float onechunk = 30.0f / total;

            //draw filled part
            int position = 1;
            for (int i = 0; i < onechunk * progress; i++)
            {
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.CursorLeft = position++;
                Console.Write(" ");
            }

            //draw unfilled part
            for (int i = position; i <= 31; i++)
            {
                Console.BackgroundColor = ConsoleColor.Gray;
                Console.CursorLeft = position++;
                Console.Write(" ");
            }

            //draw totals
            Console.CursorLeft = 35;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write(progress.ToString() + " of " + total.ToString() + "    "); //blanks at the end remove any excess
        }
    }
}
