﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Common
{
    public class DocumentType
    {
        public const string MSR = "MSR";
        public const string MSR_ROP = "MSR_ROP";
        public const string FRF = "FRF";
        public const string FUEL = "FUEL";
        public const string REPORT = "REPORT";
        public const string CEA_REMINDER = "CEA_REMINDER";
        public const string CEA_CLOSED = "CEA_CLOSED";
        public const string ACCPAC_RECHECK = "MSR_RECHECK";

        public const string ISSUED = "ISSUED";
        public const string TRANSFER = "TRANSFER";
        public const string REVERSE = "REVERSE";
        public const string PURCHASE = "PURCHASE";
        public const string CANCEL = "CANCEL";
        public const string CBE = "CBE";

        public const string CONTRACT = "CONTRACT";

        public const string PST_PLANNING = "PST-PLANNING";

        public const string PST_SCHEDULE = "PST-SCHEDULE";
        public const string PST_TRANSACTED = "PST-TRANSACTED";
        public const string PST_RANDOM = "PST-RANDOM";

    }
}
