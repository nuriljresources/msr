﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Common.Enum
{
    public class EmailType
    {
        public const string MSR_NEW = "MSR_NEW";
        public const string MSR_REVISION = "MSR_REVISION";
        public const string MSR_APPROVED = "MSR_APPROVED";
        public const string REPORT = "REPORT";
    }
}
