﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Common.Enum
{
    public class JobPriority
    {
        public const string P01 = "P01";
        public const string P02 = "P02";
        public const string P03 = "P03";
        public const string P04 = "P04";
        public const string P05 = "P05";
        public const string P06 = "P06";
        public const string P07 = "P07";
        public const string P08 = "P08";
        public const string P09 = "P09";
        public const string P10 = "P10";
    }
}
