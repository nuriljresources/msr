﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace JResources.Common
{
    public enum MSRType
    {       
        [Description("Inventory")]
        Inventory = 1,

        [Description("Non Inventory")] 
        NonInventory = 2
    }

}
