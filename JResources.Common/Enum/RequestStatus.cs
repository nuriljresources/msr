﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace JResources.Common.Enum
{
    public static class RequestStatus
    {
        public const string PARTIAL_APPROVED = "PAPP";
        public const string APPROVED = "APP";

        public const string CANCEL = "CAN";
        public const string ISSUED = "CLO";
        public const string TRANSFER = "TRF";
        public const string REVERSE_TRANSFER = "RVS_TRF";
        public const string NEW = "ENT";
        public const string ENTRY_FUEL = "ENTF";
        public const string EXPORTED_TO_ACCPAC = "EXP";
        public const string ON_HOLD = "OHL";

        public const string PARTIAL_PURCHASING = "PAP";
        public const string FULL_PURCHASING = "FUP";

        public const string PARTIAL_TRANSFER = "PAT";
        public const string FULL_TRANSFER = "FUT";

        public const string PARTIAL_COMPLETED = "PAC";
        public const string PARTIAL_ISSUED = "PAI";

        public const string COMPLETED = "COM";
        public const string PROCESSING = "PRC";
        public const string REJECTED = "RJT";
        public const string PURCHASING = "RTA";
        public const string REVISION = "RVS";
        public const string REVIEW_PLANNER = "RVW";
        public const string WAREHOUSE = "WPR";
        public const string CBE = "CBE";

        public const string DRAFT = "DRAFT";
        public const string SUBMITTED = "SUBMITTED";
        public const string POSTING = "POSTING";
        public const string REJECT = "REJECT";
        public const string DISCARD = "DISCARD";

    }


    public static class CEAStatus
    {
        public const string NEW = "NEW";
        public const string IN_PROGRESS = "IN PROGRESS";
        public const string COMPLETED = "COMPLETED";
        public const string CLOSED = "CLOSED";
    }

    public static class CBEStatus
    {
        public const string NEW = "NEW";
        public const string DRAFT = "DRAFT";
        public const string SUBMITED = "SUBMITED";
        public const string REJECT = "REJECT";
        public const string DISCARD = "DISCARD";
        public const string REVISE = "REVISE";
        public const string APPROVED = "APPROVED";
        public const string POSTING = "POSTING";

        public const string WON = "WON";
        public const string LOSE = "LOSE";
    }


}

