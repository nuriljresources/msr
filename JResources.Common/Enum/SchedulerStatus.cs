﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Common.Enum
{
    public class SCHEDULER_STATUS
    {
        public const int NEW = 0;
        public const int PROCESS = 1;
        public const int FAILED = 2;
        public const int DONE = 3;
    }
}
