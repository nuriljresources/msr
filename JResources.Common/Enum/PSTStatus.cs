﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace JResources.Common.Enum
{
    public static class PSTStatus
    {
        public const string NEW = "NEW";
        public const string FIRST_COUNT = "FIRST-COUNT";
        public const string FINAL_COUNT = "FINAL-COUNT";
        public const string ADJUSTED = "ADJUSTED";

        public static List<SelectListItem> GetSelectList()
        {
            var list = new List<SelectListItem>();
            list.Add(new SelectListItem() { Value = "", Text = "Select Status", Selected = true });
            list.Add(new SelectListItem() { Text = "New", Value = NEW });
            list.Add(new SelectListItem() { Text = "First Count", Value = FIRST_COUNT });
            list.Add(new SelectListItem() { Text = "Final Count", Value = FINAL_COUNT });
            list.Add(new SelectListItem() { Text = "Adjusted", Value = ADJUSTED });

            return list;
        }

    }
}
