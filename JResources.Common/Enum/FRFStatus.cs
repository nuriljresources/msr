﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Common.Enum
{
    public static class FRFStatus
    {
        public const string APPROVED = "APP";
        public const string ISSUED = "CLO";
        public const string ENTRY_FUEL = "ENTF";
        public const string REJECTED = "RJT";
        public const string REVISION = "RVS";

    }
}