﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Common.Enum
{
    public static class WorkOrderStatus
    {
        public const int PENDING = 0;
        public const int IN_PROGRESS = 1;
        public const int HELD = 2;
        public const int CLOSED = 3;
        public const int COMPLETE = 4;
        public const int WAITING_FOR_PARTS = 5;
        public const int COMPLETE_ADMIN = 6;
        public const int REJECTED = 7;                                                  
    }
}
