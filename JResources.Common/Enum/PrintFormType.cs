﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Common.Enum
{
    public class PRINT_FORM_TYPE
    {
        public const string PICKING_SLIP = "PICKING_SLIP";
        public const string GOOD_ISSUED = "GOOD_ISSUED";
        public const string MSR_PRINT_OUT = "MSR_PRINT_OUT";
        public const string MSR_STATUS = "MSR_STATUS";
        public const string IC_LOCATION = "IC_LOCATION";
        public const string CHECK_MSR_ACCPAC = "CHECK_MSR_ACCPAC";
        public const string PO_RECONCILE = "PO_RECONCILE";
    }
}
