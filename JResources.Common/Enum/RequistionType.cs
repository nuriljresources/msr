﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Common.Enum
{
    public static class REQUISITION_TYPE
    {
        public const string PURCHASE = "PURCHASE";
        public const string CONTRACT = "CONTRACT";
    }
}
