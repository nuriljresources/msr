﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace JResources.Common
{
    public class SiteSettings
    {
        public static bool IS_LIVE
        {
            get
            {
                bool isLive = true;

                var BASE_URL = ConfigurationManager.AppSettings["BASE_URL"];
                if (BASE_URL != null)
                {
                    if (BASE_URL.ToString().Contains("localhost"))
                    {
                        isLive = false;
                    }
                }

                return isLive;
            }
        }

        public static string VersionNumber
        {
            get
            {
                string versionApp = SiteSettings.VERSION_APP;

                if (!IS_LIVE)
                {
                    return DateTime.Now.ToString("yyyyMMddhhss");
                }

                return versionApp;
            }
        }

        public static string BASE_URL
        {
            get
            {
                string strBaseUrl = string.Empty;
                var BASE_URL = ConfigurationManager.AppSettings["BASE_URL"];
                if (BASE_URL != null)
                {
                    strBaseUrl = BASE_URL.ToString();
                }

                return strBaseUrl;
            }
        }

        public static string REPORT_SERVER_URL
        {
            get
            {
                string strBaseUrl = string.Empty;
                var BASE_URL = ConfigurationManager.AppSettings["REPORT_SERVER_URL"];
                if (BASE_URL != null)
                {
                    strBaseUrl = BASE_URL.ToString();
                }

                return strBaseUrl;
            }
        }

        public static bool IS_ACCPAC_CONNECT
        {
            get
            {
                bool IsUse = false;
                string strDomain = string.Empty;
                var domain = ConfigurationManager.AppSettings["IS_ACCPAC_CONNECT"];
                if (domain != null)
                {
                    strDomain = ConfigurationManager.AppSettings["IS_ACCPAC_CONNECT"].ToString();
                    if (strDomain.ToUpper() == "TRUE")
                    {
                        IsUse = true;
                    }
                }

                return IsUse;
            }
        }

        public static string ACCPAC_URL
        {
            get
            {
                string strBaseUrl = string.Empty;
                var BASE_URL = ConfigurationManager.AppSettings["ACCPAC_URL"];
                if (BASE_URL != null)
                {
                    strBaseUrl = BASE_URL.ToString();
                }

                return strBaseUrl;
            }
        }

        public static string ACCPAC_USERNAME
        {
            get
            {
                string strConfig = string.Empty;
                var domain = ConfigurationManager.AppSettings["ACCPAC_USERNAME"];
                if (domain != null)
                {
                    strConfig = ConfigurationManager.AppSettings["ACCPAC_USERNAME"].ToString();
                }

                return strConfig;
            }
        }

        public static string ACCPAC_PASSWORD
        {
            get
            {
                string strConfig = string.Empty;
                var domain = ConfigurationManager.AppSettings["ACCPAC_PASSWORD"];
                if (domain != null)
                {
                    strConfig = ConfigurationManager.AppSettings["ACCPAC_PASSWORD"].ToString();
                }

                return strConfig;
            }
        }

        public static string CREATED_BY
        {
            get
            {
                string CREATED_BY = string.Empty;
                var BASE_URL = ConfigurationManager.AppSettings["CREATED_BY"];
                if (BASE_URL != null)
                {
                    CREATED_BY = BASE_URL.ToString();
                }

                return CREATED_BY;
            }
        }

        public static DateTime DATE_LIVE
        {
            get
            {
                DateTime date_live = DateTime.Now;
                var BASE_URL = ConfigurationManager.AppSettings["DATE_LIVE"];
                if (BASE_URL != null)
                {
                    date_live = CommonFunction.DateTimeJResources(BASE_URL.ToString());
                }

                return date_live;
            }
        }

        public static bool IS_USE_ACTIVE_DIRECTORY
        {
            get
            {
                bool IsUse = false;
                string strDomain = string.Empty;
                var domain = ConfigurationManager.AppSettings["IS_USE_ACTIVE_DIRECTORY"];
                if (domain != null)
                {
                    strDomain = ConfigurationManager.AppSettings["IS_USE_ACTIVE_DIRECTORY"].ToString();
                    if (strDomain.ToUpper() == "TRUE")
                    {
                        IsUse = true;
                    }
                }

                return IsUse;
            }
        }

        public static string DOMAIN_ACTIVE_DIRECTORY
        {
            get
            {
                string strDomain = string.Empty;
                var domain = ConfigurationManager.AppSettings["DOMAIN_ACTIVE_DIRECTORY"];
                if(domain != null)
                {
                    strDomain = ConfigurationManager.AppSettings["DOMAIN_ACTIVE_DIRECTORY"].ToString();
                }

                return strDomain;
            }
        }

        public static string PATH_UPLOAD_EXCEL
        {
            get
            {
                string strDomain = string.Empty;
                var domain = ConfigurationManager.AppSettings["PATH_UPLOAD_EXCEL"];
                if (domain != null)
                {
                    strDomain = ConfigurationManager.AppSettings["PATH_UPLOAD_EXCEL"].ToString();
                }

                return strDomain;
            }
        }

        public static int PAGE_SIZE_PAGING
        {
            get
            {
                int strSetting = 10;
                var strValue = ConfigurationManager.AppSettings["PAGE_SIZE_PAGING"];
                if (strValue != null)
                {
                    int.TryParse(strValue.ToString(), out strSetting);
                }

                return strSetting;
            }
        }

        public static bool IS_EMAIL_ACTIVE
        {
            get
            {
                bool IsUse = false;
                string strDomain = string.Empty;
                var domain = ConfigurationManager.AppSettings["IS_EMAIL_ACTIVE"];
                if (domain != null)
                {
                    strDomain = ConfigurationManager.AppSettings["IS_EMAIL_ACTIVE"].ToString();
                    if (strDomain.ToUpper() == "TRUE")
                    {
                        IsUse = true;
                    }
                }

                return IsUse;
            }
        }

        public static string EMAIL_SMTP
        {
            get
            {
                string strConfig = string.Empty;
                var domain = ConfigurationManager.AppSettings["EMAIL_SMTP"];
                if (domain != null)
                {
                    strConfig = ConfigurationManager.AppSettings["EMAIL_SMTP"].ToString();
                }

                return strConfig;
            }
        }
        
        public static string EMAIL_USERNAME
        {
            get
            {
                string strConfig = string.Empty;
                var domain = ConfigurationManager.AppSettings["EMAIL_USERNAME"];
                if (domain != null)
                {
                    strConfig = ConfigurationManager.AppSettings["EMAIL_USERNAME"].ToString();
                }

                return strConfig;
            }
        }

        public static string EMAIL_PASSWORD
        {
            get
            {
                string strConfig = string.Empty;
                var domain = ConfigurationManager.AppSettings["EMAIL_PASSWORD"];
                if (domain != null)
                {
                    strConfig = ConfigurationManager.AppSettings["EMAIL_PASSWORD"].ToString();
                }

                return strConfig;
            }
        }

        public static string EMAIL_TEMPLATE_PATH
        {
            get
            {
                string strConfig = string.Empty;
                var domain = ConfigurationManager.AppSettings["EMAIL_TEMPLATE_PATH"];
                if (domain != null)
                {
                    strConfig = ConfigurationManager.AppSettings["EMAIL_TEMPLATE_PATH"].ToString();
                }

                return strConfig;
            }
        }


        public static List<string> ITEM_FUELS
        {
            get
            {
                List<string> items = new List<string>();

                var domain = ConfigurationManager.AppSettings["ITEM_FUELS"];
                if (domain != null && !string.IsNullOrEmpty(domain.ToString()))
                {
                    var strConfig = domain.ToString();
                    var strConfigs = strConfig.Split('|').Where(x => !string.IsNullOrEmpty(x)).ToList();
                    if (strConfigs != null && strConfigs.Count > 0)
                    {
                        items = strConfigs;
                    }
                }

                return items;
            }
        }

        public static bool IS_PST_LIVE
        {
            get
            {
                bool IsUse = false;
                string strDomain = string.Empty;
                var domain = ConfigurationManager.AppSettings["IS_PST_LIVE"];
                if (domain != null)
                {
                    strDomain = ConfigurationManager.AppSettings["IS_PST_LIVE"].ToString();
                    if (strDomain.ToUpper() == "TRUE")
                    {
                        IsUse = true;
                    }
                }

                return IsUse;
            }
        }

        public static string OPTFIELD_SHIPMENT
        {
            get
            {
                string strConfig = string.Empty;
                var domain = ConfigurationManager.AppSettings["OPTFIELD_SHIPMENT"];
                if (domain != null)
                {
                    strConfig = ConfigurationManager.AppSettings["OPTFIELD_SHIPMENT"].ToString();
                }

                return strConfig;
            }
        }

        public static string RQN_USERNAME
        {
            get
            {
                string strConfig = string.Empty;
                var domain = ConfigurationManager.AppSettings["RQN_USERNAME"];
                if (domain != null)
                {
                    strConfig = ConfigurationManager.AppSettings["RQN_USERNAME"].ToString();
                }

                return strConfig;
            }
        }

        public static string RQN_PASSWORD
        {
            get
            {
                string strConfig = string.Empty;
                var domain = ConfigurationManager.AppSettings["RQN_PASSWORD"];
                if (domain != null)
                {
                    strConfig = ConfigurationManager.AppSettings["RQN_PASSWORD"].ToString();
                }

                return strConfig;
            }
        }

        public static int SIZE_SCHEDULER_TRY
        {
            get
            {
                int strSetting = 10;
                var strValue = ConfigurationManager.AppSettings["SIZE_SCHEDULER_TRY"];
                if (strValue != null)
                {
                    int.TryParse(strValue.ToString(), out strSetting);
                }

                return strSetting;
            }
        }

        public static string VERSION_APP
        {
            get
            {
                string strConfig = "version 1.0";
                var domain = ConfigurationManager.AppSettings["VERSION_APP"];
                if (domain != null)
                {
                    strConfig = ConfigurationManager.AppSettings["VERSION_APP"].ToString();
                }

                return strConfig;
            }
        }

        public static string PATH_DOWNLOAD
        {
            get
            {
                string strConfig = string.Empty;
                var domain = ConfigurationManager.AppSettings["PATH_DOWNLOAD"];
                if (domain != null)
                {
                    strConfig = ConfigurationManager.AppSettings["PATH_DOWNLOAD"].ToString();
                }

                return strConfig;
            }
        }

        public static string ASSET_API
        {
            get
            {
                string strBaseUrl = string.Empty;
                var BASE_URL = ConfigurationManager.AppSettings["ASSET_API"];
                if (BASE_URL != null)
                {
                    strBaseUrl = BASE_URL.ToString();
                }

                return strBaseUrl;
            }
        }

    }
}
