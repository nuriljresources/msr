﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JResources.Common
{
    public class ROLE_CODE
    {
        /// <summary>
        /// LEVEL ADMINISTRATOR ALL ACCESS
        /// </summary>
        public const string ADMINISTRATOR = "ADMINISTRATOR";
        /// <summary>
        /// LEVEL DEFAULT USER
        /// </summary>
        public const string USERS = "USERS";

        public const string MSR_ENTRY = "MSR_ENTRY";
        public const string MSR_ROP_ENTRY = "MSR_ROP_ENTRY";
        public const string MSR_ISSUED = "MSR_ISSUED";
        public const string MSR_CANCEL = "MSR_CANCEL";
        public const string MSR_REVERSE_TRANSFER = "MSR_REVERSE_TRANSFER";

        public const string FRF_ENTRY = "FRF_ENTRY";
        public const string FRF_APPROVAL = "FRF_APPROVAL";
        public const string FRF_ISSUED = "FRF_ISSUED";

        public const string LOGISTIC = "LOGISTIC";
        public const string PROCUREMENT = "PROCUREMENT";


        public const string PST_CREATOR = "PST_CREATOR";        
        public const string REPORT = "REPORT";


        public const string MSR_APPROVAL = "MSR_APPROVAL";
        public const string MSR_APPROVAL_GM = "MSR_APPROVAL_GM";
        public const string MSR_APPROVAL_VP = "MSR_APPROVAL_VP";

        //20200826FM D.Nugroho
        public const string CEA_ENTRY = "CEA_ENTRY";
    }

    public class REPORT_CODE
    {
        public const string MSRStatus = "MSR_STATUS";
        public const string HISTORICAL_TRANSACTION = "HISTORICAL_TRANSACTION";
        public const string REQUISITION = "REQUISITION";
        public const string FRF = "FRF";
        public const string PICKING_LIST = "PICKING_LIST";
    }
}
